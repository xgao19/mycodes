//
//  main.cpp
//  Cosh_EffMassJackk
//
//  Created by gaoxiang on 3/19/19.
//  Copyright © 2019 Xiang Gao. All rights reserved.
//

#include <iterator>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <typeinfo>

#define rootmax 2
#define rootmin 0.00002
#define NT 128
#define rootprecision 0.0000001

double CoshEffMass(int t_nt, double t_mass, double t_cRitio){
    return t_cRitio*cosh(t_mass*(t_nt+1.0-NT/2.0))-cosh(t_mass*(t_nt-NT/2.0));
}


class CSVRow
{
public:
    std::string const& operator[](std::size_t index) const
    {
        return m_data[index];
    }
    std::size_t size() const
    {
        return m_data.size();
    }
    void readNextRow(std::istream& str)
    {
        std::string         line;
        std::getline(str, line);
        
        std::stringstream   lineStream(line);
        std::string         cell;
        
        m_data.clear();
        while(std::getline(lineStream, cell, ','))
        {
            m_data.push_back(cell);
        }
        // This checks for a trailing comma with no data after it.
        if (!lineStream && cell.empty())
        {
            // If there was a trailing comma then add an empty element.
            m_data.push_back("");
        }
    }
private:
    std::vector<std::string>    m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}


int main(int argc, const char * argv[]) {
    
    std::cout << "********** ./run filename.csv blocksize **********" << std::endl;
    if (argc<=2) {
        std::cout << "\n------------Bad input format!------------\n" << std::endl;
        std::cout << "********** Error! Please try again! **********\n" << std::endl;
        return 0;
    }
    
    //Open the input file
    std::ifstream infile;
    infile.open(argv[1]);
    if(!infile) std::cout << "Open file " << argv[1] << " failed!" << std::endl;
    int BlockSz=atoi(argv[2]);
    
    //Open a output file
    std::stringstream stream;
    stream << argv[1] << "_EMCosh_JackError_B" << BlockSz << ".txt";
    std::string filename;
    stream >> filename;
    std::ofstream Savefile(filename);
    Savefile.precision(12);
    std::cout.precision(12);
    
    //Read the data, and run the analysis
    int Nblock,Nconf,i,j;
    double mean=0,error=0,mean1,mean2,bmean1,bmean2,findroot=0,massmin,massmax,massmid;
    CSVRow row,rownext;
    infile >> row;//To remove the first describe line.
    infile >> row;//Get the first row of datas
    Nconf=row.size()-1;
    std::cout << "\n--------The number of configurations is " << Nconf << "--------\n" <<std::endl;
    Nblock=Nconf/BlockSz;
    double meanBlock[Nblock];//Mean value computed from block data
    while(infile >> rownext)
    {
        //Define the blocks
        Savefile << stod(row[0])+0.5 << "\t";
        
        //Initilaze the array above
        for (i=0; i<Nblock; i++) {meanBlock[i]=0;}
        
        //Calulate the whole mean value
        mean1=0;mean2=0;mean=0;
        for (i=0; i<Nconf; i++) {
            mean1+=stod(row[i+1]);
            mean2+=stod(rownext[i+1]);
        }
        findroot=0;massmin=rootmin;massmax=rootmax;
        do {
            massmid=(massmin+massmax)/2.0;
            findroot=CoshEffMass(stod(row[0]), massmid, mean1/mean2);
            //cout << findroot << endl;
            //cout << massmin << "\t" << massmid << "\t" << massmax << endl;
            //cout << CoshEffMass(stod(row[0]), massmin, bmean1/bmean2) << "\t" << CoshEffMass(stod(row[0]), massmid, bmean1/bmean2) << "\t" << CoshEffMass(stod(row[0]), massmax, bmean1/bmean2) << endl;
            if (findroot*CoshEffMass(stod(row[0]), massmax, mean1/mean2)<0) {
                massmin=massmid;
            }
            else{
                massmax=massmid;
            }
        } while ((massmax-massmin)>rootprecision);
        mean=massmid;
        std::cout << row[0] << "\t" << mean << std::endl;
        Savefile << mean << "\t";
        
        //Caculate the block mean value
        for (i=0; i<Nblock; i++) {
            bmean1=0;bmean2=0;
            for (j=0; j<BlockSz; j++) {
                bmean1+=stod(row[i*BlockSz+j+1]);
                bmean2+=stod(rownext[i*BlockSz+j+1]);
            }
            
            //find the root(effective mass)
            findroot=0;massmin=rootmin;massmax=rootmax;
            do {
                massmid=(massmin+massmax)/2.0;
                findroot=CoshEffMass(stod(row[0]), massmid, (mean1-bmean1)/(mean2-bmean2));
                //cout << findroot << endl;
                //cout << massmin << "\t" << massmid << "\t" << massmax << endl;
                //cout << CoshEffMass(stod(row[0]), massmin, bmean1/bmean2) << "\t" << CoshEffMass(stod(row[0]), massmid, bmean1/bmean2) << "\t" << CoshEffMass(stod(row[0]), massmax, bmean1/bmean2) << endl;
                if (findroot*CoshEffMass(stod(row[0]), massmax, (mean1-bmean1)/(mean2-bmean2))<0) {
                    massmin=massmid;
                }
                else{
                    massmax=massmid;
                }
            } while ((massmax-massmin)>rootprecision);
            meanBlock[i]=massmid;
            //cout << massmid << endl;
        }
        
        //use the block mean value to estimate the error
        error=0;
        for (i=0; i<Nblock; i++) {
            error+=(meanBlock[i]-mean)*(meanBlock[i]-mean);
        }
        error=sqrt(error*(Nblock-1)/Nblock);
        Savefile << error << std::endl;
        row=rownext;
    }
    
    infile.close();
    Savefile.close();
    std::cout << "***********    Work Is Done!    ***********" << std::endl;
    return 0;
}
