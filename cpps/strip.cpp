//
//  main.cpp
//  strip
//
//  Created by gaoxiang on 7/17/19.
//  Copyright © 2019 Xiang Gao. All rights reserved.
//

#include <iterator>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <typeinfo>

#define Max1d 20
#define datalen 2
#define ARR_LEN(array, length){ length =  sizeof(array) / sizeof(array[0]); }
#define SAVE "temp_matrix.txt"

double ** bias_matrix(const char *exfiles, const char *slfiles, int * matrixsizes);
double ** sl_stripping(double *** data, int * matrixsize);
double ** ama_stripping(double ** biasmatrix, double ** slmatrix, int * matrixsize);
double ***read_data(const char *files, int * matrixsize);
int linecount(std::string file);
void savematrix(double ** matrix, int * matrixsize);


int main(int argc, const char * argv[]) {
    
    if (argc == 1){
        std::cout << "ex, sl, or ama stripping." << std::endl;
        std::cout << "Example command format:" << std::endl;
        std::cout << "./run ex datafiles.txt" << std::endl;
        std::cout << "./run sl datafiles.txt" << std::endl;
        std::cout << "./run ama exdatafiles sldatafiles.txt" << std::endl;
    }
    
    else if (!strncmp(argv[1],"ex",2)){
        
        std::cout << "ex stripping" << std::endl;
        int * matrixsize = new int[3];
        double *** exdata = read_data(argv[2], matrixsize);
        double ** matrix = sl_stripping(exdata, matrixsize);
        savematrix(matrix, matrixsize);
    }
    else if (!strncmp(argv[1],"sl",2)){
        std::cout << "sl stripping" << std::endl;
        int * matrixsize = new int[3];
        double *** sldata = read_data(argv[2], matrixsize);
        double ** matrix = sl_stripping(sldata, matrixsize);
        savematrix(matrix, matrixsize);
    }
    else if (!strncmp(argv[1],"ama",2)){
         std::cout << "ama stripping" << std::endl;
        int * matrixsize = new int[3];
        double *** sldata = read_data(argv[3], matrixsize);
        double ** slmatrix=sl_stripping(sldata, matrixsize);
        double ** biasmatrix = bias_matrix(argv[2], argv[3], matrixsize);
        double ** amamatrix = ama_stripping(biasmatrix, slmatrix, matrixsize);
        savematrix(amamatrix, matrixsize);
    }
    else{
        std::cout << "ex, sl, or ama stripping." << std::endl;
        std::cout << "Example command format:" << std::endl;
        std::cout << "./run ex datafiles.txt" << std::endl;
        std::cout << "./run sl datafiles.txt" << std::endl;
        std::cout << "./run ama exdatafiles sldatafiles.txt\n" << std::endl;
    }
    
    return 0;
}

int linecount(std::string file){
    
    std::string tmp;
    int n=0;
    
    std::ifstream infile;
    infile.open(file);
    if(!infile) std::cout << "Open file " << file << " failed!" << std::endl;
    
    while(getline(infile,tmp,'\n'))
    {
        n++;
    }
    
    infile.close();
    return n;
}

void savematrix(double ** matrix, int * matrixsize){
    
    std::string filename = SAVE;
    std::ofstream savefile(filename);
    savefile.precision(12);
    
    for (int i=0; i<matrixsize[1]; i++) {
        for (int j=0; j<matrixsize[2]; j++) {
            savefile << matrix[i][j] << "\t";
        }
        savefile << "\n";
        //std::cout << matrix_ave[i][0] << "\t" << matrix_ave[i][1] << std::endl;
    }
    
    savefile.close();
}

double ***read_data(const char *files, int * matrixsize){
    
    std::string filelist[Max1d];
    int nfiles = 0;
    
    //std::cout << files[0] << std::endl;
    
    // Open the input file
    std::ifstream filestream;
    filestream.open(files);
    if(!filestream) std::cout << "Open file " << files << " failed!" << std::endl;
    
    std::string datafile;
    while(getline(filestream, datafile)){
        filelist[nfiles] = datafile;
        nfiles += 1;
    }
    filestream.close();
    
    double *** dataset;
    
    dataset = new double **[nfiles];
    
    int nline = linecount(filelist[0]);
    
    for (int i=0; i<nfiles; i++) {
        
        dataset[i] = new double * [nline];
        
        // Open the data file
        std::ifstream datastream;
        datastream.open(filelist[i]);
        if(!datastream) std::cout << "Open file " << datafile << " failed!" << std::endl;
        
        for (int j=0; j<nline; j++) {
            dataset[i][j] = new double[datalen];
            datastream >> dataset[i][j][0];
            datastream >> dataset[i][j][1];
            
        }

        datastream.close();
        //std::cout << i << "\t" << dataset[i][0][0] << "\t" << dataset[i][0][1] << std::endl;
    }
    
    matrixsize[0] = nfiles;
    matrixsize[1] = nline;
    matrixsize[2] = datalen;
    
    return dataset;
}

double ** sl_stripping(double *** matrixdata, int * matrixsize){
    
    //std::cout << matrixdata[0][0][0] << "\t" << matrixdata[0][0][1] << std::endl;
    
    double ** matrix_ave = new double * [matrixsize[1]];
    
    for (int i=0; i<matrixsize[1]; i++) {
        matrix_ave[i] = new double [matrixsize[2]];
        for(int j=0; j<matrixsize[2]; j++){
            matrix_ave[i][j] = 0;
        }
    }
    
    for (int i=0; i<matrixsize[0]; i++) {
        for (int j=0; j<matrixsize[1]; j++) {
            for (int k=0; k<matrixsize[2]; k++) {
                
                matrix_ave[j][k] += matrixdata[i][j][k];
                //std::cout << matrixdata[i][j][k] << std::endl;
                
            }
        }
    }

    for (int i=0; i<matrixsize[1]; i++) {
        for (int j=0; j<matrixsize[2]; j++) {
            matrix_ave[i][j]/=matrixsize[0];
        }
    }
    
    return matrix_ave;
}

double ** bias_matrix(const char *exfiles, const char *slfiles, int * matrixsizes){
    
    double exmatrix[matrixsizes[1]][matrixsizes[2]];
    double slmatrix[matrixsizes[1]][matrixsizes[2]];
    double ** biasmatrix = new double * [matrixsizes[1]];
    
    // Open the input file
    std::ifstream filestream;
    filestream.open(exfiles);
    if(!filestream) std::cout << "Open file " << exfiles << " failed!" << std::endl;
    std::string exdatafile;
    getline(filestream, exdatafile);
    filestream.close();
    
    filestream.open(slfiles);
    if(!filestream) std::cout << "Open file " << slfiles << " failed!" << std::endl;
    std::string sldatafile;
    getline(filestream, sldatafile);
    filestream.close();
    
    // Open the data file
    std::ifstream datastream;
    datastream.open(exdatafile);
    if(!datastream) std::cout << "Open file " << exdatafile << " failed!" << std::endl;
    for (int i=0; i<matrixsizes[1]; i++) {
        datastream >> exmatrix[i][0];
        datastream >> exmatrix[i][1];
    }
    datastream.close();
    
    datastream.open(sldatafile);
    if(!datastream) std::cout << "Open file " << sldatafile << " failed!" << std::endl;
    for (int i=0; i<matrixsizes[1]; i++) {
        datastream >> slmatrix[i][0];
        datastream >> slmatrix[i][1];
    }
    datastream.close();
    
    // computing the bias matrix
    for (int i=0; i<matrixsizes[1]; i++) {
        biasmatrix[i] = new double [matrixsizes[2]];
        for (int j=0; j<matrixsizes[2]; j++) {
            biasmatrix[i][j] = exmatrix[i][j] - slmatrix[i][j];
        }
    }
    
    return biasmatrix;
}

double ** ama_stripping(double ** biasmatrix, double ** slmatrix, int * matrixsize){
    
    double ** amamatrix = new double * [matrixsize[1]];
    for (int i=0; i<matrixsize[1]; i++) {
        amamatrix[i] = new double [matrixsize[2]];
        for (int j=0; j<matrixsize[2]; j++) {
            amamatrix[i][j] = biasmatrix[i][j] + slmatrix[i][j];
        }
    }
    return amamatrix;
}
