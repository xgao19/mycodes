//
//  main.cpp
//  Cosh_EffMassBoots
//
//  Created by gaoxiang on 3/19/19.
//  Copyright © 2019 Xiang Gao. All rights reserved.
//

#include <iterator>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <typeinfo>

#define rootmax 20.0
#define rootmin 0.00002
#define NT 64
#define rootprecision 0.00000001

double coshEffMass(int t_nt, double t_mass, double t_cRitio){
    return t_cRitio*cosh(t_mass*(t_nt+1.0-NT/2.0))-cosh(t_mass*(t_nt-NT/2.0));
}

class CSVRow
{
public:
    std::string const& operator[](std::size_t index) const
    {
        return m_data[index];
    }
    std::size_t size() const
    {
        return m_data.size();
    }
    void readNextRow(std::istream& str)
    {
        std::string         line;
        std::getline(str, line);
        
        std::stringstream   lineStream(line);
        std::string         cell;
        
        m_data.clear();
        while(std::getline(lineStream, cell, ','))
        {
            m_data.push_back(cell);
        }
        // This checks for a trailing comma with no data after it.
        if (!lineStream && cell.empty())
        {
            // If there was a trailing comma then add an empty element.
            m_data.push_back("");
        }
    }
private:
    std::vector<std::string>    m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}


int main(int argc, const char * argv[]) {
    
    std::cout << "******** ./run filename.csv blocksize Npick ********" << std::endl;
    if (argc<=2) {
        std::cout << "\n------------     Bad input format!     ------------\n" << std::endl;
        std::cout << "**********    Error! Please try again!    **********\n" << std::endl;
        return 0;
    }
    
    //Open the input file
    std::ifstream infile;
    infile.open(argv[1]);
    if(!infile) std::cout << "Open file " << argv[1] << " failed!" << std::endl;
    int BlockSz=atoi(argv[2]);
    int Npick=atoi(argv[3]);
    
    //Open a output file
    std::stringstream stream;
    stream << argv[1] << "_EMCosh_BootsError_B" << BlockSz << "_P" << Npick << ".txt";
    std::string filename;
    stream >> filename;
    std::ofstream Savefile(filename);
    Savefile.precision(12);
    std::cout.precision(12);
    
    //Seed the random number generator
    srand((unsigned)time(NULL));
    //cout << RAND_MAX << endl;
    
    //Read the data, and run the analysis
    int Nblock,Nconf,i,j,k,t;
    double error=0,mean,bmean1,bmean2,findroot=0,massmin,massmax,massmid;
    CSVRow row,rownext;
    infile >> row;//To remove the first describe line.
    infile >> row;//Get the first row of datas
    Nconf=row.size()-1;
    std::cout << "\n--------The number of configurations is " << Nconf << "--------\n" <<std::endl;
    Nblock=Nconf/BlockSz;
    double bootsmean[Npick];// mean value computed from randomly picked block data
    int meanmid=(int)(0.5*Npick);
    int meanlow16=(int)ceil(0.16*Npick);
    int meanhigh16=(int)(0.84*Npick);
    while(infile >> rownext)
    {
        // define the blocks
        Savefile << stod(row[0])+0.5 << "\t";
        std::cout << stod(row[0])+0.5 << "\t";
        
        // initilaze the array
        for (i=0; i<Npick; i++) {bootsmean[i]=0;}
        
        //Caculate the block effective mass
        for (t=0; t<Npick; t++) {
            //std::cout << t << std::endl;
            bmean1=0;bmean2=0;
            for (i=0; i<Nblock; i++) {
                k=(int)((double)Nblock*rand()/RAND_MAX);
                for (j=0; j<BlockSz; j++) {
                    bmean1+=stod(row[k*BlockSz+j+1]);
                    bmean2+=stod(rownext[k*BlockSz+j+1]);
                }
            }
            //find the root(effective mass)
            findroot=0;massmin=rootmin;massmax=rootmax;
            do {
                massmid=(massmin+massmax)/2.0;
                findroot=coshEffMass(stod(row[0]), massmid, bmean1/bmean2);
                //cout << findroot << endl;
                //cout << massmin << "\t" << massmid << "\t" << massmax << endl;
                //cout << CoshEffMass(stod(row[0]), massmin, bmean1/bmean2) << "\t" << CoshEffMass(stod(row[0]), massmid, bmean1/bmean2) << "\t" << CoshEffMass(stod(row[0]), massmax, bmean1/bmean2) << endl;
                if (findroot*coshEffMass(stod(row[0]), massmax, bmean1/bmean2)<0) {
                    massmin=massmid;
                }
                else{
                    massmax=massmid;
                }
            } while ((massmax-massmin)>rootprecision);
            bootsmean[t]=massmid;
            //std::cout << massmid << std::endl;
        }
        
        /*
        //use the bootsmean value to estimate the error
        error=0;mean=0;
        for (i=0; i<Npick; i++) {
            mean+=bootsmean[i];
        }
        mean/=(double)Npick;
        Savefile << mean << "\t";
        for (i=0; i<Npick; i++) {
            error+=(bootsmean[i]-mean)*(bootsmean[i]-mean);
        }
        error=sqrt(error/(Npick-1));
         */
        
        //Rank the value in array bootsmean
        for (i=0;i<Npick-1; i++) {
            for (j=0;j<Npick-i-1;j++) {
                if (bootsmean[j] > bootsmean[j + 1]) {
                    double temp = bootsmean[j];
                    bootsmean[j] = bootsmean[j + 1];
                    bootsmean[j + 1] = temp;
                }
            }
        }
        
        mean=bootsmean[meanmid];
        error=(bootsmean[meanhigh16]-bootsmean[meanlow16])/2.0;
        
        std::cout << mean << "(+" << bootsmean[meanhigh16]-mean << ")(-" << mean-bootsmean[meanlow16] << ")" << std::endl;
        Savefile << mean << "\t" << bootsmean[meanhigh16]-mean << "\t" << mean-bootsmean[meanlow16] << std::endl;
        row=rownext;
    }
    
    infile.close();
    Savefile.close();
    std::cout << "***********    Work Is Done!    ***********" << std::endl;
    return 0;
}

