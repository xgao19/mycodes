//
//  main.cpp
//  csv_mean
//
//  Created by gaoxiang on 3/15/19.
//  Copyright © 2019 Xiang Gao. All rights reserved.
//
//  ./run filename y/n(if caculate autocorrelation time)

#include <iterator>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <typeinfo>

#define TMAX 500

class CSVRow
{
public:
    std::string const& operator[](std::size_t index) const
    {
        return m_data[index];
    }
    std::size_t size() const
    {
        return m_data.size();
    }
    void readNextRow(std::istream& str)
    {
        std::string         line;
        std::getline(str, line);
        
        std::stringstream   lineStream(line);
        std::string         cell;
        
        m_data.clear();
        while(std::getline(lineStream, cell, ','))
        {
            m_data.push_back(cell);
        }
        // This checks for a trailing comma with no data after it.
        if (!lineStream && cell.empty())
        {
            // If there was a trailing comma then add an empty element.
            m_data.push_back("");
        }
    }
private:
    std::vector<std::string>    m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}

void errorcalc(std::ofstream &t_Savefile, double * t_data, int t_ndata, int t_autocorr, int t_covar);
void covariance(std::ofstream &t_Savefile, double ** t_dataset, int t_nrow, int t_ncol);

int main(int argc,char * argv[]){
    
    std::cout << "\n**********./run filename t/nt c/nc**********" << std::endl;
    std::cout << "-- t/nt: if caculate autocorrelation time" << std::endl;
    std::cout << "-- c/nc: if caculate covariance" << std::endl;
    if (argc!=4) {
        std::cout << "-------------------Bad input format-------------------" << std::endl;
        return 0;
    }
    
    // Open the input file
    std::ifstream infile;
    infile.open(argv[1]);
    if(!infile) std::cout << "Open file " << argv[1] << " failed!" << std::endl;
    
    // judge if caculate the autocorrelation time
    int autocorr;
    if (!strncmp(argv[2],"t",1)) {
        autocorr=1;
    }
    else {autocorr=0;}
    
    // judge if caculate the covariance
    int covar;
    if (!strncmp(argv[3],"c",1)) {
        covar=1;
    }
    else {covar=0;}
    
    // Open a output file for mean value and error
    std::stringstream stream;
    stream << argv[1] << "_mean.txt";
    std::string filename;
    stream >> filename;
    std::ofstream Savefile(filename);
    Savefile.precision(12);
    
    // Read the data
    int ndata,tcount=0;
    double **dataset=new double*[TMAX];
    CSVRow row;
    infile >> row;//To remove the first describe line.
    while(infile >> row)
    {
        tcount++;
        Savefile << row[0] << "\t";
        ndata=row.size()-1;
        dataset[tcount-1]=new double[ndata];
        for (int i=0; i<ndata; i++) {
            dataset[tcount-1][i]=stod(row[i+1]);
        }
        double *data=dataset[tcount-1];
        errorcalc(Savefile, data, ndata, autocorr,covar);
    }
    std::cout << "\n";
    
    if (covar) {
        // Open a output file for Covariance
        std::stringstream streamCov;
        streamCov << argv[1] << "_Covariance.txt";
        std::string filenameCov;
        streamCov >> filenameCov;
        std::ofstream SavefileCov(filenameCov);
        SavefileCov.precision(12);
        
        covariance(SavefileCov, dataset, tcount, ndata);
        SavefileCov.close();
    }
    
    infile.close();
    Savefile.close();
    std::cout << "******************    Done!    ******************" << std::endl;
    return 0;
}

// calculate and print average, errors and autocorrelation time
// calculate integrated tint = 0.5 + sum_{t=1}^N C(t)
// where N-sum is stopped when N >= TINTSTOP*tint
// C(t) = < (d[i]-<d>) (d[i-t] - <d>)> / <(d-<d>)^2>
#define TINTSTOP 6
void errorcalc(std::ofstream &t_Savefile, double * t_data, int t_ndata, int t_autocorr, int t_covar){
    int i;
    double m_mean=0, m_sigma=0;
    int m_t;                               // autocorrelation lag
    double m_tint=0;
    
    for (i=0; i<t_ndata; i++) { m_mean+=t_data[i]; }
    m_mean /= t_ndata;
    for (i=0; i<t_ndata; i++) { m_sigma+=(t_data[i]-m_mean)*(t_data[i]-m_mean); }
    m_sigma /= t_ndata;
    
    // start loop over lags
    if (t_autocorr) {
        m_tint=0.5;
        for (m_t=1; m_t<TINTSTOP*m_tint&&m_t<t_ndata/2; m_t++) {
            double m_ave1=0, m_ave2=0;     // local averages
            double m_fi=0;
            int m_nt=t_ndata-m_t;          // number of measurement
            
            for (int j=0; j<m_nt; j++) {
                m_fi+=t_data[j]*t_data[j+m_t];
                m_ave1+=t_data[j];
                m_ave2+=t_data[j+m_t];
            }
            m_fi=(m_fi/m_nt-(m_ave1/m_nt)*(m_ave2/m_nt))/m_sigma;
            m_tint+=m_fi;
            //m_tint+=m_fi*m_nt/t_ndata;
        }
        if (m_t>=t_ndata/2) { std::cout << " ** correlation > N/2\n" << std::endl; }
    }
    
    if (t_autocorr) {
        t_Savefile << m_mean << "\t"  << fabs(m_tint) << std::endl;
        //t_Savefile << m_mean << "\t"  << fabs(m_tint) << "\t" << sqrt(2.0*fabs(m_tint)*m_sigma/(t_ndata-1)) << std::endl;
    }
    else{
        t_Savefile << m_mean << "\t" << sqrt(m_sigma/(t_ndata-1)) << std::endl;
        //t_Savefile << m_mean << "\t" << sqrt(m_sigma) << std::endl;
    }
}

// caculate the covariance of the dataset
// the output format is
// ***************************************************************
// **  Cov(0,0)/(ndata-1)                                       **
// **  Cov(1,0)/(ndata-1) Cov(1,1)/(ndata-1)                    **
// **  Cov(2,0)/(ndata-1) Cov(2,1)/(ndata-1) Cov(2,2)/(ndata-1) **
// **  ...                                                      **
// ***************************************************************
void covariance(std::ofstream &t_Savefile, double ** t_dataset, int t_nrow, int t_ncol){
    int i,j,k;
    double m_ave[t_nrow],m_cov;
    for (i=0; i<t_nrow; i++) {
        m_ave[i]=0;
        for (j=0; j<t_ncol; j++) {
            m_ave[i]+=t_dataset[i][j];
        }
        m_ave[i]/=(double)t_ncol;
        std::cout << m_ave[i] << std::endl;
    }
    t_Savefile.precision(12);
    for (i=0; i<t_nrow; i++) {
        for (j=0; j<t_nrow; j++) {
            m_cov=0;
            for (k=0; k<t_ncol; k++) {
                m_cov+=(t_dataset[i][k]-m_ave[i])*(t_dataset[j][k]-m_ave[j]);
            }
            m_cov/=(double)t_ncol;
            //t_Savefile << sqrt(m_cov/(t_ncol-1.0)) << "\t";
            t_Savefile << m_cov/(t_ncol-1.0) << "\t";
        }
        t_Savefile << "\n";
    }
}
