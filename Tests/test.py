#!/usr/bin/env python3
import time
import pandas as pd
import numpy as np
from numba import jit
from scipy.optimize import curve_fit

def pdfs_model_Aab(x, n, Aab):
    #print(ab)
    return (x**n)*Aab[0]*(x**Aab[1])*((1-x)**Aab[2])


def fit(Aab):
    res = 