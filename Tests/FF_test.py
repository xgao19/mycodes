#!/usr/bin/env python3
import numpy as np
import matplotlib
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt

heavycolors = ['Crimson','SteelBlue', 'DarkOrange', 'green','purple','SlateGray', 'Magenta','black','brown','Blue','OrangeRed','DarkGreen','Gold','DimGray','Sienna','Olive','PaleVioletRed','DeepSkyBlue','Red','SpringGreen']
dot_fmt = ['s','o','D','^','h','*']

fmGeV = 5.0676896

def Ep(a,Lt,Ls,mass,px,qx,qy,qz):
    px = (px+qx)*np.pi/(Ls/2)/a/fmGeV
    py = qy*np.pi/(Ls/2)/a/fmGeV
    pz = qz*np.pi/(Ls/2)/a/fmGeV
    return np.sqrt(mass**2+px**2+py**2+pz**2)
def dE(a,Lt,Ls,mass,px,qx,qy,qz):
    return Ep(a,Lt,Ls,mass,px,qx,qy,qz) - Ep(a,Lt,Ls,mass,px,0,0,0)
def aveE(a,Lt,Ls,mass,px,qx,qy,qz):
    return (Ep(a,Lt,Ls,mass,px,qx,qy,qz) + Ep(a,Lt,Ls,mass,px,0,0,0))/2
def aveP(a,Lt,Ls,mass,px,qx,qy,qz):
    return (Ep(a,Lt,Ls,0,px,qx,qy,qz) + Ep(a,Lt,Ls,0,px,0,0,0))/2
def dEoverE(a,Lt,Ls,mass,px,qx,qy,qz):
    return dE(a,Lt,Ls,mass,px,qx,qy,qz)/aveE(a,Lt,Ls,mass,px,qx,qy,qz)
def sknewness(a,Lt,Ls,mass,px,qx,qy,qz):
    return dE(a,Lt,Ls,mass,px,qx,qy,qz)/(2*aveE(a,Lt,Ls,mass,px,qx,qy,qz)+2*px)
def poly(x,a,b):
    return a*x*x + b
#print(sknewness(0.076,64,48,0.14,1,1,1,1))


fig = plt.figure(figsize=(5,3.2))
ax = fig.add_axes([0.18, 0.15, 0.78, 0.78])

data_a076 = []
data_a06 = []
data_a04 = []

a = 0.076 #fm
Lt = 64
Ls = 64
mass = 0.14
data_format = '/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_gpd/FormFactors/g8_qx0/2-rbm/results/ff_rbmMradius_g8_c3ptfit_tmin4-5_nst2_nsk3_px*-*_qxqyqz.txt'
for ff in range(1, 5):
    data = np.loadtxt(data_format.replace('*',str(ff)))

    x = np.array([data[i][4] for i in range(1,len(data))])
    y = [data[i][5] for i in range(1,len(data))]
    yerr = [data[i][-1] for i in range(1,len(data))]

    Epin = np.array([Ep(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    Epave = np.array([aveE(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    Pave = np.array([aveP(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    EpDiff = np.array([dE(a,Lt,Ls,mass,data[i][0],data[i][1],data[i][2],data[i][3]) for i in range(1,len(data))])
    sknew = np.array([sknewness(a,Lt,Ls,mass,data[i][0],data[i][1],data[i][2],data[i][3]) for i in range(1,len(data))])
    #x = (x*a*fmGeV)**2/(Epin**2)
    #x = (x*a*fmGeV)**2/(Epave**2)
    #x = x**2*(a*fmGeV)**2*(1/Epave)**2
    #x = (EpDiff)**2*np.exp(-Ls*a*fmGeV*mass)/(mass**2)
    x = (EpDiff*a*fmGeV)**2
    #x = (x*(a*fmGeV)**2)*sknew**2
    #x = a**2*(x/(Pave**2))
    data_a076 += [[x,y,yerr]]
ax.errorbar(data_a076[0][0],data_a076[0][1],data_a076[0][2], fmt=dot_fmt[0],color=heavycolors[0], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.076,px1', elinewidth=0.8, capthick=0.5)
ax.errorbar(data_a076[1][0],data_a076[1][1],data_a076[1][2], fmt=dot_fmt[1],color=heavycolors[0], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.076,px2', elinewidth=0.8, capthick=0.5)
ax.errorbar(data_a076[2][0]*(0.076*fmGeV)**2,data_a076[2][1],data_a076[2][2], fmt=dot_fmt[2],color=heavycolors[0], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.076,px3', elinewidth=0.8, capthick=0.5)
#ax.errorbar(data_a076[3][0]*(0.076*fmGeV)**2,data_a076[3][1],data_a076[3][2], fmt=dot_fmt[3],color=heavycolors[0], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.076,px4', elinewidth=0.8, capthick=0.5)


a = 0.06 #fm
Lt = 64
Ls = 48
mass = 0.3
data_format = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_48c64_data/gpd_analysis/FormFactors/g8_qx0/2-rbm/results/ff_rbmMradius_g8_c3ptfit_tmin4-6_nst2_nsk3_px*-*_qxqyqz_3ts.txt'
for ff in range(0, 4):
    data = np.loadtxt(data_format.replace('*',str(ff)))
    x = np.array([data[i][4] for i in range(1,len(data))])
    y = [data[i][5] for i in range(1,len(data))]
    yerr = [data[i][-1] for i in range(1,len(data))]

    Epin = np.array([Ep(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    Epave = np.array([aveE(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    Pave = np.array([aveP(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    EpDiff = np.array([dE(a,Lt,Ls,mass,data[i][0],data[i][1],data[i][2],data[i][3]) for i in range(1,len(data))])
    sknew = np.array([sknewness(a,Lt,Ls,mass,data[i][0],data[i][1],data[i][2],data[i][3]) for i in range(1,len(data))])
    #x =(x*a*fmGeV)**2/(Epin**2)
    #x = (x*a*fmGeV)**2/(Epave**2)
    #x = x**2*(a*fmGeV)**2*(1/Epave)**2
    #x = (EpDiff)**2*np.exp(-Ls*a*fmGeV*mass)/(mass**2)
    x = (EpDiff*a*fmGeV)**2
    #x = (x*(a*fmGeV)**2)*sknew**2
    #x = a**2*(x/(Pave**2))
    data_a06 += [[x,y,yerr]]
ax.errorbar(data_a06[0][0],data_a06[0][1],data_a06[0][2], fmt=dot_fmt[0],color=heavycolors[1], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.06,px0', elinewidth=0.8, capthick=0.5)
ax.errorbar(data_a06[1][0],data_a06[1][1],data_a06[1][2], fmt=dot_fmt[1],color=heavycolors[1], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.06,px1', elinewidth=0.8, capthick=0.5)
ax.errorbar(data_a06[2][0],data_a06[2][1],data_a06[2][2], fmt=dot_fmt[2],color=heavycolors[1], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.06,px2', elinewidth=0.8, capthick=0.5)
#ax.errorbar(data_a06[3][0]*(0.06*fmGeV)**2,data_a06[3][1],data_a06[3][2], fmt=dot_fmt[3],color=heavycolors[1], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.06,px3', elinewidth=0.8, capthick=0.5)


a = 0.04 #fm
Lt = 64
Ls = 64
mass = 0.3
data_format = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_64c64_data/gpd_analysis/FormFactors/g8_qx0/2-rbm/results/ff_rbmMradius_g8_c3ptfit_tmin6-8_nst2_nsk4_px*-*_qxqyqz_3ts.txt'
for ff in range(1, 5):
    data = np.loadtxt(data_format.replace('*',str(ff)))
    x = np.array([data[i][4] for i in range(1,len(data))])
    y = [data[i][5] for i in range(1,len(data))]
    yerr = [data[i][-1] for i in range(1,len(data))]

    Epin = np.array([Ep(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    Epave = np.array([aveE(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    Pave = np.array([aveP(a,Lt,Ls,mass,data[i][0],0,0,0) for i in range(1,len(data))])
    EpDiff = np.array([dE(a,Lt,Ls,mass,data[i][0],data[i][1],data[i][2],data[i][3]) for i in range(1,len(data))])
    sknew = np.array([sknewness(a,Lt,Ls,mass,data[i][0],data[i][1],data[i][2],data[i][3]) for i in range(1,len(data))])
    #x = (x*a*fmGeV)**2/(Epin**2)
    #x = (x*a*fmGeV)**2/(Epave**2)
    #x = x**2*(a*fmGeV)**2*(1/Epave)**2
    #x = (EpDiff)**2*np.exp(-Ls*a*fmGeV*mass)/(mass**2)
    x = (EpDiff*a*fmGeV)**2
    #x = (x*(a*fmGeV)**2)*sknew**2
    #x = a**2*(x/(Pave**2))
    data_a04 += [[x,y,yerr]]
ax.errorbar(data_a04[0][0],data_a04[0][1],data_a04[0][2], fmt=dot_fmt[0],color=heavycolors[2], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.04,px1', elinewidth=0.8, capthick=0.5)
ax.errorbar(data_a04[1][0],data_a04[1][1],data_a04[1][2], fmt=dot_fmt[1],color=heavycolors[2], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.04,px2', elinewidth=0.8, capthick=0.5)
#ax.errorbar(data_a04[2][0]*(0.04*fmGeV)**2,data_a04[2][1],data_a04[2][2], fmt=dot_fmt[2],color=heavycolors[2], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.04,px3', elinewidth=0.8, capthick=0.5)
#ax.errorbar(data_a04[3][0]*(0.04*fmGeV)**2,data_a04[3][1],data_a04[3][2], fmt=dot_fmt[3],color=heavycolors[2], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='0.04,px4', elinewidth=0.8, capthick=0.5)

data_format = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_48c64_data/gpd_analysis_breit/FormFactors/g8_qx0/2-rbm/results/ff_breit_rbmMradius_g8_c3ptfit_nst2_nsk3_px2-2_qxqyqz.txt'
data = np.loadtxt(data_format)
x = np.array([0 for i in range(0,len(data))])
y = [data[i][5] for i in range(0,len(data))]
yerr = [data[i][-1] for i in range(0,len(data))]
data_breit = [[x,y,yerr]]
ax.errorbar(data_breit[0][0],data_breit[0][1],data_breit[0][2], fmt=dot_fmt[0],color=heavycolors[3], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label='Breit', elinewidth=0.8, capthick=0.5)

print(np.shape(data_a076),np.shape(data_a06),np.shape(data_a04))
plt.legend(loc=0,frameon=False,prop={'size': 8},ncol=2,handletextpad=0.2,columnspacing=0.2)
plt.xlabel('$a^2\Delta E^2$',fontsize=15)
plt.ylabel('$\langle r^2_{eff} \\rangle(Q^2)$',fontsize=15)
plt.show()
