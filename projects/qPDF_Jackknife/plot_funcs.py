#!/usr/bin/env python3
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from modelfuncs import *
import os.path
from pylab import *
#import rITD_fit
import crITD_fit
import modelfuncs
#import Complex_rITD_fit
#from Complex_rITD_fit import *

matplotlib.rcParams['hatch.linewidth'] = 3

lightcolors = ['tomato','skyblue','salmon','silver', 'sandybrown',  'plum','paleturquoise', 'palegreen','tomato']
heavycolors = ['Crimson','SteelBlue', 'DarkOrange', 'purple','green','SlateGray', 'Magenta','black','brown','Blue','OrangeRed','DarkGreen','Gold','DimGray','Sienna','Olive','PaleVioletRed','DeepSkyBlue','Red','SpringGreen']
heavycolors2 = ['','','Crimson', 'SteelBlue','DarkOrange', 'purple','green','SlateGray', 'Magenta','black','brown','Blue','OrangeRed','DarkGreen','Gold','DimGray','Sienna','Olive','PaleVioletRed','DeepSkyBlue','Red','SpringGreen']
hatchlist = ['XX','||','','','X','||','///','--','++','**','ooo']

def make_func_v0(func, params):

    def func_v0(x):
        return func(x, *params)

    return func_v0

def make_func_v1in2(func, params):

    def func_v1in2(x):
        return func((params[0], x), *(params[1:]))

    return func_v1in2

def make_func_v2in2(func, params):

    def func_v2in2(x):
        return func((x, params[0]), *(params[1:]))

    return func_v2in2

'''
This code plot a 2-D picture with x-y data with error bar or not.
@1 Your dataset can contain many subdataset to plot in one picture.
@2 Your subdataset should in the format 'x  y  yerror'
or in the format 'x  y  yerror- yerror+'
@3 You have a datalabel option to label your plot and subdata.
Please note: datalabel[0] is the name of this plot, while
datalabel[i] with i >= 1 is name of the subdata.
@4 You have a xy label option accapt an array to label the axes.
@5 You have a yrange option to decide if plot in a specific y range.
@6 You can choose to plot some curves use the 'func' option.
@7 You can choose to plot a band around the curve, this option should contain 2 times funcs of 'func' option.
@8 You can choose to plot in a log scale by the commander logscale==True.
'''

def plot_err(dataset, datalabel=None, xylabel=None,xlimit=[],ylimit=[], func_list=[],func_labels=[],line_list=[], line_labels=[],band_data=[],band_labels=[],folder = '/',xlogscale=False,ylogscale=False, text=[], factor_yscale = [1],figsize=[],figaxes=[]):

    plt.rcParams['figure.dpi'] = 300
    if len(figsize) == 0:
        fig = plt.figure(figsize=(5,3.2))
    else:
        fig = plt.figure(figsize=(figsize[0],figsize[1]))
    if len(figaxes) == 0:
        ax = fig.add_axes([0.18, 0.15, 0.78, 0.78])
    else:
        ax = fig.add_axes(figaxes)
    x = []
    #ax.hlines(0, -0.5, 1,color="black", linewidth=0.7,linestyle=':')
    #ax.vlines(0, -1, 5,color="black", linewidth=0.7,linestyle=':')

    # this part plot the data points
    if len(dataset) != 0:
        i=0
        factor = -1
    for subdataset in dataset:

        subdataset = np.transpose(subdataset)
        print(subdataset[0])

        mv = (subdataset[0][-1] - subdataset[0][1])/5000
        #mv = 0.0
        subdataset[0] = subdataset[0] + mv*factor
        ncol = len(subdataset)
        #datasetcount = [0,0,0,0,1,1,1,1,2,2,2,2,2,2,3,3,3,4,4,4]

        i += 1
        xnm = [1 for j in range(0,len(dataset))]
        #xnm = [2*(j+2)*3.14159/(64*0.076) for j in range(0,len(dataset))]
        #print('?????',xnm)
        dot_fmt = ['s','o','D','^','h','*']
        if len(factor_yscale) == 1:
            nm = [factor_yscale[0] for j in range(0,len(dataset))]
        else:
            nm = [factor_yscale[j] for j in range(0,len(dataset))]
        #nm_factor = [1/(0.1944*5.0675896), 1/(0.1141*5.0675896)]
        #subdataset[2] = [subdataset[0][j]/subdataset[1][j]**2*subdataset[2][j]/nm_factor[i-1] for j in range(0, len(subdataset[0]))]
        #subdataset[1] = [subdataset[0][j]/subdataset[1][j]/nm_factor[i-1] for j in range(0, len(subdataset[0]))]
        #print(i,subdataset[0],subdataset[1])
        try:
            sublabel = datalabel[i]
        except:
            sublabel = None
        # this part to plot error bar points
        if ncol == 2:
            ax.errorbar(subdataset[0],subdataset[1], fmt='o', color=heavycolors[i-1], markersize=5, capsize=5, label=sublabel, markeredgewidth=0.8, elinewidth=0.8, capthick=0.8)
        elif ncol ==3:
            print(subdataset[1])
            #print(subdataset[1]*nm[i-1])
            #ax.errorbar(subdataset[0]*xnm[i-1],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1],fmt=dot_fmt[0],color=heavycolors[i-1], markerfacecolor='none', markersize=3, capsize=0, markeredgewidth=0.6, label=sublabel, elinewidth=1, capthick=0.5)
            ax.errorbar(subdataset[0],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1], fmt=dot_fmt[0],color=heavycolors[int((i-1)/1)], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label=sublabel, elinewidth=1, capthick=0.6)
            #ax.errorbar(subdataset[0]*xnm[i-1],subdataset[1]/nm[i-1],subdataset[2]/nm[i-1], fmt=dot_fmt[1], color=heavycolors[i-1], markersize=3, capsize=0, markeredgewidth=0.5, label=sublabel, elinewidth=1.2, capthick=0.5)
            #ax.errorbar(subdataset[0]*xnm[i-1],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1],color=heavycolors[(i-1)%300], markerfacecolor='none', fmt=dot_fmt[int((i-1)/30)], markersize=7, capsize=4, markeredgewidth=1, label=sublabel, elinewidth=1, capthick=0.6)
        elif ncol ==4:
            #ax.errorbar(subdataset[0],subdataset[1],[subdataset[2],subdataset[3]], fmt='o', markerfacecolor='none', markersize=5, capsize=5, label=sublabel, markeredgewidth=0.8, elinewidth=0.8, capthick=0.8)
            ax.errorbar(subdataset[0],subdataset[1],[subdataset[2],subdataset[3]], fmt='.', color=heavycolors[i-1], markersize=4, capsize=0, markeredgewidth=0.5, label=sublabel, elinewidth=1, capthick=0.5)
        else :
            print('Bad plot data format!!!\n')
            print('Your subdata in dataset should in the format *x  y  yerror*\n')
        factor +=1

    # this part to plot curves
    if len(func_list) != 0:
        #print(func_list,i)
        it_c=12
        linestyle = ['--','--','-.','.','--','--','--']
        print(func_labels)
        if len(func_list[0]) <= 100:
            for i in range(0, len(func_list)):
                for j in range(0, len(func_list[i])):
                    #x = np.arange(0.01,1,0.001)
                    x = np.arange(dataset[i][0][0],dataset[i][-1][0]*1,0.0001)
                    y = func_list[i][j](x)
                    #print(func_list[i][j](32))
                    #print(x,y)
                    #plt.plot(x, y, linewidth=1, color = heavycolors[i], label=func_labels[i],linestyle=linestyle)
                    plt.plot(x, y, linewidth=1, color = heavycolors[int(i)], label=func_labels[i],linestyle=linestyle[int(i/10)])
        else:
            xlist = [dataset[0][i][0] for i in range(0, len(dataset[0]))]
            #xmin = 0.001
            xmin = min(xlist)
            xmax = max(xlist)
            xmax += 0.1*abs(xmax-xmin)
            x = np.arange(xmin,xmax,0.001)
            for ifunc in range(0, len(func_list)):
                ymin = []
                ymax = []
                nsamp = len(func_list[ifunc])
                low16 = int(np.ceil(0.16*nsamp))
                high16 = int(np.floor(0.84*nsamp))
                for ix in x:
                    y = sorted([func_list[ifunc][isamp](ix) for isamp in range(0, nsamp)])
                    ymin += [y[low16]]
                    ymax += [y[high16]]
                plt.fill_between(x, ymin, ymax, color = heavycolors[ifunc%7],alpha=0.35, label=func_labels[ifunc])
    #ax.legend(loc=0,prop={'size': 10})

    # this part to plot lines with error
    linestyle = ['-','-','-.','.','--','--','--']
    if len(line_list) != 0:
        if len(x) == 0:
            #x = np.arange(0,1.3,0.01)
            x = np.arange(dataset[-1][0][0],dataset[-1][-1][0],0.01)
            #x = np.arange(xlimit[0], xlimit[1],0.01)
            #print(dataset[-1][0][0],dataset[-1][-1][0])
        for i in range(0, len(line_list)):
            y = [line_list[i][0] for j in x]
            plt.plot(x, y, linewidth=1., color = heavycolors[i],linestyle=linestyle[1], label=line_labels[i])
            #y1 = [line_list[i][0] - line_list[i][1] for j in x]
            #y2 = [line_list[i][0] + line_list[i][1] for j in x]
            #plt.fill_between(x, y1, y2, facecolor=heavycolors[int(i)],edgecolor='none',alpha=0.3, label=line_labels[i])
            y1 = [line_list[i][0] - (line_list[i][2]+line_list[i][1])/2 for j in x]
            y2 = [line_list[i][0] + (line_list[i][2]+line_list[i][1])/2 for j in x]
            #plt.fill_between(x, y1, y2, facecolor=heavycolors[i],edgecolor='none',alpha=0.3, label=line_labels[i])
    
    # this part to plot the band data 
    if len(band_data) != 0:
        xnm = [1 for j in range(0,len(band_data))]
        #xnm = [5.0676896*1.8,5.0676896*2.3,5.0676896*1.845,5.0676896*2.31]
        it=2
        linestyle = ['-','--','-.',':','--','--','-.','-.','-.','-.']
        for i in range(0, len(band_data)):
            ymax = []
            ymin = []
            ymid = []
            print(np.shape(band_data))
            x = [band_data[i][0][j][0]*xnm[int(i)] for j in range(0, int(len(band_data[i][0])))] 
            if len(band_data[i]) != 1:
                for ix in range(0, len(x)):
                    iymax = max([band_data[i][j][ix][1] for j in range(0, len(band_data[i]))])
                    iymin = min([band_data[i][j][ix][1] for j in range(0, len(band_data[i]))])
                    #if ix == len(x)-1:
                    #    print('Bootstrap errors.')
                    #ix_y = sorted([band_data[i][j][ix][1] for j in range(0, len(band_data[i]))])
                    #iymax = ix_y[int(np.ceil(len(ix_y)*0.84))]
                    #iymin = ix_y[int(np.floor(len(ix_y)*0.16))]
                    #iymid = ix_y[int(np.floor(len(ix_y)*0.5))]
                    ymax += [iymax]
                    ymin += [iymin]
                    #ymid += [iymid]
                print(x[20],ymin[20],ymax[20])
                plt.fill_between(x,ymin,ymax,facecolor=heavycolors[int((i)/1)],alpha=0.3,edgecolor=heavycolors[int((i)/1)],label=band_labels[i],linewidth=0.5)
                #plt.plot(x, ymid, linewidth=1, color = heavycolors[int(i/3)],linestyle=linestyle[int(i/3)])
            else:
                y = [band_data[i][0][j][1]*xnm[int(i)] for j in range(0, int(len(band_data[i][0])))] 
                plt.plot(x, y, linewidth=1, color = heavycolors[i%int(len(band_data)/3)], label=band_labels[i],linestyle=linestyle[int(i/int(len(band_data)/3))-1])
            fillcolor = 0
            if len(band_data[i]) != 1 and i<1000:
                fillcolor = heavycolors2[i%4]
                hatchcolor= heavycolors[i%4]
                hatchstyle=hatchlist[i%4]
                alpha=0.2+(i%4)*0.2
                if i == 40:
                    alpha = 0.3
            elif len(band_data[i]) != 1:
                fillcolor = "none"
                hatchcolor=lightcolors[i+1]
                hatchstyle=hatchlist[1]
                alpha=1
                print(hatchstyle)
            if i == 800:
                x = x[200:]
                ymin = ymin[200:]
                ymax = ymax[200:]
            #print(i,fillcolor)
            #if len(band_data[i]) > 1:
                #plt.fill_between(x,ymin,ymax,color=heavycolors[(i-6)*3],alpha=0.3,linewidth=0)
                #plt.fill_between(x,ymin,ymax,facecolor=fillcolor,edgecolor=hatchcolor,alpha=alpha,label=band_labels[i],hatch=hatchstyle,linewidth=0.)

    ax.legend(loc=0,frameon=False,prop={'size': 8},ncol=3,handletextpad=0.2,columnspacing=0.2)
    #ax.legend(loc=0,frameon=False,prop={'size': 8})
    if len(text) != 0:
        print(text)
        for text_row in text:
            ax.text(text_row[1], text_row[2], text_row[0], fontsize=10)

    try:
        plttitle = datalabel[0]
    except:
        plttitle = 'Datapoint' + int(str(np.random.uniform(0,1000)))
    #plt.title(plttitle, fontsize=10)
    #plt.show()
    ficturename = folder + plttitle + '.pdf'
    #plt.figure(figsize=(10, 8)) 
    if xlogscale == True:
        ax.set_xscale('log')
    if ylogscale == True:
        ax.set_yscale('log')
    plt.xlabel(xylabel[0],fontsize=15)
    plt.ylabel(xylabel[1],fontsize=15)
    if len(xlimit) != 0:
        plt.xlim(xlimit)
        my_x_ticks = np.arange(xlimit[0], xlimit[1], 0.1)
        #plt.xticks(my_x_ticks)
    plt.ylim(ylimit)
    #my_y_ticks = np.arange(ylimit[0], ylimit[1], (ylimit[1]-ylimit[0])/5)
    #plt.yticks(my_y_ticks)
    tick_params(direction='in')
    #plt.show()
    plt.savefig(ficturename)

if __name__ == "__main__":

    if sys.argv[1] =='plot':
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()

        line = commands[0].split()
        #print(line)
        datalabel = [line[0]]
        xylabel = [line[1].replace('~',' '), line[2].replace('~',' ')]
        xlogscale = False
        ylogscale = False
        xlimit = []

        line = commands[1].split()
        savefolder = line[0]
        pltdata = []
        line_list = []
        line_labels = []
        func_list = []
        func_labels = []
        band_data = []
        band_label = []
        text = []
        factor_yscale = [1]
        figsize = []
        figaxes = []

        for index in range(2,len(commands)):

                line = commands[index].split()

                if 'ynm' in line[0]:
                        factor_yscale = [float(line[i]) for i in range(1, len(line))]
                elif 'figsize' in line[0]:
                    figsize = [float(line[1]), float(line[2])]
                elif 'figaxes' in line[0]:
                    figaxes = [float(line[1]), float(line[2]), float(line[3]), float(line[4])]
                elif 'xlimit' in line[0]:
                    xlimit = [float(line[1]), float(line[2])]
                elif 'ylimit' in line[0]:
                    ylimit = [float(line[1]), float(line[2])]
                elif 'xscale' in line[0]:
                    if 'log' in line[1]:
                        xlogscale = True
                elif 'yscale' in line[0]:
                    if 'log' in line[1]:
                        ylogscale = True
                elif 'text' == line[0]:
                    text_row = ''
                    for itext in range(3, len(line)):
                        text_row += line[itext]
                        text_row += ' '
                    text += [[text_row, float(line[1]), float(line[2])]]

                elif  'func' in line[0]:

                    i_func_list = []

                    if 'file' in line[0]:
                        func_vfix = line[1]
                        func_line = line[2]
                        func_name = line[3]
                        func_data = loadtxt(line[4])
                        func_param_loc = list(map(float, line[5:]))
                        func_plot = getattr(modelfuncs,func_name)
                        func_labels += [((line[0].replace('func','')).replace('file','')).replace(',',', ')]
                    else:
                        func_vfix = line[1]
                        func_name = line[2]
                        func_line = '0'
                        func_data = [list(map(float, line[3:]))]
                        #print('func_data',func_data)
                        #func_param_loc = func_data[0]
                        func_param_loc = [i for i in range(0, len(func_data[0]))]
                        #func_param_loc = [func_data[0][0]]+[i for i in range(1, len(func_data[0]))]
                        func_plot = getattr(modelfuncs,func_name)
                        func_labels += [line[0].replace('func','')]

                    if 'all' in func_line:
                        linemin = 0
                        linemax = len(func_data)
                    else:
                        linemin = int(func_line)
                        linemax = linemin+1

                    if '0' in func_vfix:
                        for i in range(linemin , linemax):
                            func_param = []
                            #func_param = [func_param_loc[0]]
                            print(func_param_loc)
                            for j in range(0, len(func_param_loc)):
                                #print(i,j,func_data)
                                func_param += [func_data[i][int(func_param_loc[j])]]
                            print('func_param',func_param)
                            i_func_list += [make_func_v0(func_plot, func_param)]

                    elif '1in2' in func_vfix:
                        for i in range(linemin , linemax):
                            func_param = [func_param_loc[0]]
                            for j in range(1, len(func_param_loc)):
                                func_param += [func_data[i][int(func_param_loc[j])]]
                            print('func_param',func_param)
                            i_func_list += [make_func_v1in2(func_plot, func_param)]

                    elif '2in2' in func_vfix:
                        for i in range(linemin , linemax):
                            func_param = [func_param_loc[0]]
                            for j in range(1, len(func_param_loc)):
                                func_param += [func_data[i][int(func_param_loc[j])]]
                            print(func_param)
                            i_func_list += [make_func_v2in2(func_plot, func_param)]

                    #print(shape(i_func_list))
                    func_list += [i_func_list]

                elif 'line' == line[0][-4:]:

                    linevalue = round(float(line[1]),4)
                    if len(line) > 3:
                        line_errn = float(line[-2])
                        line_errp = float(line[-1])
                        iline = [linevalue, line_errn, line_errp]
                        print(linevalue, line_errn, line_errp)
                        label_err = '(' + str(round(line_errn, 4)) + ')'
                    elif len(line) == 3:
                        line_errn = float(line[-1])
                        line_errp = float(line[-1])
                        iline = [linevalue, line_errn, line_errp]
                        print(linevalue, line_errn, line_errp)
                        #print('str',str(linevalue),str(line_errn),str(round(line_errn,4)))
                        label_err = '(' + str(round(line_errn, 4)) + ')'
                    else:
                        line_errp = 0
                        line_errn = 0
                        iline = [linevalue, line_errn, line_errp]
                        print(linevalue, line_errn, line_errp)
                        label_err = ''
                    line_list += [iline]
                    #ilabel = line[0].replace('line','') + str(round(linevalue,4)) + label_err
                    ilabel = (line[0].replace('line','')).replace(',',', ')
                    line_labels += [ilabel]

                elif 'range' in line[0]:

                    min_value = int(line[1])
                    max_value = int(line[2])
                    x_col = int(line[-2])
                    y_col = int(line[-1])
                    ilabel = line[0].replace('range','')
                    ilabel = ilabel.replace('~',' ')
                    ilabel = ilabel.replace(',',', ')
                    dataset = []
                    for i in range(min_value, max_value+1):
                        i_name = line[3].replace('#',str(i))
                        i_read = np.loadtxt(i_name)
                        #print(i_name)
                        i_data = [[i_read[j][x_col], i_read[j][y_col]] for j in range(0, len(i_read)) ]
                        dataset += [i_data]

                    band_data += [dataset]
                    band_label += [ilabel]
                    #print(band_data)
                    
                else:
                    if line[0] == 'none':
                        line[0] = ''
                    datalabel += [(line[0].replace(',',', ')).replace('~',' ')]
                    dataset = np.loadtxt(line[1])
                    subpltdata = []
                    datasetT = np.transpose(dataset)
                    for i in range(2,len(line)):
                        #print(line)
                        subpltdata += [datasetT[int(line[i])]]
                    pltdata += [np.transpose(subpltdata)]
        print(savefolder) 
        inputfile.close()   
        plot_err(pltdata,datalabel,xylabel,xlimit,ylimit,func_list,func_labels,line_list=line_list, \
            line_labels=line_labels,band_data=band_data,band_labels=band_label,folder = savefolder, \
                xlogscale=xlogscale,ylogscale=ylogscale,text=text, factor_yscale=factor_yscale, \
                    figsize=figsize,figaxes=figaxes)
