#!/usr/bin/env python3
import numpy as np
from scipy.optimize import least_squares


mean_file = '/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_gpd/c2ptfit_jackk/qx0/qx0qy0qz0/E1/results/c2pt_a076fm_SS_bxp20_px0_tmax31_E1_twostate.txt'
sample_file = '/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_gpd/c2ptfit_jackk/qx0/E0_for_paper/results/samples/c2pt_a076fm_SS_bxp20_px0_tmax31_000_jackksample_tmin#_onestate'

tmin_list = [i for i in range(4, 8)]

mean_data = np.loadtxt(mean_file)
mean_tmin = np.array([mean_data[i][0] for i in range(tmin_list[0]-1, tmin_list[-1])])
mean_center = np.array([mean_data[i][8] for i in range(tmin_list[0]-1, tmin_list[-1])])
mean_err = np.array([mean_data[i][9] for i in range(tmin_list[0]-1, tmin_list[-1])])

mean_E = np.average(mean_center)
print(mean_tmin)
#print(mean_E)
#print(mean_center)
#print(mean_err)

def Jackk_energy(E):
    res = mean_center - E
    return res/mean_err

Fit_E = least_squares(Jackk_energy, 0, method='lm')
E_chisq = sum(np.square(Fit_E.fun))/len(mean_center)
print(E_chisq,len(mean_center),Fit_E.x)
