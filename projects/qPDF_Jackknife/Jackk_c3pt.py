#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
import inspect
from scipy.optimize import curve_fit

from tools import *
import modelfuncs
from modelfuncs import *
from chisqfit_cov_jb import *
from makefuncs import *

#--------------------------------------------------------------#
# jackknife for special case: for every sample, first fit SSc2pt
# then use the E0 and E1 to fit the c3pt/c2pt with several gaven
# ts datas
# the general SP/SSc2data set should in the format as list:
# x0 conf1 conf2 conf3...
# x1 conf1 conf2 conf3...
# ...
# xN conf1 conf2 conf3...
# the general tsc2data and tsc3data should in the format as list:
# tsc2data.shape = (ts, conf), tsc3data.shape = (ts, tau, conf)
#--------------------------------------------------------------#

def jackk_c3ptfit(c2func,c3func,SSc2data,SSc2params,SSratioc3data,SSratioc2data,SSratioerr,c3fitparams,tslist,zlist,taumin=1,samplename=None,blocksize=1,**kwargs):

    ncfgs = len(SSc2data[0])-1
    nblock = int(ncfgs/blocksize)

    # mean value
    if c2func == None:
        SSc2popt = []
        print('@@@ No c2pt fit!\n')
    elif c2func.__name__ == 'aiccfit':
        SSc2popt,SSfail = c2func(SSc2data, SSc2params)   # the function return a 2d list
        aicclist = [SSc2popt[i][-2] for i in range(0, len(SSc2popt)-1)]
        model_aiccmin = 0
        for imodel in range(1, len(aicclist)):
            if aicclist[imodel] < aicclist[model_aiccmin]:
                model_aiccmin = imodel
        SSc2popt = SSc2popt[model_aiccmin]
        #print(SSc2params)
        if 'prior' in SSc2params[0][0]:
            SSc2params = [SSc2params[0]] + [SSc2params[model_aiccmin+1]]
        else:
            SSc2params = [SSc2params[model_aiccmin]]
        #print(SSc2params)
    else:
        SSc2popt = c2func(SSc2data, SSc2params)   # the function return a 2d list
        SSc2popt = SSc2popt[-1]

    c3fitparams = makec3pt_params(c3fitparams, SSc2popt)
    popt_mean = []
    for iz in range(0,len(zlist)):
        #print(SSratioc2data)
        popt_mean += [c3func(tslist,SSratioc3data[iz],SSratioc2data,SSratioerr[iz],c3fitparams,ratio='ratio',taumin=taumin, blocksize=blocksize)]
        #print(popt_mean)
    
    # jackknife loop
    jackk_err = np.zeros((len(zlist),len(popt_mean[0])))
    jackk_samples = [[] for i in range(0, len(zlist))]

    for i in range(0, nblock):

        print('\n jackknife block:',i+1,'/',nblock)
        sample_ratioc3 = d5slicedelete(SSratioc3data,[i*blocksize+1,(i+1)*blocksize+1],3)
        sample_ratioc2 = d5slicedelete(SSratioc2data,[i*blocksize+1,(i+1)*blocksize+1],1)
        sample_c2data = d5slicedelete(SSc2data,[i*blocksize+1,(i+1)*blocksize+1],1)

        if c2func == None:
            sample_SSc2popt = []
        elif c2func.__name__ == 'aiccfit':
            sample_SSc2popt,SSfail = c2func(sample_c2data, SSc2params)   # the function return a 2d list
            sample_SSc2popt = sample_SSc2popt[-1]
        else:
            sample_SSc2popt = c2func(sample_c2data, SSc2params)   # the function return a 2d list
            sample_SSc2popt = sample_SSc2popt[-1]
        c3fitparams = makec3pt_params(c3fitparams, sample_SSc2popt)

        for iz in range(0, len(zlist)):
            popt_sample = c3func(tslist,sample_ratioc3[iz],sample_ratioc2,SSratioerr[iz],c3fitparams,ratio='ratio',taumin=taumin, blocksize=blocksize)
            #print('sample popt:',popt_sample)
            for ip in range(0, len(popt_sample)):
                jackk_err[iz][ip] += (popt_sample[ip]-popt_mean[iz][ip]) * (popt_sample[ip]-popt_mean[iz][ip])
            jackk_samples[iz] += [popt_sample]


    for iz in range(0, len(popt_mean)):
        for ip in range(0, len(popt_mean[iz])):
            jackk_err[iz][ip] = np.sqrt(jackk_err[iz][ip]*(nblock-1)/nblock)

    # save samples
    if samplename != None:
        for iz in range(0, len(zlist)):
            z_sample_file = samplename + '_Z' + str(zlist[iz]) 
            np.savetxt(z_sample_file,jackk_samples[iz],fmt='%.6e')
    
    # combine the mean value and error
    #print(np.shape(jackk_err),np.shape(popt_mean))
    mean_err = []
    for iz in range(0, len(zlist)):

        iz_mean_err = [zlist[iz]]
        for ip in range(0, len(popt_mean[0])):
            iz_mean_err += [popt_mean[iz][ip]] + [jackk_err[iz][ip]]
        mean_err += [iz_mean_err]

    return mean_err
        
            
