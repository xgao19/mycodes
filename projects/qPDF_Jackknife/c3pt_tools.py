#!/usr/bin/env python3

import os
import csv
import numpy as np


##########################################################
######## ratio as function of tau-ts/2 at some z #########
##########################################################

# the c2data here is assumed to be formatted as(1D):
# ts conf1 conf2 ...
# the c3data here is assumed to be formatted as(2D):
# tau1 conf1 conf2...
# ...
# this function return a 2D list, format as:
# [z, ts, tau, ratio]
def c3c2radio(z,ts, c2data, c3data, c2mdf = None):

    ##c3/c2 ratio caculating ...
    if c2mdf == None:
        def c2mdffunc(x):
            return 0
    else:
        c2mdffunc = c2mdf

    tsratio = []
    c2data = c2data[1:]
    c3data = [raw[1:] for raw in c3data]
    if len(c3data) > ts:
        taumax = ts+1
    else:
        taumax = len(c3data)
    for i in range(1,taumax-1):
        #print(j,len(dtc3px))
        ratiomean = 0
        c3mean = np.average(c3data[i]).real
        c2mean = np.average(c2data).real - c2mdffunc(ts)
        #print(ts, np.average(dtc2px).real, c2mdf(ts))
        ratio = c3mean/c2mean
        tsratio += [[z,ts,i,ratio]]
    return tsratio