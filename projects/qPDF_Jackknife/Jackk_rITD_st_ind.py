#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
from tools import *
import inspect
from scipy.optimize import curve_fit
from modelfuncs import*
from statistics import*

'''
This code is to construct ratio scheme RG invariant ratio 
from Jackknife bare matrix elements.
'''

def jackkbm_collect(mean_format,col_mean,sample_format,col_sample,zlist,pxlist, qxyz=[]):
    
    # data collection
    bm_mean_data = []
    bm_samples_data = []
    for ipx in range(0,len(pxlist)):
        if len(qxyz) != 0:
            ipx_mean_format = mean_format.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            ipx_sample_format = sample_format.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            #if pxlist[ipx] == 5 and 'qx0qy2qz2' in ipx_mean_format:
            #    ipx_mean_format = ipx_mean_format.replace('4ts', '3ts')
            #    ipx_sample_format = ipx_sample_format.replace('4ts', '3ts')

        sampledata = (np.loadtxt((ipx_sample_format.replace('*',str(pxlist[ipx]))).replace('#', str(zlist[0]))))
        njackk = len(sampledata)

        ipx_mean_name_real = ipx_mean_format.replace('*', str(pxlist[ipx]))
        ipx_mean_data_real = np.loadtxt(ipx_mean_name_real)

        if 'real' in ipx_mean_name_real:
            ipx_mean_name_imag = ipx_mean_name_real.replace('real', 'imag')
            ipx_mean_data_imag = np.loadtxt(ipx_mean_name_imag)
            ipx_bm_mean_data = [complex(ipx_mean_data_real[iz][col_mean],ipx_mean_data_imag[iz][col_mean]) for iz in zlist]
        else:
            ipx_bm_mean_data = [ipx_mean_data_real[iz][col_mean] for iz in zlist]
        bm_mean_data += [ipx_bm_mean_data]
        #print(bm_mean_data)

        ipx_in_format = ipx_sample_format.replace('*', str(pxlist[ipx]))
        ipx_data = []

        for iz in range(0, len(zlist)):
            iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            jacksample_real = np.loadtxt(iz_ipx_in_name)
            njackk = len(jacksample_real)
            #print(jacksample_real)
            if 'real' in ipx_mean_name_real:
                jacksample_imag = np.loadtxt(iz_ipx_in_name.replace('real','imag'))
                try:
                    jacksample = [complex(jacksample_real[i][col_sample],jacksample_imag[i][col_sample]) for i in range(0, njackk)]
                except:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, njackk)]
            else:
                try:
                    jacksample = [jacksample_real[i][col_sample] for i in range(0, njackk)]
                except:
                    jacksample = [jacksample_real[i] for i in range(0, njackk)]
            ipx_data += [jacksample]
        bm_samples_data += [ipx_data]
    print('bare matrix mean shape:', np.shape(bm_mean_data),'\n')
    print('bare matrix samples shape:', np.shape(bm_samples_data),'\n')
    return bm_mean_data,bm_samples_data

def jackkbm_to_rITD(Ns,latsp,up_mean,up_col_mean,up_sample,up_col_sample, \
            down_mean,down_col_mean,down_sample,down_col_sample, \
                zlist, pxlist, outfolder, describe, qxyz):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    for iz in range(0, len(zlist)):
        if zlist[iz] == 0:
            z0 = iz
    
    # data collection
    bm_mean_norm, bm_samples_norm = jackkbm_collect(down_mean,down_col_mean,down_sample,down_col_sample,zlist,[0],[0,0,0])
    bm_mean_data, bm_samples_data = jackkbm_collect(up_mean,up_col_mean,up_sample,up_col_sample,zlist,pxlist,qxyz)

    rITD_samp_lenMax = 0
    for isamp in bm_samples_data:
        for izsamp in isamp:
            if len(izsamp) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(izsamp)
        
    # reduced ioffe time distribution
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_jackksamples'
    ritd_real = []
    ritd_imag = []
    
    for ipx in range(0, len(pxlist)):
        
        ipx_ritd_real = []
        ipx_ritd_imag = []
        
        for iz in range(0, len(zlist)):

            p0_cfg = len(bm_samples_norm[0][iz])
            px_cfg = len(bm_samples_data[ipx][iz])

            print('z=',zlist[iz],'p0 njackk:', p0_cfg, 'px', pxlist[ipx], 'njackk:', px_cfg)
            

            ipx_iz_ritd_real = []
            ipx_iz_ritd_imag = []
            jackksample_real = []
            jackksample_imag = []
            #print(bm_mean_data)
            jackkmean = bm_mean_data[ipx][iz]/bm_mean_data[ipx][0]/(bm_mean_norm[0][iz]/bm_mean_norm[0][0])
            #jackkmean = bm_mean_data[ipx][iz]/bm_mean_norm[0][iz]
            #print(bm_mean_norm[0][iz])
            #print(iz, bm_mean_data[ipx][iz], bm_mean_data[ipx][0], bm_mean_norm[0][iz], bm_mean_norm[0][0])
            jackkerr_real = 0
            jackkerr_imag = 0

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, rITD_samp_lenMax):

                if isamp < px_cfg:
                    isample_data = bm_samples_data[ipx][iz][isamp]/bm_samples_data[ipx][0][isamp]
                else:
                    isample_data = bm_mean_data[ipx][iz]/bm_mean_data[ipx][0]

                if isamp < p0_cfg:
                    isample_norm = bm_samples_norm[0][iz][isamp]/bm_samples_norm[0][0][isamp]
                else:
                    isample_norm = bm_mean_norm[0][iz]/bm_mean_norm[0][0]
                    
                isample = isample_data/isample_norm
                jackksample_real += [isample.real]
                jackksample_imag += [isample.imag]
                jackkerr_real += (jackkmean.real-isample.real)*(jackkmean.real-isample.real)
                jackkerr_imag += (jackkmean.imag-isample.imag)*(jackkmean.imag-isample.imag)

            ipx_iz_ritd_real = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, jackkmean.real, np.sqrt(jackkerr_real*(rITD_samp_lenMax-1)/rITD_samp_lenMax)]
            ipx_iz_ritd_imag = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, jackkmean.imag, np.sqrt(jackkerr_imag*(rITD_samp_lenMax-1)/rITD_samp_lenMax)]
            #ipx_iz_ritd_real = [zlist[iz]*latsp, jackkmean.real, np.sqrt(jackkerr_real*(rITD_samp_lenMax-1)/rITD_samp_lenMax)]
            #ipx_iz_ritd_imag = [zlist[iz]*latsp, jackkmean.imag, np.sqrt(jackkerr_imag*(rITD_samp_lenMax-1)/rITD_samp_lenMax)]
            ipx_ritd_real += [ipx_iz_ritd_real]
            ipx_ritd_imag += [ipx_iz_ritd_imag]
            np.savetxt(samplefile+'.real', jackksample_real)
            if 'real' in up_mean:
                np.savetxt(samplefile+'.imag', jackksample_imag)

        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print('\n Saving in:',ritd_out_name)
        ritd_out_name_real = ritd_out_name + '.real'
        if len(ipx_ritd_real) > 1:
            #print('Here')
            np.savetxt(ritd_out_name_real,ipx_ritd_real)
        else:
            p_vec_in = [pxlist[ipx]*2*3.14159/(Ns*latsp)/5.0677, 0, 0]
            p_vec_out = [(pxlist[ipx]+qxyz[0])*2*3.14159/(Ns*latsp)/5.0677, qxyz[1]*2*3.14159/(Ns*latsp)/5.0677, qxyz[2]*2*3.14159/(Ns*latsp)/5.0677]
            Ein = np.sqrt(0.3**2+p_vec_in[0]**2+p_vec_in[1]**2+p_vec_in[2]**2)
            Eout = np.sqrt(0.3**2+p_vec_out[0]**2+p_vec_out[1]**2+p_vec_out[2]**2)
            t = (p_vec_out[0]-p_vec_in[0])**2+(p_vec_out[1]-p_vec_in[1])**2+(p_vec_out[2]-p_vec_in[2])**2-(Eout-Ein)**2
            ipx_ritd_real[0][0] = t
            ipx_ritd_imag[0][0] = t
            ritd_real += ipx_ritd_real
            ritd_imag += ipx_ritd_imag
        if 'real' in up_mean:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ipx_ritd_imag)

    if len(ipx_ritd_real) == 1:
        ritd_out_name = ritd_out_format.replace('*',str(pxlist[0])+'-'+str(pxlist[-1]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        ritd_out_name_real = ritd_out_name + '.real'
        print(np.shape(ritd_real))
        np.savetxt(ritd_out_name_real,ritd_real)
        if 'real' in up_mean:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ritd_imag)



if __name__ == "__main__":

    inputfile = open(sys.argv[2],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[2])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    qxyz = []
    lat = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'qxyz' in line[0]:
            qxyz = [int(line[1]), int(line[2]), int(line[3])]
            describe = describe.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            print('qxyz:',qxyz)

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        if 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)

        if 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])
        
        if 'upmean' in line[0]:
            up_mean = line[1] #numerator of the RG invariant ratio
            up_col_mean = int(line[2])

        if 'upsample' in line[0]:
            up_sample = line[1] #numerator of the RG invariant ratio
            up_col_sample = int(line[2])

        if 'downmean' in line[0]:
            down_mean = line[1] #denominator of the RG invariant ratio
            down_col_mean = int(line[2])

        if 'downsample' in line[0]:
            down_sample = line[1] #denominator of the RG invariant ratio
            down_col_sample = int(line[2])

    if sys.argv[1] == 'ritd':
        jackkbm_to_rITD(Ns,latsp,up_mean,up_col_mean,up_sample,up_col_sample, \
            down_mean,down_col_mean,down_sample,down_col_sample,zlist,pxlist,outfolder,describe,qxyz)
