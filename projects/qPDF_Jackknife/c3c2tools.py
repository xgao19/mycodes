#!/usr/bin/env python3

import h5py
import csv
import numpy as np
from matplotlib import pyplot as plt
from modelfuncs import *
from tools import *
from plot import *

def c2c3command(commandfile, zmin, zmax, option = None, summationfit = None):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    # read describe of this command file
    describe = (commands[0].split())[0]
    # read momentum notation of the c2 and c3 files
    p2 = int((commands[0].split())[1])
    p3 = int((commands[0].split())[2])
    tslist = list(map(int,(commands[1].split())))
    print('ts list:', tslist)

    # read the output folder
    outfolder = (commands[2].split())[0]

    # read the c2pt h5file position
    c2h5file = (commands[3].split())[0]
    c2dataloc = 'c2pt/SS/meson_g15'
    printc2p(c2h5file,p2)

    # read the c3pt h5files position
    c3h5files = []
    for i in range(4,len(commands)-1):
        c3h5file = (commands[i].split())[0]
        c3h5files += [c3h5file]
        print(c3h5file)

    # read and define the subtraction function
    c2mdfA0 = float((commands[-1].split())[0])
    c2mdfE0 = float((commands[-1].split())[1])
    c2mdfA1 = float((commands[-1].split())[2])
    c2mdfE1 = float((commands[-1].split())[3])
    print(c2mdfA0,c2mdfE0,c2mdfA1,c2mdfE1)
    kap =  0.5/(4-0.033)
    def c2mdf(t):
        return c2mdfA0*np.exp(-c2mdfE0*(64-t))*(2*kap)*(2*kap)+c2mdfA1*np.exp(-c2mdfE1*(64-t))*(2*kap)*(2*kap)

    # read and store the space direction describe array
    xdicp = readc3dicp(c3h5files[0],p3)

    if option == 'radiozplot':
        c3c2radiozplot(zmin,zmax,tslist,c2h5file,c3h5files,p2,p3,xdicp,c2dataloc,kap,outfolder,describe,c2mdf)
    elif option == 'radiotauplot':
        c3c2radiotauplot(zmin,zmax,tslist,c2h5file,c3h5files,p2,p3,xdicp,c2dataloc,kap,outfolder,describe,c2mdf,summationfit)
    elif option == 'radiozsave':
        c3c2radiozsave(zmin,zmax,tslist,c2h5file,c3h5files,p2,p3,xdicp,c2dataloc,kap,outfolder,describe,c2mdf)
    elif option == 'readdata':
        return readc2c3h5(zmin, tslist, c2h5file, c3h5files, p2, p3, xdicp, c2dataloc, kap)
    else:
        print('You just take a look of the structure of the h5 file.')

# print the momentum imformation of c2pt
def printc2p(c2h5file,p2):

    #c2dataloc = 'c2pt/SS/meson_g15'
    c2f = h5py.File(c2h5file,'r')
    p = np.array(c2f.attrs['psnk_list'])
    print(p)
    print('p2:',p2, p[p2])
    
# read and store the space direction describe array
# also print the momentum information of c3pt
def readc3dicp(c3h5file,p3):

    c3f = h5py.File(c3h5file,'r')
    c3dataset = c3f['qpdf/SS/meson/l0_/g8']
    print('c3dataset shape',np.shape(c3dataset))
    p = np.array(c3f.attrs['qvec_list'])
    print(p)
    print('p3:',p3,p[p3])

    x = list(c3f['qpdf/SS/meson'].keys())
    x.sort(key=lambda x:len(x))
    xdicp = [x[0]] + x[1::2]
    print(xdicp)
    return xdicp

# read the SS and SP confs at gaven momentum
# the first column is set to be t !!!
def readc2h5(c2h5file, kap, Nt, p2):

    c2f = h5py.File(c2h5file,'r')

    SPc2ptdataset = np.array(c2f['c2pt/SP/meson_g15'])*(2*kap)*(2*kap)
    print('c2dataset shape:',np.shape(SPc2ptdataset))
    try:
        SPc2ptdataset = SPc2ptdataset[:,0,p2,:]
    except:
        SPc2ptdataset = SPc2ptdataset[:,p2,:]
    print('sub c2dataset shape:',np.shape(SPc2ptdataset))
    SPc2pt = []
    for t in range(0, len(SPc2ptdataset[0])):
        raw = [SPc2ptdataset[conf][t].real for conf in range(0,len(SPc2ptdataset))]
        raw = [t] + raw
        #print(raw)
        SPc2pt += [raw]
    print('new sub c2dataset shape:',np.shape(SPc2pt))

    SSc2ptdataset = np.array(c2f['c2pt/SS/meson_g15'])*(2*kap)*(2*kap)
    try:
        SSc2ptdataset = SSc2ptdataset[:,0,p2,:]
    except:
        SSc2ptdataset = SSc2ptdataset[:,p2,:]
    SSc2pt = []
    for t in range(0, len(SSc2ptdataset[0])):
        raw = [SSc2ptdataset[conf][t].real for conf in range(0,len(SSc2ptdataset))]
        raw = [t] + raw
        SSc2pt += [raw]

    return SPc2pt,SSc2pt

##########################################################
####### read the h5file dataset and return a array #######
##########################################################

def readc2c3h5(z, tslist, c2h5file, c3h5files, p2, p3, xdic, c2dataloc, kap):
    print(z, xdic[z])
    datname = 'qpdf/SS/meson/' + xdic[z] + '/g8'

    ######################################
    ##c2 data reading ...
    #######################################

    c2f = h5py.File(c2h5file,'r')
    c2dataset = np.array(c2f[c2dataloc])*(2*kap)*(2*kap)
    #print('c2dataset shape:',np.shape(c2dataset))
    #p = np.array(c2f.attrs['psnk_list'])
    #print(p)

    #######################################
    ##c3 data reading ...
    #######################################
    c2px = []
    c3px = []
    
    for i in range(0, len(c3h5files)):
        #print(c3h5files[i])
        try:
            c2px += [c2dataset[:,0,p2,tslist[i]]]
        except:
            c2px += [c2dataset[:,p2,tslist[i]]]
        c3f = h5py.File(c3h5files[i],'r')
        #print(c3h5files[i])
        dtc3dataset = c3f[datname]
        try:
            dtc3datasetT = np.transpose(np.array(dtc3dataset[:,0,p3,:]))*(2*kap)*(2*kap)*(2*kap)
        except:
            dtc3datasetT = np.transpose(np.array(dtc3dataset[:,p3,:]))*(2*kap)*(2*kap)*(2*kap)
        c3px += [[list(x) for x in dtc3datasetT]]
    #print('c2px shape:',np.shape(c2px),'c3px shape:',np.shape(c3px))
    return c2px,c3px


##########################################################
######## ratio as function of tau-ts/2 at some z #########
##########################################################

def c3c2radiot(tslist, c2data = None, c3data = None, c2mdf = None, blocksize = 1):

    c2px = c2data
    c3px = c3data

    taumin = 1

    ##c3/c2 ratio caculating ...
    if c2mdf == None:
        def c2mdffunc(x):
            return 0
    else:
        c2mdffunc = c2mdf

    c3c2ratio = []
    for i in range(0, len(c3px)):
        ts = tslist[i]
        conf = len(c2px[0]) - 1
        nblock = int(conf/blocksize)
        dtc2px = c2px[i]
        dtc3px = c3px[i]
        #print(dtc3px[i][0:3])
        dtratio = []
        if len(dtc3px) > ts and taumin != 0:
            taumax = ts+1
        else:
            taumax = len(dtc3px)
        for j in range(taumin,taumax-taumin):
            #print(j,len(dtc3px))
            ratiomean = 0
            #print('\n',np.average(list(np.delete(dtc3px[j],0))))
            c3mean = np.average(list(np.delete(dtc3px[j],0)))
            c2mean = np.average(np.delete(dtc2px,0)) - c2mdffunc(ts)
            #c2mean = c2mdffunc(ts)
            #print(c3mean,c2mean,c3mean/c2mean,c2mdffunc(ts))
            ratioerr = 0
            ratiomean = (c3mean/c2mean).real
            #print(nblock,ts,c3mean,dtc2px[0:5],np.average(np.delete(dtc2px,0)).real,c2mdffunc(ts),ratiomean)
            for k in range(0,nblock):
                c3mean = np.average((d5slicedelete(dtc3px[j],[k*blocksize+1,(k+1)*blocksize+1],0))[1:]).real
                c2mean = np.average((d5slicedelete(dtc2px,[k*blocksize+1,(k+1)*blocksize+1],0))[1:]).real - c2mdffunc(ts)
                #c2mean = c2mdffunc(ts)
                ratioerr += ((c3mean/c2mean).real-ratiomean)*((c3mean/c2mean).real-ratiomean)
                #if j == 4 and i==0:
                #    print(k,'/',nblock, c3mean/c2mean, c2mean, c3mean)
            ratioerr = np.sqrt(ratioerr*(nblock-1)/nblock)
            dtratio += [[j-ts/2,ratiomean,ratioerr]]
        c3c2ratio += [dtratio]
        #print(c3c2ratio)
    return c3c2ratio

##########################################################
########### ratio as function of tau at some z ###########
##########################################################

def c3fitratio(tslist, c3data = None, c2data = None, meanonly = False, jointc2 = [None], ratio=True, taumin = 1, blocksize = 1):

    c3c2ratio = []
    c3c2ratio_jackk = []

    for i in range(0, len(tslist)):
        if len(c3data[i]) < tslist[i]:
            temp = tslist[i]-len(c3data[i])+1
            if taumin < temp:
                taumin = temp

    for i in range(0, len(tslist)):  # loop of ts

        ts = tslist[i]
        conf = len(c2data[0]) - 1
        dtc2data = c2data[i]
        dtc3data = c3data[i]
        dtratio = []
        taumax = ts-taumin+1
        nblock = int(conf/blocksize)
        #print('error block n:',nblock)

        for j in range(taumin,taumax): # loop of tau(time insertion)

            # take the average of c3pt and c2pt
            ratiomean = 0
            #print(np.shape(dtc3data[j]),)
            c3mean = np.average(dtc3data[j][1:]).real
            c2mean = np.average(dtc2data[1:]).real
            if ratio == False:
                c2mean = 1

            # mean value of the c3pt/c2pt ratio
            ratioerr = 0
            ratiomean = c3mean/c2mean
            '''
            if Realratio == True:
                ratiomean = ratiomean.real
            '''

            # caculate the wanted data format
            if meanonly == True:         # only mean value is wanted
                dtratio += [[ts, j, ratiomean]]
            else:                        # jackknife error is wantted
                c3c2ratio_jackk_block = []
                for k in range(0,nblock):
                    #print((d5slicedelete(dtc3data[j],[k*blocksize+1,(k+1)*blocksize+1],0))[1:])
                    samplec3mean = np.average((d5slicedelete(dtc3data[j],[k*blocksize+1,(k+1)*blocksize+1],0))[1:]).real
                    samplec2mean = np.average((d5slicedelete(dtc2data,[k*blocksize+1,(k+1)*blocksize+1],0))[1:]).real
                    if ratio == False:
                        c2mean = 1
                    sampleratio = samplec3mean/samplec2mean
                    c3c2ratio_jackk_block += [sampleratio-ratiomean]
                    ratioerr += (sampleratio-ratiomean)*(sampleratio-ratiomean)
                ratioerr = np.sqrt(ratioerr*(conf-1)/conf)
                c3c2ratio_jackk += [c3c2ratio_jackk_block]
                dtratio += [[ts, j, ratiomean, ratioerr]]

        if meanonly == '2dmeanerr':
            c3c2ratio += dtratio
        elif meanonly == 'covariance':
            c3c2ratio = jackkblocks_cov(c3c2ratio_jackk)
        else:
            c3c2ratio += [dtratio]

    if (np.array(jointc2)).any() != None:
        dtratio = []
        tslist_joint = [jointc2[i][0] for i in range(0, len(jointc2))]
        jointc2data = (np.delete(jointc2,0,1)).real  #delete the first column which is a label
        #print(np.shape(jointc2data))
        if meanonly == True:
            for i in range(0, len(jointc2data)):
                dtratio += [[tslist_joint[i], tslist_joint[i]+1, np.average(jointc2data[i])]]
        else:
            for i in range(0, len(jointc2data)):
                dtratio += [[tslist_joint[i], tslist_joint[i]+1, np.average(jointc2data[i]), np.std(jointc2data[i], ddof = 1)]]
        
        if meanonly == '2dmeanerr':
            c3c2ratio += dtratio
        elif meanonly == 'covariance':
            c3c2ratio = jackkblocks_cov(c3c2ratio_jackk)
        else:
            c3c2ratio += [dtratio]

    return c3c2ratio

##########################################################
####### center ratio at center tau as function of z ######
##########################################################

def c3c2radioz(z, tslist, c2data = None, c3data = None, c2mdf = None):

    c2px = c2data
    c3px = c3data

    ##c3/c2 ratio caculating ...
    if c2mdf == None:
        def c2mdffunc(x):
            return 0
    else:
        c2mdffunc = c2mdf

    c3c2ratio = []
    for i in range(0, len(c3px)):
        ts = tslist[i]
        conf = len(c2px[0])
        dtc2px = c2px[i]
        dtc3px = c3px[i]
        dtratio = []
        for j in range(int(ts/2),int(ts/2)+1):
            ratiomean = 0
            c3mean = np.average(dtc3px[j]).real
            c2mean = np.average(dtc2px).real - c2mdffunc(ts)
            #print(ts, np.average(dtc2px).real, c2mdf(ts))
            ratioerr = 0
            ratiomean = c3mean/c2mean
            jackk = []
            for k in range(0,conf):
                c3mean = np.average(np.delete(dtc3px[j],k)).real
                c2mean = np.average(np.delete(dtc2px,k)).real - c2mdffunc(ts)
                ratioerr += (c3mean/c2mean-ratiomean)*(c3mean/c2mean-ratiomean)
            ratioerr = np.sqrt(ratioerr*(conf-1)/conf)
            dtratio = [z,ratiomean,ratioerr]
            #print(ts, dtratio)
        c3c2ratio += [dtratio]
        #print(c3c2ratio)
    return c3c2ratio


##########################################################
################# c3/c2 ratio ploting ... ################
##########################################################
  
def c3c2radiotauplot(zmin,zmax, tslist, c2h5file, c3h5files, p2, p3, xdic, c2dataloc, kap, pltfolder = './', plttitle = '' , c2mdf = None, summationfit=None):
    for z in range(zmin,zmax+1):
        # data point plot
        print('z=',z)
        c2data ,c3data = readc2c3h5(z,tslist,c2h5file,c3h5files,p2,p3,xdic,c2dataloc,kap)
        #print(np.shape(c2data),np.shape(c3data))
        c3c2ratio = c3c2radiot(tslist, c2data ,c3data, c2mdf)
        xylabel = ['(τ-t/2)a^-1','R(t,τ,Pz,γt)']
        datalabel = [plttitle+'_z'+str(z)]
        dtratio = []
        for i in range(0, len(c3c2ratio)):
            datalabel += ['ts'+str(tslist[i])]
            print('c3c2ratioshape:',np.shape(c3c2ratio[i]))

        # fitting curve plot
        if summationfit == True:
            #x = [x[0] for x in c3c2ratio[0]]
            #curve_x = np.arange(x[0]-1,x[-1]+1,0.01)
            #fitresult = summation(z, tslist, c2h5file, c3h5files, p2, p3, xdic, c2dataloc, kap, c2mdf)
            fitresult = [0, 0, 0]
            def pltfunc(x):
                return fitresult[0]*(x+1-x)
            def pltbandup(x):
                return (fitresult[0]-fitresult[1])*(x+1-x)
            def pltbanddown(x):
                return (fitresult[0]+fitresult[2])*(x+1-x)
            curvelabel = 'summation' + str(round(fitresult[0],3))
            #outfile = open(pltfolder+ str(p2)  + 'summation' + '.txt','a')
            #np.savetxt(outfile,[fitresult])
            #outfile.close()
            pltband = [pltbandup,pltbanddown]
        else:
            pltfunc = None
            pltband = None
            curvelabel = None
        #curve_y = [fitresult[0] for i in range(0,len(curve_x))]
        #curve_yerrn = [fitresult[0]-fitresult[1] for i in range(0,len(curve_x))]
        #curve_yerrp = [fitresult[0]+fitresult[2] for i in range(0,len(curve_x))]
        #plt.fill_between(curve_x, curve_yerrn, curve_yerrp, color= 'LightSteelBlue')

        plot_err(c3c2ratio, datalabel, xylabel, folder = pltfolder,func= pltfunc, fillfunc=pltband,funclabel=curvelabel)

# plt.xlabel('z')
def c3c2radiozplot(zmin,zmax, tslist, c2h5file, c3h5files, p2, p3, xdic, c2dataloc, kap, pltfolder = './', plttitle = '' , c2mdf = None):

    c3c2ratio = []
    for z in range(zmin,zmax+1):
        c2data ,c3data = readc2c3h5(z,tslist,c2h5file,c3h5files,p2,p3,xdic,c2dataloc,kap)
        c3c2ratio += [c3c2radioz(z, tslist, c2mdf, c2data, c3data)]         # c3c2ratio[z][dt][value&err]å
    c3c2ratio = ex12_3d(c3c2ratio)

    datalabel = [plttitle+'_center']
    for i in range(0, len(c3c2ratio)):
        datalabel += ['ts'+str(tslist[i])]
    xylabel = ['z', 'R(t,τ,Pz,γt)']
    plot_err(c3c2ratio, datalabel, xylabel, folder = pltfolder)
def c3c2radiozsave(zmin,zmax, tslist, c2h5file, c3h5files, p2, p3, xdic, c2dataloc, kap, pltfolder = './', plttitle = '' , c2mdf = None):
    c3c2ratio = []
    for z in range(zmin,zmax+1):
        c2data ,c3data = readc2c3h5(z,tslist,c2h5file,c3h5files,p2,p3,xdic,c2dataloc,kap)
        c3c2ratio += [c3c2radioz(z, tslist, c2mdf, c2data, c3data)]         # c3c2ratio[z][dt][value&err]å
    c3c2ratio = ex12_3d(c3c2ratio)
    for i in range(0,len(c3c2ratio)):
        outfile = pltfolder + plttitle + '_ts' + str(tslist[i]) + '.txt'
        np.savetxt(outfile,c3c2ratio[i])
'''
##########################################################
#################### summation fitting ###################
##########################################################

def summation(z, tslist, c2h5file, c3h5files, p2, p3, xdic, c2dataloc, kap, c2mdf):

    datname = 'qpdf/SS/meson/' + xdic[z] + '/g8'
    c2f = h5py.File(c2h5file,'r')
    c2dataset = np.array(c2f[c2dataloc])*(2*kap)*(2*kap)
    c2f.close()
    c2data = []
    c3data = []
    c2dataall = []
    for t in range(0, len(c2dataset[0][0])):
        c2dataall += [[t]+list(c2dataset[:,p2,t].real)]
    print(np.shape(c2dataall[t]))
    for i in range(0,len(tslist)):
        ts = tslist[i]
        c2data += [[ts]+list(c2dataset[:,p2,ts].real - c2mdf(ts))]
        c3f = h5py.File(c3h5files[i],'r')
        c3dataset = c3f[datname]
        if len(c3dataset[0][0][0]) >= ts+1:
            c3data += [list(np.array(c3dataset[:,0,p3,1:ts].real)*(2*kap)*(2*kap)*(2*kap))]
        else:
            c3data += [list(np.array(c3dataset[:,0,p3,1:].real)*(2*kap)*(2*kap)*(2*kap))]
    popt = sumfit(c2data, c3data, 1000)
    return popt
'''

#c3c2radiotauplot(0,0,True)
#c3c2radiotzrange(0,32)
#c3c2radiozplot(0,32)

#c2c3command(sys.argv[1], 0, 8, 'radiozsave')

# These two functions are used to combine two seperated dataset
def c3c2radiozsave2(zmin, zmax, tslist, commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    plttitle = (commands[0].split())[0]
    pltfolder = (commands[1].split())[0]
    jlabcommand = (commands[2].split())[0]
    bnlcommand = (commands[3].split())[0]
    
    c3c2ratio = []
    for z in range(zmin,zmax+1):
        c2data ,c3data = c2c3combi(jlabcommand, bnlcommand,z)
        print(np.shape(c2data),np.shape(c3data))
        c3c2ratio += [c3c2radioz(z, tslist, None, c2data, c3data)]         # c3c2ratio[z][dt][value&err]å
    c3c2ratio = ex12_3d(c3c2ratio)
    for i in range(0,len(c3c2ratio)):
        outfile = pltfolder + plttitle + '_ts' + str(tslist[i]) + '.txt'
        np.savetxt(outfile,c3c2ratio[i])

def c2c3combi(commandfile1, commandfile2,z):

        c2data1,c3data1 = c2c3command(commandfile1,z,z,'readdata')
        c2data2,c3data2 = c2c3command(commandfile2,z,z,'readdata')
        c2data = np.append(np.transpose(c2data1), np.transpose(c2data2),axis=0)
        c2data = np.transpose(c2data)
        c3data = []
        for i in range(0, len(c3data1)):
            c3dataraw = np.append(np.transpose(c3data1[i]), np.transpose(c3data2[i]),axis=0)
            c3dataraw = np.transpose(c3dataraw)
            c3data += [c3dataraw]
        print(np.shape(c2data),np.shape(c3data))
        return c2data, c3data


#c3c2radiozsave2(0, 8, [8,10,12], sys.argv[1])

#c2c3command(sys.argv[1], 0, 0, option = 'radiotauplot')