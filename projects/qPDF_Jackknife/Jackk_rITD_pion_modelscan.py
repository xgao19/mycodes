#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
#from tools import *
import inspect
from scipy.optimize import curve_fit
from scipy import special
from scipy import integrate
from statistics import *
from rITD_pion_ritdReal_modelscan import *

'''
This code is used to fit the pion with some model:

    1), Ax^a(1-x)^b

Note:
    1), all dimensional parameters are using (fm) unit when go into the functions

'''

################################################
############### Model functions  ###############
################################################
def pdfs_model_ab(x, n, *ab):
    #print(ab)
    return (x**n)*special.gamma(2+ab[0]+ab[1])/(special.gamma(1+ab[0])*special.gamma(1+ab[1]))*(x**ab[0])*((1-x)**ab[1])

def pdfs_model_abc(x, n, *abc):
    #A = integrate.quad(lambda x: (x**abc[0])*((abc[2]-x)**abc[1]), 0, 1)[0]
    #return 1/A*(x**n)*(x**abc[0])*((abc[2]-x)**abc[1])
    return (x**n)*(x**abc[0])*((abc[2]-x)**abc[1])/(abc[2]**(1+abc[0]+abc[1]) \
        * special.beta(1+abc[0],1+abc[1]) * special.betainc(1+abc[0],1+abc[1],1/abc[2]))

class pdfs_model_list:

    model_ab = pdfs_model_ab
    model_abc = pdfs_model_abc

################################################
############# Auxiliary functions  #############
################################################
def ListLast(List):
    return List[-1]

class ritd_scan_list:

    #PznNLO_real = ritd_NLO_Pzn_real
    #PznRsLL_real = ritd_RsLL_Pzn_real
    PznNLO_pznm_real = ritd_NLO_Pzn_pznm_real
    PznNLOEP_pznm_real = ritd_NLOEP_Pzn_pznm_real
    PznNNLO_pznm_real = ritd_NNLO_Pzn_pznm_real
    PznNNLOEP_pznm_real = ritd_NNLOEP_Pzn_pznm_real
    #PznNNLOevo_pznm_real = ritd_NNLOevo_Pzn_pznm_real
    #PznNLLevo_pznm_real = ritd_PznNLLevo_pznm_real

################################################
############## fitting functions  ##############
################################################
def model_scan_loops(ritd_matchFunc, rITD, rITD_err, pznm_fm, pxlist_fm, zskip, zmaxlist, params, moms, mass, mu, nmax):

    chisq_list = []
    for izmin in range(0, len(zskip)):
        for izmax in range(0, len(zmaxlist)):
            for iparams in range(0, len(moms)):

                iparams_moms = moms[iparams]
                iparams_chisq = 0
                count = 0

                for iz in range(zskip[izmin], int(zmaxlist[izmax]/latsp)+1):
                    kernel_list = []
                    for ipx in range(0, len(pxlist_fm)):
                        itdLat = rITD[ipx][iz]
                        itdLatErr = rITD_err[ipx][iz]
                        itd_expect, itd_syserr, kernel_list = \
                            ritd_matchFunc(iz*latsp,pxlist_fm[ipx],mass*fmGeV,mu*fmGeV,nmax,iparams_moms,k=0,pznm=pznm_fm,kernel_list=kernel_list)
                        #print(pxlist_GeV[ipx],iz*latsp,itdlat,itdRsLL)
                        iparams_chisq += (itdLat - itd_expect)**2/(itdLatErr**2+itd_syserr**2)
                        count += 1
                    #print(itd_expect, itd_syserr,itdLatErr)
                iparams_chisq = [pznm_fm/fmGeV, zskip[izmin]*latsp, zmaxlist[izmax], *params[iparams],iparams_chisq/(count - 2)]
                #print(iparams_chisq)
                chisq_list += [iparams_chisq]
    return chisq_list


if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'fm\n')

    # read parameters
    scanlist = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mass' in line[0]:
            mass = float(line[1])
            print('Hardron mass:', mass, 'GeV')

        elif 'mu' in line[0]:
            mu = float(line[1])
            print('Renormalization scale:', mu, 'GeV')

        elif 'zskip' in line[0]:
            zskip = [int(line[i]) for i in range(1, len(line))]
            print('Skip zmin (z/a) =', zskip)
            describe = describe.replace('zmin', 'zmin'+str(zskip[0]))

        elif 'zmax(fm)' in line[0]:
            zmaxlist = [float(line[i]) for i in range(1, len(line))]
            print('Zmax =', zmaxlist, 'fm')
            describe = describe.replace('zmax_', 'zmax'+str(zmaxlist[0])+'_')
            #print(describe)
        elif 'zmax/a' in line[0]:
            zmaxa = int(line[1])
            zmax = (zmaxa+0.5)*latsp
            zmaxlist = [zmax]
            print('Zmax =', str(zmaxa)+'a', '=', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmaxa)+'a')

        elif 'nmax' in line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)
        
        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            pxlist_fm = []
            pxlist_GeV = []
            for ipx in pxlist:
                pxlist_fm += [2*PI*ipx/(Ns*latsp)]
                pxlist_GeV += [round(2*PI*ipx/(Ns*latsp)/fmGeV,2)]
            print('pzlist:',pxlist_GeV,'(GeV)')
            if pxmin == pxmax:
                describe = describe.replace('pzn_', 'pz'+str(pxmin)+'_')
            #print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            pznm_fm = 2*PI*pznm/(Ns*latsp)
            pznm_GeV = round(2*PI*pznm/(Ns*latsp)/fmGeV,2)
            print('pznm :',pznm_GeV,'(GeV)')
        
        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

        elif 'scan' in line[0]: # Please note, you better set the scan parameter orderly
            scan = [float(line[1])+float(line[3])*iscan for iscan in range(0, math.ceil((float(line[2])-float(line[1]))/float(line[3]))+1)]
            scanlist += [scan]
            #print([float(line[1])+float(line[3])*iscan for iscan in range(0, int((float(line[2])-float(line[1]))/float(line[3]))+1)])
            print('Scan parameters setting:', '[',scan[0], ':', scan[-1], ']', 'of', len(scan), 'with grid', float(line[3]))

        elif 'model' in line[0]:
            model = getattr(pdfs_model_list, line[1])
            print('Scan model:', line[1])

        elif 'ritd' == line[0]:
            ritd_matchFunc = getattr(ritd_scan_list, line[1])
            print('rITD matching formula:', line[1])

    # Read the data here
    rITD_mean = [] #rITD_mean[ipx][iz]
    rITD_err = [] #rITD_mean_err[ipx][iz]
    rITD_sample_read = [] #rITD_sample[ipx][iz][isample]
    rITD_sample = [] #rITD_sample[isample][ipx][iz]
    rITD_samp_lenMax = 0
    for ipx in pxlist:

        ipx_mean_file = mean_input_format.replace('*', str(ipx))
        ipx_mean_data = np.loadtxt(ipx_mean_file)
        ipx_rITD_mean = []
        ipx_rITD_err = []
        zlen = len(ipx_mean_data)
        for iz in range(0, zlen):
            ipx_rITD_mean += [ipx_mean_data[iz][1]]
            ipx_rITD_err += [ipx_mean_data[iz][2]]
        rITD_mean += [ipx_rITD_mean]
        rITD_err += [ipx_rITD_err]

        ipx_sample_file = sample_input_format.replace('*', str(ipx))
        ipx_rITD_sample = []
        for iz in range(0, zlen):
            iz_sample_file = ipx_sample_file.replace('#', str(iz))
            ipx_rITD_sample += [np.loadtxt(iz_sample_file)]
            if len(ipx_rITD_sample[iz]) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(ipx_rITD_sample[iz])
        rITD_sample_read += [ipx_rITD_sample]
    print('rITD data shape:', np.shape(rITD_sample_read), ', sample maximal length:', rITD_samp_lenMax)

    for isamp in range(0,rITD_samp_lenMax):
        ipx_rITD_sample = []
        for ipx in range(0, len(pxlist_fm)):
            iz_rITD_sample = []
            for iz in range(0, zlen):
                rITD_samp_len = len(rITD_sample_read[ipx][iz])
                if isamp < rITD_samp_len:
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp]]
                else:
                    #iz_rITD_sample += [rITD_sample_read[ipx][iz][np.random.randint(0, rITD_samp_len)]]
                    iz_rITD_sample += [rITD_mean[ipx][iz]]
            ipx_rITD_sample += [iz_rITD_sample]
        rITD_sample += [ipx_rITD_sample]

    print('mean data:', np.shape(rITD_mean), ', sample data:', np.shape(rITD_sample))
                
    # model parameters and moments collection
    params = []
    moms = []
    if len(scanlist) == 2:
        for param1 in scanlist[0]:
            for param2 in scanlist[1]:
                iparams = [param1, param2]
                params += [iparams]
                iparams_moms = []
                for imom in range(0, nmax+1):
                    #print(imom)
                    iparams_moms += [integrate.quad(lambda x: model(x, imom, *iparams), 0, 1)[0]]
                    #print(imom,integrate.quad(lambda x: model(x, imom, *iparams), 0, 1)[0])
                moms += [iparams_moms]
        print('Collected moments:',np.shape(moms), ', <1>:',moms[0][0], '\n')
    elif len(scanlist) == 3:
        for param1 in scanlist[0]:
            for param2 in scanlist[1]:
                for param3 in scanlist[2]:
                    iparams = [param1, param2, param3]
                    params += [iparams]
                    iparams_moms = []
                    for imom in range(0, nmax+1):
                        iparams_moms += [integrate.quad(lambda x: model(x, imom, *iparams), 0, 1)[0]]
                    moms += [iparams_moms]
        print('Collected moments:',np.shape(moms), ', <1>:',moms[0][0], '\n')
        #print(moms)

    pdf_x = [ix/100 for ix in range(1, 101)]
    # scan jackknife mean value
    def model_scan(rITD):
        return  model_scan_loops(ritd_matchFunc, rITD, rITD_err, pznm_fm, pxlist_fm, zskip, zmaxlist, params, moms, mass, mu, nmax)
    # scan jackknife mean value
    savename = outfolder + describe + '_scan.txt'
    chisq_list = model_scan(rITD_mean)
    chisq_list.sort(key=ListLast)
    print(np.shape(chisq_list))
    np.savetxt(savename, chisq_list, fmt='%.6e')
    params_mean = chisq_list[0][3:-1]
    #print(chisq_list[0])
    pdf_fx_mean = model(np.array(pdf_x), 0, *params_mean)
    # scan jackknife samples
    params_errs = np.zeros(len(params_mean))
    pdf_fx_errs = np.zeros(len(pdf_fx_mean))
    savename = samplefolder + describe + '_jackksamples.txt'
    samplepopt = []
    print('Scanning Jackknife samples:')
    nsamps = len(rITD_sample)
    for isamp in range(0, nsamps):
        print('  ',isamp,'/', len(rITD_sample))
        isamp_chisq_list = model_scan(rITD_sample[isamp])
        isamp_chisq_list.sort(key=ListLast)
        samplepopt += [isamp_chisq_list[0]]
        params_sample = isamp_chisq_list[0][3:-1]
        for iparams in range(0, len(params_mean)):
            params_errs[iparams] += (params_mean[iparams]-params_sample[iparams])**2  
        pdf_fx_sample = model(np.array(pdf_x), 0, *params_sample)
        for ix in range(0, len(pdf_fx_sample)):
            pdf_fx_errs[ix] += (pdf_fx_mean[ix]-pdf_fx_sample[ix])**2
    np.savetxt(savename, samplepopt, fmt='%.6e')  

    popt = []
    savename = outfolder + describe + '_popt.txt'
    for iparams in range(0, len(params_mean)):
        popt += [params_mean[iparams], np.sqrt(params_errs[iparams]*(nsamps-1)/nsamps)]
    popt = chisq_list[0][0:3] + popt + [chisq_list[0][-1]]
    np.savetxt(savename, popt, fmt='%.6e') 

    pdf_x_fx_err0 = []
    pdf_x_fx_err1 = []
    pdf_x_fx_err2 = []
    savename0 = outfolder + describe + '_fx_band0.txt'
    savename1 = outfolder + describe + '_fx_band1.txt'
    savename2 = outfolder + describe + '_fx_band2.txt'
    for ix in range(0, len(pdf_fx_mean)):
        pdf_x_fx_err0 += [[pdf_x[ix], pdf_fx_mean[ix]-np.sqrt(pdf_fx_errs[ix]*(nsamps-1)/nsamps)]]
        pdf_x_fx_err1 += [[pdf_x[ix], pdf_fx_mean[ix], np.sqrt(pdf_fx_errs[ix]*(nsamps-1)/nsamps)]]
        pdf_x_fx_err2 += [[pdf_x[ix], pdf_fx_mean[ix]+np.sqrt(pdf_fx_errs[ix]*(nsamps-1)/nsamps)]]
    np.savetxt(savename0, pdf_x_fx_err0, fmt='%.6e') 
    np.savetxt(savename1, pdf_x_fx_err1, fmt='%.6e') 
    np.savetxt(savename2, pdf_x_fx_err2, fmt='%.6e') 

