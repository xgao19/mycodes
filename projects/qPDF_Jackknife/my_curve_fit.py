#!/usr/bin/env python3
import numpy as np
#from modelfuncs import *
from scipy.optimize import least_squares


data_1 = np.loadtxt('/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_qpdf/ratio/g8/X0/plot_qpdf_X0_48c64a06.txt')
data_2 = np.loadtxt('/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_qpdf/ratio/g8/X0/plot_qpdf_X0_64c64a04.txt')
data_3 = np.loadtxt('/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_qpdf/ratio/g8/X0/plot_qpdf_X0_64c64a076.txt')

x1 = [(0.06, data_1[i][2]) for i in range(0, len(data_1))]
x1a = [0.06 for i in range(0, len(data_1))]
y1 = [data_1[i][4] for i in range(0, len(data_1))]
yerr1 = [data_1[i][5] for i in range(0, len(data_1))]
data_1 = [x1,y1,yerr1]

x2 = [(0.04, data_2[i][2]) for i in range(0, len(data_2))]
x2a = [0.04 for i in range(0, len(data_2))]
y2 = [data_2[i][4] for i in range(0, len(data_2))]
yerr2 = [data_2[i][5] for i in range(0, len(data_2))]
data_2 = [x2,y2,yerr2]

x3 = [(0.076, data_3[i][2]) for i in range(0, len(data_3))]
x3a = [0.076 for i in range(0, len(data_3))]
y3 = [data_3[i][4] for i in range(0, len(data_3))]
yerr3 = [data_3[i][5] for i in range(0, len(data_3))]
data_3 = [x3,y3,yerr3]

#fitfunc = linearfunc
#fitfunc = polynomialto2
def fitfunc(x,b,c):
    latsp,apz = x
    return 1.001 + latsp * b + c * (apz)**2

def my_chisq(x,y,yerr,popt):
    chisq = 0
    for i in range(0, len(x1)):
        chisq += (fitfunc(x[i], *popt) - y[i])**2/yerr[i]**2
    return chisq

def my_popt(datalist):
    len_data = 0
    for idata in datalist:
        len_data += len(idata)
    def to_leastsq(popt):
        chisq_sum = 0
        for i in range(0,len(datalist)):
            chisq_sum += my_chisq(datalist[i][0],datalist[i][1],datalist[i][2],[popt[-2],popt[-1]])
            print(popt,chisq_sum)
        return chisq_sum
    popt = least_squares(to_leastsq, [-0.1,-0.1])
    return list(popt.x), popt.fun[0]/(len_data-len(popt.x))

print(my_popt([data_1,data_2,data_3]))

    