#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
from tools import *
import inspect
from scipy.optimize import curve_fit
from modelfuncs import *
from statistics import *

moments_CT14=[1.00376, 0.144451, 0.0476091, 0.0186867, 0.00888144, 0.00462879, \
0.0026394, 0.00159579, 0.00101775, 0.000675364, 0.000464451, \
0.000328609, 0.000238535, 0.00017687, 0.000133705, 0.000102752, \
0.0000801653, 0.000063369, 0.0000507021, 0.0000410029]
moments_NNPDF31=[0.998761, 0.139086, 0.0499636, 0.0182169, 0.0103797, 0.00444149, \
0.00352858, 0.00149469, 0.0015749, 0.000619941, 0.000835727, \
0.000299104, 0.000498936, 0.000162017, 0.00032393, 0.0000960879, \
0.000223727, 0.0000612112, 0.000161957, 0.0000412633]
moments_JAM17=[1., 0.179885, 0.0598322, 0.0256483, 0.0128658, 0.00719325, \
0.00433858, 0.00278038, 0.00186466, 0.00129981, 0.00093427, \
0.000689639, 0.00052055, 0.000400564, 0.00031357, 0.000249041, \
0.00020053, 0.000163273, 0.000134457, 0.000111696]
moments_NNPDF11pol=[1., 0.165089, 0.0533051, 0.0199898, 0.00933204, 0.00467524, \
0.0026027, 0.00152755, 0.000946391, 0.000614819, 0.000409324, \
0.000285582, 0.000200077, 0.000147173, 0.00010707, 0.0000820377, \
0.0000614174, 0.0000486047, 0.0000372084, 0.0000302249]

Ns=64
latsp=0.042
mu0=2.0
alps0=0.31
lambdaQCD = 0.3
Norder = 19
mass = 1.1 #GeV
outname = '/Users/gaoxiang/Desktop/0study/research/lattice/data/nucleon_data/analysis/ritd/globalfit/NNPDF31_real_O19_2.txt'


################################################
############ Target mass correction  ###########
################################################

def Cnk0(n,k):
    if k==0: return 1
    if n==0: return 0
    return Cnk0(n-1,k)+Cnk0(n-1,k-1)

def tgM_up(mom, c, n):
    ratio = 0
    for i in range(0, int((n+1)/2)+1):
        ratio += Cnk0(n-i+1, i)*c**i
    #print(n, ratio)
    return mom*ratio

def tgM_heli(mom, c, n):
    ratio = 0
    for i in range(0, int(n/2)+1):
        #print('!!!', n,i,(n-i+1)/(n+1)*Cnk0(n-i, i)*c**i)
        ratio += (n-i+1)/(n+1)*Cnk0(n-i, i)*c**i
    #print(n, ratio)
    return mom*ratio


################################################
############# Auxiliary functions  #############
################################################
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])
def Cn(z,n, mu = mu0*5.0676896,alps = alps0,latsp = latsp):
    CF = 4/3
    gammaE = 0.5772156649
    pi = 3.14159
    z = z * latsp
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))
    #return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))+alps*CF/(2*pi)*(2/(2+3*n+n*n))

################################################
############### model functions  ###############
################################################
'''
z = np.arange(0.01,1.01,0.01)
y = pz4_pz5_On_real_NNPDF31(z)
zylist = [[z[i],y[i]] for i in range(0, len(z))]
np.savetxt(outname,zylist)
y = pz4_pz5_On_imag_NNPDF31(z)
zylist = [[z[i],y[i]] for i in range(0, len(z))]
np.savetxt(outname.replace('real','imag'),zylist)
'''

def pz4_pz5_momN(z, *moms_list,pz4=1.85, pz5=2.31):

    zpz4=pz4*5.0676896*z*latsp
    zpz5=pz5*5.0676896*z*latsp
    cpz4 = mass**2/4/pz4**2
    cpz5 = mass**2/4/pz5**2

    Qpz4 = 1
    Qpz5 = 1

    #print(momN,exp_list)
    for i in range(0, len(moms_list)):
        i_factorial = 1
        for j in range(1, i+2):
            i_factorial *= j
        if i%2 == 1111110:
            mom = moments_JAM17[i+1]
            #print(mom)
        else:
            mom = moms_list[i]
        Qpz4 += Cn(z,(i+1))/Cn(z,0) * (-1j*zpz4)**(i+1)/i_factorial * tgM_heli(mom, cpz4, i+1)
        Qpz5 += Cn(z,(i+1))/Cn(z,0) * (-1j*zpz5)**(i+1)/i_factorial * tgM_heli(mom, cpz5, i+1)
        #print(Qpz4)
    return Qpz5/Qpz4

def complex_pz4_pz5_momN(z, *moms_list):
    N = len(z)
    z_real = z[:N//2]
    z_imag = z[N//2:]
    #print(z_real,z_imag)
    y_real = np.real(pz4_pz5_momN(z_real, *moms_list))
    y_imag = np.imag(pz4_pz5_momN(z_imag, *moms_list))
    return np.hstack([y_real,y_imag])

def complex_pz4_pz5_momN_constrained(z, *exp_list):
    N = len(z)
    z_real = z[:N//2]
    z_imag = z[N//2:]
    #print(z_real,z_imag)
    y_real = np.real(pz4_pz5_momN_constrained(z_real, *exp_list))
    y_imag = np.imag(pz4_pz5_momN_constrained(z_imag, *exp_list))
    return np.hstack([y_real,y_imag])

def moms_exp(exp_list):
    moms_list = []
    for i in range(0, len(exp_list)):
        moms_i = 0
        for j in range(i, len(exp_list)):
            moms_i += np.exp(-exp_list[j])
        moms_list += [moms_i]
    return moms_list


class rif_func_list:

    rif_O2 = pz4_pz5_O2
    complex_rif_O2 = complex_pz4_pz5_O2
    complex_rif_O3 = complex_pz4_pz5_O3
    complex_rif_O4 = complex_pz4_pz5_O4
    complex_rif_O6 = complex_pz4_pz5_O6
    complex_rif_ON = complex_pz4_pz5_momN
    complex_rif_ON_constrained = complex_pz4_pz5_momN_constrained

def jackkrif_collect(mean_format,col_mean,sample_format,zlist,pxlist):
    
    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)

    # data collection
    bm_mean_data = []
    bm_samples_data = []
    for ipx in range(0,len(pxlist)):
        sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[ipx]))).replace('#', str(zlist[0]))))
        njackk = len(sampledata)

        ipx_mean_name_real = mean_format.replace('*', str(pxlist[ipx]))
        ipx_mean_data_real = np.loadtxt(ipx_mean_name_real)
        if 'real' in ipx_mean_name_real:
            ipx_mean_name_imag = ipx_mean_name_real.replace('real', 'imag')
            ipx_mean_data_imag = np.loadtxt(ipx_mean_name_imag)
            ipx_bm_mean_data = [complex(ipx_mean_data_real[iz][col_mean],ipx_mean_data_imag[iz][col_mean]) for iz in zlist]
        else:
            ipx_bm_mean_data = [ipx_mean_data_real[iz][col_mean] for iz in zlist]
        bm_mean_data += [ipx_bm_mean_data]
        #print(bm_mean_data)

        ipx_in_format = sample_format.replace('*', str(pxlist[ipx]))
        ipx_data = []

        for iz in range(0, len(zlist)):
            iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            jacksample_real = np.loadtxt(iz_ipx_in_name)
            #print(jacksample_real)
            if 'real' in ipx_mean_name_real:
                jacksample_imag = np.loadtxt(iz_ipx_in_name.replace('real','imag'))
                try:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, njackk)]
                except:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, njackk)]
            else:
                try:
                    jacksample = [jacksample_real[i][col_sample] for i in range(0, njackk)]
                except:
                    jacksample = [jacksample_real[i] for i in range(0, njackk)]
            ipx_data += [jacksample]
        bm_samples_data += [ipx_data]
    print('mean shape:', np.shape(bm_mean_data),'\n')
    print('samples shape:', np.shape(bm_samples_data),'\n')
    return bm_mean_data,bm_samples_data


'''
fitting procedure
'''
def rITD_fit(fitfunc, input_mean_format,mean_col,sample_format,zlist, pxlist, poptstart=None, xvaluemin=-1e50, xvaluemax=1e50):

    fitfunc = getattr(rif_func_list, fitfunc)

    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)
    
    # jackknife samples data collection
    z = []
    read_ritd_mean_data,read_ritd_data = jackkrif_collect(input_mean_format,mean_col,sample_format,zlist,pxlist)

    if isinstance(read_ritd_mean_data[0][0],complex):
        z = []
        ritd_mean_data = []
        ritd_data = []
        for ipx in range(0, len(read_ritd_mean_data)):
            for iz in range(0, len(read_ritd_mean_data[ipx])):
                z += [zlist[iz]]
                ritd_mean_data += [read_ritd_mean_data[ipx][iz].real]
                ritd_data += [[read_ritd_data[ipx][iz][isamp].real for isamp in range(0, len(read_ritd_data[ipx][iz]))]]
            for iz in range(0, len(read_ritd_mean_data[ipx])):
                z += [zlist[iz]]
                ritd_mean_data += [read_ritd_mean_data[ipx][iz].imag]
                ritd_data += [[read_ritd_data[ipx][iz][isamp].imag for isamp in range(0, len(read_ritd_data[ipx][iz]))]]
        #print(np.shape(ritd_mean_data),np.shape(ritd_data))
    else:
        z = []
        ritd_mean_data = []
        ritd_data = []
        for ipx in range(0, len(read_ritd_mean_data)):
            for iz in range(0, len(read_ritd_mean_data[ipx])):
                z += [zlist[iz]]
                ritd_mean_data += [read_ritd_mean_data[ipx][iz]]
                ritd_data += [[read_ritd_data[ipx][iz][isamp] for isamp in range(0, len(read_ritd_data[ipx][iz]))]]


    cov_matrix = jackksample_cov(ritd_mean_data,ritd_data,stderr=False)
    #print(cov_matrix)
    #np.savetxt("/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_64c64_data/analysis/c3ptfit_jackk_ritd/jointfit/cov_z3.txt",cov_matrix)
    cov_matrix = [np.sqrt(cov_matrix[i][i]) for i in range(0, len(cov_matrix))]
    #print(cov_matrix)


    # fitting loop
    y = ritd_mean_data
    #print(z,y,cov_matrix)
    poptmean, popv = curve_fit(fitfunc, np.array(z), y, p0=poptstart,sigma=cov_matrix)
    xylist = [[z[i],y[i],cov_matrix[i]] for i in range(0, len(z))]
    chisq = (chisq_value_stderr(xylist, fitfunc, poptmean))/(len(z)-len(poptmean))
    #poptmean = moms_exp(poptmean)
    print('Popt:',poptmean,'chisq:',chisq,'\n')
    poptmean = list(poptmean) + [chisq]
    popterr = np.zeros(len(poptmean))

    for i in range(0, njackk):

        y = [ritd_data[j][i] for j in range(0, len(ritd_data))]
        popt,popv = curve_fit(fitfunc, np.array(z), y, p0=poptstart,sigma=cov_matrix)
        #print(popt)

        xylist = [[z[i],y[i],cov_matrix[i]] for i in range(0, len(z))]
        #chisq = (chisq_value_stderr(xylist, fitfunc, popt))/(len(z)-len(poptmean))
        chisq = 0
        popt = list(popt) + [chisq]

        for ip in range(0, len(popt)):
            popterr[ip] += (popt[ip]-poptmean[ip]) * (popt[ip]-poptmean[ip])

    meanerr = []
    for i in range(0, len(popt)):
        meanerr += [poptmean[i], np.sqrt(popterr[i]*(njackk-1)/njackk)]

    return meanerr

'''
run the fitting here
'''


if __name__ == "__main__":
    #print(sys.argv[1])
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    fitfunc = []
    poptguess = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        if 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)

        if 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])
        
        if 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        if 'sample' in line[0]:
            sample_input_format = line[1]

        if 'fitfunc' in line[0]:
            fitfunc += [line[1]]
            poptguess += [list(map(float, line[2:]))]
    
    for ifit in range(0, len(fitfunc)):

        z_rITDfit = []
        
        for iz in range(0, len(zlist)):
            print("iz:",iz)

            out_name = describe.replace('*', str(pxlist[0])+'-'+str(pxlist[-1]))
            out_name = out_name.replace('#', str(zlist[0])+'-'+str(zlist[-1]))
            out_name = outfolder+out_name+'_O'+str(len(poptguess[ifit])) + '_zpz' + str(xvalue_min) + '-' + str(xvalue_max) + '.txt'

            #print(poptguess[ifit])
            try:
                #popt = rITD_fit(fitfunc[ifit],mean_input_format,mean_col,sample_input_format,[zlist[i] for i in range(iz, iz+1)], pxlist, poptguess[ifit], xvalue_min, xvalue_max)
                popt = rITD_fit(fitfunc[ifit],mean_input_format,mean_col,sample_input_format,[zlist[i] for i in range(0, iz+1)], pxlist, poptguess[ifit], xvalue_min, xvalue_max)
                popt = [zlist[iz]*latsp] + popt
                z_rITDfit += [popt]
            except:
                pass
                
        
        np.savetxt(out_name, z_rITDfit, fmt='%.6e')


#print(pz4_pz5_On_real(1))
