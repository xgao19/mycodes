#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
import inspect
from scipy.optimize import curve_fit
#from statistics import *
from rITD_Numerical_Kernel_moments import *

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "Grid_Kernel" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################## Global parameters ######################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

alps0=0.24
#alps0=0.3
mu0=3.2 #(GeV)
#mu0=2 #(GeV)
PI=3.1415926
pi=3.1415926
fmGeV = 5.0676896
CF = 4/3
gammaE = 0.5772156649

def alpsmu(mu, alps0 = alps0, mu0=mu0*5.0676896):
    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Auxiliary functions ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def Cn(z,n, mu = mu0*5.0676896,alps = alps0):

    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

def CnC0NLO(mu,z,n,k=0):
    #if abs(mu/5.0676896 - 3.2) < 0.1:
    #    fx0 = Cn(z,n, alps=alpsmu(mu/2))/Cn(z,0, alps=alpsmu(mu/2))
    #    fx1 = Cn(z,n, alps=alpsmu(mu))/Cn(z,0, alps=alpsmu(mu))
    #    fx2 = Cn(z,n, alps=alpsmu(mu*2))/Cn(z,0, alps=alpsmu(mu*2))
    #    #return fx0,fx1,fx2
    #    return 0, fx1, 0
    return Cn(z,n, mu, alps=alpsmu(mu))/Cn(z,0, mu, alps=alpsmu(mu))

def CnC0NLO_EP(mu,z,n,k=0):
    #if abs(mu/5.0676896 - 3.2) < 0.1:
    #    fx0 = Cn(z,n, alps=alpsmu(mu/2))/Cn(z,0, alps=alpsmu(mu/2))
    #    fx1 = Cn(z,n, alps=alpsmu(mu))/Cn(z,0, alps=alpsmu(mu))
    #    fx2 = Cn(z,n, alps=alpsmu(mu*2))/Cn(z,0, alps=alpsmu(mu*2))
    #    #return fx0,fx1,fx2
    #    return 0, fx1, 0
    return 1+Cn(z,n, mu, alps=alpsmu(mu))-Cn(z,0, mu, alps=alpsmu(mu))

def CnC0evo(mu,z,n,k=0):
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)-5/2
    part1 = 1 + alpsmu(muz0)*CF/(2*pi)*part0
    part2 = (alpsmu(muz0)/alpsmu(mu))**Bn
    return part1*part2

def CnC0RsLL(mu,z,n,k=0):
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)-5/2+2*(np.log(n)+gammaE)**2+pi*pi/3
    part1 = 1 + alpsmu(muz0)*CF/(2*pi)*part0
    part2 = np.exp(alpsmu(muz0)*CF/(2*pi)*(-2*(np.log(n)+gammaE)**2-pi*pi/3))
    part3 = (alpsmu(muz0)/alpsmu(mu))**Bn
    return part1*part2*part3

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ###################### NNLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
'''
Find the kernels in rITD_Numerical_Kernel.py
'''

def CnC0NNLO(mu,z,n,k=0):
    return CnNNLO(z, n, mu, alpsmu(mu))/CnNNLO(z, 0, mu, alpsmu(mu))

def CnC0NNLO_EP(mu,z,n,k=0):
    return CnC0NNLO_EP_numerical(z, n, mu, alpsmu(mu))

def CnC0NNLO_EVO(mu,z,n,k=0):
    return CnC0NNLO_EVO_numerical(z, n, mu, alpsmu(mu))


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def factorial(n):
    n_factorial = 1
    for j in range(1, n+1):
        n_factorial *= j
    return n_factorial

def ritd_NLO_Pzn_real(z, pz, mass, mu, nmax, moms, k=0, pznm=0, kernel_list=[]):
    ritdsum = 1
    c = mass*mass/(4*pz*pz)
    kernel_list = []
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel = CnC0NLO(mu,z,n,k)
            kernel_list += [kernel]
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j
        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
    return ritdsum
def ritd_RsLL_Pzn_real(z, pz, mass, mu, nmax, moms, k=0):
    ritdsum = 1
    c = mass*mass/(4*pz*pz)
    #print(mass, pz)
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j
        ritdsum += CnC0RsLL(mu,z,n,k)*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        #print(z,pz,mu,CnC0Rs2(mu,z,2,k),n,moms[n])
    return ritdsum

def ritd_NLO_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = 1
    ritdsum_down = 1
    c = mass*mass/(4*pz*pz)
    #print(c, mass, pz)
    #print(mu,z)

    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel = CnC0NLO(mu,z,n,k)
            kernel_list += [kernel]
    #print(z,kernel_list)

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j

        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

    return ritdsum/ritdsum_down

def ritd_NLOEP_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = 1
    ritdsum_down = 1
    c = mass*mass/(4*pz*pz)
    #print(c, mass, pz)

    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel = CnC0NLO_EP(mu,z,n,k)
            kernel_list += [kernel]
    #print(kernel_list)

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j

        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

    return ritdsum/ritdsum_down

def ritd_NNLO_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = 1
    ritdsum_down = 1
    c = mass*mass/(4*pz*pz)
    kernel_list = []

    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel = CnC0NNLO(mu,z,n,k)
            kernel_list += [kernel]

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j

        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

    return ritdsum/ritdsum_down

def ritd_NNLOEP_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = 1
    ritdsum_down = 1
    c = mass*mass/(4*pz*pz)
    kernel_list = []

    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel = CnC0NNLO_EP(mu,z,n,k)
            kernel_list += [kernel]

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j

        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

    return ritdsum/ritdsum_down

def ritd_NNLOevo_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = 1
    ritdsum_down = 1
    c = mass*mass/(4*pz*pz)
    kernel_list = []

    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel = CnC0NNLO_EVO(mu,z,n,k)
            kernel_list += [kernel]

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j

        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

    return ritdsum/ritdsum_down

'''
def ritd_NNLOevo_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = [1,1,1]
    ritdsum_down = [1,1,1]
    c = mass*mass/(4*pz*pz)
    kernel_list = []

    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel0, kernel1, kernel2 = CnC0NNLOevo(mu,z,n,k)
            kernel_list += [[kernel0, kernel1, kernel2]]
    
    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        #print(i)
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j
        ritdsum[0] += kernel_list[i][0]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down[0] += kernel_list[i][0]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

        ritdsum[1] += kernel_list[i][1]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down[1] += kernel_list[i][1]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

        ritdsum[2] += kernel_list[i][2]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down[2] += kernel_list[i][2]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

        ritdsumratio = [ritdsum[0]/ritdsum_down[0],ritdsum[1]/ritdsum_down[1],ritdsum[2]/ritdsum_down[2]]
    #return ritdsumratio[1],0.5*(ritdsumratio[0]-ritdsumratio[2]),kernel_list
    return ritdsumratio[1],0.5*(ritdsumratio[0]-ritdsumratio[2]),kernel_list


def ritd_NLLevo_Pzn_pznm_real(z, pz, mass, mu, nmax, moms,k=0, pznm=0, kernel_list=[]):
    ritdsum = [1,1,1]
    ritdsum_down = [1,1,1]
    c = mass*mass/(4*pz*pz)
    kernel_list = []
    
    # collect the kernel list, label 1 is the mean value mu, 0 and 2 are the mu/2 and mu*2
    if len(kernel_list) == 0:
        kernel_list = [1]
        for i in range(1, int(nmax/2)+1):
            n = 2*i
            kernel0, kernel1, kernel2 = CnC0NLLevo(mu,z,n,k)
            kernel_list += [[kernel0, kernel1, kernel2]]
    
    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        #print(i)
        for j in range(1, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j
        ritdsum[0] += kernel_list[i][0]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down[0] += kernel_list[i][0]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

        ritdsum[1] += kernel_list[i][1]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down[1] += kernel_list[i][1]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

        ritdsum[2] += kernel_list[i][2]*( (-1)**i*(z*pz)**n/factorial(n) * n_TMC * moms[n])
        ritdsum_down[2] += kernel_list[i][2]*( (-1)**i*(z*pznm)**n/factorial(n) * n_TMC * moms[n])

        ritdsumratio = [ritdsum[0]/ritdsum_down[0],ritdsum[1]/ritdsum_down[1],ritdsum[2]/ritdsum_down[2]]
    return ritdsumratio[1],0.5*(ritdsumratio[0]-ritdsumratio[2]),kernel_list
'''

#print(CnC0NLO(2*5.0676896,0.3,2,k=0))