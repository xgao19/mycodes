import numpy as np

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

'''
def C0(z,n, mu = 2*5.0676896,alps = 0.25,latsp = 0.04):
    CF = 4/3
    #latsp = 0.04
    #alps = 0.25
    gammaE = 0.5772156649
    pi = 3.14159
    #mu = 3.2*5.0676896  # fm^-1
    z = z * latsp
    if z == 0:
        return 1
    else:
        return 1 + alps*CF/(2*pi)*(3/2*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+5/2)
'''

def Cn(z,n, mu = 3.2*5.0676896,alps = 0.25,latsp = 0.04):

    CF = 4/3
    #latsp = 0.04
    
    gammaE = 0.5772156649
    pi = 3.14159
    #mu = 3.2*5.0676896  # fm^-1
    z = z * latsp
    #alps = 0.25
    
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))
