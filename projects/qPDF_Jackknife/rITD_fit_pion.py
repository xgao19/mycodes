#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
#from tools import *
import inspect
from scipy.optimize import curve_fit
#from modelfuncs import *
from statistics import *

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "tools.py"
    5), Need "Grid_Kernel" for NNLO and so on.

'''

################################################
############## Global parameters  ##############
################################################

alps0=0.24
mu0=3.2 #(GeV)
PI=3.1415926

################################################
############# Auxiliary functions  #############
################################################
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])
def Cn(z,n, mu = mu0*5.0676896,alps = alps0):
    CF = 4/3
    gammaE = 0.5772156649
    pi = 3.14159
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

# mu (fm)
def alpsmu(mu, alps0 = alps0, mu0=mu0*5.0676896):
    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
#print(alpsmu(mu0/2*5.0676896),alpsmu(mu0*5.0676896),alpsmu(mu0*2*5.0676896))
def CnC0evo(mu,z,n,k=4):
    CF = 4/3
    gammaE = 0.5772156649
    pi = 3.14159
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)-5/2
    part1 = 1 + alpsmu(muz0)*CF/(2*pi)*part0
    part2 = (alpsmu(muz0)/alpsmu(mu))**Bn
    return part1*part2
def CnC0Rs2(mu,z,n,k=4):
    CF = 4/3
    gammaE = 0.5772156649
    pi = 3.14159
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n)-3/2)*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)-5/2+2*(np.log(n)+gammaE)**2+pi*pi/3
    part1 = 1 + alpsmu(muz0)*CF/(2*pi)*part0
    part2 = np.exp(alpsmu(muz0)*CF/(2*pi)*(-2*(np.log(n)+gammaE)**2-pi*pi/3))
    part3 = (alpsmu(muz0)/alpsmu(mu))**Bn
    return part1*part2*part3
#print(CnC0evo(2*5.0676896, 0.1, 2))

################################################
############### model functions  ###############
################################################

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def rioffe_ope_momN(xz, *moms_list):
    z,x = xz
    ritd = 1
    for i in range(0, len(moms_list)):
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        ritd += Cn(z,(i+1)*2)/Cn(z,0) * (-x*x)**(i+1)/i_factorial * moms_list[i]
    return ritd

def rioffe_ope_momN_constrained(xz, *exp_list):
    z,x = xz
    ritd = 1
    momN = 0
    for i in range(0, len(exp_list)):
        momN += np.exp(-exp_list[i])
    for i in range(0, len(exp_list)):
        if i > 0:
            momN -= np.exp(-exp_list[i-1])   
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        ritd += Cn(z,(i+1)*2)/Cn(z,0) * (-x*x)**(i+1)/i_factorial * momN
    return ritd

def rioffe_ope_momN_constrained_pznm(zx, pznm, *exp_list):
    z,x = zx
    ritd = 1
    ritd_nm = 1
    momN = 0
    for i in range(0, len(exp_list)):
        momN += np.exp(-exp_list[i])
    for i in range(0, len(exp_list)):
        if i > 0:
            momN -= np.exp(-exp_list[i-1])   
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        ritd += Cn(z,(i+1)*2)/Cn(z,0) * (-x*x)**(i+1)/i_factorial * momN
        ritd_nm += Cn(z,(i+1)*2)/Cn(z,0) * (-z*pznm*z*pznm)**(i+1)/i_factorial * momN
        #print(ritd,ritd_nm)
    return ritd/ritd_nm

def make_ritd_constrained_pznm(pznm):
    def rioffe_ope_momN_constrained(zx, *exp_list):
        return rioffe_ope_momN_constrained_pznm(zx, pznm, *exp_list)
    return rioffe_ope_momN_constrained

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Mixed matching kernel with evolution and LL resummation
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def rioffe_opeRsMix_momN_constrained(xz, *exp_list):
    z,x = xz
    ritd = 1
    momN = 0
    for i in range(0, len(exp_list)):
        momN += np.exp(-exp_list[i])
    for i in range(0, len(exp_list)):
        if i > 0:
            momN -= np.exp(-exp_list[i-1])   
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        if i == 0:
            ritd += CnC0evo(mu0*5.0676896,z,(i+1)*2) * (-x*x)**(i+1)/i_factorial * momN
        else:
            ritd += CnC0Rs2(mu0*5.0676896,z,(i+1)*2) * (-x*x)**(i+1)/i_factorial * momN
        #print((i+1)*2,Cn(z,(i+1)*2)/Cn(z,0),CnC0evo(mu0*5.0676896,z,(i+1)*2), CnC0Rs2(mu0*5.0676896,z,(i+1)*2))
    return ritd

def rioffe_opeRsMix_momN_constrained_pznm(zx, pznm, *exp_list):
    z,x = zx
    ritd = 1
    ritd_nm = 1
    momN = 0
    #print(exp_list)
    for i in range(0, len(exp_list)):
        momN += np.exp(-exp_list[i])
    for i in range(0, len(exp_list)):
        if i > 0:
            momN -= np.exp(-exp_list[i-1])   
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        if i == 0:
            ritd += CnC0evo(mu0*5.0676896,z,(i+1)*2) * (-x*x)**(i+1)/i_factorial * momN
            ritd_nm += CnC0evo(mu0*5.0676896,z,(i+1)*2) * (-z*pznm*z*pznm)**(i+1)/i_factorial * momN
        else:
            ritd += CnC0Rs2(mu0*5.0676896,z,(i+1)*2) * (-x*x)**(i+1)/i_factorial * momN
            ritd_nm += CnC0Rs2(mu0*5.0676896,z,(i+1)*2) * (-z*pznm*z*pznm)**(i+1)/i_factorial * momN
        #print(ritd,ritd_nm)
    return ritd/ritd_nm

def make_ritdRsMix_constrained_pznm(pznm):
    #a = [0.1]
    #print(rioffe_opeRsMix_momN_constrained_pznm((0.1,1), 4, *a))
    def rioffe_opeRsMix_momN_constrained(zx, *exp_list):
        return rioffe_opeRsMix_momN_constrained_pznm(zx, pznm, *exp_list)
    return rioffe_opeRsMix_momN_constrained


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Matching kernel with LL resummation only
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def rioffe_opeRsLL_momN_constrained(xz, *exp_list):
    z,x = xz
    ritd = 1
    momN = 0
    for i in range(0, len(exp_list)):
        momN += np.exp(-exp_list[i])
    for i in range(0, len(exp_list)):
        if i > 0:
            momN -= np.exp(-exp_list[i-1])   
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        ritd += CnC0Rs2(mu0*5.0676896,z,(i+1)*2) * (-x*x)**(i+1)/i_factorial * momN
        #print((i+1)*2,Cn(z,(i+1)*2)/Cn(z,0), CnC0Rs2(mu0*5.0676896,z,(i+1)*2))
    return ritd

def rioffe_opeRsLL_momN_constrained_pznm(zx, pznm, *exp_list):
    z,x = zx
    ritd = 1
    ritd_nm = 1
    momN = 0
    #print(exp_list)
    for i in range(0, len(exp_list)):
        momN += np.exp(-exp_list[i])
    for i in range(0, len(exp_list)):
        if i > 0:
            momN -= np.exp(-exp_list[i-1])   
        i_factorial = 1
        for j in range(1, (i+1)*2+1):
            i_factorial *= j
        ritd += CnC0Rs2(mu0*5.0676896,z,(i+1)*2) * (-x*x)**(i+1)/i_factorial * momN
        ritd_nm += CnC0Rs2(mu0*5.0676896,z,(i+1)*2) * (-z*pznm*z*pznm)**(i+1)/i_factorial * momN
        #print(ritd,ritd_nm)
    return ritd/ritd_nm

def make_ritdRsLL_constrained_pznm(pznm):
    #a = [0.1]
    #print(rioffe_opeRsMix_momN_constrained_pznm((0.1,1), 4, *a))
    def rioffe_opeRsLL_momN_constrained(zx, *exp_list):
        return rioffe_opeRsLL_momN_constrained_pznm(zx, pznm, *exp_list)
    return rioffe_opeRsLL_momN_constrained




def moms_exp(exp_list):
    moms_list = []
    for i in range(0, len(exp_list)):
        moms_i = 0
        for j in range(i, len(exp_list)):
            moms_i += np.exp(-exp_list[j])
        moms_list += [moms_i]
    return moms_list

def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)


class rif_func_list:

    rif_momN = rioffe_ope_momN
    rif_momN_cst = rioffe_ope_momN_constrained
    rifRsMix_momN_cst = rioffe_opeRsMix_momN_constrained
    rifRsLL_momN_cst = rioffe_opeRsLL_momN_constrained
    poly2 = polynomial2


'''
fitting procedure
'''
def rITD_fit(fitfunc_name,latsp, input_mean_format,mean_col,sample_format,zlist, pxlist, pznm, Ns,poptstart=None, xvaluemin=-1e50, xvaluemax=1e50):

    if pznm == 0:
        fitfunc = getattr(rif_func_list, fitfunc_name)
    else:
        pznm_fm = pznm*2*3.14259/(Ns*latsp)
        if 'RsMix' in fitfunc_name:
            fitfunc = make_ritdRsMix_constrained_pznm(pznm_fm)
        elif 'RsLL' in fitfunc_name:
            fitfunc = make_ritdRsLL_constrained_pznm(pznm_fm)
        else:
            fitfunc = make_ritd_constrained_pznm(pznm_fm)

    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)
    
    # jackknife samples data collection
    ritd_data = [] # list[z][px][mu]
    x1 = []
    ritd_mean_data = []
    for ipx in range(0, len(pxlist)):

        ipx_mean_name = input_mean_format.replace('*', str(pxlist[ipx]))
        ipx_ritd_mean_data_load = np.loadtxt(ipx_mean_name)

        ipx_in_format = sample_format.replace('*', str(pxlist[ipx]))
        ipx_ritd_data = []
        x1z = []
        #print(iz_ritd_mean_data)
        iz_ritd_mean_data = []

        for iz in range(0, len(zlist)):

            ipx_iz_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            ipx_iz_ritd_data = np.loadtxt(ipx_iz_in_name)
            izpx = pxlist[ipx]*zlist[iz]*2*3.14259/Ns

            #print(izpx)
            if izpx > xvaluemin and izpx < xvaluemax:
                iz_ritd_mean_data += [ipx_ritd_mean_data_load[zlist[iz]][mean_col]]
                ipx_iz_ritd_data = [izpx] + list(ipx_iz_ritd_data)
                ipx_ritd_data += [ipx_iz_ritd_data]
                x1z += [zlist[iz]*latsp]
        if len(iz_ritd_mean_data) > 0:
            ritd_mean_data += iz_ritd_mean_data
            x1 += x1z
            ritd_data += ipx_ritd_data

    #print(iz_ritd_mean_data)
    
    cov_matrix = jackksample_cov(ritd_mean_data,ritd_data,stderr=False)
    #print(cov_matrix)
    #np.savetxt("/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_64c64_data/analysis/c3ptfit_jackk_ritd/jointfit/cov_z3.txt",cov_matrix)
    cov_matrix = [np.sqrt(cov_matrix[i][i]) for i in range(0, len(cov_matrix))]


    # fitting loop
    x2 = np.array([ritd_data[j][0] for j in range(0, len(ritd_data))])
    x = (x1, x2)
    #print('ttt',x2)
    y = ritd_mean_data

    #print(poptstart)
    poptmean, popv = curve_fit(fitfunc, x, y, p0=poptstart,sigma=cov_matrix)
    xylist = [[x1[i], x2[i],y[i],cov_matrix[i]] for i in range(0, len(x1))]
    chisq = (chisq_value_stderr(xylist, fitfunc, poptmean))/(len(x1)-len(poptmean))
    poptmean = moms_exp(poptmean)
    print('Popt:',poptmean,'chisq:',chisq,'\n')
    poptmean = list(poptmean) + [chisq]
    popterr = np.zeros(len(poptmean))

    for i in range(0, njackk):

        y = [ritd_data[j][i+1] for j in range(0, len(ritd_data))]
        popt,popv = curve_fit(fitfunc, x, y, poptstart, sigma=cov_matrix)

        xylist = [[x1[i], x2[i],y[i],cov_matrix[i]] for i in range(0, len(y))]
        chisq = (chisq_value_stderr(xylist, fitfunc, popt))/(len(x1)-len(popt))
        #popt[0] = popt[0]/popt[-1]
        popt = moms_exp(popt)
        popt = list(popt) + [chisq]

        for ip in range(0, len(popt)):
            popterr[ip] += (popt[ip]-poptmean[ip]) * (popt[ip]-poptmean[ip])

    meanerr = []
    for i in range(0, len(popt)):
        meanerr += [poptmean[i], np.sqrt(popterr[i]*(njackk-1)/njackk)]

    return meanerr



'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    fitfunc = []
    poptguess = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'zrange' == line[0] :
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            print('pznm:',pznm)

        elif 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])
        
        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

        elif 'fitfunc' in line[0]:
            fitfunc += [line[1]]
            poptguess += [list(map(float, line[2:]))]
    
    for ifit in range(0, len(fitfunc)):

        z_rITDfit = []
        
        for iz in range(0, len(zlist)):
            print("iz:",iz)

            out_name = describe.replace('*', str(pxlist[0])+'-'+str(pxlist[-1]))
            out_name = out_name.replace('#', str(zlist[0])+'-'+str(zlist[-1]))
            #out_name = outfolder+out_name+'_'+str(fitfunc[ifit]) + '_zpz' + str(xvalue_min) + '-' + str(xvalue_max) + '.txt'
            out_name = outfolder+out_name+'_'+str(fitfunc[ifit]) + '.txt'

            #print(poptguess[ifit])
            try:
                #popt = rITD_fit(fitfunc[ifit],mean_input_format,mean_col,sample_input_format,[zlist[i] for i in range(iz, iz+1)], pxlist, poptguess[ifit], xvalue_min, xvalue_max)
                popt = rITD_fit(fitfunc[ifit],latsp,mean_input_format,mean_col,sample_input_format,[zlist[i] for i in range(0, iz+1)], pxlist, pznm, Ns, poptguess[ifit], xvalue_min, xvalue_max)
                popt = [zlist[iz]*latsp] + popt
                z_rITDfit += [popt]
            except:
                pass
                
        
        np.savetxt(out_name, z_rITDfit, fmt='%.6e')
