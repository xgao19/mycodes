#!/usr/bin/env python3
import numpy as np

ReadFile24D = '/Users/Xiang/Desktop/0study/research/lattice/data/DMW-qpdf/analysis_qpdf/24D_rITD/pion_fm/results/24D.pion.ritd.1hyp.g8.sumfit.nsk2.pxnm.pz*.qx0qy0qz0.X0-12.real'
ReadFile48I = '/Users/Xiang/Desktop/0study/research/lattice/data/DMW-qpdf/analysis_qpdf/48I_rITD/pion_fm/results/48I.pion.ritd.1hyp.g8.sumfit.nsk2.pxnm.pz*.qx0qy0qz0.X0-12.real'
ReadFile48c64a06 = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_48c64_data/gpd_analysis/rITD/qx0qy0qz0_boots/pion_fm/results/48c64.pion.ritd.1hyp.g8.c3ptfit.nst2.nsk2.pxnm.pz*.qx0qy0qz0.X0-15.real'
ReadFile64c64a04 = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_64c64_data/gpd_analysis/rITD/qx0qy0qz0_boots/pion_fm/results/64c64.pion.ritd.1hyp.g8.c3ptfit.nst2.nsk3.pxnm.pz*.qx0qy0qz0.X0-32.real'
ReadFile64c64a076 = '/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_qpdf/rITD/g8/qx0qy0qz0_boots/pion_fm/results/64c64.pion.ritd.1hyp.g8.c3ptfit.nst2.nsk3.pxnm.pz*.qx0qy0qz0.X0-32.real'
outfolder = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/data/rITD_data_z_pz/'

Ns24D = 24
Ns48I = 48
Ns48c64a06 = 48
Ns64c64a04 = 64
Ns64c64a076 = 64

latsp24D = 0.194416
latsp48I = 0.114063
latsp48c64a06 = 0.06
latsp64c64a04 = 0.04
latsp64c64a076 = 0.076

Pi=3.14259
fmGeV=5.0676896
pznm = 1
pzlist = [2,3,4]
ReadFile = ReadFile24D
Ns=Ns24D
latsp=latsp24D
outname = outfolder + 'DWF24D_rITD_z#_pz.txt'

ReadData = []
for ipz in range(0, len(pzlist)):
    filename = ReadFile.replace('pxnm', 'pxnm'+str(pznm))
    filename = filename.replace('*',str(pzlist[ipz]))
    ReadData += [np.loadtxt(filename)]
print(np.shape(ReadData))

SaveData = []
for iz in range(0, len(ReadData[0])):
    iz_SaveData = []
    for ipx in range(0, len(ReadData)):
        iz_ipx_Savedata = [pzlist[ipx]*2*Pi/(Ns*latsp)/fmGeV,ReadData[ipx][iz][1],ReadData[ipx][iz][2]]
        iz_SaveData += [iz_ipx_Savedata]
        print(iz_ipx_Savedata)
    SaveName = outname.replace('#', str(iz))
    np.savetxt(SaveName, iz_SaveData)
    SaveData += [iz_SaveData]
print(np.shape(SaveData))

