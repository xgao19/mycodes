#!/usr/bin/env python3

#from folddata import *
from modelfuncs import *
from makefuncs import *
from tools import *
from statistics import *
from c3c2tools import *
#from fine_PseudoPDF import *
import modelfuncs
import os
import csv
import math
import traceback 
import numpy as np
import inspect
from scipy.optimize import curve_fit
from scipy.special import gamma
from scipy.optimize import fsolve

'''
print('\n##############################################################################\n')
print('This program is used for curve chisq fit with std error or covariance.')
print('f/nf means if you want to fold the data in half.')
print('Please arrange your input file as the following format:')
print('@1 fit all possible x range:')
print('(1.1): dataset.csv f/nf allx  stderr','--std error')
print('(1.2): dataset.csv f/nf allx  covar','--covariance')
print('@2 particular x range with jackknife analysis:')
print('(2.1): dataset.csv f/nf jackk  stderr  xminl xminr  xmax  blocksize','--std error')
print('(2.2): dataset.csv f/nf jackk  covar  xminl xminr  xmax  blocksize','--covariance')
print('@3 particular x range with bootstrap analysis:')
print('(3.1): dataset.csv f/nf boots  stderr  xminl xminr  xmax  blocksize  npick','--std error')
print('(3.2): dataset.csv f/nf boots  covar  xminl xminr  xmax  blocksize  npick','--covariance')
print('\n##############################################################################\n')
'''

#------------------------------------------------------------#
####################### fit functions ########################
#------------------------------------------------------------#

####################### fit with covariance ########################
# caculate all possible tmin-tmax combination
def fitcov_all(t_func, t_dataset, t_paramguess,t_option):
    m_poptlist = []    
    for m_xmin in range(0,len(t_dataset)-1):
        for m_xmax in range(m_xmin+1,len(t_dataset)):
            try:
                m_subdata = t_dataset[m_xmin:m_xmax+1]
                m_subx = [m_row[0] for m_row in m_subdata]
                m_suby = list(map(np.mean,np.delete(m_subdata,0,1))) #
                if t_option == 'stderr':          # fit with std error       
                        m_yvar = list(map(np.var,np.delete(m_subdata,0,1)))
                        m_subcovar = np.diag(m_yvar)
                elif t_option == 'covar':        # fit with covariance
                        m_subcovar = np.cov(np.delete(m_subdata,0,1)) 
                m_popt,m_pcov = curve_fit(t_func, m_subx, m_suby, t_paramguess, m_subcovar)
                m_poptlist.append([m_xmin,m_xmax]+list(m_popt))
                print([m_xmin,m_xmax]+list(m_popt))
            except:
                traceback.print_exc()
    return np.array(m_poptlist)

# Jackknife fit at particular x range with covariance
def fitcov_Jackknife(t_func, t_dataset, t_paramguess, t_option, t_xmin, t_xmax, t_blocksize):
    m_nblock = int((len(t_dataset[0])-1)/t_blocksize)
    # caculate the mean value first
    m_dataset = t_dataset[t_xmin:t_xmax+1]
    m_x = [m_row[0] for m_row in m_dataset]
    m_y = list(map(np.mean,np.delete(m_dataset,0,1)))
    if t_option == 'stderr':          # fit with std error       
        m_yvar = list(map(np.var,np.delete(m_dataset,0,1)))
        m_covar = np.diag(m_yvar)
    elif t_option == 'covar':        # fit with covariance
        m_covar = np.cov(np.delete(m_dataset,0,1))
        #print(m_covar) 
    #print(m_x)
    #print(m_y)
    m_popt, m_pcov = curve_fit(t_func, m_x, m_y, t_paramguess, m_covar)
    m_Jackkerror = np.zeros((len(t_paramguess)))
    m_datasetT = np.transpose(m_dataset)
    for m_i in range(0,m_nblock):
        m_subdata = np.transpose(list(m_datasetT[:m_i*t_blocksize+1])+list(m_datasetT[(m_i+1)*t_blocksize+1:]))
        m_subx = [m_row[0] for m_row in m_subdata]
        m_suby = list(map(np.mean,np.delete(m_subdata,0,1)))
        if t_option == 'stderr':          # fit with std error       
                m_yvar = list(map(np.var,np.delete(m_subdata,0,1)))
                m_subcovar = np.diag(m_yvar)
        elif t_option == 'covar':        # fit with covariance
                m_subcovar = np.cov(np.delete(m_subdata,0,1)) 
        #print(m_subx,'\n',m_suby,'\n',m_subcovar)
        m_subpopt, m_subpcov = curve_fit(t_func, m_subx, m_suby, t_paramguess, m_subcovar)
        m_Jackkerror += (m_subpopt-m_popt)**2
    m_Jackkerror=(m_Jackkerror*[((m_nblock-1.0)/m_nblock)])**0.5
    m_popt=np.append([m_popt],[m_Jackkerror],axis=0)
    return m_popt.T

# Bootstrap fit at particular x range with covariance
def fitcov_Bootstrap(t_func, t_dataset, t_paramguess, t_option, t_xmin, t_xmax, t_blocksize, t_npick):
    m_dataset = t_dataset[t_xmin:t_xmax+1]
    m_nblock = int((len(t_dataset[0])-1)/t_blocksize)
    m_bootsvalue = []
    m_datasetT = np.transpose(m_dataset)
    #print(np.shape(m_dataset))
    for m_i in range(0,int(t_npick)):
        m_bootsimple = list(map(int,np.random.uniform(0,m_nblock,m_nblock)))
        m_subdata = np.array([m_datasetT[0]])
        for m_pick in m_bootsimple:
                m_subdata = np.append(m_subdata,m_datasetT[m_pick+1:m_pick+t_blocksize+1],axis=0)
        m_subdata = np.transpose(m_subdata)
        m_subx = [m_row[0] for m_row in m_subdata]
        m_suby = list(map(np.mean,np.delete(m_subdata,0,1)))
        if t_option == 'stderr':          # fit with std error       
                m_yvar = list(map(np.var,np.delete(m_subdata,0,1)))
                m_subcovar = np.diag(m_yvar)
        elif t_option == 'covar':        # fit with covariance
                m_subcovar = np.cov(np.delete(m_subdata,0,1)) 
        m_subpopt, m_subpcov = curve_fit(t_func, m_subx, m_suby, t_paramguess, m_subcovar)
        m_bootsvalue.append(list(m_subpopt))
    m_low16 = int(np.ceil(0.16*t_npick))
    m_high16 = int(np.floor(0.84*t_npick))
    m_mid = int(0.5*t_npick)
    m_popt = []
    for m_i in range(0,len(m_bootsvalue[0])):
        m_bootsvalue = sorted(m_bootsvalue,key=(lambda x:x[m_i]))
        m_bootsvalue = np.array(m_bootsvalue)
        m_ipopt = np.array([m_bootsvalue[m_mid]])
        m_ierrorn = np.array([m_bootsvalue[m_mid]-m_bootsvalue[m_low16]])
        m_ierrorp = np.array([m_bootsvalue[m_high16]-m_bootsvalue[m_mid]])
        m_popt += [[m_ipopt[0][m_i],m_ierrorn[0][m_i],m_ierrorp[0][m_i]]]
    m_popt = np.array(m_popt)
    return m_popt

# combination of fit functions
def chisqfit(t_func, t_command, t_outputfolder):

        # split the filename
        m_datafile = t_command[0].split("/")[-1]

        # read the dataset first
        m_csvfile = open(t_command[0],'r')
        m_reader = csv.reader(m_csvfile)
        m_csvdata = []
        for m_item in m_reader:
                if m_reader.line_num == 1:           # get rid of the first footnote line
                        continue
                m_csvdata.append(list(map(float,m_item)))
        m_csvfile.close()

        if str(t_command[1]) == 'f':
                m_csvdata = folddat(m_csvdata)
        # choose what to do here

        if t_command[2] == 'allx':  #fit all 

                m_param = t_command[3:]
                t_command = t_command[:3]
                m_param = list(map(float,m_param))
                m_output = fitcov_all(t_func,m_csvdata,m_param,t_command[3])
                m_outfile = t_outputfolder + '/' + m_datafile + '_' + str(t_command[3]) + '_chisqfit_allx.txt'
                np.savetxt(m_outfile,m_output)

        elif t_command[2] == 'jackk':

                m_param = t_command[8:]
                m_param = list(map(float,m_param))
                erroption = t_command[3]
                xminl = int(t_command[4])
                xminr = int(t_command[5])
                xmax = int(t_command[6])
                blocksize = int(t_command[7])

                output = []
                for xmin in range(xminl,xminr+1):
                        try:
                                suboutput = fitcov_Jackknife(t_func,m_csvdata,m_param,erroption,xmin,xmax,blocksize)
                                popt = (suboutput.T)[0]
                                dof = xmax - xmin + 1 - len(m_param)
                                chisq = chisq_value(m_csvdata, t_func, popt, xmin, xmax)/dof
                                aicc = aicc_value(m_csvdata, t_func, popt, xmin, xmax)
                                line = [xmin, xmax] + list(suboutput.flatten()) + [chisq] + [aicc]
                                output += [line]
                        except:
                                traceback.print_exc()
                #print(output)
                outfile = open(t_outputfolder + '/Jackkfit.txt','a')
                outfile.write(t_func.__name__+str(t_command)+'\n')
                np.savetxt(outfile,output,fmt='%1.5e')
                outfile.close()

        elif t_command[2] == 'boots':

                m_param = t_command[9:]
                m_param = list(map(float,m_param))
                erroption = t_command[3]
                xminl = int(t_command[4])
                xminr = int(t_command[5])
                xmax = int(t_command[6])
                blocksize = int(t_command[7])
                samplenum = int(t_command[8])

                output = []
                for xmin in range(xminl,xminr+1):
                        try:
                                suboutput = fitcov_Bootstrap(t_func,m_csvdata,m_param,erroption,xmin,xmax,blocksize,samplenum)
                                popt = suboutput.T[0]
                                dof = xmax - xmin + 1 - len(m_param)
                                chisq = chisq_value(m_csvdata, t_func, popt, xmin, xmax)/dof
                                aicc = aicc_value(m_csvdata, t_func, popt, xmin, xmax)
                                line = [xmin, xmax] + list(suboutput.flatten()) + [chisq] + [aicc]
                                output += [line]
                        except:
                                traceback.print_exc()
                #print(output)
                outfile = open(t_outputfolder + '/Bootsfit.txt','a')
                outfile.write(t_func.__name__+str(t_command)+'\n')
                np.savetxt(outfile,output,fmt='%1.5e')
                outfile.close()

#------------------------------------------------------------#
#################### AICc fitting process ####################
#------------------------------------------------------------#
'''
The AICc fitting always need more than one fitting function.
------------------------------------------------------------
Thus, the parameter in the function should be formatted as:
fixarg, funcarg, poptguess.list
fixarg, funcarg, poptguess.list
...
fixarg, funcarg, poptguess.list
------------------------------------------------------------
------------------------------------------------------------
While the datatype = 'meanerr', dataset:
x fx ferr
...
------------------------------------------------------------
The dataset here is defaulted as 2D list, you can change the
reading procedure if needed.
Raw in dataset is: [t, conf1, conf2, ...]
By the way, fp means fitting model functions, and parameters. 
'''
def aiccfit(dataset, fp, failpercent=[None], datatype='conf', fitmethod='lm', cov_matrix = [None], gaussianfactor = 0.00, maxiter = 1, **kwargs):

        count = 0  # count for the fit model, used in the model fit loop
        fail = np.zeros([len(fp)])  # save the information if the model failed
        poptlist = []
        aicclist = []
        poptlenlist = []
        lenguess = []
        failpercent = np.array(failpercent)

        # don't need failpercent in this case
        if failpercent.all() == None:
                failpercent = np.zeros(len(fp))

        if datatype == 'conf':   # fit to the given data
                #getcontext().prec = 100
                x = [row[0] for row in dataset]
                y = list(map(np.mean,np.delete(dataset,0,1)))
                ycov_temp = np.cov(np.delete(dataset,0,1))
                if len(dataset) == 33 and len(dataset[0]) == 315:
                        print(ycov_temp)
                        np.savetxt('/Users/Xiang/Desktop/0study/research/lattice/data/test/cov.txt', ycov_temp)
                ycov = ycov_temp
                #print(np.linalg.det(ycov))
                #ycov = [np.sqrt(ycov_temp[i][i]) for i in range(0,len(ycov_temp))]
        elif datatype == 'meanerr':    # directly fit to the given mean value and errors
                if len(dataset[0]) == 3:  # fit to a one variable's curve
                        x = [row[0] for row in dataset]
                        y = [row[1] for row in dataset]
                        if (np.array(cov_matrix)).any() != None:
                                ycov = cov_matrix
                        else:
                                ycov = [row[2] for row in dataset]
                elif len(dataset[0]) == 4:  # fit to a two variable's curve
                        x = ([row[0] for row in dataset],[row[1] for row in dataset])
                        y = [row[2] for row in dataset]
                        if (np.array(cov_matrix)).any() != None:
                                ycov = cov_matrix 
                        else:
                                ycov = [row[3] for row in dataset]    
        else:
                print('Wrong datatype!!!')
        #print(x)
        priorline = fp[0]
        if 'prior' in priorline[0]:
                nprior = int((len(priorline) - 1)/2)
                prior = []
                for ip in range(0, nprior):
                        iprior = []
                        for jp in range(0, 2):
                                iprior += [float(priorline[ip*2+jp+1])]
                        prior += [iprior]
                #print(ycov,prior)
                x, y, ycov = prior_data(x,y,ycov,prior)
                fp = fp[1:]
                #print('\n\n\n\n\n\n\n\n',np.linalg.inv(ycov),x,y)

        for line in fp:

                #print('line:',line)
                fixarg = line[0]
                funcarg = line[1]
                func = getattr(modelfuncs,funcarg)
                poptguess = list(map(float,line[2:]))

                # define the fitting functions and initiate fit parameters
                if 'fix' in fixarg:
                        fitfunc, fix_num = makefixfunc(funcarg,fixarg,poptguess)
                elif 'hirarchy' in fixarg:
                        fitfunc = func
                        fix_num = 0
                        for ie in range(0, int(len(poptguess)/2)):
                                if ie != 0:
                                        poptguess[ie*2+1] = poptguess[ie*2-1] + np.log(poptguess[ie*2+1]-poptguess[ie*2-1])
                                poptguess[ie*2] = np.log(poptguess[ie*2])
                        #print(poptguess)
                else:
                        fitfunc = func
                        fix_num = 0

                #('Half failed, will not fit.')
                if failpercent[count] > 0.5:
                        fail[count] = 1
                        func = getattr(modelfuncs,line[1])
                        popt = 1000*np.ones(len(poptguess))
                        aicc,chisq = aicc_value(dataset, func, popt, 0, len(dataset)+1, datatype)
                        poptlenlist += [len(popt)]
                        aicclist += [aicc]
                        poptlist += [list(popt)+[1000]+[aicc]]
                        continue

                # preparing for fitting
                lenguess += [len(poptguess)] 

                # the fitting procedure
                # try "maxiteration" times around the parameter guess
                fitpoptstart = np.zeros(len(poptguess))
                for i in range(0 ,maxiter):
                        for j in range(len(poptguess)):
                                fitpoptstart[j] = np.random.normal(poptguess[j], gaussianfactor*np.fabs(poptguess[j]))
                        try:
                                #print(x,y,fitpoptstart)
                                #print(np.shape(ycov))
                                popt, pcov = curve_fit(fitfunc, x, y, fitpoptstart, ycov)
                                #print(popt)

                                if 'hirarchy' in fixarg:
                                        aicc,chisq = aicc_value(dataset, func, popt, 0, len(dataset)+1, datatype, fix_num=fix_num, cov_matrix = cov_matrix)
                                        for ie in range(0, int(len(poptguess)/2)):
                                                if ie != 0:
                                                        popt[ie*2+1] = poptguess[ie*2-1] + np.exp(popt[ie*2+1])
                                                popt[ie*2] = np.exp(popt[ie*2])
                                        #print(fixarg,popt)
                                        poptlenlist += [len(popt)]
                                        aicclist += [aicc]
                                        poptlist += [list(popt)+[chisq/(len(x)-(len(popt)-fix_num))]+[aicc]]
                                        break
                                elif (len(popt) <= 3 or fixarg != 'full' or (popt[1]-poptguess[1])/poptguess[1] > -0.8):
                                        aicc,chisq = aicc_value(dataset, func, popt, 0, len(dataset)+1, datatype, fix_num=fix_num, cov_matrix = cov_matrix)
                                        #print(chisq)
                                        poptlenlist += [len(popt)]
                                        aicclist += [aicc]
                                        #print(popt)
                                        poptlist += [list(popt)+[chisq/(len(x)-(len(popt)-fix_num))]+[aicc]] # the aicc value is pushed at the end of popt
                                        break
                                else:
                                        if i+1 == maxiter:
                                                #print(fixarg,func.__name__, (popt[1]-poptguess[1])/poptguess[1],popt,poptguess[1], 'bad fit')
                                                fail[count] = 1
                                                popt = 1000*np.ones(len(poptguess))*int(np.random.uniform(0,2))
                                                aicc = 1e50
                                                poptlenlist += [len(popt)]
                                                aicclist += [aicc]
                                                poptlist += [list(popt)+[1000]+[aicc]]
                                        continue
                        except:
                                #traceback.print_exc()
                                if i+1 == maxiter:
                                        fail[count] = 1
                                        popt = 1000*np.ones(len(poptguess))*int(np.random.uniform(0,2))
                                        aicc,chisq = aicc_value(dataset, func, popt, 0, len(dataset)+1, datatype, cov_matrix = cov_matrix)
                                        poptlenlist += [len(popt)]
                                        aicclist += [aicc]
                                        poptlist += [list(popt)+[chisq/(len(x)-(len(popt)-fix_num))]+[aicc]]
                count += 1

        aiccmin = min(aicclist)
        lenmax = max(poptlenlist)
        aiccweight = []

        for i in range(0, len(poptlist)):
                iweight = np.exp(0.5*(aiccmin - aicclist[i]))
                if iweight < 1e-50:
                        iweight = 1e-50
                aiccweight += [iweight]

        aiccave = []
        for i in range(0, lenmax):
                #print('\n',i)
                up = 0
                down = 0
                for j in range(0, len(poptlist)):
                        temp = -1
                        try:
                                temp = poptlist[j][i]
                        except:
                                pass
                        #print(temp)
                        if temp != -1:
                                up += temp * aiccweight[j]
                                down += aiccweight[j]
                                #print(np.shape(aiccave),up,down)
                if(down == 0):
                        print('down == 0, warning!')
                        print('check the aicc list:')
                        print(aicclist)
                        print(aiccweight)
                        aiccave += [1000*int(np.random.uniform(0,2))]
                else:
                        aiccave += [up/down]

        poptlist += [aiccave]
        #print(poptlist[0], fail)
        return poptlist,fail

#------------------------------------------------------------#
################## Summation fitting process #################
#------------------------------------------------------------#
'''
The Summation fitting need both c2pt and c3pt.
--------------------------------------------------------------
The c2dataset here is first to be fitted and extract E0
This fitting only contain full onestate and twostate fit.
The fitparams should be array like: 
[tmin, tmax, A0guess, E0guess, A1guess, E1guess]
--------------------------------------------------------------
The c2dataset is a 2D list, while the c3dataset is a 4D one.
c2dataset[t][conf], each raw: [t, conf1, conf2, ...]
c3dataset[z][ts][tau][conf], each raw: [tau, conf1, conf2, ..]
'''
def sumfit(Nt, c2dataset, c3dataset, zlist, tslist, fitparams, npick):

        tmin = int(fitparams[0])
        tmax = int(fitparams[1])
        fitparams = [['full', 'onestate'] + fitparams[2:4],['full', 'twostate'] + fitparams[2:6]]
        #print('\nfitparams:',fitparams)

        nconf = len(c2dataset[0])-1
        #print(nconf)
        bootsvalue = []
        c2datasetT = np.transpose(c2dataset)
        for i in range(0,int(npick)):

                print('bootstrap sample:',i,'/', npick)

                bootsample = list(map(int,np.random.uniform(0,nconf,nconf)))
                samplec2data = np.array([c2datasetT[0]])
                samplec3data = []
                for z in range(0, len(zlist)):
                        zsamplec3data = []
                        for ts in range(0, len(tslist)):
                                tszsamplec3data = []
                                for tau in range(0, len(c3dataset[z][ts])):
                                        tszsamplec3data += [[c3dataset[z][ts][tau][0]]]
                                zsamplec3data += [tszsamplec3data]
                        samplec3data += [zsamplec3data]
                #print(samplec3data)
                for pick in bootsample:
                        samplec2data = np.append(samplec2data,[c2datasetT[pick+1]],axis=0)
                        for z in range(0, len(zlist)):
                                for ts in range(0, len(tslist)):
                                        for tau in range(0, len(samplec3data[z][ts])):
                                                samplec3data[z][ts][tau] += [c3dataset[z][ts][tau][pick+1]]
                #print('\nsamplec3data shape:',np.shape(samplec3data))
                samplec2data = np.transpose(samplec2data)

                # fit the c2pt and extract A0, E0
                #subsamplec2data = folddat(samplec2data)
                subsamplec2data = samplec2data
                if tmax != 0 and tmax > tmin:
                        subsamplec2data = subsamplec2data[tmin:tmax+1]
                        c2fitresult, failpercent = aiccfit(subsamplec2data, fitparams,np.zeros([len(fitparams)]))
                        #print('\nc2pt fitting result:',c2fitresult[-1])
                        A0 = c2fitresult[-1][0]
                        E0 = c2fitresult[-1][1]
                        #print(fitparams)
                        print('A0,E0',A0,E0)
                        def c2mdf(t):
                                return A0*np.exp(-E0*(Nt-t))
                else:
                        def c2mdf(t):
                                return 0

                # pick specific ts for ratio caculation
                tsc2data = []
                for ts in tslist:
                        tsc2data += [samplec2data[ts]]

                # iterations to do summation fit for each z
                samplevalue = []
                for k in range(0, len(samplec3data)):
                        zc3data = samplec3data[k]
                        samplevalue += [samplesumfit(tsc2data,zc3data,c2mdf)]
                #print(i,samplevalue)
                bootsvalue += [samplevalue]   # bootsvalue[npick][z][value]

        # bootstrap error extraction
        low16 = int(np.ceil(0.16*npick))
        high16 = int(np.floor(0.84*npick))
        mid = int(0.5*npick)
        popt = []
        z = 0
        for i in range(0,len(bootsvalue[0])):        # range in the number of models   
                zboots = [bootsvalue[k][i][:] for k in range(0, npick)]
                zboots = np.transpose(zboots)
                ipopt = []
                for raw in zboots:       # range in the number of parameters in the model
                        raw = sorted(raw)
                        raw = np.array(raw)
                        subpopt = raw[mid]
                        suberrorn = raw[mid]-raw[low16]
                        suberrorp = raw[high16]-raw[mid]
                        ipopt += [subpopt, suberrorn, suberrorp]
                '''
                ipoptvalue = [ipopt[0], ipopt[3]]
                ipopty = ipopt[6:-1:3]
                ipoptyerr = np.array(ipopt[7:-1:3])
                fitlist = [[tslist[ix], ipopty[ix], ipoptyerr[ix]] for ix in range(0, len(tslist))]
                ichisq = chisq_value_stderr(fitlist, linearfunc, ipoptvalue)
                popt += [[zlist[z]]+ipopt+[ichisq]]
                '''

                popt += [[zlist[z]]+ipopt]
                z += 1
        #print(popt)
        return popt
'''
The Summation fitting need both c2pt and c3pt.
--------------------------------------------------------------
The c2dataset here is first to be fitted and extract E0
This fitting only contain full onestate and twostate fit.
The fitparams should be array like: 
[tmin, tmax, A0guess, E0guess, A1guess, E1guess]
--------------------------------------------------------------
The c2dataset is a 2D list, while the c3dataset is a 4D one.
c2dataset[px][t][conf], each raw: [t, conf1, conf2, ...]
c3dataset[px][z][ts][tau][conf], each raw: [tau, conf1, conf2, ..]
c2fitparams[px]
'''
def rioffe_zcurvefit(func, Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, blocksize):

        popt = []
        rioffemean = reducedIoffe_sumfit(Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, blocksize)
        fitfunc = func
        #print('\nrioffemean',rioffemean)
        for z in range(0, len(zlist)):

                zrioffemean = rioffemean[z]

                fitx = [zrioffemean[i][0] for i in range(1, len(zrioffemean))]
                fity = [zrioffemean[i][1] for i in range(1, len(zrioffemean))]
                fityerr = [zrioffemean[i][2] for i in range(1, len(zrioffemean))]
 
                try:
                        zpopt,zpopv = curve_fit(fitfunc,fitx,fity,sigma=fityerr,method='trf')
                except:
                        zpopt = np.zeros(fitfunc.__code__.co_argcount-1)
                print('z =',z,':',zpopt)
                popt += [zpopt]

        print('/n>>>> Meanbvalue caculation is done, now go to the error >>>>/n')
        # jackknife sample and error caculation
        # jackk_ioffe = []         # jackk_ioffe[nblock][z] = [z, pxvalues...]
        nconf = len(c2dataset[0][0])-1
        nblock = int(nconf/blocksize)
        jackk_fit_err = np.zeros((len(zlist), len(popt[0])))
        for i in range(0, nblock):
                print('\n>>>> error jackk sample:',i,'/', nblock,', delete',i*blocksize+1,(i+1)*blocksize)
                samplec3dataset = d5slicedelete(c3dataset,[i*blocksize+1,(i+1)*blocksize+1],4)
                samplec2dataset = d5slicedelete(c2dataset,[i*blocksize+1,(i+1)*blocksize+1],2)
                irioffemean = reducedIoffe_sumfit(Ns, Nt, samplec2dataset, samplec3dataset, pxlist, zlist, tslist, c2fitparams, blocksize,onlymean=True)
                for z in range(1, len(zlist)):
                        izrioffemean = irioffemean[z]
                        fitx = [izrioffemean[i][0] for i in range(1, len(izrioffemean))]
                        fity = [izrioffemean[i][1] for i in range(1, len(izrioffemean))]
                        fityerr = [zrioffemean[i][2] for i in range(1, len(zrioffemean))]  # use the same error as mean fit
                        try:
                                izpopt,izpopv = curve_fit(fitfunc,fitx,fity,sigma=fityerr,method='trf')
                        except:
                                izpopt = np.zeros(fitfunc.__code__.co_argcount-1)
                        for j in range(0, len(popt[z])):
                                jackk_fit_err[z][j] += (izpopt[j] - popt[z][j]) * (izpopt[j] - popt[z][j])
        for err_i in range(0, len(jackk_fit_err)):
                for err_j in range(1, len(jackk_fit_err[err_i])):
                        jackk_fit_err[err_i][err_j] = np.sqrt(jackk_fit_err[err_i][err_j]*(nblock-1)/nblock)

        #print('\n@@@@@ ioffemean & ioffeerr:')
        rioffe_curve_combi = []
        for z in range(len(popt)):
                zrioffe_curve_combi = [zlist[z]]
                for p in range(0, len(popt[z])):
                        zrioffe_curve_combi += [popt[z][p],jackk_fit_err[z][p]]
                #print(z_ioffecombi)
                rioffe_curve_combi += [zrioffe_curve_combi]
        return rioffe_curve_combi

'''
def rioffe_plyfit_solveab(func, Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, blocksize):

        popt = []
        abstart = [-0.4,1]
        rioffemean = reducedIoffe_sumfit(Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, blocksize)
        fitfunc = func
        #print('\nrioffemean',rioffemean)
        for z in range(0, len(zlist)):

                zrioffemean = rioffemean[z]

                fitx = [zrioffemean[i][0] for i in range(1, len(zrioffemean))]
                fity = [zrioffemean[i][1] for i in range(1, len(zrioffemean))]
                fityerr = [zrioffemean[i][2] for i in range(1, len(zrioffemean))]

                zpopt,zpopv = curve_fit(fitfunc,fitx,fity,sigma=fityerr)
                print('z =',zlist[z],':',zpopt)
                popt += [zpopt]
        mom2 = -popt[6][1]*2/(Cn(zlist[6],2)/Cn(zlist[6],0))
        findab = make_mom2findab(mom2)
        ab = fsolve(findab,abstart)
        print('mom2 and ab:',mom2,ab)

        print('/n>>>> Meanbvalue caculation is done, now go to the error >>>>/n')
        # jackknife sample and error caculation
        # jackk_ioffe = []         # jackk_ioffe[nblock][z] = [z, pxvalues...]
        nconf = len(c2dataset[0][0])-1
        nblock = int(nconf/blocksize)
        jackk_fit_err = [0,0]
        poptz5 = []
        for i in range(0, nblock):
                print('\n>>>> error jackk sample:',i,'/', nblock,', delete',i*blocksize+1,(i+1)*blocksize)
                samplec3dataset = d5slicedelete(c3dataset,[i*blocksize+1,(i+1)*blocksize+1],4)
                samplec2dataset = d5slicedelete(c2dataset,[i*blocksize+1,(i+1)*blocksize+1],2)
                irioffemean = reducedIoffe_sumfit(Ns, Nt, samplec2dataset, samplec3dataset, pxlist, zlist, tslist, c2fitparams, blocksize,onlymean=True)
                ipopt = []
                for z in range(1, len(zlist)):
                        izrioffemean = irioffemean[z]
                        fitx = [izrioffemean[i][0] for i in range(1, len(izrioffemean))]
                        fity = [izrioffemean[i][1] for i in range(1, len(izrioffemean))]
                        fityerr = [zrioffemean[i][2] for i in range(1, len(zrioffemean))]  # use the same error as mean fit
                        izpopt,izpopv = curve_fit(fitfunc,fitx,fity,sigma=fityerr)
                        ipopt += [zpopt]
                imom2 = -ipopt[6][1]*2/(Cn(zlist[6],2)/Cn(zlist[6],0))
                ifindab = make_mom2findab(imom2)
                iab = fsolve(ifindab,abstart)
                for ierr in range(0, len(jackk_fit_err)):
                        jackk_fit_err[ierr] += (iab[ierr]-ab[ierr])*(iab[ierr]-ab[ierr])
        for ierr in range(0, len(jackk_fit_err)):
                jackk_fit_err[ierr] = np.sqrt(jackk_fit_err[ierr]*(nblock-1)/nblock)

        #print('\n@@@@@ ioffemean & ioffeerr:')
        abwitherr = []
        for i in range(len(ab)):
                abwitherr += [ab[i],jackk_fit_err[i]]
        return abwitherr
def make_mom2findab(mom2):
        def f(ab):
                a = float(ab[0])
                b = float(ab[1])
                return gamma(2+a)*gamma(2+a+b)/(gamma(1+a)*gamma(3+a+b))-0.215, gamma(3+a)*gamma(2+a+b)/(gamma(1+a)*gamma(4+a+b))-mom2
        return f
'''


def reducedIoffe_sumfit(Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist,c2fitparams, blocksize, onlymean = None, jackfilename = None):

        #outfilename = '/Users/gaoxiang/Desktop/0study/research/lattice/datas/new_fine_datas/analysis_new/summationfit/reduced_Ioffe/rioffe_jackblock/fine64c64'

        outfilename = jackfilename + 'rif_jackk'
        print('rif jackk file name:', outfilename)
        # first calculation the jackknife mean value
        meanbare = []
        for px in range(0, len(pxlist)):
                pxc3data = c3dataset[px]              # pxc3data[z][ts][tau][conf]
                pxc2data = c2dataset[px]              # pxc2data[t][conf]
                pxc2fitparams = c2fitparams[px]
                c2mdf = make_c2mdf(pxc2data, pxc2fitparams, Nt)

                pxfitreult = px_sumfit(pxc2data, pxc3data, tslist, c2mdf)
                meanbare += [pxfitreult]               # jackkmeanbare[px][z] is the bare matrix

        ioffemean = ioffeto_reduced(meanbare, zlist)

        # jackknife sample and error caculation
        # jackk_ioffe = []         # jackk_ioffe[nblock][z] = [z, pxvalues...]
        nconf = len(c2dataset[0][0])-1
        print('configuration number:', nconf)
        nblock = int(nconf/blocksize)
        jackk_ioffe_err = np.zeros((len(zlist), len(pxlist)+1))
        if onlymean == None:
                for i in range(0, nblock):
                        print('\n jackknife block:',i+1,'/',nblock)
                        samplec3dataset = d5slicedelete(c3dataset,[i*blocksize+1,(i+1)*blocksize+1],4)
                        samplec2dataset = d5slicedelete(c2dataset,[i*blocksize+1,(i+1)*blocksize+1],2)
                        
                        samplebare = []
                        for px in range(0, len(pxlist)):
                                samplepxc3data = samplec3dataset[px]              # pxc3data[z][ts][tau][conf]
                                samplepxc2data = samplec2dataset[px]              # pxc2data[t][conf]
                                samplepxc2fitparams = c2fitparams[px]
                                c2mdf = make_c2mdf(samplepxc2data, samplepxc2fitparams, Nt)
                                samplepxfitreult = px_sumfit(samplepxc2data, samplepxc3data, tslist, c2mdf)
                                samplebare += [samplepxfitreult]               # jackkmeanbare[px][z] is the bare matrix
                        sampleioffe = ioffeto_reduced(samplebare, zlist)

                        for z in range(len(sampleioffe)):
                                z_ioffecombi = []
                                for px in range(0, len(pxlist)):
                                        z_ioffecombi += [[pxlist[px]*zlist[z]*2*3.14259/Ns,sampleioffe[z][px+1]]]
                                outfile = outfilename + '_Z' + str(zlist[z]) + '_block' + str(i+1) + '.txt'
                                savefile = open(outfile,'w')
                                #print(outfile)
                                np.savetxt(savefile,z_ioffecombi,fmt='%.6e')
                                savefile.close()

                                
                        for err_i in range(0, len(sampleioffe)):
                                jackk_ioffe_err[err_i][0] = sampleioffe[err_i][0]
                                for err_j in range(1, len(sampleioffe[err_i])):
                                        jackk_ioffe_err[err_i][err_j] += (sampleioffe[err_i][err_j] - ioffemean[err_i][err_j])*(sampleioffe[err_i][err_j] - ioffemean[err_i][err_j])
                                
                for err_i in range(0, len(jackk_ioffe_err)):
                        for err_j in range(1, len(jackk_ioffe_err[err_i])):
                                jackk_ioffe_err[err_i][err_j] = np.sqrt(jackk_ioffe_err[err_i][err_j]*(nblock-1)/nblock)

        #print('\n@@@@@ ioffemean & ioffeerr:')
        ioffecombi = []
        for z in range(len(ioffemean)):
                z_ioffecombi = []
                for px in range(0, len(pxlist)):
                        z_ioffecombi += [[pxlist[px]*zlist[z]*2*3.14259/Ns,ioffemean[z][px+1],jackk_ioffe_err[z][px+1]]]
                #print(z_ioffecombi)
                ioffecombi += [z_ioffecombi]
        #print(ioffecombi)
        return ioffecombi


'''
This is a summation fit jackknife function.
The input 
'''
def jackk_sumfit(Nt, c2dataset, c3dataset, zlist, tslist, c2ptfitparams, blocksize=1, jackfilename = None, onlymean = None,normalization=False):

        sample_save = []
        for i in range(0, len(zlist)):
                sample_save += [[]]
        #print('sumfit jackk file name:', jackfilename)

        # first calculation the jackknife mean value
        print('First calculation the jackknife mean value and collect the standard error\n...\n...\n')
        c2mdf = make_c2mdf(c2dataset, c2ptfitparams, Nt)
        meanresult = []
        meanerr = []
        tsc2data = []
        for ts in tslist:
                tsc2data += [c2dataset[ts]]
        for z in range(0, len(zlist)):
                print('z:', zlist[z])
                zmeanresult, zmeanerr = samplesumfit(tsc2data, c3dataset[z], c2mdf, meanonly=False, blocksize=blocksize)
                if zlist[z] == 0:
                        z0result = zmeanresult
                meanresult += [zmeanresult]
                meanerr += [zmeanerr]
        if normalization == True:
                for z in range(0, len(zlist)):
                        if zlist[z] != 0:
                                meanresult[z][1] /= z0result[1]

        nconf = len(c2dataset[0])-1
        print('configuration number:', nconf)
        nblock = int(nconf/blocksize)
        jackk_err = np.zeros((len(meanresult),len(meanresult[0])))
        if onlymean == None:
                for i in range(0, nblock):

                        print('\n jackknife block:',i+1,'/',nblock)
                        samplec3dataset = d5slicedelete(c3dataset,[i*blocksize+1,(i+1)*blocksize+1],3)
                        samplec2dataset = d5slicedelete(c2dataset,[i*blocksize+1,(i+1)*blocksize+1],1)
                        
                        c2mdf = make_c2mdf(samplec2dataset, c2ptfitparams, Nt)
                        samplefitresult = []

                        sampletsc2data = []
                        for ts in tslist:
                                sampletsc2data += [samplec2dataset[ts]]

                        for z in range(0, len(zlist)):
                                samplefitresult += [samplesumfit(sampletsc2data, samplec3dataset[z], c2mdf, errmatrix = meanerr[z], blocksize=blocksize)]
                                if zlist[z] == 0:
                                        z0result = samplefitresult[z]
                                sample_save[z] += [[i] + samplefitresult[z]]

                        '''
                        savesample = []
                        for z in range(0, len(zlist)):
                                savesample += [[zlist[z]]+samplefitresult[z]]
                        outfile = outfilename + '_block' + str(i+1) + '.txt'
                        savefile = open(outfile,'w')
                        np.savetxt(savefile,savesample,fmt='%.8e')
                        savefile.close()
                        '''

                        #print(np.shape(jackk_err),np.shape(samplefitresult),np.shape(meanresult))
                        if normalization == True:
                                for iz in range(0, len(zlist)):
                                        if zlist[iz] != 0:
                                                samplefitresult[iz][1] /= z0result[1]
                                
                        for err_i in range(0, len(samplefitresult)):
                                for err_j in range(0, len(samplefitresult[0])):
                                        jackk_err[err_i][err_j] += (samplefitresult[err_i][err_j] - meanresult[err_i][err_j])*(samplefitresult[err_i][err_j] - meanresult[err_i][err_j])
                                
                for err_i in range(0, len(jackk_err)):
                        for err_j in range(0, len(samplefitresult[0])):
                                jackk_err[err_i][err_j] = np.sqrt(jackk_err[err_i][err_j]*(nblock-1)/nblock)

        #print('\n@@@@@ ioffemean & ioffeerr:')
        resultcombi = []
        for z in range(len(meanresult)):

                resultline = [zlist[z]]
                for i in range(0, len(meanresult[0])):
                        resultline += [meanresult[z][i], jackk_err[z][i]]
                resultcombi += [resultline]

                # save the jackknife samples
                if jackfilename != None:
                        z_sample_file = jackfilename + '_Z' + str(zlist[z]) 
                        np.savetxt(z_sample_file,sample_save[z],fmt='%.8e')
        #ichisq = chisq_value_stderr(fitlist, linearfunc, ipoptvalue)

        return resultcombi


# the c2sample here should hasn't been modified!!!
# c3sample raw should still be 3D, and each 2D array in it
# should be formatted as: [tau, conf1, conf2, ...]
# this function return an 1d array contains fitting result.
def samplesumfit(c2data,c3data,c2mdf,errmatrix=[None],meanonly=True,blocksize=1):
        
        nts = len(c2data)
        nconf = len(c2data[0])-1
        nblock = int(nconf/blocksize)
        c2ave = list(map(np.mean,np.delete(c2data,0,1)))
        #print('c2ave:',c2data[0][0],c2ave)
        ratiosum = []
        ratiosumerr = np.zeros((nts,nts))
        for t in range(0, nts):
                ts = c2data[t][0]
                c3taudata = c3data[t]
                c3tauave = list(map(np.mean,np.delete(c3taudata,0,1)))
                ratiosum += [np.sum(c3tauave)/(c2ave[t]-c2mdf(ts))]
                #print(ts,c2mdf(64),c2mdf(ts))

        if (np.array(errmatrix)).any() == None:
                for i in range(0,nblock):                # jackknife method to evaluate the err of summation of ratiodata
                        #print(np.shape(c2data))
                        #print(np.shape(d5slicedelete(np.delete(c2data,0,1),[i*blocksize,(i+1)*blocksize],1)))
                        jackc2ave = list(map(np.mean,(d5slicedelete(np.delete(c2data,0,1),[i*blocksize,(i+1)*blocksize],1))))
                        for t1 in range(0, nts):
                                for t2 in range(0, nts):

                                        ts1 = c2data[t1][0]
                                        jackc3taudata1 = c3data[t1]
                                        jackc3ave1 = list(map(np.mean, (d5slicedelete(np.delete(jackc3taudata1,0,1),[i*blocksize,(i+1)*blocksize],1))))
                                        jackratiosum1 = np.sum(jackc3ave1)/(jackc2ave[t1]-c2mdf(ts1))

                                        ts2 = c2data[t2][0]
                                        jackc3taudata2 = c3data[t2]
                                        jackc3ave2 = list(map(np.mean,(d5slicedelete(np.delete(jackc3taudata2,0,1),[i*blocksize,(i+1)*blocksize],1))))
                                        jackratiosum2 = np.sum(jackc3ave2)/(jackc2ave[t2]-c2mdf(ts2))

                                        #print(i, nblock, jackc2ave, jackratiosum1,jackratiosum2, ratiosum[t1], ratiosum[t2])
                                        if t1 == t2:
                                                ratiosumerr[t1][t2] += (jackratiosum1-ratiosum[t1])*(jackratiosum2-ratiosum[t2])
                for t1 in range(0, nts):
                        for t2 in range(0, nts):
                                ratiosumerr[t1][t2] = ratiosumerr[t1][t2]*(nblock-1)/nblock
        else:
                ratiosumerr = errmatrix
        
        # chi_sq fitting
        x = [raw[0] for raw in c2data]
        #print(x,'\n',ratiosum,'\n',ratiosumerr)
        popt, popv = curve_fit(linearfunc, x, ratiosum, [0,0],sigma=ratiosumerr)
        fitlist = [[x[i], ratiosum[i],0] for i in range(0, len(x))]
        chisq = chisq_value_stderr(fitlist, linearfunc, popt, cov_matrix=ratiosumerr)
        chisq = chisq/(len(x)-len(popt))
        #print('x',x)
        #print('ratiosum',ratiosum)
        #print('popt',popt)

        if meanonly == True:
                return [popt[0],popt[1]] + ratiosum +[chisq]
        else:
                return [popt[0],popt[1]] + ratiosum +[chisq], ratiosumerr

# make the c2mdf funtion to use
def make_c2mdf(c2data, c2fitparams, Nt):
        
        tmin = int(c2fitparams[0])
        tmax = int(c2fitparams[1])
        c2fitparams = [['full', 'onestate'] + c2fitparams[2:4],['full', 'twostate'] + c2fitparams[2:6]]
        #print(c2fitparams)

        if tmax != 0 and tmax > tmin:
                c2data = c2data[tmin:tmax+1]
                c2fitresult,failpercent = aiccfit(c2data, c2fitparams,np.zeros([len(c2fitparams)]))
                #print('\nc2pt fitting result:',c2fitresult[-1])
                A0 = c2fitresult[-1][0]
                E0 = c2fitresult[-1][1]
                #print('A0,E0',A0,E0, c2fitparams)
                def c2mdf(t):
                        return A0*np.exp(-E0*(Nt-t))
        else:
                A0 = float(c2fitparams[1][2])
                E0 = float(c2fitparams[1][3])
                A1 = float(c2fitparams[1][4])
                E1 = float(c2fitparams[1][5])
                def c2mdf(t):
                        return A0*np.exp(-E0*(Nt-t)) + A1*np.exp(-E1*(Nt-t))
        #print(c2mdf(64))
        return c2mdf

# use the specific px dataset to do the summation fit
# pxc2data here is pxc2data[ts][conf]
# pxc3data here is pxc3data[z][ts][tau][conf]
# this function return a 2d array: pxzvalue[z] = [fitting result]
def px_sumfit(pxc2data, pxc3data, tslist, c2mdf, matrixonly = True,**kwargs):

        # pick specific ts for ratio caculation
        pxtsc2data = []
        for ts in tslist:
                pxtsc2data += [pxc2data[ts]]
                        
        # iterations to do summation fit for each z
        pxzvalue = []
        if matrixonly == True:
                for z in range(0, len(pxc3data)):
                        pxzc3data = pxc3data[z]
                        fitresult = samplesumfit(pxtsc2data,pxzc3data,c2mdf)
                        pxzvalue += [fitresult[1]]
        else:
                for z in range(0, len(pxc3data)):
                        pxzc3data = pxc3data[z]
                        fitresult = samplesumfit(pxtsc2data,pxzc3data,c2mdf)
                        pxzvalue += [fitresult]
        
        return pxzvalue 

# transform the ioffe distribution to reduced one
# ioffedata[px][z] to reduced_ioffe[z][pxz]
# This function is in statistics now
'''
def ioffeto_reduced(ioffedata, zlist):

        rioffe = []                
        for z in range(0,len(zlist)):
                zrioffe = []
                for px in range(0,len(ioffedata)):
                        zrioffe += [ioffedata[px][z]/ioffedata[0][z]*ioffedata[0][0]/ioffedata[px][0]]
                        #print(jackkmeanmat[px][z]/jackkmeanmat[0][z]*jackkmeanmat[0][0]/jackkmeanmat[px][0])
                zrioffe = [zlist[z]] + zrioffe
                rioffe += [zrioffe]
        return rioffe
'''

#------------------------------------------------------------#
#################### c3pt fitting process ####################
#------------------------------------------------------------#
'''
The c3pt fitting here need only c3ptdata.
------------------------------------------------------------
The parameter in the function should be formatted as:
ts tau conf1 conf2 ...
... 
------------------------------------------------------------
'''
def c3ptfit(tslist,c3dataset, c2dataset, ratioerr, params, jointc2=[None],**kwargs):

        #print(c2dataset)

        priorline = params[0]
        if 'prior' in priorline[0]:
                nprior = int((len(priorline) - 1)/2)
                prior = []
                for ip in range(0, nprior):
                        iprior = []
                        for jp in range(0, 2):
                                iprior += [float(priorline[ip*2+jp+1])]
                        prior += [iprior]
                params = params[1:]
        else:
                prior = [None]

        for line in params:

                fixarg = line[0]
                funcarg = line[1]
                poptguess = list(map(float,line[2:]))
                #print(poptguess)
                
                #print(poptguess)
                # define the fitting functions and initio fitting parameters
                if 'A0E0A1E1A2E2' in fixarg:
                        fix_num = 6
                        c2popt = [poptguess[0],poptguess[1],poptguess[2],poptguess[3],poptguess[4],poptguess[5]]
                        func = getattr(modelfuncs,line[1])
                        poptstart = np.delete(poptguess,[0,1,2,3,4,5])
                        #print(poptstart)
                        if 'three' in funcarg:
                                def fitfunc(X,B0,B1,B2,B3,C1,C2,C3,C4,C5):
                                        return func(X,poptguess[0],poptguess[1],poptguess[2],poptguess[3],poptguess[4],poptguess[5],B0,B1,B2,B3,C1,C2,C3,C4,C5)
                        elif 'two' in funcarg:
                                def fitfunc(X,B0,B1,B2,B3):
                                        return func(X,poptguess[0],poptguess[1],poptguess[2],poptguess[3],B0,B1,B2,B3)
                        else:
                                print('Wrong c3pt fitting function!')
                elif  'E0E1E2' in fixarg:
                        fix_num = 3
                        c2popt = []
                        func = getattr(modelfuncs,line[1])
                        poptstart = np.delete(poptguess,[1,3,5])
                        #print(poptstart)
                        if 'three' in funcarg:
                                def fitfunc(X,A0,A1,A2,B0,B1,B2,B3,C1,C2,C3):
                                        return func(X,A0,poptguess[1],A1,poptguess[3],A2,poptguess[5],B0,B1,B2,B3,C1,C2,C3)
                        elif 'two' in funcarg:
                                def fitfunc(X,B0,B1,B2,B3):
                                        return func(X,poptguess[0],poptguess[1],poptguess[2],poptguess[3],B0,B1,B2,B3)
                        else:
                                print('Wrong c3pt fitting function!')
                elif  'A0E0A1E1' in fixarg:
                        fix_num = 4
                        c2popt = [poptguess[0],poptguess[1],poptguess[2],poptguess[3]]
                        func = getattr(modelfuncs,line[1])
                        poptstart = np.delete(poptguess,[0,1,2,3])
                        #print(poptstart)
                        if 'two' in funcarg:
                                def fitfunc(X,B0,B1,B2,B3):
                                        return func(X,poptguess[0],poptguess[1],poptguess[2],poptguess[3],B0,B1,B2,B3)
                        else:
                                print('Wrong c3pt fitting function!')
                elif 'A0E0A1' in fixarg:
                        fix_num = 3
                        c2popt = [poptguess[0],poptguess[1],poptguess[2]]
                        func = getattr(modelfuncs,line[1])
                        poptstart = np.delete(poptguess,[0,1,2])
                        #print(poptstart)
                        if 'two' in funcarg:
                                def fitfunc(X,E1,B0,B1,B2,B3):
                                        return func(X,poptguess[0],poptguess[1],poptguess[2],E1,B0,B1,B2,B3)
                        else:
                                print('Wrong c3pt fitting function!')
                elif 'A0E0' in fixarg:
                        fix_num = 2
                        c2popt = [poptguess[0],poptguess[1]]
                        func = getattr(modelfuncs,line[1])
                        poptstart = np.delete(poptguess,[0,1])
                        #print(poptstart)
                        if 'two' in funcarg:
                                def fitfunc(X,A1,E1,B0,B1,B2,B3):
                                        return func(X,poptguess[0],poptguess[1],A1,E1,B0,B1,B2,B3)
                        else:
                                print('Wrong c3pt fitting function!')
                elif fixarg == 'full':
                        fix_num = 0
                        c2popt = []
                        fitfunc = getattr(modelfuncs,line[1])
                        func = fitfunc
                        poptstart = poptguess
                else:
                        print('\nWrong input file!')
                
                c3c2ratio = c3fitratio(tslist,c3dataset,c2dataset,True,jointc2,**kwargs)
                ncfg = len(c2dataset[0]) - 1
                #c3c2cov = c3fitratio(tslist,c3dataset,c2dataset,'covariance',jointc2,ratio=ratio, taumin = taumin)
                #c3c2meanerr = c3fitratio(tslist,c3dataset,c2dataset,'2dmeanerr',jointc2,ratio=ratio, taumin = taumin)
                #print(c3c2ratio)
                x1 = []
                x2 = []
                yave = []
                yerr = []
                fitdata = []
                for t in range(0, len(c3c2ratio)):
                        for tt in range(0, len(c3c2ratio[t])):
                                #print(ratioerr[t][tt])
                                x1 += [int(c3c2ratio[t][tt][0])]
                                x2 += [int(c3c2ratio[t][tt][1])]
                                yave += [c3c2ratio[t][tt][2]]
                                yerr += [ratioerr[t][tt][3]]
                                fitdata += [[c3c2ratio[t][tt][0],c3c2ratio[t][tt][1],c3c2ratio[t][tt][2],ratioerr[t][tt][3]]]
                if (np.array(prior)).all() != None:
                        for i in range(0, len(prior)):
                                x1 += [priorsign + 1]
                                x2 += [priorsign + 1]
                                yave += [prior[i][0]]
                                yerr += [prior[i][1]]
                #print('\n\n\n\n\n\n\n\n',np.linalg.inv(ycov),x,y)

                # The fitting procedure
                popt = []
                maxiter = 1
                for i in range(0 ,maxiter):
                        for j in range(len(poptstart)):
                                poptstart[j] = np.random.normal(poptstart[j],0.00*np.fabs(poptstart[j]))
                        try:
                                #print(yave)
                                popt, pcov = curve_fit(fitfunc, (x1,x2), yave, poptstart, yerr, maxfev=2000)
                        except:
                                traceback.print_exc()
                                if i+1 == maxiter:
                                        #print('\nMax iteration reached!')
                                        popt = np.ones(len(poptstart))*1000*int(np.random.uniform(0,2))

                popt = c2popt + list(popt)
                #print(popt)
                chisq = chisq_value_stderr(fitdata, func, popt)
                lnL = -0.5*chisq
                pnum = len(poptguess) - fix_num
                aic = 2*pnum - 2 * lnL
                aicc = aic + (2*pnum*pnum + 2*pnum)/(ncfg - pnum -1)

                popt = popt + [chisq] + [aicc]
                return popt

#------------------------------------------------------------#
################### simple fitting process ###################
#------------------------------------------------------------#
'''
The fitting procedure here only need fitfunction, x, y, yerr,poptguess
xy_err[sample][x] = [x, y, yerr] or xy_err[sample][x] = [x, x2, f(x,y), ferr]
'''
def simple_fit(xy_err, ymean, fitparams, sampletype, covariance=[None]):

        #print(poptguess)
        fitfreedegr = len(ymean) - (len(fitparams[0]) - 2)  # degree of freedom

        if sampletype == 'jackknife':

                nblock = len(xy_err)

                #fitx = [xy_err[0][i][0] for i in range(0, len(xy_err[0]))]
                #fitymean = [ymean[i][1] for i in range(0, len(ymean))]
                #fityerr = [xy_err[0][i][2] for i in range(0, len(xy_err[0]))]
                #print(fitparams)
                #print('data shape:', np.shape(ymean),ymean)

                poptmean, failpercent = aiccfit(ymean, fitparams, datatype='meanerr', cov_matrix=covariance)
                chisqlist = []
                for imodel in range(0, len(poptmean)):
                        if imodel < len(fitparams):
                                chisqlist += [chisq_value_stderr(ymean, getattr(modelfuncs, fitparams[imodel][1]), poptmean[imodel], cov_matrix = covariance)/fitfreedegr]
                        else:
                                chisqlistsort = sorted(chisqlist)
                                chisqlist += [chisqlistsort[0]]
                #print(chisqlist)
                #print('poptmean',poptmean)

                popt = []
                popterr = []
                for imodel in range(0, len(poptmean)):
                        popterr += [list(np.zeros(len(poptmean[imodel])))]
                #print(popterr)
                for isamp in range(0, nblock):

                        #fity = [xy_err[isamp][i][1] for i in range(0, len(xy_err[0]))]
                        ipopt, failpercent = aiccfit(xy_err[isamp], fitparams, datatype='meanerr', cov_matrix=covariance)
                        #print(ymean,'\n',xy_err[isamp],'\n\n')
                        #print(ipopt)

                        for imodel in range(0, len(ipopt)):
                                for subp in range(0, len(popterr[imodel])):
                                        #print(popterr[subp],ipopt[subp],poptmean) 
                                        popterr[imodel][subp] += (ipopt[imodel][subp] - poptmean[imodel][subp]) * (ipopt[imodel][subp] - poptmean[imodel][subp])

                #print(popterr)
                
                for imodel in range(0, len(poptmean)):
                        imodelpopt = []
                        # fitting errors
                        for subp in range(0, len(poptmean[imodel])):
                                popterr[imodel][subp] = np.sqrt(popterr[imodel][subp]*(nblock-1)/nblock)
                                imodelpopt += [poptmean[imodel][subp],popterr[imodel][subp]]
                                #print(imodelpopt)
                        popt += [imodelpopt + [chisqlist[imodel]]]
                #print(popt)
                return popt
        
        elif sampletype == 'bootstrap':

                npick = len(xy_err)

                #fitx = [xy_err[0][i][0] for i in range(0, len(xy_err[0]))]
                #fityerr = [xy_err[0][i][2] for i in range(0, len(xy_err[0]))]
                poptlist = []
                for isamp in range(0, npick):

                        #fity = [xy_err[isamp][i][1] for i in range(0, len(xy_err[0]))]
                        #print(fitx,fity,fityerr)
                        ipopt, failpercent = aiccfit(xy_err[isamp], fitparams, datatype='meanerr')
                        poptlist += [ipopt]
                        #print(ipopt)
                        
                low16 = int(np.ceil(0.16*npick))
                high16 = int(np.floor(0.84*npick))
                mid = int(0.5*npick)

                popt = []
                poptlist = ex12_4d(poptlist)
                chisqlist = []
                for imodel in range(0, len(fitparams)+1):
                        imodelpopt = poptlist[imodel]
                        imodelpoptT = np.transpose(imodelpopt)
                        imodelpopterr = []
                        imodelpoptmean = []
                        for raw in imodelpoptT:
                                raw = sorted(raw)
                                subpopt = raw[mid]
                                suberrorn = raw[mid]-raw[low16]
                                suberrorp = raw[high16]-raw[mid]
                                imodelpopterr += [subpopt, suberrorn, suberrorp]
                                imodelpoptmean += [subpopt]
                        if imodel < len(fitparams):
                                #chisqlist += [aicc_value(ymean,getattr(modelfuncs, fitparams[imodel][1]), imodelpoptmean, datatype='meanerr')]
                                chisqlist += [chisq_value_stderr(ymean, getattr(modelfuncs, fitparams[imodel][1]), imodelpoptmean)/fitfreedegr]
                        else:
                                chisqlistsort = sorted(chisqlist)
                                chisqlist += [chisqlistsort[0]]
                        popt += [imodelpopterr + [chisqlist[imodel]]]
                #print(popt)
                return popt
        
        else:
                print('Wrong type!!!')
        '''
        elif sampletype == 'jackknife' and len(xy_err[0][0]) == 4:

                nblock = len(xy_err)

                fitx = [xy_err[0][i][0] for i in range(0, len(xy_err[0]))]
                fitx2 = [xy_err[0][i][1] for i in range(0, len(xy_err[1]))]
                fitymean = [ymean[i][2] for i in range(0, len(ymean))]
                fityerr = [xy_err[0][i][3] for i in range(0, len(xy_err[0]))]
                #print('fitx:',fitx)
                #print('fitx2:',fitx2)
                #print('fitymean:',fitymean)
                #print('fityerr:',fityerr)

                poptmean, popterr = curve_fit(fitfunc, (fitx,fitx2), fitymean, poptguess, fityerr)

                popt = []
                popterr = np.zeros(len(poptguess))
                for isamp in range(0, nblock):

                        fity = [xy_err[isamp][i][2] for i in range(0, len(xy_err[0]))]
                        ipopt, ipopv = curve_fit(fitfunc, (fitx,fitx2), fity, poptguess, fityerr)

                        for subp in range(0, len(ipopt)):
                                #print(popterr[subp],ipopt[subp],poptmean) 
                                popterr[subp] += (ipopt[subp] - poptmean[subp]) * (ipopt[subp] - poptmean[subp])
                
                for subp in range(0, len(popterr)):
                        popterr[subp] = np.sqrt(popterr[subp]*(nblock-1)/nblock)
                        popt += [poptmean[subp],popterr[subp]]
                
                return popt
        '''
