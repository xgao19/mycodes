#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
import inspect
import h5py
from scipy.optimize import curve_fit

from tools import *
from modelfuncs import *
from makefuncs import *
from statistics import *

#E0list_64c64_a04 = [0.0600003, 0.115056, 0.205309, 0.300568, 0.397249, 0.494518, 0.592085, 0.689824]
#Plist_64c64_a04 = [0., 0.0981728, 0.196346, 0.294518, 0.392691, 0.490864, 0.589037, 0.68721]

def ave_c2pt_data(c2pt_format, px, qxyz_list, breit = False):

    print('  Reading c2pt:')
    if breit == False:
        c2pt_in_dataset = []
        for i_fmt in c2pt_format:
            c2pt_in_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(0)+'_PY'+str(0)+'_PZ'+str(px))
            print('    P:', c2pt_in_name)
            c2pt_in_dataset += [csvtolist(c2pt_in_name)]
        c2pt_ave_in = comb_2d_lists(c2pt_in_dataset)
        
        c2pt_out_dataset = []
        for iqxyz in qxyz_list:
            iq_out_dataset = []
            for i_fmt in c2pt_format:
                c2pt_out_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(0+iqxyz[0])+'_PY'+str(iqxyz[1])+'_PZ'+str(px+iqxyz[2]))
                print('    P+q:', c2pt_out_name)
                iq_out_dataset += [csvtolist(c2pt_out_name)]
            c2pt_out_dataset += [comb_2d_lists(iq_out_dataset)]
        if len(c2pt_out_dataset) > 1:
            c2pt_ave_out = ave_2d_lists(c2pt_out_dataset)
        else:
            c2pt_ave_out = c2pt_out_dataset[0]

    elif breit == True:
        c2pt_in_dataset = []
        for iqxyz in qxyz_list:
            iq_in_dataset = []
            for i_fmt in c2pt_format:
                c2pt_in_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(px)+'_PY'+str(-int(iqxyz[1]/2))+'_PZ'+str(-int(iqxyz[2]/2)))
                print('    P:', c2pt_in_name)
                iq_in_dataset  += [csvtolist(c2pt_in_name)]
            c2pt_in_dataset += [comb_2d_lists(iq_in_dataset)]

        c2pt_out_dataset = []
        for iqxyz in qxyz_list:
            iq_out_dataset = []
            for i_fmt in c2pt_format:
                c2pt_out_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(px)+'_PY'+str(int(iqxyz[1]/2))+'_PZ'+str(int(iqxyz[2]/2)))
                print('    P+q:', c2pt_out_name)
                iq_out_dataset += [csvtolist(c2pt_out_name)]
            c2pt_out_dataset += [comb_2d_lists(iq_out_dataset)]
            
        if len(c2pt_in_dataset) > 1:
            c2pt_ave_in = ave_2d_lists(c2pt_in_dataset)
        else:
            c2pt_ave_in = c2pt_in_dataset[0]
        if len(c2pt_out_dataset) > 1:
            c2pt_ave_out = ave_2d_lists(c2pt_out_dataset)
        else:
            c2pt_ave_out = c2pt_out_dataset[0]

    cfg_c2pt = len(c2pt_ave_out[0]) - 1
    print('    P shape:',np.shape(c2pt_ave_in), ', P+q shape:', np.shape(c2pt_ave_out))

    return c2pt_ave_in, c2pt_ave_out

def ave_c3pt_data(c3pt_format, qxyz_list, tslist, zlist, c3pt_datatype, sign_list=[], foldz=False, breit = False):

    print('  Reading c3pt:', c3pt_datatype, ', fold z data:',foldz)
    print('    !!! P->P+q: Note the fourier transform of q is opposite.')

    #print(c3pt_format)
    # Open the h5 data files.
    c3pt_h5_list = []
    if breit == False:
        for iqxyz in qxyz_list:
            iq_h5_list = []
            for i_fmt in c3pt_format:
                i_fmt_name = i_fmt.replace('qx_qy_qz', 'qx'+str(iqxyz[0])+'_qy'+str(iqxyz[1])+'_qz'+str(iqxyz[2]))
                i_fmt_name = i_fmt_name.replace('PX_PY_PZ', 'PX'+str(0)+'_PY'+str(0)+'_PZ'+str(0))
                print('    P->P+q:', i_fmt_name)
                iq_h5_list += [h5py.File(i_fmt_name, 'r')]
            c3pt_h5_list += [iq_h5_list]
    elif breit == True:
        for iqxyz in qxyz_list:
            iq_h5_list = []
            for i_fmt in c3pt_format:
                i_fmt_name = i_fmt.replace('qx_qy_qz', 'qx'+str(-iqxyz[0])+'_qy'+str(-iqxyz[1])+'_qz'+str(-iqxyz[2]))
                i_fmt_name = i_fmt_name.replace('_PY_PZ', '_PY'+str(-int(iqxyz[1]/2))+'_PZ'+str(-int(iqxyz[2]/2)))
                print('    P->P+q:', i_fmt_name)
                iq_h5_list += [h5py.File(i_fmt_name, 'r')]
            c3pt_h5_list += [iq_h5_list] 

    # Read and preproceed the data
    c3pt_dataset = []
    for its in tslist:
        its_data = []
        for iz in zlist:
            iz_data = []
            for iq in range(0, len(c3pt_h5_list)):
                iq_data = []
                for ifmt in c3pt_h5_list[iq]:
                    #print(list(ifmt.keys()))
                    #print('dt'+str(its)+'/'+'X'+str(iz))
                    try:
                        if foldz == False:
                            iq_data += [np.array(ifmt['dt'+str(its)+'/'+'X'+str(iz)])]
                        else:
                            Zp_data = np.array(ifmt['dt'+str(its)+'/'+'X'+str(iz)])
                            Zn_data = np.array(ifmt['dt'+str(its)+'/'+'X'+str(-iz)])
                            #if its == 12:
                            #    print('\n',its,np.average(Zp_data[3][60:65]),Zp_data[3][60:65])
                            Zave_data = fold_z_data(Zp_data, Zn_data, foldz)
                            iq_data += [Zave_data]
                    except:
                        #traceback.print_exc()
                        pass
                #print(np.shape(iq_data))
                iz_data += [comb_2d_lists(iq_data, c3pt_datatype)]
                #print('iz shape',np.shape(iz_data))
            if len(sign_list) != 1:
                its_data += [ave_2d_lists(iz_data, sign_list)]
            else:
                its_data +=  [iz_data[0]]
        c3pt_dataset += [its_data]
    print('    P->P+q shape:',np.shape(c3pt_dataset))

    # Close the h5 data files.
    for iq_h5 in c3pt_h5_list:
        for ifmt_h5 in iq_h5:
            ifmt_h5.close()

    return c3pt_dataset




def gpd_ratiofit_jackk(c3pt_data, c2pt_data_in, c2pt_data_out, tslist, Ein, Eout, NT, blocksize, taumin, nm_bm):

    ncfg = len(c2pt_data_in[0]) - 1
    nblock = int(ncfg/blocksize)
    #print('ncfg:', ncfg, 'nblock:', nblock)

    # @@@ mean ratio collection
    mean_ratio = []
    ratio_ts = []
    ratio_tau = []
    for its in range(0, len(tslist)):
        ratio = gpd_ratio_constuct(c3pt_data[its], c2pt_data_in, c2pt_data_out, tslist[its], taumin, nm_bm)
        mean_ratio += ratio
        for itau in range(0, len(ratio)):
            ratio_ts += [tslist[its]]
            ratio_tau += [taumin+itau]
    #print(mean_ratio,ratio_ts,ratio_tau)
    err_ratio = [0 for i in range(0, len(mean_ratio))]
    
    # @@@ standard error collection
    sample_ratio_collection = []
    for isamp in range(0, nblock):

        isamp_c2pt_data_in = d5slicedelete(c2pt_data_in,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        isamp_c2pt_data_out = d5slicedelete(c2pt_data_out,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        isamp_c3pt_data = d5slicedelete(c3pt_data,[isamp*blocksize+1,(isamp+1)*blocksize+1],2)

        sample_ratio = []
        for its in range(0, len(tslist)):
            ratio = gpd_ratio_constuct(isamp_c3pt_data[its], isamp_c2pt_data_in, isamp_c2pt_data_out, tslist[its], taumin, nm_bm)
            sample_ratio += ratio
        for iratio in range(0, len(sample_ratio)):
            err_ratio[iratio] += (sample_ratio[iratio]-mean_ratio[iratio])**2
            #print(sample_ratio[iratio],mean_ratio[iratio],sample_ratio[iratio]-mean_ratio[iratio])
        sample_ratio_collection += [sample_ratio]

    for iratio in range(0, len(sample_ratio)):
        err_ratio[iratio] = np.sqrt(err_ratio[iratio]*(nblock-1)/nblock)
        #print(err_ratio[iratio]*(nblock-1)/nblock)
    #print(mean_ratio)
    
    # @@@ fit procedure
    fit_func = make_gpd_c3pt_ratio(Ein, Eout)
    #poptstart = [np.random.normal(0.5, 0.1) for i in range(0, fit_func.__code__.co_argcount-1)]
    poptstart = [0 for i in range(0, fit_func.__code__.co_argcount-1)]

    # mean ratio fit
    #print(ratio_tau)
    popt_mean, popv = curve_fit(fit_func,(ratio_ts, ratio_tau),mean_ratio,poptstart,sigma=err_ratio)
    chisq_data = []
    for i in range(0, len(mean_ratio)):
        chisq_data += [[ratio_ts[i], ratio_tau[i], mean_ratio[i], err_ratio[i]]]
    chisq = chisq_value_stderr(chisq_data, fit_func, popt_mean)/(len(mean_ratio)-len(popt_mean))
    #print(popt_mean, chisq_value_stderr(chisq_data, fit_func, popt_mean), (len(mean_ratio)-len(popt_mean)))
    print('Nblocks:', nblock, 'popt:',popt_mean,'chisq:',chisq)

    # sample ratio fit
    popt_sample_collection = []
    
    popt_err = [0 for ipopt in range(0, len(popt_mean))]
    for isamp in range(0, nblock):
        popt, popv = curve_fit(fit_func,(ratio_ts, ratio_tau),sample_ratio_collection[isamp],poptstart,sigma=err_ratio)
        popt_sample = list(popt)
        popt_sample_collection += [popt_sample]

        for ipopt in range(0, len(popt_mean)):
            popt_err[ipopt] += (popt_mean[ipopt]-popt_sample[ipopt])**2
            
    popt_mean_err = Ein+Eout
    for ipopt in range(0, len(popt_mean)):
        popt_mean_err += [popt_mean[ipopt], np.sqrt(popt_err[ipopt]*(nblock-1)/nblock)]
    popt_mean_err += [chisq]

    return popt_mean_err, popt_sample_collection

def gpd_sumfit_jackk(c3pt_data, c2pt_data_in, c2pt_data_out, tslist, Ein, Eout, NT, blocksize, taumin, nm_bm):

    ncfg = len(c2pt_data_in[0]) - 1
    nblock = int(ncfg/blocksize)
    #print('ncfg:', ncfg, 'nblock:', nblock)

    # make the function to eliminate the wrap round effect
    def c2mdf_in(t):
        return Ein[0]*np.exp(-Ein[1]*(NT-t)) + Ein[2]*np.exp(-Ein[3]*(NT-t))
    def c2mdf_out(t):
        return Eout[0]*np.exp(-Eout[1]*(NT-t)) + Eout[2]*np.exp(-Eout[3]*(NT-t))

    # mean ratio summation
    mean_sum = []
    for its in range(0, len(tslist)):
        mean_ratio = gpd_ratio_constuct(c3pt_data[its], c2pt_data_in, c2pt_data_out, tslist[its], taumin, nm_bm, c2mdf_in=c2mdf_in, c2mdf_out=c2mdf_out)
        mean_sum += [np.sum(mean_ratio)]
    err_sum = [0 for itau in range(0, len(tslist))]
    
    # standard error collection
    sample_sum_collection = []
    for isamp in range(0, nblock):

        isamp_c2pt_data_in = d5slicedelete(c2pt_data_in,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        isamp_c2pt_data_out = d5slicedelete(c2pt_data_out,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        isamp_c3pt_data = d5slicedelete(c3pt_data,[isamp*blocksize+1,(isamp+1)*blocksize+1],2)

        sample_sum = []
        for its in range(0, len(tslist)):
            sample_ratio = gpd_ratio_constuct(isamp_c3pt_data[its], isamp_c2pt_data_in, isamp_c2pt_data_out, tslist[its], taumin, nm_bm, c2mdf_in=c2mdf_in, c2mdf_out=c2mdf_out)
            sample_sum += [np.sum(sample_ratio)]
            err_sum[its] += (mean_sum[its]-sample_sum[its])**2
        sample_sum_collection += [sample_sum]

    for its in range(0, len(tslist)):
        err_sum[its] = np.sqrt(err_sum[its]*(nblock-1)/nblock)
    #print(np.shape(sample_sum_collection),mean_sum,err_sum)

    # mean summation fit
    #if Ein[1] < 0.07 and Eout[1] > 0.07:
    #    p000 = np.sqrt(Eout[1]**2 - Ein[1]**2)/2
    #    E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #    def fit_func(x, a, b, c):
    #        return a + b * x + c * np.exp(-E11*x) * np.exp(-(NT-2*x)*Ein[1])
    #else:
    fit_func = linearfunc
    #fit_func = make_sumfit_correction(Ein, Eout)
    poptstart = [0 for i in range(0, fit_func.__code__.co_argcount-1)]
    #print(mean_sum, poptstart,err_sum)
    popt, popv = curve_fit(fit_func, tslist, mean_sum, poptstart,sigma=err_sum)
    fitlist = [[tslist[its], mean_sum[its], err_sum[its]] for its in range(0, len(tslist))]
    chisq = chisq_value_stderr(fitlist, fit_func, popt)
    chisq = round(chisq/(len(tslist)-len(popt)), 8)
    popt_mean = list(mean_sum)  + list(popt) + [chisq]
    print('Nblocks:', nblock, 'Popt:',popt, 'chisq', chisq)

    # sample summation fit
    popt_err = [0 for ipopt in range(0, len(popt_mean))]
    popt_sample_collection = []
    for isamp in range(0, nblock):
        popt, popv = curve_fit(fit_func, tslist, sample_sum_collection[isamp], poptstart,sigma=err_sum)
        fitlist = [[tslist[its], sample_sum_collection[isamp][its], err_sum[its]] for its in range(0, len(tslist))]
        chisq = chisq_value_stderr(fitlist, fit_func, popt)
        chisq = chisq/(len(tslist)-len(popt))
        popt_sample = list(sample_sum_collection[isamp]) + list(popt) + [chisq]
        popt_sample_collection += [popt_sample]

        for ipopt in range(0, len(popt_mean)):
            popt_err[ipopt] += (popt_mean[ipopt]-popt_sample[ipopt])**2
            
    popt_mean_err = []
    for ipopt in range(0, len(popt_mean)):
        popt_mean_err += [popt_mean[ipopt], np.sqrt(popt_err[ipopt]*(nblock-1)/nblock)]
    #print(popt_mean)
        
    return popt_mean_err, popt_sample_collection

def gpd_ratio_jackk(c3pt_data, c2pt_data_in, c2pt_data_out, ts, Ein, Eout, NT, blocksize, taumin, nm_bm):

    ncfg = len(c2pt_data_in[0]) - 1
    nblock = int(ncfg/blocksize)
    #taumin = 0
    #print('ncfg:', ncfg, 'nblock:', nblock)

    # make the function to eliminate the wrap round effect
    def c2mdf_in(t):
        return Ein[0]*np.exp(-Ein[1]*(NT-t)) + Ein[2]*np.exp(-Ein[3]*(NT-t))
    def c2mdf_out(t):
        return Eout[0]*np.exp(-Eout[1]*(NT-t)) + Eout[2]*np.exp(-Eout[3]*(NT-t))

    mean_ratio = gpd_ratio_constuct(c3pt_data, c2pt_data_in, c2pt_data_out, ts, taumin, nm_bm, c2mdf_in=c2mdf_in, c2mdf_out=c2mdf_out)
    err_ratio = [0 for itau in range(0, len(mean_ratio))]

    for isamp in range(0, nblock):

        isamp_c2pt_data_in = d5slicedelete(c2pt_data_in,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        isamp_c2pt_data_out = d5slicedelete(c2pt_data_out,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        isamp_c3pt_data = d5slicedelete(c3pt_data,[isamp*blocksize+1,(isamp+1)*blocksize+1],1)
        #print(np.shape(isamp_c2pt_data_in),np.shape(isamp_c2pt_data_out), np.shape(isamp_c3pt_data))

        sample_ratio = gpd_ratio_constuct(isamp_c3pt_data, isamp_c2pt_data_in, isamp_c2pt_data_out, ts, taumin, nm_bm, c2mdf_in=c2mdf_in, c2mdf_out=c2mdf_out)

        for itau in range(0, len(mean_ratio)):
            err_ratio[itau] += (sample_ratio[itau]-mean_ratio[itau]) * (sample_ratio[itau]-mean_ratio[itau])

    mean_err = []
    for itau in range(0, len(mean_ratio)):
        mean_err += [[itau+taumin, mean_ratio[itau], np.sqrt(err_ratio[itau]*(nblock-1)/nblock)]]

    return mean_err
    

def gpd_ratio_constuct(c3pt_data, c2pt_data_in, c2pt_data_out, ts, taumin, nm_bm, c2mdf_in = None, c2mdf_out = None):

    ratio_list = []

    try:
        c2pt_mdf_in = [c2mdf_in(itau) for itau in range(0, 32)]
        c2pt_mdf_out = [c2mdf_out(itau) for itau in range(0, 32)]
    except:
        c2pt_mdf_in = [0 for itau in range(0, 32)]
        c2pt_mdf_out = [0 for itau in range(0, 32)]

    #for tau in range(taumin, 32):
    for tau in range(taumin, ts+1-taumin):

        c3pt_ave = np.average(c3pt_data[tau][1:])
        #print(c3pt_ave)

        c2pt_ave_in_ts = np.average(c2pt_data_in[ts][1:]) - c2pt_mdf_in[ts]
        c2pt_ave_in_tau = np.average(c2pt_data_in[tau][1:]) - c2pt_mdf_in[tau]
        c2pt_ave_in_tstau = np.average(c2pt_data_in[ts-tau][1:]) - c2pt_mdf_in[ts-tau]

        c2pt_ave_out_ts = np.average(c2pt_data_out[ts][1:]) - c2pt_mdf_out[ts]
        c2pt_ave_out_tau = np.average(c2pt_data_out[tau][1:]) - c2pt_mdf_out[tau]
        c2pt_ave_out_tstau = np.average(c2pt_data_out[ts-tau][1:]) - c2pt_mdf_out[ts-tau]

        #print(c3pt_ave,c2pt_ave_out_ts)
        #c2pt_ave_in_tau = 1
        #c2pt_ave_in_tstau = 1
        #c2pt_ave_out_tau = 1
        #c2pt_ave_out_tstau = 1

        #if len(c2pt_data_in[0]) == 315:
        #    sqrtroot = np.sqrt((c2pt_ave_out_tstau*c2pt_ave_in_tau*c2pt_ave_in_ts)/(c2pt_ave_in_tstau*c2pt_ave_out_tau*c2pt_ave_out_ts))
        #    print('in,',ts,c2pt_ave_in_ts,',',tau,c2pt_ave_in_tau,',',ts-tau,c2pt_ave_in_tstau,',ratio:',c3pt_ave/c2pt_ave_in_ts*sqrtroot)
        #    print('out,',ts,c2pt_ave_out_ts,',',tau,c2pt_ave_out_tau,',',ts-tau,c2pt_ave_out_tstau,',ratio:',c3pt_ave/c2pt_ave_in_ts*sqrtroot)

        ratio_list += [nm_bm*c3pt_ave/c2pt_ave_in_ts*np.sqrt((c2pt_ave_out_tstau*c2pt_ave_in_tau*c2pt_ave_in_ts)/(c2pt_ave_in_tstau*c2pt_ave_out_tau*c2pt_ave_out_ts))]
        #print(nm_bm, c3pt_ave, c2pt_ave_in_ts,np.sqrt((c2pt_ave_out_tstau*c2pt_ave_in_tau*c2pt_ave_in_ts)/(c2pt_ave_in_tstau*c2pt_ave_out_tau*c2pt_ave_out_ts)))

    return ratio_list

# match the c2pt data and c3pt data with same configuration
def match_c2pt_c3pt(c2pt_data, c3pt_data):

    cfg_c3pt = len(c3pt_data[0]) - 1
    cfg_c2pt = len(c2pt_data[0]) - 1

    temp_data = []
    for iraw in range(0, len(c2pt_data)):
        temp_data += [[c2pt_data[iraw][0]] + c2pt_data[iraw][cfg_c2pt-cfg_c3pt+1:]]

    return temp_data





if __name__ == "__main__":

    if sys.argv[1] == 'gpd':

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]
        NS = int((commands[0].split())[1])
        NT = int((commands[0].split())[2])
        blocksize = int((commands[0].split())[3])
        ave = (commands[0].split())[4]
        foldz = (commands[0].split())[5]

        # create the work folders here
        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        if 'c3ptfit' in describe:
            resultfolder = resultfolder.replace('results', 'results_c3ptfit')
        elif 'sumfit' in describe:
            resultfolder = resultfolder.replace('results', 'results_sumfit')
        elif 'ratio' in describe:
            resultfolder = resultfolder.replace('results', 'results_ratio')
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)

        print('\nNote! The Ein and Eout in the input file must be listed from small to large !\n')
        print('\nJob describe:', describe, 'NS:', NS, 'NT:', NT, 'jackknife block size:', blocksize)

        Ein_count = 0
        Eout_count = 0
        taumin = 0
        pxlist = []
        bxplist = []
        c2pt_format = []
        c3pt_format = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'gmlist' == line[0]:
                gmlist = [line[i] for i in range(1, len(line))]
                print('gmlist:', gmlist)

            elif 'px' == line[0]:
                pxlist += [i for i in range(int(line[2]), int(line[3])+1)]
                bxplist += [line[1] for i in range(int(line[2]), int(line[3])+1)]
                Ein_list = []
                Eout_list = []
            
            elif 'qxyz' in line[0]:
                qxyz = [int(line[1]),int(line[2]),int(line[3])]
                print('qxyz:', qxyz)

            elif 'ts' in line[0]:
                tslist = [int(line[i]) for i in range(1, len(line))]
                describe = describe.replace('ts', str(len(tslist))+'ts')
                print('tslist:', tslist)

            elif 'taumin' in line[0]:
                taumin = int(line[1])
                describe = describe.replace('nsk', 'nsk'+str(taumin))
                print('taumin:', taumin)

            elif 'zrange' in line[0]:
                zlist = [iz for iz in range(int(line[1]),int(line[2])+1)]
                print('zlist:', zlist)

            elif 'c3pt' in line[0]:
                c3pt_format += [line[1]]
                c3pt_datatype = line[2]
            elif 'c2pt' in line[0]:
                c2pt_format += [line[1]]

            # Note! The Ein and Eout in the input file must be listed from small to large !!!
            elif 'Ein' in line[0]:
                if int(line[1]) in pxlist:
                    Ein_list += [[float(line[i]) for i in range(2, len(line))]]
                    print('Ein:',', px', pxlist[Ein_count], bxplist[Ein_count], Ein_list[Ein_count])
                    Ein_count += 1
            elif 'Eout' in line[0]:
                if int(line[1]) in pxlist:
                    Eout_list += [[float(line[i]) for i in range(2, len(line))]]
                    print('Eout:', ', px',pxlist[Eout_count], bxplist[Eout_count], Eout_list[Eout_count])
                    Eout_count += 1

        # consider the fold of z
        if 'fz' in foldz:
            foldz_print = 'Data for c3pt will be folded (averaged) by (+-) Z.'
            if '+' in foldz:
                foldz = '+'
            elif '-' in foldz:
                foldz = '-'
            else:
                print('***Please spicify how to fold z.')
                os._exit(0)
        else:
            foldz_print = 'Data for c3pt will NOT be folded (averaged) by (+-) Z.'
            foldz = False

        # consider the average of different qx,qy,qz
        qxyz_list = []
        qylist = [0]
        qzlist = [0]
        g2_sign = []
        g4_sign = []
        if ave == 'ave':

            describe = describe.replace('gm', 'gm_ave')

            # if qy is not 0
            if qxyz[1] != 0:
                qylist = [qxyz[1], -qxyz[1]]
            # if qz is not 0
            if qxyz[2] != 0:
                qzlist = [qxyz[2], -qxyz[2]]
                
            for iqy in qylist:
                for iqz in qzlist:
                    if iqy != 0:
                        i_g2_sign = iqy/qxyz[1]
                    else:
                        i_g2_sign = 1
                    if iqz != 0:
                        i_g4_sign = iqz/qxyz[2]
                    else:
                        i_g4_sign = 1
                    g2_sign += [i_g2_sign]
                    g4_sign += [i_g4_sign]
                    qxyz_list += [[qxyz[0], iqy, iqz]]
        else:
            qxyz_list = [qxyz]
            g2_sign = [1]
            g4_sign = [1]

        # run the analysis from here
        samplename = samplefolder + describe + '_jackksample'
        for igm in gmlist:

            if (igm == 'g8' or igm == 'g1') and 'ave' in describe:
                if qxyz[1]*qxyz[2] == 0 and abs(qxyz[1])+abs(qxyz[2]) != 0:
                    qxyz_list+=[[qxyz_list[0][0],qxyz_list[0][2],qxyz_list[0][1]]]
                    qxyz_list+=[[qxyz_list[1][0],qxyz_list[1][2],qxyz_list[1][1]]]

            igm_describe = describe.replace('gm', igm)
            igm_samplename = samplename.replace('gm', igm)
            igm_c3pt_format = []
            for i_fmt in c3pt_format:
                igm_c3pt_format += [i_fmt.replace('gm', igm)]

            for ipx in range(0,len(pxlist)):

                # The normalization foctor here, please note our pz is in X direction.
                p_vec_in = [Ein_list[ipx][1], pxlist[ipx]*2*3.14159/NS, 0, 0]
                p_vec_out = [Eout_list[ipx][1], (qxyz[0]+pxlist[ipx])*2*3.14159/NS, qxyz[1]*2*3.14159/NS, qxyz[2]*2*3.14159/NS]
                #print(ipx,p_vec_in,p_vec_out)

                if igm == 'g8':
                    nm_bm = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])/(p_vec_in[0]+p_vec_out[0])
                    sign_list = [1 for iq in range(0, len(qxyz_list))]
                elif igm == 'g1':
                    nm_bm = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])/(p_vec_in[1]+p_vec_out[1])
                    sign_list = [1 for iq in range(0, len(qxyz_list))]
                elif igm == 'g2':
                    nm_bm = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])/(p_vec_in[2]+p_vec_out[2])
                    sign_list = g2_sign
                elif igm == 'g4':
                    nm_bm = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])/(p_vec_in[1]+p_vec_out[1])
                    sign_list = g4_sign
                elif igm == 'g0':
                    nm_bm = 1
                    sign_list = [1 for iq in range(0, len(qxyz_list))]

                # just fit the ratio for nnow
                #nm_bm = 1
                if igm in ['g1', 'g2', 'g4']:
                    nm_bm = -nm_bm

                print('\n\n>>> PX =', pxlist[ipx], '\n')
                print('Data for c2pt (out state) will be averaged by qxyz.')
                print('Data for c3pt will be averaged by qxyz with proper sign. qxyz list:', qxyz_list, ', sign list:',sign_list)
                print(foldz_print)
                print('Ratio will be normalized (times) by (Sqrt(Eout*Ein))/((Pout_mu+Pin_mu)/2) =', round(nm_bm,3))

                ipx_describe = igm_describe.replace('*', str(pxlist[ipx]))
                ipx_describe = ipx_describe.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
                ipx_samplename = igm_samplename.replace('*', str(pxlist[ipx]))
                ipx_samplename = ipx_samplename.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))

                print('\nLoading data ...')

                # Data format
                ipx_c2pt_format = []
                for i_fmt in  c2pt_format:
                    ipx_c2pt_format += [i_fmt.replace('.bxp', '.'+bxplist[ipx])]
                ipx_c3pt_format = []
                for i_fmt in igm_c3pt_format:
                    temp = i_fmt.replace('.bxp', '.'+bxplist[ipx])
                    ipx_c3pt_format += [temp.replace('*', str(pxlist[ipx]))]

                # Read averaged data
                c2pt_data_in, c2pt_data_out = ave_c2pt_data(ipx_c2pt_format, pxlist[ipx], qxyz_list, breit=True)
                c3pt_data = ave_c3pt_data(ipx_c3pt_format, qxyz_list, tslist, zlist, c3pt_datatype, sign_list, foldz, breit=True)

                if 'ratio' in describe:
                    print('\nConstruct the ratio ...')
                    for its in range(0, len(tslist)):
                        its_describe = ipx_describe.replace('ts', 'ts'+str(tslist[its]))
                        for iz in range(0, len(zlist)):
                            iz_describe = its_describe.replace('_X', '_X'+str(zlist[iz]))

                            iz_c2pt_data_in = match_c2pt_c3pt(c2pt_data_in, c3pt_data[its][iz])
                            iz_c2pt_data_out = match_c2pt_c3pt(c2pt_data_out, c3pt_data[its][iz])
                            print('  PX:', pxlist[ipx], ', ts:', tslist[its],', z:', zlist[iz], ', data-shape:',np.shape(iz_c2pt_data_in), np.shape(c3pt_data[its][iz]))
                            
                            iz_ratio_err = gpd_ratio_jackk(c3pt_data[its][iz], iz_c2pt_data_in, iz_c2pt_data_out, tslist[its], Ein_list[ipx], Eout_list[ipx], NT, blocksize, taumin, nm_bm)
                            for itau in range(0, len(iz_ratio_err)):
                                iz_ratio_err[itau][0] -= tslist[its]/2
                            
                            out_file = resultfolder + '/' + iz_describe + '.txt'
                            np.savetxt(out_file, iz_ratio_err,fmt='%.6e')

                elif 'sumfit' in describe:
                    print('\nDoing the summation fit ...')
                    ipx_bm = []
                    for iz in range(0, len(zlist)):
                        iz_samplename = ipx_samplename.replace('_X', '_X'+str(zlist[iz])) + '.txt'

                        iz_c3pt_data = []
                        for its in range(0, len(tslist)):
                            iz_c3pt_data += [c3pt_data[its][iz]]
                        
                        iz_c2pt_data_in = match_c2pt_c3pt(c2pt_data_in, c3pt_data[its][iz])
                        iz_c2pt_data_out = match_c2pt_c3pt(c2pt_data_out, c3pt_data[its][iz])
                        print('PX:', pxlist[ipx],', z:', zlist[iz],', data-shape:',np.shape(iz_c2pt_data_in), np.shape(iz_c3pt_data), '\nEin:', Ein_list[ipx][1], ', Eout:', Eout_list[ipx][1])

                        iz_bm, iz_bm_sample = gpd_sumfit_jackk(iz_c3pt_data, iz_c2pt_data_in, iz_c2pt_data_out, tslist, Ein_list[ipx], Eout_list[ipx], NT, blocksize, taumin, nm_bm)
                        np.savetxt(iz_samplename,iz_bm_sample,fmt='%.6e')
                        iz_bm = [zlist[iz]] + iz_bm
                        ipx_bm += [iz_bm]

                    ipx_outname = resultfolder + '/' + ipx_describe + '.txt'
                    np.savetxt(ipx_outname,ipx_bm,fmt='%.6e')

                elif 'c3ptfit' in describe:

                    print('\nDoing the ratio fit ...')

                    ipx_bm = []
                    for iz in range(0, len(zlist)):

                        iz_samplename = ipx_samplename.replace('_X', '_X'+str(zlist[iz])) + '.txt'

                        iz_c3pt_data = []
                        for its in range(0, len(tslist)):
                            iz_c3pt_data += [c3pt_data[its][iz]]

                        iz_c2pt_data_in = match_c2pt_c3pt(c2pt_data_in, c3pt_data[its][iz])
                        iz_c2pt_data_out = match_c2pt_c3pt(c2pt_data_out, c3pt_data[its][iz])
                        print('PX:', pxlist[ipx], ', z:', zlist[iz],', data-shape:',np.shape(iz_c2pt_data_in), np.shape(iz_c3pt_data), '\nEin:', Ein_list[ipx][1], ', Eout:', Eout_list[ipx][1])

                        iz_bm, iz_bm_sample = gpd_ratiofit_jackk(iz_c3pt_data, iz_c2pt_data_in, iz_c2pt_data_out, tslist, Ein_list[ipx], Eout_list[ipx], NT, blocksize, taumin, nm_bm)
                        np.savetxt(iz_samplename,iz_bm_sample,fmt='%.6e')
                        iz_bm = [zlist[iz]] + iz_bm
                        ipx_bm += [iz_bm]

                    ipx_outname = resultfolder + '/' + ipx_describe + '.txt'
                    np.savetxt(ipx_outname,ipx_bm,fmt='%.6e')

                        


