#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
import inspect
from scipy.optimize import curve_fit
from statistics import *
from rITD_pion_ritdReal_moments import *

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "rITD_pion_ritdReal_moments" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def moms_exp(exp_list):
    moms_list = []
    for i in range(0, len(exp_list)):
        moms_i = 0
        for j in range(i, len(exp_list)):
            moms_i += np.exp(-exp_list[j])
        moms_list += [moms_i]
    return moms_list

def make_NLO_ritd_Pzn_pznm_real(mass, mu, nmax, k=4, pznm=0, kernel_list=[], fixN = []):
    def ritd_NLO_Pzn_pznm_real_constrained(zpz, *exp_list):
        z, pz = zpz
        '''
        momN = 0
        for i in range(0, len(exp_list)):
            momN += np.exp(-exp_list[i])
        moms = [1, 0, momN]
        for i in range(0, len(exp_list)-1):
            momN -= np.exp(-exp_list[i])
            moms += [0, momN]
        '''
        moms = [1]
        for i in range(len(exp_list)):
            moms += [0, np.exp(exp_list[i])]
        #print(moms)
        a = ritd_NLO_Pzn_pznm_real(z,pz,mass,mu,nmax,moms,k,pznm,kernel_list)
        return a
    return ritd_NLO_Pzn_pznm_real_constrained

def make_NLOEP_ritd_Pzn_pznm_real(mass, mu, nmax, k=4, pznm=0, kernel_list=[], fixN = []):
    def ritd_NLOEP_Pzn_pznm_real_constrained(zpz, *exp_list):
        z, pz = zpz
        '''
        momN = 0
        for i in range(0, len(exp_list)):
            momN += np.exp(-exp_list[i])
        moms = [1, 0, momN]
        for i in range(0, len(exp_list)-1):
            momN -= np.exp(-exp_list[i])
            moms += [0, momN]
        '''
        moms = [1]
        for i in range(len(exp_list)):
            moms += [0, np.exp(exp_list[i])]
        a = ritd_NLOEP_Pzn_pznm_real(z,pz,mass,mu,nmax,moms,k,pznm,kernel_list)
        return a
    return ritd_NLOEP_Pzn_pznm_real_constrained

def make_NNLO_ritd_Pzn_pznm_real(mass, mu, nmax, k=4, pznm=0, kernel_list=[], fixN = []):
    def ritd_NNLO_Pzn_pznm_real_constrained(zpz, *exp_list):
        #print(zpz)
        z, pz = zpz
        '''
        momN = 0
        moms = [1]
        for i in range(0, len(fixN)):
            moms += [0, fixN[i]]
        for i in range(0, len(exp_list)):
            momN += np.exp(-exp_list[i])
        moms += [0, momN]
        #print(exp_list,moms)
        for i in range(0, len(exp_list)-1):
            momN -= np.exp(-exp_list[i])
            moms += [0, momN]
        '''
        moms = [1]
        for i in range(len(exp_list)):
            moms += [0, np.exp(exp_list[i])]
        a = ritd_NNLO_Pzn_pznm_real(z,pz,mass,mu,nmax,moms,k,pznm,kernel_list)
        return a
    return ritd_NNLO_Pzn_pznm_real_constrained

def make_NNLOEP_ritd_Pzn_pznm_real(mass, mu, nmax, k=4, pznm=0, kernel_list=[], fixN = []):
    def ritd_NNLOEP_Pzn_pznm_real_constrained(zpz, *exp_list):
        #print(zpz)
        z, pz = zpz
        '''
        momN = 0
        moms = [1]
        for i in range(0, len(fixN)):
            moms += [0, fixN[i]]
        for i in range(0, len(exp_list)):
            momN += np.exp(-exp_list[i])
        moms += [0, momN]
        #print(exp_list,moms)
        for i in range(0, len(exp_list)-1):
            momN -= np.exp(-exp_list[i])
            moms += [0, momN]
        '''
        moms = [1]
        for i in range(len(exp_list)):
            moms += [0, np.exp(exp_list[i])]
        a = ritd_NNLOEP_Pzn_pznm_real(z,pz,mass,mu,nmax,moms,k,pznm,kernel_list)
        return a
    return ritd_NNLOEP_Pzn_pznm_real_constrained

def make_NNLOevo_ritd_Pzn_pznm_real(mass, mu, nmax, k=4, pznm=0, kernel_list=[], fixN = []):
    def ritd_NNLOevo_Pzn_pznm_real_constrained(zpz, *exp_list):
        #print(zpz)
        z, pz = zpz
        '''
        momN = 0
        moms = [1]
        for i in range(0, len(fixN)):
            moms += [0, fixN[i]]
        for i in range(0, len(exp_list)):
            momN += np.exp(-exp_list[i])
        moms += [0, momN]
        #print(exp_list,moms)
        for i in range(0, len(exp_list)-1):
            momN -= np.exp(-exp_list[i])
            moms += [0, momN]
        '''
        moms = [1]
        for i in range(len(exp_list)):
            moms += [0, np.exp(exp_list[i])]
        a = ritd_NNLOevo_Pzn_pznm_real(z,pz,mass,mu,nmax,moms,k,pznm,kernel_list)
        return a
    return ritd_NNLOevo_Pzn_pznm_real_constrained

'''
def make_NNLOevo_ritd_Pzn_pznm_real(mass, mu, nmax, k=4, pznm=0, kernel_list=[], fixN = []):
    def ritd_NNLOevo_Pzn_pznm_real_constrained(zpz, *exp_list):
        z, pz = zpz
        momN = 0
        for i in range(0, len(exp_list)):
            momN += np.exp(-exp_list[i])
        moms = [1, 0, momN]
        for i in range(0, len(exp_list)-1):
            momN -= np.exp(-exp_list[i])
            moms += [0, momN]

        #moms = [1]
        #for i in range(len(exp_list)):
        #    moms += [0, exp_list[i]]
        
        return ritd_NNLOevo_Pzn_pznm_real(z,pz,mass,mu,nmax,moms,k,pznm,kernel_list)
    return ritd_NNLOevo_Pzn_pznm_real_constrained
'''

'''
fitting procedure
'''
#def model_scan_loops(ritd_matchFunc, rITD, rITD_err, pznm_fm, pxlist_fm, zskip, zmaxlist, params, moms, mass, mu, nmax):
#def rITD_fit(fitfunc_name,Ns,latsp,mass,mu,nmax,KernelK,zlist,pxlist,pznm,rITD_mean,rITD_err,rITD_sample):
def rITD_fit(fitfunc_name, rITD, rITD_err, pznm_fm, pxlist_fm, zskip, zmax, mass, mu, nmax, KernelK, fixN = []):

    if 'ritdNLO' == fitfunc_name:
        fitfunc = make_NLO_ritd_Pzn_pznm_real(mass, mu, nmax, k=KernelK, pznm=pznm_fm, fixN=fixN)
    elif 'ritdNLOEP' == fitfunc_name:
        fitfunc = make_NLOEP_ritd_Pzn_pznm_real(mass, mu, nmax, k=KernelK, pznm=pznm_fm, fixN=fixN)
    elif 'ritdNNLO' == fitfunc_name:
        fitfunc = make_NNLO_ritd_Pzn_pznm_real(mass, mu, nmax, k=KernelK, pznm=pznm_fm, fixN=fixN)
    elif 'ritdNNLOEP' == fitfunc_name:
        fitfunc = make_NNLOEP_ritd_Pzn_pznm_real(mass, mu, nmax, k=KernelK, pznm=pznm_fm, fixN=fixN)
    elif 'ritdNNLOevo' == fitfunc_name:
        fitfunc = make_NNLOevo_ritd_Pzn_pznm_real(mass, mu, nmax, k=KernelK, pznm=pznm_fm, fixN=fixN)
    #elif 'ritdNNLOevo' == fitfunc_name:
    #    fitfunc = make_NNLOevo_ritd_Pzn_pznm_real(mass, mu, nmax, k=KernelK, pznm=pznm_fm, fixN=fixN)

    poptlist = []

    for izmin in range(0, len(zskip)):

        itdLat = []
        itdLatErr = []
        itdz_fm = []
        itdpz_fm = []
        for iz in range(zskip[izmin], int(zmax/latsp)+1):

            itdLat = []
            itdLatErr = []
            itdz_fm = []
            itdpz_fm = []
            for ipx in range(0, len(pxlist_fm)):
                itdLat += [rITD[ipx][iz]]
                itdLatErr += [rITD_err[ipx][iz]]
                itdz_fm += [iz*latsp]
                itdpz_fm += [pxlist_fm[ipx]]
            if len(itdLat) < int(nmax/2):
                continue
            #print(np.shape(itdz_fm))
            itd_zpz = (itdz_fm, itdpz_fm)
            for i in range(0, 10):
                try:
                    #print(itd_zpz, itdLat, itdLatErr)
                    #poptstart = [np.random.normal(j+1, 0.05*(j+1)) for j in range(0, int(nmax/2))]
                    poptstart = [np.random.normal(np.log(0.2/(j+1)), abs(0.1*(np.log(0.2/(j+1))))) for j in range(len(fixN), int(nmax/2))]
                    popt, popv = curve_fit(fitfunc, itd_zpz, itdLat, p0=poptstart, sigma=itdLatErr)
                    #print(iz, popt,moms_exp(popt))
                    break
                except:
                    #traceback.print_exc()
                    pass
            xylist = [[itdz_fm[i], itdpz_fm[i],itdLat[i],itdLatErr[i]] for i in range(0, len(itdz_fm))]
            chisq = (chisq_value_stderr(xylist, fitfunc, popt))/(len(itdz_fm)-len(popt))
            #popt = moms_exp(popt)
            popt = [np.exp(popt[j]) for j in range(0, len(popt))]
            popt = fixN + list(popt) + [chisq]
            #if len(itdz_fm) == 18:
            #    print(len(itdz_fm),popt)
            poptlist += [popt]

    return poptlist



'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    fitfunc = []
    poptguess = []
    fixN = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mass' in line[0]:
            mass = float(line[1])
            print('Hardron mass:', mass, 'GeV')

        elif 'mu' in line[0]:
            mu = float(line[1])
            print('Renormalization scale:', mu, 'GeV')

        elif 'zskip' in line[0]:
            zskip = [int(line[i]) for i in range(1, len(line))]
            print('Skip zmin (z/a) =', zskip)
            describe = describe.replace('zmin', 'zmin'+str(zskip[0]))

        elif 'zmax(fm)' in line[0]:
            zmax = float(line[1])
            print('Zmax =', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmax))

        elif 'zmax/a' in line[0]:
            zmaxa = int(line[1])
            zmax = (zmaxa+0.5)*latsp
            print('Zmax =', str(zmaxa)+'a', '=', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmaxa)+'a')
            #print(describe)

        elif 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])

        elif 'nmax' in line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)

        elif 'KernelK' in line[0]:
            KernelK = float(line[1])
            print('rITD kernel k =', KernelK)

        elif 'fixN' in line[0]:
            fixN = [float(line[i]) for i in range(1, len(line))]
            print('rITD fixed moments:', fixN)

        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            pxlist_fm = []
            pxlist_GeV = []
            for ipx in pxlist:
                pxlist_fm += [2*PI*ipx/(Ns*latsp)]
                pxlist_GeV += [round(2*PI*ipx/(Ns*latsp)/fmGeV,2)]
            print('pzlist:',pxlist_GeV,'(GeV)')
            if len(pxlist) == 1:
                describe = describe.replace('pzup','pz'+str(pxmin))
            #print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            pznm_fm = 2*PI*pznm/(Ns*latsp)
            pznm_GeV = round(2*PI*pznm/(Ns*latsp)/fmGeV,2)
            print('pznm :',pznm_GeV,'(GeV)')

        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

        elif 'ritd' == line[0]:
            fitfunc += [line[1]]
            
            #poptguess += [list(map(float, line[2:]))]

    # Read the data here
    rITD_mean = [] #rITD_mean[ipx][iz]
    rITD_err = [] #rITD_mean_err[ipx][iz]
    rITD_sample_read = [] #rITD_sample[ipx][iz][isample]
    rITD_sample = [] #rITD_sample[isample][ipx][iz]
    rITD_samp_lenMax = 0
    for ipx in pxlist:

        ipx_mean_file = mean_input_format.replace('*', str(ipx))
        ipx_mean_data = np.loadtxt(ipx_mean_file)
        ipx_rITD_mean = []
        ipx_rITD_err = []
        zlen = len(ipx_mean_data)
        for iz in range(0, zlen):
            ipx_rITD_mean += [ipx_mean_data[iz][1]]
            ipx_rITD_err += [ipx_mean_data[iz][2]]
        rITD_mean += [ipx_rITD_mean]
        rITD_err += [ipx_rITD_err]

        ipx_sample_file = sample_input_format.replace('*', str(ipx))
        ipx_rITD_sample = []
        for iz in range(0, zlen):
            iz_sample_file = ipx_sample_file.replace('#', str(iz))
            ipx_rITD_sample += [np.loadtxt(iz_sample_file)]
            if len(ipx_rITD_sample[iz]) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(ipx_rITD_sample[iz])
        rITD_sample_read += [ipx_rITD_sample]
    print('rITD data shape:', np.shape(rITD_sample_read), ', sample maximal length:', rITD_samp_lenMax)

    for isamp in range(0,rITD_samp_lenMax):
        ipx_rITD_sample = []
        for ipx in range(0, len(pxlist_fm)):
            iz_rITD_sample = []
            for iz in range(0, zlen):
                rITD_samp_len = len(rITD_sample_read[ipx][iz])
                if isamp < rITD_samp_len:
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp]]
                else:
                    #iz_rITD_sample += [rITD_sample_read[ipx][iz][np.random.randint(0, rITD_samp_len)]]
                    iz_rITD_sample += [rITD_mean[ipx][iz]]
            ipx_rITD_sample += [iz_rITD_sample]
        rITD_sample += [ipx_rITD_sample]

    print('mean data:', np.shape(rITD_mean), ', sample data:', np.shape(rITD_sample))
    
    for ifit in range(0, len(fitfunc)):

        moms_list = []

        out_name = describe.replace('*', str(pxlist[0])+'-'+str(pxlist[-1]))
        out_name = outfolder+out_name.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit])) + '.txt'
        out_name = out_name.replace('evo','evoK'+str(KernelK))

        poptmean = rITD_fit(fitfunc[ifit], rITD_mean, rITD_err, pznm_fm, pxlist_fm, zskip, zmax, mass*fmGeV, mu*fmGeV, nmax, KernelK, fixN)
        #print(len(poptmean))
        poptsample = []

        
        for isamp in range(0, rITD_samp_lenMax):
            poptsample += [rITD_fit(fitfunc[ifit], rITD_sample[isamp], rITD_err, pznm_fm, pxlist_fm, zskip, zmax, mass*fmGeV, mu*fmGeV, nmax, KernelK, fixN)]
                
        iz = -1
        for izmin in range(0, len(zskip)):
            for izmax in range(zskip[izmin], int(zmax/latsp)+1):
                if (izmax-zskip[izmin]+1)*len(pxlist) < int(nmax/2):
                    continue
                iz += 1
                iz_moms_err = [0 for ipopt in range(0, len(poptmean[iz])-1)]
                for isamp in range(0, rITD_samp_lenMax):
                    for ipopt in range(0, len(iz_moms_err)):
                        iz_moms_err[ipopt] += (poptmean[iz][ipopt]-poptsample[isamp][iz][ipopt])**2
                iz_moms = [zskip[izmin]*latsp, izmax*latsp]
                for ipopt in range(0, len(iz_moms_err)):
                    iz_moms += [poptmean[iz][ipopt], np.sqrt(iz_moms_err[ipopt]*(rITD_samp_lenMax-1)/rITD_samp_lenMax)]
                iz_moms += [poptmean[iz][-1]]
                np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
                print(iz_moms)
                moms_list += [iz_moms]
        #print(out_name)
        np.savetxt(out_name, moms_list, fmt='%.4e')
        
