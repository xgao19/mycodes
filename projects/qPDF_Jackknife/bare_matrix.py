#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
from tools import *
import inspect
from scipy.optimize import curve_fit

#Ns=64
#latsp=0.04
#mu=3.2
#alps=0.25

def jackkbm_collect(mean_format,col_mean,sample_format,col_sample,zlist,pxlist):
    
    #sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    #njackk = len(sampledata)

    # data collection
    bm_mean_data = []
    bm_samples_data = []
    for ipx in range(0,len(pxlist)):
        #sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[ipx]))).replace('#', str(zlist[0]))))
        #njackk = len(sampledata)

        ipx_mean_name_real = mean_format.replace('*', str(pxlist[ipx]))
        ipx_mean_data_real = np.loadtxt(ipx_mean_name_real)
        if 'real' in ipx_mean_name_real:
            ipx_mean_name_imag = ipx_mean_name_real.replace('real', 'imag')
            ipx_mean_data_imag = np.loadtxt(ipx_mean_name_imag)
            ipx_bm_mean_data = [complex(ipx_mean_data_real[iz][col_mean],ipx_mean_data_imag[iz][col_mean]) for iz in zlist]
        else:
            ipx_bm_mean_data = [ipx_mean_data_real[iz][col_mean] for iz in zlist]
        bm_mean_data += [ipx_bm_mean_data]
        #print(bm_mean_data)

        ipx_in_format = sample_format.replace('*', str(pxlist[ipx]))
        ipx_data = []

        for iz in range(0, len(zlist)):
            iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            jacksample_real = np.loadtxt(iz_ipx_in_name)
            #print(jacksample_real)
            if 'real' in ipx_mean_name_real:
                jacksample_imag = np.loadtxt(iz_ipx_in_name.replace('real','imag'))
                try:
                    jacksample = [complex(jacksample_real[i][col_sample],jacksample_imag[i][col_sample]) for i in range(0, len(jacksample_real))]
                except:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, len(jacksample_real))]
            else:
                try:
                    jacksample = [jacksample_real[i][col_sample] for i in range(0, len(jacksample_real))]
                except:
                    jacksample = [jacksample_real[i] for i in range(0, len(jacksample_real))]
            ipx_data += [jacksample]
        bm_samples_data += [ipx_data]
    print('bare matrix mean shape:', np.shape(bm_mean_data),'\n')
    print('bare matrix samples shape:', np.shape(bm_samples_data),'\n')
    return bm_mean_data,bm_samples_data

def jackkbm_collect_excited(mean_format,col_mean,sample_format,col_sample,zlist,pxlist):
    
    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)

    # data collection
    bm_mean_data = []
    bm_samples_data = []
    for ipx in range(0,len(pxlist)):
        sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[ipx]))).replace('#', str(zlist[0]))))
        njackk = len(sampledata)

        ipx_mean_name_real = mean_format.replace('*', str(pxlist[ipx]))
        ipx_mean_data_real = np.loadtxt(ipx_mean_name_real)
        if 'real' in ipx_mean_name_real:
            ipx_mean_name_imag = ipx_mean_name_real.replace('real', 'imag')
            ipx_mean_data_imag = np.loadtxt(ipx_mean_name_imag)
            ipx_bm_mean_data = [complex(ipx_mean_data_real[iz][col_mean],ipx_mean_data_imag[iz][col_mean]) for iz in zlist]
        else:
            ipx_bm_mean_data = [ipx_mean_data_real[iz][col_mean]/ipx_mean_data_real[iz][5]*ipx_mean_data_real[iz][1] for iz in zlist]
            #print(ipx_bm_mean_data)
        bm_mean_data += [ipx_bm_mean_data]
        #print(bm_mean_data)

        ipx_in_format = sample_format.replace('*', str(pxlist[ipx]))
        ipx_data = []

        for iz in range(0, len(zlist)):
            iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            jacksample_real = np.loadtxt(iz_ipx_in_name)
            #print(jacksample_real)
            if 'real' in ipx_mean_name_real:
                jacksample_imag = np.loadtxt(iz_ipx_in_name.replace('real','imag'))
                try:
                    jacksample = [complex(jacksample_real[i][col_sample],jacksample_imag[i][col_sample]) for i in range(0, njackk)]
                except:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, njackk)]
            else:
                try:
                    jacksample = [jacksample_real[i][col_sample]/jacksample_real[i][2]*jacksample_real[i][0] for i in range(0, njackk)]
                except:
                    jacksample = [jacksample_real[i] for i in range(0, njackk)]
            ipx_data += [jacksample]
        bm_samples_data += [ipx_data]
    print('bare matrix mean shape:', np.shape(bm_mean_data),'\n')
    print('bare matrix samples shape:', np.shape(bm_samples_data),'\n')
    return bm_mean_data,bm_samples_data


# extract mean and error bare matrix from bootstrap sample
def bootsbm(Ns,latsp,sample_format,zlist,pxlist,outfolder,describe,qlist=['qx0.qy0.qz0']):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)
    

    sampledata = ((np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0])))))[0:500]
    nboots = len(sampledata)
    low16 = int(np.ceil(0.16*nboots))
    high16 = int(np.floor(0.84*nboots))
    mid = int(0.5*nboots)

    for iq in range(0, len(qlist)):

        iq_input_format = sample_format.replace('qx0.qy0.qz0',qlist[iq])
        iq_out_format = outfolder + describe
        iq_out_format = iq_out_format.replace('qx0.qy0.qz0',qlist[iq])
        
        for ipx in range(0,len(pxlist)):
            ipx_in_format = iq_input_format.replace('*',str(pxlist[ipx]))
            ipx_out_name = iq_out_format.replace('*', str(pxlist[ipx]))
            ipx_out_data = []
            
            for iz in range(0, len(zlist)):
                iz_ipx_in_name = ipx_in_format.replace('#',str(zlist[iz]))
                ibootsvalue = (np.loadtxt(iz_ipx_in_name))[0:500]
                ibootsvalue = [ibootsvalue[i][0] for i in range(0, nboots)]
                ibootsvalue = sorted(ibootsvalue)
                ipx_out_data += [[zlist[iz], ibootsvalue[mid], ibootsvalue[mid]-ibootsvalue[low16], ibootsvalue[high16]-ibootsvalue[mid]]]
            
            ipx_out_name = ipx_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
            print(ipx_out_name)
            np.savetxt(ipx_out_name, ipx_out_data)

def bootsbm_to_rITD(Ns,latsp,sample_format,zlist,pxlist,outfolder,describe,qlist=['qx0.qy0.qz0']):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    sampledata = ((np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0])))))[0:500]
    nboots = len(sampledata)
    low16 = int(np.ceil(0.16*nboots))
    high16 = int(np.floor(0.84*nboots))
    mid = int(0.5*nboots)

    # data collection
    bm_data = []
    for iq in range(0, len(qlist)):

        iq_input_format = sample_format.replace('qx0.qy0.qz0',qlist[iq])
        iq_input_format = iq_input_format.replace('qx0.qy0.qz0',qlist[iq])

        iq_data = []
        for ipx in range(0,len(pxlist)):
            
            ipx_in_format = iq_input_format.replace('*', str(pxlist[ipx]))
            ipx_data = []
            
            for iz in range(0, len(zlist)):
                
                iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
                ibootsvalue = (np.loadtxt(iz_ipx_in_name))[0:500]
                ibootsvalue = [ibootsvalue[i][0] for i in range(0, nboots)]
                ipx_data += [ibootsvalue]

            iq_data += [ipx_data]

        bm_data += [iq_data]
    print(np.shape(bm_data))
    
    # reduced ioffe time distribution
    for iq in range(0, len(qlist)):

        iq_out_format = outfolder + describe
        iq_out_format = iq_out_format.replace('qx0.qy0.qz0',qlist[iq])

        for ipx in range(0, len(pxlist)):

            ipx_ritd = []
            ipx_out_name = iq_out_format.replace('*', str(pxlist[ipx]))

            for iz in range(0, len(zlist)):
            
                iz_ipx_ritd = []
                bootsvalue = []
                
                for iboots in range(0, nboots):
                    bootsvalue += [bm_data[iq][ipx][iz][iboots]/bm_data[0][0][iz][iboots]*bm_data[0][0][0][iboots]/bm_data[0][ipx][0][iboots]]

                bootsvalue = sorted(bootsvalue)
                iz_ipx_ritd = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, bootsvalue[mid], bootsvalue[mid]-bootsvalue[low16], bootsvalue[high16]-bootsvalue[mid]]
                if pxlist[ipx] == 0:
                    iz_ipx_ritd[0] = zlist[iz]
                ipx_ritd += [iz_ipx_ritd]
            ipx_out_name = ipx_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
            print(ipx_out_name)
            np.savetxt(ipx_out_name,ipx_ritd)

def jackk1bm_save(Ns,latsp,mean_format,col_mean,sample_format,col_sample,zlist,pxlist,outfolder,describe):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)
    
    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)

    for iz in range(0, len(zlist)):
        if zlist[iz] == 0:
            z0 = iz
    
    # data collection
    bm_mean_data = []
    bm_samples_data = []
    bm_mean_data, bm_samples_data = jackkbm_collect_excited(mean_format,col_mean,sample_format,col_sample,zlist,pxlist)
        
    # reduced bm
    rbm_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_jackksamples'
    
    for ipx in range(0, len(pxlist)):
        
        ipx_rbm_real = []
        ipx_rbm_imag = []
        
        for iz in range(0, len(zlist)):

            jackksample_real = []
            jackksample_imag = []
            jackkmean = bm_mean_data[ipx][iz]
            jackkerr_real = 0
            jackkerr_imag = 0
            #print(bm_mean_data[0][0],bm_samples_data[0][0])

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, njackk):
                isample = bm_samples_data[ipx][iz][isamp]
                #isample = bm_samples_data[ipx][iz][isamp]/bm_mean_data[ipx][z0]
                jackksample_real += [isample.real]
                jackksample_imag += [isample.imag]
                jackkerr_real += (jackkmean.real-isample.real)*(jackkmean.real-isample.real)
                jackkerr_imag += (jackkmean.imag-isample.imag)*(jackkmean.imag-isample.imag)
            ipx_iz_rbm_real = [zlist[iz], jackkmean.real, np.sqrt(jackkerr_real*(njackk-1)/njackk)]
            ipx_iz_rbm_imag = [zlist[iz], jackkmean.imag, np.sqrt(jackkerr_imag*(njackk-1)/njackk)]
            ipx_rbm_real += [ipx_iz_rbm_real]
            ipx_rbm_imag += [ipx_iz_rbm_imag]
            np.savetxt(samplefile, jackksample_real)
            if 'real' in samplefile:
                np.savetxt(samplefile.replace('real','imag'), jackksample_imag)
        rbm_out_name = rbm_out_format.replace('*',str(pxlist[ipx]))
        rbm_out_name = rbm_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print(rbm_out_name)
        np.savetxt(rbm_out_name,ipx_rbm_real)
        if 'real' in rbm_out_name:
            np.savetxt(rbm_out_name.replace('real', 'imag'),ipx_rbm_imag)

def jackkbm_reduce(Ns,latsp,mean_format,col_mean,sample_format,col_sample,zlist,pxlist,outfolder,describe):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)
    
    #sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk_max = 0

    for iz in range(0, len(zlist)):
        if zlist[iz] == 0:
            z0 = iz
    
    # data collection
    bm_mean_data = []
    bm_samples_data = []
    bm_mean_data, bm_samples_data = jackkbm_collect(mean_format,col_mean,sample_format,col_sample,zlist,pxlist)

    for ipx in range(0, len(pxlist)):
        for iz in range(0, len(zlist)):
            if len(bm_samples_data[ipx][iz]) > njackk_max:
                njackk_max = len(bm_samples_data[ipx][iz])
        
    # reduced bm
    rbm_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_jackksamples'
    
    for ipx in range(0, len(pxlist)):
        
        ipx_rbm_real = []
        ipx_rbm_imag = []
        
        for iz in range(0, len(zlist)):

            jackksample_real = []
            jackksample_imag = []
            jackkmean = bm_mean_data[ipx][iz]/bm_mean_data[ipx][z0]
            jackkerr_real = 0
            jackkerr_imag = 0
            #print(bm_mean_data[0][0],bm_samples_data[0][0])

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, njackk_max):

                if isamp < len(bm_samples_data[ipx][z0]):
                    isample_z0 = bm_samples_data[ipx][z0][isamp]
                else:
                    isample_z0 = bm_mean_data[ipx][z0]
                    
                if isamp < len(bm_samples_data[ipx][iz]):
                    isample = bm_samples_data[ipx][iz][isamp]/isample_z0
                else:
                    isample = bm_mean_data[ipx][iz]/isample_z0
                jackksample_real += [isample.real]
                jackksample_imag += [isample.imag]
                jackkerr_real += (jackkmean.real-isample.real)*(jackkmean.real-isample.real)
                jackkerr_imag += (jackkmean.imag-isample.imag)*(jackkmean.imag-isample.imag)
            ipx_iz_rbm_real = [zlist[iz], jackkmean.real, np.sqrt(jackkerr_real*(njackk_max-1)/njackk_max)]
            ipx_iz_rbm_imag = [zlist[iz], jackkmean.imag, np.sqrt(jackkerr_imag*(njackk_max-1)/njackk_max)]
            ipx_rbm_real += [ipx_iz_rbm_real]
            ipx_rbm_imag += [ipx_iz_rbm_imag]
            np.savetxt(samplefile, jackksample_real)
            np.savetxt(samplefile.replace('real','imag'), jackksample_imag)
        rbm_out_name = rbm_out_format.replace('*',str(pxlist[ipx]))
        rbm_out_name = rbm_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print(rbm_out_name)
        np.savetxt(rbm_out_name,ipx_rbm_real)
        #np.savetxt(rbm_out_name.replace('real', 'imag'),ipx_rbm_imag)

def jackkbm_to_rITD(Ns,latsp,mean_format,col_mean,sample_format,col_sample, zlist, pxlist, outfolder, describe):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)
    
    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    p0_cfg = len(sampledata)

    for iz in range(0, len(zlist)):
        if zlist[iz] == 0:
            z0 = iz
    
    # data collection
    bm_mean_data = []
    bm_samples_data = []
    bm_mean_data, bm_samples_data = jackkbm_collect(mean_format,col_mean,sample_format,col_sample,zlist,pxlist)
        
    # reduced ioffe time distribution
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_jackksamples'
    ritd = []
    
    for ipx in range(0, len(pxlist)):
        
        ipx_ritd_real = []
        ipx_ritd_imag = []
        px_cfg = len(bm_samples_data[ipx][0])
        if px_cfg != p0_cfg:
            njackk = px_cfg
        else:
            njackk = p0_cfg
        print('njackk:', njackk)
        
        for iz in range(0, len(zlist)):

            ipx_iz_ritd_real = []
            ipx_iz_ritd_imag = []
            jackksample_real = []
            jackksample_imag = []
            #print(bm_mean_data)
            jackkmean = bm_mean_data[ipx][iz]/bm_mean_data[0][iz]*bm_mean_data[0][0]/bm_mean_data[ipx][0]
            #jackkmean = bm_mean_data[ipx][iz]/bm_mean_data[0][iz]
            #print(iz, bm_mean_data[ipx][iz][col_mean], bm_mean_data[0][iz][col_mean], bm_mean_data[ipx][iz][col_mean]/bm_mean_data[0][iz][col_mean])
            jackkerr_real = 0
            jackkerr_imag = 0

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, njackk):
                if px_cfg == p0_cfg:
                    #isample = bm_samples_data[ipx][iz][isamp]/bm_samples_data[0][iz][isamp]
                    isample = bm_samples_data[ipx][iz][isamp]/bm_samples_data[0][iz][isamp]*bm_samples_data[0][0][isamp]/bm_samples_data[ipx][0][isamp]
                else:
                    #print('111')
                    #isample = bm_samples_data[ipx][iz][isamp]/bm_mean_data[0][iz][col_mean]
                    #print(bm_mean_data)
                    isample = bm_samples_data[ipx][iz][isamp]/bm_mean_data[0][iz]*bm_mean_data[0][0]/bm_samples_data[ipx][0][isamp]
                    #print(isamp, jackkmean, isample,bm_mean_data[ipx][iz][col_mean],bm_samples_data[ipx][iz][isamp])
                jackksample_real += [isample.real]
                jackksample_imag += [isample.imag]
                jackkerr_real += (jackkmean.real-isample.real)*(jackkmean.real-isample.real)
                jackkerr_imag += (jackkmean.imag-isample.imag)*(jackkmean.imag-isample.imag)

            ipx_iz_ritd_real = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, jackkmean.real, np.sqrt(jackkerr_real*(njackk-1)/njackk)]
            ipx_iz_ritd_imag = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, jackkmean.imag, np.sqrt(jackkerr_imag*(njackk-1)/njackk)]
            #ipx_iz_ritd_real = [zlist[iz]*0.042, jackkmean.real, np.sqrt(jackkerr_real*(njackk-1)/njackk)]
            #ipx_iz_ritd_imag = [zlist[iz]*0.042, jackkmean.imag, np.sqrt(jackkerr_imag*(njackk-1)/njackk)]
            ipx_ritd_real += [ipx_iz_ritd_real]
            ipx_ritd_imag += [ipx_iz_ritd_imag]
            np.savetxt(samplefile+'.real', jackksample_real)
            np.savetxt(samplefile+'.imag', jackksample_imag)

        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print(ritd_out_name)
        ritd_out_name_real = ritd_out_name + '.real'
        ritd_out_name_imag = ritd_out_name + '.imag'
        np.savetxt(ritd_out_name_real,ipx_ritd_real)
        np.savetxt(ritd_out_name_imag,ipx_ritd_imag)


if __name__ == "__main__":

    inputfile = open(sys.argv[2],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[2])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        if 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)

        if 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])
        
        if 'mean' in line[0]:
            mean_input_format = line[1]
            col_mean = int(line[2])

        if 'sample' in line[0]:
            sample_input_format = line[1]
            col_sample = int(line[2])

    if sys.argv[1] == 'jackritd':
        jackkbm_to_rITD(Ns,latsp,mean_input_format,col_mean,sample_input_format,col_sample,zlist,pxlist,outfolder,describe)
    elif sys.argv[1] == 'jackrbm':
        jackkbm_reduce(Ns,latsp,mean_input_format,col_mean,sample_input_format,col_sample,zlist,pxlist,outfolder,describe)
    elif sys.argv[1] == 'bootsritd':
        bootsbm_to_rITD(Ns,latsp,sample_input_format,zlist,pxlist,outfolder,describe)
    elif sys.argv[1] == 'bootsbm':
        bootsbm(Ns,latsp,sample_input_format,zlist,pxlist,outfolder,describe)
    elif sys.argv[1] == 'jack1bm':
        jackk1bm_save(Ns,latsp,mean_input_format,col_mean,sample_input_format,col_sample,zlist,pxlist,outfolder,describe)