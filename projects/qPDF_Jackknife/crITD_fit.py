#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
from tools import *
import inspect
from scipy.optimize import curve_fit
from modelfuncs import *
from statistics import *

Ns=48
latsp=0.06
mu0=3.2
alps0=0.25
lambdaQCD = 0.2

#input_format_boots = '/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_64c64_data/analysis/nikhil_data/BOOT_64c64/boot.px0.qx0.qy0.qz0.g8.1hyp.nst2.nsk2.z0.dat'
#input_format_jackk = '/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_64c64_data/analysis/rITD_ts3/results/samples/rITD_px0qx0_z0_ama_sumfit_g8_3ts_taumin3_jackksamples.txt'
#input_mean = '/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_64c64_data/analysis/rITD_ts3/results/rITD_px0-5qx0_z0_ama_sumfit_g8_3ts_taumin3.txt'
#outfolder = '/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_64c64_data/analysis/rITD_ts3/results/'
#samplefolder = outfolder + 'fit_samples/'

'''
model functions
'''
# rITD fit functions
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])
def Cn(z,n, mu = mu0*5.0676896,alps = alps0,latsp = latsp):

    CF = 4/3
    #latsp = 0.04
    
    gammaE = 0.5772156649
    pi = 3.14159
    #mu = 3.2*5.0676896  # fm^-1
    z = z * latsp
    #alps = 0.25
    
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))
def rioffe_ope_mom2(xz, a3):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2)

def rioffe_ope_mom4(xz, a3, a5):
    #a3 = 1.029078e-01
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24)

def rioffe_ope_mom6(xz, a3, a5, a7):
    #a3 = 1.079096e-01
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720)

def rioffe_ope_mom8(xz, a3, a5, a7, a9):
    #a3 = 1.079096e-01
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720) + Cn(z,8)/Cn(z,0) * a9 * (x*x*x*x*x*x*x*x/40320)

def rioffe_ope_mom10(xz, a3, a5, a7, a9, a11):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720) + Cn(z,8)/Cn(z,0) * a9 * (x*x*x*x*x*x*x*x/40320) - Cn(z,10)/Cn(z,0) * a11 * (x*x*x*x*x*x*x*x*x*x/3628800)
def rioffe_ope_mom12(xz, a3, a5, a7, a9, a11, a13):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720) + Cn(z,8)/Cn(z,0) * a9 * (x*x*x*x*x*x*x*x/40320) - Cn(z,10)/Cn(z,0) * a11 * (x*x*x*x*x*x*x*x*x*x/3628800) + Cn(z,12)/Cn(z,0) * a13 * (x*x*x*x*x*x*x*x*x*x*x*x/479001600)
def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)

class rif_func_list:

    rif_mom2 = rioffe_ope_mom2
    rif_mom4 = rioffe_ope_mom4
    rif_mom6 = rioffe_ope_mom6
    rif_mom8 = rioffe_ope_mom8
    rif_mom10 = rioffe_ope_mom10
    rif_mom12 = rioffe_ope_mom12
    poly2 = polynomial2

def Cratio(z, mu):
    z /= latsp
    return Cn(z,2,mu=mu*5.0676896)/Cn(z,0,mu=mu*5.0676896)*0.1
def Cratio_exp(z, mu):
    z /= latsp
    #print(np.exp(Cn(z,2,mu=mu*5.0676896)/Cn(z,0,mu=mu*5.0676896)-1)*0.1)
    return np.exp(Cn(z,2,mu=mu*5.0676896)-Cn(z,0,mu=mu*5.0676896))*0.1
def Cratio_evo(z, a3):
    z /= latsp
    muz = 4/(z*latsp)
    l = lambdaQCD * 5.0676896
    alps = 4*3.14159/9/np.log(muz*muz/l/l)
    print(muz/5.0676896,alps,Cn(z,2,mu=muz,alps=alps),Cn(z,2,mu=muz,alps=alps)/Cn(z,0,mu=muz,alps=alps),(np.log(muz*muz/l/l)/np.log(mu0*mu0*5.0676896*5.0676896/l/l)))
    return Cn(z,2,mu=muz,alps=alps)/Cn(z,0,mu=muz,alps=alps) * 0.1 *((np.log(muz*muz/l/l)/np.log(mu0*mu0*5.0676896*5.0676896/l/l))**-(0.617284))
    
'''
fitting procedure
'''
def rITD_fit(fitfunc, input_mean_format, sample_format, zlist, pxlist, poptstart=None, xvaluemin=-1e50, xvaluemax=1e50):

    fitfunc = getattr(rif_func_list, fitfunc)

    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)
    
    # jackknife samples data collection
    ritd_data = [] # list[z][px][mu]
    x1 = []
    ritd_mean_data = []
    for ipx in range(0, len(pxlist)):

        ipx_mean_name = input_mean_format.replace('*', str(pxlist[ipx]))
        ipx_ritd_mean_data_load = np.loadtxt(ipx_mean_name)
        #print(ipx_mean_name,'\n',ipx_ritd_mean_data_load)

        ipx_in_format = sample_format.replace('*', str(pxlist[ipx]))
        ipx_ritd_data = []
        x1z = []
        #print(iz_ritd_mean_data)
        iz_ritd_mean_data = []

        for iz in range(0, len(zlist)):

            ipx_iz_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            ipx_iz_ritd_data = np.loadtxt(ipx_iz_in_name)
            izpx = pxlist[ipx]*zlist[iz]*2*3.14259/Ns
            #print(ipx_iz_in_name )

            #print(izpx)
            if izpx > xvaluemin and izpx < xvaluemax:
                iz_ritd_mean_data += [ipx_ritd_mean_data_load[zlist[iz]][1]]
                ipx_iz_ritd_data = [izpx] + list(ipx_iz_ritd_data)
                ipx_ritd_data += [ipx_iz_ritd_data]
                x1z += [zlist[iz]]
        if len(iz_ritd_mean_data) > 0:
            ritd_mean_data += iz_ritd_mean_data
            x1 += x1z
            ritd_data += ipx_ritd_data

    #print(x1,ritd_data)
    
    cov_matrix = jackksample_cov(ritd_mean_data,ritd_data,stderr=False)
    cov_matrix = [np.sqrt(cov_matrix[i][i]) for i in range(0, len(cov_matrix))]

    # fitting loop
    x2 = np.array([ritd_data[j][0] for j in range(0, len(ritd_data))])
    x = (x1, x2)
    print(len(x1),len(x2))
    y = ritd_mean_data
    #print(y)
    if '' in str(fitfunc):

        poptmean, popv = curve_fit(fitfunc, x, y, poptstart,sigma=cov_matrix)
        #print(poptmean)
        xylist = [[x1[i], x2[i],y[i],cov_matrix[i]] for i in range(0, len(x1))]
        #print('!',chisq_value_stderr(xylist, fitfunc, poptmean, cov_matrix))
        chisq = (chisq_value_stderr(xylist, fitfunc, poptmean))/len(x1)
        #print(xylist)
        print('!!',poptmean,chisq)
        poptmean = list(poptmean) + [chisq]
        popterr = np.zeros(len(poptmean))

        for i in range(0, njackk):

            y = [ritd_data[j][i+1] for j in range(0, len(ritd_data))]
            popt,popv = curve_fit(fitfunc, x, y, poptstart, sigma=cov_matrix)

            xylist = [[x1[i], x2[i],y[i],cov_matrix[i]] for i in range(0, len(y))]
            chisq = (chisq_value_stderr(xylist, fitfunc, popt))/len(x1)
            popt = list(popt) + [chisq]

            for ip in range(0, len(popt)):
                popterr[ip] += (popt[ip]-poptmean[ip]) * (popt[ip]-poptmean[ip])

        meanerr = []
        for i in range(0, len(popt)):
            meanerr += [poptmean[i], np.sqrt(popterr[i]*(njackk-1)/njackk)]

        return meanerr

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    fitfunc = []
    poptguess = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        if 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)

        if 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])
        
        if 'mean' in line[0]:
            mean_input_format = line[1]

        if 'sample' in line[0]:
            sample_input_format = line[1]

        if 'fitfunc' in line[0]:
            fitfunc += [line[1]]
            poptguess += [list(map(float, line[2:]))]
    
    for ifit in range(0, len(fitfunc)):

        z_rITDfit = []
        
        for iz in range(0, len(zlist)):

            out_name = describe.replace('*', str(pxlist[0])+'-'+str(pxlist[-1]))
            out_name = out_name.replace('#', str(zlist[0])+'-'+str(zlist[-1]))
            out_name = outfolder+out_name+'_'+str(fitfunc[ifit])

            #print(poptguess[ifit])
            #popt = rITD_fit(fitfunc[ifit],mean_input_format,sample_input_format,[zlist[iz]], pxlist, poptguess[ifit], xvalue_min, xvalue_max)
            popt = rITD_fit(fitfunc[ifit],mean_input_format,sample_input_format,[zlist[i] for i in range(iz, iz+1)], pxlist, poptguess[ifit], xvalue_min, xvalue_max)
            popt = [zlist[iz]*latsp] + popt
            z_rITDfit += [popt]
        
        np.savetxt(out_name, z_rITDfit, fmt='%.6e')

