#!/usr/bin/env python3

import modelfuncs
#from stderr import *
from chisqfit_cov_jb import *
from modelfuncs import *
from tools import *
from plot import *
from statistics import *
from c3c2tools import *
import os
import sys
import matplotlib
import numpy as np
from matplotlib import pyplot as plt

random_number_file = 'random_number_1e7'

#------------------------------------------------------------#
################## help & code introduction ##################
#------------------------------------------------------------#

if len(sys.argv) <=1 or sys.argv[1]=='--help':
        print('This code contain 3 options for now:\n')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('>>>>>>>>>>>>>>>>>>>>>>>> @1 average&std error >>>>>>>>>>>>>>>>>>>>>>>>')
        print('-------------------------------------------------------------------')
        print('./main.py stderr commandlist.txt')
        print('-------------------------------------------------------------------')
        print('Each line of commandlist.txt should be:')
        print('datafile.csv fold=Ture/False\n')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('>>>>>>>>>>>>>>>>>>>>>>>> @2 average&std error >>>>>>>>>>>>>>>>>>>>>>>>')
        print('-------------------------------------------------------------------')
        print('./main.py fit commandlist.txt')
        print('-------------------------------------------------------------------')
        print('Each line of commandlist.txt should be:')
        print('#1 fit all possible x range:')
        print('(1.1): fitfunc dataset.csv f/nf allx  stderr param_guess1 param_guess2 ...')
        print('(1.2): fitfunc dataset.csv f/nf allx  covar param_guess1 param_guess2 ...')
        print('#2 particular x range with jackknife analysis:')
        print('(2.1): fitfunc dataset.csv f/nf jackk  stderr  xminl xminr xmax  blocksize param_guess1 param_guess2 ...')
        print('(2.2): fitfunc dataset.csv f/nf jackk  covar  xminl xminr xmax  blocksize param_guess1 param_guess2 ...')
        print('#3 particular x range with bootstrap analysis:')
        print('(3.1): fitfunc dataset.csv f/nf boots  stderr  xminl xminr xmax  blocksize  npick param_guess1 param_guess2 ...')
        print('(3.2): fitfunc dataset.csv f/nf boots  covar  xminl xminr xmax  blocksize  npick param_guess1 param_guess2 ...')
        print('f/nf means if you want to fold the data in half.\n')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>> @3 error bar plot >>>>>>>>>>>>>>>>>>>>>>>>>')
        print('***********jackknife data errbar plot with curve')
        print('-------------------------------------------------------------------')
        print('./main.py jackkplot commandlist.txt')
        print('-------------------------------------------------------------------')
        print('The commandlist.txt should be arranged as:')
        print('xydata.txt func=None scale_log=False \nxmin xmax param_guess1 param_guess2 ...\nxmin xmax param_guess1 param_guess2 ...\n')
        print('***********bootstrap data errbar plot')
        print('-------------------------------------------------------------------')
        print('./main.py bootsplot commandlist.txt')
        print('-------------------------------------------------------------------')
        print('The commandlist.txt should be arranged as:')
        print('plottitle xlabel ylabel ymin ymax\n')
        print('user/output/folder\n')
        print('datalabel ./datapostion x_col y_col yerr-_col yerr+_col\n')
        print('...\n')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>> @4 aicc average >>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('-------------------------------------------------------------------')
        print('./main.py aiccave dataset.txt position1 position2 ...')
        print('-------------------------------------------------------------------')
        print('The dataset.txt should be arranged as:')
        print('xmin xmax param0 error0 param1 ... chisq/dof aicc\n')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>@5 aicc error >>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print('-------------------------------------------------------------------')
        print('./main.py aicc commandlist.txt')
        print('-------------------------------------------------------------------')
        print('Tdataset.csv f/nf xminl xminr xmax npick')
        print('The command file should be format as:')
        print('fitfunc1 [paramguess1]')
        print('fitfunc2 [paramguess2]')
        print('...')
        print('fitfuncN [paramguessN]')

#------------------------------------------------------------#
################### compute mean&std error ###################
#------------------------------------------------------------#

elif sys.argv[1] =='stderr':
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        for index in range(0,len(commands)):
                line = commands[index].split()
                print('\n',index+1,'in',len(commands),'commands:',line,'\n')
                if line[1] == 'True':
                        judge = True
                else:
                        judge =False
                #stderr(line[0],judge)

elif sys.argv[1] == 'fold':
        datafile = sys.argv[2]
        dataset = csvtolist(datafile)
        dataset = folddat(dataset)
        newfile = datafile + '_fold.txt'
        header = ['tau, conf1, conf2, ...']
        with open(newfile, "w", newline='') as outfile:
                writer = csv.writer(outfile)
                writer.writerows([header])
                for raw in dataset:
                        #print(raw)       
                        writer.writerows([raw])
                outfile.close()


#------------------------------------------------------------#
####################### fit procedure ########################
#------------------------------------------------------------#

elif sys.argv[1] == 'fit':
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        for index in range(0,len(commands)):
                line = commands[index].split()
                print('\n',index+1,'in',len(commands),'commands:',line,'\n')
                fitfunc = getattr(modelfuncs,line[0])
                line = line[1:]
                chisqfit(fitfunc, line, resultfolder)

#------------------------------------------------------------#
###################### plot  procedure #######################
#------------------------------------------------------------#

elif sys.argv[1] =='plot':
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()

        line = commands[0].split()
        #print(line)
        datalabel = [line[0]]
        xylabel = [line[1].replace('!',' '), line[2].replace('!',' ')]
        yrange = [float(line[3]), float(line[4])]
        try:
                logscale = line[5]
                if logscale == 'log':
                        logscale = True
                else:
                        logscale = False
        except:
                logscale = False


        line = commands[1].split()
        savefolder = line[0]
        pltdata = []
        linefuncs = []
        fillfuncs = []
        funclabels = []
        text = []

        def make_f(x):
                def f(y):
                        return x
                return f
        factor_yscale = 1
        for index in range(2,len(commands)):

                line = commands[index].split()

                if 'yscale' in line[0]:
                        factor_yscale = float(line[1])
                elif  'line' in line[0]:
                        linevalue = float(line[1])
                        if len(line) > 3:
                                line_errn = float(line[-2])
                                line_errp = float(line[-1])
                                fillfuncs += [make_f(linevalue-line_errn),make_f(linevalue+line_errp)]
                                print(linevalue, line_errn, line_errp)
                                label_err = ''
                        elif len(line) == 3:
                                line_errn = float(line[-1])
                                line_errp = float(line[-1])
                                fillfuncs += [make_f(linevalue-line_errn),make_f(linevalue+line_errp)]
                                print(linevalue, line_errn, line_errp)
                                label_err = '(' + str(round(line_errn,4))[-2:] + ')'
                        else:
                                fillfuncs = None
                                label_err = ''
                        #linefunclabel = line[0].replace('line','') + str(round(linevalue,4)) + label_err
                        linefunclabel = line[0].replace('line','')
                        linefuncs += [make_f(linevalue)]
                        funclabels += [linefunclabel]
                elif 'text' == line[0]:
                        text_row = ''
                        for itext in range(3, len(line)):
                                text_row += line[itext]
                                text_row += ' '
                        text += [[text_row, float(line[1]), float(line[2])]]
                        #print(text)
                else:
                        # plot data
                        if line[0] == 'none':
                                line[0] = ''
                        tmp_label = (line[0].replace(',',', ')).replace('~',' ')
                        tmp_label = tmp_label.replace('=',' = ')
                        datalabel += [tmp_label]
                        #print(line[1])
                        dataset = np.loadtxt(line[1])
                        #print('???')
                        subpltdata = []
                        datasetT = np.transpose(dataset)
                        for i in range(2,len(line)):
                                #print(line)
                                subpltdata += [datasetT[int(line[i])]]
                        pltdata += [np.transpose(subpltdata)]
        #print(pltdata)

        if len(linefuncs) < 1:
                linefuncs = None
                fillfuncs = None
                funclabels = None
        #print(pltdata)
        #print(savefolder)    
        plot_err(pltdata, datalabel, xylabel, yrange, linefuncs, funclabels, fillfuncs ,folder = savefolder,logscale=logscale,text=text, factor_yscale=factor_yscale)
        inputfile.close()

#------------------------------------------------------------#
############ simple mean value weight procedure #############
#------------------------------------------------------------#

elif sys.argv[1] =='aiccave':
        inputfile = open(sys.argv[2],'r')
        readin = inputfile.readlines()
        dataset = []
        for line in readin:
                dataset += [list(map(float,line.split()))]
        #print(dataset[-1])
        position = []
        for i in range(3,len(sys.argv)):
                position += [int(sys.argv[i])]
        #print(position)
        weightdata = np.array(aicc_weight(dataset,position))
        np.savetxt(sys.argv[2]+'_aiccwt.txt',weightdata)
#------------------------------------------------------------#
############### AICc weighted En chisq fitting ###############
#------------------------------------------------------------#

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# this procedure built with bootstrap method
# the command file should be format as:
# output_file_name
# dataset.csv f/nf xminl xminr xmax npick
# fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
# fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
# ...
# fitdescribe(full/fixE0/fixSPE0E1) fitfuncN [paramguessN]
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
elif sys.argv[1] =='aicc':

        #print('\nFirst, read the random number from storage \n...\n... \n')
        #random_number = np.loadtxt(random_number_file)
        random_number=[None]

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]
        line = commands[1].split()
        dataset = csvtolist(line[0])

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples'
        mkdir(samplefolder)

        print('*\n*\n*\n*\n*\n*\n*\n*\nconfiguration number:', len(dataset[-1])-1)

        xminl = int(line[2])
        xminr = int(line[3])
        xmax = int(line[4])
        npick = int(line[5])
        print('xmin range:',xminl,xminr, '\tnpick:',npick)

        if line[1] == 'f':
                dataset = folddat(dataset)
                print('Data is folded!!!')
        elif line[1] == 'nf':
                print('Data is unfolded!!!')
        print('t number:',dataset[-1][0]+1)
        
        bootsparam = []
        for index in range(2,len(commands)):
                line = commands[index].split()
                bootsparam += [list(map(str,line))]
        #print(bootsparam)
      
        popt = []
        for xmin in range(xminl,xminr+1):
                #xmax = len(dataset) - xmin
                print(xmin, ' in ', xminl, '~', xminr)
                xmin_sample_name = samplefolder + '/' + describe + '_tmin' + str(xmin)

                subdataset = dataset[xmin:xmax+1]
                bootsresult = bootstrap(subdataset, npick, aiccfit, bootsparam, random_number, xmin_sample_name)
                subpopt = []
                aiccpopt = bootsresult[-1]
                aiccpoptchisq = []
                for r in range(0,len(bootsresult)-1):
                        modelpopt = bootsresult[r]
                        if 'prior' in bootsparam[0][0]:
                                imodel = r+1
                        else:
                                imodel = r
                        #print(raw[0:-1:3])
                        modelchisq = [chisq_value(dataset,getattr(modelfuncs,bootsparam[imodel][1]),modelpopt[0:(len(bootsparam[imodel])-2)*3:3],xmin,xmax)]
                        aiccpoptchisq += [chisq_value(dataset,getattr(modelfuncs,bootsparam[imodel][1]),aiccpopt[0:(len(bootsparam[imodel])-2)*3:3],xmin,xmax)]
                        #print(bootsparam[r][1],aiccpopt[0:(len(bootsparam[r])-2)*3:3],aiccpoptchisq)
                        subpopt += [[xmin, xmax] + modelpopt + modelchisq]
                        #r += 1
                subpopt += [[xmin, xmax] + aiccpopt + aiccpoptchisq]
                popt += [subpopt]                # popt[tmin][model][value]

        outfile = open(resultfolder + '/' + describe + '.txt','a')
        outfile.write(str(commands[1].split())+'\n')
        if 'prior' in bootsparam[0][0]:
                outfile.write(str(bootsparam[0])+'\n')
                bootsparam = bootsparam[1:]
        #print(popt)
        for i in range(0, len(popt[0])-1):
                subpopt = [popt[k][i][:] for k in range(0, len(popt))]
                outfile.write(str(bootsparam[i])+'\n')
                np.savetxt(outfile,subpopt,fmt='%1.5e')
        subpopt = [popt[k][-1][:] for k in range(0, len(popt))]
        outfile.write('aicc weighted results of above functions'+'\n')
        np.savetxt(outfile,subpopt,fmt='%1.5e')
        outfile.close()

        #save the data seperately
        #print(popt)
        for imodel in range(0,len(popt[0])):
                if imodel < len(popt[0])-1:
                        modelname = bootsparam[imodel][1]
                else:
                        modelname = 'aicc'
                print(modelname)
                outfilename = resultfolder + '/' + describe + '_' + modelname + '.txt'
                imodelpopt = [popt[k][imodel][:] for k in range(0, len(popt))]
                outfile = open(outfilename,'w')
                np.savetxt(outfile, imodelpopt, fmt='%.6e')
                outfile.close()

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This is a special case: put SP fitting results into fitting of SS data
# this procedure also built with bootstrap method
# the command file should be format as:
# output_file_name
# SPdataset.csv SSdataset.csv f/nf xminl xminr xmax npick
# SP fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
# SP fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
# ...
# SS fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
# SS fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
# ...
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
elif sys.argv[1] =='aiccSS':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        outfilename = (commands[0].split())[0]
        line = commands[1].split()
        SPdataset = csvtolist(line[0])
        SSdataset = csvtolist(line[1])
        xminl = int(line[3])
        xminr = int(line[4])
        xmax = int(line[5])
        npick = int(line[6])
        #print(xminl,xminr)

        if line[2] == 'f':
                SPdataset = folddat(SPdataset)
                SSdataset = folddat(SSdataset)
        
        SPparams = []
        SSparams = []
        for index in range(2,len(commands)):
                line = commands[index].split()
                if line[0] == 'SP':
                        SPparams += [list(map(str,line[1:]))]
                elif line[0] == 'SS':
                        SSparams += [list(map(str,line[1:]))]
        #print('SP',SPparams)
        #print('SS',SSparams)

        popt = []
        for xmin in range(xminl,xminr+1):
                print(xmin)
                subSPdataset = SPdataset[xmin:xmax+1]
                subSSdataset = SSdataset[xmin:xmax+1]
                bootsresult = SSbootstrap(subSPdataset,subSSdataset,npick,aiccfit,SPparams,SSparams)
                subpopt = []
                for raw in bootsresult:
                        subpopt += [[xmin, xmax] + raw]
                popt += [subpopt]

        
        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        mkdir(resultfolder)

        outfile = open(resultfolder + '/' + outfilename,'a')
        outfile.write('SP&SS'+str(commands[1].split())+'\n')

        #print(popt)
        params = SPparams + SSparams
        #print(params)
        for i in range(0, len(SPparams)):
                subpopt = [popt[k][i][:] for k in range(0, len(popt))]
                outfile.write(str(['SP']+params[i])+'\n')
                np.savetxt(outfile,subpopt,fmt='%1.5e')
        subpopt = [popt[k][len(SPparams)][:] for k in range(0, len(popt))]
        outfile.write('aicc weighted results of above functions'+'\n')
        np.savetxt(outfile,subpopt,fmt='%1.5e')
        for i in range(len(SPparams)+1, len(SPparams)+len(SSparams)+1):
                subpopt = [popt[k][i][:] for k in range(0, len(popt))]
                outfile.write(str(['SS']+params[i-1])+'\n')
                np.savetxt(outfile,subpopt,fmt='%1.5e')
        subpopt = [popt[k][-1][:] for k in range(0, len(popt))]
        outfile.write('aicc weighted results of above functions'+'\n')
        np.savetxt(outfile,subpopt,fmt='%1.5e')
        outfile.close()

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This is a linear fitting procedure for 3-point function
# this procedure is built with bootstrap method
# the command file should be format as:
# output_file_name p2 p3
# tslist
# z tmin(for c2pt fitting to get E0 and E1) tmax
# output folder
# c2pt.h5
# tsc3pt.h5 
# ...
# SPc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
# SPc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
# ...
# SSc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
# SSc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
# ...
# SSc3 fitdescribe(full/fixSSA0E0E1) fitfunc1 [paramguess1]
# ...
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def c3ptfitcommand(commandfile):

        #print('\nFirst, read the random number from storage \n...\n... \n')
        #random_number = np.loadtxt(random_number_file)
        random_number = []
        
        #Nt = 64
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()

        # read and create the output folder
        inputfolder = os.path.dirname(commandfile)
        outfolder = inputfolder + '/results'   # default, mkdir at the input file folder
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples'
        mkdir(samplefolder)
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        npick = int((commands[0].split())[1])
        taumin = int((commands[0].split())[2])
        describe = describe + str(taumin)
        print('\nnpick:', npick, ', taumin:', taumin, ', job describe:', describe)

        # read if the data need to be folded by z
        if 'imag' in describe:
                datatype = 'imag'
        else:
                datatype = 'real'
        foldz = False
        try:
                foldz = (commands[0].split())[3]
        except:
                pass
        
        # read time seperation for the following c3pt
        zmin = int((commands[1].split())[1])
        zmax = int((commands[1].split())[2])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nz list:', zlist)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[2].split())[1:]))
        print('\nts list:', tslist)
        
        # read c2pt fitting tmin and tmax
        tmin = int((commands[3].split())[1])
        tmax = int((commands[3].split())[2])
        print('c2pt fit tmin & tmax: ', tmin, tmax)

        # make the output file & sample file name
        outfilename = outfolder + '/' + describe + '_' + str(tmin) + 't' + str(tmax) + '.txt'
        samplename = samplefolder + '/' + describe + '_' + str(tmin) + 't' + str(tmax) + '_boots'

        # read and reformat the c2pt datas, c2data[t]: [t, conf...]
        c3filenames = []
        SSratioc2 = []
        SPc2data = []
        print('\nc3pt files:')
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'c3pt':
                        c3filename = line[1]
                        c3filenames += [c3filename]
                        print(c3filename)
                elif line[0] == 'SPc2pt':
                        SPc2file = line[1]
                        SPc2data = (csvtolist(SPc2file))[tmin:tmax+1]
                elif line[0] == 'SSc2pt':
                        SSc2file = line[1]
                        SSc2filedata = csvtolist(SSc2file)
                        SSc2data = SSc2filedata[tmin:tmax+1]
                        for ts in tslist:
                                SSratioc2 += [SSc2filedata[ts]]
        inputfile.close()

        # read and reformat the c3pt data, c3data[z][ts][tau]:  [tau, conf...]
        if foldz == 'fz':
                print('\n@@@@ Z data will be folded by positive and negetive direction.!')
                foldz = True
        c3data = []
        for z in zlist:
                zc3data = []
                for i in range(0,len(tslist)):
                        c3filename = c3filenames[i].replace('*', str(z))
                        i_zc3data_p = csvtolist(c3filename)
                        if foldz == True and z != 0:
                                c3filename = c3filenames[i].replace('*','-'+str(z))
                                i_zc3data_n = csvtolist(c3filename)
                                i_zc3data = folddat_z(i_zc3data_p, i_zc3data_n, datatype)
                                #print(z, 'folded')
                        else:
                                i_zc3data = i_zc3data_p
                        zc3data += [i_zc3data]
                c3data += [zc3data]
        #print(c3data[0][0][0])

        # read SP and SS c2pt fitting parameters
        c2ptfit = aiccfit
        SPc2ptparams = []
        SSc2ptparams = []
        SSc3ptparams = []
        for index in range(2,len(commands)):
                line = commands[index].split()
                if line[0] == 'SPc2':
                        SPc2ptparams += [list(map(str,line[1:]))]
                elif line[0] == 'SSc2':
                        SSc2ptparams += [list(map(str,line[1:]))]
                elif line[0] == 'SSc3':
                        SSc3ptparams += [list(map(str,line[1:]))]
        if len(SPc2ptparams) == 0:
                SPc2ptparams = np.array([None])
        if len(SSc2ptparams) == 0 or SSc2ptparams[0][0] == 'None':
                c2ptfit = None

        ratioerr = []
        ratiomean = []
        meantype = 'meanerr'
        jointc2 = [None]
        if 'joint' in SSc3ptparams[0][1]:
                print("joint fitting.")
                jointc2 = []
                tsmin = tslist[0]
                tsmax = tmax
                for ts in range(tsmin, tsmax+1):
                        jointc2 += [SSc2filedata[ts]]
        if 'ratio' in SSc3ptparams[0][1]:
                ratio = True
        else:
                ratio = False

        print('\nStart to collect standard error by jackknife method ...')
        for z in range(0,len(zlist)):
                ratioerr += [c3fitratio(tslist,c3data[z],SSratioc2,meanonly=meantype,jointc2=jointc2,ratio=ratio, taumin = taumin)]
                #cov = c3fitratio(tslist,c3data[z],SSratioc2,meanonly='covariance',jointc2=jointc2,ratio=ratio, taumin = taumin)
        print('Error collecting done! \n')

        for i in range(0, len(ratioerr[0])):
                print(tslist[i], "ts data shape:", np.shape(ratioerr[0][i]))
        print("\n")

        popt = c3ptfitbootstrap(c2ptfit,c3ptfit,SSc2data,SSc2ptparams,c3data,SSratioc2,ratioerr,SSc3ptparams,tslist,zlist,npick,random_number,jointc2=jointc2,ratio=ratio,taumin=taumin,sample_save=samplename)
        #print(popt)
        outfile = open(outfilename,'w')
        np.savetxt(outfile,popt,fmt='%.6e')
        outfile.close()

if sys.argv[1] == 'c3fit':
        c3ptfitcommand(sys.argv[2])

# c3pt fit and reduced ioffe time distribution
def c3ptfit_rif(commandfiles):
        
        Nt = 64
        pxlist = []
        SPc2data_allpx = []
        SPc2ptparams_allpx = []
        SSc2data_allpx = []
        SSc2ptparams_allpx = []
        c3data_allpx = []
        SSratioc2_allpx = []
        ratioerr_allpx = []
        SSc3ptparams_allpx = []
        commandfiles_file = open(commandfiles,'r')
        commandsfiles_allpx = commandfiles_file.readlines()

        # read and create the output folder
        inputfolder = os.path.dirname(commandfiles)
        outfolder = inputfolder + '/results'   # default, mkdir at the input file folder
        samplefolder = inputfolder + '/sample_results'
        mkdir(outfolder)
        mkdir(samplefolder)
        outfolder = outfolder
        samplesave = samplefolder + '/' + 'reducedioffetime' + '_Z'
        #samplesave = None

        for pxcommandfile in commandsfiles_allpx:
                pxlist += [float((pxcommandfile.split())[0])]
                pxcommandfile = (pxcommandfile.split())[1]
                inputfile = open(pxcommandfile,'r')
                commands = inputfile.readlines()
                
                # read describe of this command file
                describe = (commands[0].split())[0]
                
                # read time seperation for the following c3pt
                zmin = int((commands[1].split())[1])
                zmax = int((commands[1].split())[2])
                zlist = [z for z in range(zmin,zmax+1)]
                print('\nz list:', zlist)
                        
                # read time seperation for the following c3pt
                tslist = list(map(int,(commands[2].split())[1:]))
                print('\nts list:', tslist)
                
                # read c2pt fitting tmin and tmax
                tmin = int((commands[3].split())[1])
                tmax = int((commands[3].split())[2])
                npick = 1000
                print('c2pt fit tmin & tmax: ', tmin, tmax)
                
                # read and reformat the c2pt datas, c2data[t]: [t, conf...]
                SPc2file = (commands[4].split())[-1]
                SPc2data = (csvtolist(SPc2file))[tmin:tmax+1]
                SSc2file = (commands[5].split())[-1]
                SSc2data = (csvtolist(SSc2file))[tmin:tmax+1]
                SSratioc2 = []
                for ts in tslist:
                        SSratioc2 += [(csvtolist(SSc2file))[ts]]
                
                # read and reformat the c3pt datas, c3data[z][ts][tau]:  [tau, conf...]
                c3filenames = []
                print('\nc3pt files:')
                for index in range(0,len(commands)):
                        line = commands[index].split()
                        if line[0] == 'c3pt':
                                c3filename = line[1]
                                c3filenames += [c3filename]
                                print(c3filename)
                inputfile.close()
                
                c3data = []
                for z in zlist:
                        zc3data = []
                        for i in range(0,len(tslist)):
                                c3filename = c3filenames[i].replace('*', str(z))
                                zc3data += [csvtolist(c3filename)]
                        c3data += [zc3data]
                
                # caculate the c3c2 ratio error to do the linear fit
                ratioerr = []
                for z in range(0,len(zlist)):
                        ratioerr += [c3fitratio(tslist,c3data[z],SSratioc2,meanonly='meanerr')]
                        
                # read SP and SS c2pt fitting parameters
                SPc2ptparams = []
                SSc2ptparams = []
                SSc3ptparams = []
                for index in range(2,len(commands)):
                        line = commands[index].split()
                        if line[0] == 'SPc2':
                                SPc2ptparams += [list(map(str,line[1:]))]
                        elif line[0] == 'SSc2':
                                SSc2ptparams += [list(map(str,line[1:]))]
                        elif line[0] == 'SSc3':
                                SSc3ptparams += [list(map(str,line[1:]))]
                if len(SPc2ptparams) == 0:
                        SPc2ptparams = np.array([None])

                SPc2data_allpx += [SPc2data]
                SPc2ptparams_allpx += [SPc2ptparams]
                SSc2data_allpx += [SSc2data]
                SSc2ptparams_allpx += [SSc2ptparams]
                c3data_allpx += [c3data]
                SSratioc2_allpx += [SSratioc2]
                ratioerr_allpx += [ratioerr]
                SSc3ptparams_allpx += [SSc3ptparams]
        print('pxlist:',pxlist)

        Ns = 64
        popt = c3ptfitbootstrap_rioffe(Ns,aiccfit,c3ptfit,SPc2data_allpx,SPc2ptparams_allpx,SSc2data_allpx,SSc2ptparams_allpx,c3data_allpx,SSratioc2_allpx,ratioerr_allpx,SSc3ptparams_allpx,pxlist,tslist,zlist,npick,samplesave)
        for z in range(0, len(zlist)):
                outfilename = outfolder + '/' + 'reducedioffetime' + '_Z' + str(zlist[z]) + '.txt'
                outfile = open(outfilename,'w')
                np.savetxt(outfile,popt[z],fmt='%.6e')
                outfile.close()
if sys.argv[1] == 'c3fitrif':
        c3ptfit_rif(sys.argv[2])
        
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# sample file fitting 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if sys.argv[1] == 'spfit':
        fit_samplefile(simple_fit, sys.argv[2])
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This is summation fitting function, the c2pt all has been  modified by
# subtract the wrap part. And this subtraction is built in the bootstap
# procedure, so the errors of A0 and E0 are propagated.
# The command file should be formatted as:
# the command file should be format as:
# output_file_name
# zmin zmax
# tslist
# tmin tmax(for c2pt fitting to get E0 and E1)
# output folder
# c2pt fileposition
# c3pt filepositions 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def summationfit(commandfile):
        
        Nt = 64
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        npick = int((commands[0].split())[1])
        taumin = int((commands[0].split())[2])
        describe = describe + str(taumin)
        print('\nnpick:', npick, 'taumin:', taumin, 'job describe:', describe)
        
        # read time seperation for the following c3pt
        zmin = int((commands[1].split())[0])
        zmax = int((commands[1].split())[1])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nzmin zmax:', zmin, zmax)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[2].split())))
        print('\nts list:', tslist)
                
        # read time seperation for the c2pt fitting
        c2fitparams = list(commands[3].split())
        print('\nc2fitparam:',c2fitparams)
                
        # read the output folder
        inputfolder = os.path.dirname(commandfile)
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        
        # read and reformat the c2pt datas
        c2file = (commands[4].split())[-1]
        c2data = csvtolist(c2file)
        print("configuration number: ", len(c2data[0])-1)
        #c2data = folddat(c2data)
        
        # read and reformat the c3pt datas
        c3filenames = []
        print('\nc3pt files:')
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'c3pt':
                        c3filename = line[1]
                        c3filenames += [c3filename]
                        print(c3filename)
        inputfile.close()
        
        c3data = []
        for z in zlist:
                zc3data = []
                for i in range(0,len(tslist)):
                        c3filename = c3filenames[i].replace('*', str(z))
                        zc3data += [csvtolist(c3filename)]
                c3data += [zc3data]

        print('\nc3data shape:',np.shape(c3data),'c3data[z][ts][tau][conf]', '\n')
        print('job describe:', describe,'\n')
        c3data = c3dataslice_sumfit(c3data, tslist, taumin = taumin)
        
        for i in range(0, len(c3data[0])):
                print(tslist[i], "ts data shape:", np.shape(c3data[0][i]))
        print("\n")

        popt = sumfit(Nt,c2data,c3data,zlist,tslist,c2fitparams,npick)

        savepath = outfolder + describe + '.txt'
        savefile = open(savepath,'w')
        np.savetxt(savefile, popt)
        savefile.close()

if sys.argv[1] == 'sumfit':
        summationfit(sys.argv[2])

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This is summation fitting function, the c2pt all has been  modified by
# subtract the wrap part. And this subtraction is built in the bootstap
# procedure, so the errors of A0 and E0 are propagated.
# The command file should be formatted as:
# the command file should be format as:
# output_file_name
# zmin zmax
# tslist
# tmin tmax(for c2pt fitting to get E0 and E1)
# output folder
# c2pt fileposition
# c3pt filepositions 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def jacksumfit(commandfile):
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        Nt = int((commands[0].split())[1])
        taumin = int((commands[0].split())[2])
        blocksize = int((commands[0].split())[4])
        describe = describe + str(taumin)
        foldz = False
        try:
                foldz = (commands[0].split())[3]
        except:
                pass
        if 'imag' in describe:
                datatype = 'imag'
        else:
                datatype = 'real'
        

        if 'reduced' in describe:
                print('\n@@@@@Summation fit result will be reduced by Z=0')
                normalization = True
        else:
                normalization = False
        
        # read time seperation for the following c3pt
        zmin = int((commands[1].split())[0])
        zmax = int((commands[1].split())[1])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nzmin zmax:', zmin, zmax)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[2].split())))
        print('\nts list:', tslist)
                
        # read time seperation for the c2pt fitting
        c2fitparams = list(commands[3].split())
        print('\nc2fitparam:',c2fitparams)
                
        # read the output folder
        inputfolder = os.path.dirname(commandfile)
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)

        jackfolder = outfolder + 'samples/'
        mkdir(jackfolder)
        jackname = jackfolder + describe + '_jackk'
        print('\nNt:', Nt, 'taumin:', taumin, 'job describe:', describe, 'blocksize:', blocksize)
        
        # read and reformat the c2pt datas
        c2file = (commands[4].split())[-1]
        c2data = csvtolist(c2file)
        print("configuration number: ", len(c2data[0])-1)
        #c2data = folddat(c2data)
        #print('c2pt data is folded!')
        
        # read and reformat the c3pt datas
        c3filenames = []
        print('\nc3pt files:')
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'c3pt':
                        c3filename = line[1]
                        c3filenames += [c3filename]
                        print(c3filename)
        inputfile.close()

        # read the c3pt data
        if foldz == 'fz':
                print('\n@@@@ Z data will be folded by positive and negetive direction.!')
                foldz = True
        c3data = []
        for z in zlist:
                zc3data = []
                for i in range(0,len(tslist)):
                        c3filename = c3filenames[i].replace('*', str(z))
                        i_zc3data_p = csvtolist(c3filename)
                        if foldz == True and z != 0:
                                c3filename = c3filenames[i].replace('*','-'+str(z))
                                i_zc3data_n = csvtolist(c3filename)
                                i_zc3data = folddat_z(i_zc3data_p, i_zc3data_n, datatype)
                                #print(z, 'folded')
                        else:
                                i_zc3data = i_zc3data_p
                        zc3data += [i_zc3data]
                c3data += [zc3data]

        print('\nc3data shape:',np.shape(c3data),'c3data[z][ts][tau][conf]', '\n')
        print('job describe:', describe,'\n')
        c3data = c3dataslice_sumfit(c3data, tslist, taumin = taumin)
        '''
        if 'reduced' in describe:
                c3data = c3data_norm(c3data, zlist)
        '''
        
        for i in range(0, len(c3data[0])):
                print(tslist[i], "ts data shape:", np.shape(c3data[0][i]))
        print("\n")

        popt = jackk_sumfit(Nt,c2data,c3data,zlist,tslist,c2fitparams,blocksize=blocksize,jackfilename=jackname,normalization=normalization)

        savepath = outfolder + describe + '.txt'
        savefile = open(savepath,'w')
        np.savetxt(savefile, popt)
        savefile.close()

if sys.argv[1] == 'jacksumfit':
        jacksumfit(sys.argv[2])

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This is summation fitting function, the c2pt all has been  modified by
# subtract the wrap part. And this subtraction is built in the bootstap
# procedure, so the errors of A0 and E0 are propagated.
# The command file should be formatted as:
# the command file should be format as:
# output_file_name
# zmin zmax
# tslist
# tmin tmax(for c2pt fitting to get E0 and E1)
# output folder
# c2pt fileposition
# c3pt filepositions 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def c3c2ratio(commandfile):
        
        Nt = 64
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        foldz = (commands[0].split())[1]
        if 'imag' in describe:
                datatype = 'imag'
        else:
                datatype = 'real'
        
        # read time seperation for the following c3pt
        zmin = int((commands[1].split())[0])
        zmax = int((commands[1].split())[1])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nzmin zmax:', zmin, zmax)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[2].split())))
        print('\nts list:', tslist)
                
        # read time seperation for the c2pt fitting
        c2fitparams = list(commands[3].split())
        print('\nc2fitparam:',c2fitparams)
        A0 = float(c2fitparams[2])
        E0 = float(c2fitparams[3])
        A1 = float(c2fitparams[4])
        E1 = float(c2fitparams[5])
        def c2mdf(t):
                return A0*np.exp(-E0*(Nt-t))+A1*np.exp(-E1*(Nt-t))
        #def c2mdf(t):
        #        return A0*np.exp(-E0*(t))+A1*np.exp(-E1*(t))
                
        # read the output folder
        inputfolder = os.path.dirname(commandfile)
        outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
        mkdir(outfolder)

        # read and reformat the c2pt datas
        c2file = (commands[4].split())[-1]
        c2data = csvtolist(c2file)
        '''
        try:
                #print(c2data[1][0:10])
                c2file_img = c2file.replace('real','img')
                c2data_img = csvtolist(c2file_img)
                c2data = comb_real_img_2d(c2data, c2data_img)
                #print(c2data[1][0:10])
        except:
                #traceback.print_exc()
                pass
        '''

        #c2data = folddat(c2data)
        #print('c2pt data is folded!')

        tsc2data = []
        for ts in tslist:
                tsc2data += [c2data[ts]]
        
        # read and reformat the c3pt datas
        c3filenames = []
        print('\nc3pt files:')
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'c3pt':
                        c3filename = line[1]
                        c3filenames += [c3filename]
                        print(c3filename)
        inputfile.close()

        # read the c3pt data
        if foldz == 'fz':
                print('\n@@@@ Z data will be folded by positive and negetive direction.!')
                foldz = True
        ratio = []
        for z in zlist:
                print('z:',z)
                zc3data = []
                for i in range(0,len(tslist)):
                        c3filename = c3filenames[i].replace('*', str(z))
                        i_zc3data_p = csvtolist(c3filename)
                        if foldz == True and z != 0:
                                c3filename = c3filenames[i].replace('*','-'+str(z))
                                i_zc3data_n = csvtolist(c3filename)
                                i_zc3data = folddat_z(i_zc3data_p, i_zc3data_n, datatype)
                                #print(z, 'folded')
                        else:
                                i_zc3data = i_zc3data_p
                        zc3data += [i_zc3data]
                                
                ratio = c3c2radiot(tslist,tsc2data,zc3data,c2mdf)
                for i in range(0, len(tslist)):
                        savepath = outfolder + describe + '_dt' + str(tslist[i]) + '_Z' + str(z) + '.txt'
                        np.savetxt(savepath,ratio[i])


if sys.argv[1] == 'c3c2ratio':
        c3c2ratio(sys.argv[2])

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This function is used to transform the z distribution
# to Ioffe time distribution of bare matrix element.
# The dataset is assumed to be 3D, which should be 
# arraged as dataset[p][z][z,matrix_elemnt,err,...]
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def qusitopseudo(commandfile):
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file and Lx
        describe = (commands[0].split())[0]
        Lx = int((commands[0].split())[1])
        dp = 2 * 3.14159265 / Lx

        # read the output folder
        folder = (commands[1].split())[0]
        
        # read time seperation for the following c3pt
        zmin = int((commands[2].split())[0])
        zmax = int((commands[2].split())[1])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nzmin zmax:', zmin, zmax)

        # read and reformat the z_distribution qusi-pdf datas
        plist = []
        zdataset = []
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'px':
                        plist += [float(line[1])*dp]
                        pxzdata = np.loadtxt(line[2])
                        zdataset += [pxzdata]
        inputfile.close()
                
        qdataset = ex12_3d(zdataset)  # dataset[z][p][z,matrix_elemnt,err,...]
        
        for i in range(0, len(qdataset)):
                for j in range(0, len(plist)):
                        qdataset[i][j][0] = qdataset[i][j][0]*plist[j]

        for i in range(0, len(zlist)):
                filename = folder + describe + '_z' + str(zlist[i]) + '.txt'
                np.savetxt(filename,qdataset[i])
                
        #return zlist,qdataset


if sys.argv[1] == 'qtop':
        qusitopseudo(sys.argv[2])
        
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# This is summation fitting function, the c2pt all has been  modified by
# subtract the wrap part. And this subtraction is built in the bootstap
# procedure, so the errors of A0 and E0 are propagated.
# The command file should be formatted as:
# the command file should be format as:
# output_file_name
# zmin zmax
# tslist
# tmin tmax(for c2pt fitting to get E0 and E1)
# output folder
# c2pt fileposition
# c3pt filepositions 
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def reducedIoffe_summationfit(commandfile):
        
        Nt = 64
        Ns = 64
        #taulen = 16
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        taumin = int((commands[0].split())[1])
        describe = describe + str(taumin)
        jackfolder = '/rif_taumin' + str(taumin) + '_jackk/'
        print('\ntaumin:', taumin, 'job describe:', describe)

        # read time momentum for the following c2pt and c3pt
        pxmin = int((commands[1].split())[1])
        pxmax = int((commands[1].split())[2])
        pxlist = [px for px in range(pxmin,pxmax+1)]
        print('\npx list:', pxlist)
        
        # read time seperation for the following c3pt
        zmin = int((commands[2].split())[1])
        zmax = int((commands[2].split())[2])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nz list:', zlist)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[3].split())[1:]))
        print('\nts list:', tslist)
                
        # read the output folder
        inputfolder = os.path.dirname(commandfile)
        outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
        jackfolder = inputfolder + jackfolder
        mkdir(outfolder)
        mkdir(jackfolder)
        
        # read and reformat the c2pt datas
        c2dataset = []      # c2dataset gonna to be c2dataset[px][t] = [t, conf...]
        c2fitparams = []
        for ip in range(0, len(pxlist)):
                for index in range(0,len(commands)):
                        line = commands[index].split()
                        if line[0] == 'c2pt' and int(line[1]) == pxlist[ip]:
                                c2filename = line[2]
                                c2dataset += [csvtolist(c2filename)]
                                c2fitparams += [line[3:]]
        print('\nc2ptdata:',np.shape(c2dataset))
        print('c2fit parameters:',np.shape(c2fitparams))
        #c2data = folddat(c2data)

        # read and reformat the c3pt datas
        c3dataset = []      # c3dataset gonna to be c3dataset[px][z][ts][tau] = [tau, conf...]
        for px in range(0, len(pxlist)):   # px
                pxzc3dataset = []
                for z in zlist:            # z
                        pxztsc3dataset = []
                        for index in range(0,len(commands)):       # ts
                                line = commands[index].split()
                                if line[0] == 'c3pt' and int(line[1]) == pxlist[px]:
                                        #print(line)
                                        c3filename = line[2].replace('*', str(z))
                                        pxztsc3dataset += [csvtolist(c3filename)]
                        pxzc3dataset += [pxztsc3dataset]
                pxzc3dataset = c3dataslice_sumfit(pxzc3dataset,tslist, taumin=taumin)
                c3dataset += [pxzc3dataset]
        for i in range(0, len(c3dataset[0][0])):
                print(tslist[i], "ts data shape:", np.shape(c3dataset[0][0][i]))
        print("\n")    
        inputfile.close()
        print('\n@@@@@@@@@-- job name:',describe)

        reduced_ioffe = reducedIoffe_sumfit(Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, 1, jackfilename=jackfolder)
        #print('reduced_ioffe shape:',reduced_ioffe)

        # save the reduced ioffe time distribution
        for z in range(0, len(zlist)):
                filename = outfolder + describe + '_Z' + str(zlist[z]) + '.txt'
                savefile = open(filename,'w')
                np.savetxt(savefile,reduced_ioffe[z],fmt='%.6e')
                savefile.close()

if sys.argv[1] == 'rifsum':
        reducedIoffe_summationfit(sys.argv[2])

def reducedIoffe_zcurvefit(commandfile):
        
        Nt = 64
        Ns = 64
        #taulen = 16
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        fitfuncname = (commands[0].split())[1]
        fitfunc = getattr(modelfuncs,fitfuncname)
        jackkblocksize = int((commands[0].split())[2])

        # read time momentum for the following c2pt and c3pt
        pxmin = int((commands[1].split())[1])
        pxmax = int((commands[1].split())[2])
        pxlist = [px for px in range(pxmin,pxmax+1)]
        print('\npx list:', pxlist)
        
        # read time seperation for the following c3pt
        zmin = int((commands[2].split())[1])
        zmax = int((commands[2].split())[2])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nz list:', zlist)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[3].split())[1:]))
        print('\nts list:', tslist)
                
        # read the output folder
        outfolder = (commands[4].split())[0]
        
        # read and reformat the c2pt datas
        c2dataset = []      # c2dataset gonna to be c2dataset[px][t] = [t, conf...]
        c2fitparams = []
        for ip in range(0, len(pxlist)):
                for index in range(0,len(commands)):
                        line = commands[index].split()
                        if line[0] == 'c2pt' and int(line[1]) == pxlist[ip]:
                                c2filename = line[2]
                                c2dataset += [csvtolist(c2filename)]
                                c2fitparams += [line[3:]]
        print('\nc2ptdata:',np.shape(c2dataset))
        print('c2fit parameters:',np.shape(c2fitparams))
        #c2data = folddat(c2data)

        # read and reformat the c3pt datas
        c3dataset = []      # c3dataset gonna to be c3dataset[px][z][ts][tau] = [tau, conf...]
        for px in range(0, len(pxlist)):   # px
                pxzc3dataset = []
                for z in zlist:            # z
                        pxztsc3dataset = []
                        for index in range(0,len(commands)):       # ts
                                line = commands[index].split()
                                if line[0] == 'c3pt' and int(line[1]) == pxlist[px]:
                                        #print(line)
                                        c3filename = line[2] + str(z) + '.txt'
                                        pxztsc3dataset += [csvtolist(c3filename)]
                        pxzc3dataset += [pxztsc3dataset]
                pxzc3dataset = c3dataslice_sumfit(pxzc3dataset,tslist)
                c3dataset += [pxzc3dataset]
        print('c3ptadata:',np.shape(c3dataset),',c3ptadata[0][0][0]:',np.shape(c3dataset[0][0][0]),',c3ptadata[0][0][-1]:',np.shape(c3dataset[0][0][-1]))             
        inputfile.close()
        print('\n@@@@@@@@@-- job name:',describe)

        popt = rioffe_zcurvefit(fitfunc, Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, jackkblocksize)
        #print('reduced_ioffe shape:',reduced_ioffe)

        # save the reduced ioffe time distribution
        filename = outfolder + describe+ '_px' + str(pxmin) + '-' + str(pxmax) + '_Z' + str(zmin) + '-' + str(zmax) + '_' + fitfuncname + '_popt.txt'
        savefile = open(filename,'w')
        np.savetxt(savefile,popt,fmt='%.6e')
        savefile.close()

if sys.argv[1] == 'ricfsum':
        reducedIoffe_zcurvefit(sys.argv[2])

def rIoffe_plyfit_solveab(commandfile):
        
        Nt = 64
        Ns = 64
        #taulen = 16
        
        inputfile = open(commandfile,'r')
        commands = inputfile.readlines()
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        fitfuncname = (commands[0].split())[1]
        fitfunc = getattr(modelfuncs,fitfuncname)
        jackkblocksize = int((commands[0].split())[2])

        # read time momentum for the following c2pt and c3pt
        pxmin = int((commands[1].split())[1])
        pxmax = int((commands[1].split())[2])
        pxlist = [px for px in range(pxmin,pxmax+1)]
        print('\npx list:', pxlist)
        
        # read time seperation for the following c3pt
        zmin = int((commands[2].split())[1])
        zmax = int((commands[2].split())[2])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nz list:', zlist)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[3].split())[1:]))
        print('\nts list:', tslist)
                
        # read the output folder
        outfolder = (commands[4].split())[0]
        
        # read and reformat the c2pt datas
        c2dataset = []      # c2dataset gonna to be c2dataset[px][t] = [t, conf...]
        c2fitparams = []
        for ip in range(0, len(pxlist)):
                for index in range(0,len(commands)):
                        line = commands[index].split()
                        if line[0] == 'c2pt' and int(line[1]) == pxlist[ip]:
                                c2filename = line[2]
                                c2dataset += [csvtolist(c2filename)]
                                c2fitparams += [line[3:]]
        print('\nc2ptdata:',np.shape(c2dataset))
        print('c2fit parameters:',np.shape(c2fitparams))
        #c2data = folddat(c2data)

        # read and reformat the c3pt datas
        c3dataset = []      # c3dataset gonna to be c3dataset[px][z][ts][tau] = [tau, conf...]
        for px in range(0, len(pxlist)):   # px
                pxzc3dataset = []
                for z in zlist:            # z
                        pxztsc3dataset = []
                        for index in range(0,len(commands)):       # ts
                                line = commands[index].split()
                                if line[0] == 'c3pt' and int(line[1]) == pxlist[px]:
                                        #print(line)
                                        c3filename = line[2] + str(z) + '.txt'
                                        pxztsc3dataset += [csvtolist(c3filename)]
                        pxzc3dataset += [pxztsc3dataset]
                pxzc3dataset = c3dataslice_sumfit(pxzc3dataset,tslist)
                c3dataset += [pxzc3dataset]
        print('c3ptadata:',np.shape(c3dataset),',c3ptadata[0][0][0]:',np.shape(c3dataset[0][0][0]),',c3ptadata[0][0][-1]:',np.shape(c3dataset[0][0][-1]))             
        inputfile.close()
        print('\n@@@@@@@@@-- job name:',describe)

        #popt = rioffe_plyfit_solveab(fitfunc, Ns, Nt, c2dataset, c3dataset, pxlist, zlist, tslist, c2fitparams, jackkblocksize)
        #print('reduced_ioffe shape:',reduced_ioffe)

        # save the reduced ioffe time distribution
        filename = outfolder + describe+ '_px' + str(pxmin) + '-' + str(pxmax) + '_Z' + str(zmin) + '-' + str(zmax) + '_' + fitfuncname + '_popt.txt'
        savefile = open(filename,'a')
        np.savetxt(savefile,popt,fmt='%.6e')
        savefile.close()

if sys.argv[1] == 'solveab':
        rIoffe_plyfit_solveab(sys.argv[2])