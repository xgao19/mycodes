#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
import inspect
from scipy.optimize import curve_fit

from tools import *
import modelfuncs
from modelfuncs import *
from chisqfit_cov_jb import *


def jackk_c2ptfit(c2ptdata, fitparams, samplename = None, blocksize=1, c2pt_t0=[None]):

    ncfgs = len(c2ptdata[0]) - 1
    nblock = int(ncfgs/blocksize)
    sample_fitparams=[]
    for i in range(0, len(fitparams)):
            sample_fitparams += [fitparams[i]]

    if 'sample' in fitparams[0][0]:
        sample_fitparams[0] = sample_fitparams[0][:3]
        #print(fitparams[0])
        sample_prior_data = np.loadtxt(fitparams[0][3])
        sample_prior_data = [sample_prior_data[i][int(fitparams[0][4])] for i in range(0, len(sample_prior_data))]

    # mean value
    #print(np.shape(c2ptdata))
    poptlist, poptfail = aiccfit(c2ptdata, sample_fitparams)
    #print(sample_fitparams,poptlist)
    poptlist = poptlist[:-1]

    if len(c2pt_t0) > 1:
        c2pt_t0_mean = np.mean(c2pt_t0[1:])
        #print(c2pt_t0_mean)
        for i in range(0, len(poptlist)):
            poptlist[i][4] = poptlist[i][4]/c2pt_t0_mean
            poptlist[i][2] = poptlist[i][2]/c2pt_t0_mean
            poptlist[i][0] = poptlist[i][0]/c2pt_t0_mean
            poptlist[i][-2] = poptlist[i][4]+poptlist[i][2]+poptlist[i][0]
            #print(poptlist[i][4],poptlist[i][2],poptlist[i][0],poptlist[i][-2])
    #print('2pt fit mean value:',poptlist)
    
    # jackknife loop
    jackk_err = []
    jackk_samples = []
    #sample_E1 = float(sample_fitparams[0][1])

    for line in fitparams:
        if 'prior' not in line[0]:
            #print(fitparams)
            poptguess = list(map(float,line[2:]))
            jackk_err += [list(np.zeros(len(poptguess)+2))]
            jackk_samples += [[]]

    #print(len(sample_prior_data))
    for iblk in range(0, nblock):
        #print('\n jackknife block:',iblk+1,'/',nblock)
        if 'sample' in fitparams[0][0]:
            sample_fitparams[0][1] = str(sample_prior_data[iblk])
            #sample_fitparams[0][1] = str(sample_prior_data[iblk]-3.727078e-01+sample_E1)
        #print(sample_fitparams[0])

        iblk_c2ptdata = d5slicedelete(c2ptdata,[iblk*blocksize+1,(iblk+1)*blocksize+1],1)
        iblk_popt, iblk_fail = aiccfit(iblk_c2ptdata, sample_fitparams, failpercent=poptfail)
        iblk_popt = iblk_popt[:-1]
        #print(iblk_popt)

        if len(c2pt_t0) > 1:
            c2pt_t0_mean = np.mean(np.delete(c2pt_t0,[0, iblk+1]))
            #print(c2pt_t0_mean)
            for i in range(0, len(iblk_popt)):
                iblk_popt[i][4] = iblk_popt[i][4]/c2pt_t0_mean
                iblk_popt[i][2] = iblk_popt[i][2]/c2pt_t0_mean
                iblk_popt[i][0] = iblk_popt[i][0]/c2pt_t0_mean
                iblk_popt[i][-2] = iblk_popt[i][4]+iblk_popt[i][2]+iblk_popt[i][0]

        for ierr in range(0, len(iblk_popt)):

            jackk_samples[ierr] += [iblk_popt[ierr]]
            for jerr in range(0, len(iblk_popt[ierr])):
                #print(np.shape(jackk_err),np.shape(iblk_popt))
                jackk_err[ierr][jerr] += (iblk_popt[ierr][jerr] - poptlist[ierr][jerr])*(iblk_popt[ierr][jerr] - poptlist[ierr][jerr])

    # save the jackknife samples
    if samplename != None:
        if 'prior' in fitparams[0][0]:
            fitparams = fitparams[1:]
        for imodel in range(0, len(fitparams)):
            imodel_samplename = samplename + '_' + fitparams[imodel][1]
            #print(np.shape(jackk_samples[imodel]))
            np.savetxt(imodel_samplename, jackk_samples[imodel])

    # jackknife error
    for ierr in range(0 ,len(jackk_err)):
        for jerr in range(0, len(jackk_err[ierr])):
            jackk_err[ierr][jerr] = np.sqrt(jackk_err[ierr][jerr]*(nblock-1)/nblock)

    mean_err = []
    for i in range(0, len(poptlist)):
        i_mean_err = []
        for j in range(0, len(poptlist[i])):
            #print(j, int(j/2)-j/2,poptlist[i])
            if poptlist[i][j]-jackk_err[i][j]>0:
                i_mean_err += [poptlist[i][j], jackk_err[i][j]]
                #elif int(j/2)-j/2 != 0:
                #    i_mean_err += [1000,0]
            else:
                i_mean_err += [poptlist[i][j], jackk_err[i][j]]
        mean_err += [i_mean_err]
        #print(i_mean_err)
    
    return mean_err


def jackk2pt_amplitudes_norm(c2pt_file,mean_file,col_mean,sample_format,col_sample):

    mean_data = np.loadtxt(mean_file)
    tslist = [int(mean_data[i][0]) for i in range(0, len(mean_data))]
    c2pt_t0 = csvtolist(c2pt_file)[0]
    c2pt_t0 = c2pt_t0[1:]
    ncfg = len(c2pt_t0)
    it_sampledata = (np.loadtxt((sample_format.replace('#',str(tslist[0])))))
    nblock = len(it_sampledata)
    blocksize = int(ncfg/nblock)
    print(tslist)

    # data collection
    norm_amp = []
    for it in range(0,len(tslist)):

        mean_c2pt_t0 = np.mean(c2pt_t0)
        it_norm_amp = [tslist[it]]
        sample_data = (np.loadtxt((sample_format.replace('#',str(tslist[it])))))

        for icol in range(0 ,len(col_sample)):
            icol_amp = mean_data[it][col_mean[icol]]/mean_c2pt_t0
            print(it,col_mean[icol],icol_amp)
            #icol_amp = mean_data[it][col_mean[1]]/mean_data[it][col_mean[0]]
            icol_err = 0
            for iblk in range(0, nblock):
                iblk_c2pt_t0 = d5slicedelete(c2pt_t0,[iblk*blocksize,(iblk+1)*blocksize],0)
                iblk_c2pt_t0 = np.mean(iblk_c2pt_t0)
                icol_err += (sample_data[iblk][col_sample[icol]]/iblk_c2pt_t0-icol_amp)**2
                #icol_err += (sample_data[iblk][col_sample[1]]/sample_data[iblk][col_sample[0]]-icol_amp)**2
            icol_err = np.sqrt(icol_err*(nblock-1)/nblock)
            it_norm_amp += [icol_amp,icol_err]
        print(it_norm_amp)

        norm_amp += [it_norm_amp]
    return norm_amp

if __name__ == "__main__":
    
    if sys.argv[1] == 'Aratio':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()

        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results'   # default, mkdir at the input file folder
        mkdir(outfolder)

        # read describe of this command file
        describe = (commands[0].split())[0]

        for index in range(0,len(commands)):
            line = commands[index].split()
            if line[0] == 'pxrange':
                pxlist = [ipx for ipx in range(int(line[1]),int(line[2])+1)]
            elif line[0] == 'c2pt':
                c2pt_format = line[1]
            elif line[0] == 'mean':
                mean_file = line[1]
                mean_col = [int(line[i]) for i in range(2, len(line))]
            elif line[0] == 'sample':
                sample_format = line[1]
                sample_col = [int(line[i]) for i in range(2, len(line))]

        for ipx in pxlist:

            c2pt_file = c2pt_format.replace('*', str(ipx))
            ipx_mean_file = mean_file.replace('*', str(ipx))
            ipx_sample_format = sample_format.replace('*', str(ipx))

            ipx_data = jackk2pt_amplitudes_norm(c2pt_file,ipx_mean_file,mean_col,ipx_sample_format,sample_col)

            outfilename = outfolder + '/' + describe.replace('*', str(ipx)) + '.txt'

            outfile = open(outfilename,'w')
            np.savetxt(outfile,ipx_data,fmt='%.6e')
            outfile.close()







