#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
from scipy.interpolate import interp1d

grid_nmax = 20
mu0 = 3.2 #GeV
KernelKlist = [i for i in range(0, 15)]


#####################################################
################    NNLO kernel    ##################
#####################################################

Kernel_NNLO_K4_mu3p2GeV_grid_file0 = "/Users/Xiang/Desktop/0study/research/lattice/data/rITD/kernel_ratio_file/ListKernelNNLO_K4_mu3p2GeV_down_n#.txt"
Kernel_NNLO_K4_mu3p2GeV_grid_file1 = "/Users/Xiang/Desktop/0study/research/lattice/data/rITD/kernel_NNLO_file/ListRatioKernelNNLO_mu3p2GeV_n#.txt"
Kernel_NNLO_K4_mu3p2GeV_grid_file2 = "/Users/Xiang/Desktop/0study/research/lattice/data/rITD/kernel_ratio_file/ListKernelNNLO_K4_mu3p2GeV_up_n#.txt"

Kernel_NNLO_K4_mu3p2GeV_interp_list0 = []
Kernel_NNLO_K4_mu3p2GeV_interp_list1 = []
Kernel_NNLO_K4_mu3p2GeV_interp_list2 = []

for i in range(0, grid_nmax+1):
    try:
        input_name = Kernel_NNLO_K4_mu3p2GeV_grid_file0.replace('#', str(i))
        kernel_data = np.loadtxt(input_name)
        z_fm = [kernel_data[i][3] for i in range(0, len(kernel_data))]
        kernel = [kernel_data[i][4] for i in range(0, len(kernel_data))]
        Kernel_NNLO_K4_mu3p2GeV_interp_list0 += [interp1d(z_fm, kernel, kind='cubic')]
    except:
        Kernel_NNLO_K4_mu3p2GeV_interp_list0 += [None]

    try:
        input_name = Kernel_NNLO_K4_mu3p2GeV_grid_file1.replace('#', str(i))
        kernel_data = np.loadtxt(input_name)
        z_fm = [kernel_data[i][3] for i in range(0, len(kernel_data))]
        kernel = [kernel_data[i][4] for i in range(0, len(kernel_data))]
        Kernel_NNLO_K4_mu3p2GeV_interp_list1 += [interp1d(z_fm, kernel, kind='cubic')]
    except:
        Kernel_NNLO_K4_mu3p2GeV_interp_list1 += [None]

    try:
        input_name = Kernel_NNLO_K4_mu3p2GeV_grid_file2.replace('#', str(i))
        kernel_data = np.loadtxt(input_name)
        z_fm = [kernel_data[i][3] for i in range(0, len(kernel_data))]
        kernel = [kernel_data[i][4] for i in range(0, len(kernel_data))]
        Kernel_NNLO_K4_mu3p2GeV_interp_list2 += [interp1d(z_fm, kernel, kind='cubic')]
    except:
        Kernel_NNLO_K4_mu3p2GeV_interp_list2 += [None]

#print(len(Kernel_NNLO_K4_mu3p2GeV_interp_list0))


def CnC0NNLO(mu,z,n,k=4):
    if abs(mu/5.0676896 - 3.2) < 0.1:
        fx0 = Kernel_NNLO_K4_mu3p2GeV_interp_list0[n]
        fx1 = Kernel_NNLO_K4_mu3p2GeV_interp_list1[n]
        fx2 = Kernel_NNLO_K4_mu3p2GeV_interp_list2[n]
    else:
        sys.exit('You should give a mu = 3.2 GeV.')
    #return fx0(z),fx1(z),fx2(z)
    return 0,fx1(z),0

#####################################################
###############    NNLOevo kernel    ################
#####################################################

Kernel_NNLOevo_mu3p2GeV_grid_file = "/Users/Xiang/Desktop/0study/research/lattice/data/rITD/kernel_NNLOevo_file/ListKernelNNLOevo_K*_mu3p2GeV_n#.txt"

Kernel_NNLOevo_mu3p2GeV_interp_list = []

for k in KernelKlist:
    Kernel_NNLOevo_mu3p2GeV_interp_list_k = []
    input_name_k = Kernel_NNLOevo_mu3p2GeV_grid_file.replace('*', str(k))
    for i in range(0, grid_nmax+1):
        try:
            
            input_name_n = input_name_k.replace('#', str(i))
            input_name_n0 = input_name_k.replace('#', str(0))
            #print(input_name_n)
            kernel_data_n = np.loadtxt(input_name_n)
            kernel_data_n0 = np.loadtxt(input_name_n0)
            z_fm = [kernel_data_n[i][3] for i in range(0, len(kernel_data_n))]
            kernel = [kernel_data_n[i][4]/kernel_data_n0[i][4] for i in range(0, len(kernel_data_n))]
            Kernel_NNLOevo_mu3p2GeV_interp_list_k += [interp1d(z_fm, kernel, kind='cubic')]
            #print(Kernel_NNLOevo_mu3p2GeV_interp_list_k[i](0.5))
            
        except:
            #traceback.print_exc()
            Kernel_NNLOevo_mu3p2GeV_interp_list_k += [None]
    Kernel_NNLOevo_mu3p2GeV_interp_list += [Kernel_NNLOevo_mu3p2GeV_interp_list_k]

def CnC0NNLOevo(mu,z,n,k=4):
    #print(round(k),n)
    #print(Kernel_NNLOevo_mu3p2GeV_interp_list)
    if abs(mu/5.0676896 - 3.2) < 0.1:
        fx1 = Kernel_NNLOevo_mu3p2GeV_interp_list[round(k)][n]
        #fx0 = Kernel_NNLOevo_mu3p2GeV_interp_list[round(k)][n]
        #fx2 = Kernel_NNLOevo_mu3p2GeV_interp_list[round(k)][n]
    else:
        sys.exit('You should give a mu = 3.2 GeV.')
    #return fx0(z),fx1(z),fx2(z)
    return 0,fx1(z),0