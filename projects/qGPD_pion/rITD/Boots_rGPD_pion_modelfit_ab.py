#!/usr/bin/env python3
import numpy as np
import sys
from statistics import *
from qGPD_pion.rITD.rITD_pion_ritdReal import *
from scipy.optimize import least_squares
from utils.tools import *
from scipy import special
from scipy.special import gamma
from scipy import integrate

np.random.seed(2020)
random_uniform = np.random.uniform(0,1,1000000)
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "rITD_pion_ritdReal_moments" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def PDF_ab(x,a,b):
    a = -abs(a)
    b = abs(b)
    return gamma(2 + a + b)/(gamma(1 + a) * gamma(1 + b)) * x**a * (1-x)**b

def moms_params(params, nmax):
    a = -abs(params[0])
    b = abs(params[1])
    moms = [(special.gamma(3+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(4+a+b))\
        ,(special.gamma(5+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(6+a+b))\
            ,(special.gamma(7+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(8+a+b))\
                ,(special.gamma(9+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(10+a+b))\
                    ,(special.gamma(11+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(12+a+b))\
                        ,(special.gamma(13+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(14+a+b))\
                            ,(special.gamma(15+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(16+a+b))\
                                ,(special.gamma(17+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(18+a+b))\
                                    ,(special.gamma(19+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(20+a+b))\
                                        ,(special.gamma(21+a) * special.gamma(2+a+b)) / (special.gamma(1+a) * special.gamma(22+a+b))]
    return moms[0:int(nmax/2)]

def ritd_Pzn_pznm_real(latsp, z, pz, mass, mu, nmax, moms,bconti,rcut,cmass,k=0, pznm=0, kernel_list=[], **kwargs):
    c = mass*mass/(4*pz*pz)
    ritdsum = 1
    ritdsum_down = 1
    mass0 = 0.14*fmGeV

    if len(bconti) < int(nmax/2):
        for i in range(len(bconti), int(nmax/2)):
            bconti += [0]

    if len(cmass) < int(nmax/2):
        for i in range(len(cmass),int(nmax/2)):
            cmass += [0]

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 1
        #for j in range(0, i+1):
        #    n_TMC += np.math.factorial(n-j)/np.math.factorial(j)/np.math.factorial(n-2*j)*c**j

        #print(n,i,kernel_list[i])
        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/np.math.factorial(n) * n_TMC * (moms[n]+cmass[i-1]*(mass-mass0)+bconti[i-1]*latsp*latsp))
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/np.math.factorial(n) * n_TMC * (moms[n]+cmass[i-1]*(mass-mass0)+bconti[i-1]*latsp*latsp))
    
    ritdsum += rcut*(latsp*pz)*(latsp*pz)
    ritdsum_down += rcut*(latsp*pznm)*(latsp*pznm)

    return float(ritdsum/ritdsum_down)

def make_ritd_Pzn_pznm_real(mu, nmax, contiN, CmassN, cutoff, k=0, Nparams=2,**kwargs):
    def ritd_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z_kernel, popt_list):

        latsp, mass, pznm, pz, z, kernel_list = latsp_mass_pznm_pz_z_kernel

        moms_list = moms_params(popt_list[0:Nparams],nmax)
        moms = [1]
        for imoms in range(0, len(moms_list)):
            moms += [0, moms_list[imoms]]

        bconti = [0 for i in range(0, int(nmax/2))]
        for i in range(0, int(contiN/2)):
            bconti[i] = popt_list[Nparams+i]
        cmass = [0 for i in range(0, int(nmax/2))]
        for i in range(0, int(CmassN/2)):
            cmass[i] = popt_list[Nparams+contiN+i]
        if cutoff == 0:
            rcut = 0 # cutoff effect
        elif cutoff == 1:
            rcut = popt_list[-1]
        else:
            rcut = cutoff
        func = ritd_Pzn_pznm_real(latsp,z,pz,mass,mu,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list, **kwargs)
        return func
    return ritd_Pzn_pznm_real_constrained

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### Read data here ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def read_rITD_fitdata(inputfile):

    readfile = open(inputfile,'r')
    commands = readfile.readlines()

    for index in range(0, len(commands)):

        line = commands[index].split()
        #print(line)

        if 'Ns' == line[0]:
            Ns = int(line[1])
        
        elif 'Nt' == line[0]:
            Nt = int(line[1])

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            print('  Lattice', str(Ns)+'c'+str(Nt)+',', 'a =', latsp, 'fm.')
        
        elif 'mass' in line[0]:
            mass = float(line[1])
            print('  Hardron mass:', mass, 'GeV')

        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            pxlist_fm = []
            pxlist_GeV = []
            for ipx in pxlist:
                pxlist_fm += [2*PI*ipx/(Ns*latsp)]
                pxlist_GeV += [round(2*PI*ipx/(Ns*latsp)/fmGeV,2)]
            print('  pzlist:',pxlist_GeV,'(GeV)')
            if len(pxlist) == 1:
                describe = describe.replace('pzup','pz'+str(pxmin))
            #print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            if pznm == 0:
                pznm = 0.0001
            pznm_fm = 2*PI*pznm/(Ns*latsp)
            pznm_GeV = round(2*PI*pznm/(Ns*latsp)/fmGeV,5)
            print('  pznm :',pznm_GeV,'(GeV)')

        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

    rITD_params = [Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]

    # Read the data here
    rITD_mean = [] #rITD_mean[ipx][iz]
    rITD_err = [] #rITD_mean_err[ipx][iz]
    rITD_sample_read = [] #rITD_sample[ipx][iz][isample]
    rITD_sample = [] #rITD_sample[isample][ipx][iz]
    rITD_samp_lenMax = 0
    for ipx in pxlist:

        ipx_mean_file = mean_input_format.replace('*', str(ipx))
        ipx_mean_data = np.loadtxt(ipx_mean_file)
        ipx_rITD_mean = []
        ipx_rITD_err = []
        zlen = len(ipx_mean_data)
        for iz in range(0, zlen):
            ipx_rITD_mean += [ipx_mean_data[iz][1]]
            ipx_rITD_err += [ipx_mean_data[iz][2]]
        rITD_mean += [ipx_rITD_mean]
        rITD_err += [ipx_rITD_err]

        ipx_sample_file = sample_input_format.replace('*', str(ipx))
        ipx_rITD_sample = []
        for iz in range(0, zlen):
            iz_sample_file = ipx_sample_file.replace('#', str(iz))
            ipx_rITD_sample += [np.loadtxt(iz_sample_file)]
            if len(ipx_rITD_sample[iz]) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(ipx_rITD_sample[iz])
        rITD_sample_read += [ipx_rITD_sample]
    print('  rITD data shape:', np.shape(rITD_sample_read), ', sample maximal length:', rITD_samp_lenMax)

    for isamp in range(0,rITD_samp_lenMax):
        ipx_rITD_sample = []
        for ipx in range(0, len(pxlist_fm)):
            iz_rITD_sample = []
            for iz in range(0, zlen):
                rITD_samp_len = len(rITD_sample_read[ipx][iz])
                #iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp%rITD_samp_len]]
                if isamp < rITD_samp_len:
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp]]
                else:
                    ipick = random_uniform_int[-1][isamp]
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][ipick%rITD_samp_len]]
                    #iz_rITD_sample += [rITD_sample_read[ipx][iz][np.random.randint(0, rITD_samp_len)]]
            ipx_rITD_sample += [iz_rITD_sample]
        rITD_sample += [ipx_rITD_sample]

    print('  mean data:', np.shape(rITD_mean), ', sample data:', np.shape(rITD_sample))
    return rITD_params, rITD_err, rITD_sample

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### fitting procedure ########################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def rITD_fit(fitfunc_name, rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu, nmax, alps=None,fixN = [], cutoff=0, contiN=0, CmassN=0, zcombine=True):

    # To determine the alphas of factorization scale
    mu_down = mu
    mu_mid = mu
    mu_up = mu

    #mu = mu*fmGeV
    #if alps == None:
    #    fixOrder_alps = alpsmu(mu)
    #else:
    #    fixOrder_alps = alps

    if 'ritdLO' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return 1
    elif 'ritdNLO' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLO(mu,z,n,**kwargs)
    elif 'ritdNLOEP' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLO_EP(mu,z,n,**kwargs)
    elif 'ritdNLONLL' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLONLL(mu,z,n,**kwargs)
    elif 'ritdNLOevo' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLOevo(mu,z,n,**kwargs)
    elif 'ritdNNLO' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLO(mu,z,n,**kwargs)
    elif 'ritdNNLOEP' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLO_EP(mu,z,n,**kwargs)
    elif 'ritdNNLOevo' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLOevo(mu,z,n,**kwargs)
    elif 'ritdNNLOevoSat' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLOevoSat(mu,z,n,**kwargs)
    elif 'ritdNLOevoSat' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLOevoSat(mu,z,n,**kwargs)

    poptlist = []
    zfmlist = []
    pdflist = []
    itdlist = []
    ritdlist = []
    zfm = zmv
    momsExpStart = [-2.3,-4.5,-6.6,-8.3,-10.2]
    for zfm in np.arange(zmax-zmv*2, zmax+zmv/2, zmv):

        # fit y
        itdLat = []
        itdLatErr = []
        
        # fit x: (latsp,mass,pznm,pz,z)
        itdlatsp_fm = []
        itdmass_fm = []
        itdpznm_fm = []
        itdpz_fm = []
        itdz_fm = []
        itdz_kernel = []

        for ilat in range(0, len(rITD_params)):

            ilat_rITD = rITD_sample[ilat]
            ilat_rITD_err = rITD_err[ilat]

            ilat_latsp = rITD_params[ilat][2]
            ilat_mass_fm = rITD_params[ilat][3] * fmGeV
            ilat_pznm = rITD_params[ilat][4]
            ilat_pzlist = rITD_params[ilat][-1]

            for ipx in range(0, len(ilat_pzlist)):

                if zcombine == True:
                    zmin = zskip
                else:
                    zmin =  int(zfm/ilat_latsp)

                for iz in range(zmin, round(zfm/ilat_latsp)+1):

                    itdLat += [ilat_rITD[ipx][iz]]
                    itdLatErr += [ilat_rITD_err[ipx][iz]]

                    itdlatsp_fm += [ilat_latsp]
                    itdmass_fm += [ilat_mass_fm]
                    itdpznm_fm += [ilat_pznm]
                    itdpz_fm += [ilat_pzlist[ipx]]
                    itdz_fm += [iz*ilat_latsp]
                    itdz_kernel += []

                    iz_kernel_mu = [1] + [kernel(mu_mid, iz*ilat_latsp, 2*i) for i in range(1, int(nmax/2)+1)]
                    iz_kernel_halfmu = [1] + [kernel(mu_down, iz*ilat_latsp, 2*i) for i in range(1, int(nmax/2)+1)]
                    iz_kernel_doubmu = [1] + [kernel(mu_up, iz*ilat_latsp, 2*i) for i in range(1, int(nmax/2)+1)]
                    iz_kernel = [iz_kernel_mu, iz_kernel_halfmu, iz_kernel_doubmu]
                    #iz_kernel = [iz_kernel_mu, iz_kernel_mu, iz_kernel_mu]
                    itdz_kernel += [iz_kernel]
                    #print(alpsmu(mu),alpsmu(mu/2),alpsmu(mu*2))

        #print(itdLat)
        if zcombine == True:
            def ritd_res(popt):
                res = []
                for i in range(0, len(itdz_fm)):
                    latsp_mass_pznm_pz_z_kernel0 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][0])
                    ritd_kernel0 = fitfunc(latsp_mass_pznm_pz_z_kernel0, popt)
                    latsp_mass_pznm_pz_z_kernel1 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][1])
                    #ritd_kernel1 = fitfunc(latsp_mass_pznm_pz_z_kernel1, popt)
                    ritd_kernel1 = 1
                    latsp_mass_pznm_pz_z_kernel2 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][2])
                    ritd_kernel2 = 1
                    
                    deviation = (ritd_kernel0 - itdLat[i])*(ritd_kernel0 - itdLat[i])
                    err = itdLatErr[i]*itdLatErr[i] + (ritd_kernel1-ritd_kernel2)/2*(ritd_kernel1-ritd_kernel2)/2
                    res += [np.sqrt(deviation/err)]
                return res
        else:
            def ritd_res(popt):
                res = []
                for i in range(0, len(itdz_fm)):
                    latsp_mass_pznm_pz_z_kernel0 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][0])
                    ritd_kernel0 = fitfunc(latsp_mass_pznm_pz_z_kernel0, popt)
                    ritd_kernel1 = 1
                    ritd_kernel2 = 1
                    
                    deviation = (ritd_kernel0 - itdLat[i])*(ritd_kernel0 - itdLat[i])
                    err = itdLatErr[i]*itdLatErr[i] + (ritd_kernel1-ritd_kernel2)/2*(ritd_kernel1-ritd_kernel2)/2
                    res += [np.sqrt(deviation/err)]
                return res


        poptstart = [-1,1]
        if contiN != 0:
            for i in range(0, int(contiN/2)):
                poptstart += [0.01]
        if CmassN != 0:
            for i in range(0, int(CmassN/2)):
                poptstart += [0.01]
        if cutoff != 0:
            poptstart += [0.01]
        #print('poptstart:', poptstart, ', zfm:', round(zfm,2))

        if zcombine == True and len(itdLat) < len(poptstart) + 2:
            continue
        zfmlist += [round(zfm,2)]

        res = least_squares(ritd_res, poptstart, method='lm')
        popt = res.x
        xlist = np.arange(0, 1000, 1)
        xlist = np.exp(-0.01*(xlist+0.0001))
        dxlist = 0.01*xlist
        zfm_pdflist = []
        for x in xlist:
            zfm_pdflist += [[x, PDF_ab(x, popt[0], popt[1])]]
        pdflist += [zfm_pdflist]
        zfm_itdlist = []
        zPzlist = np.arange(0, 20, 0.05)
        for zPz in zPzlist:
            def func(x):
                return PDF_ab(x, popt[0], popt[1]) * np.cos(x * zPz)
            zfm_itdlist += [[zPz, integrate.quad(func,0,1)[0]]]
        itdlist += [zfm_itdlist]

        zfm_ritdlist = []
        for i in range(0, len(itdz_fm)):
            latsp_mass_pznm_pz_z_kernel0 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][0])
            ritd_kernel0 = fitfunc(latsp_mass_pznm_pz_z_kernel0, popt)
            zfm_ritdlist += [[itdpz_fm[i],itdz_fm[i],ritd_kernel0]]
        ritdlist += [zfm_ritdlist]

        popt[0] = -abs(popt[0])
        popt[1] = abs(popt[1])
        resvec = res.fun
        chisq = sum(np.square(resvec)) / (len(itdz_fm)-len(popt))
        aic = sum(np.square(resvec)) + 2*len(popt)
        aicc = 0
        if zcombine == True:
            aicc = sum(np.square(resvec)) + 2*len(popt) + (2*len(popt)*len(popt)+2*len(popt))/(len(itdz_fm)-len(popt)-1)
        #popt = list(popt) + [chisq, aicc, aic]
        popt = list(popt) + moms_params(popt[0:2],nmax)[:2] + [chisq]
        poptlist += [popt]

    return poptlist, zfmlist, pdflist, itdlist, ritdlist

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    # read parameters
    fitfunc = []
    rITD_err = []
    poptguess = []
    fixN = []
    dataset = []
    cutoff = False
    fixOrder_alps = None
    conti = 0
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu' in line[0]:
            mu = float(line[1])
            print('Factorization scale:', mu, 'GeV', ', alpha_s =', alpsmu(mu*fmGeV))
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'fixOrder_alps' in line[0]:
            fixOrder_alps = float(line[1])
            print('Fix order alpha_s:', fixOrder_alps)
            describe = describe + '_alps' + line[1]

        elif 'zskip' in line[0]:
            zskip = int(line[1])
            print('Skip zmin (z/a) =', zskip)
            describe = describe.replace('zmin', 'zmin'+str(zskip))

        elif 'zmax(fm)' in line[0]:
            zmax = float(line[1])
            print('Zmax =', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmax))

        elif 'zmv(fm)' in line[0]:
            zmv = float(line[1])
            print('Z move =', zmv, 'fm')

        elif 'nmax' in line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)

        elif 'ritd' == line[0]:
            fitfunc += [line[1]]

        elif 'zCombine' in line[0]:
            if 'y' in line[1]:
                zcombine = True
                print('This is a combine fit of a range of z.')
            else:
                zcombine = False
                print('This is a fit for each z.')

        elif 'fixN' in line[0]:
            fixN = [float(line[i]) for i in range(1, len(line))]
            print('rITD fixed moments:', fixN)

        elif 'cutoff' == line[0]:
            if line[1] == 'y':
                cutoff = [1,1,1,1,1]
                describe = describe + '_rcut'
                print('Cutoff parameter: yes')
            elif line[1] == 'n':
                cutoff = [0,0,0,0,0]
                print('Cutoff parameter: no')
            else:
                cutoff = [float(line[1]) for i in range(1, len(line))]
                print('Cutoff parameter r=',cutoff)

        elif 'continuum' == line[0]:
            contiN = int(line[1])
            describe = describe + '_contiN' + str(contiN)

        elif 'masscorrection' == line[0]:
            CmassN = int(line[1])
            describe = describe + '_CmassN' + str(CmassN)

        elif 'data' == line[0]:
            print('Reading data:')
            # data_params=[Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]
            data_params, data_err, data_sample = read_rITD_fitdata(line[1])
            dataset += [[data_params, data_err, data_sample]]
            #print(np.shape(data_sample))
            rITD_err += [data_err]
            if len(data_sample) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(data_sample)

    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)

    for ifit in range(0, len(fitfunc)):

        moms_list = []
        out_name = outfolder+describe.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit])) + '.txt'
        print('\nGonna save in:',out_name)

        poptsample = [] #poptsample[isamp][iz][ipopt]
        pdfsamples = []
        itdsamples = []
        ritdsamples = []
        for isamp in range(0, rITD_samp_lenMax):
            rITD_sample = []
            rITD_params = []
            ipick = random_uniform_int[-2][isamp]
            for idata in dataset:
                rITD_params += [idata[0]]
                rITD_samp_len = len(idata[2])
                if isamp < rITD_samp_len:
                    rITD_sample += [idata[2][isamp]]
                else:
                    rITD_sample += [idata[2][ipick%rITD_samp_len]]
            popt, zfmlist, pdflist, itdlist,ritdlist = rITD_fit(fitfunc[ifit], rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu*fmGeV, nmax, fixOrder_alps, fixN, cutoff[ifit], contiN, CmassN, zcombine)
            print('Bootstrap sample:', isamp, '/', rITD_samp_lenMax,', popt:',  popt)
            poptsample += [popt]
            pdfsamples += [pdflist]
            itdsamples += [itdlist]
            ritdsamples += [ritdlist]

        for isamp in range(0, rITD_samp_lenMax):
            poptsample_save = []
            sample_name = samplefolder+describe+'_bootssample'+str(int(isamp))+'.txt'
            for iz in range(0, len(zfmlist)):
                poptsample_save += [[zskip, zfmlist[iz]] + poptsample[isamp][iz]]
            #print(np.shape(poptsample[isamp]),poptsample[0])
            np.savetxt(sample_name, poptsample_save)

        for iz in range(0, len(zfmlist)):
            iz_moms = [zskip, zfmlist[iz]]
            for ipopt in range(0, len(poptsample[0][iz])):
                ipopt = [poptsample[isamp][iz][ipopt] for isamp in range(0, rITD_samp_lenMax)]
                ipopt = sorted(ipopt)
                iz_moms += [(ipopt[high16]+ipopt[low16])/2, (ipopt[high16]-ipopt[low16])/2]
            np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
            #print(iz_moms)
            moms_list += [iz_moms]
        #print(out_name)
        np.savetxt(out_name, moms_list, fmt='%.4e')

        for iz in range(0, len(zfmlist)):
            pdf_0 = []
            pdf_1 = []
            pdf_2 = []
            for ix in range(0, len(pdfsamples[0][iz])):
                ix = len(pdfsamples[0][iz]) - ix - 1
                ix_samples = sorted([pdfsamples[isamp][iz][ix][1] for isamp in range(0, len(pdfsamples))])
                pdf_0 += [[pdfsamples[0][iz][ix][0], ix_samples[low16], pdfsamples[0][iz][ix][0]*ix_samples[low16]]]
                pdf_1 += [[pdfsamples[0][iz][ix][0], ix_samples[mid], pdfsamples[0][iz][ix][0]*ix_samples[mid]]]
                pdf_2 += [[pdfsamples[0][iz][ix][0], ix_samples[high16], pdfsamples[0][iz][ix][0]*ix_samples[high16]]]
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_PDF_band0.txt'
            np.savetxt(out_name, pdf_0, fmt='%.4e')
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_PDF_band1.txt'
            np.savetxt(out_name, pdf_1, fmt='%.4e')
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_PDF_band2.txt'
            np.savetxt(out_name, pdf_2, fmt='%.4e')

        for iz in range(0, len(zfmlist)):
            itd_0 = []
            itd_1 = []
            itd_2 = []
            for ix in range(0, len(itdsamples[0][iz])):
                ix_samples = sorted([itdsamples[isamp][iz][ix][1] for isamp in range(0, len(itdsamples))])
                itd_0 += [[itdsamples[0][iz][ix][0], ix_samples[low16]]]
                itd_1 += [[itdsamples[0][iz][ix][0], ix_samples[mid]]]
                itd_2 += [[itdsamples[0][iz][ix][0], ix_samples[high16]]]
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_ITD_band0.txt'
            np.savetxt(out_name, itd_0, fmt='%.4e')
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_ITD_band1.txt'
            np.savetxt(out_name, itd_1, fmt='%.4e')
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_ITD_band2.txt'
            np.savetxt(out_name, itd_2, fmt='%.4e')

        for iz in range(0, len(zfmlist)):
            itd_0 = []
            itd_1 = []
            itd_2 = []
            for ix in range(0, len(ritdsamples[0][iz])):
                ix_samples = sorted([ritdsamples[isamp][iz][ix][2] for isamp in range(0, len(ritdsamples))])
                itd_0 += [[ritdsamples[0][iz][ix][0]/fmGeV,ritdsamples[0][iz][ix][1], ix_samples[low16]]]
                itd_1 += [[ritdsamples[0][iz][ix][0]/fmGeV,ritdsamples[0][iz][ix][1], ix_samples[mid]]]
                itd_2 += [[ritdsamples[0][iz][ix][0]/fmGeV,ritdsamples[0][iz][ix][1], ix_samples[high16]]]
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_rITD_band0.txt'
            np.savetxt(out_name, itd_0, fmt='%.4e')
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_rITD_band1.txt'
            np.savetxt(out_name, itd_1, fmt='%.4e')
            out_name = outfolder+describe.replace(str(zmax),str(zfmlist[iz])) + '_rITD_band2.txt'
            np.savetxt(out_name, itd_2, fmt='%.4e')

        
