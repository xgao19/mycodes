#!/usr/bin/env python3
import numpy as np
import numba as nb
import sys
from utils.tools import *


'''----------------------------------'''
'''----------renormalization---------'''
'''----------------------------------'''

# bm (bare matrix elements) is a 1-D array. bm[z]
# dm : delta_m calculated from Polyakov loop
# dm [GeV]; a [fm]; zs [1]
def Poly_renorm(bm_up, bm_down, dm, a, zup, zs):
    fmGeV = 5.0676896
    return bm_up/bm_down*np.exp((zup-zs)*a*dm*fmGeV)

def Jackk_Poly_renorm(sampleup_format, sampledown_format, pzlist, zlist, dm, a, zs):

    zs_sampledown_format = sampledown_format.replace('#',str(zs))
    zs_sampledown = np.loadtxt(zs_sampledown_format)
    npick = len(zs_sampledown)
    print('npick:', npick)

    # sample collection
    PolyRbm_sample = []
    PolyRbmLog_sample = []
    PolyRbmLogDiff_sample = []
    for ipz in range(0, len(pzlist)):
        ipz_sampleup_format = sampleup_format.replace('*',str(pzlist[ipz]))
        ipz_PolyRbm_sample = []
        ipz_PolyRbmLog_sample = []
        ipz_PolyRbmLogDiff_sample = []

        for iz in range(0, len(zlist)):
            zup = zlist[iz]
            iz_sampleup_format = ipz_sampleup_format.replace('#',str(zup))
            iz_sampleup = np.loadtxt(iz_sampleup_format)

            iz_PolyRbm_sample = [zup]
            iz_PolyRbmLog_sample = [zup]
            iz_PolyRbmLogDiff_sample = [zup]
            for isamp in range(0, npick):
                iz_PolyRbm_sample += [Poly_renorm(iz_sampleup[isamp][0], zs_sampledown[isamp][0], dm, a, zup, zs)]
                iz_PolyRbmLog_sample += [np.log(Poly_renorm(iz_sampleup[isamp][0], zs_sampledown[isamp][0], dm, a, zup, zs))]
                if iz != 0:
                    iz_PolyRbmLogDiff_sample += [iz_PolyRbmLog_sample[isamp+1]-ipz_PolyRbmLog_sample[iz-1][isamp+1]]
                else:
                    iz_PolyRbmLogDiff_sample += [0]
            ipz_PolyRbm_sample += [iz_PolyRbm_sample]
            ipz_PolyRbmLog_sample += [iz_PolyRbmLog_sample]
            ipz_PolyRbmLogDiff_sample += [iz_PolyRbmLogDiff_sample]
        PolyRbm_sample += [ipz_PolyRbm_sample]
        PolyRbmLog_sample += [ipz_PolyRbmLog_sample]
        PolyRbmLogDiff_sample += [ipz_PolyRbmLogDiff_sample]
    print('Collected Hybrid renormalized samples:', np.shape(PolyRbm_sample))

    # error collection
    PolyRbm_popt = []
    for ipz in range(0, len(pzlist)):
        ipz_PolyRbm_popt = []

        for iz in range(0, len(zlist)):
            #print(PolyRbm_sample[ipz][iz][1:])
            #print(ipz,iz,PolyRbm_sample[0][0])
            iz_PolyRbm_sample = sorted(PolyRbm_sample[ipz][iz][1:])
            iz_PolyRbmLog_sample = sorted(PolyRbmLog_sample[ipz][iz][1:])
            iz_PolyRbmLogDiff_sample = sorted(PolyRbmLogDiff_sample[ipz][iz][1:])
            # z, zpz, ...
            iz_PolyRbm_popt = [PolyRbm_sample[ipz][iz][0]*a, pzlist[ipz]*zlist[iz]*2*3.14259/Ns, np.average(iz_PolyRbm_sample), np.std(iz_PolyRbm_sample)*np.sqrt(npick-1)]
            iz_PolyRbm_popt += [np.average(iz_PolyRbmLog_sample), np.std(iz_PolyRbmLog_sample)*np.sqrt(npick-1)]
            iz_PolyRbm_popt += [np.average(iz_PolyRbmLogDiff_sample), np.std(iz_PolyRbmLogDiff_sample)*np.sqrt(npick-1)]
            ipz_PolyRbm_popt += [iz_PolyRbm_popt]
            #print(iz_PolyRbm_popt)
        PolyRbm_popt += [ipz_PolyRbm_popt]
    print('Collected Hybrid renormalized errors:', np.shape(PolyRbm_popt),'\n')
    return PolyRbm_popt, PolyRbm_sample

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='rbm':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        #zstep = latsp
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zstep':
                zstep = float(line[1])
                print('zstep:',zstep,'fm')

            elif line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*zstep,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'pzrange':
                pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('pzlist:',pzlist)

            elif line[0] == 'zlist':
                zlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('zlist:','['+line[1]+', '+line[2]+']')

            elif line[0] == 'dma':
                dma = float(line[1])
                dm = dma/(latsp*fmGeV)
                print('dm*a:',dma, ', dm:', round(dm,4), 'GeV')

            elif line[0] == 'sampleup':
                sampleup_format = line[1]
                print('\nsampleup:', sampleup_format.split('/')[-1])
            elif line[0] == 'sampledown':
                sampledown_format = line[1]
                print('sampledown:', sampledown_format.split('/')[-1],'\n')

        PolyRbm_popt, PolyRbm_sample = Jackk_Poly_renorm(sampleup_format, sampledown_format, pzlist, zlist, dm, zstep, zs)
        for ipz in range(0, len(pzlist)):
            ipz_describe = describe.replace('*',str(pzlist[ipz]))
            print('>>> Saving:',ipz_describe)
            save_name = outfolder + ipz_describe + '.txt'
            sample_name = samplefolder + ipz_describe + '_jackksamples.txt'
            np.savetxt(save_name, PolyRbm_popt[ipz], fmt='%.8e')
            np.savetxt(sample_name, PolyRbm_sample[ipz], fmt='%.8e')
