#!/usr/bin/env python3
import numpy as np
import numba as nb
import traceback 
import sys
from utils.tools import *
import inspect
from scipy.optimize import least_squares

#from rITD_Numerical_Kernel import *

'''----------------------------------'''
'''--------------kernels-------------'''
'''----------------------------------'''

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

# mu [fm]
'''
@nb.jit
def alpsmu(mu):
    #alps0 = 0.24
    #mu0 = 3.2*5.0676896
    alps0 = 0.296
    mu0 = 2*5.0676896
    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
#print(alpsmu(2*5.0676896))
'''

def alpsmu(mu, Nf = 3):
    mu /= 5.0676896
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s

def Cn_NLO(z,n, mu):
    alps = alpsmu(mu)
    CF = 4/3
    gammaE = 0.5772156649
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

def Cn_NNLO(z, n, mu, alps = 0):
    if alps == 0:
        alps = alpsmu(mu)
    #print(z, n, mu/5.0676896, alps,CnNNLO(z, n, mu, alps))
    return CnNNLO(z, n, mu, alps)

def CnC0_NLO(mu,z,n):
    return Cn_NLO(z,n, mu)/Cn_NLO(z,0, mu)
def CnC0_NNLO(mu,z,n):
    return Cn_NNLO(z, n, mu)/Cn_NNLO(z, 0, mu)


'''----------------------------------'''
'''----------renormalization---------'''
'''----------------------------------'''

# bm (bare matrix elements) is a 1-D array. bm[z]
# dm : delta_m calculated from Polyakov loop
# dm [GeV]; a [fm]; zs [1]
def Hybrid_renorm(bm_up, bm_down, dm, a, zup, zdown, dm0, tw1):
    fmGeV = 5.0676896
    rbm_up = bm_up * np.exp(zup*a*dm*fmGeV) * np.exp(dm0*zup*a*fmGeV)
    rbm_down = (bm_down) * np.exp(zdown*a*dm*fmGeV) * np.exp(dm0*zdown*a*fmGeV) - tw1*(zdown*a*fmGeV)**2
    return rbm_up/rbm_down

def Hybrid_renorm_test(bm_up, bm_down, dm, a, zup, zdown, dm0, tw1):
    fmGeV = 5.0676896
    bm_down = (Cn_NNLO(zdown*a,0,mu*fmGeV) + tw1*(zdown*a*fmGeV)**2) * np.exp(-dm0*zdown*a*fmGeV)
    bm_up = (bm_up) * bm_down
    rbm_up = (bm_up) * np.exp(zup*a*dm*fmGeV) * np.exp(dm0*zup*a*fmGeV) - tw1*(zup*a*fmGeV)**2
    #rbm_up = bm_up * np.exp(zup*a*dm*fmGeV) * np.exp(dm0*zup*a*fmGeV)
    rbm_down = (bm_down) * np.exp(zdown*a*dm*fmGeV) * np.exp(dm0*zdown*a*fmGeV) - tw1*(zdown*a*fmGeV)**2
    return rbm_up/rbm_down

def Boots_Hybrid_renorm(sampleup_format, sampledown_format, pzlist, zlist, dm, a, zs, dm0_samples, tw1_samples):

    zs_sampledown_format = sampledown_format.replace('#',str(zs))
    print(zs_sampledown_format)
    zs_sampledown = np.loadtxt(zs_sampledown_format)
    npick = len(zs_sampledown)
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)
    print('npick:', npick)

    if len(dm0_samples) == 0:
        print('NOTE! dm0 is empty!')
        dm0_samples = np.zeros(npick)
    if len(tw1_samples) == 0:
        print('NOTE! tw1 is empty!')
        tw1_samples = np.zeros(npick)

    # sample collection
    HybRbm_sample = []
    for ipz in range(0, len(pzlist)):
        ipz_sampleup_format = sampleup_format.replace('*',str(pzlist[ipz]))
        ipz_HybRbm_sample = []

        iz_sampleup_format = ipz_sampleup_format.replace('#',str(0))
        iz_sampleup_z0 = np.loadtxt(iz_sampleup_format)
        iz_sampledown_format = sampledown_format.replace('#',str(0))
        iz_sampledown_z0 = np.loadtxt(iz_sampledown_format)

        for iz in range(0, len(zlist)):
            zup = zlist[iz]
            if zup <= zs:
                zdown = zup
            else:
                zdown = zs

            iz_sampleup_format = ipz_sampleup_format.replace('#',str(zup))
            iz_sampleup = np.loadtxt(iz_sampleup_format)
            iz_sampledown_format = sampledown_format.replace('#',str(zdown))
            iz_sampledown = np.loadtxt(iz_sampledown_format)

            iz_HybRbm_sample = [zup]
            for isamp in range(0, npick):
                #iz_HybRbm_sample += [Hybrid_renorm(iz_sampleup[isamp][0], iz_sampledown[isamp][0], dm, latsp, zup, zdown, dm0_samples[isamp], tw1_samples[isamp])]
                iz_HybRbm_sample += [Hybrid_renorm(iz_sampleup[isamp][0]/iz_sampleup_z0[isamp][0], iz_sampledown[isamp][0]/iz_sampledown_z0[isamp][0], dm, latsp, zup, zdown, dm0_samples[isamp], tw1_samples[isamp])]
                #iz_HybRbm_sample += [Hybrid_renorm(iz_sampleup[isamp][1], iz_sampledown[isamp][1], dm, latsp, zup, zdown, dm0_samples[isamp], tw1_samples[isamp])]
            ipz_HybRbm_sample += [iz_HybRbm_sample]
        HybRbm_sample += [ipz_HybRbm_sample]
    print('Collected Hybrid renormalized samples:', np.shape(HybRbm_sample))

    # error collection
    HybRbm_popt = []
    for ipz in range(0, len(pzlist)):
        ipz_HybRbm_popt = []

        for iz in range(0, len(zlist)):
            iz_HybRbm_sample = sorted(HybRbm_sample[ipz][iz])
            iz_HybRbm_popt = [HybRbm_sample[ipz][iz][0]*a, pzlist[ipz]*zlist[iz]*2*3.14259/Ns, (iz_HybRbm_sample[high16]+iz_HybRbm_sample[low16])/2, (iz_HybRbm_sample[high16]-iz_HybRbm_sample[low16])/2]
            ipz_HybRbm_popt += [iz_HybRbm_popt]
        HybRbm_popt += [ipz_HybRbm_popt]
    print('Collected Hybrid renormalized errors:', np.shape(HybRbm_popt),'\n')
    return HybRbm_popt, HybRbm_sample


'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='rbm':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        dm0_samples = []
        tw1_samples = []
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*latsp,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))
                print('mu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))

            elif line[0] == 'pzrange':
                pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('pzlist:',pzlist)

            elif line[0] == 'zlist':
                zlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('zlist:','['+line[1]+', '+line[2]+']')

            elif line[0] == 'dma':
                dma = float(line[1])
                dm = dma/(latsp*fmGeV)
                print('dm*a:',dma, ', dm:', round(dm,4), 'GeV')

            elif line[0] == 'dm0':
                dm0_read = np.loadtxt(line[1].replace('mu','mu'+str(mu).replace('.','p')))
                dm0_line = int(line[2])
                dm0_samples  = dm0_read[dm0_line][2:]

                print('Read dm0 '+ str(len(dm0_samples)) +' samples from z in', dm0_read[dm0_line][:2],'e.g.',dm0_samples[0],'...')
            elif line[0] == 'tw1':
                tw1_read = np.loadtxt(line[1].replace('mu','mu'+str(mu).replace('.','p')))
                tw1_line = int(line[2])
                tw1_samples  = tw1_read[dm0_line][2:]
                print('Read tw1 '+ str(len(tw1_samples)) +' samples from z in', tw1_read[dm0_line][:2],'e.g.',tw1_samples[0],'...')

            elif line[0] == 'sampleup':
                sampleup_format = line[1]
                print('\nsampleup:', sampleup_format.split('/')[-1])

            elif line[0] == 'sampledown':
                sampledown_format = line[1]
                print('sampledown:', sampledown_format.split('/')[-1],'\n')

        HybRbm_popt, HybRbm_sample = Boots_Hybrid_renorm(sampleup_format, sampledown_format, pzlist, zlist, dm, latsp, zs, dm0_samples, tw1_samples)
        for ipz in range(0, len(pzlist)):
            ipz_describe = describe.replace('*',str(pzlist[ipz]))
            print('>>> Saving:',ipz_describe)
            save_name = outfolder + ipz_describe + '.txt'
            sample_name = samplefolder + ipz_describe + '_bootssamples.txt'
            np.savetxt(save_name, HybRbm_popt[ipz], fmt='%.5e')
            np.savetxt(sample_name, HybRbm_sample[ipz], fmt='%.5e')
