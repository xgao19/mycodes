#!/usr/bin/env python3
import numpy as np
import math
import sys

from utils.tools import *
from qGPD_nucleon.Constants import *

from scipy.optimize import least_squares
from scipy.interpolate import interp1d

'''----------------------------------'''
'''--------------kernels-------------'''
'''----------------------------------'''

fmGeV = 5.0676896

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

def alphas(mu):
    return alphas_5loop(mu)

#mu in GeV, z in fm
def Cn_NLO(z, n, mu):
    alps = alphas(mu)
    z_tmp = z * fmGeV
    L = np.log(mu*mu*z_tmp*z_tmp*np.exp(2*gammaE)/4)
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*L\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

#mu in GeV, z in fm
def Cn_NLO_evo(z,n, mu):
    b1 = 11-2*3/3
    z_tmp = z * fmGeV
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n))
    muz0 = 0.75*np.sqrt(4/(z_tmp*z_tmp*np.exp(2*gammaE)))
    part0 = (5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)
    part1 = 1 + alphas(muz0)*CF/(2*np.pi)*part0
    part2 = (alphas(muz0)/alphas(mu))**Bn
    return part1*part2

'''
#mu in GeV, z in fm
def Cn_NLO_LRRevo(z,n, mu):
    b0 = (33-2*3)/3
    b1 = 2*(153-19*3)/3
    b2 = (2857-5033/9*3+325/27*nf**2)/2
    b = b1/(2*b0**2)
    c1 = (b1**2-b0*b2)/(4*b*b0**4)
    Nm = 0.575
    beta0 = 11-2*3/3
    Bn = 2*CF/beta0*((3+2*n)/(2+3*n+n*n)+2*Hn(n))

    z_tmp = z * fmGeV
    muz0 = 1*np.sqrt(4/(z_tmp*z_tmp*np.exp(2*gammaE)))
    alps = alphas(muz0)

    part0 = c1*alps - (np.exp(-2*np.pi/(alps*b0))*(alps*b1*c1+4*np.pi*b0)*re(ExpIntegralE(1+b1/(2*b0**2), -2*np.pi/(alps*b0)))/(2*b0**2))
    part1 = 1 + Nm*part0 + alps*(5/2*CF/(2*np.pi)-Nm*(1+c1))
    part2 = (alps/alphas(mu))**Bn
    return part1*part2
'''
#mu in GeV, z in fm
File_Cn_NLO_LRRevo = '/Users/Xiang/Desktop/docs/0-2022/projects/8-Coulomb-qPDF/analysis_prod/3-hybrid/GI/NLO+LRR+RG/0-PZ0_dm/kernels_GI_LRR_RG_#overZ.txt'
def Cn_NLO_LRRevo(z,n, mu):
    kernels = np.transpose(np.loadtxt(File_Cn_NLO_LRRevo.replace('#','075')))
    kernels_z = interp1d(kernels[0], kernels[1])
    return kernels_z(z)
#mu in GeV, z in fm
File_Cn_NLO_LRR = '/Users/Xiang/Desktop/docs/0-2022/projects/8-Coulomb-qPDF/analysis_prod/3-hybrid/GI/NLO+LRR/0-PZ0_dm/kernels_GI_LRR_mu#.txt'
def Cn_NLO_LRR(z,n, mu):
    kernels = np.transpose(np.loadtxt(File_Cn_NLO_LRR.replace('#','10')))
    kernels_z = interp1d(kernels[0], kernels[1])
    return kernels_z(z)

#mu in GeV, z in fm
def Cn_NLO_CG(z,n, mu):
    alps = alphas(mu)
    a_s = alps/(2*np.pi)
    z_tmp = z * fmGeV
    L = np.log(mu*mu*z_tmp*z_tmp*np.exp(2*gammaE)/4)
    return 1 + a_s*CF/2*(- L + 1)

#mu in GeV, z in fm
def Cn_NLO_CGevo(z,n, mu):

    z_tmp = z * fmGeV
    muz0 = 1.5*np.sqrt(4/(z_tmp*z_tmp*np.exp(2*gammaE)))
    alps = alphas(muz0)
    a_s = alps/(2*np.pi)
    b0 = (11-2*3/3)/2
    Bn = -CF/(2*b0)
    
    part0 = 1
    part1 = 1 + a_s*CF/2*part0
    part2 = (alphas(muz0)/alphas(mu))**Bn
    return part1*part2

'''----------------------------------'''
'''----------renormalization---------'''
'''----------------------------------'''

# bm (bare matrix elements) is a 1-D array. bm[z]
# dm : delta_m calculated from Polyakov loop
# dm [GeV]; a [fm]; zs [1]
def Poly_renorm(bm_up, bm_down, dm, a, zup, zs):
    return bm_up/bm_down*np.exp((zup-zs)*a*dm*fmGeV)
##################################################
# renormalon and higher twist model: fixed order
##################################################
# mu [GeV]; zs [1]; z [fm]
def Poly_renorm_GI(popt, zup, zs, a, mu):
    dm0 = popt[0]
    tw1 = popt[1]
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2
    OPEdown = Cn_NLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2
    return exp*(OPEup/OPEdown)

def Poly_renorm_CG(popt, zup, zs, a, mu):
    dm0 = 0
    tw1 = popt[0]
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO_CG(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2
    OPEdown = Cn_NLO_CG(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2
    return exp*(OPEup/OPEdown)

def Poly_renorm_GI_LRR(popt, zup, zs, a, mu):
    dm0 = popt[0]
    tw1 = popt[1]
    tw1 = 0
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO_LRR(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2
    OPEdown = Cn_NLO_LRR(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2
    return exp*(OPEup/OPEdown)

##################################################
# renormalon and higher twist model: resummed
##################################################
def Poly_renorm_GI_RG(popt, zup, zs, a, mu):
    dm0 = popt[0]
    tw1 = popt[1]
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO_evo(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2)
    OPEdown = Cn_NLO_evo(zs*a,0,mu) * np.exp(tw1*(zs*a*fmGeV)**2)
    return exp*(OPEup/OPEdown)
def Poly_renorm_GI_LRR_RG(popt, zup, zs, a, mu):
    dm0 = popt[0]
    tw1 = popt[1]
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO_LRRevo(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2)
    OPEdown = Cn_NLO_LRRevo(zs*a,0,mu) * np.exp(tw1*(zs*a*fmGeV)**2)
    return exp*(OPEup/OPEdown)


def Poly_renorm_CG_RG(popt, zup, zs, a, mu):
    dm0 = 0
    tw1 = popt[0]
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO_CGevo(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2)
    OPEdown = Cn_NLO_CGevo(zs*a,0,mu) * np.exp(tw1*(zs*a*fmGeV)**2)
    return exp*(OPEup/OPEdown)

class model_list:
    modelGI = Poly_renorm_GI
    modelGI_RG = Poly_renorm_GI_RG

    modelGI_LRR = Poly_renorm_GI_LRR
    modelGI_LRR_RG = Poly_renorm_GI_LRR_RG

    modelCG = Poly_renorm_CG
    modelCG_RG = Poly_renorm_CG_RG

'''----------------------------------'''
'''----------    #Read#     ---------'''
'''----------------------------------'''


def read_commands(filename, nParam):

    inputfile = open(filename,'r')
    commands = inputfile.readlines()

    # read parameters
    for index in range(0, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'lattice':
            Ns = int(line[1])
            Nt = int(line[2])
            latsp = float(line[3])
            print('Ensemble:',Ns,'x',Nt,', a =',latsp,'fm')

        elif line[0] == 'zs':
            zs = int(line[1])
            print('zs:',str(zs)+'a =',zs*latsp,'fm')

        elif line[0] == 'mass(GeV)':
            mass = float(line[1])
            print('mass:',mass,'GeV')

        elif line[0] == 'sample':
            sample = line[1]
            print('sample:', sample.split('/')[-1])
            sample = sample.replace('zs#','zs'+str(zs))
            sample = np.loadtxt(sample)
            print('sample shape:', np.shape(sample))
            sample_new = []
            mean_ave_new = []
            mean_err_new = []
            npick = len(sample[0]) - 1
            dw, up = int(0.16*npick), int(0.84*npick)
            for iz in range(0, len(sample)):
                row_new = np.array(sample[iz][1:]) / np.array(sample[iz-1][1:])
                sample_new += [[sample[iz][0]] + list(row_new)]
                row_sort = sorted(row_new)
                mean_ave_new += [(row_sort[up]+row_sort[dw])/2]
                mean_err_new += [(row_sort[up]-row_sort[dw])/2]
            sample = sample_new
            mean_ave = mean_ave_new
            mean_err = mean_err_new
            print('mean ave:',[round(mean_ave[i],5) for i in range(0, zs+1)],'...',np.shape(mean_ave))
            print('mean err:',[round(mean_err[i],5) for i in range(0, zs+1)],'...',np.shape(mean_err))
            lattice = [Ns, Nt, latsp, mass]

    return lattice, zs, sample, mean_err

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    fmGeV = 5.0676896

    # read parameters
    lattice_list = []
    zs_list = []
    sample_list = []
    err_list = []
    Nfix = 0
    for index in range(1, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'm0':
            m0_prior = float(line[1])
            m0_err = float(line[2])
            if m0_err < 1e5:
                describe = describe + '_m0'
                Nfix += 1
            print('m0 priored:', m0_prior, m0_err)

        elif line[0] == 'ht1':
            ht1_prior = float(line[1])
            ht1_err = float(line[2])
            if ht1_err < 1e5:
                describe = describe + '_ht1'
                Nfix += 1
            print('ht1 priored:', ht1_prior, ht1_err)

        elif line[0] == 'mu(GeV)':
            mu = float(line[1])
            print('\n\n\n\nmu:',mu,'GeV', ',alphas:', alphas(mu*fmGeV))
            describe = describe.replace('mu','mu'+str(mu).replace('.','p'))

        elif line[0] == 'model':
            models = [line[i] for i in range(1, len(line))]

        elif line[0] == 'nParam':
            nParam = int(line[1])
            print('Number of parameters:', nParam)
            describe = describe+'_nP'+str(nParam)

        elif line[0] == 'data':
            print('\n-------------------------------------------------')
            lattice, zs, sample, err = read_commands(line[1], nParam)
            lattice_list += [lattice]
            zs_list += [zs]
            sample_list += [sample]
            err_list += [err]
            print('-------------------------------------------------\n')

        elif line[0] == 'zrange':
            zmin = float(line[1])
            zmax = float(line[2])
            zstep = float(line[3])
            print(zmin, '[fm] to', zmax, '[fm] with interval', zstep, '[fm].')

    #if len(zs_list) == 1 and (zs_list[0]+1)*lattice_list[0][2] > zmin:
    #    zmin = (zs_list[0]+1)*lattice_list[0][2]
    #    print('zmin is re-setted:',zmin)

    npick = len(sample[0])-1
    print('sample count:', npick, '\n')
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    for imodel in models:

        fitfunc = getattr(model_list,imodel)
        imodel_describe = describe.replace('HTfit', imodel)
        
        #Poly_renorm_model1(popt, zup, zs, a, mu):

        def Fit_model(latsp, zup, zs, popt):
            return fitfunc(popt, zup, zs, latsp, mu)/fitfunc(popt, zup-1, zs, latsp, mu)

        # fit loop
        poptlist = []
        izmax = zmin
        popt_samples = [[] for i in range(0, nParam)]
        while izmax <= zmax:
            zlist = []
            N_data = 0
            for iLat in range(0, len(lattice_list)):
                iLat_latsp = lattice_list[iLat][2]
                #zlist += [[i for i in range(math.ceil(zmin/iLat_latsp), int(izmax/iLat_latsp)+1)]]
                zlist += [[i for i in range(int(izmax/iLat_latsp), int(izmax/iLat_latsp)+1)]]
                N_data += len(zlist[iLat])
            if N_data < nParam:
                izmax += zstep
                izmax = round(izmax,2)
                continue

            print('zlist:', zlist)

            poptstart = 0.01*np.ones(nParam)
            popt_sample = [[] for i in range(0, nParam)]
            chisq_sample = []
            
            for isamp in range(0, npick):
                
                def Fit_res(popt):
                    #res = [(m0_prior-popt[0])/m0_err, (ht1_prior-popt[1])/ht1_err]
                    res = []
                    for iLat in range(0, len(lattice_list)):
                        iLat_zlist = zlist[iLat]
                        iLat_Ns = lattice_list[iLat][0]
                        iLat_Nt = lattice_list[iLat][1]
                        iLat_latsp = lattice_list[iLat][2]
                        iLat_mass = lattice_list[iLat][3]
                        ilat_zs = zs_list[iLat]
                        ilat_sample = sample_list[iLat]
                        ilat_err = err_list[iLat]
                        #print('???\n')
                        for zup in iLat_zlist:
                            res += [(Fit_model(iLat_latsp, zup, ilat_zs, popt)-ilat_sample[zup][isamp+1])/(ilat_err[zup])]
                            #res += [(Fit_model(iLat_latsp, zup, ilat_zs, popt)-ilat_sample[zup][isamp+1])/(ilat_err[zup]+ilat_sample[zup][isamp+1]*0.01)]
                    return res
                    
                res = least_squares(Fit_res, poptstart, method='lm')
                popt = res.x
                chisq = sum(np.square(res.fun[2:])) / (N_data-len(popt)+Nfix)
                for i in range(0, nParam):
                    popt_sample[i] += [popt[i]]
                chisq_sample += [chisq]

            for i in range(0, nParam):
                popt_samples[i] += [[round(zlist[0][0]*lattice_list[0][2],4), round(zlist[0][-1]*lattice_list[0][2],4)]+popt_sample[i]]
                
            chisq = sorted(chisq_sample)[mid]
            izmax_poptlist = [round(zlist[0][0]*lattice_list[0][2],4), round(zlist[0][-1]*lattice_list[0][2],4)]
            for i in range(0, nParam):
                i_popt_sample = sorted(popt_sample[i])
                izmax_poptlist += [(i_popt_sample[high16]+i_popt_sample[low16])/2,(i_popt_sample[high16]-i_popt_sample[low16])/2]
            izmax_poptlist += [chisq]
            print('z in',izmax_poptlist[:2], ', chisq/dof:',round(izmax_poptlist[-1],4))
            poptlist += [izmax_poptlist]
            izmax += zstep
        
        save_name = outfolder + imodel_describe + '.txt'
        print('\nSaving: ', save_name.split('/')[-1])
        np.savetxt(save_name, poptlist, fmt='%.5e')

        print(np.shape(popt_samples))
        save_name = samplefolder + imodel_describe + '_popt'
        for i in range(0, nParam):
            i_save_name = save_name + str(i) + '_boots.txt'
            np.savetxt(i_save_name, popt_samples[i], fmt='%.5e')
