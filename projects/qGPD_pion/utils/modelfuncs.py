#!/usr/bin/env python3
import numpy as np
import os
from utils.tools import *
from math import gamma
from math import pow
import traceback 
from scipy import integrate
from scipy import special

#from rITD_pion_ritdReal import *

#------------------------------------------------------------#
###################### model functions #######################
#------------------------------------------------------------#
priorsign = 1000
# define the T~0 c2pt function
def oneexp(x, A0, E0):
    return A0 * np.exp(-E0 * x)
def twoexp(x, A0, E0, A1, E1):
    return A0 * np.exp(-E0 * x) + A1 * np.exp(-E1 * x)
def threeexp(x, A0, E0, A1, E1, A2, E2):
    return A0 * np.exp(-E0 * x) + A1 * np.exp(-E1 * x)+A2 * np.exp(-E2 * x)

# define the function to fit
TSIZE = 64                # length of t direction
def onestate(x, A0, E0):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate(x, A0, E0, A1, E1):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))
def threestate(x, A0, E0, A1, E1, A2, E2):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

def onestate_hirarchy(x, a0, E0):
    A0 = np.exp(a0)
    return A0 * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate_hirarchy(x, a0, E0, a1, e1):
    A0 = np.exp(a0)
    A1 = np.exp(a1)
    E1 = E0 + np.exp(e1)
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))
def threestate_hirarchy(x, a0, E0, a1, e1, a2, e2):
    A0 = np.exp(a0)
    A1 = np.exp(a1)
    A2 = np.exp(a2)
    E1 = E0 + np.exp(e1)
    E2 = E0 + np.exp(e1) + np.exp(e2)
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

#####################################################
###################gpd c3pt fit######################
#####################################################
#Ein is like [A0, E0, A1, E1, ...]
def gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    NT=64
    part00 = B00*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-Eout[1]*tau-Ein[1]*(ts-tau))
    part01 = B01*np.sqrt(Eout[0])*np.sqrt(Ein[2])*np.exp(-Eout[1]*tau-Ein[3]*(ts-tau))
    part10 = B10*np.sqrt(Eout[2])*np.sqrt(Ein[0])*np.exp(-Eout[3]*tau-Ein[1]*(ts-tau))
    part11 = B11*np.sqrt(Eout[2])*np.sqrt(Ein[2])*np.exp(-Eout[3]*tau-Ein[3]*(ts-tau))
    #part_thermo_1 = B_thermo_1*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*(ts-tau)-Eout[1]*(NT-ts))
    p000 = np.sqrt(Eout[1]**2 - Ein[1]**2)/2
    E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #print(Ein[1], Eout[1], Ein[1]+Eout[1],E11)
    part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-E11*tau-Ein[1]*(NT-ts))
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-Ein[1]*(NT-ts))
    part_thermo_3 = B_thermo_3*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    return part00 + part01 + part10 + part11 + part_thermo_2  + part_thermo_3

def gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    NT=64
    part00 = B00*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-Eout[1]*tau-Ein[1]*(ts-tau))
    part01 = B01*np.sqrt(Eout[0])*np.sqrt(Ein[2])*np.exp(-Eout[1]*tau-Ein[3]*(ts-tau))
    part10 = B10*np.sqrt(Eout[2])*np.sqrt(Ein[0])*np.exp(-Eout[3]*tau-Ein[1]*(ts-tau))
    part11 = B11*np.sqrt(Eout[2])*np.sqrt(Ein[2])*np.exp(-Eout[3]*tau-Ein[3]*(ts-tau))
    part02 = B02*np.sqrt(Eout[0])*np.sqrt(Ein[4])*np.exp(-Eout[1]*tau-Ein[5]*(ts-tau))
    part20 = B20*np.sqrt(Eout[4])*np.sqrt(Ein[0])*np.exp(-Eout[5]*tau-Ein[1]*(ts-tau))
    part12 = B12*np.sqrt(Eout[2])*np.sqrt(Ein[4])*np.exp(-Eout[3]*tau-Ein[5]*(ts-tau))
    part21 = B21*np.sqrt(Eout[4])*np.sqrt(Ein[2])*np.exp(-Eout[5]*tau-Ein[3]*(ts-tau))
    part22 = B22*np.sqrt(Eout[4])*np.sqrt(Ein[4])*np.exp(-Eout[5]*tau-Ein[5]*(ts-tau))
    part_thermo_1 = B_thermo_1*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    #p000 = np.sqrt(abs(Eout[1]**2 - Ein[1]**2))/2
    #E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-E11*tau-Ein[1]*(NT-ts))
    part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-Ein[1]*(NT-ts))
    part_thermo_3 = B_thermo_3*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    return part00 + part01 + part10 + part11 + part02 + part20 + part12 + part21+ part22 + part_thermo_1 + part_thermo_2  + part_thermo_3

def gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #print(Ein)
    part1 = gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2, B_thermo_3)/twostate(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part2 = twostate(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3])*twostate(tau, Ein[0],Ein[1],Ein[2],Ein[3])*twostate(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part3 = twostate(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3])*twostate(tau, Eout[0],Eout[1],Eout[2],Eout[3])*twostate(ts, Eout[0],Eout[1],Eout[2],Eout[3])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_twostate_exp(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #print(Ein)
    part1 = gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2, B_thermo_3)/twoexp(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part2 = twoexp(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(tau, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part3 = twoexp(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(tau, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(ts, Eout[0],Eout[1],Eout[2],Eout[3])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #B02 = 0
    #B20 = 0
    #B22=0
    part1 = gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1, B_thermo_2, B_thermo_3)/threestate(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part2 = threestate(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threestate(tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threestate(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part3 = threestate(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threestate(tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threestate(ts, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_threestate_exp(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #B02 = 0
    #B20 = 0
    #B22=0
    part1 = gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1, B_thermo_2, B_thermo_3)/threeexp(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part2 = threeexp(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threeexp(tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threeexp(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part3 = threeexp(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threeexp(tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threeexp(ts, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])
    return part1*np.sqrt(part2/part3)



#####################################################
##############     polynomial fit    ################
#####################################################
def const(x,a):
    return a
def const_sum(x,a):
    return a*(x-3)

def linearfunc(x, a, b):
    return a + b * x
def linearfunc_pcorrection(x, a, b, c, dE1=1000):
    return a + b * x + c * np.exp(-dE1*x)


def polynomialto2(x, b, c):
    return c + b * x * x

def polynomial2func(x, b):
    return 1 + b * x * x

def polynomial4func(x, b, c):
    return 1 + b * x * x + c * x * x * x * x

def polynomial6func(x, b, c, d):
    return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x

def polynomial8func(x, b, c, d, g):
    return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x + g * x * x * x * x * x * x * x * x

def polynomial10func(x, b, c, d, g, h):
    return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x + g * x * x * x * x * x * x * x * x + h * x * x * x * x * x * x * x * x * x * x
