#!/usr/bin/env python3
import numpy as np
import sys
from utils.tools import *

def data_collection(filename, sample, row):
    
    read_data = []
    for i in sample:
        read_data += [np.loadtxt(filename.replace('#', str(i)))]
    dataset = []
    for i in row:
        row_samples = []
        for isamp in range(0, len(read_data)):
            row = list(read_data[isamp][i])
            #x2, x4 = row[3], row[4]
            #row[3] = -7/12+35/12*x2
            #row[4] = 11/24-77/12*x2+77/8*x4
            #x1, x3 = row[2], row[3]
            #row[2] = 5/3*x1
            #row[3] = -9/4*x1 + 21/4*x3
            row_samples += [row]
        dataset += [row_samples]
    return dataset

def moms_conti(momsSample, contiSample, massSample=[], order=1):
    Latsp = 0
    up = int(len(momsSample)*0.84)
    mid = int(len(momsSample)*0.5)
    down = int(len(momsSample)*0.16)
    momslist0 = []
    momslist1 = []
    momslist2 = []
    while Latsp <= 0.1:
        iasp_momsSample = []
        if order == 1:
            x = Latsp
        elif order == 2:
            x = Latsp*Latsp
        for i in range(0,len(momsSample)):
            iasp_momsSample += [momsSample[i]+x*contiSample[i]]
        iasp_momsSample = sorted(iasp_momsSample)
        #if asq==0:
        #    print(iasp_momsSample)
        #momslist0 += [[asq, np.average(iasp_momsSample)-np.std(iasp_momsSample)*np.sqrt(len(iasp_momsSample)/(len(iasp_momsSample)-1))]]
        #momslist1 += [[asq, np.average(iasp_momsSample)]]
        #momslist2 += [[asq, np.average(iasp_momsSample)+np.std(iasp_momsSample)*np.sqrt(len(iasp_momsSample)/(len(iasp_momsSample)-1))]]
        momslist0 += [[Latsp*Latsp, iasp_momsSample[down]]]
        momslist1 += [[Latsp*Latsp, iasp_momsSample[mid]]]
        momslist2 += [[Latsp*Latsp, iasp_momsSample[up]]]
        Latsp += 0.001
    return momslist0,momslist1,momslist2


if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    line2nd = commands[1].split()
    sample = [i for i in range(int(line2nd[1]), int(line2nd[2])+1)]
    print('Sample in range: [', line2nd[1],',', line2nd[2],']')

    dataset = []
    orderLatsp = 2

    for index in range(2, len(commands)):

        line = commands[index].split()
        if 'data' == line[0]:
            filename = line[1]
            rowlist = [i for i in range(int(line[2]), int(line[3])+1)]
            print('Reading file:', filename.split('/')[-1], ', row in range: [', line[2],',', line[3],']')
            dataset += data_collection(filename, sample, rowlist)
        elif 'orderLatsp' == line[0]:
            orderLatsp = int(line[1])
            print('Order of continuum extrapolation:', orderLatsp)
    
    print('-- Read data with shape:', np.shape(dataset))

    new_dataset = np.zeros((len(dataset[0][0]), len(dataset[0]), len(dataset)))
    for iv in range(0, len(dataset[0][0])):        #variable
        for isamp in range(0, len(dataset[0])):    #sample
            for ic in range(0, len(dataset)):      #column
                new_dataset[iv][isamp][ic] = dataset[ic][isamp][iv]
                if isamp == 0 and iv == 0:
                    print(dataset[ic][isamp])
    print('-- Re-arrange the data with shape:', np.shape(new_dataset))

    MeanErrSamp = np.zeros((len(new_dataset), len(new_dataset[0]), 2))
    for iv in range(0, len(new_dataset)):
        for isamp in range(0, len(new_dataset[0])):
            #print(new_dataset[iv][isamp])
            MeanErrSamp[iv][isamp][0] = np.average(new_dataset[iv][isamp])
            MeanErrSamp[iv][isamp][1] = np.std(new_dataset[iv][isamp], ddof=1)

    up = int((len(sample)*0.84))
    mid = int((len(sample)*0.5))
    down = int((len(sample)*0.16))
    MeanErr = np.zeros((len(MeanErrSamp), 4))
    for iv in range(0, len(MeanErrSamp)):
        meanlist = sorted([MeanErrSamp[iv][isamp][0] for isamp in range(0, len(MeanErrSamp[iv]))])
        SysErrlist = sorted([MeanErrSamp[iv][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        MeanErr[iv][0] = (meanlist[down] + meanlist[up])/2
        MeanErr[iv][1] = (meanlist[up]-meanlist[down])/2
        #MeanErr[iv][2] = np.sqrt(MeanErr[iv][1]**2 + np.average(SysErrlist)**2)
        MeanErr[iv][2] = MeanErr[iv][1] + np.average(SysErrlist)
        MeanErr[iv][3] = np.average(SysErrlist)
        #MeanErr[iv][3] = MeanErr[iv][1] + np.average(SysErrlist)
        #MeanErr[iv][3] = np.sqrt(MeanErr[iv][1]**2 + MeanErr[iv][1]**2)
        #MeanErr[iv][4] = np.sqrt(MeanErr[iv][2]**2 + MeanErr[iv][2]**2)
        #MeanErr[iv][5] = np.average(SysErrlist)

    print(MeanErr)
    np.savetxt(outfolder+describe+'.txt', MeanErr)

    if 'joint' in describe:

        moms2Sample = [MeanErrSamp[2][isamp][0] for isamp in range(0, len(sample))]
        moms4Sample = [MeanErrSamp[3][isamp][0] for isamp in range(0, len(sample))]
        moms6Sample = [MeanErrSamp[4][isamp][0] for isamp in range(0, len(sample))]
        moms8Sample = [MeanErrSamp[5][isamp][0] for isamp in range(0, len(sample))]
        Conti2Sample = [MeanErrSamp[6][isamp][0] for isamp in range(0, len(sample))]
        Conti4Sample = [MeanErrSamp[7][isamp][0] for isamp in range(0, len(sample))]
        Conti6Sample = [MeanErrSamp[8][isamp][0] for isamp in range(0, len(sample))]
        mass2Sample = [0 for isamp in range(0, len(sample))]
        mass4Sample = [0 for isamp in range(0, len(sample))]
        mass6Sample = [0 for isamp in range(0, len(sample))]

        if len(MeanErrSamp) > 20:
            print('Mass correction included.')
            mass2Sample = [MeanErrSamp[9][isamp][0] for isamp in range(0, len(sample))]
            mass4Sample = [MeanErrSamp[10][isamp][0] for isamp in range(0, len(sample))]
            mass6Sample = [MeanErrSamp[11][isamp][0] for isamp in range(0, len(sample))]
        
        moms2list0,moms2list1,moms2list2 = moms_conti(moms2Sample,Conti2Sample,mass2Sample,order=orderLatsp)
        moms4list0,moms4list1,moms4list2 = moms_conti(moms4Sample,Conti4Sample,mass4Sample,order=orderLatsp)
        moms6list0,moms6list1,moms6list2 = moms_conti(moms6Sample,Conti6Sample,mass6Sample,order=orderLatsp)
        
        np.savetxt(outfolder+describe+'_moms2_plot0.txt', moms2list0)
        np.savetxt(outfolder+describe+'_moms2_plot1.txt', moms2list1)
        np.savetxt(outfolder+describe+'_moms2_plot2.txt', moms2list2)
        np.savetxt(outfolder+describe+'_moms4_plot0.txt', moms4list0)
        np.savetxt(outfolder+describe+'_moms4_plot1.txt', moms4list1)
        np.savetxt(outfolder+describe+'_moms4_plot2.txt', moms4list2)
        np.savetxt(outfolder+describe+'_moms6_plot0.txt', moms6list0)
        np.savetxt(outfolder+describe+'_moms6_plot1.txt', moms6list1)
        np.savetxt(outfolder+describe+'_moms6_plot2.txt', moms6list2)

        '''
        SysErrlist = sorted([MeanErrSamp[2][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        moms2list0Sys = [[moms2list0[i][0],moms2list1[i][1]-np.sqrt((moms2list1[i][1]-moms2list0[i][1])**2+SysErrlist[mid]**2)] for i in range(0, len(moms2list0))]
        moms2list1Sys = [[moms2list1[i][0],moms2list1[i][1]] for i in range(0, len(moms2list0))]
        moms2list2Sys = [[moms2list2[i][0],moms2list1[i][1]+np.sqrt((moms2list2[i][1]-moms2list1[i][1])**2+SysErrlist[mid]**2)] for i in range(0, len(moms2list0))]
        SysErrlist = sorted([MeanErrSamp[3][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        moms4list0Sys = [[moms4list0[i][0],moms4list1[i][1]-np.sqrt((moms4list1[i][1]-moms4list0[i][1])**2+SysErrlist[mid]**2)] for i in range(0, len(moms4list0))]
        moms4list1Sys = [[moms4list1[i][0],moms4list1[i][1]] for i in range(0, len(moms4list0))]
        moms4list2Sys = [[moms4list2[i][0],moms4list1[i][1]+np.sqrt((moms4list2[i][1]-moms4list1[i][1])**2+SysErrlist[mid]**2)] for i in range(0, len(moms4list0))]
        SysErrlist = sorted([MeanErrSamp[4][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        moms6list0Sys = [[moms6list0[i][0],moms6list1[i][1]-np.sqrt((moms6list1[i][1]-moms6list0[i][1])**2+SysErrlist[mid]**2)] for i in range(0, len(moms4list0))]
        moms6list1Sys = [[moms6list1[i][0],moms6list1[i][1]] for i in range(0, len(moms4list0))]
        moms6list2Sys = [[moms6list2[i][0],moms6list1[i][1]+np.sqrt((moms6list2[i][1]-moms6list1[i][1])**2+SysErrlist[mid]**2)] for i in range(0, len(moms4list0))]
        '''

        SysErrlist = sorted([MeanErrSamp[2][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        moms2list0Sys = [[moms2list0[i][0],moms2list0[i][1]-SysErrlist[mid]] for i in range(0, len(moms2list0))]
        moms2list1Sys = [[moms2list1[i][0],moms2list1[i][1]] for i in range(0, len(moms2list0))]
        moms2list2Sys = [[moms2list2[i][0],moms2list2[i][1]+SysErrlist[mid]] for i in range(0, len(moms2list0))]
        SysErrlist = sorted([MeanErrSamp[3][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        moms4list0Sys = [[moms4list0[i][0],moms4list0[i][1]-SysErrlist[mid]] for i in range(0, len(moms4list0))]
        moms4list1Sys = [[moms4list1[i][0],moms4list1[i][1]] for i in range(0, len(moms4list0))]
        moms4list2Sys = [[moms4list2[i][0],moms4list2[i][1]+SysErrlist[mid]] for i in range(0, len(moms4list0))]
        SysErrlist = sorted([MeanErrSamp[4][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        moms6list0Sys = [[moms6list0[i][0],moms6list0[i][1]-SysErrlist[mid]] for i in range(0, len(moms6list0))]
        moms6list1Sys = [[moms6list1[i][0],moms6list1[i][1]] for i in range(0, len(moms4list0))]
        moms6list2Sys = [[moms6list2[i][0],moms6list2[i][1]+SysErrlist[mid]] for i in range(0, len(moms6list0))]
        #print('\n\n,!!!',moms2list0)
        #print('\n\n,???',moms2list0Sys)
        np.savetxt(outfolder+describe+'_moms2_plotsys0.txt', moms2list0Sys)
        np.savetxt(outfolder+describe+'_moms2_plotsys1.txt', moms2list1Sys)
        np.savetxt(outfolder+describe+'_moms2_plotsys2.txt', moms2list2Sys)
        np.savetxt(outfolder+describe+'_moms4_plotsys0.txt', moms4list0Sys)
        np.savetxt(outfolder+describe+'_moms4_plotsys1.txt', moms4list1Sys)
        np.savetxt(outfolder+describe+'_moms4_plotsys2.txt', moms4list2Sys)
        np.savetxt(outfolder+describe+'_moms6_plotsys0.txt', moms6list0Sys)
        np.savetxt(outfolder+describe+'_moms6_plotsys1.txt', moms6list1Sys)
        np.savetxt(outfolder+describe+'_moms6_plotsys2.txt', moms6list2Sys)
        
        


