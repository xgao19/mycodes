#!/usr/bin/env python3
import numpy as np
import sys
from  scipy.special import gamma
from  scipy.special import jv
from statistics import *
from scipy.optimize import least_squares
from utils.tools import *
from utils.Constants import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#


def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

def gamma_n(n, alps):
    return alps*CF/(4*np.pi) * (4*Hn(n+1) - 2/(n+1)/(n+2) - 3)

#mu in GeV
def cnNLO_Z5_C(n, alps):
    return 1 + alps*CF/(2*np.pi) * ((7+2*n)/(2+3*n+n*n) + 2*(1-Hn(n))*Hn(n) - 2*Hn2(n))

#mu in GeV
def alphas(mu, Nf = 3):
    return alphas_5loop(mu, Nf)

def moms_exp(exp_list):
    exp_list1 = list(map(np.exp, exp_list))
    exp_list2= []
    for i in range(0, len(exp_list1)):
        exp_list2 += [sum(exp_list1[i:])]
    moms_list = []
    #for i in range(0, len(exp_list2)):
    #    moms_list += [sum(exp_list2[i:])]
    return exp_list1

def FnLO_Z5_C(n, mu, alps, lbd, z):
    alps = 0
    gO = alps*CF/(4*np.pi) * 3 #gamma_O
    gn = gamma_n(n, alps) #gamma_n
    part1 = cnNLO_Z5_C(n, alps) * (mu*mu*z*z)**(gn+gO) * gamma(2-gn)*gamma(1+n)/gamma(1+n+gn)
    part2 = 3/4*(complex(0,1))**n*np.sqrt(np.pi) * (n+1)*(n+2)/2 * gamma(n+gn+5/2)/gamma(n+5/2)
    part3 = (lbd/2)**(-3/2-gn) * jv(n+gn+3/2, lbd)
    #print(part1, part2, part3)
    #print('LO',n, lbd, (lbd/2)**(-3/2) * jv(n+gn+3/2, lbd))
    return (part1 * part2 * part3)


def ITD_DA_M(moms, z_fm, pz, nmax, mu, alps):

    z = z_fm * fmGeV # GeV

    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)

    C0_n  = np.array(moms)

    C1_0 = 3/2*L + 7/2
    C1_1 = (17/6*L - 1/2) * moms[1]
    C1_2 = (43/12*L - 37/12) * moms[2] + 11/12 - 5/12*L
    C1_3 = (247/60*L - 923/180) * moms[3] + (79/60 - 11/20*L) * moms[1]
    C1_4 = (68/15*L -247/36) * moms[4] + (5/3 - 19/30*L) * moms[2] + 1/4 - 2/15*L

    C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_0, C1_1, C1_2, C1_3, C1_4])

    ITD = 1 + alps*CF/(2*np.pi) * C1_0
    for n in range(1, nmax+1):
        ITD += C_n[n] * (complex(0,-1)*z*pz/2)**n / np.math.factorial(n)
    #print(C_n, ITD, (complex(0,-1)*z*pz/2)**n / np.math.factorial(n), z_fm, pz)

    return ITD.real

def ITD_DA_M_Real(moms, z_fm, pz, nmax, mu, alps):

    z = z_fm * fmGeV # GeV

    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)

    C0_n  = np.array(moms)

    C1_0 = 3/2*L + 7/2
    C1_2 = (43/12*L - 37/12) * moms[1] + 11/12 - 5/12*L
    C1_4 = (68/15*L -247/36) * moms[2] + (5/3 - 19/30*L) * moms[1] + 1/4 - 2/15*L
    C1_6 = (-122831 + 65115*L)/12600 * moms[3] + (627 - 205*L)/280 * moms[2] + (383 - 195*L)/840 * moms[1] + (1475 - 825*L)/12600


    #C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_0, C1_2])
    #C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_0, C1_2, C1_4])
    C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_0, C1_2, C1_4, C1_6])

    ITD = 1 + alps*CF/(2*np.pi) * C1_0
    for i in range(1, round(nmax/2)+1):
        n = i * 2
        ITD += C_n[i] * (-1)**i*(z*pz/2)**n / np.math.factorial(n)
    #print(C_n, ITD, (complex(0,-1)*z*pz/2)**n / np.math.factorial(n), z_fm, pz)

    return ITD

def ITD_DA_M_Imag(moms, z_fm, pz, nmax, mu, alps):

    #moms[1] = 0

    z = z_fm * fmGeV # GeV

    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)

    #C0_n  = np.array(moms[:3])
    C0_n  = np.array(moms[:2])
    #C0_n  = np.array(moms[:1])
    #moms[1] = 0

    C1_1 = (17/6*L - 1/2) * moms[0]
    C1_3 = (247/60*L - 923/180) * moms[1] + (79/60 - 11/20*L) * moms[0]
    #C1_5 = (30720/6300*L - 52813/6300) * moms[2] + (414/210 - 145/210*L) * moms[1] + (149/420 - 80/420*L) * moms[0]

    #C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_1, C1_3, C1_5])
    C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_1, C1_3])
    #C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_1])

    ITD = 0
    for i in range(0, round(nmax/2)):
        n = i * 2 + 1
        ITD += C_n[i] * (-1)**(i+1)*(z*pz/2)**n / np.math.factorial(n)
        ITD += (z/pz) * moms[round(nmax/2)+i] * (-1)**i*(z*pz/2)**n / np.math.factorial(n)

    #print(C_n, ITD, (complex(0,-1)*z*pz/2)**n / np.math.factorial(n), z_fm, pz)
    return ITD

'''
def ITD_DA_M_Imag(moms, z_fm, pz, nmax, mu, alps):

    #moms[1] = 0

    z = z_fm * fmGeV # GeV

    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)

    #C0_n  = np.array(moms[:3])
    C0_n  = np.array(moms[:2])
    #C0_n  = np.array(moms[:1])

    C1_1 = (17/6*L - 1/2) * moms[0]
    C1_3 = (247/60*L - 923/180) * moms[1] + (79/60 - 11/20*L) * moms[0]
    #C1_5 = (30720/6300*L - 52813/6300) * moms[2] + (414/210 - 145/210*L) * moms[1] + (149/420 - 80/420*L) * moms[0]

    #C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_1, C1_3, C1_5])
    C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_1, C1_3])
    #C_n = C0_n + alps*CF/(2*np.pi) * np.array([C1_1])

    ITD = 0
    for i in range(0, round(nmax/2)):
        n = i * 2 + 1
        ITD += C_n[i] * (-1)**i*(z*pz/2)**n / np.math.factorial(n)
        #ITD += (-1)**i*(z*pz/2)**n / np.math.factorial(n) / pz**2 * moms[round(nmax/2)+i]
        ITD += (-1)**i*(z*pz/2)**(n-1) / np.math.factorial(n-1) * (z/pz) * moms[round(nmax/2)+i]
        #ITD += (FnLO_Z5_C(n, mu, alps, z*pz, z)/complex(0,1)).real * moms[round(nmax/2)+i] / pz**2

    #print(C_n, ITD, (complex(0,-1)*z*pz/2)**n / np.math.factorial(n), z_fm, pz)
    return ITD
'''

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    print('\n\n\n\n\n\n\njob describe:',describe,', Ns =', Ns)

    # read parameters
    fitfunc = []
    rITD_err = []
    poptguess = []
    fixN = []
    dataset = []
    cutoff = False
    fixOrder_alps = None
    conti = 0
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu(GeV)' == line[0]:
            mu = float(line[1])
            print('Factorization scale:', mu, 'GeV', ', alpha_s =', round(alphas(mu),3))
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            dP = 2*np.pi/(Ns*latsp*fmGeV)
            print('Lattice spacing is', latsp, ', dP =', dP, '[GeV]')

        elif 'bzmin' == line[0]:
            bzmin = int(line[1])
            print('bzmin (z/a) =', bzmin)
            describe = describe.replace('bzmin', 'bzmin'+str(bzmin))

        elif 'bzmax' == line[0]:
            bzmax = int(line[1])
            print('bzmax (z/a)=', bzmax)
            describe = describe.replace('_bzmax', '_bzmax'+str(bzmax))

        elif 'bzmaxband' == line[0]:
            bzmaxband = int(line[1])
            ibzmaxband = bzmaxband - bzmin

        elif 'nmax' == line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)
            describe = describe.replace('nmax', 'nmax'+line[1])

        elif 'kernel' == line[0]:
            fitfunc += [line[1]]
            print('matching kernel:', line[1])

        elif 'zCombine' in line[0]:
            if 'y' in line[1]:
                zcombine = True
                print('This is a combine fit of a range of z.')
            else:
                zcombine = False
                print('This is a fit for each z.')
                describe = describe + "_iz"

        elif 'cutoff-y' == line[0]:
            cutoff = True
            print('Adding a cut off effect parameter l(aP_z)^2')
            describe = describe + "_cut"

        elif 'PN' == line[0]:
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('PN list:', pzlist)

        elif 'PD' == line[0]:
            PD = int(line[1])
            print('PD:', PD)
            if PD == 0:
                PD += 1E-4

        elif 'meanerr' == line[0]:
            MeanErr = []
            MeanErr_Format = line[1]
            for ip, pz in enumerate(pzlist):
                ipErr = []
                ipRead = np.loadtxt(MeanErr_Format.replace('*', str(pz)))
                for i in range(bzmin, bzmax+1):
                    ipErr += [[ipRead[i][0], ipRead[i][3], ipRead[i][4]]]
                #print(ipErr)
                MeanErr += [ipErr]
            print('Reading data:', MeanErr_Format.split('/')[-1], 'in shape', np.shape(MeanErr))

        elif 'samples' == line[0]:
            samples_All = []
            samples_Format = line[1]
            for ip, pz in enumerate(pzlist):
                ipSamples = []
                ip_Format = samples_Format.replace('*', str(pz))
                for iz in range(bzmin, bzmax+1):
                    izRead = np.loadtxt(ip_Format.replace('#', str(iz)))
                    ipSamples += [izRead]
                    #print(pz,iz, ipSamples)
                samples_All += [ipSamples]
            print('Reading data:', samples_Format.split('/')[-1], 'in shape', np.shape(samples_All))

    print('\n\n---')
    for ifit in range(0, len(fitfunc)):
        print(str(fitfunc[ifit]))
        moms_list = []
        out_name = outfolder+describe.replace('moms','moms_'+str(fitfunc[ifit])) + '.txt'


        if '_LO' in str(fitfunc[ifit]):
            alps = 0
        elif '_NLO' in str(fitfunc[ifit]):
            alps = alphas(mu)
        def ITD_Real(moms, z, pz):
            return ITD_DA_M_Real(moms, z, pz, 6, mu, alps)
        def ITD_Imag(moms, z, pz):
            return ITD_DA_M_Imag(moms, z, pz, nmax, mu, alps)
        
        def rITD(moms, z, pz):
            return ITD_Imag(moms, z, pz)/ITD_Real(np.array([1, 0.250464833, 0.118503583, 4.761066666666666919e-02]), z, PD*dP)

        model_samples = []
        popt_samples = []
        if cutoff == False:
            poptstart = [1e-5 for i in range(0, round(nmax))]
        elif cutoff == True:
            poptstart = [1e-5 for i in range(0, round(nmax)+1)]
        #poptstart[0] = 1

        for isamp in range(0, len(samples_All[0][0])):

            if (isamp+1) % 10 == 0:
                print('  ', isamp+1, '/', len(samples_All[0][0]))
            
            if zcombine == True:
                zminlist = [0 for i in range(0, len(samples_All[0]))]
                zmaxlist = [i for i in range(0, len(samples_All[0]))]
            elif zcombine == False:
                zminlist = [i for i in range(0, len(samples_All[0]))]
                zmaxlist = [i for i in range(0, len(samples_All[0]))]

            #print([MeanErr[0][iz][0] for iz in range(0, len(MeanErr[0]))])
            #print(zminlist)
            #print(zmaxlist)
            #print('\n')

            popt_zmax = []
            for i in range(0, len(zmaxlist)):
                izmin = zminlist[i]
                izmax = zmaxlist[i]
                def Fit_res(popt):
                    res = []
                    for ip, PN in enumerate(pzlist):
                        for iz in range(izmin, izmax+1):
                            z = MeanErr[ip][iz][0] * latsp # fm; bz = z/2
                            pz = PN*dP # GeV
                            #print(z, pz)
                            if cutoff == False:
                                rITD_diff = rITD(popt, z, pz) - samples_All[ip][iz][isamp]
                            elif cutoff == True:
                                rITD_diff = rITD_cutoff(popt[:-1], z, pz, popt[-1]) - samples_All[ip][iz][isamp]
                            res += [rITD_diff/MeanErr[ip][iz][2]]
                    #res += [(popt[0]+0.015)/0.01*len(res)]
                    #res += [(popt[0]-0.011)/5*len(res)*1000]
                    return res
                res = least_squares(Fit_res, poptstart, method='lm')
                popt = list(res.x)
                chisq = sum(np.square(res.fun[1:])) / (len(samples_All[0])*len(pzlist)-nmax/2)
                popt += [chisq]
                popt_zmax += [popt]
            popt_samples += [popt_zmax]

            '''
            model_P = []
            for ip, PN in enumerate(pzlist):
                pz = PN*dP # GeV

                if cutoff == False:
                    def model(z):
                        return rITD(res.x, z, pz)
                elif cutoff == True:
                    def model(z):
                        return rITD_cutoff(res.x[:-1], z, pz, res.x[-1])
                model_curve = []
                zskip = 0.01
                model_z = np.array([1e-3] + list(np.arange(zskip, 2*bzmax*latsp+latsp, zskip)))
                for iz, z in enumerate(model_z):
                    model_curve += [model(z)]
                model_P += [model_curve]
            #model_samples += [model_P]
            '''

            model_z = []
            for iz, zmax in enumerate(zmaxlist):
                z = 2 * MeanErr[0][iz][0] * latsp # fm; bz = z/2

                if zcombine == True:
                    popt_band = popt_zmax[ibzmaxband][:-1]
                elif zcombine == False:
                    popt_band = popt_zmax[iz][:-1]

                if cutoff == False:
                    def model(pz):
                        #print(res.x, z, zpz/(z*fmGeV))
                        return rITD(popt_band, z, pz)
                elif cutoff == True:
                    def model(pz):
                        return rITD_cutoff(popt_band[:-1], z, pz, popt_band[-1])
                model_curve = []
                pzskip = 0.02
                model_pz = np.array([1e-4] + list(np.arange(pzskip, 3, pzskip)))
                for ipz, pz in enumerate(model_pz):
                    model_curve += [model(pz)]
                model_z += [model_curve]
            model_samples += [model_z]


        print(fitfunc[ifit] ,'>> Shape of popt:', np.shape(popt_samples), '>> Shape of curve:', np.shape(model_samples))
        
        npick = len(popt_samples)
        low = int(0.16*npick)
        mid = int(0.5*npick)
        up  = int(0.84*npick)
        popt_list = []
        for iz in range(0, len(popt_samples[0])):
            iz_popt = [MeanErr[0][zminlist[iz]][0], MeanErr[0][zmaxlist[iz]][0]]
            for ip in range(0, len(popt_samples[0][0])):
                ip_samples = [popt_samples[i][iz][ip] for i in range(0, len(popt_samples))]
                ip_samples_sorted = sorted(ip_samples)
                iz_popt += [(ip_samples_sorted[up]+ip_samples_sorted[low])/2, (ip_samples_sorted[up]-ip_samples_sorted[low])/2]
            popt_list += [iz_popt]
        savename = out_name.replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1]))
        print('Gonna save in:', savename, '\n')
        np.savetxt(savename, popt_list, fmt='%.6f')

        for iz in range(0, len(zmaxlist)):
            z = 2 * MeanErr[0][iz][0] * latsp # fm; bz = z/2
            iz_real_curve0 = []
            iz_real_curve1 = []
            iz_real_curve2 = []
            for ip, pz in enumerate(model_pz):
                ip_samples = [model_samples[isamp][iz][ip] for isamp in range(0, len(model_samples))]
                ip_samples_sorted = sorted(ip_samples)
                ip_mean = (ip_samples_sorted[up] + ip_samples_sorted[low])/2
                ip_err  = (ip_samples_sorted[up] - ip_samples_sorted[low])/2
                iz_real_curve0 += [[pz, z*pz*fmGeV, ip_mean-ip_err]]
                iz_real_curve1 += [[pz, z*pz*fmGeV, ip_mean]]
                iz_real_curve2 += [[pz, z*pz*fmGeV, ip_mean+ip_err]]

            if zcombine == False:
                iz_out_name = out_name.replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1])).replace('iz.txt','iz'+str(bzmin+zmaxlist[iz])+'_band#.txt')
            elif zcombine == True:
                iz_out_name = out_name.replace('_bzmax'+str(bzmax), '_bzmax'+str(bzmaxband)).replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1])).replace('.txt','_iz'+str(bzmin+zmaxlist[iz])+'_band#.txt')
            savename = iz_out_name.replace('#', '0')
            #np.savetxt(savename, iz_real_curve0, fmt='%.6f')
            savename = iz_out_name.replace('#', '1')
            #np.savetxt(savename, iz_real_curve1, fmt='%.6f')
            savename = iz_out_name.replace('#', '2')
            #np.savetxt(savename, iz_real_curve2, fmt='%.6f')

            iz_MeanErr = [[PN*dP, PN*dP*z*fmGeV, MeanErr[ip][iz][1], MeanErr[ip][iz][2]] for ip, PN in enumerate(pzlist)]
            savename = iz_out_name.replace('band#', 'data')
            #np.savetxt(savename, iz_MeanErr, fmt='%.6f')
