#!/usr/bin/env python3
import numpy as np
import sys
from statistics import *
from scipy.optimize import least_squares
from scipy.special import gamma
from scipy.special import hyp2f1
from utils.tools import *
from utils.Constants import *



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################## model functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def PDF_ab(x,a,b):
    a = abs(a)
    b = abs(b)
    return gamma(2 + a + b)/(gamma(1 + a) * gamma(1 + b)) * x**a * (1-x)**b

def moms_ab(n, params):
    a = abs(params[0])
    b = abs(params[1])
    return gamma(2+a+b)/(gamma(1+a)*gamma(1+b)) * (2**(-1-a) * gamma(-1-a-n)*gamma(1+n) * hyp2f1(1+a,-b,2+a+n,0.5)/gamma(-a) \
                                                   + (-1)**n * 2**(-1-a) * gamma(1+a)*gamma(1+n) * hyp2f1(1+a,-b,2+a+n,0.5)/gamma(2+a+n) \
                                                    +  2**n * gamma(1+b)*gamma(1+a+n) * hyp2f1(-1-a-b-n,-n,-a-n,0.5)/gamma(2+a+b+n))

def Cn_M(n, L, moms_in):

    moms = list(moms_in) + [0 for i in range(len(moms_in), 11)]

    switcher = {
        0: 3/2*L + 7/2,
        1: (17/6*L - 1/2) * moms[1],
        2: (43/12*L - 37/12) * moms[2] + 11/12 - 5/12*L,
        3: (247/60*L - 923/180) * moms[3] + (79/60 - 11/20*L) * moms[1],
        4: (68/15*L - 247/36) * moms[4] + (5/3 - 19/30*L) * moms[2] + 1/4 - 2/15*L,
        5: (30720/6300*L - 52813/6300) * moms[5] + (414/210 - 145/210*L) * moms[3]\
          + (149/420 - 80/420*L) * moms[1],
        6: (-122831 + 65115*L)/12600 * moms[6] + (627 - 205*L)/280 * moms[4]\
          + (383 - 195*L)/840 * moms[2] + (1475 - 825*L)/12600,
        7: (-969497 + 478205*L)/88200 * moms[7] + (6243 - 1925*L)/2520 * moms[5]\
          + (1389 - 665*L)/2520  * moms[3] + (83 - 49*L)/504 * moms[1],
        8: (-2140979 + 996100*L)/176400 * moms[8] + (3391 - 994*L)/1260 * moms[6] \
        + (1613 - 728*L)/2520 * moms[4] + (267 - 154*L)/1260 * moms[2] - 7/720*(-7 + 4*L),
        9: (-230527361 + 102141900*L)/17463600 * moms[9] + (50386140 - 14129640*L)/17463600 * moms[7]\
          + (12624570 - 5397840*L)/17463600 * moms[5] + (4500300 - 2487240*L)/17463600  * moms[3]\
              + (1664775 - 1031940*L)/17463600 * moms[1],
        10: (-247891673 + 105343560*L)/17463600 * moms[10] + (42446 - 11445*L)/13860 * moms[8]\
          - (-22181 + 9030*L)/27720 * moms[6] + (12542 - 6615*L)/41580 * moms[4]\
              - (-971 + 600*L)/7920 * moms[2] + (147 - 85*L)/3300,
    }

    return switcher.get(n, "nothing")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ########################## ITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#mu in GeV
def alphas(mu, Nf = 3):
    return alphas_5loop(mu, Nf)

def ITD_DA_M(moms, z_fm, pz, nmax, mu, alps):

    z = z_fm * fmGeV # GeV

    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)

    C0_n  = np.array(moms)

    pQCDCorrection = np.zeros(len(moms))
    for i in range(0, len(moms)):
        pQCDCorrection[i] = Cn_M(i, L, moms)
    
    C_n = C0_n + alps*CF/(2*np.pi) * pQCDCorrection

    ITD = 1 + alps*CF/(2*np.pi) *  Cn_M(0, L, moms)
    for n in range(1, nmax+1):
        ITD += C_n[n] * (complex(0,-1)*z*pz/2)**n / np.math.factorial(n)

    return ITD

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    print('\n\n\n\n\n\n\njob describe:',describe,', Ns =', Ns)

    # read parameters
    fitfunc = []
    rITD_err = []
    poptguess = []
    fixN = []
    dataset = []
    cutoff = False
    fixOrder_alps = None
    conti = 0
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu(GeV)' == line[0]:
            mu = float(line[1])
            print('Factorization scale:', mu, 'GeV', ', alpha_s =', round(alphas(mu),3))
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            dP = 2*np.pi/(Ns*latsp*fmGeV)
            print('Lattice spacing is', latsp, ', dP =', dP, '[GeV]')

        elif 'bzmin' == line[0]:
            bzmin = int(line[1])
            print('bzmin (z/a) =', bzmin)
            describe = describe.replace('bzmin', 'bzmin'+str(bzmin))

        elif 'bzmax' == line[0]:
            bzmax = int(line[1])
            print('bzmax (z/a)=', bzmax)
            describe = describe.replace('_bzmax', '_bzmax'+str(bzmax))

        elif 'bzmaxband' == line[0]:
            bzmaxband = int(line[1])
            ibzmaxband = bzmaxband - bzmin

        elif 'nmax' == line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)
            describe = describe.replace('nmax', 'nmax'+line[1])

        elif 'kernel' == line[0]:
            fitfunc += [line[1]]
            print('matching kernel:', line[1])

        elif 'zCombine' in line[0]:
            if 'y' in line[1]:
                zcombine = True
                print('This is a combine fit of a range of z.')
            else:
                zcombine = False
                print('This is a fit for each z.')
                describe = describe + "_iz"

        elif 'cutoff-y' == line[0]:
            cutoff = True
            print('Adding a cut off effect parameter l(aP_z)^2')
            describe = describe + "_cut"

        elif 'PN' == line[0]:
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('PN list:', pzlist)

        elif 'PD' == line[0]:
            PD = int(line[1])
            print('PD:', PD)
            if PD == 0:
                PD += 1E-4

        elif 'meanerr' == line[0]:
            MeanErr = []
            MeanErr_Format = line[1]
            for ip, pz in enumerate(pzlist):
                ipErr = []
                ipRead_real = np.loadtxt(MeanErr_Format.replace('*', str(pz)))
                ipRead_imag = np.loadtxt(MeanErr_Format.replace('*', str(pz)).replace('real', 'imag'))
                for i in range(bzmin, bzmax+1):
                    ipErr += [[ipRead_real[i][0], ipRead_real[i][3], complex(ipRead_real[i][4],ipRead_imag[i][4])]]
                #print(ipErr)
                MeanErr += [ipErr]
            print('Reading data:', MeanErr_Format.split('/')[-1], 'in shape', np.shape(MeanErr))

        elif 'samples' == line[0]:
            samples_All = []
            samples_Format = line[1]
            for ip, pz in enumerate(pzlist):
                ipSamples = []
                ip_Format = samples_Format.replace('*', str(pz))
                for iz in range(bzmin, bzmax+1):
                    izRead_real = np.loadtxt(ip_Format.replace('#', str(iz)))
                    izRead_imag = np.loadtxt(ip_Format.replace('#', str(iz)).replace('real', 'imag'))
                    izRead = [complex(izRead_real[i], izRead_imag[i]) for i in range(0, len(izRead_real))]
                    ipSamples += [izRead]
                    #print(pz,iz, ipSamples)
                samples_All += [ipSamples]
            print('Reading data:', samples_Format.split('/')[-1], 'in shape', np.shape(samples_All))

    print('\n\n---')
    for ifit in range(0, len(fitfunc)):
        print(str(fitfunc[ifit]))
        moms_list = []
        out_name = outfolder+describe.replace('moms','moms_'+str(fitfunc[ifit])) + '.txt'

        if '_LO' in str(fitfunc[ifit]):
            alps = 0
        elif '_NLO' in str(fitfunc[ifit]):
            alps = alphas(mu)

        def ITD(moms, z, pz):
            return ITD_DA_M(moms, z, pz, nmax, mu, alps)
        
        def rITD(moms, z, pz):
            return ITD(moms, z, pz)/(ITD(moms, z, PD*dP).real)
        def rITD_cutoff(moms, z, pz, l2):
            return (ITD(moms, z, pz) + l2*(latsp*pz)**2)/(ITD(moms, z, PD*dP).real + l2*(latsp*PD*dP)**2)

        model_samples = []
        popt_samples = []
        pdf_samples = []
        xlist = np.array([i*0.01 for i in range(0, 100)])

        if cutoff == False:
            poptstart = [0.1 for i in range(0, 2)]
        elif cutoff == True:
            poptstart = [0.1 for i in range(0, 2+1)]

        for isamp in range(0, len(samples_All[0][0])):

            if (isamp+1) % 10 == 0:
                print('  ', isamp+1, '/', len(samples_All[0][0]))
            
            if zcombine == True:
                zminlist = [0 for i in range(0, len(samples_All[0]))]
                zmaxlist = [i for i in range(0, len(samples_All[0]))]
            elif zcombine == False:
                zminlist = [i for i in range(0, len(samples_All[0]))]
                zmaxlist = [i for i in range(0, len(samples_All[0]))]

            popt_zmax = []
            model_samples_zmax = []
            pdf_zmax = []
            for i in range(0, len(zmaxlist)):
                izmin = zminlist[i]
                izmax = zmaxlist[i]
                def Fit_res(popt):
                    res = []
                    for ip, PN in enumerate(pzlist):
                        for iz in range(izmin, izmax+1):
                            z = MeanErr[ip][iz][0] * latsp # fm; bz = z/2
                            pz = PN*dP # GeV
                            #print(z, pz)
                            if cutoff == False:
                                moms = [moms_ab(n, popt) for n in range(0, nmax+1)]
                                #print(popt, moms, rITD(moms, z, pz), samples_All[ip][iz][isamp])
                                rITD_diff = rITD(moms, z, pz) - samples_All[ip][iz][isamp]
                            elif cutoff == True:
                                moms = [moms_ab(n, popt[:-1]) for n in range(0, nmax+1)]
                                rITD_diff = rITD_cutoff(moms, z, pz, popt[-1]) - samples_All[ip][iz][isamp]
                            res += [rITD_diff.real/MeanErr[ip][iz][2].real]
                            if PN == 9:
                                res += [rITD_diff.imag/MeanErr[ip][iz][2].imag]
                    #res += [(popt[0]+0.015)/0.01*len(res)]
                    #res += [popt[2]/popt[1]/5*len(res)]
                    return res
                res = least_squares(Fit_res, poptstart, method='lm')
                popt = list(res.x)
                #pdf_x = PDF_ab(xlist,popt[0],popt[1])
                chisq = sum(np.square(res.fun[1:])) / ((izmax-izmin+1)*len(pzlist)+(izmax-izmin+1)-len(popt))
                popt += [moms_ab(n, res.x) for n in range(0, nmax+1)]
                popt += [chisq]
                #print(popt, [moms_ab(n, popt) for n in range(0, nmax+1)])
                popt_zmax += [popt]
                #pdf_zmax += [pdf_x]

                model_P = []
                for ip, PN in enumerate(pzlist):
                    pz = PN*dP # GeV

                    if cutoff == False:
                        moms = [moms_ab(n, res.x) for n in range(0, nmax+1)]
                        def model(z):
                            return rITD(moms, z, pz)
                    elif cutoff == True:
                        moms = [moms_ab(n, res.x[:-1]) for n in range(0, nmax+1)]
                        def model(z):
                            return rITD_cutoff(moms, z, pz, res.x[-1])
                    model_curve = []
                    zskip = 0.01
                    model_z = np.array([1e-3] + list(np.arange(zskip, 2*bzmax*latsp+latsp, zskip)))
                    for iz, z in enumerate(model_z):
                        model_curve += [model(z)]
                    model_P += [model_curve]
                model_samples_zmax += [model_P]

            popt_samples += [popt_zmax]
            model_samples += [model_samples_zmax]
            #pdf_samples += [pdf_zmax]

            '''
            model_z = []
            for iz, zmax in enumerate(zmaxlist):
                z = 2 * MeanErr[0][iz][0] * latsp # fm; bz = z/2

                if zcombine == True:
                    popt_band = popt_zmax[ibzmaxband][:-1]
                elif zcombine == False:
                    popt_band = popt_zmax[iz][:-1]

                if cutoff == False:
                    def model(pz):
                        #print(res.x, z, zpz/(z*fmGeV))
                        return rITD(popt_band, z, pz)
                elif cutoff == True:
                    def model(pz):
                        return rITD_cutoff(popt_band[:-1], z, pz, popt_band[-1])
                model_curve = []
                pzskip = 0.02
                model_pz = np.array([1e-4] + list(np.arange(pzskip, 3, pzskip)))
                for ipz, pz in enumerate(model_pz):
                    model_curve += [model(pz)]
                model_z += [model_curve]
            model_samples += [model_z]
        '''

        print(fitfunc[ifit] ,'>> Shape of popt:', np.shape(popt_samples), '>> Shape of curve:', np.shape(model_samples))
        
        npick = len(popt_samples)
        low = int(0.16*npick)
        mid = int(0.5*npick)
        up  = int(0.84*npick)

        # save parameter samples
        for isamp in range(0, len(popt_samples)):
            samples = []
            for iz in range(0, len(popt_samples[0])):
                samples += [[MeanErr[0][zminlist[iz]][0], MeanErr[0][zmaxlist[iz]][0]]+popt_samples[isamp][iz]]
            savename = samplefolder+describe.replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1])) + 'sample'+str(isamp) + '.txt'
            np.savetxt(savename, samples, fmt='%.6f')

        # save parameter mean + errors
        popt_list = []
        for iz in range(0, len(popt_samples[0])):
            iz_popt = [MeanErr[0][zminlist[iz]][0], MeanErr[0][zmaxlist[iz]][0]]
            for ip in range(0, len(popt_samples[0][0])):
                ip_samples = [popt_samples[i][iz][ip] for i in range(0, len(popt_samples))]
                ip_samples_sorted = sorted(ip_samples)
                iz_popt += [(ip_samples_sorted[up]+ip_samples_sorted[low])/2, (ip_samples_sorted[up]-ip_samples_sorted[low])/2]
            popt_list += [iz_popt]
        savename = out_name.replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1]))
        print('Gonna save in:', savename, '\n')
        np.savetxt(savename, popt_list, fmt='%.6f')

        print('>>>',np.shape(model_samples))
        for izmax in range(0, len(zmaxlist)):
            for ip, PN in enumerate(pzlist):
                pz = PN*dP # GeV
                ip_real_curve0, ip_imag_curve0 = [], []
                ip_real_curve1, ip_imag_curve1 = [], []
                ip_real_curve2, ip_imag_curve2 = [], []
                for iz in range(0, len(model_samples[isamp][0][0])):
                    z = model_z[iz]
                    iz_samples = [model_samples[isamp][izmax][ip][iz].real for isamp in range(0, len(model_samples))]
                    iz_samples_sorted = sorted(iz_samples)
                    iz_mean = (iz_samples_sorted[up] + iz_samples_sorted[low])/2
                    iz_err  = (iz_samples_sorted[up] - iz_samples_sorted[low])/2
                    ip_real_curve0 += [[z, z*pz*fmGeV, iz_mean-iz_err]]
                    ip_real_curve1 += [[z, z*pz*fmGeV, iz_mean]]
                    ip_real_curve2 += [[z, z*pz*fmGeV, iz_mean+iz_err]]

                    iz_samples = [model_samples[isamp][izmax][ip][iz].imag for isamp in range(0, len(model_samples))]
                    iz_samples_sorted = sorted(iz_samples)
                    iz_mean = (iz_samples_sorted[up] + iz_samples_sorted[low])/2
                    iz_err  = (iz_samples_sorted[up] - iz_samples_sorted[low])/2
                    ip_imag_curve0 += [[z, z*pz*fmGeV, iz_mean-iz_err]]
                    ip_imag_curve1 += [[z, z*pz*fmGeV, iz_mean]]
                    ip_imag_curve2 += [[z, z*pz*fmGeV, iz_mean+iz_err]]

                ip_out_name = out_name.replace(outfolder, bandfolder).replace('_bzmax'+str(bzmax), '_bzmax'+str(bzmaxband)).replace('PN', 'PN'+str(PN)).replace('.txt','_izmax'+str(bzmin+zmaxlist[izmax])+'_real_band#.txt')
                savename = ip_out_name.replace('#', '0')
                np.savetxt(savename, ip_real_curve0, fmt='%.6f')
                savename = ip_out_name.replace('#', '1')
                np.savetxt(savename, ip_real_curve1, fmt='%.6f')
                savename = ip_out_name.replace('#', '2')
                np.savetxt(savename, ip_real_curve2, fmt='%.6f')
                ip_out_name = out_name.replace(outfolder, bandfolder).replace('_bzmax'+str(bzmax), '_bzmax'+str(bzmaxband)).replace('PN', 'PN'+str(PN)).replace('.txt','_izmax'+str(bzmin+zmaxlist[izmax])+'_imag_band#.txt')
                savename = ip_out_name.replace('#', '0')
                np.savetxt(savename, ip_imag_curve0, fmt='%.6f')
                savename = ip_out_name.replace('#', '1')
                np.savetxt(savename, ip_imag_curve1, fmt='%.6f')
                savename = ip_out_name.replace('#', '2')
                np.savetxt(savename, ip_imag_curve2, fmt='%.6f')

            #iz_MeanErr = [[PN*dP, PN*dP*z*fmGeV, MeanErr[ip][iz][1], MeanErr[ip][iz][2]] for ip, PN in enumerate(pzlist)]
            #savename = ip_out_name.replace('band#', 'data')
            #np.savetxt(savename, iz_MeanErr, fmt='%.6f')

        '''
        for iz in range(0, len(zmaxlist)):
            z = 2 * MeanErr[0][iz][0] * latsp # fm; bz = z/2
            iz_real_curve0 = []
            iz_real_curve1 = []
            iz_real_curve2 = []
            for ip, pz in enumerate(model_pz):
                ip_samples = [model_samples[isamp][iz][ip] for isamp in range(0, len(model_samples))]
                ip_samples_sorted = sorted(ip_samples)
                ip_mean = (ip_samples_sorted[up] + ip_samples_sorted[low])/2
                ip_err  = (ip_samples_sorted[up] - ip_samples_sorted[low])/2
                iz_real_curve0 += [[pz, z*pz*fmGeV, ip_mean-ip_err]]
                iz_real_curve1 += [[pz, z*pz*fmGeV, ip_mean]]
                iz_real_curve2 += [[pz, z*pz*fmGeV, ip_mean+ip_err]]

            if zcombine == False:
                iz_out_name = out_name.replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1])).replace('iz.txt','iz'+str(bzmin+zmaxlist[iz])+'_band#.txt')
            elif zcombine == True:
                iz_out_name = out_name.replace('_bzmax'+str(bzmax), '_bzmax'+str(bzmaxband)).replace('PN', 'PN'+str(pzlist[0])+'-'+str(pzlist[-1])).replace('.txt','_iz'+str(bzmin+zmaxlist[iz])+'_band#.txt')
            savename = iz_out_name.replace('#', '0')
            #np.savetxt(savename, iz_real_curve0, fmt='%.6f')
            savename = iz_out_name.replace('#', '1')
            #np.savetxt(savename, iz_real_curve1, fmt='%.6f')
            savename = iz_out_name.replace('#', '2')
            #np.savetxt(savename, iz_real_curve2, fmt='%.6f')

            iz_MeanErr = [[PN*dP, PN*dP*z*fmGeV, MeanErr[ip][iz][1], MeanErr[ip][iz][2]] for ip, PN in enumerate(pzlist)]
            savename = iz_out_name.replace('band#', 'data')
            #np.savetxt(savename, iz_MeanErr, fmt='%.6f')
        '''
