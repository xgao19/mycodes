#!/usr/bin/env python3
import numpy as np
import sys
from utils.tools import *
from statistics import*

def bootsbm_collect(sample_format,col_sample,zlist,pxlist, qxyz=[]):
    
    # data collection
    bm_mean_data = []
    bm_samples_data = []
    for ipx in range(0,len(pxlist)):
        if len(qxyz) != 0:
            ipx_sample_format = sample_format.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            #if pxlist[ipx] == 5 and 'qx0qy2qz2' in ipx_mean_format:
            #    ipx_mean_format = ipx_mean_format.replace('4ts', '3ts')
            #    ipx_sample_format = ipx_sample_format.replace('4ts', '3ts')

        sampledata = (np.loadtxt((ipx_sample_format.replace('*',str(pxlist[ipx]))).replace('#', str(zlist[0]))))
        nboots = len(sampledata)

        ipx_in_format = ipx_sample_format.replace('*', str(pxlist[ipx]))
        ipx_data = []

        for iz in range(0, len(zlist)):
            iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            jacksample_real = np.loadtxt(iz_ipx_in_name)
            nboots = len(jacksample_real)
            #print(jacksample_real)
            if 'real' in iz_ipx_in_name:
                jacksample_imag = np.loadtxt(iz_ipx_in_name.replace('real','imag'))
                try:
                    jacksample = [complex(jacksample_real[i][col_sample],jacksample_imag[i][col_sample]) for i in range(0, nboots)]
                except:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, nboots)]
            else:
                try:
                    jacksample = [jacksample_real[i][col_sample] for i in range(0, nboots)]
                except:
                    jacksample = [jacksample_real[i] for i in range(0, nboots)]
            ipx_data += [jacksample]
        bm_samples_data += [ipx_data]
    print('bare matrix samples shape:', np.shape(bm_samples_data),'\n')
    return bm_samples_data


def bootsbm_to_rbm(Ns,latsp,up_sample,up_col_sample, down_sample,down_col_sample, \
                zlist, pxlist, outfolder, describe, qxyz):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    # data collection
    bm_samples_data = bootsbm_collect(up_sample,up_col_sample,zlist,pxlist,qxyz)
    bm_samples_norm = bootsbm_collect(down_sample,down_col_sample,zlist,pxlist,qxyz)

    rITD_samp_lenMax = 0
    for isamp in bm_samples_data:
        for izsamp in isamp:
            if len(izsamp) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(izsamp)
    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)
        
    # reduced bare matrix elements
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_bootssamples'
    
    for ipx in range(0, len(pxlist)):

        if pxlist[ipx] == 0:
            print('px=0 skipped.')
            continue
        
        ipx_ritd_real = []
        ipx_ritd_imag = []
        
        for iz in range(0, len(zlist)):

            p0_cfg = len(bm_samples_norm[0][iz])
            px_cfg = len(bm_samples_data[ipx][iz])

            print('z=',zlist[iz],'p0 #samples:', p0_cfg, 'px', pxlist[ipx], 'nboots:', px_cfg)
            
            ipx_iz_ritd_real = []
            ipx_iz_ritd_imag = []
            sample_real = []
            sample_imag = []

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))

            phase = 2*np.pi/Ns * pxlist[ipx]
            bm_samples_px = np.array(bm_samples_data[ipx][iz])
            bm_samples_p0 = np.array(bm_samples_norm[0][iz])
            #bm_samples_px = (bm_samples_px - bm_samples_p0)*np.exp(complex(0, -phase*zlist[iz]/2))
            bm_samples_px = (bm_samples_px*np.exp(complex(0, -phase*zlist[iz]/2)) - bm_samples_p0)
            #print(pxlist[ipx], (2*np.pi/Ns*pxlist[ipx]), (2*np.pi/Ns*pxlist[ipx])/(latsp*5.0676896))
            for isamp in range(0, rITD_samp_lenMax):
                
                isample = bm_samples_px[isamp]/bm_samples_data[ipx][0][isamp]
                sample_real += [isample.real]
                sample_imag += [isample.imag]
            
            sort_sample_real = sorted(sample_real)
            sort_sample_imag = sorted(sample_imag)
            mean_real = sort_sample_real[mid]
            mean_real_err = (sort_sample_real[high16]-sort_sample_real[low16])/2
            mean_imag = sort_sample_imag[mid]
            mean_imag_err = (sort_sample_imag[high16]-sort_sample_imag[low16])/2

            ipx_iz_ritd_real = [zlist[iz], zlist[iz]*latsp, mean_real, mean_real_err]
            ipx_iz_ritd_imag = [zlist[iz], zlist[iz]*latsp, mean_imag, mean_imag_err]
            ipx_ritd_real += [ipx_iz_ritd_real]
            ipx_ritd_imag += [ipx_iz_ritd_imag]
            sample_real_save = [[sample_real[i], sample_real[i]] for i in range(0, len(sample_real))]
            np.savetxt(samplefile+'.real', sample_real_save)
            if 'real' in up_sample:
                sample_imag_save = [[sample_imag[i], sample_imag[i]] for i in range(0, len(sample_imag))]
                np.savetxt(samplefile+'.imag', sample_imag_save)

        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print('\n Saving in:',ritd_out_name)
        ritd_out_name_real = ritd_out_name + '.real'
        np.savetxt(ritd_out_name_real,ipx_ritd_real)
        if 'real' in up_sample:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ipx_ritd_imag)

def bootsbm_to_rITD(Ns,latsp,up_sample,up_col_sample, down_sample,down_col_sample, \
                zlist, pxlist, outfolder, describe, qxyz):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    # data collection
    bm_samples_data = bootsbm_collect(up_sample,up_col_sample,zlist,pxlist,qxyz)
    bm_samples_norm = bootsbm_collect(down_sample,down_col_sample,zlist,pxlist,qxyz)

    rITD_samp_lenMax = 0
    for isamp in bm_samples_data:
        for izsamp in isamp:
            if len(izsamp) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(izsamp)
    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)
        
    # reduced bare matrix elements
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_bootssamples'
    
    ritd_real = []
    ritd_imag = []
    for ipx in range(0, len(pxlist)):
        
        ipx_ritd_real = []
        ipx_ritd_imag = []
        
        for iz in range(0, len(zlist)):

            p0_cfg = len(bm_samples_norm[0][iz])
            px_cfg = len(bm_samples_data[ipx][iz])

            print('z=',zlist[iz],'p0 #samples:', p0_cfg, 'px', pxlist[ipx], 'nboots:', px_cfg)
            

            ipx_iz_ritd_real = []
            ipx_iz_ritd_imag = []
            sample_real = []
            sample_imag = []

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, rITD_samp_lenMax):
                
                isample_up = bm_samples_data[ipx][iz][isamp]/bm_samples_data[ipx][0][isamp]
                isample_down = bm_samples_norm[0][iz][isamp]/bm_samples_norm[0][0][isamp]
                isample = isample_up/isample_down.real
                sample_real += [isample.real]
                sample_imag += [isample.imag]
            
            sort_sample_real = sorted(sample_real)
            sort_sample_imag = sorted(sample_imag)
            mean_real = sort_sample_real[mid]
            mean_real_err = (sort_sample_real[high16]-sort_sample_real[low16])/2
            mean_imag = sort_sample_imag[mid]
            mean_imag_err = (sort_sample_imag[high16]-sort_sample_imag[low16])/2

            ipx_iz_ritd_real = [zlist[iz], zlist[iz]*latsp, zlist[iz] * 2*np.pi/Ns*pxlist[ipx], mean_real, mean_real_err]
            ipx_iz_ritd_imag = [zlist[iz], zlist[iz]*latsp, zlist[iz] * 2*np.pi/Ns*pxlist[ipx], mean_imag, mean_imag_err]
            ipx_ritd_real += [ipx_iz_ritd_real]
            ipx_ritd_imag += [ipx_iz_ritd_imag]
            np.savetxt(samplefile+'.real', sample_real)
            if 'real' in up_sample:
                np.savetxt(samplefile+'.imag', sample_imag)
        ritd_real += [ipx_ritd_real]
        ritd_imag += [ipx_ritd_imag]

        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print('\n Saving in:',ritd_out_name)
        ritd_out_name_real = ritd_out_name + '.real'
        np.savetxt(ritd_out_name_real,ipx_ritd_real)
        if 'real' in up_sample:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ipx_ritd_imag)
    
    fmGeV = 5.0676896
    dP = 2*np.pi/(Ns*latsp*fmGeV)*pxlist[ipx]
    for iz in range(0, len(zlist)):
        iz_ritd_real = [[ritd_real[ipx][iz][0], pxlist[ipx]*dP] + ritd_real[ipx][iz][2:] for ipx in range(0, len(ritd_real))]
        iz_ritd_imag = [[ritd_imag[ipx][iz][0], pxlist[ipx]*dP] + ritd_imag[ipx][iz][2:] for ipx in range(0, len(ritd_imag))]
        ritd_out_name = ritd_out_format.replace('*',str(pxlist[0])+'-'+str(pxlist[-1]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[iz]))
        ritd_out_name_real = ritd_out_name + '.real'
        #np.savetxt(ritd_out_name_real,iz_ritd_real)
        #if 'real' in up_sample:
        #    ritd_out_name_imag = ritd_out_name + '.imag'
        #    np.savetxt(ritd_out_name_imag,iz_ritd_imag)

            
if __name__ == "__main__":

    inputfile = open(sys.argv[2],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[2])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')
    print('Will subtract P0.')

    # read parameters
    qxyz = []
    lat = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'qxyz' in line[0]:
            qxyz = [int(line[1]), int(line[2]), int(line[3])]
            describe = describe.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            print('qxyz:',qxyz)

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        if 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)

        if 'upsample' in line[0]:
            up_sample = line[1] #numerator of the RG invariant ratio
            up_col_sample = int(line[2])

        if 'downsample' in line[0]:
            down_sample = line[1] #denominator of the RG invariant ratio
            down_col_sample = int(line[2])

    if sys.argv[1] == 'rbm':
        bootsbm_to_rbm(Ns,latsp,up_sample,up_col_sample,down_sample,down_col_sample,zlist,pxlist,outfolder,describe,qxyz)

    elif sys.argv[1] == 'ritd':
        bootsbm_to_rITD(Ns,latsp,up_sample,up_col_sample,down_sample,down_col_sample,zlist,pxlist,outfolder,describe,qxyz)