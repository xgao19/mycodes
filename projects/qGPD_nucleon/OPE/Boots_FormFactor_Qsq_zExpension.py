#!/usr/bin/env python3
import numpy as np
from utils.tools import *
from scipy.optimize import least_squares

def ff_dipole(qsq, A, M):
    return A / (1+qsq/M**2)**2

def ff_z(qsq, tcut, topt, ak):
    t = -qsq
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-topt))/(np.sqrt(tcut-t)+np.sqrt(tcut-topt)) 

    f = 0
    for k in range(0, len(ak)):
        f += ak[k] * np.power(z, k)

    return f

def ff_samples_collection(format, zminlist, zmaxlist):

    samples_read = []
    for i, zmin in enumerate(zminlist):
        for j, zmax in enumerate(zmaxlist):
            if zmax - zmin <= 1:
                continue
            samples_read += [np.loadtxt(format.replace('zmin','zmin'+str(zmin)).replace('zmax','zmax'+str(zmax)))]

    samples_ave = []
    samples_std = []
    for i in range(0, len(samples_read[0])):
        i_samples = [samples_read[j][i] for j in range(0, len(samples_read))]
        samples_ave += [np.average(i_samples, axis=0)]
        samples_std += [np.std(i_samples, axis=0)]

    #print(np.average(samples_std, axis=1))
    return samples_ave, np.average(samples_std, axis=1)

if __name__ == "__main__":

    if sys.argv[1] == 'zr':

        fmGeV = 5.0676896

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        bandfolder = resultfolder + '/bands/'
        mkdir(bandfolder)
        datafolder = resultfolder + '/data/'
        mkdir(datafolder)
        print('\nReading parameters:')

        qsq_list = []
        sample_format_list = []
        zminlist = []
        zmaxlist = []
        for index in range(1, len(commands)):

            line = commands[index].split()
            if 'mass' == line[0]:
                mass = float(line[1])
                describe = describe.replace('mass', 'mass'+str(mass)+'GeV')
                print('  Hadron mass (GeV):', mass)

            elif 'massPi' == line[0]:
                massPi = float(line[1])
                print('  Pion mass (GeV):', massPi)

            elif 'latsp' in line[0]:
                latsp = float(line[1])
                Ls = int(line[2])
                Lt = int(line[3])
                print('  Lattice spacing (fm):', latsp, 'size:',Ls,'x',Lt)

            elif 'QsqMax' in line[0]:
                Qsqmaxlist = np.array([float(line[i]) for i in range(1, len(line))])
                print('  Qsq max (GeV^2):', Qsqmaxlist)

            elif 'ak' == line[0]:
                kmin = int(line[1])
                kmax = int(line[2])
                print('  kmin:', kmin,', kmax:', kmax)
                describe = describe.replace('kmax', 'k'+str(kmin)+'-'+str(kmax))
                poptstart = -np.ones(kmax-kmin+1)

            elif 'Nmoms' == line[0]:
                Nmomslist = [int(line[i]) for i in range(1, len(line))]
                print('  Order of moments:', Nmomslist)

            elif 'zmin' == line[0]:
                zminlist = [int(line[i]) for i in range(1, len(line))]
                print('  zmin:', zminlist)

            elif 'zmax' == line[0]:
                zmaxlist = [int(line[i]) for i in range(1, len(line))]
                print('  zmax:', zmaxlist)

            elif 'type' == line[0]:
                typelist = [line[i] for i in range(1, len(line))]
                print('  GPD type:', typelist)

            elif 'qsq' == line[0]:
                qsq_list += [float(line[1])]
                #sample_format = line[2].replace('zmin','zmin'+str(zmin)).replace('zmax','zmax'+str(zmax))
                sample_format_list += [line[2]]
                print('  -t (qsq):', round(float(line[1]),2), line[2].split('/')[-1])
                
        print('Job describe:', describe,'\n')
        curve_qsq = np.arange(0, 5, 0.1)

        for iG, GPDtype in enumerate(typelist):
        
            iG_describe = describe.replace('type', GPDtype)
            if 'u-d' in GPDtype:
                tcut = 4 * massPi**2
            elif 'u+d' in GPDtype:
                tcut = 9 * massPi**2
            print('\n#################################################################')
            print('GPD type:', GPDtype, ', tcut =', round(tcut,2))

            iG_samples = []
            iG_Errs = []
            iG_Mean = []
            iG_MeanErr = []
            for iq, qsq in enumerate(qsq_list):
                #iq_samples = np.loadtxt(sample_format_list[iq].replace('type', GPDtype))
                iq_samples_aveZ, iq_samples_stdZ = ff_samples_collection(sample_format_list[iq].replace('type', GPDtype), zminlist, zmaxlist)
                iq_samples = [iq_samples_aveZ[m] * (-1)**m for m in Nmomslist] # FIXME: flipped sign of odd moments
                iG_samples += [iq_samples]

                iq_Err = []
                iq_Mean = []
                iq_MeanErr = [qsq]
                for m in range(0, len(iq_samples)):
                    im_samples = sorted(iq_samples[m])
                    low, up = int(0.16*len(im_samples)), int(0.84*len(im_samples))
                    iq_Err += [(im_samples[up]-im_samples[low])/2+iq_samples_stdZ[m]]
                    iq_Mean += [(im_samples[up]+im_samples[low])/2]
                    iq_MeanErr += [(im_samples[up]+im_samples[low])/2, (im_samples[up]-im_samples[low])/2,iq_samples_stdZ[m],(im_samples[up]-im_samples[low])/2+iq_samples_stdZ[m]]
                iG_Errs += [iq_Err]
                iG_Mean += [iq_Mean]
                iG_MeanErr += [iq_MeanErr]
            print('Collected samples:', np.shape(iG_samples), 'Errors:', np.shape(iG_Errs))
            savename = datafolder + iG_describe.replace('#','n') + '.txt'
            np.savetxt(savename, iG_MeanErr, fmt='%.6e')

            iG_popt = []
            iG_curve = []
            for imax, qsqmax in enumerate(Qsqmaxlist):
                imax_describe = iG_describe.replace('QsqMax', 'QsqMax'+str(Qsqmaxlist[0]))
                imax_samples = []
                imax_Errs = []
                imax_Mean = []
                imax_qsq = []
                for iq, qsq in enumerate(qsq_list):
                    if qsq <= qsqmax:
                        imax_samples += [iG_samples[iq]]
                        imax_Errs += [iG_Errs[iq]]
                        imax_Mean += [iG_Mean[iq]]
                        imax_qsq += [qsq]
                #print('Qsq:', imax_qsq,'Collected samples:', np.shape(imax_samples), 'Values:', imax_Mean)
                topt = tcut * (1 - np.sqrt(1 + max(imax_qsq)/tcut))
                print('Qsq:', np.shape(imax_qsq), ', topt:', round(topt,2),'Collected samples:', np.shape(imax_samples), 'Values:', np.shape(imax_Mean))

                imax_popt = []
                imax_curve = []
                for imom in range(0, len(Nmomslist)):
                    imom_describe = imax_describe.replace('#', str(Nmomslist[imom]))
                    imom_popt = []
                    imom_curve = []
                    for isamp in range(0, len(iG_samples[0][0])):
                        #ff_z(qsq, tcut, topt, ak)
                        def ff_res(popt):
                            res = []
                            for iq, qsq in enumerate(imax_qsq):
                                res += [(ff_z(qsq, tcut, topt, popt)-imax_samples[iq][imom][isamp])/imax_Errs[iq][imom]]
                            for k, ak in enumerate(popt):
                                res += [(ak/popt[0])/5]
                            return res
                        res = least_squares(ff_res, poptstart, method='lm')
                        #print(len(imax_qsq), len(res.fun), res.fun)
                        chisq = sum(np.square(res.fun[:len(imax_qsq)])) / (len(imax_qsq)-len(poptstart))
                        imom_popt += [[ff_z(0, tcut, topt, res.x)] + list(res.x) + [chisq]]
                        imom_curve += [ff_z(curve_qsq, tcut, topt, res.x)]
                    imax_popt += [imom_popt]
                    imax_curve += [imom_curve]
                iG_popt += [imax_popt]
                iG_curve += [imax_curve]
            print('  Collected popt samples:', np.shape(iG_popt), 'curves: ', np.shape(iG_curve))

            iG_popt_MeanErr = []
            for imom in range(0, len(Nmomslist)):

                imom_describe = iG_describe.replace('#', str(Nmomslist[imom]))

                imom_popt_MeanErr = []
                for imax, qsqmax in enumerate(Qsqmaxlist):

                    imax_describe = imom_describe.replace('QsqMax', 'QsqMax'+str(qsqmax))

                    imax_popt_MeanErr = [0, qsqmax]
                    imax_popt_samples = []
                    for ip in range(0, len(iG_popt[imax][imom][isamp])):
                        samples = [iG_popt[imax][imom][isamp][ip] for isamp in range(0, len(iG_popt[imax][imom]))]
                        samples_sorted = sorted(samples)
                        imax_popt_MeanErr += [(samples_sorted[up]+samples_sorted[low])/2, (samples_sorted[up]-samples_sorted[low])/2]
                        imax_popt_samples += [samples]
                    imom_popt_MeanErr += [imax_popt_MeanErr]
                    #print(imom_popt_MeanErr)
                    savename = samplefolder + imax_describe + '_samples.txt'
                    np.savetxt(savename, imax_popt_samples, fmt='%.6e')

                    imax_curve_0 = []
                    imax_curve_1 = []
                    imax_curve_2 = []
                    for iq, qsq in enumerate(curve_qsq):
                        samples = [iG_curve[imax][imom][isamp][iq] for isamp in range(0, len(iG_curve[imax][imom]))]
                        samples_sorted = sorted(samples)
                        imax_curve_0 += [[qsq, samples_sorted[low]]]
                        imax_curve_1 += [[qsq, (samples_sorted[up]+samples_sorted[low])/2]]
                        imax_curve_2 += [[qsq, samples_sorted[up]]]
                    savename = bandfolder + imax_describe + '_band0.txt'
                    np.savetxt(savename, imax_curve_0, fmt='%.6e')
                    savename = bandfolder + imax_describe + '_band1.txt'
                    np.savetxt(savename, imax_curve_1, fmt='%.6e')
                    savename = bandfolder + imax_describe + '_band2.txt'
                    np.savetxt(savename, imax_curve_2, fmt='%.6e')

                savename = resultfolder + imom_describe + '.txt'
                np.savetxt(savename, imom_popt_MeanErr, fmt='%.6e')
