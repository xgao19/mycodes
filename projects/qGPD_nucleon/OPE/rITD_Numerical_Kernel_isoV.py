#!/usr/bin/env python3
import numpy as np

#####################################################
################    NNLO kernel    ##################
#####################################################

def CnNNLO(z, n, mu, alps):

    if n == 0:
        return 1. + alps*(0.456712 + 0.802918*alps) + \
            alps*np.log(mu*mu*z*z)*(0.31831  + 0.738604*alps + 0.164647*alps*np.log(mu*mu*z*z))
    
    elif n == 2:
        return 1. + alps*(-0.865982 + 1.89622*alps) + \
            alps*np.log(mu*mu*z*z)*(0.760407 - 0.661031*alps + 0.56141*alps*np.log(mu*mu*z*z))

    elif n == 4:
        return 1. + alps*(-1.69317 + 3.93486*alps) + \
            alps*np.log(mu*mu*z*z)*(0.962003 - 2.09953*alps + 0.807217*alps*np.log(mu*mu*z*z))

    elif n == 6:
        return 1. + alps*(-2.33055 + 6.12108*alps) + \
            alps*np.log(mu*mu*z*z)*(1.09665 - 3.40088*alps + 0.994034*alps*np.log(mu*mu*z*z))

    elif n == 8:
        return 1. + alps*(-2.85812 + 8.32132*alps) + \
            alps*np.log(mu*mu*z*z)*(1.19829  - 4.58506*alps + 1.14706*alps*np.log(mu*mu*z*z))

    elif n == 10:
        return 1. + alps*(-3.31224 + 10.4935*alps) + \
            alps*np.log(mu*mu*z*z)*(1.28007 - 5.6741*alps + 1.27768*alps*np.log(mu*mu*z*z))

    elif n == 1:
        return 1. + alps*(-0.316247 + 1.07596*alps) + \
            alps*np.log(mu*mu*z*z)*(0.601252 + 0.0954987*alps + 0.39606*alps*np.log(mu*mu*z*z))

    elif n == 3:
        return 1. + alps*(-1.31192 + 2.87993*alps) + \
            alps*np.log(mu*mu*z*z)*(0.873584 - 1.39762*alps + 0.694403*alps*np.log(mu*mu*z*z))

    elif n == 5:
        return 1. + alps*(-2.02896 + 5.02138*alps) + \
            alps*np.log(mu*mu*z*z)*(1.03476 - 2.76639*alps + 0.90591*alps*np.log(mu*mu*z*z))

    elif n == 7:
        return 1. + alps*(-2.60525 + 7.22297*alps) + \
            alps*np.log(mu*mu*z*z)*(1.15055 - 4.00612*alps + 1.07389*alps*np.log(mu*mu*z*z))

    elif n == 9:
        return 1. + alps*(-3.09286 + 9.41217*alps) + \
            alps*np.log(mu*mu*z*z)*(1.24116 - 5.14029*alps + 1.2147*alps*np.log(mu*mu*z*z))

    else:
        return 0