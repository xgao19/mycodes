#!/usr/bin/env python3
import numpy as np
from tools import *
from scipy.optimize import least_squares
from scipy.misc import derivative
from scipy.optimize import curve_fit

def array2D_exit(array1D, array2D):
    for i in range(0, len(array2D)):
        if (array1D==array2D[i]).all() == True:
            return True
    return False

def ff_z(qsq, tcut, topt, ak):
    t = -qsq
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-topt))/(np.sqrt(tcut-t)+np.sqrt(tcut-topt)) 
    a0 = 1
    z0 = (np.sqrt(tcut)-np.sqrt(tcut-topt))/(np.sqrt(tcut)+np.sqrt(tcut-topt))
    for k in range(0, len(ak)):
        a0 -= ak[k] * np.power(z0, k+1)

    f = a0
    for k in range(0, len(ak)):
        f += ak[k] * np.power(z, k+1)

    return f

def ff_zP(qsq, tcut, topt, ak):

    a1 = 0
    for k in range(1, len(ak)):
        a1 += (k+1) * ak[k] * np.power(-1, k+1)
    ak[0] = a1

    t = -qsq
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-topt))/(np.sqrt(tcut-t)+np.sqrt(tcut-topt)) 
    a0 = 1
    z0 = (np.sqrt(tcut)-np.sqrt(tcut-topt))/(np.sqrt(tcut)+np.sqrt(tcut-topt))
    for k in range(0, len(ak)):
        a0 -= ak[k] * np.power(z0, k+1)

    f = a0
    for k in range(0, len(ak)):
        f += ak[k] * np.power(z, k+1)

    return f

def Ep(a,Lt,Ls,mass,px,qx,qy,qz):
    px = (px+qx)*np.pi/(Ls/2)/a/fmGeV
    py = qy*np.pi/(Ls/2)/a/fmGeV
    pz = qz*np.pi/(Ls/2)/a/fmGeV
    return np.sqrt(mass**2+px**2+py**2+pz**2)

def Eff_radius(FF, Qsq):
    #print(Qsq,FF,6*(1/FF-1)/Qsq,6*(1/FF-1)/Qsq/fmGeV/fmGeV)
    return 6*(1/FF-1)/Qsq


if __name__ == "__main__":

    if sys.argv[1] == 'rbm':
        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        pxlist = []
        qxyzlist = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'mass' in line[0]:
                mass = float(line[1])
                #describe = describe.replace('mass', 'mass'+str(mass)+'GeV')
                print('  Hadron mass (GeV):', mass)

            elif 'px' == line[0]:
                pxlist += [i for i in range(int(line[1]), int(line[2])+1)]
                describe = describe.replace('*', str(pxlist[0])+'-'+str(pxlist[-1]))
                print('  px: ', pxlist)
            
            elif 'tmin' == line[0]:
                tminlist = [i for i in range(int(line[1]), int(line[2])+1)]
                describe = describe.replace('tmin', 'tmin'+str(tminlist[0])+'-'+str(tminlist[-1]))
                print('  tmin: ', tminlist)

            elif 'qxyz' == line[0]:
                qxyz = [int(line[1]), int(line[2]), int(line[3])]
                qxyzlist += [qxyz]
                print('  qxyz: ', qxyz)

            elif 'sampleup' == line[0]:
                data_up_format = line[1]
                col_up = int(line[2])
                print('Data on top:', data_up_format.split('/')[-1])

            elif 'sampledown' == line[0]:
                data_down_format = line[1]
                col_down = int(line[2])
                print('Data on bottom:', data_down_format.split('/')[-1])

        npick_file = data_down_format.replace('*', str(pxlist[0]))
        #npick_file = npick_file.replace('tmin', 'tmin'+str(tminlist[0]))
        npick_data = np.loadtxt(npick_file)
        npick = len(npick_data)
        high16 = int(npick*0.84)
        low16 = int(npick*0.16)
        mid = int(npick*0.5)
        print('\nJob describe:', describe, '<<>> npick:', npick, '\n')

        fmGeV = 5.0676896
        data_rbm_collection = []
        data_rbm_samples = []
        data_rbminv_collection = []
        data_rbminv_samples = []
        data_rbmMeff_collection = []
        data_rbmMeff_samples = []
        data_rbmMradius_collection = []
        data_rbmMradius_samples = []
        for ipx in range(0, len(pxlist)):
            
            ipx_data_up_format = data_up_format.replace('*', str(pxlist[ipx]))
            ipx_data_down_format = data_down_format.replace('*', str(pxlist[ipx]))
            ipx_describe = describe.replace(str(pxlist[0])+'-'+str(pxlist[-1]), str(pxlist[ipx]))

            for iq in range(0, len(qxyzlist)):

                iq_data_up_format = ipx_data_up_format.replace('qxqyqz', 'qx'+str(qxyzlist[iq][0])+'qy'+str(qxyzlist[iq][1])+'qz'+str(qxyzlist[iq][2]))
                iq_describe = ipx_describe.replace('qxqyqz', 'qx'+str(qxyzlist[iq][0])+'qy'+str(qxyzlist[iq][1])+'qz'+str(qxyzlist[iq][2]))
                iq_rbm_collection = np.zeros((npick,len(tminlist)))
                iq_rbminv_collection = np.zeros((npick,len(tminlist)))
                iq_rbmMeff_collection = np.zeros((npick,len(tminlist)))
                iq_rbmMradius_collection = np.zeros((npick,len(tminlist)))
                
                for it in range(0, len(tminlist)):

                    #if 'nst3' not in iq_data_up_format:
                    #    it_data_up_format = iq_data_up_format.replace('tmin', 'tmin'+str(tminlist[it]))
                    #else:
                    #    it_data_up_format = iq_data_up_format
                    #it_data_up_format = iq_data_up_format.replace('tmin', 'tmin'+str(tminlist[it]))
                    #if 'qy2' in it_data_up_format or 'qz2' in it_data_up_format:
                    #if 'qy2qz2' in it_data_up_format or 'qy1qz2' in it_data_up_format:
                    #if pxlist[ipx] > 2:
                    #    it_data_up_format = it_data_up_format.replace('4ts','3ts')
                    #        print(it_data_up_format)
                    it_data_up_format = iq_data_up_format
                    it_data_up = np.loadtxt(it_data_up_format)
                    print(it_data_up_format,it_data_up)
                    qsq = it_data_up[0][0]
                    it_data_up = [it_data_up[i][col_up] for i in range(0, len(it_data_up))]
                    #print(it_data_up)

                    #if 'nst3' not in ipx_data_down_format:
                    #    it_data_down_format = ipx_data_down_format.replace('tmin', 'tmin'+str(tminlist[it]))
                    #else:
                    #    it_data_down_format = ipx_data_down_format
                    #it_data_down_format = ipx_data_down_format.replace('tmin', 'tmin'+str(tminlist[it]))
                    #if 'qy2' in it_data_up_format or 'qz2' in it_data_up_format:
                    #if 'qy2qz2' in it_data_up_format or 'qy1qz2' in it_data_up_format:
                    #if pxlist[ipx] > 2:
                    #    it_data_down_format = it_data_down_format.replace('4ts','3ts')
                    it_data_down_format = ipx_data_down_format
                    it_data_down = np.loadtxt(it_data_down_format)
                    #print(it_data_down_format)
                    #if 'px0py0pz0' in it_data_down_format:
                    #    it_data_down = it_data_down/1.02
                    it_data_down = [it_data_down[i][col_down] for i in range(0, len(it_data_down))]
                    #print(np.shape(it_data_up),np.shape(it_data_down))
                    it_data_rbm = [it_data_up[i]/it_data_down[i] for i in range(0, len(it_data_down))]

                    for ipick in range(0, npick):
                        iq_rbm_collection[ipick][it] = it_data_rbm[ipick]
                        iq_rbminv_collection[ipick][it] = 1/it_data_rbm[ipick]
                        iq_rbmMeff_collection[ipick][it] = 1/np.sqrt((1/it_data_rbm[ipick]-1)/qsq)
                        iq_rbmMradius_collection[ipick][it] = (1/it_data_rbm[ipick]-1)*6/qsq/fmGeV/fmGeV

                # compute reduced matrix elements
                iq_rbm_sample = [np.average(iq_rbm_collection[ipick]) for ipick in range(0, npick)]
                data_rbm_samples += [iq_rbm_sample]
                iq_rbm_sample = np.sort(iq_rbm_sample)
                #iq_rbm_ave = (iq_rbm_sample[high16] + iq_rbm_sample[low16])/2
                #iq_rbm_std = (iq_rbm_sample[high16] - iq_rbm_sample[low16])/2
                iq_rbm_ave = iq_rbm_sample[mid]
                iq_rbm_std = max([iq_rbm_sample[high16]-iq_rbm_sample[mid], iq_rbm_sample[mid]-iq_rbm_sample[low16]])
                #print(iq_rbm_std,iq_rbm_sample[high16]-iq_rbm_sample[mid], iq_rbm_sample[mid]-iq_rbm_sample[low16])
                iq_rbm_sys = np.average([np.std(iq_rbm_collection[ipick]) for ipick in range(0, npick)])
                iq_rbm = [qsq, iq_rbm_ave, iq_rbm_std, iq_rbm_sys, np.sqrt(iq_rbm_std*iq_rbm_std+iq_rbm_sys*iq_rbm_sys)]
                np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
                print('px:',pxlist[ipx], 'qxyz:',qxyzlist[iq], '>> Qsq and rbm:',np.array(iq_rbm))
                iq_rbm = [pxlist[ipx]] + qxyzlist[iq] + iq_rbm
                data_rbm_collection += [iq_rbm]

                # inverse form factors
                iq_rbminv_sample = [np.average(iq_rbminv_collection[ipick]) for ipick in range(0, npick)]
                data_rbminv_samples += [iq_rbminv_sample]
                iq_rbminv_sample = np.sort(iq_rbminv_sample)
                #iq_rbminv_ave = (iq_rbminv_sample[high16] + iq_rbminv_sample[low16])/2
                #iq_rbminv_std = (iq_rbminv_sample[high16] - iq_rbminv_sample[low16])/2
                iq_rbminv_ave = iq_rbminv_sample[mid]
                iq_rbminv_std = max([iq_rbminv_sample[high16]-iq_rbminv_sample[mid], iq_rbminv_sample[mid]-iq_rbminv_sample[low16]])
                iq_rbminv_sys = np.average([np.std(iq_rbminv_collection[ipick]) for ipick in range(0, npick)])
                iq_rbminv = [qsq, iq_rbminv_ave, iq_rbminv_std, iq_rbminv_sys, np.sqrt(iq_rbminv_std*iq_rbminv_std+iq_rbminv_sys*iq_rbminv_sys)]
                np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
                #print('px:',pxlist[ipx], 'qxyz:',qxyzlist[iq], '>> Qsq and rbm:',np.array(iq_rbminv))
                iq_rbminv = [pxlist[ipx]] + qxyzlist[iq] + iq_rbminv
                data_rbminv_collection += [iq_rbminv]

                # Monopole effective mass
                iq_rbmMeff_sample = [np.average(iq_rbmMeff_collection[ipick]) for ipick in range(0, npick)]
                data_rbmMeff_samples += [iq_rbmMeff_sample]
                iq_rbmMeff_sample = np.sort(iq_rbmMeff_sample)
                #iq_rbmMeff_ave = (iq_rbmMeff_sample[high16] + iq_rbmMeff_sample[low16])/2
                #iq_rbmMeff_std = (iq_rbmMeff_sample[high16] - iq_rbmMeff_sample[low16])/2
                iq_rbmMeff_ave = iq_rbmMeff_sample[mid]
                iq_rbmMeff_std = max([iq_rbmMeff_sample[high16]-iq_rbmMeff_sample[mid], iq_rbmMeff_sample[mid]-iq_rbmMeff_sample[low16]])
                iq_rbmMeff_sys = np.average([np.std(iq_rbmMeff_collection[ipick]) for ipick in range(0, npick)])
                iq_rbmMeff = [qsq/(mass*mass), iq_rbmMeff_ave, iq_rbmMeff_std, iq_rbmMeff_sys, np.sqrt(iq_rbmMeff_std*iq_rbmMeff_std+iq_rbmMeff_sys*iq_rbmMeff_sys)]
                np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
                #print(iq_rbmMeff_sample[high16]-iq_rbmMeff_sample[mid], iq_rbmMeff_sample[mid]-iq_rbmMeff_sample[low16])
                print('px:',pxlist[ipx], 'qxyz:',qxyzlist[iq], '>> Qsq and Meff:',np.array(iq_rbmMeff))
                iq_rbmMeff = [pxlist[ipx]] + qxyzlist[iq] + iq_rbmMeff
                data_rbmMeff_collection += [iq_rbmMeff]           

                # Monopole effective radius squared
                iq_rbmMradius_sample = [np.average(iq_rbmMradius_collection[ipick]) for ipick in range(0, npick)]
                data_rbmMradius_samples += [iq_rbmMradius_sample]
                iq_rbmMradius_sample = np.sort(iq_rbmMradius_sample)
                #iq_rbmMradius_ave = (iq_rbmMradius_sample[high16] + iq_rbmMradius_sample[low16])/2
                #iq_rbmMradius_std = (iq_rbmMradius_sample[high16] - iq_rbmMradius_sample[low16])/2
                iq_rbmMradius_ave = iq_rbmMradius_sample[mid]
                iq_rbmMradius_std = max([iq_rbmMradius_sample[high16]-iq_rbmMradius_sample[mid], iq_rbmMradius_sample[mid]-iq_rbmMradius_sample[low16]])
                iq_rbmMradius_sys = np.average([np.std(iq_rbmMradius_collection[ipick]) for ipick in range(0, npick)])
                iq_rbmMradius = [qsq, iq_rbmMradius_ave, iq_rbmMradius_std, iq_rbmMradius_sys, np.sqrt(iq_rbmMradius_std*iq_rbmMradius_std+iq_rbmMradius_sys*iq_rbmMradius_sys)]
                np.set_printoptions(formatter={'float': '{: 0.4f}'.format})
                print('px:',pxlist[ipx], 'qxyz:',qxyzlist[iq], '>> Qsq and radius:',np.array(iq_rbmMradius),'\n')
                iq_rbmMradius = [pxlist[ipx]] + qxyzlist[iq] + iq_rbmMradius
                data_rbmMradius_collection += [iq_rbmMradius]     

        sample_name = samplefolder + describe + '_bootssamples.txt'
        np.savetxt(sample_name, data_rbm_samples, fmt='%.6e')
        save_name = resultfolder + '/' + describe + '.txt'
        np.savetxt(save_name, data_rbm_collection, fmt='%.6e')

        sample_name = samplefolder + describe.replace('rbm','rbminv') + '_bootssamples.txt'
        np.savetxt(sample_name, data_rbminv_samples, fmt='%.6e')
        save_name = resultfolder + '/' + describe.replace('rbm','rbminv') + '.txt'
        np.savetxt(save_name, data_rbminv_collection, fmt='%.6e')

        sample_name = samplefolder + describe.replace('rbm','rbmMeff') + '_bootssamples.txt'
        np.savetxt(sample_name, data_rbmMeff_samples, fmt='%.6e')
        save_name = resultfolder + '/' + describe.replace('rbm','rbmMeff') + '.txt'
        np.savetxt(save_name, data_rbmMeff_collection, fmt='%.6e')

        sample_name = samplefolder + describe.replace('rbm','rbmMradius') + '_bootssamples.txt'
        np.savetxt(sample_name, data_rbmMradius_samples, fmt='%.6e')
        save_name = resultfolder + '/' + describe.replace('rbm','rbmMradius') + '.txt'
        np.savetxt(save_name, data_rbmMradius_collection, fmt='%.6e')

        '''
        # local effective radius squared
        print(np.shape(data_rbminv_collection),np.shape(data_rbminv_samples))
        data_localRadius_samples = []
        data_localRadius_collection = []
        for iq in range(1, len(data_rbminv_collection)):
            iq_localRadius_sample = []
            d_qsq = (data_rbminv_collection[iq][4] - data_rbminv_collection[iq-1][4])*(mass*mass)
            #print(d_qsq)
            for isamp in range(0, len(data_rbminv_samples[0])):
                iq_localRadius_sample += [(data_rbminv_samples[iq][isamp]-data_rbminv_samples[iq-1][isamp])/d_qsq*6/fmGeV/fmGeV]
            iq_localRadius_sample = np.sort(iq_localRadius_sample)
            iq_localRadius_ave = iq_localRadius_sample[mid]
            iq_localRadius_std = max([iq_localRadius_sample[high16]-iq_localRadius_sample[mid], iq_localRadius_sample[mid]-iq_localRadius_sample[low16]])
            iq_localRadius_sys =0
            iq_localRadius =data_rbm_collection[iq][:5]+[iq_localRadius_ave, iq_localRadius_std, iq_localRadius_sys, np.sqrt(iq_localRadius_std*iq_localRadius_std+iq_localRadius_sys*iq_localRadius_sys)]
            print('Local effective radius:', iq_localRadius)
            data_localRadius_collection += [iq_localRadius]
        print(data_localRadius_collection)
        save_name = resultfolder + '/' + describe.replace('rbm','localRadius') + '.txt'
        np.savetxt(save_name, data_localRadius_collection, fmt='%.6e')
        '''


    elif sys.argv[1] == 'zr':

        fmGeV = 5.0676896

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        pxlist = []
        qxyzlist = []
        for index in range(1, len(commands)):

            line = commands[index].split()
            if 'mass' in line[0]:
                mass = float(line[1])
                describe = describe.replace('mass', 'mass'+str(mass)+'GeV')
                tcut = 4 * mass * mass
                print('  Hadron mass (GeV):', mass, ', tcut:', round(tcut,5))

            elif 'latsp' in line[0]:
                latsp = float(line[1])
                print('  Lattice spacing (fm):', latsp)

            elif 'ak' == line[0]:
                kmin = int(line[1])
                kmax = int(line[2])
                print('  kmin:', kmin,', kmax:', kmax)
                describe = describe.replace('kmax', 'k'+str(kmin)+'-'+str(kmax))

            elif 'Qsqmax' in line[0]:
                Qsqmaxlist = np.array([float(line[i]) for i in range(1, len(line))])
                topt = tcut * (1 - np.sqrt(1 + Qsqmaxlist/tcut))
                print('  Qsq max (GeV^2):', Qsqmaxlist,', topt:', [round(topt[i],5) for i in range(0, len(topt))])
                describe = describe.replace('Qsqmax', 'Qsqmax'+str(Qsqmaxlist[0]))

            elif 'px' == line[0]:
                ipxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                pxlist += [ipxlist]
                describe = describe.replace('*', str(min(min(pxlist)))+'-'+str(max(max(pxlist))))
                print('  px: ', ipxlist[0], '-' , ipxlist[-1])
            
            elif 'tmin' == line[0]:
                tminlist = [i for i in range(int(line[1]), int(line[2])+1)]
                #describe = describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1]))
                print('  tmin: ', tminlist)

            elif 'qxyz' == line[0]:
                qxyz = np.array([int(line[1]), int(line[2]), int(line[3])])
                qxyzlist += [qxyz]
                print('  qxyz: ', qxyz)

            elif 'meanerr' == line[0]:
                mean_format = line[1]
                col = [int(line[2]), int(line[3])]
                print('  Mean errors file:', mean_format.split('/')[-1])

            elif 'sample' == line[0]:
                sample_format = line[1]
                print('  Sample format:', sample_format.split('/')[-1])
        print('Job describe:', describe,'\n')

        popt_collection = []
        popt_samples = [[],[]]
        popt_x = np.array([0.01*i+0.000001 for i in range(0, 301)])
        popt_FFqsq_samples = []

        for iak in range(0, kmax):
            popt_samples += [[]]
        for it in range(0, len(tminlist)):
            it_mean_data = np.loadtxt(mean_format.replace('#', str(tminlist[it])+'-'+str(tminlist[it])))
            it_sample_data = np.loadtxt(sample_format.replace('#', str(tminlist[it])+'-'+str(tminlist[it])))
            for ip in range(0, len(pxlist)):
                ipxlist = pxlist[ip]
                for iQsq in range(0, len(Qsqmaxlist)):
                    iQsqmax = Qsqmaxlist[iQsq]
                    iQsqmax_topt = topt[iQsq]
                    ff_qsq = []
                    ff_samples = []
                    ff_err = []
                    for iq in range(0, len(it_mean_data)):
                        if round(it_mean_data[iq][0]) in ipxlist:
                            iqxyz = np.array([it_mean_data[iq][1],it_mean_data[iq][2],it_mean_data[iq][3]])
                            if array2D_exit(iqxyz, qxyzlist):
                                if it_mean_data[iq][col[0]] < iQsqmax:
                                    ff_qsq += [it_mean_data[iq][col[0]]]
                                    ff_err += [it_mean_data[iq][col[1]]]
                                    ff_samples += [it_sample_data[iq]]
                    ff_samples = np.transpose(ff_samples)
                    print('tmin:', tminlist[it], ', pxlist:', ipxlist[0], '-', ipxlist[-1], ', Qsqmax:', iQsqmax,'Data shape:',np.shape(ff_samples))
                    
                    def ff_res(qsq,ff,fferr,ak):
                        res = ff_z(qsq, tcut, iQsqmax_topt, ak) - ff
                        return res/fferr
                    
                    #def ff_poly(qsq, a1,a2,a3):
                    #    return 1 - 1/6*a1*qsq + a2*qsq*qsq + a3*qsq*qsq*qsq
                        
                    ak_samples = []
                    chisq_samples = []
                    aicc_samples = []
                    radius_samples = []
                    FFqsq_samples = []
                    for ik in range(kmin, kmax+1):
                        ak_start = np.zeros(ik)
                        ik_ak_samples = []
                        ik_chisq_samples = []
                        ik_aicc_samples = []
                        ik_radius_samples = [] # <r^2>(Q^2)
                        ik_FFqsq_samples = [] # FF(Q^2)
                        for isamp in range(0, len(ff_samples)):
                            def mini_ff_res(ak):
                                return ff_res(np.array(ff_qsq),np.array(ff_samples[isamp]),np.array(ff_err),ak)
                            isamp_res = least_squares(mini_ff_res, ak_start, method='lm')
                            isamp_chisq = sum(np.square(isamp_res.fun)) / (len(ff_qsq)-len(ak_start))
                            isamp_aicc = 2*len(ak_start)+isamp_chisq*(len(ff_qsq)-len(ak_start))+(2*len(ak_start)*len(ak_start) + 2*len(ak_start))/(len(ff_qsq) - len(ak_start) -1)
                            #isamp_aicc = 2*len(ak_start)+isamp_chisq*(len(ff_qsq)-len(ak_start))
                            isamp_ak = np.zeros(kmax)
                            for ix in range(0, len(isamp_res.x)):
                                isamp_ak[ix] = isamp_res.x[ix]
                            ik_ak_samples += [list(isamp_ak)]
                            ik_chisq_samples += [isamp_chisq]
                            ik_aicc_samples += [isamp_aicc]
                            def ff_radius(qsq):
                                return ff_z(qsq, tcut, iQsqmax_topt, isamp_res.x)
                            ik_radius_samples += [-6*derivative(ff_radius, 0, dx=1e-6)/fmGeV/fmGeV]
                            ik_FFqsq_samples += [list(ff_radius(popt_x))]
                        ak_samples += [ik_ak_samples]
                        chisq_samples += [ik_chisq_samples]
                        aicc_samples += [ik_aicc_samples]
                        radius_samples += [ik_radius_samples]
                        FFqsq_samples += [ik_FFqsq_samples]

                    
                    npick = len(ff_samples)
                    mid = int(npick*0.5)
                    high16 = int(npick*0.84)
                    low16 = int(npick*0.16)
                    aicc_min = 10**10
                    aicc_nk = 0
                    print('\nAICc:')
                    for ik in range(0, len(aicc_samples)):
                        ik_aicc_sort = np.sort(aicc_samples[ik])
                        print(ik+kmin,ik_aicc_sort[mid])
                        if ik_aicc_sort[mid] < aicc_min:
                            aicc_nk = ik
                            aicc_min = ik_aicc_sort[mid]
                            #print(aicc_min,ik,ik_aicc_sort[mid])
                    print('AICc selected Nk:',aicc_nk+kmin,'of',aicc_min,'\n')
                    ak_samples = ak_samples[aicc_nk]
                    chisq_samples = chisq_samples[aicc_nk]
                    radius_samples = radius_samples[aicc_nk]
                    FFqsq_samples = FFqsq_samples[aicc_nk]

                    radius_samples_sorted = np.sort(radius_samples)
                    #popt = [(radius_samples_sorted[high16]+radius_samples_sorted[low16])/2,(radius_samples_sorted[high16]-radius_samples_sorted[low16])/2,(radius_samples_sorted[high16]-radius_samples_sorted[low16])/2,np.sort(chisq_samples)[mid]]
                    popt = [radius_samples_sorted[mid],radius_samples_sorted[mid]-radius_samples_sorted[low16],radius_samples_sorted[high16]-radius_samples_sorted[mid]]
                    ak_samples = np.transpose(ak_samples)
                    for iak in range(0, len(ak_samples)):
                        #print(iak, ak_samples[iak])
                        popt_ak = [np.sort(ak_samples[iak])[mid],np.sort(ak_samples[iak])[mid]-np.sort(ak_samples[iak])[low16],np.sort(ak_samples[iak])[high16]-np.sort(ak_samples[iak])[mid]]
                        print('Popt ak', iak+1, ':', [round(popt_ak[i],4) for i in range(0, len(popt_ak))])
                        popt += popt_ak
                    popt_radius = [radius_samples_sorted[mid],radius_samples_sorted[mid]-radius_samples_sorted[low16],radius_samples_sorted[high16]-radius_samples_sorted[mid],np.sort(chisq_samples)[mid]]
                    popt += [np.sort(chisq_samples)[mid]]
                    aicc = 2*len(ak_start)+popt_radius[-1]*(len(ff_qsq)-len(ak_start))+(2*len(ak_start)*len(ak_start) + 2*len(ak_start))/(len(ff_qsq) - len(ak_start) -1)
                    #aicc = 2*len(ak_start)+popt_radius[-1]*(len(ff_qsq)-len(ak_start))
                    #print(np.sort(chisq_samples))
                    print('Popt radius:', [round(popt_radius[i],4) for i in range(0, len(popt_radius))])

                    popt_collection += [[tminlist[it], ipxlist[0], ipxlist[-1], iQsqmax]+popt]
                    popt_samples[0] += [radius_samples]
                    popt_samples[1] += [chisq_samples]
                    for iak in range(0, kmax):
                        popt_samples[iak+2] += [ak_samples[iak]]

                    popt_FFqsq_samples += [FFqsq_samples] # FF(Q^2)[i_condition][samples][Q^2]

        # Save the popt and errors
        print('\nPopt shape:',np.shape(popt_collection), 'Popt samples shape:',np.shape(popt_samples))
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '.txt'
        print('Saving >>',save_name.split('/')[-1])

        # Save the radius samples
        np.savetxt(save_name, popt_collection, fmt='%.6e')
        save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_radius.txt'
        print('Saving >>',save_name.split('/')[-1])
        np.savetxt(save_name, popt_samples[0], fmt='%.6e')

        # Save the chisq samples
        save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_chisq.txt'
        print('Saving >>',save_name.split('/')[-1])
        np.savetxt(save_name, popt_samples[1], fmt='%.6e')

        # Save the ak samples
        for iak in range(0, kmax):
            save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_ak' + str(iak+1) + '.txt'
            print('Saving >>',save_name.split('/')[-1])
            np.savetxt(save_name, popt_samples[iak+2], fmt='%.6e')

        # Save radius systematic errors
        samples = popt_samples[0]
        boots_ave = []
        boots_std = []
        for isamp in range(0, len(samples[0])):
            isamp_boots = [samples[i][isamp] for i in range(0, len(samples))]
            boots_ave += [np.average(isamp_boots)]
            boots_std += [np.std(isamp_boots)]
        boots_ave = np.sort(boots_ave)
        boots_std = np.sort(boots_std)
        sys_popt = [boots_ave[mid], boots_ave[mid]-boots_ave[low16],boots_ave[high16]-boots_ave[mid]]
        sys_popt += [boots_std[mid]]
        samples = popt_samples[1]
        boots_ave = []
        boots_std = []
        for isamp in range(0, len(samples[0])):
            isamp_boots = [samples[i][isamp] for i in range(0, len(samples))]
            boots_ave += [np.average(isamp_boots)]
            boots_std += [np.std(isamp_boots)]
        boots_ave = np.sort(boots_ave)
        boots_std = np.sort(boots_std)
        sys_popt += [boots_ave[mid]]
        print('\nRadius and Systematic errors',[round(sys_popt[i],4) for i in range(0, len(sys_popt)) ])
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_RadiusSys.txt'
        np.savetxt(save_name, sys_popt, fmt='%.6e')

        # save the fit results fx from bootstrap samples
        popt_fx_samples = popt_FFqsq_samples
        print('Collected fx:', np.shape(popt_fx_samples))
        popt_fx0 = []
        popt_fx1 = []
        popt_fx2 = []
        popt_invfx0 = []
        popt_invfx1 = []
        popt_invfx2 = []
        popt_fxMr0 = []
        popt_fxMr1 = []
        popt_fxMr2 = []
        for ix in range(0, len(popt_x)):
            # collect fx
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [popt_fx_samples[i][isamp][ix] for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_fx0 += [[popt_x[ix], boots_ave[low16]]]
            popt_fx1 += [[popt_x[ix], boots_ave[mid]]]
            popt_fx2 += [[popt_x[ix], boots_ave[high16]]]

            # collect inv_fx
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [1/popt_fx_samples[i][isamp][ix] for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_invfx0 += [[popt_x[ix], boots_ave[low16]]]
            popt_invfx1 += [[popt_x[ix], boots_ave[mid]]]
            popt_invfx2 += [[popt_x[ix], boots_ave[high16]]]

            # collect effective radius
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [Eff_radius(popt_fx_samples[i][isamp][ix], popt_x[ix])/fmGeV/fmGeV for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_fxMr0 += [[popt_x[ix]/(mass*mass), boots_ave[low16]]]
            popt_fxMr1 += [[popt_x[ix]/(mass*mass), boots_ave[mid]]]
            popt_fxMr2 += [[popt_x[ix]/(mass*mass), boots_ave[high16]]]
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx0.txt'
        np.savetxt(save_name, popt_fx0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx1.txt'
        np.savetxt(save_name, popt_fx1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx2.txt'
        np.savetxt(save_name, popt_fx2, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx0.txt'
        np.savetxt(save_name, popt_invfx0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx1.txt'
        np.savetxt(save_name, popt_invfx1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx2.txt'
        np.savetxt(save_name, popt_invfx2, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfxMr0.txt'
        np.savetxt(save_name, popt_fxMr0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfxMr1.txt'
        np.savetxt(save_name, popt_fxMr1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfxMr2.txt'
        np.savetxt(save_name, popt_fxMr2, fmt='%.6e')

    elif sys.argv[1] == 'zrP':

        fmGeV = 5.0676896

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        pxlist = []
        qxyzlist = []
        for index in range(1, len(commands)):

            line = commands[index].split()
            if 'mass' in line[0]:
                mass = float(line[1])
                describe = describe.replace('mass', 'mass'+str(mass)+'GeV')
                tcut = 4 * mass * mass
                print('  Hadron mass (GeV):', mass, ', tcut:', round(tcut,5))

            elif 'latsp' in line[0]:
                latsp = float(line[1])
                print('  Lattice spacing (fm):', latsp)

            elif 'ak' == line[0]:
                kmin = int(line[1])
                kmax = int(line[2])
                print('  kmin:', kmin,', kmax:', kmax)
                describe = describe.replace('kmax', 'k'+str(kmin)+'-'+str(kmax))

            elif 'Qsqmax' in line[0]:
                Qsqmaxlist = np.array([float(line[i]) for i in range(1, len(line))])
                topt = tcut * (1 - np.sqrt(1 + Qsqmaxlist/tcut))
                print('  Qsq max (GeV^2):', Qsqmaxlist,', topt:', [round(topt[i],5) for i in range(0, len(topt))])
                describe = describe.replace('Qsqmax', 'Qsqmax'+str(Qsqmaxlist[0]))

            elif 'px' == line[0]:
                ipxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                pxlist += [ipxlist]
                describe = describe.replace('*', str(min(min(pxlist)))+'-'+str(max(max(pxlist))))
                print('  px: ', ipxlist[0], '-' , ipxlist[-1])
            
            elif 'tmin' == line[0]:
                tminlist = [i for i in range(int(line[1]), int(line[2])+1)]
                #describe = describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1]))
                print('  tmin: ', tminlist)

            elif 'qxyz' == line[0]:
                qxyz = np.array([int(line[1]), int(line[2]), int(line[3])])
                qxyzlist += [qxyz]
                print('  qxyz: ', qxyz)

            elif 'meanerr' == line[0]:
                mean_format = line[1]
                col = [int(line[2]), int(line[3])]
                print('  Mean errors file:', mean_format.split('/')[-1])

            elif 'sample' == line[0]:
                sample_format = line[1]
                print('  Sample format:', sample_format.split('/')[-1])
        print('Job describe:', describe,'\n')

        popt_collection = []
        popt_samples = [[],[]]
        popt_x = np.array([0.01*i+0.000001 for i in range(0, 301)])
        popt_FFqsq_samples = []

        for iak in range(0, kmax):
            popt_samples += [[]]
        for it in range(0, len(tminlist)):
            it_mean_data = np.loadtxt(mean_format.replace('#', str(tminlist[it])+'-'+str(tminlist[it])))
            it_sample_data = np.loadtxt(sample_format.replace('#', str(tminlist[it])+'-'+str(tminlist[it])))
            for ip in range(0, len(pxlist)):
                ipxlist = pxlist[ip]
                for iQsq in range(0, len(Qsqmaxlist)):
                    iQsqmax = Qsqmaxlist[iQsq]
                    iQsqmax_topt = topt[iQsq]
                    ff_qsq = []
                    ff_samples = []
                    ff_err = []
                    for iq in range(0, len(it_mean_data)):
                        if round(it_mean_data[iq][0]) in ipxlist:
                            iqxyz = np.array([it_mean_data[iq][1],it_mean_data[iq][2],it_mean_data[iq][3]])
                            if array2D_exit(iqxyz, qxyzlist):
                                if it_mean_data[iq][col[0]] < iQsqmax:
                                    ff_qsq += [it_mean_data[iq][col[0]]]
                                    ff_err += [it_mean_data[iq][col[1]]]
                                    ff_samples += [it_sample_data[iq]]
                    ff_samples = np.transpose(ff_samples)
                    print('tmin:', tminlist[it], ', pxlist:', ipxlist[0], '-', ipxlist[-1], ', Qsqmax:', iQsqmax,'Data shape:',np.shape(ff_samples))
                    
                    def ff_res(qsq,ff,fferr,ak):
                        res = ff_zP(qsq, tcut, iQsqmax_topt, ak) - ff
                        ratio = res/fferr
                        #for i in range(0, len(ak)):
                        #    ratio = np.append(ratio, ak[i]*np.exp(-i))
                        return ratio
                    
                    #def ff_poly(qsq, a1,a2,a3):
                    #    return 1 - 1/6*a1*qsq + a2*qsq*qsq + a3*qsq*qsq*qsq
                        
                    ak_samples = []
                    chisq_samples = []
                    aicc_samples = []
                    radius_samples = []
                    FFqsq_samples = []
                    for ik in range(kmin, kmax+1):
                        #ak_start = np.zeros(ik)-0.1
                        ik_ak_samples = []
                        ik_chisq_samples = []
                        ik_aicc_samples = []
                        ik_radius_samples = [] # <r^2>(Q^2)
                        ik_FFqsq_samples = [] # FF(Q^2)
                        for isamp in range(0, len(ff_samples)):
                            def mini_ff_res(ak):
                                return ff_res(np.array(ff_qsq),np.array(ff_samples[isamp]),np.array(ff_err),ak)
                            ak_start = np.random.normal(0, np.exp(-(ik-1)), ik)
                            isamp_res = least_squares(mini_ff_res, ak_start, method='lm')
                            len_param = len(ak_start)-1
                            isamp_chisq = sum(np.square(isamp_res.fun)) / (len(ff_qsq)-len_param)
                            isamp_aicc = 2*len_param+isamp_chisq*(len(ff_qsq)-len_param)+(2*len_param*len_param + 2*len_param)/(len(ff_qsq) - len_param -1)
                            #isamp_aicc = 2*len(ak_start)+isamp_chisq*(len(ff_qsq)-len(ak_start))
                            isamp_ak = np.zeros(kmax)
                            for ix in range(0, len(isamp_res.x)):
                                isamp_ak[ix] = isamp_res.x[ix]
                            ik_ak_samples += [list(isamp_ak)]
                            ik_chisq_samples += [isamp_chisq]
                            ik_aicc_samples += [isamp_aicc]
                            def ff_radius(qsq):
                                return ff_zP(qsq, tcut, iQsqmax_topt, isamp_res.x)
                            ik_radius_samples += [-6*derivative(ff_radius, 0, dx=1e-6)/fmGeV/fmGeV]
                            ik_FFqsq_samples += [list(ff_radius(popt_x))]
                        ak_samples += [ik_ak_samples]
                        chisq_samples += [ik_chisq_samples]
                        aicc_samples += [ik_aicc_samples]
                        radius_samples += [ik_radius_samples]
                        FFqsq_samples += [ik_FFqsq_samples]

                    print('F(Qsq=0):', ff_radius(0))

                    
                    npick = len(ff_samples)
                    mid = int(npick*0.5)
                    high16 = int(npick*0.84)
                    low16 = int(npick*0.16)
                    aicc_min = 10**10
                    aicc_nk = 0
                    print('\nAICc:')
                    for ik in range(0, len(aicc_samples)):
                        ik_aicc_sort = np.sort(aicc_samples[ik])
                        print(ik+kmin,ik_aicc_sort[mid])
                        if ik_aicc_sort[mid] < aicc_min:
                            aicc_nk = ik
                            aicc_min = ik_aicc_sort[mid]
                            #print(aicc_min,ik,ik_aicc_sort[mid])
                    print('AICc selected Nk:',aicc_nk+kmin,'of',aicc_min,'\n')
                    ak_samples = ak_samples[aicc_nk]
                    chisq_samples = chisq_samples[aicc_nk]
                    radius_samples = radius_samples[aicc_nk]
                    FFqsq_samples = FFqsq_samples[aicc_nk]

                    radius_samples_sorted = np.sort(radius_samples)
                    #popt = [(radius_samples_sorted[high16]+radius_samples_sorted[low16])/2,(radius_samples_sorted[high16]-radius_samples_sorted[low16])/2,(radius_samples_sorted[high16]-radius_samples_sorted[low16])/2,np.sort(chisq_samples)[mid]]
                    popt = [radius_samples_sorted[mid],radius_samples_sorted[mid]-radius_samples_sorted[low16],radius_samples_sorted[high16]-radius_samples_sorted[mid]]
                    ak_samples = np.transpose(ak_samples)
                    for iak in range(0, len(ak_samples)):
                        #print(iak, ak_samples[iak])
                        popt_ak = [np.sort(ak_samples[iak])[mid],np.sort(ak_samples[iak])[mid]-np.sort(ak_samples[iak])[low16],np.sort(ak_samples[iak])[high16]-np.sort(ak_samples[iak])[mid]]
                        print('Popt ak', iak+1, ':', [round(popt_ak[i],4) for i in range(0, len(popt_ak))])
                        popt += popt_ak
                    popt_radius = [radius_samples_sorted[mid],radius_samples_sorted[mid]-radius_samples_sorted[low16],radius_samples_sorted[high16]-radius_samples_sorted[mid],np.sort(chisq_samples)[mid]]
                    popt += [np.sort(chisq_samples)[mid]]
                    aicc = 2*len_param+popt_radius[-1]*(len(ff_qsq)-len_param)+(2*len_param*len_param + 2*len_param)/(len(ff_qsq) - len_param -1)
                    #aicc = 2*len(ak_start)+popt_radius[-1]*(len(ff_qsq)-len(ak_start))
                    #print(np.sort(chisq_samples))
                    print('Popt radius:', [round(popt_radius[i],4) for i in range(0, len(popt_radius))])

                    popt_collection += [[tminlist[it], ipxlist[0], ipxlist[-1], iQsqmax]+popt]
                    popt_samples[0] += [radius_samples]
                    popt_samples[1] += [chisq_samples]
                    for iak in range(0, kmax):
                        popt_samples[iak+2] += [ak_samples[iak]]

                    popt_FFqsq_samples += [FFqsq_samples] # FF(Q^2)[i_condition][samples][Q^2]

        # Save the popt and errors
        print('\nPopt shape:',np.shape(popt_collection), 'Popt samples shape:',np.shape(popt_samples))
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '.txt'
        print('Saving >>',save_name.split('/')[-1])

        # Save the radius samples
        np.savetxt(save_name, popt_collection, fmt='%.6e')
        save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_radius.txt'
        print('Saving >>',save_name.split('/')[-1])
        np.savetxt(save_name, popt_samples[0], fmt='%.6e')

        # Save the chisq samples
        save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_chisq.txt'
        print('Saving >>',save_name.split('/')[-1])
        np.savetxt(save_name, popt_samples[1], fmt='%.6e')

        # Save the ak samples
        for iak in range(0, kmax):
            save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_ak' + str(iak+1) + '.txt'
            print('Saving >>',save_name.split('/')[-1])
            np.savetxt(save_name, popt_samples[iak+2], fmt='%.6e')

        # Save radius systematic errors
        samples = popt_samples[0]
        boots_ave = []
        boots_std = []
        for isamp in range(0, len(samples[0])):
            isamp_boots = [samples[i][isamp] for i in range(0, len(samples))]
            boots_ave += [np.average(isamp_boots)]
            boots_std += [np.std(isamp_boots)]
        boots_ave = np.sort(boots_ave)
        boots_std = np.sort(boots_std)
        sys_popt = [boots_ave[mid], boots_ave[mid]-boots_ave[low16],boots_ave[high16]-boots_ave[mid]]
        sys_popt += [boots_std[mid]]
        samples = popt_samples[1]
        boots_ave = []
        boots_std = []
        for isamp in range(0, len(samples[0])):
            isamp_boots = [samples[i][isamp] for i in range(0, len(samples))]
            boots_ave += [np.average(isamp_boots)]
            boots_std += [np.std(isamp_boots)]
        boots_ave = np.sort(boots_ave)
        boots_std = np.sort(boots_std)
        sys_popt += [boots_ave[mid]]
        print('\nRadius and Systematic errors',[round(sys_popt[i],4) for i in range(0, len(sys_popt)) ])
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_RadiusSys.txt'
        np.savetxt(save_name, sys_popt, fmt='%.6e')

        # save the fit results fx from bootstrap samples
        popt_fx_samples = popt_FFqsq_samples
        print('Collected fx:', np.shape(popt_fx_samples))
        popt_fx0 = []
        popt_fx1 = []
        popt_fx2 = []
        popt_invfx0 = []
        popt_invfx1 = []
        popt_invfx2 = []
        popt_fxMr0 = []
        popt_fxMr1 = []
        popt_fxMr2 = []
        for ix in range(0, len(popt_x)):
            # collect fx
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [popt_fx_samples[i][isamp][ix] for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_fx0 += [[popt_x[ix], boots_ave[low16]]]
            popt_fx1 += [[popt_x[ix], boots_ave[mid]]]
            popt_fx2 += [[popt_x[ix], boots_ave[high16]]]

            # collect inv_fx
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [1/popt_fx_samples[i][isamp][ix] for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_invfx0 += [[popt_x[ix], boots_ave[low16]]]
            popt_invfx1 += [[popt_x[ix], boots_ave[mid]]]
            popt_invfx2 += [[popt_x[ix], boots_ave[high16]]]

            # collect effective radius
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [Eff_radius(popt_fx_samples[i][isamp][ix], popt_x[ix])/fmGeV/fmGeV for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_fxMr0 += [[popt_x[ix]/(mass*mass), boots_ave[low16]]]
            popt_fxMr1 += [[popt_x[ix]/(mass*mass), boots_ave[mid]]]
            popt_fxMr2 += [[popt_x[ix]/(mass*mass), boots_ave[high16]]]
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx0.txt'
        np.savetxt(save_name, popt_fx0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx1.txt'
        np.savetxt(save_name, popt_fx1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx2.txt'
        np.savetxt(save_name, popt_fx2, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx0.txt'
        np.savetxt(save_name, popt_invfx0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx1.txt'
        np.savetxt(save_name, popt_invfx1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx2.txt'
        np.savetxt(save_name, popt_invfx2, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfxMr0.txt'
        np.savetxt(save_name, popt_fxMr0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfxMr1.txt'
        np.savetxt(save_name, popt_fxMr1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfxMr2.txt'
        np.savetxt(save_name, popt_fxMr2, fmt='%.6e')

    elif sys.argv[1] == 'mr':

        fmGeV = 5.0676896

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        pxlist = []
        qxyzlist = []
        for index in range(1, len(commands)):

            line = commands[index].split()
            if 'mass' in line[0]:
                mass = float(line[1])
                describe = describe.replace('mass', 'mass'+str(mass)+'GeV')
                tcut = 4 * mass * mass
                print('  Hadron mass (GeV):', mass, ', tcut:', round(tcut,5))

            elif 'latsp' in line[0]:
                latsp = float(line[1])
                Ls = int(line[2])
                Lt = int(line[3])
                print('  Lattice spacing (fm):', latsp, 'size:',Ls,'x',Lt,np.exp(-Ls*latsp*fmGeV*mass))

            elif 'kmax' == line[0]:
                kmax = int(line[1])
                print('  kmax:', kmax)
                describe = describe.replace('kmax', 'kmax'+str(kmax))

            elif 'Qsqmax' in line[0]:
                Qsqmaxlist = np.array([float(line[i]) for i in range(1, len(line))])
                topt = tcut * (1 - np.sqrt(1 + Qsqmaxlist/tcut))
                print('  Qsq max (GeV^2):', Qsqmaxlist,', topt:', [round(topt[i],5) for i in range(0, len(topt))])
                describe = describe.replace('Qsqmax', 'Qsqmax'+str(Qsqmaxlist[0]))

            elif 'px' == line[0]:
                ipxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                pxlist += [ipxlist]
                describe = describe.replace('*', str(min(min(pxlist)))+'-'+str(max(max(pxlist))))
                print('  px: ', ipxlist[0], '-' , ipxlist[-1])
            
            elif 'tmin' == line[0]:
                tminlist = [i for i in range(int(line[1]), int(line[2])+1)]
                #describe = describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1]))
                print('  tmin: ', tminlist)

            elif 'qxyz' == line[0]:
                qxyz = np.array([int(line[1]), int(line[2]), int(line[3])])
                qxyzlist += [qxyz]
                print('  qxyz: ', qxyz)

            elif 'meanerr' == line[0]:
                mean_format = line[1]
                col = [int(line[2]), int(line[3])]
                print('  Mean errors file:', mean_format.split('/')[-1])

            elif 'sample' == line[0]:
                sample_format = line[1]
                print('  Sample format:', sample_format.split('/')[-1])
        print('Job describe:', describe,'\n')

        def dE(px,qx,qy,qz):
            return Ep(latsp,Lt,Ls,mass,px,qx,qy,qz) - Ep(latsp,Lt,Ls,mass,px,0,0,0)
        popt_collection = []
        popt_samples = [[],[],[]]
        popt_x = np.array([0.01*i for i in range(0, 301)])
        popt_fx_samples = []

        for it in range(0, len(tminlist)):
            it_mean_data = np.loadtxt(mean_format.replace('#', str(tminlist[it])+'-'+str(tminlist[it])))
            it_sample_data = np.loadtxt(sample_format.replace('#', str(tminlist[it])+'-'+str(tminlist[it])))
            for ip in range(0, len(pxlist)):
                ipxlist = pxlist[ip] #ipxlist is a list of px
                for iQsq in range(0, len(Qsqmaxlist)):
                    iQsq_popt_fx_samples = []
                    iQsqmax = Qsqmaxlist[iQsq]
                    iQsqmax_topt = topt[iQsq]
                    ff_dEsq = []
                    ff_qsq = []
                    ff_samples = []
                    ff_err = []
                    for iq in range(0, len(it_mean_data)):
                        if round(it_mean_data[iq][0]) in ipxlist:
                            iqxyz = np.array([it_mean_data[iq][1],it_mean_data[iq][2],it_mean_data[iq][3]])
                            if array2D_exit(iqxyz, qxyzlist):
                                if it_mean_data[iq][col[0]] < iQsqmax:
                                    ff_dEsq += [(dE(it_mean_data[iq][0],it_mean_data[iq][1],it_mean_data[iq][2],it_mean_data[iq][3])*latsp*fmGeV)**2]
                                    ff_qsq += [it_mean_data[iq][col[0]]]
                                    ff_err += [it_mean_data[iq][col[1]]]
                                    ff_samples += [it_sample_data[iq]]
                    ff_samples = np.transpose(ff_samples)
                    print('tmin:', tminlist[it], ', pxlist:', ipxlist[0], '-', ipxlist[-1], ', Qsqmax:', iQsqmax,'Data shape:',np.shape(ff_samples))

                    def ff_res(qsq,dEsq,ff,fferr,rsq,k):
                        #res = 1/(1 + (rsq + k[0]*dEsq) * qsq/6 + k[1]*qsq*qsq) - ff
                        res = 1/(1 + (rsq + 0*dEsq) * qsq/6) - ff
                        return res/fferr

                    radius_samples = []
                    k_samples = []
                    chisq_samples = []
                    popt_start = [1] + [0 for i in range(0, 1)]
                    #print(ff_dEsq)
                    for isamp in range(0, len(ff_samples)):

                        def mini_ff_res(popt):
                            rsq = popt[0]
                            k = [popt[i] for i in range(1, len(popt))]
                            return ff_res(np.array(ff_qsq),np.array(ff_dEsq),np.array(ff_samples[isamp]),np.array(ff_err),rsq,k)
                        res = least_squares(mini_ff_res, popt_start, method='lm')
                        radius_samples += [res.x[0]/(fmGeV**2)]
                        k_samples += [res.x[1]/(fmGeV**2)]
                        chisq_samples += [sum(np.square(res.fun)) / (len(ff_qsq)-len(popt_start))]

                        def ff_fx(qsq):
                            return 1/(1 + (res.x[0]) * qsq/6)
                        iQsq_popt_fx_samples += [ff_fx(popt_x)]

                    popt_samples[0] += [radius_samples]
                    popt_samples[1] += [k_samples]
                    #print(k_samples)
                    popt_samples[2] += [chisq_samples]
                    popt_fx_samples += [iQsq_popt_fx_samples]

        # Save the radius samples
        save_name = samplefolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_samples_radius.txt'
        print('Saving >>',save_name.split('/')[-1])
        np.savetxt(save_name, popt_samples[0], fmt='%.6e')

        # save the fit parameters from bootstrap
        print('Collected parameters:',np.shape(popt_samples))
        npick = len(ff_samples)
        mid = int(npick*0.5)
        high16 = int(npick*0.84)
        low16 = int(npick*0.16)
        poptlist = []
        for i in range(0, len(popt_samples)):
            boots_ave = []
            boots_std = []
            for isamp in range(0, len(popt_samples[i][0])):
                isamp_boots = [popt_samples[i][j][isamp] for j in range(0, len(popt_samples[i]))]
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            sys_popt = [boots_ave[mid], boots_ave[mid]-boots_ave[low16],boots_ave[high16]-boots_ave[mid]]
            sys_popt += [boots_std[mid],np.sqrt(max([boots_ave[mid]-boots_ave[low16],boots_ave[high16]-boots_ave[mid]])**2+boots_std[mid]**2)]
            print(sys_popt)
            poptlist += [sys_popt]
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_RadiusSys.txt'
        np.savetxt(save_name, poptlist, fmt='%.6e')

        # save the fit results fx from bootstrap samples
        print('Collected fx:', np.shape(popt_fx_samples))
        popt_fx0 = []
        popt_fx1 = []
        popt_fx2 = []
        popt_invfx0 = []
        popt_invfx1 = []
        popt_invfx2 = []
        for ix in range(0, len(popt_x)):
            # collect fx
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [popt_fx_samples[i][isamp][ix] for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_fx0 += [[popt_x[ix], boots_ave[low16]]]
            popt_fx1 += [[popt_x[ix], boots_ave[mid]]]
            popt_fx2 += [[popt_x[ix], boots_ave[high16]]]

            # collect inv_fx
            boots_ave = []
            boots_std = []
            for isamp in range(0, npick):
                isamp_boots = [1/popt_fx_samples[i][isamp][ix] for i in range(0, len(popt_fx_samples))]
                #print(np.shape(boots_ave))
                boots_ave += [np.average(isamp_boots)]
                boots_std += [np.std(isamp_boots)]
            boots_ave = np.sort(boots_ave)
            boots_std = np.sort(boots_std)
            popt_invfx0 += [[popt_x[ix], boots_ave[low16]]]
            popt_invfx1 += [[popt_x[ix], boots_ave[mid]]]
            popt_invfx2 += [[popt_x[ix], boots_ave[high16]]]
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx0.txt'
        np.savetxt(save_name, popt_fx0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx1.txt'
        np.savetxt(save_name, popt_fx1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_FFfx2.txt'
        np.savetxt(save_name, popt_fx2, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx0.txt'
        np.savetxt(save_name, popt_invfx0, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx1.txt'
        np.savetxt(save_name, popt_invfx1, fmt='%.6e')
        save_name = resultfolder + describe.replace('#', str(tminlist[0])+'-'+str(tminlist[-1])) + '_invFFfx2.txt'
        np.savetxt(save_name, popt_invfx2, fmt='%.6e')

    elif sys.argv[1] == 'bmerr':

        fmGeV = 5.0676896

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        data_meanerr = []
        data_samples = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'px' == line[0]:
                pxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                #describe = describe.replace('*', str(min(pxlist))+'-'+str(max(pxlist)))
                print('  px: ', pxlist[0], '-' , pxlist[-1])

            elif 'meanerr' == line[0]:
                data = np.loadtxt(line[1])
                try:
                    tmp = data[0][0]
                    data_meanerr += [data]
                except:
                    data_meanerr += [[data]]

                #data_meanerr += [np.loadtxt(line[1])]
                print('  Mean errors file:', line[1].split('/')[-1])

            elif 'sample' == line[0]:
                data = np.loadtxt(line[1])
                try:
                    tmp = data[0][0]
                    data_samples += [data]
                except:
                    data_samples += [[data]]
                #data_samples += [np.loadtxt(line[1])]
                print('  Sample format:', line[1].split('/')[-1])

        npick = len(data_samples[0][0])
        mid = int(npick*0.5)
        high16 = int(npick*0.84)
        low16 = int(npick*0.16)
        print(  'npick:',npick)

        sys_data_meanerr = []
        sys_data_samples = []
        main_data_samples = []
        cmp_data_samples = []

        for ipx in pxlist:
            ipx_sys_data_meanerr = []
            ipx_sys_data_samples = []
            ipx_main_data_samples = []
            ipx_cmp_data_samples = []
            for iq in range(0, len(data_meanerr[0])):
                if int(data_meanerr[0][iq][0]) == ipx:
                    
                    ipx_main_data_samples += [data_samples[0][iq]]
                    ipx_cmp_data_samples += [data_samples[1][iq]]
                    iq_diff_samples = data_samples[0][iq] - data_samples[1][iq]
                    ipx_sys_data_samples += [list(iq_diff_samples)]
                    iq_diff_samples_sort = np.sort(iq_diff_samples)
                    #print(iq_diff_samples_sort,iq_diff_samples_sort[mid])
                    
                    iq_main_meanerr = data_meanerr[0][iq]
                    iq_main_meanerr[-2] = iq_diff_samples_sort[mid]
                    iq_main_meanerr[-1] = np.sqrt(iq_main_meanerr[-2]**2+iq_main_meanerr[-3]**2)
                    ipx_sys_data_meanerr += [data_meanerr[0][iq]]

            sys_data_meanerr += ipx_sys_data_meanerr
            sys_data_samples += ipx_sys_data_samples
            main_data_samples += ipx_main_data_samples
            cmp_data_samples += ipx_cmp_data_samples

            ipx_describe = describe.replace('*', str(ipx)+'-'+str(ipx))
            save_name = resultfolder + ipx_describe + '.txt'
            np.savetxt(save_name, ipx_sys_data_meanerr, fmt='%.6e')
            save_name = samplefolder + ipx_describe.replace('sys_','syserr_') + '_bootssamples.txt'
            np.savetxt(save_name, ipx_sys_data_samples, fmt='%.6e')
            save_name = samplefolder + ipx_describe.replace('sys_','nst3_') + '_bootssamples.txt'
            np.savetxt(save_name, ipx_main_data_samples, fmt='%.6e')
            save_name = samplefolder + ipx_describe.replace('sys_','nst2_') + '_bootssamples.txt'
            np.savetxt(save_name, ipx_cmp_data_samples, fmt='%.6e')
        
        describe = describe.replace('*', str(min(pxlist))+'-'+str(max(pxlist)))
        save_name = resultfolder + describe + '.txt'
        np.savetxt(save_name, sys_data_meanerr, fmt='%.6e')
        save_name = samplefolder + describe.replace('sys_','syserr_') + '_bootssamples.txt'
        np.savetxt(save_name, sys_data_samples, fmt='%.6e')
        save_name = samplefolder + describe.replace('sys_','nst3_') + '_bootssamples.txt'
        np.savetxt(save_name, main_data_samples, fmt='%.6e')
        save_name = samplefolder + describe.replace('sys_','nst2_') + '_bootssamples.txt'
        np.savetxt(save_name, cmp_data_samples, fmt='%.6e')

    elif sys.argv[1] == 'fxerr':

        fmGeV = 5.0676896

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        cmp_data = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'main' == line[0]:
                main_data0 = np.loadtxt(line[1].replace('#','0'))
                main_x = [main_data0[i][0] for i in range(0, len(main_data0))]
                main_data0 = np.array([main_data0[i][1] for i in range(0, len(main_data0))])
                main_data1 = np.loadtxt(line[1].replace('#','1'))
                main_data1 = np.array([main_data1[i][1] for i in range(0, len(main_data1))])
                main_data2 = np.loadtxt(line[1].replace('#','2'))
                main_data2 = np.array([main_data2[i][1] for i in range(0, len(main_data2))])
            
            elif 'cmp' == line[0]:
                i_data = np.loadtxt(line[1].replace('#','1'))
                cmp_data += [np.array([i_data[i][1] for i in range(0, len(i_data))])]

        diff_data_sq = np.zeros(len(main_data1))
        for ic in range(0, len(cmp_data)):
            ic_diff = cmp_data[ic] - main_data1
            diff_data_sq = diff_data_sq + np.power(ic_diff, 2)

        main_data0sys = main_data1 - np.sqrt(np.power(main_data0-main_data1,2)+diff_data_sq)
        main_data2sys = main_data1 + np.sqrt(np.power(main_data2-main_data1,2)+diff_data_sq)

        save_name = resultfolder + describe.replace('#','0')
        np.savetxt(save_name, np.transpose([main_x,main_data0]))
        save_name = resultfolder + describe.replace('#','1')
        np.savetxt(save_name, np.transpose([main_x,main_data1]))
        save_name = resultfolder + describe.replace('#','2')
        np.savetxt(save_name, np.transpose([main_x,main_data2]))

        save_name = resultfolder + describe.replace('#','sys0')
        np.savetxt(save_name, np.transpose([main_x,main_data0sys]))
        save_name = resultfolder + describe.replace('#','sys1')
        np.savetxt(save_name, np.transpose([main_x,main_data1]))
        save_name = resultfolder + describe.replace('#','sys2')
        np.savetxt(save_name, np.transpose([main_x,main_data2sys]))