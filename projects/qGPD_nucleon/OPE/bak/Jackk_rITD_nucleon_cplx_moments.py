#!/usr/bin/env python3
import numpy as np
import sys
from statistics import *
from qGPD_nucleon.OPE.rITD_Kernel_isoV import *
from scipy.optimize import least_squares
from utils.tools import *

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "rITD_pion_ritdReal_moments" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

class ITD_models:
    LO = CnC0LO
    NLO = CnC0NLO
    NNLO = CnC0NNLO

def alphas(mu):
    return alphas_5loop(mu)

def moms_exp(exp_list):
    exp_list1 = list(map(np.exp, exp_list))
    exp_list2= []
    for i in range(0, len(exp_list1)):
        exp_list2 += [sum(exp_list1[i:])]
    moms_list = []
    #for i in range(0, len(exp_list2)):
    #    moms_list += [sum(exp_list2[i:])]
    return exp_list1


def ITD_PDF(moms, z, pz, nmax, kernel):

    ITD = 1
    for n in range(1, nmax+1):
        ITD += kernel(z, n) * ((-complex(0, z*pz))**n/np.math.factorial(n)) * moms[n]

    return ITD

def ITD_GPD(moms, z, pz, nmax, kernel):

    ITD = 0
    for n in range(0, nmax+1):
        ITD += kernel(z, n) * ((-complex(0, z*pz))**n/np.math.factorial(n)) * moms[n]

    return ITD

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    # read parameters
    fitfunc = []
    rITD_err = []
    poptguess = []
    fixN = []
    dataset = []
    cutoff = False
    fixOrder_alps = None
    conti = 0
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu(GeV)' == line[0]:
            mu = float(line[1])
            print('Factorization scale:', mu, 'GeV', ', alpha_s =', round(alphas(mu),3))
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            print('Lattice spacing is', latsp)

        elif 'zmin' == line[0]:
            zmin = int(line[1])
            print('zmin (z/a) =', zmin)
            describe = describe.replace('zmin', 'zmin'+str(zmin))

        elif 'zmax' == line[0]:
            zmax = int(line[1])
            print('zmax (z/a)=', zmax)
            describe = describe.replace('_zmax', '_zmax'+str(zmax))

        elif 'zmv(fm)' == line[0]:
            zmv = float(line[1])
            print('Z move =', zmv, 'fm')

        elif 'nmax' == line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)
            describe = describe.replace('nmax', 'nmax'+line[1])

        elif 'kernel' == line[0]:
            fitfunc += [line[1]]
            print('matching kernel:', line[1])

        elif 'zCombine' in line[0]:
            if 'y' in line[1]:
                zcombine = True
                print('This is a combine fit of a range of z.')
            else:
                zcombine = False
                print('This is a fit for each z.')

        elif 'type' == line[0]:
            GPDtype = line[1]
            print('GPD type:', GPDtype)
            describe = describe.replace('type', GPDtype)


        elif 'pz' == line[0]:
            pzlist = [int(line[i]) for i in range(1, len(line))]
            print('pzlist:', pzlist)

        elif 'qxqyqz' == line[0]:
            Qlist = []
            for i in range(1, len(line)):
                q = line[i].split('-')
                Qlist += [[int(q[0]),int(q[1]),int(q[2])]]
            print('Qlist:', Qlist)

        elif 'meanerr' == line[0]:
            meanerrFormat = line[1].replace('@', GPDtype)
            MeanErr = []
            for iq, q in enumerate(Qlist):
                iqFormat = meanerrFormat.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2]))
                iqErr = []
                izReal = np.loadtxt(iqFormat)
                izImag = np.loadtxt(iqFormat.replace('real', 'imag'))
                for i in range(zmin, zmax+1):
                    iqErr += [[izReal[i][0], complex(izReal[i][1], izImag[i][1]),complex(izReal[i][2], izImag[i][2])]]
                MeanErr += [iqErr]
            print('Reading data:', meanerrFormat.split('/')[-1], 'in shape', np.shape(MeanErr))
            #print(MeanErr)

        elif 'samples' == line[0]:
            sampleFormat = line[1].replace('@', GPDtype)
            samples_All = []
            for iq, q in enumerate(Qlist):
                iqFormat = sampleFormat.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2]))
                iqSamples = []
                for iz in range(zmin, zmax+1):
                    izFile = iqFormat.replace('#', str(iz))
                    izReal = np.loadtxt(izFile)
                    izImag = np.loadtxt(izFile.replace('real', 'imag'))
                    iqSamples += [[complex(izReal[i], izImag[i]) for i in range(0, len(izImag))]]
                samples_All += [iqSamples]
            print('Reading data:', sampleFormat.split('/')[-1], 'in shape', np.shape(samples_All))

    for ifit in range(0, len(fitfunc)):

        print('\n')

        moms_list = []
        out_name = outfolder+describe.replace('moms','moms'+str(fitfunc[ifit])) + '.txt'

        moms_D = [1, 0.15355, 0.05536, 0.02197, 0.010846]
        kernel_tmp = getattr(ITD_models,fitfunc[ifit])
        # z [fm]
        def kernel(z, n):
            return kernel_tmp(mu,z,n)
        # z [fm], pz [GeV]
        def ITD_N(moms, z, pz):
            return ITD_GPD(moms, z, pz*fmGeV, nmax, kernel)
        def ITD_D(moms, z, pz):
            return ITD_PDF(moms, z, pz*fmGeV, len(moms_D)-1, kernel)

        def rITD(moms, z, pz):
            return ITD_N(moms, z, pz)/ITD_D(moms_D, z, pz)

        zskip = 0.02
        model_z = np.arange(zskip, zmax*latsp, zskip)
        model_samples = []
        popt_samples = []
        poptstart = [0.1 for i in range(0, nmax+1)]
        for isamp in range(0, len(samples_All[0][0])):
            popt_Q = []
            model_Q = []
            for iq in range(0, len(samples_All)):

                popt_zmax = []
                for izmax in range(nmax, len(samples_All[0])+1):
                    def Fit_res(popt):
                        res = []
                        for iz in range(0, izmax):
                            #if izmax == len(samples_All[0]):
                            #    print(MeanErr[iq][iz][0])
                            z = MeanErr[iq][iz][0] * latsp
                            pz = 1.25 # 1.25 GeV
                            rITD_diff = rITD(popt, z, pz) - samples_All[iq][iz][isamp]
                            res += [rITD_diff.real/MeanErr[iq][iz][2].real, rITD_diff.imag/MeanErr[iq][iz][2].imag]
                        return res
                    res = least_squares(Fit_res, poptstart, method='lm')
                    popt = list(res.x)
                    chisq = sum(np.square(res.fun[1:])) / (len(samples_All[0])*2-len(popt))
                    popt += [chisq]
                    popt_zmax += [popt]

                popt_Q += [popt_zmax]

                def model(z):
                    pz = 1.25
                    return rITD(res.x, z, pz)
                model_curve = []
                for iz, z in enumerate(model_z):
                    model_curve += [model(z)]
                model_Q += [model_curve]

            popt_samples += [popt_Q]
            model_samples += [model_Q]
        print(fitfunc[ifit] ,'>> Shape of popt:', np.shape(popt_samples), '>> Shape of curve:', np.shape(model_samples))

        for iq in range(0, len(popt_samples[0])):
            iq_popt = []
            for iz in range(0, len(popt_samples[0][0])):
                iz_popt = [zmin, nmax+1+iz]
                for ip in range(0, len(popt_samples[0][0][0])):
                    ip_samples = [popt_samples[i][iq][iz][ip] for i in range(0, len(popt_samples))]
                    iz_popt += [np.mean(ip_samples), np.std(ip_samples)*np.sqrt(len(popt_samples)-1)]
                iq_popt += [iz_popt]
            q = Qlist[iq]
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2]))
            print('Gonna save in:', savename, '\n')
            np.savetxt(savename, iq_popt, fmt='%.6f')

            iq_real_curve0 = []
            iq_real_curve1 = []
            iq_real_curve2 = []
            for iz, z in enumerate(model_z):
                iz_samples = [model_samples[isamp][iq][iz].real for isamp in range(0, len(model_samples))]
                iz_mean = np.mean(iz_samples)
                iz_err = np.std(iz_samples)*np.sqrt(len(iz_samples)-1)
                iq_real_curve0 += [[z, iz_mean-iz_err]]
                iq_real_curve1 += [[z, iz_mean]]
                iq_real_curve2 += [[z, iz_mean+iz_err]]
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2])).replace('.txt','_real_band0.txt')
            np.savetxt(savename, iq_real_curve0, fmt='%.6f')
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2])).replace('.txt','_real_band1.txt')
            np.savetxt(savename, iq_real_curve1, fmt='%.6f')
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2])).replace('.txt','_real_band2.txt')
            np.savetxt(savename, iq_real_curve2, fmt='%.6f')
            iq_imag_curve0 = []
            iq_imag_curve1 = []
            iq_imag_curve2 = []
            for iz, z in enumerate(model_z):
                iz_samples = [model_samples[isamp][iq][iz].imag for isamp in range(0, len(model_samples))]
                iz_mean = np.mean(iz_samples)
                iz_err = np.std(iz_samples)*np.sqrt(len(iz_samples)-1)
                iq_imag_curve0 += [[z, iz_mean-iz_err]]
                iq_imag_curve1 += [[z, iz_mean]]
                iq_imag_curve2 += [[z, iz_mean+iz_err]]
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2])).replace('.txt','_imag_band0.txt')
            np.savetxt(savename, iq_imag_curve0, fmt='%.6f')
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2])).replace('.txt','_imag_band1.txt')
            np.savetxt(savename, iq_imag_curve1, fmt='%.6f')
            savename = out_name.replace('qxqyqz', 'qx'+str(q[0]) + 'qy'+str(q[1]) + 'qz'+str(q[2])).replace('.txt','_imag_band2.txt')
            np.savetxt(savename, iq_imag_curve2, fmt='%.6f')



    '''
    for ifit in range(0, len(fitfunc)):

        moms_list = []
        out_name = outfolder+describe.replace('moms','moms'+str(fitfunc[ifit])) + '.txt'
        print('\nGonna save in:',out_name)

        poptsample = [] #poptsample[isamp][iz][ipopt]
        for isamp in range(0, rITD_samp_lenMax):
            print('Bootstrap sample:', isamp, '/', rITD_samp_lenMax)
            rITD_sample = []
            rITD_params = []
            ipick = random_uniform_int[-2][isamp]
            for idata in dataset:
                rITD_params += [idata[0]]
                rITD_samp_len = len(idata[2])
                if isamp < rITD_samp_len:
                    rITD_sample += [idata[2][isamp]]
                else:
                    rITD_sample += [idata[2][ipick%rITD_samp_len]]
            popt, zfmlist = rITD_fit(fitfunc[ifit], rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu*fmGeV, nmax, fixOrder_alps, fixN, cutoff[ifit], contiN, CmassN, zcombine)
            poptsample += [popt]
    

        for isamp in range(0, rITD_samp_lenMax):
            poptsample_save = []
            sample_name = samplefolder+describe.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit]))+'_bootssample'+str(int(isamp))+'.txt'
            for iz in range(0, len(zfmlist)):
                poptsample_save += [[zskip, zfmlist[iz]] + poptsample[isamp][iz]]
            #print(np.shape(poptsample[isamp]),poptsample[0])
            np.savetxt(sample_name, poptsample_save)

        for iz in range(0, len(zfmlist)):
            iz_moms = [zskip, zfmlist[iz]]
            for ipopt in range(0, len(poptsample[0][iz])):
                ipopt = [poptsample[isamp][iz][ipopt] for isamp in range(0, rITD_samp_lenMax)]
                ipopt = sorted(ipopt)
                ipopt_err = [ipopt[high16] - ipopt[mid], ipopt[mid] - ipopt[low16]]
                iz_moms += [ipopt[mid], max(ipopt_err)]
            np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
            #print(iz_moms)
            moms_list += [iz_moms]
        #print(out_name)
        np.savetxt(out_name, moms_list, fmt='%.4e')

    '''

        
