#!/usr/bin/env python3
import numpy as np
import numba as nb
import sys
import os
import traceback 
from scipy.optimize import least_squares
from scipy.optimize import curve_fit
from scipy import integrate
from tools import *
from qPDF_FT import *

# model large z
def hz_extra_Acd(z, A, c, d):
    return A * np.exp(-abs(c)*z) * z**(-abs(d))

# mu [fm]
@nb.jit
def alpsmu(mu):
    mu /= 5.0676896
    beta0 = 0.716197
    beta1 = 0.405285
    beta2 = 0.324447
    beta3 = 0.484842
    LL = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*LL) - 1/(beta0**2*LL**2)*beta1/beta0*np.log(LL) + 1/(beta0**3*LL**3)*((beta1/beta0)**2*(np.log(LL)**2-np.log(LL) - 1) + beta2/beta0) \
        + 1/(beta0**4*LL**4)*((beta1/beta0)**3 * (-np.log(LL)**3 + 5/2*np.log(LL)**2 + 2*np.log(LL) - 1/2) - (3*beta1*beta2)/beta0**2 * np.log(LL) + beta3/(2*beta0))
    return alpha_s

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='bgft':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        #zstep = latsp
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zstep':
                zstep = float(line[1])
                zsave = np.arange(0, 5, zstep)
                print('zstep:',zstep,'fm')

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'pzrange':
                pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('pzlist:',pzlist)

            elif line[0] == 'zmax':
                zmaxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('zmaxlist:','['+line[1]+', '+line[2]+']')

            elif line[0] == 'mean':
                mean_format = line[1]
                col_err = int(line[2])
                print('\nmean:', mean_format.split('/')[-1])
                
            elif line[0] == 'sample':
                sample_format = line[1]
                col_samp = int(line[2])
                print('sample:', sample_format.split('/')[-1],'\n')
        
        for ipz in range(0, len(pzlist)):

            ipz_describe = describe.replace('*', str(pzlist[ipz]))

            ipz_mean = np.loadtxt(mean_format.replace('*',str(pzlist[ipz])))
            ipz_sample_format = sample_format.replace('*',str(pzlist[ipz]))
            ipz_samples = []
            for iz in range(0, len(ipz_mean)):
                iz_samples = np.loadtxt(ipz_sample_format.replace('#',str(iz)))
                ipz_samples +=  [[iz_samples[isamp][col_samp] for isamp in range(0, len(iz_samples))]]

            npick = len(ipz_samples[0])
            low16 = int((0.16*npick))
            high16 = int(np.ceil(0.84*npick))
            mid = int(0.5*npick)
            ipz_fm = round(2*np.pi*pzlist[ipz]/(Ns*latsp),2)
            print('\npz =', ipz_fm, '[fm^-1]; npick:', npick)

            ipz_zmaxlist = zmaxlist
            print('ipz zmax list:', ipz_zmaxlist)
            
            for izmax in ipz_zmaxlist:

                izmax_describe = ipz_describe.replace('zmax','zmax'+str(izmax))

                ipz_zlist = [ipz_mean[iz][0]*latsp for iz in range(0, izmax+1)]
                ipz_err = [ipz_mean[iz][col_err] for iz in range(0, izmax+1)]

                ipz_hzBG_save_samples = []
                for isamp in range(0, npick):
                    ipz_isamp_hzLat = [ipz_samples[iz][isamp] for iz in range(0, izmax+1)]
                    ipz_isamp_hzBG=qPDF_matrix_BayesInfer(np.array(ipz_zlist), np.array(ipz_isamp_hzLat), np.array(ipz_err))
                    ipz_isamp_hzBG_save = [ipz_isamp_hzBG(iz) for iz in zsave]
                    ipz_hzBG_save_samples += [ipz_isamp_hzBG_save]
                
                ipz_hzBG_save_mean = []
                ipz_hzBG_save_0 = []
                ipz_hzBG_save_1 = []
                ipz_hzBG_save_2 = []
                for iz in range(0, len(zsave)):
                    iz_hzBG_save_samples = [ipz_hzBG_save_samples[isamp][iz] for isamp in range(0, npick)]
                    iz_hzBG_save_samples = sorted(iz_hzBG_save_samples)
                    ipz_hzBG_save_mean += [[zsave[iz],  (iz_hzBG_save_samples[high16]+iz_hzBG_save_samples[low16])/2, (iz_hzBG_save_samples[high16]-iz_hzBG_save_samples[low16])/2]]
                    ipz_hzBG_save_0 += [[zsave[iz],  iz_hzBG_save_samples[low16]]]
                    ipz_hzBG_save_1 += [[zsave[iz],  (iz_hzBG_save_samples[high16]+iz_hzBG_save_samples[low16])/2]]
                    ipz_hzBG_save_2 += [[zsave[iz],  iz_hzBG_save_samples[high16]]]

                save_name = outfolder + izmax_describe + '_0.txt'
                np.savetxt(save_name, ipz_hzBG_save_0, fmt='%.8e')
                save_name = outfolder + izmax_describe + '_1.txt'
                np.savetxt(save_name, ipz_hzBG_save_1, fmt='%.8e')
                save_name = outfolder + izmax_describe + '_2.txt'
                np.savetxt(save_name, ipz_hzBG_save_2, fmt='%.8e')

                save_name = outfolder + izmax_describe + '.txt'
                np.savetxt(save_name, ipz_hzBG_save_mean, fmt='%.8e')
                sample_name = samplefolder + izmax_describe + '_bootssamples.txt'
                np.savetxt(sample_name, ipz_hzBG_save_samples, fmt='%.8e')
                #print(np.shape(ipz_hzBG_save_samples),np.shape(ipz_hzBG_save_mean))