#!/usr/bin/env python3
import numpy as np
import numba as nb
from scipy.optimize import least_squares

#-------------------------------------------------#
#---------------------  DFT  ---------------------#
#-------------------------------------------------#

# data is a 2-d array.
# data[z][0] have z from 0 to zmax in fm.
# data[z][1] is the matrix elements.
def qPDF_DFT_real(x, pz, data):
    a = data[1][0] - data[0][0]
    qPDF = pz*a/(2*np.pi) * data[0][1]
    for iz in range(1, len(data)):
        qPDF += 2*pz*a/(2*np.pi) * np.cos(x*pz*data[iz][0]) * data[iz][1]
    return qPDF

#-------------------------------------------------#
#----------------------  BG  ---------------------#
#-------------------------------------------------#

# data is a 2-d array.
# data[z][0] have z from 0 to zmax in fm.
# data[z][1] is the matrix elements.
# precondition e^(-2x)
def Mij(x, zpz1, zpz2):
    part1 = 256/np.power(16+(zpz1-zpz2)*(zpz1-zpz2),3) - 4*(3+8*x)/np.power(16+(zpz1-zpz2)*(zpz1-zpz2),2) + x*(1+2*x)/(16+(zpz1-zpz2)*(zpz1-zpz2))
    part2 = 256/np.power(16+(zpz1+zpz2)*(zpz1+zpz2),3) - 4*(3+8*x)/np.power(16+(zpz1+zpz2)*(zpz1+zpz2),2) + x*(1+2*x)/(16+(zpz1+zpz2)*(zpz1+zpz2))
    return 2 * (part1 + part2)
def uj(zpz):
    return 4/(4 + zpz*zpz)
def qPDF_BG_real(x, pz, data, a):

    print(data)

    zlen = len(data)
    xlen = len(x)

    zlist = np.zeros(zlen)
    ITD = np.zeros(zlen)
    for i in range(0, zlen):
        zlist[i] = data[i][0] * a
        ITD[i] = data[i][1]

    qPDF = np.zeros(xlen)
    sigmax = np.zeros(xlen)
    u = uj(pz*zlist)
    for ix in range(0, xlen):
        ix_M = np.zeros((zlen,zlen))
        for i in range(0, zlen):
            ix_M[i] = Mij(x[ix], pz*zlist[i], pz*zlist)
        ix_Minv = np.linalg.inv(ix_M)
        temp1 = np.dot(ix_Minv, u)
        temp2 = np.dot(u, temp1)
        ix_a = 1/temp2*temp1
        #print(ix_a,ITD,np.sum(ix_a*ITD))
        qPDF[ix] = np.sum(ix_a*ITD) * np.exp(-2*x[ix])
        sigmax[ix] = np.dot(np.transpose(ix_a), np.dot(ix_M, ix_a))

    return qPDF,sigmax

'''
data = [[0, 1], [1, 0.8], [2, 0.5]]
pz = 5
a = 0.1
x = [0.1, 0.2]
qPDF, sigmax = qPDF_BG_real(x, pz, data, a)
print(qPDF, sigmax)
'''


#-------------------------------------------------#
#------------  Bayes-Gauss inference  ------------#
#-------------------------------------------------#
@nb.njit
def Bayes_covSE(dz,l,sigma):
    return sigma*sigma * np.exp(-dz*dz / (2*l*l))
@nb.njit
def qPDF_matrix_prior(z):
    return 0
@nb.njit
def qPDF_matrix_lkhd_covSE(z, h, herr, l, sigma):
    #cov matrix
    cov_kp = np.zeros((len(z), len(z)))
    for i in range(0, len(z)):
        for j in range(0, len(z)):
            cov_kp[i][j] = Bayes_covSE(abs(z[i]-z[j]),l,sigma)
            if i == j:
                cov_kp[i][j] += herr[i]
    #print(np.dot(np.transpose(h), np.linalg.inv(cov_kp)),np.dot(np.dot(np.transpose(h), np.linalg.inv(cov_kp)), h))
    lkhd = -0.5*np.dot(np.dot(np.transpose(h), np.linalg.inv(cov_kp)), h) - 0.5*np.log(np.linalg.det(cov_kp)) - 0.5*len(z)*np.log(2*np.pi)
    #print(lkhd)
    return lkhd
def qPDF_matrix_covSE(z, h, herr):
    def lkhd(popt):
        return -qPDF_matrix_lkhd_covSE(z, h, herr, popt[0], popt[1])
    res = least_squares(lkhd, [1,1],method='dogbox')
    #print(res.x)
    def cov(dz):
        return Bayes_covSE(dz,res.x[0],res.x[1])
    return cov
def qPDF_matrix_BayesInfer(z, h, herr):

    h = np.array([h[i] - qPDF_matrix_prior(z[i]) for  i in range(0, len(z))])
    cov_kp_func = qPDF_matrix_covSE(z, h, herr)

    cov_kp = np.zeros((len(z), len(z)))
    for i in range(0, len(z)):
        for j in range(0, len(z)):
            cov_kp[i][j] = cov_kp_func(abs(z[i]-z[j]))
            if i == j:
                cov_kp[i][j] += herr[i]
    cov_kp_inv = np.linalg.inv(cov_kp)
    kinv_h = np.dot(cov_kp_inv, h)

    def qPDF_matrix(x):
        v = np.zeros(len(z))
        for i in range(0, len(z)):
            v[i] = cov_kp_func(abs(x-z[i]))
        return qPDF_matrix_prior(x) + np.dot(np.transpose(v), kinv_h)
    return qPDF_matrix

#z = np.array([0, 1, 2])
#h = np.array([1, 0.9, 0.7])
#herr = np.array([0.00001, 0.00002, 0.00003])
#hz = qPDF_matrix_BayesInfer(z, h, herr)
#print(hz(0),hz(1),hz(2),hz(5),hz(10))
fmGeV = 5.0676896
pz = 3 * fmGeV
def h(z):
    return np.exp(-10*z**2)