#!/usr/bin/env python3
import numpy as np
import numba as nb
import traceback 
import sys
from tools import *
import inspect
from scipy.optimize import least_squares

from rITD_Numerical_Kernel import *

'''----------------------------------'''
'''--------------kernels-------------'''
'''----------------------------------'''

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

alphas_NNLO_data = np.loadtxt('/Users/Xiang/Desktop/docs/0-2020/papers/2020-4-Pion_qpdf_PhysicalMass_qPDF_v0/analysis/rITD/moments/NNLO_evo_cmp/alpha_s/alphas_NNLO.txt')
mulist = [alphas_NNLO_data[i][0] for i in range(0, len(alphas_NNLO_data))]
alpslist = [alphas_NNLO_data[i][1] for i in range(0, len(alphas_NNLO_data))]

# mu [fm]
@nb.jit
def alpsmu(mu):
    #alps0 = 0.24
    #mu0 = 3.2*5.0676896
    alps0 = 0.296
    mu0 = 2*5.0676896
    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
#print(alpsmu(3.2*5.0676896))
#2 GeV, 0.296

def alpsmuNNLO(mu):
    f = interp1d(mulist, alpslist)
    return f(mu)
#print(alpsmuNNLO(2*5.0676896))

def Cn_NLO(z,n, mu):
    alps = alpsmu(mu)
    CF = 4/3
    gammaE = 0.5772156649
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

def Cn_NNLO(z, n, mu, alps = 0):
    if alps == 0:
        alps = alpsmu(mu)
    #print(z, n, mu/5.0676896, alps,CnNNLO(z, n, mu, alps))
    return CnNNLO(z, n, mu, alps)

def CnC0_NLO(mu,z,n):
    return Cn_NLO(z,n, mu)/Cn_NLO(z,0, mu)
def CnC0_NNLO(mu,z,n):
    return Cn_NNLO(z, n, mu)/Cn_NNLO(z, 0, mu)


'''----------------------------------'''
'''----------renormalization---------'''
'''----------------------------------'''

# bm (bare matrix elements) is a 1-D array. bm[z]
# dm : delta_m calculated from Polyakov loop
# dm [GeV]; a [fm]; zs [1]
def Poly_renorm(bm_up, bm_down, dm, a, zup, zs):
    fmGeV = 5.0676896
    #Polymt = np.zeros(len(bm_up))
    #for iz in range(0, len(bm_up)):
    #    Polymt[iz] = bm_up[iz]/bm_down[zs]*np.exp((iz-zs)*a*dm*fmGeV)
    return bm_up/bm_down*np.exp((zup-zs)*a*dm*fmGeV)

# renormalon and higher twist model
# mu [GeV]; zs[1]
def Poly_renorm_model1(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**4 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**4 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model2(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**4) + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**4) + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model3(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model3a(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model3b(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2 + cut1/zup/zup + cut2/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2 + cut1/zs/zs + cut2/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model4(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2) + cut1/zup/zup + cut2/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2) + cut1/zs/zs + cut2/zs
    return exp*(OPEup/OPEdown)
def Poly_renorm_model4a(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2) + cut1/zup/zup + cut2/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2) + cut1/zs/zs + cut2/zs
    return exp*(OPEup/OPEdown)
def Poly_renorm_model4b(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2) + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2) + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

'''
def Poly_renorm_model3(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*Cn_NNLO(zup*a,2,mu)*(zup*a*fmGeV)**2 + tw2*Cn_NNLO(zup*a,4,mu)*(zup*a*fmGeV)**4 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*Cn_NNLO(zs*a,2,mu)*(zs*a*fmGeV)**2 + tw2*Cn_NNLO(zs*a,4,mu)*(zs*a*fmGeV)**4 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)
'''

def Poly_renorm_model4(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*Cn_NNLO(zup*a,2,mu)*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**2 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*Cn_NNLO(zup*a,2,mu)*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**2 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model5(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * (1 + tw1*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**4) + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* (1 + tw1*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**4) + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model_TMC(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*Cn_NNLO(zup*a,2,mu)*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**2 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*Cn_NNLO(zs*a,2,mu)*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**2 + cut2/zs/zs + cut1/zs
    #print(zup*a, Cn_NNLO(zup*a,0,mu))
    return exp*(OPEup/OPEdown)

def Poly_renorm_model_dm0(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    return -dm0*(zup-zs)*a*fmGeV


class model_list:

    model1 = Poly_renorm_model1
    model2 = Poly_renorm_model2
    model3 = Poly_renorm_model3
    model3a = Poly_renorm_model3a
    model3b = Poly_renorm_model3b
    model4 = Poly_renorm_model4
    model5 = Poly_renorm_model5
    model_dm0 = Poly_renorm_model_dm0

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='ht':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')

        #fitfunc = Poly_renorm_model
        fmGeV = 5.0676896
        m0 = 0
        models = []
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*latsp,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))
                print('mu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))

            elif line[0] == 'm0':
                m0_prior = float(line[1])
                m0_err = float(line[2])
                if m0_err < 1e5:
                    describe = describe + '_m0'
                print('m0 priored:', m0_prior, m0_err)

            elif line[0] == 'ht1':
                ht1_prior = float(line[1])
                ht1_err = float(line[2])
                if ht1_err < 1e5:
                    describe = describe + '_ht1'
                print('ht1 priored:', ht1_prior, ht1_err)

            elif line[0] == 'zmin':
                zminlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('zminlist:','['+line[1]+', '+line[2]+']')
                #if len(zminlist) == 1:
                #    describe = describe.replace('zlist','zmin'+line[1])

            elif line[0] == 'zmax':
                zmaxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('zmaxlist:','['+line[1]+', '+line[2]+']')
                if len(zmaxlist) == 1:
                    describe = describe.replace('zlist','zmax'+line[1])

            elif line[0] == 'model':
                
                models = [line[i] for i in range(1, len(line))]
                #fitfunc = getattr(model_list,line[1])
                #describe = describe.replace('HTfit', line[1])

            elif line[0] == 'nParam':
                nParam = int(line[1])
                print('Number of parameters:', nParam)
                describe = describe+'_nP'+str(nParam)

            elif line[0] == 'mean':
                mean = line[1]
                col_ave = int(line[2])
                col_err = int(line[3])
                print('\nmean:', mean.split('/')[-1])
                mean = mean.replace('zs#','zs'+str(zs))
                mean = np.loadtxt(mean)
                print('mean shape:', np.shape(mean))
                mean_ave = [mean[i][col_ave] for i in range(0, len(mean))]
                mean_err = [mean[i][col_err] for i in range(0, len(mean))]
                #mean_err[zs] = 1.0
                print('mean ave:',[round(mean_ave[i],5) for i in range(0, zs+1)],'...')
                print('mean err:',[round(mean_err[i],5) for i in range(0, zs+1)],'...')

            elif line[0] == 'sample':
                sample = line[1]
                print('sample:', sample.split('/')[-1])
                sample = sample.replace('zs#','zs'+str(zs))
                sample = np.loadtxt(sample)
                print('sample shape:', np.shape(sample))
                npick = len(sample[0])-1
                print('sample count:', npick, '\n')
                low16 = int((0.16*npick))
                high16 = int(np.ceil(0.84*npick))
                mid = int(0.5*npick)

        # create z list
        for izmin in zminlist:

            iz_describe = describe.replace('zlist','zmin'+str(izmin))

            zlist = []
            for izmax in zmaxlist:
                if izmax - izmin < nParam:
                    continue
                izlist = [iz for iz in range(izmin, izmax+1)]
                if zs in izlist:
                    izlist.remove(zs)
                zlist += [izlist]

            for imodel in models:

                fitfunc = getattr(model_list,imodel)
                imodel_describe = iz_describe.replace('HTfit', imodel)
                #print(describe,imodel_describe)
                
                def Fit_model(zup, popt):
                    fitpopt = [0, 0, 0, 0, 0]
                    for i in range(0, len(popt)):
                        fitpopt[i] = popt[i]
                    return fitfunc(fitpopt[0], fitpopt[1], fitpopt[2], fitpopt[3], fitpopt[4], latsp, zup, zs, mu)

                # fit loop
                poptlist = []
                for izlist in zlist:
                    poptstart = np.zeros(nParam)
                    popt_sample = [[] for i in range(0, nParam)]
                    chisq_sample = []
                    
                    for isamp in range(0, npick):
                        
                        def Fit_res(popt):
                            res = [(m0_prior-popt[0])/m0_err, (ht1_prior-popt[1])/ht1_err]
                            for iz in range(0, len(izlist)):
                                zup = izlist[iz]
                                res += [(Fit_model(zup, popt)-sample[zup][isamp+1])/mean_err[zup]]
                            return res
                            
                        res = least_squares(Fit_res, poptstart, method='lm')
                        popt = res.x
                        chisq = sum(np.square(res.fun)) / (len(izlist)-len(popt))
                        for i in range(0, nParam):
                            popt_sample[i] += [popt[i]]
                        chisq_sample += [chisq]
                        
                    chisq = sorted(chisq_sample)[mid]
                    izlist_poptlist = [izlist[0]*latsp, izlist[-1]*latsp]
                    for i in range(0, nParam):
                        i_popt_sample = sorted(popt_sample[i])
                        izlist_poptlist += [(i_popt_sample[high16]+i_popt_sample[low16])/2,(i_popt_sample[high16]-i_popt_sample[low16])/2]
                    izlist_poptlist += [chisq]
                    print('z in',izlist_poptlist[:2], ', chisq/dof:',izlist_poptlist[-1])
                    poptlist += [izlist_poptlist]
                
                save_name = outfolder + imodel_describe + '.txt'
                print('\nSaving: ', save_name.split('/')[-1])
                np.savetxt(save_name, poptlist)

                zmax_poptlist = poptlist[-1]
                zmax_popt = [0, 0, 0, 0, 0]
                for i in range(0, nParam):
                    zmax_popt[i] = zmax_poptlist[2*(i+1)]
                print('Saving plot of:',zmax_popt)
                zfm_list = np.linspace(0.01,1,200)
                mtx_list = Fit_model(zfm_list/latsp, zmax_popt)
                save_data = np.transpose([list(zfm_list), list(mtx_list)])
                #print(save_data)
                #save_name = outfolder + imodel_describe + '_CurveZmax' + str(zmaxlist[-1]) + '.txt'
                #np.savetxt(save_name, save_data)



