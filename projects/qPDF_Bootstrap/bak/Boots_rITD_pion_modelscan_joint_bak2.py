#!/usr/bin/env python3
import numpy as np
import time
import math
import traceback 
import sys
#from tools import *
import inspect
from scipy.optimize import curve_fit
from scipy import special
from scipy import integrate
from statistics import *
from multiprocessing.dummy import Pool as ThreadPool
from rITD_pion_ritdReal import *


random_uniform = np.loadtxt('random_uniform.txt')
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### Model functions ########################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def pdfs_model_ab(x, n, *ab):
    #print(ab)
    return (x**n)*special.gamma(2+ab[0]+ab[1])/(special.gamma(1+ab[0])*special.gamma(1+ab[1]))*(x**ab[0])*((1-x)**ab[1])

def pdfs_model_abc(x, n, *abc):
    #A = integrate.quad(lambda x: (x**abc[0])*((abc[2]-x)**abc[1]), 0, 1)[0]
    #return 1/A*(x**n)*(x**abc[0])*((abc[2]-x)**abc[1])
    return (x**n)*(x**abc[0])*((abc[2]-x)**abc[1])/(abc[2]**(1+abc[0]+abc[1]) \
        * special.beta(1+abc[0],1+abc[1]) * special.betainc(1+abc[0],1+abc[1],1/abc[2]))

class pdfs_model_list:

    model_ab = pdfs_model_ab
    model_abc = pdfs_model_abc

def ListLast(List):
    return List[-1]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def moms_exp(exp_list):
    moms_list = []
    for i in range(0, len(exp_list)):
        moms_i = 0
        for j in range(i, len(exp_list)):
            moms_i += np.exp(-exp_list[j])
        moms_list += [moms_i]
    return moms_list

def ritd_Pzn_pznm_real(latsp, z, pz, mass, mu, nmax, moms,bconti,rcut,cmass,k=0, pznm=0, kernel_list=[], **kwargs):
    c = mass*mass/(4*pz*pz)
    ritdsum = 1
    ritdsum_down = 1

    if len(bconti) < int(nmax/2):
        for i in range(len(bconti), int(nmax/2)):
            bconti += [0]

    if len(cmass) < int(nmax/2):
        for i in range(len(cmass),int(nmax/2)):
            cmass += [0]

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 0
        for j in range(0, i+1):
            n_TMC += factorial(n-j)/factorial(j)/factorial(n-2*j)*c**j

        #print(n,i,kernel_list[i])
        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/factorial(n) * 1 * (moms[n]+cmass[i-1]*(mass-mass0)+bconti[i-1]*latsp*latsp))
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/factorial(n) * 1 * (moms[n]+cmass[i-1]*(mass-mass0)+bconti[i-1]*latsp*latsp))
    
    ritdsum += rcut*(latsp*pz)*(latsp*pz)
    ritdsum_down += rcut*(latsp*pznm)*(latsp*pznm)

    return ritdsum/ritdsum_down

def make_ritd_Pzn_pznm_real(mu, nmax, k=0, fixN = []):
    def ritd_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z,exp_list):
        latsp, mass, pznm, pz, z, kernel_list = latsp_mass_pznm_pz_z
        #print(latsp, mass, pznm, pz, z, kernel_list)
        moms = [1]
        bconti = []
        cmass = []
        if len(exp_list) > int(nmax/2):
            for i in range(len(fixN), int(nmax/2)):
                moms += [0, exp_list[i]]
            if len(exp_list) > int(nmax/2)+1:
                for i in range(int(nmax/2), int(nmax/2)+2):
                    bconti += [exp_list[i]]
            if len(exp_list) > int(nmax/2)+3:
                for i in range(int(nmax/2)+1, int(nmax/2)+3):
                    cmass += [exp_list[i]]
            rcut = exp_list[-1]
            #print(latsp_mass_pznm_pz_z)
            #print(bconti,rcut,cmass,moms)
        func = ritd_Pzn_pznm_real(latsp,z,pz,mass,mu,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list)
        return func
    return ritd_Pzn_pznm_real_constrained

################################################
############## fitting functions  ##############
################################################

#def model_scan_loops(ritd_matchFunc, rITD, rITD_err, pznm_fm, pxlist_fm, zskip, zmaxlist, params, moms, mass, mu, nmax):
# rITD[ilat][iz], rITD_err[ilat][iz]
def model_scan_loops(ritd_matchFunc, rITD_sample, rITD_err, rITD_params, zskip, zmax, params, moms, mu, nmax, joint_params=[]):

    if 'ritdNLO' == ritd_matchFunc:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax)
        def kernel(mu, z, n,**kwargs):
            return CnC0NLO(mu,z,n,k=0,**kwargs)
    elif 'ritdNLOLL' == ritd_matchFunc:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax)
        def kernel(mu, z, n,**kwargs):
            if n <= 8:
                return CnC0NLO(mu,z,n,k=0,**kwargs)
            else:
                return CnC0NLOLL(mu,z,n,k=0,**kwargs)
    elif 'ritdNLOEP' == ritd_matchFunc:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax)
        def kernel(mu, z, n,**kwargs):
            return CnC0NLO_EP(mu,z,n,k=0,**kwargs)
    elif 'ritdNNLO' == ritd_matchFunc:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax)
        def kernel(mu, z, n,**kwargs):
            return CnC0NNLO(mu,z,n,k=0,**kwargs)
    elif 'ritdNNLOEP' == ritd_matchFunc:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax)
        def kernel(mu, z, n,**kwargs):
            return CnC0NNLO_EP(mu,z,n,k=0,**kwargs)
    elif 'ritdNNLONLL' == ritd_matchFunc:
        fitfunc = make_ritd_Pzn_pznm_real(mu, nmax)
        def kernel(mu, z, n,**kwargs):
            if n <= 8:
                return CnC0NNLO(mu,z,n,k=0,**kwargs)
            else:
                return CnC0NNLONLL(mu,z,n,k=0,**kwargs)

    chisq_list = []
    poptlist = []
    zfmlist = []
    #zfm = zmv
    for zfm in [zmax]:
        # fit y
        itdLat = []
        itdLatErr = []
        
        # fit x: (latsp,mass,pznm,pz,z)
        itdlatsp_fm = []
        itdmass_fm = []
        itdpznm_fm = []
        itdpz_fm = []
        itdz_fm = []
        itdz_kernel = []

        for ilat in range(0, len(rITD_params)):

            ilat_rITD = rITD_sample[ilat]
            ilat_rITD_err = rITD_err[ilat]

            ilat_latsp = rITD_params[ilat][2]
            ilat_mass_fm = rITD_params[ilat][3] * fmGeV
            ilat_pznm = rITD_params[ilat][4]
            ilat_pzlist = rITD_params[ilat][-1]

            for ipx in range(0, len(ilat_pzlist)):
                for iz in range(zskip, round(zfm/ilat_latsp)+1):

                    itdLat += [ilat_rITD[ipx][iz]]
                    itdLatErr += [ilat_rITD_err[ipx][iz]]

                    itdlatsp_fm += [ilat_latsp]
                    itdmass_fm += [ilat_mass_fm]
                    itdpznm_fm += [ilat_pznm]
                    itdpz_fm += [ilat_pzlist[ipx]]
                    itdz_fm += [iz*ilat_latsp]

                    iz_kernel_mu = [1] + [kernel(mu, iz*ilat_latsp, 2*i, alps=alpsmu(mu)) for i in range(1, int(nmax/2)+1)]
                    iz_kernel_halfmu = [1] + [kernel(mu, iz*ilat_latsp, 2*i, alps=alpsmu(mu/2)) for i in range(1, int(nmax/2)+1)]
                    iz_kernel_doubmu = [1] + [kernel(mu, iz*ilat_latsp, 2*i, alps=alpsmu(mu*2)) for i in range(1, int(nmax/2)+1)]
                    iz_kernel = [iz_kernel_mu, iz_kernel_halfmu, iz_kernel_doubmu]
                    itdz_kernel += [iz_kernel]

        #if len(itdLat) < int(nmax):
        #    continue
        def ritd_res(popt):
            res = []
            latsp_mass_pznm_pz_z_kernel0 = []
            latsp_mass_pznm_pz_z_kernel1 = []
            latsp_mass_pznm_pz_z_kernel2 = []
            for i in range(0, len(itdz_fm)):
                latsp_mass_pznm_pz_z_kernel0 += [(itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][0])]
                latsp_mass_pznm_pz_z_kernel1 += [(itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][1])]
                latsp_mass_pznm_pz_z_kernel2 += [(itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][2])]
            def func(latsp_mass_pznm_pz_z_kernel):
                return fitfunc(latsp_mass_pznm_pz_z_kernel, popt)
            ritd_kernel0 = np.array(list(map(func,latsp_mass_pznm_pz_z_kernel0)))
            ritd_kernel1 = np.array(list(map(func,latsp_mass_pznm_pz_z_kernel1)))
            ritd_kernel2 = np.array(list(map(func,latsp_mass_pznm_pz_z_kernel2)))

            deviation = np.square(ritd_kernel0 - np.array(itdLat))
            err = np.square(np.array(itdLatErr)) + np.square((np.array(ritd_kernel1)-np.array(ritd_kernel2))/2)
            res = np.sqrt(deviation/err)
            return res

        #itd_IV = (itdlatsp_fm, itdmass_fm, itdpznm_fm, itdpz_fm, itdz_fm)  #latsp_mass_pznm_pz_z
        #zfmlist += [zfm]
        start0 = time.time()
        count = 0
        for imoms in range(0, len(moms)):

            iparams_moms = [moms[imoms][(i+1)*2] for i in range(0, int(len(moms[imoms])/2))]
            #start2 = time.time()
            for iparams_joint in joint_params:
                iparams_fit = iparams_moms+iparams_joint
                resvec = ritd_res(iparams_fit)
                #print(resvec)
                iparams_chisq = sum(np.square(resvec)) / (len(itdz_fm)-len(iparams_fit))
                iparams_chisq = [zskip, zmax, *params[imoms],*iparams_joint,iparams_chisq]
                #print(iparams_chisq)
                chisq_list += [iparams_chisq]
            count += 1
        print('   Timing0:',time.time()-start0, np.shape(itdlatsp_fm), np.shape(moms))
    return chisq_list

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### Read data here ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def read_rITD_fitdata(inputfile):

    readfile = open(inputfile,'r')
    commands = readfile.readlines()

    for index in range(0, len(commands)):

        line = commands[index].split()
        #print(line)

        if 'Ns' == line[0]:
            Ns = int(line[1])
        
        elif 'Nt' == line[0]:
            Nt = int(line[1])

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            print('  Lattice', str(Ns)+'c'+str(Nt)+',', 'a =', latsp, 'fm.')
        
        elif 'mass' in line[0]:
            mass = float(line[1])
            print('  Hardron mass:', mass, 'GeV')

        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            pxlist_fm = []
            pxlist_GeV = []
            for ipx in pxlist:
                pxlist_fm += [2*PI*ipx/(Ns*latsp)]
                pxlist_GeV += [round(2*PI*ipx/(Ns*latsp)/fmGeV,2)]
            print('  pzlist:',pxlist_GeV,'(GeV)')
            if len(pxlist) == 1:
                describe = describe.replace('pzup','pz'+str(pxmin))
            #print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            pznm_fm = 2*PI*pznm/(Ns*latsp)
            pznm_GeV = round(2*PI*pznm/(Ns*latsp)/fmGeV,2)
            print('  pznm :',pznm_GeV,'(GeV)')

        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

    rITD_params = [Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]

    # Read the data here
    rITD_mean = [] #rITD_mean[ipx][iz]
    rITD_err = [] #rITD_mean_err[ipx][iz]
    rITD_sample_read = [] #rITD_sample[ipx][iz][isample]
    rITD_sample = [] #rITD_sample[isample][ipx][iz]
    rITD_samp_lenMax = 0
    for ipx in pxlist:

        ipx_mean_file = mean_input_format.replace('*', str(ipx))
        ipx_mean_data = np.loadtxt(ipx_mean_file)
        ipx_rITD_mean = []
        ipx_rITD_err = []
        zlen = len(ipx_mean_data)
        for iz in range(0, zlen):
            ipx_rITD_mean += [ipx_mean_data[iz][1]]
            ipx_rITD_err += [ipx_mean_data[iz][2]]
        rITD_mean += [ipx_rITD_mean]
        rITD_err += [ipx_rITD_err]

        ipx_sample_file = sample_input_format.replace('*', str(ipx))
        ipx_rITD_sample = []
        for iz in range(0, zlen):
            iz_sample_file = ipx_sample_file.replace('#', str(iz))
            ipx_rITD_sample += [np.loadtxt(iz_sample_file)]
            if len(ipx_rITD_sample[iz]) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(ipx_rITD_sample[iz])
        rITD_sample_read += [ipx_rITD_sample]
    print('  rITD data shape:', np.shape(rITD_sample_read), ', sample maximal length:', rITD_samp_lenMax)

    for isamp in range(0,rITD_samp_lenMax):
        ipx_rITD_sample = []
        for ipx in range(0, len(pxlist_fm)):
            iz_rITD_sample = []
            for iz in range(0, zlen):
                rITD_samp_len = len(rITD_sample_read[ipx][iz])
                #iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp%rITD_samp_len]]
                if isamp < rITD_samp_len:
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp]]
                else:
                    ipick = random_uniform_int[-1][isamp]
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][ipick%rITD_samp_len]]
                    #iz_rITD_sample += [rITD_sample_read[ipx][iz][np.random.randint(0, rITD_samp_len)]]
            ipx_rITD_sample += [iz_rITD_sample]
        rITD_sample += [ipx_rITD_sample]

    print('  mean data:', np.shape(rITD_mean), ', sample data:', np.shape(rITD_sample))
    return rITD_params, rITD_err, rITD_sample


if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    #run_samp = int(sys.argv[2])

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    # read parameters
    scanlist = []
    dataset = []
    rITD_err = []
    params_list = [] # parameters for joint fit
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu' in line[0]:
            mu = float(line[1])
            print('Renormalization scale:', mu, 'GeV')
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'zskip' in line[0]:
            zskip = int(line[1])
            print('Skip zmin (z/a) =', zskip)
            describe = describe.replace('zmin', 'zmin'+str(zskip))

        elif 'zmax(fm)' in line[0]:
            zmax = float(line[1])
            print('Zmax =', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmax)+'fm')

        elif 'zmv(fm)' in line[0]:
            zmv = float(line[1])
            print('Z move =', zmv, 'fm')

        elif 'nmax' in line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)

        elif 'scan' in line[0]: # Please note, you better set the scan parameter orderly
            scan = [float(line[1])+float(line[3])*iscan for iscan in range(0, math.ceil((float(line[2])-float(line[1]))/float(line[3]))+1)]
            scanlist += [scan]
            print('Scan parameters setting:', '[',scan[0], ':', scan[-1], ']', 'of', len(scan), 'with grid', float(line[3]))

        elif 'jointparams' in line[0]:
            #print(line)
            params = [float(line[1])+float(line[3])*iscan for iscan in range(0, math.ceil((float(line[2])-float(line[1]))/float(line[3]))+1)]
            params_list += [params]
            print('Joint parameters setting:', '[',params[0], ':', params[-1], ']', 'of', len(params), 'with grid', float(line[3]))

        elif 'model' in line[0]:
            model = getattr(pdfs_model_list, line[1])
            print('Scan model:', line[1])

        elif 'ritd' == line[0]:
            ritd_matchFunc = line[1]
            print('rITD matching formula:', line[1])
            describe = describe.replace('rITD', 'rITD_'+line[2])

        elif 'data' == line[0]:
            print('Reading data:')
            # data_params=[Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]
            data_params, data_err, data_sample = read_rITD_fitdata(line[1])
            dataset += [[data_params, data_err, data_sample]]
            #print(np.shape(data_sample))
            rITD_err += [data_err]
            if len(data_sample) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(data_sample)

    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)

    # model parameters and moments collection
    params = []
    moms = []
    if len(scanlist) == 2:
        for param1 in scanlist[0]:
            for param2 in scanlist[1]:
                iparams = [param1, param2]
                params += [iparams]
                iparams_moms = []
                for imom in range(0, nmax+1):
                    #print(imom)
                    iparams_moms += [integrate.quad(lambda x: model(x, imom, *iparams), 0, 1)[0]]
                    #print(imom,integrate.quad(lambda x: model(x, imom, *iparams), 0, 1)[0])
                moms += [iparams_moms]
                #print(iparams,iparams_moms)
        print('\nCollected moments:',np.shape(moms), ', <1>:',moms[0][0])
    elif len(scanlist) == 3:
        for param1 in scanlist[0]:
            for param2 in scanlist[1]:
                for param3 in scanlist[2]:
                    iparams = [param1, param2, param3]
                    params += [iparams]
                    iparams_moms = []
                    for imom in range(0, nmax+1):
                        iparams_moms += [integrate.quåad(lambda x: model(x, imom, *iparams), 0, 1)[0]]
                    moms += [iparams_moms]
                    print(iparams_moms)
        print('\nCollected moments:',np.shape(moms), ', <1>:',moms[0][0])

    # joint fit parameters collection
    joint_params = []
    for i in range(0, len(params_list[0])):
        for j in range(0, len(params_list[1])):
            for k in range(0, len(params_list[2])):
                joint_params += [[params_list[0][i],params_list[1][j],params_list[2][k]]]
                #joint_params += [[params_list[2][k]]]
    #print(joint_params)
    print('Collected joint fit parameters:',np.shape(joint_params), '\n')


    pdf_x = [ix/100 for ix in range(1, 101)]
    # scan jackknife mean value
    def model_scan(rITD_sample, rITD_params):
        return  model_scan_loops(ritd_matchFunc, rITD_sample, rITD_err, rITD_params, zskip, zmax, params, moms, mu*fmGeV, nmax, joint_params)

    # scan jackknife mean value
    #print(np.shape(data_err))
    poptsample = [] #poptsample[isamp][iz][ipopt]
    savename = samplefolder + describe + '_bootssamples.txt'
    for isamp in range(0, rITD_samp_lenMax):
        print('  ',isamp,'/', rITD_samp_lenMax, 'samples')
        save_sample_name = samplefolder + describe + '_bootssample' + str(int(isamp)) + '.txt'
        rITD_sample = []
        rITD_params = []
        ipick = random_uniform_int[-2][isamp]
        for idata in dataset:
            rITD_params += [idata[0]]
            rITD_samp_len = len(idata[2])
            if isamp < rITD_samp_len:
                rITD_sample += [idata[2][isamp]]
            else:
                rITD_sample += [idata[2][ipick%rITD_samp_len]]
        chisq_list = model_scan(rITD_sample, rITD_params)
        chisq_list.sort(key=ListLast)
        np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
        #np.savetxt(save_sample_name, chisq_list, fmt='%.6e')
        poptsample += [chisq_list[0]]
    np.savetxt(savename, poptsample, fmt='%.6e')

    savename = outfolder + describe + '_popt.txt'
    popt_mean = [[poptsample[0][0], poptsample[0][1]]]
    for ipopt in range(2, len(poptsample[0])):
        ipopt_sample = [poptsample[isamp][ipopt] for isamp in range(0, len(poptsample))]
        ipopt_sample = sorted(ipopt_sample)
        popt_mean += [[ipopt_sample[mid], (ipopt_sample[high16] - ipopt_sample[low16])/2]]
    print(popt_mean)
    np.savetxt(savename, popt_mean , fmt='%.6e') 


