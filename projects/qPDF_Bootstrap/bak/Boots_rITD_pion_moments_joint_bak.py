#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
import inspect
from scipy.optimize import curve_fit
from statistics import *
from rITD_pion_ritdReal import *

random_uniform = np.loadtxt('random_uniform.txt')
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "rITD_pion_ritdReal_moments" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def moms_exp(exp_list):
    moms_list = []
    for i in range(0, len(exp_list)):
        moms_i = 0
        for j in range(i, len(exp_list)):
            moms_i += np.exp(-exp_list[j])
        moms_list += [moms_i]
    return moms_list

def make_NLO_ritd_Pzn_pznm_real(mu, nmax, contiN, CmassN, cutoff, k=0, kernel_list=[], fixN = []):
    def ritd_NLO_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z,*exp_list):
        latsp, mass, pznm, pz, z = latsp_mass_pznm_pz_z
        #print(exp_list)
        #print('\n\n@@@@@@\n\n',cutoff)
        moms = [1]
        bconti = [0 for i in range(0, int(nmax/2))]
        cmass = [0 for i in range(0, int(nmax/2))]
        #print('\n\n******\n\n',cutoff,exp_list)
        if len(exp_list)+len(fixN) >= int(nmax/2):
            #print('\n\n******\n\n',cutoff)
            for i in range(0, len(fixN)):
                moms += [0, fixN[i]]
            for i in range(len(fixN)-len(fixN), int(nmax/2)-len(fixN)):
                #print(i)
                moms += [0, np.exp(exp_list[i])]
            if contiN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(contiN/2)):
                    bconti[i-tmp] = exp_list[i]
            if CmassN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(CmassN/2)):
                    cmass[i-tmp] = exp_list[i]
            #print('\n\n******\n\n',exp_list,exp_list[i])
            if cutoff == 0:
                rcut = 0 # cutoff effect
            elif cutoff == 1:
                rcut = exp_list[-1]
            else:
                rcut = cutoff
            #print(moms,bconti,cmass,rcut)
        func = ritd_NLO_Pzn_pznm_real(latsp,z,pz,mass,mu,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list)
        return func
    return ritd_NLO_Pzn_pznm_real_constrained

def make_NLOEP_ritd_Pzn_pznm_real(mu, nmax, contiN, CmassN, cutoff, k=0, kernel_list=[], fixN = []):
    def ritd_NLOEP_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z,*exp_list):
        latsp, mass, pznm, pz, z = latsp_mass_pznm_pz_z
        #print(exp_list)
        moms = [1]
        bconti = [0 for i in range(0, int(nmax/2))]
        cmass = [0 for i in range(0, int(nmax/2))]
        if len(exp_list)+len(fixN) >= int(nmax/2):
            #print('\n\n******\n\n',cutoff)
            for i in range(0, len(fixN)):
                moms += [0, fixN[i]]
            for i in range(len(fixN)-len(fixN), int(nmax/2)-len(fixN)):
                #print(i)
                moms += [0, np.exp(exp_list[i])]
            if contiN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(contiN/2)):
                    bconti[i-tmp] = exp_list[i]
            if CmassN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(CmassN/2)):
                    cmass[i-tmp] = exp_list[i]
            #print('\n\n******\n\n',exp_list,exp_list[i])
            if cutoff == 0:
                rcut = 0 # cutoff effect
            elif cutoff == 1:
                rcut = exp_list[-1]
            else:
                rcut = cutoff
            #print(moms,bconti,cmass,rcut)
        func = ritd_NLOEP_Pzn_pznm_real(latsp,z,pz,mass,mu,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list)
        return func
    return ritd_NLOEP_Pzn_pznm_real_constrained

def make_NNLO_ritd_Pzn_pznm_real(mu, nmax, contiN, CmassN, cutoff, k=0, kernel_list=[], fixN = []):
    def ritd_NNLO_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z,*exp_list):
        latsp, mass, pznm, pz, z = latsp_mass_pznm_pz_z
        #print(exp_list)
        moms = [1]
        bconti = [0 for i in range(0, int(nmax/2))]
        cmass = [0 for i in range(0, int(nmax/2))]
        if len(exp_list)+len(fixN) >= int(nmax/2):
            #print('\n\n******\n\n',cutoff)
            for i in range(0, len(fixN)):
                moms += [0, fixN[i]]
            for i in range(len(fixN)-len(fixN), int(nmax/2)-len(fixN)):
                #print(i)
                moms += [0, np.exp(exp_list[i])]
            if contiN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(contiN/2)):
                    bconti[i-tmp] = exp_list[i]
            if CmassN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(CmassN/2)):
                    cmass[i-tmp] = exp_list[i]
            #print('\n\n******\n\n',exp_list,exp_list[i])
            if cutoff == 0:
                rcut = 0 # cutoff effect
            elif cutoff == 1:
                rcut = exp_list[-1]
            else:
                rcut = cutoff
            #print(moms,bconti,cmass,rcut)
        func = ritd_NNLO_Pzn_pznm_real(latsp,z,pz,mass,mu,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list)
        return func
    return ritd_NNLO_Pzn_pznm_real_constrained

def make_NNLOEP_ritd_Pzn_pznm_real(mu, nmax, contiN, CmassN, cutoff, k=0, kernel_list=[], fixN = []):
    def ritd_NNLOEP_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z,*exp_list):
        latsp, mass, pznm, pz, z = latsp_mass_pznm_pz_z
        #print(exp_list)
        moms = [1]
        bconti = [0 for i in range(0, int(nmax/2))]
        cmass = [0 for i in range(0, int(nmax/2))]
        if len(exp_list)+len(fixN) >= int(nmax/2):
            #print('\n\n******\n\n',cutoff)
            for i in range(0, len(fixN)):
                moms += [0, fixN[i]]
            for i in range(len(fixN)-len(fixN), int(nmax/2)-len(fixN)):
                #print(i) 
                moms += [0, np.exp(exp_list[i])]
            if contiN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(contiN/2)):
                    bconti[i-tmp] = exp_list[i]
            if CmassN != 0:
                tmp = i+1
                for i in range(tmp, i+1+int(CmassN/2)):
                    cmass[i-tmp] = exp_list[i]
            #print('\n\n******\n\n',exp_list,exp_list[i])
            if cutoff == 0:
                rcut = 0 # cutoff effect
            elif cutoff == 1:
                rcut = exp_list[-1]
            else:
                rcut = cutoff
            #print(moms,bconti,cmass,rcut)
        func = ritd_NNLOEP_Pzn_pznm_real(latsp,z,pz,mass,mu,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list)
        return func
    return ritd_NNLOEP_Pzn_pznm_real_constrained

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### Read data here ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def read_rITD_fitdata(inputfile):

    readfile = open(inputfile,'r')
    commands = readfile.readlines()

    for index in range(0, len(commands)):

        line = commands[index].split()
        #print(line)

        if 'Ns' == line[0]:
            Ns = int(line[1])
        
        elif 'Nt' == line[0]:
            Nt = int(line[1])

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            print('  Lattice', str(Ns)+'c'+str(Nt)+',', 'a =', latsp, 'fm.')
        
        elif 'mass' in line[0]:
            mass = float(line[1])
            print('  Hardron mass:', mass, 'GeV')

        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            pxlist_fm = []
            pxlist_GeV = []
            for ipx in pxlist:
                pxlist_fm += [2*PI*ipx/(Ns*latsp)]
                pxlist_GeV += [round(2*PI*ipx/(Ns*latsp)/fmGeV,2)]
            print('  pzlist:',pxlist_GeV,'(GeV)')
            if len(pxlist) == 1:
                describe = describe.replace('pzup','pz'+str(pxmin))
            #print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            if pznm == 0:
                pznm = 0.0001
            pznm_fm = 2*PI*pznm/(Ns*latsp)
            pznm_GeV = round(2*PI*pznm/(Ns*latsp)/fmGeV,2)
            print('  pznm :',pznm_GeV,'(GeV)')

        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

    rITD_params = [Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]

    # Read the data here
    rITD_mean = [] #rITD_mean[ipx][iz]
    rITD_err = [] #rITD_mean_err[ipx][iz]
    rITD_sample_read = [] #rITD_sample[ipx][iz][isample]
    rITD_sample = [] #rITD_sample[isample][ipx][iz]
    rITD_samp_lenMax = 0
    for ipx in pxlist:

        ipx_mean_file = mean_input_format.replace('*', str(ipx))
        ipx_mean_data = np.loadtxt(ipx_mean_file)
        ipx_rITD_mean = []
        ipx_rITD_err = []
        zlen = len(ipx_mean_data)
        for iz in range(0, zlen):
            ipx_rITD_mean += [ipx_mean_data[iz][1]]
            ipx_rITD_err += [ipx_mean_data[iz][2]]
        rITD_mean += [ipx_rITD_mean]
        rITD_err += [ipx_rITD_err]

        ipx_sample_file = sample_input_format.replace('*', str(ipx))
        ipx_rITD_sample = []
        for iz in range(0, zlen):
            iz_sample_file = ipx_sample_file.replace('#', str(iz))
            ipx_rITD_sample += [np.loadtxt(iz_sample_file)]
            if len(ipx_rITD_sample[iz]) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(ipx_rITD_sample[iz])
        rITD_sample_read += [ipx_rITD_sample]
    print('  rITD data shape:', np.shape(rITD_sample_read), ', sample maximal length:', rITD_samp_lenMax)

    for isamp in range(0,rITD_samp_lenMax):
        ipx_rITD_sample = []
        for ipx in range(0, len(pxlist_fm)):
            iz_rITD_sample = []
            for iz in range(0, zlen):
                rITD_samp_len = len(rITD_sample_read[ipx][iz])
                #iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp%rITD_samp_len]]
                if isamp < rITD_samp_len:
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp]]
                else:
                    ipick = random_uniform_int[-1][isamp]
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][ipick%rITD_samp_len]]
                    #iz_rITD_sample += [rITD_sample_read[ipx][iz][np.random.randint(0, rITD_samp_len)]]
            ipx_rITD_sample += [iz_rITD_sample]
        rITD_sample += [ipx_rITD_sample]

    print('  mean data:', np.shape(rITD_mean), ', sample data:', np.shape(rITD_sample))
    return rITD_params, rITD_err, rITD_sample
'''
def err_modify(kernel, rITD_err, zlist):
    for ilat in range(0, len(rITD_err)):
        for ipz in range(0, len(rITD_err[ilat])):
            for iz in range(0, len(rITD_err[ilat][ipz])):
'''


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### fitting procedure ########################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def rITD_fit(fitfunc_name, rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu, nmax, fixN = [], cutoff=0, contiN=0, CmassN=0, zcombine=True):

    #mu = mu*fmGeV

    if 'ritdNLO' == fitfunc_name:
        fitfunc = make_NLO_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
    elif 'ritdNLOEP' == fitfunc_name:
        fitfunc = make_NLOEP_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
    elif 'ritdNNLO' == fitfunc_name:
        fitfunc = make_NNLO_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
    elif 'ritdNNLOEP' == fitfunc_name:
        fitfunc = make_NNLOEP_ritd_Pzn_pznm_real(mu, nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)

    poptlist = []
    zfmlist = []
    zfm = zmv
    for zfm in np.arange(zfm, zmax+zmv/2, zmv):

        # fit y
        itdLat = []
        itdLatErr = []
        
        # fit x: (latsp,mass,pznm,pz,z)
        itdlatsp_fm = []
        itdmass_fm = []
        itdpznm_fm = []
        itdpz_fm = []
        itdz_fm = []

        for ilat in range(0, len(rITD_params)):

            ilat_rITD = rITD_sample[ilat]
            ilat_rITD_err = rITD_err[ilat]

            ilat_latsp = rITD_params[ilat][2]
            ilat_mass_fm = rITD_params[ilat][3] * fmGeV
            ilat_pznm = rITD_params[ilat][4]
            ilat_pzlist = rITD_params[ilat][-1]

            for ipx in range(0, len(ilat_pzlist)):

                if zcombine == True:
                    zmin = zskip
                else:
                    zmin =  int(zfm/ilat_latsp)

                for iz in range(zmin, int(zfm/ilat_latsp)+1):

                    itdLat += [ilat_rITD[ipx][iz]]
                    itdLatErr += [ilat_rITD_err[ipx][iz]]

                    itdlatsp_fm += [ilat_latsp]
                    itdmass_fm += [ilat_mass_fm]
                    itdpznm_fm += [ilat_pznm]
                    itdpz_fm += [ilat_pzlist[ipx]]
                    itdz_fm += [iz*ilat_latsp]
        #print(itdLat)
        if len(itdLat) < int(nmax/2):
            continue

        itd_IV = (itdlatsp_fm, itdmass_fm, itdpznm_fm, itdpz_fm, itdz_fm)  #latsp_mass_pznm_pz_z
        zfmlist += [zfm]
        for i in range(0, 10):
            try:
                poptstart = [np.random.normal(np.log(0.2/(j+1)), abs(0.1*(np.log(0.2/(j+1))))) for j in range(len(fixN), int(nmax/2))]
                #poptstart += [0.1 for j in range(len(fixN), int(nmax/2)-1)]
                #print(cutoff)
                if cutoff != 0:
                    poptstart += [0.1]
                if contiN != 0:
                    for i in range(0, int(contiN/2)):
                        poptstart += [0.1]
                if CmassN != 0:
                    for i in range(0, int(CmassN/2)):
                        poptstart += [0.1]
                popt, popv = curve_fit(fitfunc, itd_IV, itdLat, p0=poptstart, sigma=itdLatErr)
                #print(iz, popt,moms_exp(popt))
                break
            except:
                traceback.print_exc()
                pass
        xylist = [[(itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i]),itdLat[i],itdLatErr[i]] for i in range(0, len(itdz_fm))]
        chisq = (chisq_value_stderr(xylist, fitfunc, popt))/(len(itdz_fm)-len(popt))
        popt = fixN + list(popt) + [chisq]
        for j in range(len(fixN), int(nmax/2)):
            popt[j] = np.exp(popt[j])
        poptlist += [popt]
        #print(np.shape(itd_IV),len(itdz_fm)-len(popt),popt)

    return poptlist, zfmlist

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    # read parameters
    fitfunc = []
    rITD_err = []
    poptguess = []
    fixN = []
    dataset = []
    cutoff = False
    conti = 0
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu' in line[0]:
            mu = float(line[1])
            print('Renormalization scale:', mu, 'GeV')
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'zskip' in line[0]:
            zskip = int(line[1])
            print('Skip zmin (z/a) =', zskip)
            describe = describe.replace('zmin', 'zmin'+str(zskip))

        elif 'zmax(fm)' in line[0]:
            zmax = float(line[1])
            print('Zmax =', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmax))

        elif 'zmv(fm)' in line[0]:
            zmv = float(line[1])
            print('Z move =', zmv, 'fm')

        elif 'nmax' in line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)

        elif 'ritd' == line[0]:
            fitfunc += [line[1]]

        elif 'zCombine' in line[0]:
            if 'y' in line[1]:
                zcombine = True
                print('This is a combine fit of a range of z.')
            else:
                zcombine = False
                print('This is a fit for each z.')

        elif 'fixN' in line[0]:
            fixN = [float(line[i]) for i in range(1, len(line))]
            print('rITD fixed moments:', fixN)

        elif 'cutoff' == line[0]:
            if line[1] == 'y':
                cutoff = [1,1,1,1,1]
                describe = describe + '_rcut'
            elif line[1] == 'n':
                cutoff = [0,0,0,0,0]
            else:
                cutoff = [float(line[1]) for i in range(1, len(line))]
                print('Cutoff parameter r=',cutoff)

        elif 'continuum' == line[0]:
            contiN = int(line[1])
            describe = describe + '_contiN' + str(contiN)

        elif 'masscorrection' == line[0]:
            CmassN = int(line[1])
            describe = describe + '_CmassN' + str(CmassN)

        elif 'data' == line[0]:
            print('Reading data:')
            # data_params=[Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]
            data_params, data_err, data_sample = read_rITD_fitdata(line[1])
            dataset += [[data_params, data_err, data_sample]]
            #print(np.shape(data_sample))
            rITD_err += [data_err]
            if len(data_sample) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(data_sample)

    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)

    for ifit in range(0, len(fitfunc)):

        moms_list = []
        out_name = outfolder+describe.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit])) + '.txt'
        print('\nGonna save in:',out_name)

        poptsample = [] #poptsample[isamp][iz][ipopt]
        for isamp in range(0, rITD_samp_lenMax):
            rITD_sample = []
            rITD_params = []
            ipick = random_uniform_int[-2][isamp]
            for idata in dataset:
                rITD_params += [idata[0]]
                rITD_samp_len = len(idata[2])
                if isamp < rITD_samp_len:
                    rITD_sample += [idata[2][isamp]]
                else:
                    rITD_sample += [idata[2][ipick%rITD_samp_len]]
            popt, zfmlist = rITD_fit(fitfunc[ifit], rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu*fmGeV, nmax, fixN, cutoff[ifit], contiN, CmassN, zcombine)
            poptsample += [popt]

        for isamp in range(0, rITD_samp_lenMax):
            poptsample_save = []
            sample_name = samplefolder+describe.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit]))+'_bootssample'+str(int(isamp))+'.txt'
            for iz in range(0, len(zfmlist)):
                poptsample_save += [[zskip, zfmlist[iz]] + poptsample[isamp][iz]]
            #print(np.shape(poptsample[isamp]),poptsample[0])
            np.savetxt(sample_name, poptsample_save)

        for iz in range(0, len(zfmlist)):
            iz_moms = [zskip, zfmlist[iz]]
            for ipopt in range(0, len(poptsample[0][iz])):
                ipopt = [poptsample[isamp][iz][ipopt] for isamp in range(0, rITD_samp_lenMax)]
                ipopt = sorted(ipopt)
                iz_moms += [ipopt[mid], (ipopt[high16] - ipopt[low16])/2]
            np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
            #print(iz_moms)
            moms_list += [iz_moms]
        #print(out_name)
        np.savetxt(out_name, moms_list, fmt='%.4e')

        
