#!/usr/bin/env python3

import os
import sys
import matplotlib
import numpy as np
from matplotlib import pyplot as plt

from modelfuncs import *
from tools import *
from tools2 import *
from statistics import *
from c3c2tools import *
from Jackk_c2pt import *
from Jackk_c3pt import *

if __name__ == "__main__":
        
    #------------------------------------------------#
    # transform txt data format to csv data format
    #------------------------------------------------#
    if sys.argv[1] == 'txtcsv':
        txtcsvRI(sys.argv[2])
    
    #--------------------------------------------------------#
    # this procedure built with jackknife method
    # the command file should be format as:
    # output_file_name
    # dataset.csv f/nf xminl xminr xmax blocksize
    # fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
    # fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
    # ...
    # fitdescribe(full/fixE0/fixSPE0E1) fitfuncN [paramguessN]
    #--------------------------------------------------------#

    if sys.argv[1] == 'jackc2fit':

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]
        line = commands[1].split()
        dataset = csvtolist(line[0])
        #t0_data = dataset[0]
        t0_data = []

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        samplename = samplefolder + describe + '_jackksample'

        print('*\n*\n*\n*\n*\n*\n*\n*\njob describe:', describe)
        print('\nconfiguration number:', len(dataset[-1])-1)

        # read the fit range xmin and xmax
        xminl = int(line[2])
        xminr = int(line[3])
        xmax = int(line[4])
        blocksize = int(line[5])
        print('xmin range:',xminl,xminr, '\tblocksize:',blocksize)

        # fold or not fold the data
        print(line[0],np.shape(dataset))
        if line[1] == 'f':
                dataset = folddat(dataset)
                print('\n@@@ Data is folded!!!')
        elif line[1] == 'nf':
                print('\n@@@ Data is unfolded!!!')
        #print(dataset)
        print('t number:',dataset[-1][0]+1,'\n')
        #print(line[0].replace('c2pt_ab_pdf','c2pt_ab_fold'))
        #savecsv(dataset,line[0].replace('c2pt_ab_pdf','c2pt_ab_fold'))

        # collect the fit model and fit parameters
        fitparams = []
        for index in range(2,len(commands)):
                line = commands[index].split()
                fitparams += [list(map(str,line))]
        print('\nfit parameters:',fitparams)

        # fit proceduce
        popt = []
        popt_aicc = []
        for line in fitparams:
                if 'prior' not in line[0]:
                        popt += [[]]
        for xmin in range(xminl,xminr+1):
                
                print(xmin, ' in ', xminl, '~', xminr)

                xmin_samplename = samplename + '_tmin' + str(xmin)
                #xmin_samplename = None
                xmin_sample_name = samplefolder + '/' + describe + '_tmin' + str(xmin)
                xmin_data = dataset[xmin:xmax+1]
                #xmin_data = dataset[xmin:xmin+10]
                print(np.shape(xmin_data),[xmin_data[i][0] for i in range(0, len(xmin_data))])
                xmin_popt = jackk_c2ptfit(xmin_data, fitparams, samplename=xmin_samplename, blocksize=blocksize, c2pt_t0=t0_data)

                for imodel in range(0, len(popt)):
                        popt[imodel] += [[xmin, xmax] + xmin_popt[imodel]]
                
                #pick best model fit by aicc
                imodel_aiccmin = 0
                lenmin = 100
                for imodel in range(0, len(xmin_popt)):
                        #print(xmin_popt[imodel])
                        if xmin_popt[imodel][-2] < xmin_popt[imodel_aiccmin][-2]:
                                imodel_aiccmin = imodel
                        if len(xmin_popt[imodel]) < lenmin:
                                lenmin = len(xmin_popt[imodel])
                popt_aicc += [[xmin, xmax] + xmin_popt[imodel_aiccmin][0:lenmin]]

        # save the fit results
        if 'prior' in fitparams[0][0]:
                fitparams = fitparams[1:]
        for imodel in range(0,len(popt)):
                modelname = fitparams[imodel][1]
                print(modelname)
                outfilename = resultfolder + '/' + describe + '_' + modelname + '.txt'
                outfile = open(outfilename,'w')
                np.savetxt(outfile, popt[imodel], fmt='%.6e')
                outfile.close()

        # save the aicc picked model:
        outfilename = resultfolder + '/' + describe + '_aicc.txt'
        #outfile = open(outfilename,'w')
        #np.savetxt(outfile, popt_aicc, fmt='%.6e')
        #outfile.close()




    #--------------------------------------------------------------#
    # This is a linear fitting procedure for 3-point function
    # this procedure is built with bootstrap method
    # the command file should be format as:
    # output_file_name p2 p3
    # tslist
    # z tmin(for c2pt fitting to get E0 and E1) tmax
    # output folder
    # c2pt.h5
    # tsc3pt.h5 
    # ...
    # SPc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
    # SPc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
    # ...
    # SSc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc1 [paramguess1]
    # SSc2 fitdescribe(full/fixE0/fixSPE0E1) fitfunc2 [paramguess2]
    # ...
    # SSc3 fitdescribe(full/fixSSA0E0E1) fitfunc1 [paramguess1]
    # ...
    #--------------------------------------------------------------#
    if sys.argv[1] == 'jackc3fit':
    
        random_number = [None]
        
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()

        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results'   # default, mkdir at the input file folder
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples'
        mkdir(samplefolder)
        
        # read describe of this command file
        describe = (commands[0].split())[0]
        blocksize = int((commands[0].split())[1])
        taumin = int((commands[0].split())[2])
        describe = describe + str(taumin)
        print('\nblocksize:', blocksize, ', taumin:', taumin, ', job describe:', describe)
        foldz = (commands[0].split())[3]
        if 'imag' in describe:
                datatype = 'imag'
        else:
                datatype = 'real'
        foldt = (commands[0].split())[4]
            
        # read time seperation for the following c3pt
        zmin = int((commands[1].split())[1])
        zmax = int((commands[1].split())[2])
        zlist = [z for z in range(zmin,zmax+1)]
        print('\nz list:', zlist)
                
        # read time seperation for the following c3pt
        tslist = list(map(int,(commands[2].split())[1:]))
        print('\nts list:', tslist)
        
        # read c2pt fitting tmin and tmax
        tmin = int((commands[3].split())[1])
        tmax = int((commands[3].split())[2])
        print('c2pt fit tmin & tmax: ', tmin, tmax)

        # read and reformat the c2pt datas, c2data[t]: [t, conf...]
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'SSc2pt':
                        SSc2file = line[1]
        SSc2filedata = csvtolist(SSc2file)
        print('SSc2filedata shape:',np.shape(SSc2filedata))
        SSratioc2 = []
        for ts in tslist:
                SSratioc2 += [SSc2filedata[ts]]
        if foldt == 'ft':
                print('@@@ c2pt fit data is folded !!!')
                SSc2filedata = folddat(SSc2filedata)
                print('SSc2filedata shape(after folding):',np.shape(SSc2filedata))
        SSc2data = SSc2filedata[tmin:tmax+1]
        print('\nconfiguration number:', len(SSc2data[0])-1)
        
        # read and reformat the c3pt datas, c3data[z][ts][tau]:  [tau, conf...]
        # read the c3pt data
        if foldz == 'fz':
                print('\n@@@@ Z data will be folded by positive and negetive direction.!')
                foldz = True
        else:
                foldz = False
        c3filenames = []
        print('\nc3pt files:')
        for index in range(0,len(commands)):
                line = commands[index].split()
                if line[0] == 'c3pt':
                        c3filename = line[1]
                        c3filenames += [c3filename]
                        print(c3filename)
        inputfile.close()
        c3data = []
        for z in zlist:
                zc3data = []
                for i in range(0,len(tslist)):
                        c3filename = c3filenames[i].replace('*', str(z))
                        i_zc3data_p = csvtolist(c3filename)
                        if foldz == True and z != 0:
                                c3filename = c3filenames[i].replace('*','-'+str(z))
                                i_zc3data_n = csvtolist(c3filename)
                                i_zc3data = folddat_z(i_zc3data_p, i_zc3data_n, datatype)
                        elif foldz == True and z == 0:
                                i_zc3data_n = i_zc3data_p
                                i_zc3data = folddat_z(i_zc3data_p, i_zc3data_n, datatype)
                        else:
                                i_zc3data = i_zc3data_p
                        zc3data += [i_zc3data]
                c3data += [zc3data]

        # read SS c2pt fitting parameters
        c2ptfit = aiccfit
        SSc2ptparams = []
        SSc3ptparams = []
        for index in range(2,len(commands)):
                line = commands[index].split()
                if line[0] == 'SSc2':
                        SSc2ptparams += [list(map(str,line[1:]))]
                elif line[0] == 'SSc3':
                        SSc3ptparams += [list(map(str,line[1:]))]
        print(SSc3ptparams)
        if len(SSc2ptparams) == 0 or SSc2ptparams[0][0] == 'None':
                c2ptfit = None

        ratioerr = []
        ratiomean = []
        meantype = 'meanerr'
        jointc2 = [None]
        if 'joint' in SSc3ptparams[0][1]:
                print("joint fitting.")
                jointc2 = []
                tsmin = tslist[0]
                tsmax = tmax
                for ts in range(tsmin, tsmax+1):
                        jointc2 += [SSc2filedata[ts]]
        if 'ratio' in SSc3ptparams[0][1]:
                ratio = True
        else:
                ratio = False

        print('\nStart to collect standard error by jackknife method ...')
        for z in range(0,len(zlist)):
                ratioerr += [c3fitratio(tslist,c3data[z],SSratioc2,meanonly=meantype,jointc2=jointc2,ratio=ratio, taumin = taumin,blocksize=blocksize)]
        print('Error collecting done! \n')

        for i in range(0, len(ratioerr[0])):
                print(tslist[i], "ts data shape:", np.shape(ratioerr[0][i]))
        print("\n")

        # make the output file & sample file name
        #if c2ptfit == aiccfit:
        if c2ptfit == 'aiccfit':
                outfilename = outfolder + '/' + describe + '_' + str(tmin) + 't' + str(tmax) + '.txt'
                samplename = samplefolder + '/' + describe + '_' + str(tmin) + 't' + str(tmax) + '_jackk'
        else:
                outfilename = outfolder + '/' + describe + '.txt'
                samplename = samplefolder + '/' + describe  + '_jackk'       
        
        popt = jackk_c3ptfit(c2ptfit,c3ptfit,SSc2data,SSc2ptparams,c3data,SSratioc2,ratioerr,SSc3ptparams,tslist,zlist,taumin=taumin,samplename=samplename,blocksize=blocksize)
        outfile = open(outfilename,'w')
        np.savetxt(outfile,popt,fmt='%.6e')
        outfile.close()
