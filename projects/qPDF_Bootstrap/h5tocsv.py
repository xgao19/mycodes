#!/usr/bin/env python3

import h5py
import csv
import sys
import numpy as np
from tools import *
from tools2 import *

def combine_h5_gpd(filename):
    dir_list = ['/Volumes/XIANG_2/0study/lattice/data/l64c64a076pi/l64c64a076pi_g8/c3pt_k2_pz1/ts6-8'\
        ,'/Volumes/XIANG_2/0study/lattice/data/l64c64a076pi/l64c64a076pi_g8/c3pt_k2_pz1/ts20']
    savefile = '/Volumes/XIANG_2/0study/lattice/data/l64c64a076pi/l64c64a076pi_g8/c3pt_k2_pz1'
    tslist1 = [6,8,10]
    tslist2 = [20]
    tslist = [tslist1] + [tslist2]
    zlist = [i for i in range(-32, 33)]

    input_h5_list = []
    for idir in dir_list:
        input_h5_list += [h5py.File(idir+'/'+filename, 'r')]

    comb_h5 = h5py.File(savefile+'/'+filename, 'w')
    for i in range(0, len(tslist)):
        for its in tslist[i]:
            #print(its)
            its_comb_h5 = comb_h5.create_group('dt'+str(its))
            for ix in zlist:
                #print(ix)
                read_keys = 'dt' + str(its) + '/X'  + str(ix)
                read_dataset = list(input_h5_list[i][read_keys])
                its_comb_h5.create_dataset('X'+str(ix),data=read_dataset)
    comb_h5.close()

    for i_read_h5 in input_h5_list:
        i_read_h5.close()

def ave_h5_gpd(filename):
    dir_list = ['src000_a_c3pt/g0','src001_a_c3pt/g0','src010_a_c3pt/g0','src100_a_c3pt/g0'\
        ,'src110_a_c3pt/g0','src011_a_c3pt/g0','src101_a_c3pt/g0','src111_a_c3pt/g0','srcT000+001_a_c3pt/g0']
    savefile = 'gpd_a_set1-9/'
    tslist = [6,8,10]
    zlist = [i for i in range(-32, 33)]

    input_h5_list = []
    for idir in dir_list:
        input_h5_list += [h5py.File(idir+'/'+filename, 'r')]

    ave_h5 = h5py.File(savefile+filename, 'w')
    for its in tslist:
        its_ave_h5 = ave_h5.create_group('dt'+str(its))
        for ix in zlist:
            read_keys = 'dt' + str(its) + '/X'  + str(ix)
            read_dataset = []
            for i_read_h5 in input_h5_list:
                read_dataset += [list(i_read_h5[read_keys])]
            ave_dataset = ave_2d_lists(read_dataset)
            its_ave_h5.create_dataset('X'+str(ix),data=ave_dataset)
    ave_h5.close()

    for i_read_h5 in input_h5_list:
        i_read_h5.close()

def ToUnpo_h5_gpd(proton_posSxplus_U):

    print('\nposSxplus_U:', proton_posSxplus_U.split('/')[-1])

    savefile = proton_posSxplus_U.replace('posSxplus.U','Unpos.U-D')

    proton_posSxminus_D = h5py.File(proton_posSxplus_U.replace('posSxplus.U','posSxminus.D'), 'r')
    proton_posSxminus_U = h5py.File(proton_posSxplus_U.replace('posSxplus.U','posSxminus.U'), 'r')
    proton_posSxplus_D = h5py.File(proton_posSxplus_U.replace('posSxplus.U','posSxplus.D'), 'r')
    proton_posSxplus_U = h5py.File(proton_posSxplus_U, 'r')

    tslist = [6,8,10,12]
    zlist = [i for i in range(-32,33)]

    proton_Unpos = h5py.File(savefile, 'w')
    for its in tslist:
        its_proton_Unpos = proton_Unpos.create_group('dt'+str(its))
        for ix in zlist:
            read_keys = 'dt' + str(its) + '/X'  + str(ix)
            read_dataset = [list(proton_posSxplus_U[read_keys]),list(proton_posSxplus_D[read_keys]),list(proton_posSxminus_U[read_keys]),list(proton_posSxminus_D[read_keys])]
            ave_dataset = ave_2d_lists(read_dataset,[2,-2,2,-2])
            its_proton_Unpos.create_dataset('X'+str(ix),data=ave_dataset)
    proton_Unpos.close()

    proton_posSxminus_D.close()
    proton_posSxminus_U.close()
    proton_posSxplus_D.close()
    proton_posSxplus_U.close()

def ToHeli_h5_gpd(proton_posSxplus_U):

    print('\nposSxplus_U:', proton_posSxplus_U.split('/')[-1])

    savefile = proton_posSxplus_U.replace('posSxplus.U','Heli.U-D')

    proton_posSxminus_D = h5py.File(proton_posSxplus_U.replace('posSxplus.U','posSxminus.D'), 'r')
    proton_posSxminus_U = h5py.File(proton_posSxplus_U.replace('posSxplus.U','posSxminus.U'), 'r')
    proton_posSxplus_D = h5py.File(proton_posSxplus_U.replace('posSxplus.U','posSxplus.D'), 'r')
    proton_posSxplus_U = h5py.File(proton_posSxplus_U, 'r')

    tslist = [6,8,10,12]
    zlist = [i for i in range(-32,33)]

    proton_Unpos = h5py.File(savefile, 'w')
    for its in tslist:
        its_proton_Unpos = proton_Unpos.create_group('dt'+str(its))
        for ix in zlist:
            read_keys = 'dt' + str(its) + '/X'  + str(ix)
            read_dataset = [list(proton_posSxplus_U[read_keys]),list(proton_posSxplus_D[read_keys]),list(proton_posSxminus_U[read_keys]),list(proton_posSxminus_D[read_keys])]
            ave_dataset = ave_2d_lists(read_dataset,[4,-4,-4,4])
            its_proton_Unpos.create_dataset('X'+str(ix),data=ave_dataset)
    proton_Unpos.close()

    proton_posSxminus_D.close()
    proton_posSxminus_U.close()
    proton_posSxplus_D.close()
    proton_posSxplus_U.close()

def ToTrans_h5_gpd(proton_posSzplus_U_g5):

    print('\nposSzplus_U_g5:', proton_posSzplus_U_g5.split('/')[-1])

    savefile = proton_posSzplus_U_g5.replace('posSzplus.U.g5','Trans.U-D.g5')

    proton_posSzplus_D_g5 = h5py.File(proton_posSzplus_U_g5.replace('posSzplus.U.g5','posSzplus.D.g5'), 'r')
    proton_posSzplus_U_g5 = h5py.File(proton_posSzplus_U_g5, 'r')

    tslist = [6,8,10,12]
    zlist = [i for i in range(-32,33)]
    
    proton_Unpos = h5py.File(savefile, 'w')
    for its in tslist:
        its_proton_Unpos = proton_Unpos.create_group('dt'+str(its))
        for ix in zlist:
            read_keys = 'dt' + str(its) + '/X'  + str(ix)
            read_dataset = [list(proton_posSzplus_U_g5[read_keys]),list(proton_posSzplus_D_g5[read_keys])]
            ave_dataset = ave_2d_lists(read_dataset,[2,-2])
            its_proton_Unpos.create_dataset('X'+str(ix),data=ave_dataset)
    proton_Unpos.close()

    proton_posSzplus_U_g5.close()
    proton_posSzplus_D_g5.close()


def refine_dwf_h5(commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    describe = (commands[0].split())[0]
    print('\n Job decribe:', describe)
    for index in range(1, len(commands)):
        line = commands[index].split()

        if line[0] == 'cfgfile':
            cfglist = np.loadtxt(line[1],int)
            print('\n  cfg number:', len(cfglist))

        elif line[0] == 'in':
            in_name = line[1]
            print('  Read data as:', in_name)

        elif line[0] == 'out':
            outfolder = line[1]
            outformat = outfolder+'/'+describe+'.h5'
            print('  Save data as:', outformat)

        elif line[0] == 'pzlist':
            pzlist = [int(line[i]) for i in range(1, len(line))]
            print('  pzlist:', pzlist)

        elif line[0] == 'hyp':
            hyplist = [int(line[i]) for i in range(1, len(line))]
            print('  hyplist:', hyplist)

        elif line[0] == 'gm':
            gmlist = [line[i] for i in range(1, len(line))]
            print('  gmlist:', gmlist)

        elif line[0] == 'tslist':
            tslist = [int(line[i]) for i in range(1, len(line))]
            print('  tslist:', tslist)

        elif line[0] == 'zrange':
            zlist = [iz for iz in range(int(line[1]), int(line[2])+1)]
            print('  zlist:', zlist)
            outformat = outformat.replace('.Zrange','.Z'+str(zlist[0])+'to'+str(zlist[-1]))
    
    inputfile.close()
    print('\n')

    for ihyp in range(0, len(hyplist)):
        ihyp_outname = outformat.replace('.hyp', '.hyp'+str(hyplist[ihyp]))
        for igm in range(0, len(gmlist)):
            igm_outname = ihyp_outname.replace('.gm', '.'+gmlist[igm])
            for ip in pzlist:
                ip_outname = igm_outname.replace('.PZ', '.PZ'+str(ip))
                ip_in_name = in_name.replace('.mom','.mom'+str(ip))
                ip_data = []
                print(' Collecting data: HYP =', hyplist[ihyp],'gm = ', gmlist[igm],' Pz =', ip)
                for its in tslist:
                    its_in_name = ip_in_name.replace('.tsep','.tsep'+str(its))
                    ip_its_data = []
                    temp_data_flat = []
                    for icfg in cfglist:
                        icfg_in_name = its_in_name.replace('.cfg','.cfg'+str(icfg))
                        
                        icfg_data_flat = []
                        if 'u-d' not in outformat:
                            for dataline in open(icfg_in_name,'r'):
                                dataline = dataline.split()
                                icfg_data_flat += [complex(float(dataline[4]),float(dataline[5]))]
                        elif 'u-d' in outformat:
                            icfg_data_flat_u = []
                            for dataline in open(icfg_in_name,'r'):
                                dataline = dataline.split()
                                icfg_data_flat_u += [complex(float(dataline[4]),float(dataline[5]))]
                            icfg_data_flat_d = []
                            for dataline in open(icfg_in_name.replace('proton_u','proton_d'),'r'):
                                dataline = dataline.split()
                                icfg_data_flat_d += [complex(float(dataline[4]),float(dataline[5]))]
                            for i in range(0, len(icfg_data_flat_u)):
                                #print(i,icfg_data_flat_u[i],icfg_data_flat_d[i],icfg_data_flat_u[i]-icfg_data_flat_d[i])
                                icfg_data_flat += [icfg_data_flat_u[i]-icfg_data_flat_d[i]]

                        # take ihyp data out
                        hyp_count = int(len(icfg_data_flat)/len(hyplist))
                        icfg_data_flat = icfg_data_flat[ihyp*hyp_count:(ihyp+1)*hyp_count]
                        # take igm data out
                        gm_count = int(len(icfg_data_flat)/len(gmlist))
                        icfg_data_flat = icfg_data_flat[igm*gm_count:(igm+1)*gm_count]

                        temp_data_flat += [icfg_data_flat]

                    print('   Data (flat), tsep =', its, ', shape:',np.shape(temp_data_flat))
                    #print('   Data (flat), tsep =', its, ', tail:',temp_data_flat[-1][-1])
                    # Re-arrange data
                    for iz in range(0, len(zlist)):
                        ip_its_iz_data = []
                        for itau in range(0, its+1):
                            ip_its_iz_itau_data = [itau]
                            for icfg in range(0, len(cfglist)):
                                ip_its_iz_itau_data += [temp_data_flat[icfg][itau*len(zlist)+iz]]
                            ip_its_iz_data += [ip_its_iz_itau_data]
                        ip_its_data += [ip_its_iz_data]
                    print('   Data (arranged), tsep =', its, ', shape:',np.shape(ip_its_data))
                    #print('   Data (arranged), tsep =', its, ', tail:',ip_its_data[0][0][1],ip_its_data[0][4][1])
                    ip_data += [ip_its_data]
                print(' Collecting done with shape:',np.shape(ip_data))
                ip_h5 = h5py.File(ip_outname, 'w')
                #print(ip_data[0][0][0][3])
                for its in range(0,len(tslist)):
                    its_h5_group = ip_h5.create_group('dt'+str(tslist[its]))
                    for iz in range(0, len(zlist)):
                        its_h5_group.create_dataset('Z'+str(zlist[iz]),data=ip_data[its][iz])
                ip_h5.close()
                print(' Collected data saved in:', ip_outname, '\n')



                    
def DAcsvtoh5(commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    describe = (commands[0].split())[0]
    in_name = describe
    out_name = describe

    bxplist = []
    for index in range(1, len(commands)):
        line = commands[index].split()

        if line[0] == 'in':
            infolder = line[1]
            print('\n@@@Read data in', infolder)
        
        elif line[0] == 'out':
            outfolder = line[1]
            print('@@@Save data in', outfolder, '\n')

        elif line[0] == 'gm':
            gmlist = [line[i] for i in range(1, len(line))]

        elif line[0] == 'bxp':
            bxplist += [[line[1],line[2],line[3]]]

        elif line[0] == 'xrange':
            xlist = [i for i in range(int(line[1]), int(line[2])+1)]
            out_name = out_name.replace('#', line[1] + 'to' + line[2])

        elif line[0] == 'datatype':
            datatype = [line[i] for i in range(1, len(line))]

    for igm in gmlist:
        print('gm:', igm)
        igm_outfolder = outfolder + '/'
        mkdir(igm_outfolder)
        igm_in_name = in_name.replace('gm', igm)
        igm_out_name = igm_outfolder+out_name.replace('gm', igm)

        for ibxp in bxplist:
            print('  bxp:', ibxp[0])
            ibxp_in_name = igm_in_name.replace('bxp', ibxp[0])
            ibxp_out_name = igm_out_name.replace('bxp', ibxp[0])

            for ipx in range(int(ibxp[1]), int(ibxp[2])+1):
                ipx = str(ipx)
                print('    px:', ipx)
                ipx_in_name = ibxp_in_name.replace('*', ipx)
                ipx_out_name = ibxp_out_name.replace('*', ipx)+'.h5'

                ipx_h5 = h5py.File(ipx_out_name, 'w')
                print(ipx_out_name)
                ipx_h5_group = ipx_h5.require_group('DA/bT0')
                
                for ix in xlist:
                    ix_in_name = ipx_in_name.replace('#', str(ix))
                    ix_in_name = infolder + '/' + ix_in_name

                    ireal_dataset = csvtolist(ix_in_name + '.' + datatype[0])
                    try:
                        imag_dataset = csvtolist(ix_in_name + '.' + datatype[1])
                        ix_dataset = comb_real_img_2d(ireal_dataset, imag_dataset)
                    except:
                        ix_dataset = ireal_dataset
                    #print(np.shape(ix_dataset))
                    #print('dt'+str(its)+'/Z'+str(ix)) 
                    ix_dataset = np.delete(np.transpose(ix_dataset), 0, 0)
                    ipx_h5_group.create_dataset('bz'+str(ix),data=ix_dataset)

                ipx_h5.close()

        


def c3ptcsvtoh5(commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    describe = (commands[0].split())[0]
    in_name = describe
    out_name = describe

    bxplist = []
    for index in range(1, len(commands)):
        line = commands[index].split()

        if line[0] == 'in':
            infolder = line[1]
            print('\n@@@Read data in', infolder)
        
        elif line[0] == 'out':
            outfolder = line[1]
            print('@@@Save data in', outfolder, '\n')

        elif line[0] == 'gm':
            gmlist = [line[i] for i in range(1, len(line))]

        elif line[0] == 'bxp':
            bxplist += [[line[1],line[2],line[3]]]

        elif line[0] == 'qxrange':
            qxlist = [i for i in range(int(line[1]), int(line[2])+1)]

        elif line[0] == 'qyrange':
            qylist = [i for i in range(int(line[1]), int(line[2])+1)]

        elif line[0] == 'qzrange':
            qzlist = [i for i in range(int(line[1]), int(line[2])+1)]

        elif line[0] == 'tslist':
            tslist = [int(line[i]) for i in range(1, len(line))]

        elif line[0] == 'xrange':
            xlist = [i for i in range(int(line[1]), int(line[2])+1)]
            out_name = out_name.replace('#', line[1] + 'to' + line[2])

        elif line[0] == 'datatype':
            datatype = [line[i] for i in range(1, len(line))]
    
    qxyzlist = []
    for iqx in qxlist:
        for iqy in qylist:
            for iqz in qzlist:
                qxyzlist += [[iqx,iqy,iqz]]

    for igm in gmlist:
        print('gm:', igm)
        igm_outfolder = outfolder + '/' + igm
        mkdir(igm_outfolder)
        igm_in_name = in_name.replace('gm', igm)
        igm_out_name = out_name.replace('gm', igm)

        for ibxp in bxplist:
            print('  bxp:', ibxp[0])
            ibxp_in_name = igm_in_name.replace('bxp', ibxp[0])
            ibxp_out_name = igm_out_name.replace('bxp', ibxp[0])

            for ipx in range(int(ibxp[1]), int(ibxp[2])+1):
                ipx = str(ipx)
                print('    px:', ipx)
                ipx_in_name = ibxp_in_name.replace('*', ipx)
                ipx_out_name = ibxp_out_name.replace('*', ipx)
                ipx_out_name = ipx_out_name.replace('_dt', '') + '.h5'

                for iqxyz in qxyzlist:
                    iqxyz_in_name = ipx_in_name.replace('qx_qy_qz', 'qx'+str(iqxyz[0])+'_qy'+str(iqxyz[1])+'_qz'+str(iqxyz[2]))
                    iqxyz_out_name = ipx_out_name.replace('qx_qy_qz', 'qx'+str(iqxyz[0])+'_qy'+str(iqxyz[1])+'_qz'+str(iqxyz[2]))
                    print('      ', iqxyz_out_name)
                    iqxyz_out_name = igm_outfolder + '/' + iqxyz_out_name
                    iqxyz_out_name = iqxyz_out_name.replace('.Z','.X')

                    iqxyz_h5 = h5py.File(iqxyz_out_name, 'w')

                    for its in tslist:

                        its_h5_group = iqxyz_h5.require_group('dt'+str(its))
                        its_in_name = iqxyz_in_name.replace('dt', 'dt'+str(its))
                        
                        for ix in xlist:
                           ix_in_name = its_in_name.replace('#', str(ix))
                           ix_in_name = infolder + '/' + ix_in_name

                           ireal_dataset = csvtolist(ix_in_name + '.' + datatype[0])
                           try:
                               imag_dataset = csvtolist(ix_in_name + '.' + datatype[1])
                               ix_dataset = comb_real_img_2d(ireal_dataset, imag_dataset)
                           except:
                                ix_dataset = ireal_dataset
                           #print(np.shape(ix_dataset))
                           #print('dt'+str(its)+'/Z'+str(ix)) 
                           its_h5_group.create_dataset('X'+str(ix),data=ix_dataset)

                    iqxyz_h5.close()








def c3pth5tocsv(commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()
    
    #print(commands)
    outfolder = (commands[0].split())[0]
    c3dataloc = (commands[0].split())[1]
    kap = float((commands[0].split())[2])

    filespace = []
    namespace = []
    for i in range(1, len(commands)):
        line = commands[i].split()
        ifilespace = []
        for j in range(1, len(line)):
            ifilespace += [line[j]]
        filespace += [ifilespace]
        namespace += [line[0]]
    header = ['tau, conf1, conf2, ...']
    print(namespace)
    print(filespace)

    # store the direction descirbe
    xdicp = []      # wilson line length in positive direction
    xdicn = []      # wilson line length in negetive direction
    c3f = h5py.File(filespace[0][0],'r')
    x = list(c3f[c3dataloc].keys())
    x.sort(key=lambda x:len(x))
    print(x)
    xdicp = [x[0]] + x[1::2]
    xdicn = x[0::2]
    xdicp = x


    header = ['tau, conf1, conf2, ...']
    # positive direction
    print(xdicp)
    for i in range(0,len(filespace)):
        print(namespace[i])
        f = h5py.File(filespace[i][0],'r')
        p = np.array(f.attrs['qvec_list'])
        #print(p)
        for j in range(0,len(xdicp)):
            xname = c3dataloc + xdicp[j] + '/g8'
            print(j,'xname',xname)
            #dataset = np.array(f[xname])*(2*kap)*(2*kap)*(2*kap)
            dataset = np.array(f[xname])
            #print('look here:',np.shape(dataset))
            for fn in range(1, len(filespace[i])):
                otherfile = h5py.File(filespace[i][fn],'r')
                if fn == 20 or fn==50:
                    subdata = np.array(otherfile[xname])
                    new_subdata = []
                    for ic in range(0, len(subdata)):
                        ic_new_subdata = []
                        for io in range(0, 1):
                            io_new_subdata = []
                            for iq in range(0, len(subdata[0])):
                                iq_new_subdata = []
                                for itau in range(0, 16):
                                    iq_new_subdata += [subdata[ic][iq][itau]]
                                io_new_subdata += [iq_new_subdata]
                            ic_new_subdata += [io_new_subdata]
                        new_subdata += [ic_new_subdata]
                    #print('look here:',np.shape(new_subdata))
                    dataset = np.append(dataset,new_subdata,0)

                    #dataset = np.append(dataset, np.array(otherfile[xname])*(2*kap)*(2*kap)*(2*kap),0)
                else:
                    #print('look here:',np.shape(otherfile[xname]))
                    dataset = np.append(dataset, np.array(otherfile[xname])*(2*kap)*(2*kap)*(2*kap),0)
                otherfile.close()
            print(np.shape(dataset))
            for k in range(0,len(dataset[0][0])):
                X_value = xdicp[j].replace('x','')
                X_value = X_value.replace('X','')
                X_value = X_value.replace('l','')
                X_value = X_value.replace('_','')
                if 'x' in xdicp[j]:
                    X_value = '-' + X_value
                outputname = outfolder + namespace[i] + '.X' + X_value + '.g8.qx' + str(p[k][0])+ '_qy' + str(p[k][1])+ '_qz' + str(p[k][2]) + '.real'
                #print(outputname)
                with open(outputname, "w", newline='') as outfile:
                    writer = csv.writer(outfile)
                    writer.writerows([header])
                    for t in range(0,len(dataset[0][0][0])):
                        raw = [dataset[conf][0][k][t].real for conf in range(0,len(dataset))]
                        raw = [t] + raw
                        writer.writerows([raw])
                    outfile.close()
        f.close()
    '''
    # negetive direction
    print(xdicn)
    for i in range(0,len(filespace)):
        print(namespace[i])
        f = h5py.File(filespace[i][0],'r')
        p = np.array(f.attrs['qvec_list'])
        #print(p)
        for j in range(0,len(xdicn)):
            xname = c3dataloc + xdicn[j] + '/g8'
            print(j,'xname',xname)
            #dataset = np.array(f[xname])*(2*kap)*(2*kap)*(2*kap)
            dataset = np.array(f[xname])
            for fn in range(1, len(filespace[i])):
                otherfile = h5py.File(filespace[i][fn],'r')
                if fn == 30:
                    dataset = np.append(dataset, np.array(otherfile[xname])*(2*kap)*(2*kap)*(2*kap),0)
                else:
                    dataset = np.append(dataset, np.array(otherfile[xname]),0)
                otherfile.close()
            print(np.shape(dataset))
            for k in range(0,len(dataset[0][0])):
                outputname = outfolder + namespace[i] + '_X' + str(-j) + '_' + 'qx' + str(p[k][0])+ 'qy' + str(p[k][1])+ 'qz' + str(p[k][2]) + '.txt'
                with open(outputname, "w", newline='') as outfile:
                    writer = csv.writer(outfile)
                    writer.writerows([header])
                    for t in range(0,len(dataset[0][0][0])):
                        raw = [dataset[conf][0][k][t].real for conf in range(0,len(dataset))]
                        raw = [t] + raw
                        writer.writerows([raw])
                    outfile.close()
        f.close()
        '''

        

# the c2pt.h5 to c2ptPXPYPZ.csv command file should be orgenized as:
# outfolder dataset_location_in_h5file(like:c2pt/SP/meson_g15) kap
# csvfile_describe h5file_location
# ...
def c2pth5tocsv(commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()
    outfolder = (commands[0].split())[0]
    c2dataloc = (commands[0].split())[1]
    kap =  float((commands[0].split())[2])
    print('kap',kap)
    print('c2dataloc',c2dataloc)
    filespace = []
    namespace = []
    for i in range(1, len(commands)):
        line = (commands[i].split())
        ifilespace = []
        for j in range(1, len(line)):
            ifilespace += [line[j]]
        filespace += [ifilespace]
        namespace += [line[0]]
    header = ['t, conf1, conf2, ...']

    for k in range(0, len(filespace)):                   # this is h5file name
        print('namespace[k]',namespace[k])
        c2f = h5py.File(filespace[k][0],'r')
        p = np.array(c2f.attrs['psnk_list'])
        #dataset = np.array(c2f[c2dataloc])*(2*kap)*(2*kap)
        dataset = np.array(c2f[c2dataloc])
        #print(np.shape(dataset))
        c2f.close()
        for f in range(1, len(filespace[k])):
            #print(filespace[k][f])
            c2f = h5py.File(filespace[k][f],'r')
            p = np.array(c2f.attrs['psnk_list'])
            if f == 3:
                dataset = np.append(dataset, np.array(c2f[c2dataloc])*(2*kap)*(2*kap),0)
            else:
                dataset = np.append(dataset, np.array(c2f[c2dataloc]),0) 
            print(np.shape(dataset))
            try:
                print(f+1)
                print(dataset[187][18][0],dataset[188][18][0])
            except:
                print(f+1)
            c2f.close()
        #print(np.shape(dataset))
        for i in range(0, len(dataset[0])):           # this is momentum
            #print(p[i])
            outputname = outfolder + namespace[k] + '.' + 'PX' + str(p[i][0])+ '_PY' + str(p[i][1])+ '_PZ' + str(p[i][2]) + '.real'
            with open(outputname, "w", newline='') as outfile:
                writer = csv.writer(outfile)
                writer.writerows([header])
                for t in range(0, len(dataset[0][0])):
                    raw = [dataset[conf][i][t].real for conf in range(0,len(dataset))]
                    raw = [t] + raw
                    writer.writerows([raw])
                outfile.close()
        c2f.close()

def csv_combination(csvfile1, csvfile2, outfolder):

    outname = csvfile1.split('/')[-1]
    outname = outfolder + '/' + outname
    print(outname)

    data_1 = csvtolist(csvfile1)
    data_2 = csvtolist(csvfile2)
    data = []

    print(np.shape(data_1),np.shape(data_2))
    
    for i in range(0, len(data_1)):
        row = data_1[i] + data_2[i][1:]
        data += [row]
    print(np.shape(data))
    '''
    row_max = max(len(data_1[0]),len(data_2[0]))
    row_min = min(len(data_1[0]),len(data_2[0]))
    for i in range(0, len(data_1)):
        row = []
        for j in range(0, row_min):
            row += [(data_1[i][j] + data_2[i][j])/2]
            #print(np.shape(row))
        for j in range(row_min,row_max):
            row += [data_2[i][j]]
        data += [row]
    print(np.shape(data))
    '''

    ncfg = len(data[0]) - 1

    savecsv(data, outname, 'csv, rows:32, Ncfg:'+str(ncfg))




if __name__ == "__main__":

    #c2pth5tocsv(sys.argv[1])
    #csv_combination(sys.argv[1], sys.argv[2], sys.argv[3])

    c3ptcsvtoh5(sys.argv[1])
    #DAcsvtoh5(sys.argv[1])

    #refine_dwf_h5(sys.argv[1])
    #combine_h5_gpd(sys.argv[1])
    #ToUnpo_h5_gpd(sys.argv[1])
    #ToHeli_h5_gpd(sys.argv[1])
    #ToTrans_h5_gpd(sys.argv[1])
