#!/usr/bin/env python3
import numpy as np
import os
from tools import *
from math import gamma
from math import pow
import traceback 
from scipy import integrate
from scipy import special

#from rITD_pion_ritdReal import *

#------------------------------------------------------------#
###################### model functions #######################
#------------------------------------------------------------#
priorsign = 1000
# define the T~0 c2pt function
def oneexp(x, A0, E0):
    return A0 * np.exp(-E0 * x)
def twoexp(x, A0, E0, A1, E1):
    return A0 * np.exp(-E0 * x) + A1 * np.exp(-E1 * x)
def threeexp(x, A0, E0, A1, E1, A2, E2):
    return A0 * np.exp(-E0 * x) + A1 * np.exp(-E1 * x)+A2 * np.exp(-E2 * x)

# define the function to fit
TSIZE = 64                # length of t direction
def onestate(x, A0, E0):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate(x, A0, E0, A1, E1):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))
def threestate(x, A0, E0, A1, E1, A2, E2):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

def onestate_hirarchy(x, a0, E0):
    A0 = np.exp(a0)
    return A0 * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate_hirarchy(x, a0, E0, a1, e1):
    A0 = np.exp(a0)
    A1 = np.exp(a1)
    E1 = E0 + np.exp(e1)
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))
def threestate_hirarchy(x, a0, E0, a1, e1, a2, e2):
    A0 = np.exp(a0)
    A1 = np.exp(a1)
    A2 = np.exp(a2)
    E1 = E0 + np.exp(e1)
    E2 = E0 + np.exp(e1) + np.exp(e2)
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

def onestate_2pi(x, A0, E0, C0):
    return (A0+C0*np.exp(-E0 * TSIZE)) * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate_2pi(x, A0, E0, A1, E1, C0):
    return (A0+C0*np.exp(-E0 * TSIZE)) * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x))) + A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))

def threestate_t(x, A0, E0, A1, E1, A2, E2):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)/x+np.exp(-E2 * (TSIZE-x))/(TSIZE-x))

def threestate_joint(x, A0, E0, A1, E1, A2, E2, B1, B2, B3):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

#####################################################
##########         c2pt prior fit        ############
#####################################################

def twoexp_prior_E0(x, A0, E0, A1, E1):
    try:
        ylist = []
        for i in range(0, len(x)):
            if x[i] < priorsign:
                ylist += [float(twoexp(x[i], A0, E0, A1, E1))]
            else:
                ylist += [float(E0)]
        return np.array(ylist)
    except:
        if x < priorsign:
            return twoexp(x, A0, E0, A1, E1)
        else:
            return E0
def threeexp_prior_E0(x, A0, E0, A1, E1, A2, E2):
    try:
        ylist = []
        for i in range(0, len(x)):
            if x[i] < priorsign:
                ylist += [float(threeexp(x[i], A0, E0, A1, E1, A2, E2))]
            else:
                ylist += [float(E0)]
        return np.array(ylist)
    except:
        if x < priorsign:
            return threeexp(x, A0, E0, A1, E1, A2, E2)
        else:
            return E0
def twoexp_prior_E1(x, A0, E0, A1, E1):
    try:
        ylist = []
        for i in range(0, len(x)):
            if x[i] < priorsign:
                ylist += [float(twoexp(x[i], A0, E0, A1, E1))]
            else:
                ylist += [float(E1)]
        return np.array(ylist)
    except:
        if x < priorsign:
            return twoexp(x, A0, E0, A1, E1)
        else:
            return E1
def threeexp_prior_E1(x, A0, E0, A1, E1, A2, E2):
    try:
        ylist = []
        for i in range(0, len(x)):
            if x[i] < priorsign:
                ylist += [float(threeexp(x[i], A0, E0, A1, E1, A2, E2))]
            else:
                ylist += [float(E1)]
        return np.array(ylist)
    except:
        if x < priorsign:
            return threeexp(x, A0, E0, A1, E1, A2, E2)
        else:
            return E1

def twostate_prior_E1(x, A0, E0, A1, E1):
    try:
        ylist = []
        for i in range(0, len(x)):
            if x[i] < priorsign:
                ylist += [float(twostate(x[i], A0, E0, A1, E1))]
            else:
                ylist += [float(E1)]
        return np.array(ylist)
    except:
        if x < priorsign:
            return twostate(x, A0, E0, A1, E1)
        else:
            return E1
def threestate_prior_E1(x, A0, E0, A1, E1, A2, E2):
    try:
        ylist = []
        for i in range(0, len(x)):
            if x[i] < priorsign:
                ylist += [float(threestate(x[i], A0, E0, A1, E1, A2, E2))]
            else:
                ylist += [float(E1)]
        return np.array(ylist)
    except:
        if x < priorsign:
            return threestate(x, A0, E0, A1, E1, A2, E2)
        else:
            return E1


#####################################################
##############c3pt/c2pt ratio fitting################
#####################################################
def c3pt_ratio_two_sum_plot(tsnsk, A0, E0, A1, E1, B0, B1, B2, B3):
    ts, nsk = tsnsk
    #print(tsnsk)
    def f(x):
        return c3pt_twostate((ts, x), A0, E0, A1, E1, B0, B1, B2, B3)/twostate(ts,A0,E0,A1,E1)
    y, err = integrate.quad(f, nsk-0.5, ts-nsk+0.5)
    return y
def c3pt_ratio_three_sum_plot(tsnsk, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts, nsk = tsnsk
    def f(x):
        return c3pt_ratio_three((ts, x), A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)
    y, err = integrate.quad(f, nsk-0.5, ts-nsk+0.5)
    return y
def c3pt_ratio_two_exp_plot(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    tau = tau + ts/2
    tstau=(ts,tau)
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twoexp(ts,A0,E0,A1,E1)
def c3pt_ratio_two_exp_plotmid(invts, A0, E0, A1, E1, B0, B1, B2, B3):
    ts = 1/invts
    tau = ts/2
    tstau=(ts,tau)
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twoexp(ts,A0,E0,A1,E1)
def c3pt_ratio_three_exp_plot(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    tau = tau + ts/2
    tstau=(ts,tau)
    return c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)/threeexp(ts,A0,E0,A1,E1,A2,E2)
def c3pt_ratio_two_exp_approx_plot(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    tau = tau + ts/2
    dE = E1 - E0
    return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)



def c3pt_ratio_two_exp(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twoexp(ts,A0,E0,A1,E1)
def c3pt_ratio_two_exp_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    #return B0
    return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)
def c3pt_ratio_two_exp_approx2(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)+B3*np.exp(-dE*ts)
def c3pt_ratio_two_exp_approx_prdE(tstau, B0, B1, B2, dE):
    ts,tau = tstau
    try:
        ylist = []
        for i in range(0, len(ts)):
            if ts[i] < priorsign:
                ylist += [B0+B1*np.exp(-dE*(ts[i]-tau[i]))+B2*np.exp(-dE*tau[i])]
            else:
                ylist += [dE]
        return np.array(ylist)
    except:
        if ts < priorsign:
            return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)
        else:
            return dE

def c3pt_ratio_three_exp(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    return c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)/threeexp(ts,A0,E0,A1,E1, A2, E2)
def c3pt_ratio_three_exp_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    dE1 = E1 - E0
    dE2 = E2 - E0
    return B0+B1*np.exp(-dE1*(ts-tau))+B2*np.exp(-dE1*tau)+C1*np.exp(-dE2*(ts-tau))+C2*np.exp(-dE2*tau)


# special case: fix E1list
E1_px4 = [0.8377661,0.9017177,0.8323226,0.76122,0.7146389,0.692232,0.6626753,0.6843433,0.6708452,0.6930043,0.6808153,0.6808153]
E1_px4 = E1_px4 + [0.512885 for i in range(0,20)]
dE1_px4 = np.array([E1_px4[i]-0.45829 for i in range(0, len(E1_px4))])
E1_px5 = [0.8940821,0.9889847,0.922745,0.8461154,0.8047077,0.7801863,0.7394406,0.7482306,0.7063681,0.7063681,0.7063681,0.7063681]
E1_px5 = E1_px5 + [0.591435 for i in range(0,20)]
dE1_px5 = np.array([E1_px5[i]-0.54477 for i in range(0, len(E1_px5))])
def c3pt_ratio_twoexp_approx_px4(tstau, B0, B1, B2):
    ts,tau = tstau
    #print(tau)
    return B0+B1*np.exp(-dE1_px4[(ts-tau)]*(ts-tau))+B2*np.exp(-dE1_px4[tau]*tau)
def c3pt_ratio_twoexp_approx_px5(tstau, B0, B1, B2):
    ts,tau = tstau
    return B0+B1*np.exp(-dE1_px5[(ts-tau)]*(ts-tau))+B2*np.exp(-dE1_px5[tau]*tau)

def c3pt_ratio_two(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twostate(ts,A0,E0,A1,E1)
def c3pt_ratio_two_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    return c3pt_twostate_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twostate(ts,A0,E0,A1,E1)
def c3pt_ratio_three(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    return c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)/threestate(ts,A0,E0,A1,E1,A2,E2)
def c3pt_ratio_three_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    return c3pt_threestate_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3)/threestate(ts,A0,E0,A1,E1,A2,E2)
def c3pt_ratio_three_approx_ov2(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    return c3pt_threestate_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3)/twostate(ts,A0,E0,A1,E1)

#####################################################
#####################c3pt fit########################
#####################################################

def c3pt_twostate_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau))

def c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)+B3*np.exp(-dE*ts))

def c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    dE1 = E1 - E0
    dE2 = E2 - E0
    dE12 = E2 - E1
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE1*(ts-tau))+B2*np.exp(-dE1*tau)+B3*A1/A0*np.exp(-dE1*ts)+C1*np.exp(-dE2*(ts-tau))+C2*np.exp(-dE2*tau)+C3*np.exp(-dE12*tau)+C4*np.exp(-dE12*ts)+C5*np.exp(-dE2*ts))
def c3pt_threestate_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    dE1 = E1 - E0
    dE2 = E2 - E0
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE1*(ts-tau))+B2*np.exp(-dE1*tau)+B3*np.exp(-dE1*ts)+C1*np.exp(-dE2*(ts-tau))+C2*np.exp(-dE2*tau))

#####################################################
###################gpd c3pt fit######################
#####################################################
#Ein is like [A0, E0, A1, E1, ...]
def gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    NT=64
    part00 = B00*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-Eout[1]*tau-Ein[1]*(ts-tau))
    part01 = B01*np.sqrt(Eout[0])*np.sqrt(Ein[2])*np.exp(-Eout[1]*tau-Ein[3]*(ts-tau))
    part10 = B10*np.sqrt(Eout[2])*np.sqrt(Ein[0])*np.exp(-Eout[3]*tau-Ein[1]*(ts-tau))
    part11 = B11*np.sqrt(Eout[2])*np.sqrt(Ein[2])*np.exp(-Eout[3]*tau-Ein[3]*(ts-tau))
    #part_thermo_1 = B_thermo_1*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*(ts-tau)-Eout[1]*(NT-ts))
    p000 = np.sqrt(Eout[1]**2 - Ein[1]**2)/2
    E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #print(Ein[1], Eout[1], Ein[1]+Eout[1],E11)
    part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-E11*tau-Ein[1]*(NT-ts))
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-Ein[1]*(NT-ts))
    part_thermo_3 = B_thermo_3*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    return part00 + part01 + part10 + part11 + part_thermo_2  + part_thermo_3

def gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    NT=64
    part00 = B00*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-Eout[1]*tau-Ein[1]*(ts-tau))
    part01 = B01*np.sqrt(Eout[0])*np.sqrt(Ein[2])*np.exp(-Eout[1]*tau-Ein[3]*(ts-tau))
    part10 = B10*np.sqrt(Eout[2])*np.sqrt(Ein[0])*np.exp(-Eout[3]*tau-Ein[1]*(ts-tau))
    part11 = B11*np.sqrt(Eout[2])*np.sqrt(Ein[2])*np.exp(-Eout[3]*tau-Ein[3]*(ts-tau))
    part02 = B02*np.sqrt(Eout[0])*np.sqrt(Ein[4])*np.exp(-Eout[1]*tau-Ein[5]*(ts-tau))
    part20 = B20*np.sqrt(Eout[4])*np.sqrt(Ein[0])*np.exp(-Eout[5]*tau-Ein[1]*(ts-tau))
    part12 = B12*np.sqrt(Eout[2])*np.sqrt(Ein[4])*np.exp(-Eout[3]*tau-Ein[5]*(ts-tau))
    part21 = B21*np.sqrt(Eout[4])*np.sqrt(Ein[2])*np.exp(-Eout[5]*tau-Ein[3]*(ts-tau))
    part22 = B22*np.sqrt(Eout[4])*np.sqrt(Ein[4])*np.exp(-Eout[5]*tau-Ein[5]*(ts-tau))
    part_thermo_1 = B_thermo_1*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    #p000 = np.sqrt(abs(Eout[1]**2 - Ein[1]**2))/2
    #E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-E11*tau-Ein[1]*(NT-ts))
    part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-Ein[1]*(NT-ts))
    part_thermo_3 = B_thermo_3*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    return part00 + part01 + part10 + part11 + part02 + part20 + part12 + part21+ part22 + part_thermo_1 + part_thermo_2  + part_thermo_3

def gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #print(Ein)
    part1 = gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2, B_thermo_3)/twostate(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part2 = twostate(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3])*twostate(tau, Ein[0],Ein[1],Ein[2],Ein[3])*twostate(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part3 = twostate(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3])*twostate(tau, Eout[0],Eout[1],Eout[2],Eout[3])*twostate(ts, Eout[0],Eout[1],Eout[2],Eout[3])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #B02 = 0
    #B20 = 0
    #B22=0
    part1 = gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1, B_thermo_2, B_thermo_3)/threestate(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part2 = threestate(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threestate(tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threestate(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part3 = threestate(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threestate(tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threestate(ts, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_two_plot(tstau,Ain0,Ein0,Ain1,Ein1,Aout0,Eout0,Aout1,Eout1,B00,B01,B10,B11, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    tau = tau+ts/2
    tstau=(ts,tau)
    Ein = [Ain0,Ein0,Ain1,Ein1]
    Eout = [Aout0,Eout0,Aout1,Eout1]
    part1 = gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2, B_thermo_3=B_thermo_3)/twoexp(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part2 = twoexp(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(tau, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part3 = twoexp(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(tau, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(ts,Eout[0],Eout[1],Eout[2],Eout[3])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_three_plot(tstau, Ain0,Ein0,Ain1,Ein1,Ain2,Ein2,Aout0,Eout0,Aout1,Eout1,Aout2,Eout2, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    tau = tau+ts/2
    tstau=(ts,tau)
    Ein = [Ain0,Ein0,Ain1,Ein1,Ain2,Ein2]
    Eout = [Aout0,Eout0,Aout1,Eout1,Aout2,Eout2]
    #B02 = 0
    #B20 = 0
    part1 = gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=B_thermo_2, B_thermo_3=B_thermo_3)/threeexp(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part2 = threeexp(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threeexp(tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threeexp(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part3 = threeexp(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threeexp(tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threeexp(ts, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_two_sum_plot(tsnsk, Ain0,Ein0,Ain1,Ein1,Aout0,Eout0,Aout1,Eout1,B00,B01,B10,B11, B_thermo_2=0, B_thermo_3=0):
    ts, nsk = tsnsk
    #print(ts, nsk)
    Ein = [Ain0,Ein0,Ain1,Ein1]
    Eout = [Aout0,Eout0,Aout1,Eout1]
    y = []
    for its in ts:
        def f(x):
            part1 = gpd_c3pt_twostate((its, x), Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2, B_thermo_3=B_thermo_3)/twoexp(its, Ein[0],Ein[1],Ein[2],Ein[3])
            part2 = twoexp(its-x, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(x, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(its, Ein[0],Ein[1],Ein[2],Ein[3])
            part3 = twoexp(its-x, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(x, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(its, Eout[0],Eout[1],Eout[2],Eout[3])
            return part1*np.sqrt(part2/part3)
        #print(its, nsk, f(1))
        y_its, err = integrate.quad(f, nsk-0.5, its-nsk+0.5)
        y += [y_its]

    return y

#####################################################
##############     polynomial fit    ################
#####################################################
def const(x,a):
    return a
def const_sum(x,a):
    return a*(x-3)

def linearfunc(x, a, b):
    #b = 1.020675532812552966e+00
    return a + b * x
def linearfunc_pcorrection(x, a, b, c, dE1=1000):
    #dE1 = 8.406099e-01 - 5.715695e-01 #tmin5 nz=6
    #dE1 = 6.130471e-01 - 3.811633e-01 #tmin5 nz=4
    #dE1 = 5.847173e-01 - 2.033796e-01 #tmin5 nz=2
    #dE1 = 9.011164e-01 - 5.715695e-01 #tmin4 nz=6
    #dE1 = 6.912640e-01 - 3.811633e-01 #tmin4 nz=4
    #dE1 = 6.181203e-01 - 2.033796e-01 #tmin4 nz=2
    return a + b * x + c * np.exp(-dE1*x)

def polynomial2func_mom2cutoff_NLO(x, moms, r, a):
    print(CnC0NLO(mu0*5.0676896,x,2),CnC0NNLO(mu0*5.0676896,x,2))
    #return moms - 2 * r / (x/a) / (x/a) / 1
    #return moms - 2 * r / ((x/a)**2 + 2)/CnC0NLO(mu0*5.0676896,x,2)
    return moms - 2 * r / (x/a) / (x/a) / CnC0NLO(mu0*5.0676896,x,2)
    #return moms - 2 * r / (x/a) / (x/a) / Cn(x,2, mu = mu0*5.0676896,alps = alps0)
def polynomial2func_mom2cutoff_NNLO(x, moms, r, a):
    #return moms - 2 * r / (x/a) / (x/a) / 1
    #return moms - 2 * r / ((x/a)**2 + 2)/CnC0NNLO(mu0*5.0676896,x,2)
    return moms - 2 * r / (x/a) / (x/a) / CnC0NNLO(mu0*5.0676896,x,2)
    #return moms - 2 * r / (x/a) / (x/a) / CnNNLO(x, 2, mu = mu0*5.0676896,alps = alps0)

def polynomialto2(x, b, c):
    return c + b * x * x

def polynomial2func(x, b):
    return 1 + b * x * x

def polynomial4func(x, b, c):
    return 1 + b * x * x + c * x * x * x * x

def polynomial6func(x, b, c, d):
    return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x

def polynomial8func(x, b, c, d, g):
    return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x + g * x * x * x * x * x * x * x * x

def polynomial10func(x, b, c, d, g, h):
    return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x + g * x * x * x * x * x * x * x * x + h * x * x * x * x * x * x * x * x * x * x

def make_fixb_ply4func(b):
    def fixb_ply4func(x,c):
        return 1 + b * x * x + c * x * x * x * x
    return fixb_ply4func

def make_fixb_ply6func(b):
    def fixb_ply6func(x,c,d):
        return 1 + b * x * x + c * x * x * x * x + d * x * x * x * x * x * x
    return fixb_ply6func

#####################################################
###############    rITD joint fit    ################
#####################################################

def form_factor(t, *ak):
    tcut = 4*0.3*0.3
    to = tcut*(1-np.sqrt(1+0.353/tcut))
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-to))/(np.sqrt(tcut-t)+np.sqrt(tcut-to))
    print(z,len(z),to)
    fmfct = 1
    for i in range(0, len(ak)):
        fmfct += ak[i] * (z**(i+1))
    return fmfct

def polynomial_form_factor(t, *ak):
    t=-t
    fmfct = 1
    for i in range(0, len(ak)):
        fmfct += ak[i] * t**(i+1)
    return fmfct

'''
def rioffe_ope_mom2(xz, a3):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2)

def rioffe_ope_mom4(xz, a3, a5):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24)

def rioffe_ope_mom6(xz, a3, a5, a7):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720)

def rioffe_ope_mom8(xz, a3, a5, a7, a9):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720) + Cn(z,8)/Cn(z,0) * a9 * (x*x*x*x*x*x*x*x/40320)

def rioffe_ope_mom10(xz, a3, a5, a7, a9, a11):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720) + Cn(z,8)/Cn(z,0) * a9 * (x*x*x*x*x*x*x*x/40320) - Cn(z,10)/Cn(z,0) * a11 * (x*x*x*x*x*x*x*x*x*x/3628800)

def rioffe_ope_mom12(xz, a3, a5, a7, a9, a11, a13):
    z,x = xz
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720) + Cn(z,8)/Cn(z,0) * a9 * (x*x*x*x*x*x*x*x/40320) - Cn(z,10)/Cn(z,0) * a11 * (x*x*x*x*x*x*x*x*x*x/3628800) + Cn(z,12)/Cn(z,0) * a13 * (x*x*x*x*x*x*x*x*x*x*x*x/479001600)

#####################################################
###############     rITD a,b fit    #################
#####################################################
def pdf(x, a, b):
    print(a,b,gamma(2+a+b)/(gamma(1+a)*gamma(1+b))*(x**a)*((1-x)**b))
    return gamma(2+a+b)/(gamma(1+a)*gamma(1+b))*(x**a)*((1-x)**b)

def rioffe_ab_mom2(xz, a, b):
    z,x = xz
    print('mom2',xz)
    #a3 = (gamma(3+a)*gamma(2+a+b))/(gamma(1+a)*gamma(4+a+b))
    a3 = integrate.quad(x**2*pdf(x, a, b),0,1)
    #print(a3, a, b)
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2)

def rioffe_ab_mom4(xz, a, b):
    z,x = xz
    print('mom4',xz)
    #a3 = (gamma(3+a)*gamma(2+a+b))/(gamma(1+a)*gamma(4+a+b))
    #a5 = (gamma(5+a)*gamma(2+a+b))/(gamma(1+a)*gamma(6+a+b))
    a3 = integrate.quad(x**2*pdf(x, a, b),0,1)
    a5 = integrate.quad(x**4*pdf(x, a, b),0,1)
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24)

def rioffe_ab_mom6(xz, a, b):
    z,x = xz
    #a3 = (gamma(3+a)*gamma(2+a+b))/(gamma(1+a)*gamma(4+a+b))
    #a5 = (gamma(5+a)*gamma(2+a+b))/(gamma(1+a)*gamma(6+a+b))
    #a7 = (gamma(7+a)*gamma(2+a+b))/(gamma(1+a)*gamma(8+a+b))
    a3 = integrate.quad(x**2*pdf(x, a, b),0,1)
    a5 = integrate.quad(x**4*pdf(x, a, b),0,1)
    a7 = integrate.quad(x**6*pdf(x, a, b),0,1)
    return 1 - Cn(z,2)/Cn(z,0) * a3 * (x*x/2) + Cn(z,4)/Cn(z,0) * a5 * (x*x*x*x/24) - Cn(z,6)/Cn(z,0) * a7 * (x*x*x*x*x*x/720)
'''


def dispersion(p,m):
    return np.sqrt(m*m+p*p)
    #print(p,p/np.sqrt(m*m+p*p))
    #return p/np.sqrt(m*m+p*p)

def tau_exp_px0(ts,A,B):
    #print(B,A)
    #A=-1.02
    #B=0
    E1 = 0.06
    E2 = 0.296
    E11 = 0.12
    NT=64
    return (A*np.exp(-(NT-ts)*E1) + B*np.exp(-(NT-ts)*E1)*np.exp(-ts*E11))/(np.exp(-E1*ts)+0.44*np.exp(-(E2-E1)*ts))

def model_ab(x, a, b):
    #print(ab)
    ab = [a,b]
    return special.gamma(2+ab[0]+ab[1])/(special.gamma(1+ab[0])*special.gamma(1+ab[1]))*(x**ab[0])*((1-x)**ab[1])
