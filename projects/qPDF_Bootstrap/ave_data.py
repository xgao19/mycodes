#!/usr/bin/env python3
import numpy as np
from tools import *
from tools2 import *

def c2pt_pxyq_ave(input_format, output_format):

    #bxplist = ['CG52bxp00_CG52bxp00','CG52bxp20_CG52bxp20','CG52bxp30_CG52bxp30']
    bxplist = ['CG52bxp20_CG52bxp20']
    smlist = ['SS', 'SP']
    gmlist = ['meson_g15']
    #PXlist = [i for i in range(-5,6)]
    PYlist = [0, 1, -1]
    PZlist = [0, 1, -1]
    PXlist = [i for i in range(2,3)]
    #PYlist = [0, 1, -1]
    #PZlist = [0, 1, -1]

    for ibxp in bxplist:
        ibxp_input_format = input_format.replace('.bxp','.'+ibxp)
        ibxp_output_format = output_format.replace('.bxp','.'+ibxp)
        for ipx in PXlist:
            for ism in range(0, len(smlist)):
                ism_input_format = ibxp_input_format.replace('.sm','.'+smlist[ism])
                ism_output_format = ibxp_output_format.replace('.sm','.'+smlist[ism])
                for igm in range(0, len(gmlist)):
                    igm_input_format = ism_input_format.replace('.gm','.'+gmlist[igm])
                    igm_output_format = ism_output_format.replace('.gm','.'+gmlist[igm])
                    
                    p_values = []
                    p_collection = []
                    for ipy in PYlist:
                        for ipz in PZlist:
                            psq = ipy*ipy + ipz*ipz
                            if psq not in p_values:
                                p_values += [psq]
                                p_collection += [[[ipx,ipy,ipz]]]
                            else:
                                for ip in range(0,len(p_values)):
                                    if psq == p_values[ip]:
                                        p_collection[ip] += [[ipx,ipy,ipz]]

                    for ip in range(0, len(p_values)):
                        output_name = igm_output_format.replace('PX_','PX'+str(p_collection[ip][0][0])+'_')
                        output_name = output_name.replace('PY_','PY'+str(p_collection[ip][0][1])+'_')
                        output_name = output_name.replace('_PZ','_PZ'+str(p_collection[ip][0][2]))
                        pxyz_dataset = []
                        for pxyz in p_collection[ip]:
                            input_name = igm_input_format.replace('PX_','PX'+str(pxyz[0])+'_')
                            input_name = input_name.replace('PY_','PY'+str(pxyz[1])+'_')
                            input_name = input_name.replace('_PZ','_PZ'+str(pxyz[2]))
                            pxyz_dataset += [csvtolist(input_name)]
                        dataset_ave = ave_2d_lists(pxyz_dataset)
                        print(np.shape(dataset_ave))
                        savecsv(dataset_ave, output_name, 'csv, rows:'+str(len(dataset_ave))+', Ncfg:'+str(len(dataset_ave[0])-1))

def c3pt_pxyq_ave(input_format, output_format):

    bxplist = ['CG52bxp00_CG52bxp00','CG52bxp20_CG52bxp20','CG52bxp30_CG52bxp30']

    smlist = ['SS']
    #gmlist = ['g0','g1','g2','g4','g8']
    #PXlist = [0,1,2,3,4,5]
    #tslist = [9,12,15,18]
    gmlist = ['g8']
    PXlist = [0,1]
    tslist = [8,10,12]
    zlist = [iz for iz in range(-15,16)]
    qxlist = [0, 1, -1]
    qylist = [0, 1, -1]
    qzlist = [0, 1, -1]

    for ism in range(0, len(smlist)):
        ism_input_format = input_format.replace('.sm','.'+smlist[ism])
        ism_output_format = output_format.replace('.sm','.'+smlist[ism])
        for igm in range(0, len(gmlist)):
            igm_input_format = ism_input_format.replace('.gm','.'+gmlist[igm])
            igm_output_format = ism_output_format.replace('.gm','.'+gmlist[igm])
            for ipx in PXlist:
                ipx_input_format = igm_input_format.replace('.PX','.PX'+str(ipx))
                ipx_output_format = igm_output_format.replace('.PX','.PX'+str(ipx))
                ibxp_input_format = ipx_input_format.replace('.bxp','.'+bxplist[int(ipx/2)])
                ibxp_output_format = ipx_output_format.replace('.bxp','.'+bxplist[int(ipx/2)])
                for its in range(0,len(tslist)):
                    its_input_format = ibxp_input_format.replace('_dt','_dt'+str(tslist[its]))
                    its_output_format = ibxp_output_format.replace('_dt','_dt'+str(tslist[its]))
                    for iz in range(0,len(zlist)):
                        iz_input_format = its_input_format.replace('.X','.X'+str(zlist[iz]))
                        iz_output_format = its_output_format.replace('.X','.X'+str(zlist[iz]))
                        for iqx in qxlist:

                            print(smlist[ism],gmlist[igm],'PX'+str(ipx),'dt'+str(tslist[its]),'z'+str(zlist[iz]),'qx'+str(iqx))
                            
                            q_values = []
                            q_collection = []
                            for iqy in qylist:
                                for iqz in qzlist:
                                    qsq = iqy*iqy + iqz*iqz
                                    if qsq not in q_values:
                                        q_values += [qsq]
                                        q_collection += [[[iqx,iqy,iqz]]]
                                    else:
                                        for iq in range(0,len(q_values)):
                                            if qsq == q_values[iq]:
                                                q_collection[iq] += [[iqx,iqy,iqz]]
                                                
                            for iq in range(0, len(q_values)):
                                output_name = iz_output_format.replace('qx_','qx'+str(q_collection[iq][0][0])+'_')
                                output_name = output_name.replace('qy_','qy'+str(q_collection[iq][0][1])+'_')
                                output_name = output_name.replace('_qz','_qz'+str(q_collection[iq][0][2]))
                                qxyz_dataset = []
                                for qxyz in q_collection[iq]:
                                    input_name = iz_input_format.replace('qx_','qx'+str(qxyz[0])+'_')
                                    input_name = input_name.replace('qy_','qy'+str(qxyz[1])+'_')
                                    input_name = input_name.replace('_qz','_qz'+str(qxyz[2]))
                                    qxyz_dataset += [csvtolist(input_name)]
                                dataset_ave = ave_2d_lists(qxyz_dataset)
                                print(np.shape(dataset_ave),q_collection[iq])
                                savecsv(dataset_ave, output_name, 'csv, rows:'+str(len(dataset_ave))+', Ncfg:'+str(len(dataset_ave[0])-1))

def save_c3pt_ave(q_values, q_collection, iz_input_format, iz_output_format):
    for iq in range(0, len(q_values)):
        output_name = iz_output_format.replace('qx_','qx'+str(q_collection[iq][0][0])+'_')
        output_name = output_name.replace('qy_','qy'+str(q_collection[iq][0][1])+'_')
        output_name = output_name.replace('_qz','_qz'+str(q_collection[iq][0][2]))
        qxyz_dataset = []
        for qxyz in q_collection[iq]:
            input_name = iz_input_format.replace('qx_','qx'+str(qxyz[0])+'_')
            input_name = input_name.replace('qy_','qy'+str(qxyz[1])+'_')
            input_name = input_name.replace('_qz','_qz'+str(qxyz[2]))
            qxyz_dataset += [csvtolist(input_name)]
        dataset_ave = ave_2d_lists(qxyz_dataset)
        print(np.shape(dataset_ave))
        savecsv(dataset_ave, output_name, 'csv, rows:'+str(len(dataset_ave))+', Ncfg:'+str(len(dataset_ave[0])))



if __name__ == "__main__":
    input_format = '/Volumes/XIANG_2/0study/lattice/data/l48c64a06pi/breit_h5data/c2pt_a3/c2pt.bxp.sm.gm.PX_PY_PZ.real'
    output_format= '/Volumes/XIANG_2/0study/lattice/data/l48c64a06pi/breit_h5data/c2pt_a3_ave/c2pt.bxp.sm.gm.PX_PY_PZ.real'
    #input_format = '/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_48c64_data/gpd_data_c/c3pt_comb_bxp00_a2b2/qpdf.SS.meson.ama.bxp_hyp.PX_PY0_PZ0_dt.X.gm.qx_qy_qz.real'
    #output_format= '/Users/gaoxiang/Desktop/0study/research/lattice/data/pion_48c64_data/gpd_data_c/ave_c3pt_bxp00_a2b2/qpdf.SS.meson.ama.bxp_hyp.PX_PY0_PZ0_dt.X.gm.qx_qy_qz.real'
    if 'c2pt' in input_format:
        c2pt_pxyq_ave(input_format, output_format)
    elif 'c3pt' in input_format:
        c3pt_pxyq_ave(input_format, output_format)