#!/usr/bin/env python3
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from modelfuncs import *
import os.path
from pylab import *
#from rITD_fit import *

lightcolors = ['silver', 'sandybrown','paleturquoise', 'palegreen',  'plum', 'blue', 'black']
heavycolors = ['Crimson','SteelBlue', 'DarkOrange', 'purple','SlateGray','green','brown','black','Magenta','Blue','OrangeRed','DarkGreen','Gold','DimGray','Sienna','Olive','PaleVioletRed','DeepSkyBlue','Red','SpringGreen']
heavycolors2 = ['Crimson','SteelBlue', 'DarkOrange', 'purple','green', 'brown','SlateGray','black','Magenta','DimGray','black','black','black','black','black','black','black','black','Red','SpringGreen']

'''
This code plot a 2-D picture with x-y data with error bar or not.
@1 Your dataset can contain many subdataset to plot in one picture.
@2 Your subdataset should in the format 'x  y  yerror'
or in the format 'x  y  yerror- yerror+'
@3 You have a datalabel option to label your plot and subdata.
Please note: datalabel[0] is the name of this plot, while
datalabel[i] with i >= 1 is name of the subdata.
@4 You have a xy label option accapt an array to label the axes.
@5 You have a yrange option to decide if plot in a specific y range.
@6 You can choose to plot some curves use the 'func' option.
@7 You can choose to plot a band around the curve, this option should contain 2 times funcs of 'func' option.
@8 You can choose to plot in a log scale by the commander logscale==True.
'''

def plot_err(dataset, datalabel=None, xylabel=None, yrange=None, func=None, funclabel=None, fillfunc=None, folder = '/', logscale=False, text=[], factor_yscale = 1):

    plt.rcParams['figure.dpi'] = 300
    fig = plt.figure(figsize=(5,3.2))
    #fig = plt.figure(figsize=(3.8,5))
    #ax = fig.add_subplot(111)
    ax = fig.add_axes([0.18, 0.18, 0.78, 0.78])

    # this part to plot curves
    #print(func[0])
    #func = [curve_2state_px4_z12_fixdE1,curve_2state_px4_z12_fixdE2,curve_3state_px4_z12_fixdE1dE2]
    if func != None:
        try:
            func[0]
        except:
            func = [func]
        #print(dataset[0])
        x = np.arange(dataset[0][0][0],dataset[0][-1][0],0.01)
        #x = np.arange(18,20,0.01)
        #print(x)
        for j in range(0, len(func)):
            #x = np.arange(dataset[j][0][0],dataset[j][-1][0],0.01)
            try:
                curve_label = funclabel[j]
            except:
                curve_label = func[j].__name__
            y = [func[j](t) for t in x]
            #print(y)
            plt.plot(x, y, linewidth=1.2, color = heavycolors[j],label=curve_label)
            #plt.plot(x, y, linewidth=1.2, color = heavycolors[j])
    
    if fillfunc != None:
        for i in range(0, int(len(fillfunc)/2)):
            x = np.arange(dataset[0][0][0],dataset[0][-1][0],0.01)
            #print(x)
            y1 = [fillfunc[2*i](t) for t in x]
            y2 = [fillfunc[2*i+1](t) for t in x]
            plt.fill_between(x, y1, y2, color = heavycolors[i],alpha=0.2)

    # this part plot the data points
    i=0
    factor = -1
    for subdataset in dataset:

        subdataset = np.transpose(subdataset)
        print(subdataset[0])

        mv = (subdataset[0][-1] - subdataset[0][0])/20000
        #mv = 0.0
        #print(mv)
        subdataset[0] = subdataset[0] + mv*factor - int(len(dataset)/2)*mv
        ncol = len(subdataset)

        i += 1
        dot_fmt = ['s','o','D','^','h']
        nm = [factor_yscale for j in range(0,len(dataset))]
        if subdataset[1][0] < 0:
            nm[i-1] = -1
        Lslist = [24,48,48,64,64]
        #xnm = [(subdataset[0][j]*2*3.141596/Lslist[i-1])**2 for j in range(0,len(subdataset[0]))]
        #ynm = [0.3,0.3,0,0,0]
        #mv = (xnm[-1] - xnm[0])/200
        #xnm = xnm + mv*factor
        #xnm = [0.194412,0.11406]
        try:
            sublabel = datalabel[i]
        except:
            sublabel = None
        # this part to plot error bar points
        if ncol == 2:
            ax.errorbar(subdataset[0],subdataset[1]*nm[i-1], fmt=dot_fmt[int((i-1)/100)], color=heavycolors[int((i-1))], markersize=3, capsize=1, label=sublabel, markeredgewidth=0.8, elinewidth=0.8, capthick=0.8)
        elif ncol ==3:
            #panel[int((i-1)/5)].errorbar(subdataset[0],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1], fmt=dot_fmt[int((i-1)/5)], color=heavycolors[int((i-1)%5)], markerfacecolor='none', markersize=3, capsize=0, markeredgewidth=0.6, label=sublabel, elinewidth=1, capthick=0.5)
            #ax.errorbar(subdataset[0],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1], fmt=dot_fmt[0],color=heavycolors[i-1], markerfacecolor='none', markersize=7, capsize=4, markeredgewidth=1, label=sublabel, elinewidth=1, capthick=0.6)
            ax.errorbar(subdataset[0],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1], fmt=dot_fmt[0],color=heavycolors[i-1], markerfacecolor='none', markersize=5, capsize=3, markeredgewidth=1, label=sublabel, elinewidth=1, capthick=0.6)
            #ax.errorbar(subdataset[0],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1], fmt=dot_fmt[int((0)/3)], color=heavycolors[int((i-1)%3000)], markerfacecolor='none', markersize=3, capsize=0, markeredgewidth=0.6, label=sublabel, elinewidth=1, capthick=0.5)
            #ax.errorbar(subdataset[0],subdataset[1]*nm[i-1],subdataset[2]*nm[i-1], fmt=dot_fmt[1], color=heavycolors[i-1], markersize=2, capsize=0, markeredgewidth=0.5, label=sublabel, elinewidth=1, capthick=0.5)
        elif ncol ==4:
            ax.errorbar(subdataset[0],subdataset[1],[subdataset[2],subdataset[3]], fmt='o', markerfacecolor='none', markersize=5, capsize=5, label=sublabel, markeredgewidth=0.8, elinewidth=0.8, capthick=0.8)
            #ax.errorbar(subdataset[0],subdataset[1],[subdataset[2],subdataset[3]], fmt='.', color=heavycolors[i-1], markersize=3, capsize=0, markeredgewidth=0.5, label=sublabel, elinewidth=0.8, capthick=0.5)
        else :
            print('Bad plot data format!!!\n')
            print('Your subdata in dataset should in the format *x  y  yerror*\n')
        factor +=1

    if len(text) != 0:
        for text_row in text:
            ax.text(text_row[1], text_row[2], text_row[0], fontsize=12)
    try:
        plttitle = datalabel[0]
    except:
        plttitle = 'Datapoint' + int(str(np.random.uniform(0,1000)))
    
    plt.legend(loc=0,prop={'size': 9},ncol=2,frameon=False,handletextpad=0.001)
    #plt.title(plttitle, fontsize=10)
    #plt.show()
    ficturename = folder + plttitle + '.pdf'
    #plt.figure(figsize=(10, 8)) 
    if logscale == True:
        ax.set_yscale('log')
    plt.xlabel(xylabel[0],fontsize=15)
    plt.ylabel(xylabel[1],fontsize=15)
    #plt.xlim([1.5,20.5])
    plt.ylim(yrange)
    #my_x_ticks = np.arange(2, 22, 2)
    #plt.xticks(my_x_ticks)
    tick_params(direction='in')
    #plt.show()
    print(ficturename,'\n')
    plt.savefig(ficturename)
