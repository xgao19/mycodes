#!/usr/bin/env python3
import numpy as np
import numba as nb
import traceback 
import sys
from tools import *
import inspect
from scipy.optimize import least_squares
from scipy.optimize import root

from rITD_Numerical_Kernel import *

'''----------------------------------'''
'''--------------kernels-------------'''
'''----------------------------------'''

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

alphas_NNLO_data = np.loadtxt('/Users/Xiang/Desktop/docs/0-2020/papers/2020-4-Pion_qpdf_PhysicalMass_qPDF_v0/analysis/rITD/moments/NNLO_evo_cmp/alpha_s/alphas_NNLO.txt')
mulist = [alphas_NNLO_data[i][0] for i in range(0, len(alphas_NNLO_data))]
alpslist = [alphas_NNLO_data[i][1] for i in range(0, len(alphas_NNLO_data))]

'''
# mu [fm]
@nb.jit
def alpsmu(mu):
    #alps0 = 0.24
    #mu0 = 3.2*5.0676896
    alps0 = 0.296
    mu0 = 2*5.0676896
    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
'''

# mu [fm]
@nb.jit
def alpsmu(mu):
    mu /= 5.0676896
    beta0 = 0.716197
    beta1 = 0.405285
    beta2 = 0.324447
    beta3 = 0.484842
    LL = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*LL) - 1/(beta0**2*LL**2)*beta1/beta0*np.log(LL) + 1/(beta0**3*LL**3)*((beta1/beta0)**2*(np.log(LL)**2-np.log(LL) - 1) + beta2/beta0) \
        + 1/(beta0**4*LL**4)*((beta1/beta0)**3 * (-np.log(LL)**3 + 5/2*np.log(LL)**2 + 2*np.log(LL) - 1/2) - (3*beta1*beta2)/beta0**2 * np.log(LL) + beta3/(2*beta0))
    return alpha_s

def alpsmuNNLO(mu):
    f = interp1d(mulist, alpslist)
    return f(mu)

def Cn_NLO(z,n, mu):
    alps = alpsmu(mu)
    CF = 4/3
    gammaE = 0.5772156649
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

def Cn_NNLO(z, n, mu, alps = 0):
    if alps == 0:
        alps = alpsmu(mu)
    #print(z, n, mu/5.0676896, alps,CnNNLO(z, n, mu, alps))
    return CnNNLO(z, n, mu, alps)

def CnC0_NLO(mu,z,n):
    return Cn_NLO(z,n, mu)/Cn_NLO(z,0, mu)
def CnC0_NNLO(mu,z,n):
    return Cn_NNLO(z, n, mu)/Cn_NNLO(z, 0, mu)


'''----------------------------------'''
'''----------renormalization---------'''
'''----------------------------------'''

# bm (bare matrix elements) is a 1-D array. bm[z]
# dm : delta_m calculated from Polyakov loop
# dm [GeV]; a [fm]; zs [1]
def Poly_renorm(bm_up, bm_down, dm, a, zup, zs):
    fmGeV = 5.0676896
    return bm_up/bm_down*np.exp((zup-zs)*a*dm*fmGeV)

#
def Poly_renorm_Log_Diff(bm_up, bm_down, dm, a, zup, zs, mu, pz=0, mom2=0):

    fmGeV = 5.0676896
    mu = mu*fmGeV
    #mom2 = 0
    Zv = 1
    #OPEup = Cn_NNLO(zup*a,0,mu)
    OPEup = Cn_NNLO(zup*a,0,mu) - Cn_NNLO(zup*a,2,mu) * mom2 * (zup*a*fmGeV*pz)**2/2
    OPEdown = Cn_NNLO(zs*a,0,mu)
    left = np.log(Poly_renorm(bm_up, bm_down, dm, a, zup, zs)*Zv)
    right = np.log(OPEup/OPEdown)
    return left-right

def Boots_Poly_renorm(sampleup_format, sampledown_format, pzlist, zlist, dm, a, zs):

    zs_sampledown_format = sampledown_format.replace('#',str(zs))
    zs_sampledown = np.loadtxt(zs_sampledown_format)
    npick = len(zs_sampledown)
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)
    print('npick:', npick)

    # sample collection
    PolyRbm_sample = []
    PolyRbmLog_sample = []
    for ipz in range(0, len(pzlist)):
        ipz_sampleup_format = sampleup_format.replace('*',str(pzlist[ipz]))
        ipz_PolyRbm_sample = []
        ipz_PolyRbmLog_sample = []

        for iz in range(0, len(zlist)):
            zup = zlist[iz]
            iz_sampleup_format = ipz_sampleup_format.replace('#',str(zup))
            iz_sampleup = np.loadtxt(iz_sampleup_format)
            #print(np.shape(iz_sampleup_data),iz_sampleup_data[0])
            iz_PolyRbm_sample = [zup]
            iz_PolyRbmLog_sample = [zup]
            for isamp in range(0, npick):
                iz_PolyRbm_sample += [Poly_renorm(iz_sampleup[isamp], zs_sampledown[isamp], dm, a, zup, zs)]
                iz_PolyRbmLog_sample += [np.log(Poly_renorm(iz_sampleup[isamp], zs_sampledown[isamp], dm, a, zup, zs))]
            ipz_PolyRbm_sample += [iz_PolyRbm_sample]
            ipz_PolyRbmLog_sample += [iz_PolyRbmLog_sample]
        PolyRbm_sample += [ipz_PolyRbm_sample]
        PolyRbmLog_sample += [ipz_PolyRbmLog_sample]
    print('Collected Hybrid renormalized samples:', np.shape(PolyRbm_sample))

    # error collection
    PolyRbm_popt = []
    for ipz in range(0, len(pzlist)):
        ipz_PolyRbm_popt = []

        for iz in range(0, len(zlist)):
            #print(ipz,iz,PolyRbm_sample[0][0])
            iz_PolyRbm_sample = sorted(PolyRbm_sample[ipz][iz][1:])
            iz_PolyRbmLog_sample = sorted(PolyRbmLog_sample[ipz][iz][1:])
            # z, zpz, ...
            iz_PolyRbm_popt = [PolyRbm_sample[ipz][iz][0]*a, pzlist[ipz]*zlist[iz]*2*3.14259/Ns, (iz_PolyRbm_sample[high16]+iz_PolyRbm_sample[low16])/2, (iz_PolyRbm_sample[high16]-iz_PolyRbm_sample[low16])/2]
            iz_PolyRbm_popt += [(iz_PolyRbmLog_sample[high16]+iz_PolyRbmLog_sample[low16])/2, (iz_PolyRbmLog_sample[high16]-iz_PolyRbmLog_sample[low16])/2]
            ipz_PolyRbm_popt += [iz_PolyRbm_popt]
            #print(iz_PolyRbm_popt)
        PolyRbm_popt += [ipz_PolyRbm_popt]
    print('Collected Hybrid renormalized errors:', np.shape(PolyRbm_popt),'\n')
    return PolyRbm_popt, PolyRbm_sample

def Boots_Poly_renorm_Log(sampleup_format, sampledown_format, pzlist, zlist, dm, a, zs):

    zs_sampledown_format = sampledown_format.replace('#',str(zs))
    zs_sampledown = np.loadtxt(zs_sampledown_format)
    npick = len(zs_sampledown)
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)
    print('npick:', npick)

    # sample collection
    PolyRbm_sample = []
    for ipz in range(0, len(pzlist)):
        ipz_sampleup_format = sampleup_format.replace('*',str(pzlist[ipz]))
        ipz_PolyRbm_sample = []

        for iz in range(0, len(zlist)):
            zup = zlist[iz]
            iz_sampleup_format = ipz_sampleup_format.replace('#',str(zup))
            iz_sampleup = np.loadtxt(iz_sampleup_format)
            #print(np.shape(iz_sampleup_data),iz_sampleup_data[0])
            iz_PolyRbm_sample = [zup]
            for isamp in range(0, npick):
                iz_PolyRbm_sample += [np.log(Poly_renorm(iz_sampleup[isamp], zs_sampledown[isamp], dm, a, zup, zs))]
            #print(np.shape())
            ipz_PolyRbm_sample += [iz_PolyRbm_sample]
        PolyRbm_sample += [ipz_PolyRbm_sample]
    print('Collected Hybrid renormalized samples:', np.shape(PolyRbm_sample))

    # error collection
    PolyRbm_popt = []
    for ipz in range(0, len(pzlist)):
        ipz_PolyRbm_popt = []

        for iz in range(0, len(zlist)):
            iz_PolyRbm_sample = sorted(PolyRbm_sample[ipz][iz][1:])
            # z, zpz, ...
            iz_PolyRbm_popt = [PolyRbm_sample[ipz][iz][0]*a, pzlist[ipz]*zlist[iz]*2*3.14259/Ns, (iz_PolyRbm_sample[high16]+iz_PolyRbm_sample[low16])/2, (iz_PolyRbm_sample[high16]-iz_PolyRbm_sample[low16])/2]
            ipz_PolyRbm_popt += [iz_PolyRbm_popt]
            #print(iz_PolyRbm_popt)
        PolyRbm_popt += [ipz_PolyRbm_popt]
    print('Collected Hybrid renormalized errors:', np.shape(PolyRbm_popt),'\n')
    return PolyRbm_popt, PolyRbm_sample

def Boots_Poly_renorm_Log_Diff(sampleup_format, sampledown_format, pzlist, zlist, dm, a, zs, mu, Ns):

    fmGeV = 5.0676896

    zs_sampledown_format = sampledown_format.replace('#',str(zs))
    zs_sampledown = np.loadtxt(zs_sampledown_format)
    npick = len(zs_sampledown)
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)
    print('npick:', npick)

    # sample collection
    PolyRbm_sample = []
    dm0_sample = []
    dm2_sample = []
    for ipz in range(0, len(pzlist)):
        ipz_sampleup_format = sampleup_format.replace('*',str(pzlist[ipz]))
        ipz_PolyRbm_sample = []
        ipz_dm0_sample = []
        ipz_dm2_sample = []

        zs_sampleup_format = ipz_sampleup_format.replace('#',str(zs))
        zs_sampleup = np.loadtxt(zs_sampleup_format)

        for iz in range(0, len(zlist)):
            zup = zlist[iz]
            iz_sampleup_format = ipz_sampleup_format.replace('#',str(zup))
            iz_sampleup = np.loadtxt(iz_sampleup_format)
            #print(np.shape(iz_sampleup_data),iz_sampleup_data[0])
            iz_PolyRbm_sample = [zup]
            iz_dm0_sample = [zup]
            iz_dm2_sample = [zup]
            moms_samples = []
            for isamp in range(0, npick):
                #isamp_PolyRbm = Poly_renorm_Log_Diff(iz_sampleup[isamp][0], zs_sampledown[isamp][0], dm, latsp, zup, zs, mu)
                def findRoot(moms2):
                    return Poly_renorm_Log_Diff(zs_sampleup[isamp], zs_sampledown[isamp], dm, a, zs, zs, mu, 2*np.pi*pzlist[ipz]/(a*Ns)/fmGeV, moms2)
                #print(2*np.pi*pzlist[ipz]/(a*Ns)/fmGeV,findRoot(0.12))
                moms2 = root(findRoot,0, method='lm').x[0]
                moms_samples += [moms2]
                #print(moms2)
                isamp_PolyRbm = Poly_renorm_Log_Diff(iz_sampleup[isamp], zs_sampledown[isamp], dm, a, zup, zs, mu, 2*np.pi*pzlist[ipz]/(a*Ns)/fmGeV, moms2)
                iz_PolyRbm_sample += [isamp_PolyRbm]
                iz_dm0_sample  += [-isamp_PolyRbm/(zup*a-zs*a)/fmGeV]
                if iz == 0 or iz == 1:
                    #iz_dm0_sample += [0]
                    iz_dm2_sample += [0]
                else:
                    #iz_dm0_sample  += [-(iz_PolyRbm_sample[isamp+1]-ipz_PolyRbm_sample[iz-1][isamp+1])/a/fmGeV]
                    iz_dm2_sample  += [(iz_dm0_sample[isamp+1]-ipz_dm0_sample[iz-1][isamp+1])/a/fmGeV]
            #print(np.shape())
            ipz_PolyRbm_sample += [iz_PolyRbm_sample]
            ipz_dm0_sample += [iz_dm0_sample]
            ipz_dm2_sample += [iz_dm2_sample]

            moms_samples_sort = sorted(moms_samples)
            #print(moms_samples_sort[mid],(moms_samples_sort[high16]-moms_samples_sort[low16])/2)

        PolyRbm_sample += [ipz_PolyRbm_sample]
        dm0_sample += [ipz_dm0_sample]
        dm2_sample += [ipz_dm2_sample]
    print('Collected Hybrid renormalized samples:', np.shape(PolyRbm_sample))

    # error collection
    PolyRbm_popt = []
    for ipz in range(0, len(pzlist)):
        ipz_PolyRbm_popt = []

        for iz in range(0, len(zlist)):
            iz_PolyRbm_sample = sorted(PolyRbm_sample[ipz][iz][1:])
            iz_PolyRbm_ave = (iz_PolyRbm_sample[high16]+iz_PolyRbm_sample[low16])/2
            iz_PolyRbm_err = (iz_PolyRbm_sample[high16]-iz_PolyRbm_sample[low16])/2
            iz_dm0_sample = sorted(dm0_sample[ipz][iz][1:])
            iz_dm0_ave = (iz_dm0_sample[high16]+iz_dm0_sample[low16])/2
            iz_dm0_err = (iz_dm0_sample[high16]-iz_dm0_sample[low16])/2
            iz_dm2_sample = sorted(dm2_sample[ipz][iz][1:])
            iz_dm2_ave = (iz_dm2_sample[high16]+iz_dm2_sample[low16])/2
            iz_dm2_err = (iz_dm2_sample[high16]-iz_dm2_sample[low16])/2
            # z, zpz, ...
            #print(pzlist[ipz],zup)
            iz_PolyRbm_popt = [PolyRbm_sample[ipz][iz][0]*a, pzlist[ipz]*zlist[iz]*2*3.14259/Ns, iz_PolyRbm_ave, iz_PolyRbm_err, iz_dm0_ave, iz_dm0_err, iz_dm2_ave, iz_dm2_err]
            #iz_PolyRbm_popt += [OPE_SysErr[iz], np.sqrt(OPE_SysErr[iz]**2+iz_PolyRbm_err**2)]
            ipz_PolyRbm_popt += [iz_PolyRbm_popt]
        PolyRbm_popt += [ipz_PolyRbm_popt]
    print('Collected Hybrid renormalized errors:', np.shape(PolyRbm_popt),'\n')
    return PolyRbm_popt, PolyRbm_sample, dm0_sample, dm2_sample

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='rbm':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        #zstep = latsp
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zstep':
                zstep = float(line[1])
                print('zstep:',zstep,'fm')

            elif line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*zstep,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))
                print('mu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))

            elif line[0] == 'pzrange':
                pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('pzlist:',pzlist)

            elif line[0] == 'zlist':
                zlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('zlist:','['+line[1]+', '+line[2]+']')

            elif line[0] == 'dma':
                dma = float(line[1])
                dm = dma/(latsp*fmGeV)
                print('dm*a:',dma, ', dm:', round(dm,4), 'GeV')

            elif line[0] == 'sampleup':
                sampleup_format = line[1]
                print('\nsampleup:', sampleup_format.split('/')[-1])
            elif line[0] == 'sampledown':
                sampledown_format = line[1]
                print('sampledown:', sampledown_format.split('/')[-1],'\n')

        PolyRbm_popt, PolyRbm_sample = Boots_Poly_renorm(sampleup_format, sampledown_format, pzlist, zlist, dm, zstep, zs)
        for ipz in range(0, len(pzlist)):
            ipz_describe = describe.replace('*',str(pzlist[ipz]))
            print('>>> Saving:',ipz_describe)
            save_name = outfolder + ipz_describe + '.txt'
            sample_name = samplefolder + ipz_describe + '_bootssamples.txt'
            np.savetxt(save_name, PolyRbm_popt[ipz], fmt='%.8e')
            np.savetxt(sample_name, PolyRbm_sample[ipz], fmt='%.8e')

        '''
        PolyRbm_popt, PolyRbm_sample = Boots_Poly_renorm_Log(sampleup_format, sampledown_format, pzlist, zlist, dm, zstep, zs)
        for ipz in range(0, len(pzlist)):
            ipz_describe = describe.replace('*',str(pzlist[ipz]))
            print('>>> Saving:',ipz_describe)
            save_name = outfolder + ipz_describe + '_Log.txt'
            sample_name = samplefolder + ipz_describe + '_Log_bootssamples.txt'
            np.savetxt(save_name, PolyRbm_popt[ipz])
            np.savetxt(sample_name, PolyRbm_sample[ipz])
        '''

        if len(pzlist) == 1:
            PolyRbm_popt, PolyRbm_sample, m0_sample, m2_sample = Boots_Poly_renorm_Log_Diff(sampleup_format, sampledown_format, pzlist, zlist, dm, zstep, zs,mu, Ns)
            for ipz in range(0, len(pzlist)):
                ipz_describe = describe.replace('*',str(pzlist[ipz]))
                print('>>> Saving:',ipz_describe)
                save_name = outfolder + ipz_describe + '_LogDiff.txt'
                np.savetxt(save_name, PolyRbm_popt[ipz], fmt='%.8e')
                sample_name = samplefolder + ipz_describe + '_LogDiff_bootssamples.txt'
                #np.savetxt(sample_name, PolyRbm_sample[ipz])
                sample_name = samplefolder + ipz_describe + '_LogDiffm0_bootssamples.txt'
                #np.savetxt(sample_name, m0_sample[ipz])
                sample_name = samplefolder + ipz_describe + '_LogDiffm2_bootssamples.txt'
                #np.savetxt(sample_name, m2_sample[ipz])
        print('\nDone.')