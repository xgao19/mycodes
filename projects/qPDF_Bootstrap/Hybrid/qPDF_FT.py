#!/usr/bin/env python3
import numpy as np
import numba as nb
import traceback 
from scipy.optimize import least_squares
from scipy.optimize import minimize
from scipy import integrate
from matplotlib import pyplot as plt

#-------------------------------------------------#
#---------------------  DFT  ---------------------#
#-------------------------------------------------#

# data is a 2-d array.
# data[z][0] have z from 0 to zmax in fm.
# data[z][1] is the matrix elements.
# pz in fm
def qPDF_DFT_real(x, pz, data):
    a = data[1][0] - data[0][0]
    qPDF = pz*a/(2*np.pi) * data[0][1]
    for iz in range(1, len(data)):
        qPDF += 2*pz*a/(2*np.pi) * np.cos(x*pz*data[iz][0]) * data[iz][1]
    return qPDF

#-------------------------------------------------#
#----------------------  BG  ---------------------#
#-------------------------------------------------#

# data is a 2-d array.
# data[z][0] have z from 0 to zmax in fm.
# data[z][1] is the matrix elements.
# pz in fm
# pre0 x**(-0.1)*(1 - x)
# pre1 (1 - x)**2
# pre2 1/x
xlow = 0.0001
xup = 1
def pre_qPDF(x):
    return 1/x
def K(x,z,pz):
    return 2*np.cos(x*pz*z)*pre_qPDF(x)
def uz(z, pz, xmin=xlow, xmax=xup):
    return integrate.quad(K, xmin, xmax, args=(z,pz))[0]
def Mzz(xb, z1, z2, pz, xmin=xlow, xmax=xup):
    def func(x):
        return (x-xb)**2 * K(x,z1,pz) * K(x,z2,pz)
    return integrate.quad(func, xmin, xmax)[0]

def qPDF_BG_real(xb, pz, data):

    a = data[1][0] - data[0][0]
    zlist = [row[0] for row in data]
    hz = np.array([row[1] for row in data])
    zlen = len(data)

    u = np.array([uz(z, pz) for z in zlist])

    M = np.zeros((zlen,zlen))
    for iz1 in range(0, len(zlist)):
        for iz2 in range(0, len(zlist)):
            M[iz1][iz2] = Mzz(xb, zlist[iz1], zlist[iz2], pz)
        M[iz1][iz1] += 0.0001

    Minv = np.linalg.inv(M)
    temp1 = np.dot(Minv, u)
    temp2 = np.dot(u, temp1)
    T = 1/temp2*temp1
    qPDF = np.sum(T*hz)
    sigmax = np.dot(np.transpose(T), np.dot(M, T))

    return qPDF,sigmax

#-------------------------------------------------#
#------------  Bayes-Gauss inference  ------------#
#-------------------------------------------------#
@nb.njit
def Bayes_covSE(dz,l,sigma):
    return sigma*sigma * np.exp(-dz*dz / (2*l*l))
@nb.njit
def qPDF_matrix_prior(z):
    return 0
@nb.njit
def qPDF_matrix_lkhd_covSE(z, h, herr, l, sigma):
    #cov matrix
    cov_kp = np.zeros((len(z), len(z)))
    for i in range(0, len(z)):
        for j in range(0, len(z)):
            cov_kp[i][j] = Bayes_covSE(abs(z[i]-z[j]),l,sigma)
        cov_kp[i][i] += herr[i]**2
    cov_kp_inv = np.linalg.inv(cov_kp)
    lkhd = -0.5*np.dot(np.dot(np.transpose(h), cov_kp_inv), h) - 0.5*np.log(np.linalg.det(cov_kp)) - 0.5*len(z)*np.log(2*np.pi)
    #lkhd = -0.5*np.dot(np.dot(np.transpose(h), cov_kp_inv), h)
    return lkhd
def qPDF_matrix_covSE(z, h, herr):
    def lkhd(popt):
        return -qPDF_matrix_lkhd_covSE(z, h, herr, popt[0], popt[1])

    for i in range(0, 100):
        try:
            popt_0 = np.random.rand()*1
            popt_1 = np.random.rand()*1
            #res = least_squares(lkhd, [popt_0,popt_1],method='trf')
            res = minimize(lkhd, [popt_0,popt_1])
            break
        except:
            traceback.print_exc()
    #print([popt_0,popt_1],res.x)
    def cov(dz):
        return Bayes_covSE(dz,res.x[0],res.x[1])
    return cov
def qPDF_matrix_BayesInfer(z, h, herr):

    h = np.array([h[i] - qPDF_matrix_prior(z[i]) for  i in range(0, len(z))])
    cov_kp_func = qPDF_matrix_covSE(z, h, herr)

    cov_kp = np.zeros((len(z), len(z)))
    for i in range(0, len(z)):
        for j in range(0, len(z)):
            cov_kp[i][j] = cov_kp_func(abs(z[i]-z[j]))
        cov_kp[i][i] += herr[i]**2
    cov_kp_inv = np.linalg.inv(cov_kp)
    kinv_h = np.dot(cov_kp_inv, h)

    def qPDF_matrix(x):
        v = np.zeros(len(z))
        for i in range(0, len(z)):
            v[i] = cov_kp_func(abs(x-z[i]))
        return qPDF_matrix_prior(x) + np.dot(np.transpose(v), kinv_h)
    return qPDF_matrix

#-------------------------------------------------#
#-------------  Model extrapolation  -------------#
#-------------------------------------------------#



'''
fmGeV = 5.0676896
pz = 2 * fmGeV
hzlist = np.loadtxt('/Users/Xiang/Desktop/docs/0-2021/analysis/0-hybrid-FTtest/mock-data/hz-test-2GeV.txt')
zmax = 0.4 #fm
#savename_hz = '/Users/Xiang/Desktop/docs/0-2021/analysis/0-hybrid-FTtest/Bayes-Gauss-FT/hz_zmax0p4_BGFT.txt'
savename = '/Users/Xiang/Desktop/docs/0-2021/analysis/0-hybrid-FTtest/Backus-Gilbert/qx_zmax0p4_BG_pre1.txt'

xlist = np.arange(0.01,1.01,0.05)
xlist = np.array(list(np.arange(0.01,1.01,0.05))+list(np.arange(1.1,2.01,0.1)))
xlist = np.array([1])
print('\nxlist:',np.shape(xlist))

a = hzlist[1][0]-hzlist[0][0]
hzlist_zmax = []
iz = 0
while iz*a <= zmax:
    hzlist_zmax += [[hzlist[iz][0],hzlist[iz][1]]]
    iz += 5
hzlist = hzlist_zmax
print('zmax:', (iz-1)*a, 'fm. ', 'Read hz in shape:', np.shape(hzlist))
'''


'''
# DFT
print('pz = ', pz/fmGeV, '. q(x=0) =', qPDF_DFT_real(0, pz, hzlist))
qx_DFT=qPDF_DFT_real(xlist, pz, hzlist)
np.savetxt(savename,np.transpose([list(xlist)]+[list(qx_DFT)]),fmt='%.6e')
'''

'''
#BG
for iz in range(0, len(hzlist)):
    if hzlist[iz][0] != 0:
        hzlist += [[-hzlist[iz][0],hzlist[iz][1]]]
qx_BG = []
qx_BG_sigma = []
for ix in xlist:
    print(ix)
    ix_result, ix_sigma = qPDF_BG_real(ix, pz, hzlist)
    qx_BG += [ix_result*pre_qPDF(ix)]
    qx_BG_sigma += [ix_sigma]
print('qx_BG:',qx_BG)
print('qx_BG_sigma:',qx_BG_sigma)
#np.savetxt(savename,np.transpose([list(xlist)]+[list(qx_BG)]),fmt='%.6e')
'''

'''
# BGFT
zlist = [hzlist[iz][0] for iz in range(0,len(hzlist))]
f=qPDF_matrix_BayesInfer(np.array(zlist), np.array([hzlist[i][1] for i in range(0, len(hzlist))]), np.array([1e-3 for iz in zlist]))
fzlist = []
for iz in list(np.arange(0, 5.01, 0.01)):
    fzlist += [[iz, f(iz)]]
np.savetxt(savename_hz,fzlist,fmt='%.6e')
qx_DFT=qPDF_DFT_real(xlist, pz, fzlist)
np.savetxt(savename,np.transpose([list(xlist)]+[list(qx_DFT)]),fmt='%.6e')
'''

'''
zlist = np.array([0.04, 0.08])
hzlist = np.array([9.93927e-01, 9.75048e-01])
hzErrlist = np.array([1.86867e-04, 7.28202e-04])
#hzErrlist = np.array([0, 0])
plt.errorbar(zlist, hzlist, hzErrlist, fmt='o',color='Orange', markerfacecolor='Orange', markersize=4, capsize=4, markeredgewidth=0.6, elinewidth=0.6, capthick=0.35)

f=qPDF_matrix_BayesInfer(zlist, hzlist, hzErrlist)
zlist = np.arange(0, 0.1, 0.002)
fzlist = np.arange(0, 0.1, 0.002)
for iz in range(0, len(zlist)):
    fzlist[iz] = f(zlist[iz])
plt.plot(zlist, fzlist)
plt.xlim([0.03, 0.09])
plt.ylim([0.96,1])
plt.savefig('/Users/Xiang/Desktop/docs/0-2021/analysis/0-hybrid-1/2GeV-Renorm-Pz-FT/Bayes-Gauss/prior-0/test.pdf')
'''

