#!/usr/bin/env python3
import numpy as np
import numba as nb
import math
import traceback 
import sys
from numpy.core.fromnumeric import sort

from scipy.optimize.minpack import curve_fit
from tools import *
import inspect
from scipy.optimize import least_squares
from scipy.interpolate import interp1d

from rITD_Numerical_Kernel import *

'''----------------------------------'''
'''--------------kernels-------------'''
'''----------------------------------'''

fmGeV = 5.0676896

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

'''
# mu [fm]
@nb.jit
def alpsmu(mu):
    #alps0 = 0.24
    #mu0 = 3.2*5.0676896
    alps0 = 0.296
    mu0 = 2*5.0676896
    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
'''


# mu [fm]
'''
@nb.jit
def alpsmu(mu):
    mu /= 5.0676896
    beta0 = 0.716197
    beta1 = 0.405285
    beta2 = 0.324447
    beta3 = 0.484842
    LL = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*LL) - 1/(beta0**2*LL**2)*beta1/beta0*np.log(LL) + 1/(beta0**3*LL**3)*((beta1/beta0)**2*(np.log(LL)**2-np.log(LL) - 1) + beta2/beta0) \
        + 1/(beta0**4*LL**4)*((beta1/beta0)**3 * (-np.log(LL)**3 + 5/2*np.log(LL)**2 + 2*np.log(LL) - 1/2) - (3*beta1*beta2)/beta0**2 * np.log(LL) + beta3/(2*beta0))
    return alpha_s
'''
def alpsmu(mu, Nf = 3):
    mu /= 5.0676896
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s

def Cn_NLO(z,n, mu):
    alps = alpsmu(mu)
    CF = 4/3
    gammaE = 0.5772156649
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

def Cn_NNLO(z, n, mu, alps = 0):
    if alps == 0:
        alps = alpsmu(mu)
    #print(z, n, mu/5.0676896, alps,CnNNLO(z, n, mu, alps))
    return CnNNLO(z, n, mu, alps)

def Cn_NNNLO(z, n, mu, alps = 0):
    if alps == 0:
        alps = alpsmu(mu)
    #print(z, n, mu/5.0676896, alps,CnNNLO(z, n, mu, alps))
    return CnNNNLO(z, n, mu, alps)


def CnC0_NLO(mu,z,n):
    return Cn_NLO(z,n, mu)/Cn_NLO(z,0, mu)
def CnC0_NNLO(mu,z,n):
    return Cn_NNLO(z, n, mu)/Cn_NNLO(z, 0, mu)


'''----------------------------------'''
'''----------renormalization---------'''
'''----------------------------------'''

# bm (bare matrix elements) is a 1-D array. bm[z]
# dm : delta_m calculated from Polyakov loop
# dm [GeV]; a [fm]; zs [1]
def Poly_renorm(bm_up, bm_down, dm, a, zup, zs):
    fmGeV = 5.0676896
    #Polymt = np.zeros(len(bm_up))
    #for iz in range(0, len(bm_up)):
    #    Polymt[iz] = bm_up[iz]/bm_down[zs]*np.exp((iz-zs)*a*dm*fmGeV)
    return bm_up/bm_down*np.exp((zup-zs)*a*dm*fmGeV)

def Poly_renorm_modeltest(dm0, k, l, tw1, tw2, tw3, tw4, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    #OPEup = 1 + tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a*fmGeV)**2)*(zup*a*fmGeV)**2 + cut2/zup/zup + cut1/zup
    #OPEdown = 1 + tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a*fmGeV)**2)*(zs*a*fmGeV)**2 + cut2/zs/zs + cut1/zs
    OPEup = 1 + k + l*(zup*a*fmGeV)**2 + tw1*np.log((zup*a*fmGeV)**2) + tw2*(np.log((zup*a*fmGeV)**2))**2 + tw3*(np.log((zup*a*fmGeV)**2))**3 + tw4*(np.log((zup*a*fmGeV)**2))**4
    OPEdown = 1 + k + l*(zs*a*fmGeV)**2 + tw1*np.log((zs*a*fmGeV)**2) + tw2*(np.log((zs*a*fmGeV)**2))**2 + tw3*(np.log((zs*a*fmGeV)**2))**3 + tw4*(np.log((zs*a*fmGeV)**2))**4
    return exp*(OPEup/OPEdown)

# renormalon and higher twist model
# mu [GeV]; zs[1]
def Poly_renorm_model1(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**4 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**4 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model2(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO(zup*a,0,mu) * (1 + tw1*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**4) + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NLO(zs*a,0,mu) * (1+ + tw1*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**4) + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)
def Poly_renorm_model1_ConTa(dm0, tw1, dis1, dis2, cut1, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    dm0 = dm0 + dis1 * a
    tw1 = tw1 + dis2 * a
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + cut1/zup/zup
    OPEdown = Cn_NLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + cut1/zs/zs
    return exp*(OPEup/OPEdown)
def Poly_renorm_model1_ConTb(dm0, tw1, dis1, dis2, cut1, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    dm0 = dm0 + dis1 * a * a
    tw1 = tw1 + dis2 * a * a
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + cut1/zup/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + cut1/zs/zs
    return exp*(OPEup/OPEdown)
'''
def Poly_renorm_model2(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2 + tw2*(zup*a*fmGeV)**4) + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2 + tw2*(zs*a*fmGeV)**4) + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)
'''
def Poly_renorm_model2_ConTa(dm0, tw1, dis1, dis2, cut1, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    dm0 = dm0 + dis1 * a
    tw1 = tw1 + dis2 * a
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2) + cut1/zup/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2) + cut1/zs/zs
    return exp*(OPEup/OPEdown)
def Poly_renorm_model2_ConTb(dm0, tw1, dis1, dis2, cut1, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    dm0 = dm0 + dis1 * a * a
    tw1 = tw1 + dis2 * a * a
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2) + cut1/zup/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2) + cut1/zs/zs
    return exp*(OPEup/OPEdown)

def Poly_renorm_model3(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) + tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2 + cut2/zup/zup + cut1/zup
    OPEdown = Cn_NNLO(zs*a,0,mu) + tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2 + cut2/zs/zs + cut1/zs
    return exp*(OPEup/OPEdown)


def Poly_renorm_model4(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    fmGeV = 5.0676896
    mu = mu*fmGeV
    exp = np.exp(-dm0*(zup-zs)*a*fmGeV)
    OPEup = Cn_NNLO(zup*a,0,mu) * np.exp(tw1*(zup*a*fmGeV)**2 + tw2*np.log((zup*a)**2)*(zup*a*fmGeV)**2) + cut1/zup/zup + cut2/zup
    OPEdown = Cn_NNLO(zs*a,0,mu)* np.exp(tw1*(zs*a*fmGeV)**2 + tw2*np.log((zs*a)**2)*(zs*a*fmGeV)**2) + cut1/zs/zs + cut2/zs
    return exp*(OPEup/OPEdown)


def Poly_renorm_model_dm0(dm0, tw1, tw2, cut1, cut2, a, zup, zs, mu):
    return -dm0*(zup-zs)*a*fmGeV


class model_list:

    modeltest = Poly_renorm_modeltest

    model1 = Poly_renorm_model1
    model1ConTa = Poly_renorm_model1_ConTa
    model1ConTb = Poly_renorm_model1_ConTb
    model2 = Poly_renorm_model2
    model2ConTa = Poly_renorm_model2_ConTa
    model2ConTb = Poly_renorm_model2_ConTb
    model3 = Poly_renorm_model3
    model4 = Poly_renorm_model4
    model_dm0 = Poly_renorm_model_dm0

class model_orders:
    NLO = Poly_renorm_model1_ConTa
    NNLO = Poly_renorm_model1_ConTb

def hzConT_model_a(a, hzConT, k):
    return hzConT + k*a
def hzConT_model_a2(a, hzConT, k):
    return hzConT + k*a*a

class hzConT_model_list:

    hzConTa = hzConT_model_a
    hzConTa2 = hzConT_model_a2

'''----------------------------------'''
'''----------    #Read#     ---------'''
'''----------------------------------'''


def read_commands(filename, nParam):

    inputfile = open(filename,'r')
    commands = inputfile.readlines()

    # read parameters
    for index in range(0, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'lattice':
            Ns = int(line[1])
            Nt = int(line[2])
            latsp = float(line[3])
            print('Ensemble:',Ns,'x',Nt,', a =',latsp,'fm')

        elif line[0] == 'zs':
            zs = int(line[1])
            print('zs:',str(zs)+'a =',zs*latsp,'fm')

        elif line[0] == 'mass(GeV)':
            mass = float(line[1])
            print('mass:',mass,'GeV')

        elif line[0] == 'mean':
                mean = line[1]
                col_ave = int(line[2])
                col_err = int(line[3])
                print('mean:', mean.split('/')[-1])
                mean = mean.replace('zs#','zs'+str(zs))
                mean = np.loadtxt(mean)
                print('mean shape:', np.shape(mean))
                mean_ave = [mean[i][col_ave] for i in range(0, len(mean))]
                mean_err = [mean[i][col_err] for i in range(0, len(mean))]
                #mean_err[zs] = 1.0
                print('mean ave:',[round(mean_ave[i],5) for i in range(0, zs+1)],'...')
                print('mean err:',[round(mean_err[i],5) for i in range(0, zs+1)],'...')

        elif line[0] == 'sample':
            sample = line[1]
            print('sample:', sample.split('/')[-1])
            sample = sample.replace('zs#','zs'+str(zs))
            sample = np.loadtxt(sample)
            print('sample shape:', np.shape(sample))

            lattice = [Ns, Nt, latsp, mass]

    return lattice, zs, sample, mean_err

def interp_samples(samples, a):
    hz_samples = []
    zlist = [samples[iz][0]*a for iz in range(0, len(samples))]
    for isamp in range(0, len(samples[0])-1):
        isamp_hPR = [samples[iz][isamp+1] for iz in range(0, len(samples))]
        hz_samples += [interp1d(zlist, isamp_hPR, kind='cubic')]
        #print(isamp, isamp_hPR[0], hz_samples[isamp](0.03), isamp_hPR[1])
    return hz_samples

def hzConT_interp_samples(hzConT_model, hz_samples, lattice_list, zmin, zmax, zstep):

    npick = len(hz_samples[0])
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    latsp_list = [lattice_list[iLat][2] for iLat in range(0, len(lattice_list))]

    zlist = np.arange(0,zmax+zstep/2,zstep)
    ConT_hz_samples = []
    ConT_hz_errs = []
    for iz in range(0, len(zlist)):
        iz_hz_samples = []
        iz_hz_errs = []
        iz_ConT_hz_samples = [zlist[iz]]
        for iLat in range(0, len(lattice_list)):
            iLat_hz_samples = [hz_samples[iLat][isamp](zlist[iz]) for isamp in range(0, len(hz_samples[iLat]))]
            iz_hz_samples += [iLat_hz_samples]
            iLat_hz_samples_sort = sorted(iLat_hz_samples)
            iz_hz_errs += [(iLat_hz_samples_sort[high16]-iLat_hz_samples_sort[low16])/2]
        for isamp in range(0, len(iz_hz_samples[0])):
            isamp_samples = [iz_hz_samples[iLat][isamp] for iLat in range(0, len(lattice_list))]
            #print(zlist[iz],hz_samples[iLat][isamp](zlist[iz]),latsp_list, isamp_samples, iz_hz_errs)
            isamp_ConT_hz, isamp_popv = curve_fit(hzConT_model, latsp_list, isamp_samples, sigma=iz_hz_errs)
            iz_ConT_hz_samples += [isamp_ConT_hz[0]]
        ConT_hz_samples += [iz_ConT_hz_samples]
        iz_ConT_hz_samples_sort = sorted(iz_ConT_hz_samples)
        ConT_hz_errs += [(iz_ConT_hz_samples_sort[high16]-iz_ConT_hz_samples_sort[low16])/2]

    return zlist, ConT_hz_samples, ConT_hz_errs

def hz_interp_save(hz_samples, savename, zmax):

    npick = len(hz_samples)
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    zlist = np.arange(0, zmax+0.01, 0.01)
    hz_save_0 = []
    hz_save_1 = []
    hz_save_2 = []
    for izfm in zlist:
        iz_samples = [hz_samples[isamp](izfm) for isamp in range(0, npick)]
        iz_samples_sort = sorted(iz_samples)
        hz_save_0 += [[izfm, iz_samples_sort[low16]]]
        hz_save_1 += [[izfm, iz_samples_sort[mid]]]
        hz_save_2 += [[izfm, iz_samples_sort[high16]]]

    np.savetxt(savename+'_0.txt', hz_save_0)
    np.savetxt(savename+'_1.txt', hz_save_1)
    np.savetxt(savename+'_2.txt', hz_save_2)

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='ht':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()

        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        fmGeV = 5.0676896

        # read parameters
        lattice_list = []
        zs_list = []
        sample_list = []
        err_list = []
        Nfix = 0
        for index in range(1, len(commands)):
            
            line = commands[index].split()

            if line[0] == 'm0':
                m0_prior = float(line[1])
                m0_err = float(line[2])
                if m0_err < 1e5:
                    describe = describe + '_m0'
                    Nfix += 1
                print('m0 priored:', m0_prior, m0_err)

            elif line[0] == 'ht1':
                ht1_prior = float(line[1])
                ht1_err = float(line[2])
                if ht1_err < 1e5:
                    describe = describe + '_ht1'
                    Nfix += 1
                print('ht1 priored:', ht1_prior, ht1_err)

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                print('\n\n\n\nmu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))

            elif line[0] == 'model':
                models = [line[i] for i in range(1, len(line))]

            elif line[0] == 'nParam':
                nParam = int(line[1])
                print('Number of parameters:', nParam)
                describe = describe+'_nP'+str(nParam)

            elif line[0] == 'data':
                print('\n-------------------------------------------------')
                lattice, zs, sample, err = read_commands(line[1], nParam)
                lattice_list += [lattice]
                zs_list += [zs]
                sample_list += [sample]
                err_list += [err]
                print('-------------------------------------------------\n')

            elif line[0] == 'zrange':
                zmin = float(line[1])
                zmax = float(line[2])
                zstep = float(line[3])
                print(zmin, '[fm] to', zmax, '[fm] with interval', zstep, '[fm].')

        if len(zs_list) == 1 and (zs_list[0]+1)*lattice_list[0][2] > zmin:
            zmin = (zs_list[0]+1)*lattice_list[0][2]
        print('zmin is re-setted:',zmin)

        npick = len(sample[0])-1
        print('sample count:', npick, '\n')
        low16 = int((0.16*npick))
        high16 = int(np.ceil(0.84*npick))
        mid = int(0.5*npick)

        for imodel in models:

            fitfunc = getattr(model_list,imodel)
            imodel_describe = describe.replace('HTfit', imodel)
            
            def Fit_model(latsp, zup, zs, popt):
                fitpopt = [0, 0, 0, 0, 0]
                for i in range(0, len(popt)):
                    fitpopt[i] = popt[i]
                return fitfunc(fitpopt[0], fitpopt[1], fitpopt[2], fitpopt[3], fitpopt[4], latsp, zup, zs, mu)

            # fit loop
            poptlist = []
            izmax = zmin
            popt_samples = [[] for i in range(0, nParam)]
            while izmax <= zmax:
                zlist = []
                N_data = 0
                for iLat in range(0, len(lattice_list)):
                    iLat_latsp = lattice_list[iLat][2]
                    zlist += [[i for i in range(math.ceil(zmin/iLat_latsp), int(izmax/iLat_latsp)+1)]]
                    N_data += len(zlist[iLat])
                if N_data < nParam:
                    izmax += zstep
                    izmax = round(izmax,2)
                    continue

                poptstart = 0.01*np.ones(nParam)
                popt_sample = [[] for i in range(0, nParam)]
                chisq_sample = []
                
                for isamp in range(0, npick):
                    
                    def Fit_res(popt):
                        res = [(m0_prior-popt[0])/m0_err, (ht1_prior-popt[1])/ht1_err]
                        for iLat in range(0, len(lattice_list)):
                            iLat_zlist = zlist[iLat]
                            iLat_Ns = lattice_list[iLat][0]
                            iLat_Nt = lattice_list[iLat][1]
                            iLat_latsp = lattice_list[iLat][2]
                            iLat_mass = lattice_list[iLat][3]
                            ilat_zs = zs_list[iLat]
                            ilat_sample = sample_list[iLat]
                            ilat_err = err_list[iLat]
                            #print('???\n')
                            for zup in iLat_zlist:
                                #res += [(Fit_model(iLat_latsp, zup, ilat_zs, popt)-ilat_sample[zup][isamp+1])/(ilat_err[zup])]
                                res += [(Fit_model(iLat_latsp, zup, ilat_zs, popt)-ilat_sample[zup][isamp+1])/(ilat_err[zup]+ilat_sample[zup][isamp+1]*0.01)]
                        return res
                        
                    res = least_squares(Fit_res, poptstart, method='lm')
                    popt = res.x
                    #print(res.fun)
                    chisq = sum(np.square(res.fun[2:])) / (N_data-len(popt)+Nfix)
                    for i in range(0, nParam):
                        popt_sample[i] += [popt[i]]
                    chisq_sample += [chisq]

                for i in range(0, nParam):
                    popt_samples[i] += [[round(zmin,4), round(izmax,4)]+popt_sample[i]]
                    
                chisq = sorted(chisq_sample)[mid]
                izmax_poptlist = [round(zmin,4), round(izmax,4)]
                for i in range(0, nParam):
                    i_popt_sample = sorted(popt_sample[i])
                    izmax_poptlist += [(i_popt_sample[high16]+i_popt_sample[low16])/2,(i_popt_sample[high16]-i_popt_sample[low16])/2]
                izmax_poptlist += [chisq]
                print('z in',izmax_poptlist[:2], ', chisq/dof:',round(izmax_poptlist[-1],4))
                poptlist += [izmax_poptlist]
                izmax += zstep
            
            save_name = outfolder + imodel_describe + '.txt'
            print('\nSaving: ', save_name.split('/')[-1])
            np.savetxt(save_name, poptlist, fmt='%.5e')

            print(np.shape(popt_samples))
            save_name = samplefolder + imodel_describe + '_popt'
            for i in range(0, nParam):
                i_save_name = save_name + str(i) + '_boots.txt'
                np.savetxt(i_save_name, popt_samples[i], fmt='%.5e')

            '''
            zmax_poptlist = poptlist[-1]
            zmax_popt = [0, 0, 0, 0, 0]
            for i in range(0, nParam):
                zmax_popt[i] = zmax_poptlist[2*(i+1)]
            print('Saving plot of:',zmax_popt)
            zfm_list = np.linspace(0.01,1,200)
            mtx_list = Fit_model(zfm_list/latsp, zmax_popt)
            save_data = np.transpose([list(zfm_list), list(mtx_list)])

            '''
    elif sys.argv[1] =='hti':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()

        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        fmGeV = 5.0676896

        # read parameters
        lattice_list = []
        zs_list = []
        sample_list = []
        err_list = []
        hz_sample_list = []
        data_zmax_fm = 10
        zinput_list = []
        for index in range(1, len(commands)):
            
            line = commands[index].split()

            if line[0] == 'm0':
                m0_prior = float(line[1])
                m0_err = float(line[2])
                if m0_err < 1e5:
                    describe = describe + '_m0'
                print('m0 priored:', m0_prior, m0_err)

            elif line[0] == 'ht1':
                ht1_prior = float(line[1])
                ht1_err = float(line[2])
                if ht1_err < 1e5:
                    describe = describe + '_ht1'
                print('ht1 priored:', ht1_prior, ht1_err)

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                print('\n\n\n\nmu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))

            elif line[0] == 'hzConT':
                hzConT = line[1]
                print('Continuum extrapolation model:', hzConT)
                describe = describe.replace('ConT',hzConT)

            elif line[0] == 'model':
                models = [line[i] for i in range(1, len(line))]

            elif line[0] == 'nParam':
                nParam = int(line[1])
                print('Number of parameters:', nParam)
                describe = describe+'_nP'+str(nParam)

            elif line[0] == 'zrange':
                zmin = float(line[1])
                zmax = float(line[2])
                zstep = float(line[3])
                print(zmin, '[fm] to', zmax, '[fm] with step', zstep, '[fm].')

            elif line[0] == 'data':
                print('\n-------------------------------------------------')
                lattice, zs, sample, err = read_commands(line[1], nParam)
                latsp = lattice[2]
                Ns = lattice[0]
                sample_intp = interp_samples(sample, latsp)
                if (len(sample) - 1) * latsp < data_zmax_fm:
                    data_zmax_fm = (len(sample) - 1) * latsp
                zinput_list += [round(i*latsp/zstep) for i in range(0, math.ceil(Ns/2))]

                hz_sample_list += [sample_intp]
                lattice_list += [lattice]
                zs_list += [zs]
                sample_list += [sample]
                err_list += [err]
                #zsConT = zs
                zsConT = round(zs*lattice[2]/zstep)
                print('Reading data:',zs,lattice[2],zstep,zsConT)

                savename = outfolder + 'HISQ_hz_intp_a' + str(round(lattice[2],2)).replace('0.','')
                hz_interp_save(sample_intp, savename, data_zmax_fm)
                print('-------------------------------------------------\n')

        hzConT_model = getattr(hzConT_model_list, hzConT)
        hzConT_zlist, hzConT_samples, hzConT_errs = hzConT_interp_samples(hzConT_model, hz_sample_list, lattice_list, zmin, data_zmax_fm, zstep)
        hzConT_format = 'HISQaConT.pion.1hyp.g8.pz0.X#_bootssamples.real'
        print(hzConT_zlist)
        for iz in range(0, len(hzConT_samples)):
            save_name = hzConT_format.replace('#',str(iz))
            #print(save_name)
            np.savetxt(samplefolder + save_name, hzConT_samples[iz][1:],fmt='%.8e')
        sample_intp = interp_samples(hzConT_samples, 1)
        savename = outfolder + 'HISQ_hz_intp_a_' + hzConT
        hz_interp_save(sample_intp, savename, data_zmax_fm)
        print('Continuum extrapolated hz:', np.shape(hzConT_samples))

        npick = len(sample[0])-1
        print('sample count:', npick, '\n')
        low16 = int((0.16*npick))
        high16 = int(np.ceil(0.84*npick))
        mid = int(0.5*npick)
        #print(hz_sample_list)
        
        for imodel in models:

            fitfunc = getattr(model_list,imodel)
            imodel_describe = describe.replace('HTfit', imodel)
            
            def Fit_model(latsp, zup, zs, popt):
                fitpopt = [0, 0, 0, 0, 0]
                for i in range(0, len(popt)):
                    fitpopt[i] = popt[i]
                return fitfunc(fitpopt[0], fitpopt[1], fitpopt[2], fitpopt[3], fitpopt[4], latsp, zup, zs, mu)

            # fit loop
            poptlist = []
            poptlist_band0 = []
            poptlist_band1 = []
            poptlist_band2 = []
            popt_samples = [[] for i in range(0, nParam)]
            for izmax in range(int(zmin/zstep)+nParam-1, int(zmax/zstep)+1):
                zlist = []
                for iz in range(int(zmin/zstep), izmax+1):
                    zup = int(round(hzConT_zlist[iz]/zstep))
                    if zup in zinput_list:
                        zlist += [zup]
                #zlist = [int(round(hzConT_zlist[iz]/zstep)) for iz in range(int(zmin/zstep), izmax+1)]
                if len(zlist) < nParam:
                    continue
                print(izmax, [zlist[i]*zstep for i in range(0, len(zlist))])

                poptstart = np.zeros(nParam)
                poptstart[0] = 0.4
                poptstart[1] = -0.3
                izmax_popt_sample = [[] for i in range(0, nParam)]
                chisq_sample = []
                
                for isamp in range(0, npick):
                    
                    def Fit_res(popt):
                        res = [(m0_prior-popt[0])/m0_err, (ht1_prior-popt[1])/ht1_err]
                        for zup in zlist:
                            res += [(Fit_model(zstep, zup, zsConT, popt)-hzConT_samples[zup][isamp+1])/hzConT_errs[zup]]
                        return res
                    
                    res = least_squares(Fit_res, poptstart, method='lm')
                    popt = res.x
                    chisq = sum(np.square(res.fun)) / (len(zlist)-len(popt))
                    for i in range(0, nParam):
                        izmax_popt_sample[i] += [popt[i]]
                    chisq_sample += [chisq]
                    #print(isamp,popt)
                    
                chisq = sorted(chisq_sample)[mid]
                izmax_poptlist = [round(zmin,4), round(zlist[-1]*zstep,4)]
                izmax_poptlist_band0 = [round(zmin,4), round(zlist[-1]*zstep,4)]
                izmax_poptlist_band1 = [round(zmin,4), round(zlist[-1]*zstep,4)]
                izmax_poptlist_band2 = [round(zmin,4), round(zlist[-1]*zstep,4)]
                for i in range(0, nParam):
                    i_popt_sample = sorted(izmax_popt_sample[i])
                    izmax_poptlist += [(i_popt_sample[high16]+i_popt_sample[low16])/2,(i_popt_sample[high16]-i_popt_sample[low16])/2]
                    izmax_poptlist_band0 +=[i_popt_sample[low16]]
                    izmax_poptlist_band1 +=[i_popt_sample[mid]]
                    izmax_poptlist_band2 +=[i_popt_sample[high16]]
                izmax_poptlist += [chisq]
                print('z in',izmax_poptlist[:2], ', chisq/dof:',round(izmax_poptlist[-1],4))
                poptlist += [izmax_poptlist]
                izmax += zstep
                poptlist_band0 += [izmax_poptlist_band0]
                poptlist_band1 += [izmax_poptlist_band1]
                poptlist_band2 += [izmax_poptlist_band2]

                for i in range(0, nParam):
                    popt_samples[i] += [[round(zmin,4), round(zlist[-1]*zstep,4)]+izmax_popt_sample[i]]
            
            save_name = outfolder + imodel_describe + '.txt'
            print('\nSaving: ', save_name.split('/')[-1])
            np.savetxt(save_name, poptlist, fmt='%.5e')
            save_name = outfolder + imodel_describe + '_band0.txt'
            np.savetxt(save_name, poptlist_band0, fmt='%.5e')
            save_name = outfolder + imodel_describe + '_band1.txt'
            np.savetxt(save_name, poptlist_band1, fmt='%.5e')
            save_name = outfolder + imodel_describe + '_band2.txt'
            np.savetxt(save_name, poptlist_band2, fmt='%.5e')

            print(np.shape(popt_samples))
            save_name = samplefolder + imodel_describe + '_popt'
            for i in range(0, nParam):
                i_save_name = save_name + str(i) + '_boots.txt'
                np.savetxt(i_save_name, popt_samples[i], fmt='%.5e')

        '''
            zmax_poptlist = poptlist[-1]
            zmax_popt = [0, 0, 0, 0, 0]
            for i in range(0, nParam):
                zmax_popt[i] = zmax_poptlist[2*(i+1)]
            print('Saving plot of:',zmax_popt)
            zfm_list = np.linspace(0.01,1,200)
            mtx_list = Fit_model(zfm_list/latsp, zmax_popt)
            save_data = np.transpose([list(zfm_list), list(mtx_list)])

        '''

    elif sys.argv[1] =='rc':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        dm0_samples = []
        tw1_samples = []
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*latsp,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))
                print('mu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))

            elif line[0] == 'zlist':
                zlist = [i/10 for i in range(int(line[1]), int(line[2])*10+1)]
                zlist = np.array(zlist[1:])
                print('zlist:','['+line[1]+', '+line[2]+']')

            elif line[0] == 'order':
                order = line[1]
                print('Wilson coefficients order:', order)
                model = getattr(model_orders,order)

            elif line[0] == 'dm0':
                dm0_read = np.loadtxt(line[1].replace('mu','mu'+str(mu).replace('.','p')))
                dm0_line = int(line[2])
                dm0_samples  = dm0_read[dm0_line][2:]
                print('Read dm0 '+ str(len(dm0_samples)) +' samples from z in', dm0_read[dm0_line][:2],'e.g.',dm0_samples[0],'...')

            elif line[0] == 'tw1':
                tw1_read = np.loadtxt(line[1].replace('mu','mu'+str(mu).replace('.','p')))
                tw1_line = int(line[2])
                tw1_samples  = tw1_read[dm0_line][2:]
                print('Read tw1 '+ str(len(tw1_samples)) +' samples from z in', tw1_read[dm0_line][:2],'e.g.',tw1_samples[0],'...')

        npick = len(dm0_samples)
        print('sample count:', npick, '\n')
        low16 = int((0.16*npick))
        high16 = int(np.ceil(0.84*npick))
        mid = int(0.5*npick)

        rc_samples = []
        for isamp in range(0, npick):
           rc_samples += [model(dm0_samples[isamp], tw1_samples[isamp], 0, 0, 0, latsp, zlist, zs, mu)]

        rc_band0 = []
        rc_band1 = []
        rc_band2 = []
        for iz in range(0, len(zlist)):
            iz_samples = sorted([rc_samples[isamp][iz] for isamp in range(0, npick)])
            rc_band0 += [[zlist[iz]*latsp,iz_samples[low16]]]
            rc_band1 += [[zlist[iz]*latsp,iz_samples[mid]]]
            rc_band2 += [[zlist[iz]*latsp,iz_samples[high16]]]

        save_name0 = outfolder + describe + '_band0.txt'
        save_name1 = outfolder + describe + '_band1.txt'
        save_name2 = outfolder + describe + '_band2.txt'
        np.savetxt(save_name0, rc_band0, fmt='%.6e')
        np.savetxt(save_name1, rc_band1, fmt='%.6e')
        np.savetxt(save_name2, rc_band2, fmt='%.6e')