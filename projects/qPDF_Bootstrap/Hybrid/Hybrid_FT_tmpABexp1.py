#!/usr/bin/env python3
import numpy as np
import numba as nb
import time
import math
import sys
import os
from scipy.optimize import least_squares
from scipy.interpolate import interp1d
from scipy.special import gamma
from tools import *
from qPDF_FT import *
from scipy import integrate
from scipy.special import expn
from mpmath import hyp1f2
from sympy import uppergamma
from sympy import re
from mpmath import *
mp.dps = 25; mp.pretty = True

def ExpIntegralE(n, z):
    return (z**(n-1) * gammainc(1-n,z))

#print(ExpIntegralE(2, 0.2), expn(2, 0.2))
#print(ExpIntegralE(2, complex(1,0.9)), expn(2, complex(1,0.9)))

#print(re(ExpIntegralE(2, complex(1,0.9))))
#print(hyp1f2(2,3,4,50))

# model large z
def hz_extra_exp_0(z, A, c, d):
    return abs(A) * np.exp(-(abs(c)+0*0.506769)*z) * z**(-abs(d))
def hz_extra_exp_1(z, A, c, d):
    return abs(A) * np.exp(-(abs(c)+1*0.506769)*z) * z**(-abs(d))
def hz_extra_exp_2(z, A, c, d):
    return abs(A) * np.exp(-(abs(c)+2*0.506769)*z) * z**(-abs(d))

def hz_extra_powLaw_0(z, A, c, d):
    return abs(A) * z**(-abs(d))

#def hz_extra_abAsymp_0(z, a, b, c, d):
#    return np.exp(-(abs(d)+0*0.506769)*z) * c*(gamma(1+a)/(complex(0,-z)**(a+1)) + np.exp(complex(0,z))*gamma(1+b)/(complex(0,z)**(b+1))).real

#def hz_extra_abAsymp_0(z, a, b, c1, c2, d):
#    return np.exp(-(abs(d)+0*0.506769)*z) * (c1/(complex(0,-z)**(a+1)) + np.exp(complex(0,z))*c2/(complex(0,z)**(b+1))).real
def hz_extra_abAsymp_0(z, a, b, c, d):
    b = abs(b)
    c = abs(c)
    d = abs(d)+0*0.506769
    return float(np.exp(-d*z) * c * (gamma(1+a)/(complex(0,-z)**(a+1)) + np.exp(complex(0,z))*gamma(1+b)/(complex(0,z)**(b+1))).real)
def hz_extra_abAsymp_1(z, a, b, c, d):
    b = abs(b)
    c = abs(c)
    d = abs(d)+1*0.506769
    return float(np.exp(-d*z) * c * (gamma(1+a)/(complex(0,-z)**(a+1)) + np.exp(complex(0,z))*gamma(1+b)/(complex(0,z)**(b+1))).real)
def hz_extra_abAsymp_2(z, a, b, c, d):
    b = abs(b)
    c = abs(c)
    d = abs(d)+2*0.506769
    return float(np.exp(-d*z) * c * (gamma(1+a)/(complex(0,-z)**(a+1)) + np.exp(complex(0,z))*gamma(1+b)/(complex(0,z)**(b+1))).real)
def qPDF_extra_abAsymp_0(xPz, z, a, b, c, d):
    b = abs(b)
    c = abs(c)
    d = abs(d)+0*0.506769
    return -c*re(complex(0,-1)**(1-a) * z**(-a) * ExpIntegralE(1+a, complex(d, -xPz)*z) * gamma(1+a) + complex(0,1)**(1-b) * z**(-b) * ExpIntegralE(1+b, complex(d, -(1 + xPz))*z) * gamma(1 + b))
def qPDF_extra_abAsymp_1(xPz, z, a, b, c, d):
    b = abs(b)
    c = abs(c)
    d = abs(d)+1*0.506769
    return -c*re(complex(0,-1)**(1-a) * z**(-a) * ExpIntegralE(1+a, complex(d, -xPz)*z) * gamma(1+a) + complex(0,1)**(1-b) * z**(-b) * ExpIntegralE(1+b, complex(d, -(1 + xPz))*z) * gamma(1 + b))
def qPDF_extra_abAsymp_2(xPz, z, a, b, c, d):
    b = abs(b)
    c = abs(c)
    d = abs(d)+2*0.506769
    return -c*re(complex(0,-1)**(1-a) * z**(-a) * ExpIntegralE(1+a, complex(d, -xPz)*z) * gamma(1+a) + complex(0,1)**(1-b) * z**(-b) * ExpIntegralE(1+b, complex(d, -(1 + xPz))*z) * gamma(1 + b))
class model_list:

    model_exp_0 = hz_extra_exp_0
    model_exp_1 = hz_extra_exp_1
    model_exp_2 = hz_extra_exp_2

    model_powLaw_0 = hz_extra_powLaw_0

    model_abAsymp_0 = hz_extra_abAsymp_0
    model_abAsymp_1 = hz_extra_abAsymp_1
    model_abAsymp_2 = hz_extra_abAsymp_2
    modelqPDF_abAsymp_0 = qPDF_extra_abAsymp_0
    modelqPDF_abAsymp_1 = qPDF_extra_abAsymp_1
    modelqPDF_abAsymp_2 = qPDF_extra_abAsymp_2

fitfunc = getattr(model_list, 'model_abAsymp_1')
qPDFfunc = getattr(model_list, 'modelqPDF_abAsymp_1')

'''
def hz_extra_ab(zpz, a, b, c):
    part1 = math.sin(np.pi*a)*gamma(a+b+2)*gamma(-a)*math.sin(np.pi*b/2-zpz)/np.pi/(zpz**(1+b))
    part2 = ((a+1)*b/math.sin(np.pi*a/2) - zpz/math.cos(np.pi*a/2)/2/gamma(1+b)/(zpz**(2+a)))
    return part1 + part2
'''

# mu [fm]
'''
@nb.jit
def alpsmu(mu):
    mu /= 5.0676896
    beta0 = 0.716197
    beta1 = 0.405285
    beta2 = 0.324447
    beta3 = 0.484842
    LL = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*LL) - 1/(beta0**2*LL**2)*beta1/beta0*np.log(LL) + 1/(beta0**3*LL**3)*((beta1/beta0)**2*(np.log(LL)**2-np.log(LL) - 1) + beta2/beta0) \
        + 1/(beta0**4*LL**4)*((beta1/beta0)**3 * (-np.log(LL)**3 + 5/2*np.log(LL)**2 + 2*np.log(LL) - 1/2) - (3*beta1*beta2)/beta0**2 * np.log(LL) + beta3/(2*beta0))
    return alpha_s
'''

@nb.jit
def alpsmu(mu, Nf = 3):
    mu /= 5.0676896
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":

    if sys.argv[1] =='bgft':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        #zstep = latsp
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zstep':
                zstep = float(line[1])
                zsave = np.arange(0, 10, zstep)
                print('zstep:',zstep,'fm')

            elif line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*latsp,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))
                print('mu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))

            elif line[0] == 'pzrange':
                pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('pzlist:',pzlist)

            elif line[0] == 'dzL':
                #zmaxlist = [int(line[i]) for i in range(1, len(line))]
                #print('zmaxlist:', zmaxlist)
                dzL = int(line[1])
                print('dzL:', dzL*latsp, 'fm.')

            elif line[0] == 'mean':
                mean_format = line[1]
                mean_format = mean_format.replace('#',str(zs))
                mean_format = mean_format.replace('mu','mu'+str(mu).replace('.','p'))
                print('\nmean:', mean_format.split('/')[-1])

            elif line[0] == 'sample':
                sample_format = line[1]
                sample_format = sample_format.replace('#',str(zs))
                sample_format = sample_format.replace('mu','mu'+str(mu).replace('.','p'))
                print('sample:', sample_format.split('/')[-1],'\n')
        
        for ipz in range(0, len(pzlist)):

            ipz_describe = describe.replace('*', str(pzlist[ipz]))

            ipz_mean = np.loadtxt(mean_format.replace('*',str(pzlist[ipz])))
            ipz_samples = np.loadtxt(sample_format.replace('*',str(pzlist[ipz])))

            zmax = int(Ns/2)
            for iz in range(0, int(Ns/2)):
                #print(ipz_mean[iz][0])
                if ipz_mean[iz][2] <=0:
                    zmax = iz-1
                    break
            zmaxlist = [zmax-2*dzL,zmax-dzL,zmax]
            ipz_zmaxlist = zmaxlist

            npick = len(ipz_samples[0]) - 1
            low16 = int((0.16*npick))
            high16 = int(np.ceil(0.84*npick))
            mid = int(0.5*npick)
            ipz_fm = round(2*np.pi*pzlist[ipz]/(Ns*latsp),2)
            print('\npz =', ipz_fm, '[fm^-1]; npick:', npick)
            print('zmax is', zmax, ipz_mean[zmax][2])
            print('ipz zmax list:', ipz_zmaxlist)
            
            for izmax in ipz_zmaxlist:

                print('>> zmax =', izmax)

                izmax_describe = ipz_describe.replace('zmax','zmax'+str(izmax))

                ipz_zlist = [ipz_mean[iz][0] for iz in range(0, izmax+1)]
                ipz_err = [ipz_mean[iz][-1] for iz in range(0, izmax+1)]
                ipz_err[0] = 1e-6
                #print(ipz_zlist)

                ipz_hzBG_save_samples = []
                ipz_qPDF_save_samples = []
                for isamp in range(0, npick):
                    ipz_isamp_hzLat = [ipz_samples[iz][isamp+1] for iz in range(0, izmax+1)]
                    #ipz_err = [1e-6, 1e-6]
                    #print(np.array(ipz_zlist), np.array(ipz_isamp_hzLat), np.array(ipz_err))
                    ipz_isamp_hzBG=qPDF_matrix_BayesInfer(np.array(ipz_zlist), np.array(ipz_isamp_hzLat), np.array(ipz_err))
                    ipz_isamp_hzBG_save = [ipz_isamp_hzBG(iz) for iz in zsave]
                    ipz_hzBG_save_samples += [ipz_isamp_hzBG_save]

                    def qPDF(x):
                        DFT = ipz_fm*zstep/(2*np.pi)
                        for iz in range(1, len(zsave)):
                            DFT += 2*ipz_fm*zstep/(2*np.pi) * np.cos(x*ipz_fm*zsave[iz]) * ipz_isamp_hzBG_save[iz]
                        return 2*DFT
                    xlist = np.arange(0.0,5.02,0.02)
                    ipz_isamp_qPDF_save = qPDF(xlist)
                    ipz_qPDF_save_samples += [list(ipz_isamp_qPDF_save)]

                ipz_hzBG_save_mean = []
                ipz_hzBG_save_0 = []
                ipz_hzBG_save_1 = []
                ipz_hzBG_save_2 = []
                for iz in range(0, len(zsave)):
                    iz_hzBG_save_samples = [ipz_hzBG_save_samples[isamp][iz] for isamp in range(0, npick)]
                    iz_hzBG_save_samples = sorted(iz_hzBG_save_samples)
                    ipz_hzBG_save_mean += [[zsave[iz], zsave[iz]*ipz_fm, (iz_hzBG_save_samples[high16]+iz_hzBG_save_samples[low16])/2, (iz_hzBG_save_samples[high16]-iz_hzBG_save_samples[low16])/2]]
                    ipz_hzBG_save_0 += [[zsave[iz], zsave[iz]*ipz_fm, iz_hzBG_save_samples[low16]]]
                    ipz_hzBG_save_1 += [[zsave[iz], zsave[iz]*ipz_fm, (iz_hzBG_save_samples[high16]+iz_hzBG_save_samples[low16])/2]]
                    ipz_hzBG_save_2 += [[zsave[iz], zsave[iz]*ipz_fm, iz_hzBG_save_samples[high16]]]

                save_name = outfolder + izmax_describe + '_0.txt'
                np.savetxt(save_name, ipz_hzBG_save_0, fmt='%.8e')
                save_name = outfolder + izmax_describe + '_1.txt'
                np.savetxt(save_name, ipz_hzBG_save_1, fmt='%.8e')
                save_name = outfolder + izmax_describe + '_2.txt'
                np.savetxt(save_name, ipz_hzBG_save_2, fmt='%.8e')

                save_name = outfolder + izmax_describe + '.txt'
                np.savetxt(save_name, ipz_hzBG_save_mean, fmt='%.8e')
                sample_name = samplefolder + izmax_describe + '_bootssamples.txt'
                np.savetxt(sample_name, ipz_hzBG_save_samples, fmt='%.8e')


                ipz_qPDF_save_mean = []
                ipz_qPDF_save_0 = []
                ipz_qPDF_save_1 = []
                ipz_qPDF_save_2 = []
                for ix in range(0, len(xlist)):
                    ix_qPDF_save_samples = [ipz_qPDF_save_samples[isamp][ix] for isamp in range(0, npick)]
                    ix_qPDF_save_samples = sorted(ix_qPDF_save_samples)
                    ix_xqPDF_save_samples = [xlist[ix]*ipz_qPDF_save_samples[isamp][ix] for isamp in range(0, npick)]
                    ix_xqPDF_save_samples = sorted(ix_xqPDF_save_samples)
                    ipz_qPDF_save_mean += [[xlist[ix], (ix_qPDF_save_samples[high16]+ix_qPDF_save_samples[low16])/2, (ix_qPDF_save_samples[high16]-ix_qPDF_save_samples[low16])/2, (ix_xqPDF_save_samples[high16]+ix_xqPDF_save_samples[low16])/2, (ix_xqPDF_save_samples[high16]-ix_xqPDF_save_samples[low16])/2]]
                    ipz_qPDF_save_0 += [[xlist[ix], ix_qPDF_save_samples[low16], ix_xqPDF_save_samples[low16]]]
                    ipz_qPDF_save_1 += [[xlist[ix], (ix_qPDF_save_samples[high16]+ix_qPDF_save_samples[low16])/2, (ix_xqPDF_save_samples[high16]+ix_xqPDF_save_samples[low16])/2]]
                    ipz_qPDF_save_2 += [[xlist[ix], ix_qPDF_save_samples[high16], ix_xqPDF_save_samples[high16]]]

                save_name = outfolder + izmax_describe + '_qPDF_0.txt'
                np.savetxt(save_name, ipz_qPDF_save_0, fmt='%.8e')
                save_name = outfolder + izmax_describe + '_qPDF_1.txt'
                np.savetxt(save_name, ipz_qPDF_save_1, fmt='%.8e')
                save_name = outfolder + izmax_describe + '_qPDF_2.txt'
                np.savetxt(save_name, ipz_qPDF_save_2, fmt='%.8e')

                save_name = outfolder + izmax_describe + '_qPDF.txt'
                np.savetxt(save_name, ipz_qPDF_save_mean, fmt='%.8e')
                sample_name = samplefolder + izmax_describe + '_qPDF_bootssamples.txt'
                ipz_qPDF_save_samples = np.transpose([xlist]+ipz_qPDF_save_samples)
                np.savetxt(sample_name, ipz_qPDF_save_samples, fmt='%.8e')

    elif sys.argv[1] =='exp':

        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        
        # read and create the output folder
        inputfolder = os.path.dirname(sys.argv[2])
        outfolder = inputfolder + '/results/'
        mkdir(outfolder)
        outfolder = outfolder
        samplefolder = outfolder + '/samples/'
        mkdir(samplefolder)
        
        # read describe of this command file
        line1st = commands[0].split()
        describe = line1st[0]
        Ns = int(line1st[1])
        Nt = int(line1st[2])
        latsp = float(line1st[3])
        #zstep = latsp
        print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
        
        fmGeV = 5.0676896
        # read parameters
        for index in range(1, len(commands)):
            
            line = commands[index].split()
            
            if line[0] == 'zstep':
                zstep = float(line[1])
                #zsave = np.arange(0, 5, zstep)
                print('zstep:',zstep,'fm')

            elif line[0] == 'zs':
                zs = int(line[1])
                print('zs:',str(zs)+'a =',zs*zstep,'fm')
                describe = describe.replace('#',str(zs))

            elif line[0] == 'mass(GeV)':
                mass = float(line[1])
                print('mass:',mass,'GeV')

            elif line[0] == 'mu(GeV)':
                mu = float(line[1])
                describe = describe.replace('mu','mu'+str(mu).replace('.','p'))
                print('mu:',mu,'GeV', ',alphas:', alpsmu(mu*fmGeV))

            elif line[0] == 'pzrange':
                pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
                print('pzlist:',pzlist)

            elif line[0] == 'dzL':
                #zLlist = [int(line[i]) for i in range(1, len(line))]
                #print('zLlist:','['+line[1]+', '+line[2]+']')
                dzL = int(line[1])
                print('dzL:', dzL*latsp, 'fm.')

            elif line[0] == 'zmin':
                zmin = int(line[1])
                print('zmin:', zmin*latsp, 'fm.')
                #zmaxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                #print('zmaxlist:','['+line[1]+', '+line[2]+']')

            elif line[0] == 'mean':
                mean_format = line[1]
                mean_format = mean_format.replace('#',str(zs))
                mean_format = mean_format.replace('mu','mu'+str(mu).replace('.','p'))
                print('\nmean:', mean_format.split('/')[-1])

            elif line[0] == 'sample':
                sample_format = line[1]
                sample_format = sample_format.replace('#',str(zs))
                sample_format = sample_format.replace('mu','mu'+str(mu).replace('.','p'))
                print('sample:', sample_format.split('/')[-1],'\n')

        for ipz in range(0, len(pzlist)):

            ipz_describe = describe.replace('*', str(pzlist[ipz]))

            ipz_mean = np.loadtxt(mean_format.replace('*',str(pzlist[ipz])))
            ipz_samples = np.loadtxt(sample_format.replace('*',str(pzlist[ipz])))

            zmax = int(Ns/2)
            zmin_01 = zmax
            zmin_02 = zmax
            for iz in range(zs, int(Ns/2)):
                #print(iz,ipz_mean[iz][0])
                if ipz_mean[iz][2] <=0:
                    zmax = iz-1
                    break
                if zmin_01 == zmax and ipz_mean[iz][2] < 0.1:
                    zmin_01 = iz
                if zmin_02 == zmax and ipz_mean[iz][2] < 0.2:
                    zmin_02 = iz
            zmaxlist = [zmax-2*dzL,zmax-dzL,zmax]
            zmaxlist = [zmax]
            print('zmin of hz<0.1:', zmin_01)
            print('zmin of hz<0.2:', zmin_02)
            print('zmaxlist:', zmaxlist)
            zmin_tmp = zmin_02
            while zmax-2*dzL-zmin_tmp+1 < 4:
                zmin_tmp -= 1

            npick = len(ipz_samples[0]) - 1
            low16 = int((0.16*npick))
            high16 = int(np.ceil(0.84*npick))
            mid = int(0.5*npick)
            ipz_fm = round(2*np.pi*pzlist[ipz]/(Ns*latsp),2)
            print('\npz =', ipz_fm, '[fm^-1]; npick:', npick)
            print('zmax is', zmax, ipz_mean[zmax][2])

            ipz_zmaxlist = []
            for izmax in zmaxlist:
                i_zlist = [iz for iz in range(zmin_tmp, izmax+1)]
                ipz_zmaxlist += [i_zlist]
                print('zmax list:', i_zlist)
            
            ipz_popterr = []
            for i_zlist in ipz_zmaxlist:

                #z_save = list(np.arange(0, 5, zstep)) + list(np.arange(5, 100, zstep*2))+ list(np.arange(100, 500, zstep*5))
                z_save = list(np.arange(0, 5, zstep))

                #iz_describe = ipz_describe.replace('zmax','zmax'+str(i_zlist[-1]))
                iz_describe = ipz_describe.replace('zL','zL'+str(i_zlist[-1]))

                ipz_zlist = [ipz_mean[iz][0] for iz in i_zlist]
                ipz_err = [ipz_mean[iz][-1] for iz in i_zlist]
                ipz_err[int(len(ipz_err)/2)] = ipz_err[int(len(ipz_err)/2)]/10
                print('zlist:',ipz_zlist)

                popt_samples = []
                chisq_samples = []
                hz_samples = [[iz] for iz in z_save]
                ipz_qPDF_save_samples = []
                for isamp in range(0, npick):
                    
                    ipz_isamp_hzLat = [ipz_samples[iz][isamp+1] for iz in i_zlist]
                    print(isamp+1,'/',npick)
                    #print(ipz_isamp_hzLat)
                    #print(ipz_zlist)

                    def Fit_res(popt):
                            res = []
                            for iz in range(0, len(i_zlist)):
                                res += [(fitfunc(ipz_zlist[iz], popt[0], popt[1], popt[2], popt[3])-ipz_isamp_hzLat[iz])/ipz_err[iz]]
                            return res

                    poptstart = [-0.78500579,  1.65825652,  2.81316338,  3.0809646] 
                    res = least_squares(Fit_res, poptstart, method='lm')
                    popt = res.x
                    chisq = sum(np.square(res.fun)) / (len(i_zlist)-len(popt))
                    popt_samples += [popt]
                    chisq_samples += [chisq]
                    print(popt, chisq)

                    itp_ipz_isamp_hzLat = [ipz_samples[iz][isamp+1] for iz in range(0, i_zlist[-1])] + [fitfunc(i_zlist[-1]*latsp, popt[0], popt[1], popt[2], popt[3])]
                    itp_zlist = [iz*latsp for iz in range(0, i_zlist[-1]+1)]
                    itp_hz = interp1d(itp_zlist, itp_ipz_isamp_hzLat)
                    
                    def i_hz(z):
                        if z < i_zlist[int(len(ipz_err)/2)]*latsp:
                            return itp_hz(z)
                        else:
                            return fitfunc(z, popt[0], popt[1], popt[2], popt[3])
                    for iz in range(0, len(hz_samples)):
                        hz_samples[iz] += [i_hz(hz_samples[iz][0])]

                    '''
                    def qPDF(x):
                        DFT = 0
                        for iz in range(1, len(z_save)):
                            dz = z_save[iz] - z_save[iz-1]
                            hz = 2*ipz_fm/(2*np.pi) * (hz_samples[iz][isamp+1]*np.cos(x*ipz_fm*z_save[iz])+hz_samples[iz-1][isamp+1]*np.cos(x*ipz_fm*z_save[iz-1]))/2
                            DFT += hz*dz
                        zL = z_save[-1]
                        #print(np.shape(DFT),DFT[0])
                        for i in range(0, len(x)):
                            ix = x[i]
                            DFT[i] += 2*ipz_fm/(2*np.pi) * popt[0]*(zL**(1-popt[2])*hyp1f2(0.5-abs(popt[2])/2,0.5,1.5-abs(popt[2])/2,-0.25*(ix*ipz_fm*zL)**2)/(-1+popt[2])+(ix*ipz_fm)**(-1+popt[2])*gamma(1-popt[2])*math.sin(popt[2]*np.pi/2))
                        return 2*DFT
                    '''

                    def qPDF(x):
                        DFT = 0
                        for iz in range(1, len(z_save)):
                            dz = z_save[iz] - z_save[iz-1]
                            hz = 2*ipz_fm/(2*np.pi) * (hz_samples[iz][isamp+1]*np.cos(x*ipz_fm*z_save[iz])+hz_samples[iz-1][isamp+1]*np.cos(x*ipz_fm*z_save[iz-1]))/2
                            #hz -= 2*ipz_fm/(2*np.pi) * (fitfunc(z_save[iz], popt[0], popt[1], popt[2])*np.cos(x*ipz_fm*z_save[iz])+fitfunc(z_save[iz-1], popt[0], popt[1], popt[2])*np.cos(x*ipz_fm*z_save[iz-1]))/2
                            DFT += hz*dz
                        zL = z_save[-1]
                        for i in range(0, len(x)):
                            ix = x[i]
                            DFT[i] += 2*ipz_fm/(2*np.pi) * qPDFfunc(ix*ipz_fm, zL, popt[0], popt[1], popt[2], popt[3])
                        return 2*DFT
                    
                    xlist = [0.000475297727*np.exp(0.0304639897*t) for t in range(1, 100)] + list(np.arange(0.02,5.02,0.02))
                    xlist[0] = 0.00049
                    xlist = np.array(xlist)
                    ipz_isamp_qPDF_save = qPDF(xlist)
                    #ipz_isamp_qPDF_save = []
                    #for ix in xlist:
                    #    ipz_isamp_qPDF_save += [qPDF(ix)]
                    ipz_qPDF_save_samples += [list(ipz_isamp_qPDF_save)]

                i_popterr = [ipz_zlist[0], ipz_zlist[-1]]
                i_poptmean = [ipz_zlist[0], ipz_zlist[-1]]
                for ip in range(0, len(poptstart)):
                    ip_samples = sorted([popt_samples[isamp][ip] for isamp in range(0, len(popt_samples))])
                    i_popterr += [(ip_samples[high16]+ip_samples[low16])/2, (ip_samples[high16]-ip_samples[low16])/2]
                    i_poptmean += [(ip_samples[high16]+ip_samples[low16])/2]
                ipz_popterr += [i_popterr + [sorted(chisq_samples)[mid]]]
                save_name = outfolder + iz_describe + '_popt.txt'
                np.savetxt(save_name, ipz_popterr, fmt='%.8e')
                print(i_poptmean, sorted(chisq_samples)[mid])

                ipz_hz_0 = []
                ipz_hz_1 = []
                ipz_hz_2 = []
                for iz in range(0, len(hz_samples)):
                    iz_hz_samples = sorted(hz_samples[iz][1:])
                    ipz_hz_0 += [[hz_samples[iz][0], hz_samples[iz][0]*ipz_fm, iz_hz_samples[low16]]]
                    ipz_hz_1 += [[hz_samples[iz][0], hz_samples[iz][0]*ipz_fm, (iz_hz_samples[low16]+iz_hz_samples[high16])/2]]
                    ipz_hz_2 += [[hz_samples[iz][0], hz_samples[iz][0]*ipz_fm, iz_hz_samples[high16]]]
                save_name_0 = outfolder + iz_describe + '_hz0.txt'
                save_name_1 = outfolder + iz_describe + '_hz1.txt'
                save_name_2 = outfolder + iz_describe + '_hz2.txt'
                np.savetxt(save_name_0, ipz_hz_0, fmt='%.8e')
                np.savetxt(save_name_1, ipz_hz_1, fmt='%.8e')
                np.savetxt(save_name_2, ipz_hz_2, fmt='%.8e')

                save_name = samplefolder + iz_describe + '_hz_bootssamples.txt'
                #np.savetxt(save_name, hz_samples, fmt='%.8e')

                ipz_qPDF_save_mean = []
                ipz_qPDF_save_0 = []
                ipz_qPDF_save_1 = []
                ipz_qPDF_save_2 = []
                for ix in range(0, len(xlist)):
                    ix_qPDF_save_samples = [ipz_qPDF_save_samples[isamp][ix] for isamp in range(0, npick)]
                    ix_qPDF_save_samples = sorted(ix_qPDF_save_samples)
                    ix_xqPDF_save_samples = [xlist[ix]*ipz_qPDF_save_samples[isamp][ix] for isamp in range(0, npick)]
                    ix_xqPDF_save_samples = sorted(ix_xqPDF_save_samples)
                    ipz_qPDF_save_mean += [[xlist[ix], (ix_qPDF_save_samples[high16]+ix_qPDF_save_samples[low16])/2, (ix_qPDF_save_samples[high16]-ix_qPDF_save_samples[low16])/2, (ix_xqPDF_save_samples[high16]+ix_xqPDF_save_samples[low16])/2, (ix_xqPDF_save_samples[high16]-ix_xqPDF_save_samples[low16])/2]]
                    ipz_qPDF_save_0 += [[xlist[ix], ix_qPDF_save_samples[low16], ix_xqPDF_save_samples[low16]]]
                    ipz_qPDF_save_1 += [[xlist[ix], (ix_qPDF_save_samples[high16]+ix_qPDF_save_samples[low16])/2, (ix_xqPDF_save_samples[high16]+ix_xqPDF_save_samples[low16])/2]]
                    ipz_qPDF_save_2 += [[xlist[ix], ix_qPDF_save_samples[high16], ix_xqPDF_save_samples[high16]]]

                save_name = outfolder + iz_describe + '_qPDF_0.txt'
                np.savetxt(save_name, ipz_qPDF_save_0, fmt='%.8e')
                save_name = outfolder + iz_describe + '_qPDF_1.txt'
                np.savetxt(save_name, ipz_qPDF_save_1, fmt='%.8e')
                save_name = outfolder + iz_describe + '_qPDF_2.txt'
                np.savetxt(save_name, ipz_qPDF_save_2, fmt='%.8e')

                save_name = outfolder + iz_describe + '_qPDF.txt'
                np.savetxt(save_name, ipz_qPDF_save_mean, fmt='%.8e')
                sample_name = samplefolder + iz_describe + '_qPDF_bootssamples.txt'
                ipz_qPDF_save_samples = np.transpose([xlist]+ipz_qPDF_save_samples)
                np.savetxt(sample_name, ipz_qPDF_save_samples, fmt='%.8e')




