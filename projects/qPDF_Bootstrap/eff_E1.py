#!/usr/bin/env python3
from tools import *
from modelfuncs import *
import numpy as np
import sys


# This function is used to subtract the E0 part in 
# c2pt, and then you can use another function to
# compute the eff_E1
# The command file should be arraged as:
# describe A0 E0
# save_position
# file_position
def eff_E1_subtE0(commandfile):
    
    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()
        
    # read describe of this command file
    describe = (commands[0].split())[0]
        
    # read time seperation for the following c3pt
    A0 = float((commands[0].split())[1])
    E0 = float((commands[0].split())[2])      

    # read save folder
    folder = (commands[1].split())[0]

    c2data = csvtolist((commands[2].split())[0])

    header = ['t, conf1, conf2, ...']

    outfilename = folder + describe + '.txt'
    with open(outfilename, "w", newline='') as outfile:
        writer = csv.writer(outfile)
        writer.writerows([header])
        for i in range(0, len(c2data)):
            t = c2data[i][0]
            for j in range(1,len(c2data[i])):
                c2data[i][j] -= onestate(t, A0, E0)
            raw = list(c2data[i])
            writer.writerows([raw])
    outfile.close()

eff_E1_subtE0(sys.argv[1])