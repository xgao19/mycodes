#!/usr/bin/env python3
import numpy as np
import sys
import h5py
from scipy.optimize import curve_fit

from utils.tools import *
from qPDF_Bootstrap.modelfuncs import *
from qPDF_Bootstrap.makefuncs import *
from statistics import *

np.random.seed(2020)
random_uniform = np.random.uniform(0,1,1000000)
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This code read 2pt data from csv format and read 3pt data 
# from h5 format. 
#
# Some variable names are set as momentum along with x direction,
# , you will need to modify the code to adjust your data format,
# such as the kineimatic paramters and the breit-frame direction.
#
# Note:
#   1, gmlist is important:
#     a), if gmlist[0] == g8 and len(gmlist) > 0, the code
#     will suppose you want to extract breit frame data by 
#     combine different gamma components.
#     b), if gmlist[0] != g8, the code
#     will simply average different gamma components.
#   2, nave or ave means if will average the different
#   momentum transfer \vec{q} with same Q^2.
#   3, c2pt[ts][cfgs], c3pt[gm][ts][z][tau][cfgs]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Take a 2-d list, and average per bin with certain bin size.
def boots_avebin(data, binsize):
    new_data = []
    ncfg = len(data[0])-1
    for i in range(0, len(data)):
        i_new_data = [data[i][0]]
        for j in range(0, int(ncfg/binsize)):
            i_new_data += [np.average(data[i][1+j*binsize:1+(j+1)*binsize])]
        #print(ncfg,binsize,int(ncfg/binsize),len(i_new_data))
        new_data += [i_new_data]
    return new_data

# Take a 2-d list, and pick the data using random_uniform_int.
def boots_pick(data, isamp):
    new_data = []
    ncfg = len(data[0])-1
    for i in range(0, len(data)):
        i_new_data = [data[i][0]]
        i_data = data[i][1:]
        #picklist = []
        for ipick in range(0, ncfg):
            #print(np.shape(i_data), ipick%len(i_data))
            i_new_data += [i_data[random_uniform_int[isamp][ipick]%len(i_data)]]
            #picklist += [random_uniform_int[isamp][ipick]%len(i_data)]
        #print(picklist)
        new_data += [i_new_data]
    return new_data


def qxyz_generate(qxyz):

    qxyz_list = []
    qylist = [0]
    qzlist = [0]

    if qxyz[1] != 0:
        qylist = [qxyz[1], -qxyz[1]]
    if qxyz[2] != 0:
        qzlist = [qxyz[2], -qxyz[2]]
    for iqy in qylist:
        for iqz in qzlist:
            qxyz_list += [[qxyz[0], iqy, iqz]]

    return qxyz_list

def sign_combine(Ein, Eout, px, qxyz_list, gm):

    factor_list = []
    gm_qxyz_list = []

    for iqxyz in qxyz_list:

        p_vec_in = [Ein, px*2*3.14159/NS, 0, 0]
        p_vec_out = [Eout, (iqxyz[0]+px)*2*3.14159/NS, iqxyz[1]*2*3.14159/NS, iqxyz[2]*2*3.14159/NS]

        if gm == 'g8':
            num = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])
            denm = (p_vec_in[0]+p_vec_out[0])
        elif gm == 'g1':
            num = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])
            denm = (p_vec_in[1]+p_vec_out[1])
        elif gm == 'g2':
            num = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])
            denm = (p_vec_in[2]+p_vec_out[2])
        elif gm == 'g4':
            num = 2*np.sqrt(p_vec_in[0]*p_vec_out[0])
            denm = (p_vec_in[3]+p_vec_out[3])

        if gm == 'g8':
            if num*denm != 0:
                factor_list += [num/denm]
                gm_qxyz_list += [iqxyz]
        else:
            #print(num,denm)
            if num*denm != 0:
                factor_list += [num/denm]
                gm_qxyz_list += [iqxyz]

    return factor_list, gm_qxyz_list

def To_breit_c3pt(c3pt_data, gmlist, Ein, Eout):

    #print(c3pt_data[0][0][0][4][:10])
    if len(gmlist) == 1:
        return c3pt_data[0]

    dE = Eout-Ein
    Eave = (Eout+Ein)/2
    r = dE/Eave

    if gmlist[0] == 'g8' and len(gmlist) > 1:
        fct_g8 = 2/(2-r)
        fct_g2g4 = -r/(2-r)
        data_g8 = c3pt_data[0]

        # average the g2, g4 data
        if len(gmlist) > 2:
            data_g2g4_ave = []
            for its in range(0, len(c3pt_data[0])):
                its_data_g2g4_ave = []
                for iz in range(0, len(c3pt_data[0][0])):
                    gm_data = []
                    for igm in range(1, len(c3pt_data)):
                        gm_data += [c3pt_data[igm][its][iz]]
                    its_data_g2g4_ave += [ave_2d_lists(gm_data)]
                data_g2g4_ave += [its_data_g2g4_ave]

        # solve the breit frame data
        #print(data_g8[0][0][4][:10], '\n', data_g2g4_ave[0][0][4][:10])
        data_ave = []
        for its in range(0, len(data_g8)):
            its_data_ave = []
            for iz in range(0, len(data_g8[0])):
                #print(Ein, Eout,dE,Eave,dE/Eave,[fct_g8,fct_g2g4])
                iz_data_ave = ave_2d_lists([data_g8[its][iz], data_g2g4_ave[its][iz]], [fct_g8*2,fct_g2g4*2])
                its_data_ave += [iz_data_ave]
            data_ave += [its_data_ave]
        #print([fct_g8,fct_g2g4],data_ave[0][0][4][:10])


    else:
        data_ave = []
        for its in range(0, len(c3pt_data[0])):
            its_data_ave = []
            for iz in range(0, len(c3pt_data[0][0])):
                gm_data = []
                for igm in range(0, len(c3pt_data)):
                    gm_data += [c3pt_data[igm][its][iz]]
                its_data_ave += [ave_2d_lists(gm_data)]
                #its_data_ave += [ratio_2d_lists(gm_data)]
            data_ave += [its_data_ave]

    print('  C3pt data is averaged (or solved to breit) in shape:', np.shape(data_ave))
    return data_ave



def ave_c2pt_data(c2pt_format, px, qxyz_list, breit = True):

    print('  Reading c2pt:')
    if breit == False:
        c2pt_in_dataset = []
        for i_fmt in c2pt_format:
            c2pt_in_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(0)+'_PY'+str(0)+'_PZ'+str(px))
            print('    P:', c2pt_in_name)
            c2pt_in_dataset += [csvtolist(c2pt_in_name)]
        c2pt_ave_in = comb_2d_lists(c2pt_in_dataset)
        
        c2pt_out_dataset = []
        for iqxyz in qxyz_list:
            iq_out_dataset = []
            for i_fmt in c2pt_format:
                c2pt_out_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(0+iqxyz[0])+'_PY'+str(iqxyz[1])+'_PZ'+str(px+iqxyz[2]))
                print('    P+q:', c2pt_out_name)
                iq_out_dataset += [csvtolist(c2pt_out_name)]
            c2pt_out_dataset += [comb_2d_lists(iq_out_dataset)]
        if len(c2pt_out_dataset) > 1:
            c2pt_ave_out = ave_2d_lists(c2pt_out_dataset)
        else:
            c2pt_ave_out = c2pt_out_dataset[0]

    elif breit == True:
        c2pt_in_dataset = []
        for iqxyz in qxyz_list:
            iq_in_dataset = []
            for i_fmt in c2pt_format:
                c2pt_in_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(px)+'_PY'+str(-int(iqxyz[1]/2))+'_PZ'+str(-int(iqxyz[2]/2)))
                print('    P:', c2pt_in_name)
                iq_in_dataset  += [csvtolist(c2pt_in_name)]
                temp = csvtolist(c2pt_in_name)
                print(temp[12])
            c2pt_in_dataset += [comb_2d_lists(iq_in_dataset)]

        c2pt_out_dataset = []
        for iqxyz in qxyz_list:
            iq_out_dataset = []
            for i_fmt in c2pt_format:
                c2pt_out_name = i_fmt.replace('PX_PY_PZ', 'PX'+str(px)+'_PY'+str(int(iqxyz[1]/2))+'_PZ'+str(int(iqxyz[2]/2)))
                print('    P+q:', c2pt_out_name)
                iq_out_dataset += [csvtolist(c2pt_out_name)]
            c2pt_out_dataset += [comb_2d_lists(iq_out_dataset)]
            
        if len(c2pt_in_dataset) > 1:
            c2pt_ave_in = ave_2d_lists(c2pt_in_dataset)
        else:
            c2pt_ave_in = c2pt_in_dataset[0]
        if len(c2pt_out_dataset) > 1:
            c2pt_ave_out = ave_2d_lists(c2pt_out_dataset)
        else:
            c2pt_ave_out = c2pt_out_dataset[0]

    cfg_c2pt = len(c2pt_ave_out[0]) - 1
    print('    P shape:',np.shape(c2pt_ave_in), ', P+q shape:', np.shape(c2pt_ave_out))

    return c2pt_ave_in, c2pt_ave_out

def ave_c3pt_data(c3pt_format, qxyz_list, tslist, zlist, c3pt_datatype, sign_list=[], foldz=False, breit = True):

    print('  Reading c3pt:', c3pt_datatype, ', fold z data:',foldz)
    print('    !!! P->P+q: Note the fourier transform of q is opposite.')

    #print(c3pt_format)
    # Open the h5 data files.
    c3pt_h5_list = []
    if breit == False:
        for iqxyz in qxyz_list:
            iq_h5_list = []
            for i_fmt in c3pt_format:
                i_fmt_name = i_fmt.replace('qx_qy_qz', 'qx'+str(iqxyz[0])+'_qy'+str(iqxyz[1])+'_qz'+str(iqxyz[2]))
                i_fmt_name = i_fmt_name.replace('PX_PY_PZ', 'PX'+str(0)+'_PY'+str(0)+'_PZ'+str(0))
                print('    P->P+q:', i_fmt_name)
                iq_h5_list += [h5py.File(i_fmt_name, 'r')]
            c3pt_h5_list += [iq_h5_list]
    elif breit == True:
        for iqxyz in qxyz_list:
            iq_h5_list = []
            for i_fmt in c3pt_format:
                i_fmt_name = i_fmt.replace('qx_qy_qz', 'qx'+str(-iqxyz[0])+'_qy'+str(-iqxyz[1])+'_qz'+str(-iqxyz[2]))
                i_fmt_name = i_fmt_name.replace('_PY_PZ', '_PY'+str(-int(iqxyz[1]/2))+'_PZ'+str(-int(iqxyz[2]/2)))
                print('    P->P+q:', i_fmt_name)
                iq_h5_list += [h5py.File(i_fmt_name, 'r')]
            c3pt_h5_list += [iq_h5_list] 

    # Read and preproceed the data
    c3pt_dataset = []
    for its in tslist:
        its_data = []
        for iz in zlist:
            iz_data = []
            for iq in range(0, len(c3pt_h5_list)):
                iq_data = []
                for ifmt in c3pt_h5_list[iq]:
                    #print(list(ifmt.keys()))
                    #print('dt'+str(its)+'/'+'X'+str(iz))
                    try:
                        if foldz == False:
                            iq_data += [np.array(ifmt['dt'+str(its)+'/'+'X'+str(iz)])]
                        else:
                            Zp_data = np.array(ifmt['dt'+str(its)+'/'+'X'+str(iz)])
                            Zn_data = np.array(ifmt['dt'+str(its)+'/'+'X'+str(-iz)])
                            #if its == 12:
                            #    print('\n',its,np.average(Zp_data[3][60:65]),Zp_data[3][60:65])
                            Zave_data = fold_z_data(Zp_data, Zn_data, foldz)
                            iq_data += [Zave_data]
                    except:
                        #traceback.print_exc()
                        pass
                #print(np.shape(iq_data))
                iz_data += [comb_2d_lists(iq_data, c3pt_datatype)]
                #print('iz shape',np.shape(iz_data))
            if len(sign_list) != 1:
                its_data += [ave_2d_lists(iz_data, sign_list)]
            else:
                its_data +=  [iz_data[0]]
        c3pt_dataset += [its_data]
    print('    P->P+q shape:',np.shape(c3pt_dataset))

    # Close the h5 data files.
    for iq_h5 in c3pt_h5_list:
        for ifmt_h5 in iq_h5:
            ifmt_h5.close()

    return c3pt_dataset




def gpd_ratiofit_boots(c3pt_data, c2pt_data_in, c2pt_data_out, tslist, Ein, Eout, NT, blocksize, taumin, nm_bm, npick=0):

    ncfg = len(c2pt_data_in[0]) - 1
    nblock = int(ncfg/blocksize)
    temp = []
    for idata in c3pt_data:
        temp += [boots_avebin(idata, blocksize)]
    c3pt_data = temp
    c2pt_data_in = boots_avebin(c2pt_data_in, blocksize)
    c2pt_data_out = boots_avebin(c2pt_data_out, blocksize)
    print('    Bootstrap binned data shape: ',np.shape(c2pt_data_in),np.shape(c2pt_data_out), np.shape(c3pt_data))
    if npick == 0:
        npick = nblock
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    # @@@ mean ratio collection
    ratio_ts = []
    ratio_tau = []
    for its in range(0, len(tslist)):
        ratio = gpd_ratio_constuct(c3pt_data[its], c2pt_data_in, c2pt_data_out, tslist[its], taumin, nm_bm)
        for itau in range(0, len(ratio)):
            ratio_ts += [tslist[its]]
            ratio_tau += [taumin+itau]
    
    # @@@ standard error collection
    sample_ratio_collection = []
    for isamp in range(0, npick):

        isamp_c2pt_data_in = boots_pick(c2pt_data_in, isamp)
        isamp_c2pt_data_out = boots_pick(c2pt_data_out, isamp)
        isamp_c3pt_data = []
        for its in range(0, len(tslist)):
            isamp_c3pt_data += [boots_pick(c3pt_data[its], isamp)]

        sample_ratio = []
        for its in range(0, len(tslist)):
            ratio = gpd_ratio_constuct(isamp_c3pt_data[its], isamp_c2pt_data_in, isamp_c2pt_data_out, tslist[its], taumin, nm_bm)
            sample_ratio += ratio
        sample_ratio_collection += [sample_ratio] #data[isamp][its,itau]
    print('    Bootstrap re-sampled data shape: ',np.shape(sample_ratio_collection))

    err_ratio = []
    for iratio in range(0, len(sample_ratio_collection[0])):
        samp_ratio = [sample_ratio_collection[isamp][iratio] for isamp in range(0, npick)]
        samp_ratio = sorted(samp_ratio)
        err_ratio += [(samp_ratio[high16] - samp_ratio[low16])/2]
    #print(err_ratio)
    
    # @@@ fit procedure
    fit_func = make_gpd_c3pt_ratio(Ein, Eout)
    #poptstart = [np.random.normal(0.5, 0.1) for i in range(0, fit_func.__code__.co_argcount-1)]
    poptstart = [0 for i in range(0, fit_func.__code__.co_argcount-1)]

    # sample ratio fit
    popt_sample_collection = []
    for isamp in range(0, npick):
        popt, popv = curve_fit(fit_func,(ratio_ts, ratio_tau),sample_ratio_collection[isamp],poptstart,sigma=err_ratio)
        popt_sample = list(popt)
        chisq_data = []
        for i in range(0, len(sample_ratio_collection[isamp])):
            chisq_data += [[ratio_ts[i], ratio_tau[i], sample_ratio_collection[isamp][i], err_ratio[i]]]
        chisq = chisq_value_stderr(chisq_data, fit_func, popt_sample)/(len(sample_ratio_collection[isamp])-len(popt_sample))

        popt_sample_collection += [popt_sample+[chisq]]

    # mean value and error collection
    popt_mean_err = list(Ein)+list(Eout)
    popt_mean = []
    for ipopt in range(0, len(popt_sample_collection[0])):
        samp_ipopt = [popt_sample_collection[isamp][ipopt] for isamp in range(0, npick)]
        samp_ipopt = sorted(samp_ipopt)
        popt_mean += [samp_ipopt[mid]]
        #print(popt_mean_err, [samp_ipopt[mid], (samp_ipopt[high16] - samp_ipopt[low16])/2])
        popt_mean_err += [samp_ipopt[mid], (samp_ipopt[high16] - samp_ipopt[low16])/2]
            
    print('popt+chisq:',popt_mean)

    return popt_mean_err, popt_sample_collection

def gpd_sumfit_boots(c3pt_data, c2pt_data_in, c2pt_data_out, tslist, Ein, Eout, NT, blocksize, taumin, nm_bm, npick=0):

    ncfg = len(c2pt_data_in[0]) - 1
    nblock = int(ncfg/blocksize)
    temp = []
    for idata in c3pt_data:
        temp += [boots_avebin(idata, blocksize)]
    c3pt_data = temp
    c2pt_data_in = boots_avebin(c2pt_data_in, blocksize)
    c2pt_data_out = boots_avebin(c2pt_data_out, blocksize)
    print('    Bootstrap binned data shape: ',np.shape(c2pt_data_in),np.shape(c2pt_data_out), np.shape(c3pt_data))
    if npick == 0:
        npick = nblock
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    # make the function to eliminate the wrap round effect
    def c2mdf_in(t):
        return Ein[0]*np.exp(-Ein[1]*(NT-t)) + Ein[2]*np.exp(-Ein[3]*(NT-t))
    def c2mdf_out(t):
        return Eout[0]*np.exp(-Eout[1]*(NT-t)) + Eout[2]*np.exp(-Eout[3]*(NT-t))
    
    # standard error collection
    sample_sum_collection = []
    for isamp in range(0, npick):

        isamp_c2pt_data_in = boots_pick(c2pt_data_in, isamp)
        isamp_c2pt_data_out = boots_pick(c2pt_data_out, isamp)
        isamp_c3pt_data = []
        for its in range(0, len(tslist)):
            isamp_c3pt_data += [boots_pick(c3pt_data[its], isamp)]

        sample_sum = []
        for its in range(0, len(tslist)):
            sample_ratio = gpd_ratio_constuct(isamp_c3pt_data[its], isamp_c2pt_data_in, isamp_c2pt_data_out, tslist[its], taumin, nm_bm, c2mdf_in=c2mdf_in, c2mdf_out=c2mdf_out)
            sample_sum += [np.sum(sample_ratio)]
        sample_sum_collection += [sample_sum] # data[isamp][its]
    print('    Bootstrap re-sampled data shape: ',np.shape(sample_sum_collection))
    
    err_sum = []
    for its in range(0, len(tslist)):
        its_sum = [sample_sum_collection[isamp][its] for isamp in range(0, npick)]
        its_sum = sorted(its_sum)
        err_sum += [(its_sum[high16] - its_sum[low16])/2]
    #print(np.shape(sample_sum_collection),mean_sum,err_sum)

    # mean summation fit
    #if Ein[1] < 0.07 and Eout[1] > 0.07:
    #    p000 = np.sqrt(Eout[1]**2 - Ein[1]**2)/2
    #    E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #    def fit_func(x, a, b, c):
    #        return a + b * x + c * np.exp(-E11*x) * np.exp(-(NT-2*x)*Ein[1])
    #else:
    #fit_func = linearfunc
    def fit_func(x, a, b):
        return a + b * x
        #return a + b * x + c * np.exp(-(Ein[3]-Ein[1])*x)
    #fit_func = make_sumfit_correction(Ein, Eout)
    poptstart = [0 for i in range(0, fit_func.__code__.co_argcount-1)]

    # sample summation fit
    popt_sample_collection = [] #data[isamp][popt]
    for isamp in range(0, npick):
        #print(err_sum)
        popt, popv = curve_fit(fit_func, tslist, sample_sum_collection[isamp], poptstart,sigma=err_sum)
        fitlist = [[tslist[its], sample_sum_collection[isamp][its], err_sum[its]] for its in range(0, len(tslist))]
        chisq = chisq_value_stderr(fitlist, fit_func, popt)
        chisq = chisq/(len(tslist)-len(popt))
        popt_sample = list(sample_sum_collection[isamp]) + list(popt) + [chisq]
        popt_sample_collection += [popt_sample]

    popt_mean_err = []
    for ipopt in range(0, len(popt_sample_collection[0])):
        ipopt_samp = [popt_sample_collection[isamp][ipopt] for isamp in range(0, npick)]
        ipopt_samp = sorted(ipopt_samp)
        popt_mean_err += [ipopt_samp[mid], (ipopt_samp[high16] - ipopt_samp[low16])/2]
        
    return popt_mean_err, popt_sample_collection

def gpd_ratio_boots(c3pt_data, c2pt_data_in, c2pt_data_out, ts, Ein, Eout, NT, blocksize, taumin, nm_bm, npick=0):

    ncfg = len(c2pt_data_in[0]) - 1
    nblock = int(ncfg/blocksize)
    c3pt_data = boots_avebin(c3pt_data, blocksize)
    c2pt_data_in = boots_avebin(c2pt_data_in, blocksize)
    c2pt_data_out = boots_avebin(c2pt_data_out, blocksize)
    print('    Bootstrap binned data shape: ',np.shape(c2pt_data_in),np.shape(c2pt_data_out), np.shape(c3pt_data))

    # make the function to eliminate the wrap round effect
    def c2mdf_in(t):
        return Ein[0]*np.exp(-Ein[1]*(NT-t)) + Ein[2]*np.exp(-Ein[3]*(NT-t))
    def c2mdf_out(t):
        return Eout[0]*np.exp(-Eout[1]*(NT-t)) + Eout[2]*np.exp(-Eout[3]*(NT-t))

    if npick == 0:
        npick = nblock
    popt_sample_collection = []
    for isamp in range(0, npick):

        isamp_c2pt_data_in = boots_pick(c2pt_data_in, isamp)
        isamp_c2pt_data_out = boots_pick(c2pt_data_out, isamp)
        isamp_c3pt_data = boots_pick(c3pt_data, isamp)

        sample_ratio = gpd_ratio_constuct(isamp_c3pt_data, isamp_c2pt_data_in, isamp_c2pt_data_out, ts, taumin, nm_bm, c2mdf_in=c2mdf_in, c2mdf_out=c2mdf_out)
        popt_sample_collection += [sample_ratio] # popt_sample_collection[isamp][itau]
    print('    Bootstrap re-sampled data shape: ',np.shape(popt_sample_collection))
    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    mean_err = []
    for itau in range(0, len(popt_sample_collection[0])):
        itau_samples = [popt_sample_collection[isamp][itau] for isamp in range(0, npick)]
        itau_samples = sorted(itau_samples)
        #print(itau_samples)
        mean = itau_samples[mid]
        err = (itau_samples[high16] - itau_samples[low16])/2
        mean_err += [[itau+taumin, mean, err]]

    return mean_err, popt_sample_collection
    

def gpd_ratio_constuct(c3pt_data, c2pt_data_in, c2pt_data_out, ts, taumin, nm_bm, c2mdf_in = None, c2mdf_out = None):

    ratio_list = []

    try:
        c2pt_mdf_in = [c2mdf_in(itau) for itau in range(0, 32)]
        c2pt_mdf_out = [c2mdf_out(itau) for itau in range(0, 32)]
    except:
        c2pt_mdf_in = [0 for itau in range(0, 32)]
        c2pt_mdf_out = [0 for itau in range(0, 32)]

    #for tau in range(taumin, 32):
    for tau in range(taumin, ts+1-taumin):

        c3pt_ave = np.average(c3pt_data[tau][1:])

        c2pt_ave_in_ts = np.average(c2pt_data_in[ts][1:]) - c2pt_mdf_in[ts]
        c2pt_ave_in_tau = np.average(c2pt_data_in[tau][1:]) - c2pt_mdf_in[tau]
        c2pt_ave_in_tstau = np.average(c2pt_data_in[ts-tau][1:]) - c2pt_mdf_in[ts-tau]

        c2pt_ave_out_ts = np.average(c2pt_data_out[ts][1:]) - c2pt_mdf_out[ts]
        c2pt_ave_out_tau = np.average(c2pt_data_out[tau][1:]) - c2pt_mdf_out[tau]
        c2pt_ave_out_tstau = np.average(c2pt_data_out[ts-tau][1:]) - c2pt_mdf_out[ts-tau]

        #print(c3pt_ave,c2pt_ave_out_ts)
        #c2pt_ave_in_tau = 1
        #c2pt_ave_in_tstau = 1
        #c2pt_ave_out_tau = 1
        #c2pt_ave_out_tstau = 1

        #if len(c2pt_data_in[0]) == 315:
        #    sqrtroot = np.sqrt((c2pt_ave_out_tstau*c2pt_ave_in_tau*c2pt_ave_in_ts)/(c2pt_ave_in_tstau*c2pt_ave_out_tau*c2pt_ave_out_ts))
        #    print('in,',ts,c2pt_ave_in_ts,',',tau,c2pt_ave_in_tau,',',ts-tau,c2pt_ave_in_tstau,',ratio:',c3pt_ave/c2pt_ave_in_ts*sqrtroot)
        #    print('out,',ts,c2pt_ave_out_ts,',',tau,c2pt_ave_out_tau,',',ts-tau,c2pt_ave_out_tstau,',ratio:',c3pt_ave/c2pt_ave_in_ts*sqrtroot)

        ratio_list += [nm_bm*c3pt_ave/c2pt_ave_in_ts*np.sqrt((c2pt_ave_out_tstau*c2pt_ave_in_tau*c2pt_ave_in_ts)/(c2pt_ave_in_tstau*c2pt_ave_out_tau*c2pt_ave_out_ts))]
        #ratio_list += [c3pt_ave]
        #print(ratio_list,nm_bm)
        if ts == 12 and tau == 3:
           print(c3pt_ave, c2pt_ave_in_ts)

    return ratio_list

# match the c2pt data and c3pt data with same configuration
def match_c2pt_c3pt(c2pt_data, c3pt_data):

    cfg_c3pt = len(c3pt_data[0]) - 1
    cfg_c2pt = len(c2pt_data[0]) - 1

    temp_data = []
    for iraw in range(0, len(c2pt_data)):
        temp_data += [[c2pt_data[iraw][0]] + c2pt_data[iraw][cfg_c2pt-cfg_c3pt+1:]]

    return temp_data





if __name__ == "__main__":

    if sys.argv[1] == 'gpd':

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]
        NS = int((commands[0].split())[1])
        NT = int((commands[0].split())[2])
        blocksize = int((commands[0].split())[3])
        ave = (commands[0].split())[4]
        foldz = (commands[0].split())[5]
        npick = 0

        # create the work folders here
        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        if 'c3ptfit' in describe:
            resultfolder = resultfolder.replace('results', 'results_c3ptfit')
        elif 'sumfit' in describe:
            resultfolder = resultfolder.replace('results', 'results_sumfit')
        elif 'ratio' in describe:
            resultfolder = resultfolder.replace('results', 'results_ratio')
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)

        print('\nNote! The Ein and Eout in the input file must be listed from small to large !\n')
        print('\nJob describe:', describe, 'NS:', NS, 'NT:', NT, 'Bootstrap bin size:', blocksize)

        Ein_count = 0
        Eout_count = 0
        taumin = 0
        pxlist = []
        bxplist = []
        c2pt_format = []
        c3pt_format = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'npick' == line[0]:
                npick = int(line[1])
                print('Bootstrap sample number: npick =', npick)

            elif 'gmlist' == line[0]:
                gmlist = [line[i] for i in range(1, len(line))]
                print('gmlist:', gmlist)
                if len(gmlist) == 1:
                    describe = describe.replace('gm', gmlist[0])

            elif 'px' == line[0]:
                pxlist += [i for i in range(int(line[2]), int(line[3])+1)]
                bxplist += [line[1] for i in range(int(line[2]), int(line[3])+1)]
                Ein_list = []
                Eout_list = []
            
            elif 'qxyz' in line[0]:
                qxyz = [int(line[1]),int(line[2]),int(line[3])]
                print('qxyz:', qxyz)

            elif 'ts' == line[0]:
                tslist = [int(line[i]) for i in range(1, len(line))]
                describe = describe.replace('ts', str(len(tslist))+'ts')
                print('tslist:', tslist)

            elif 'taumin' in line[0]:
                taumin = int(line[1])
                describe = describe.replace('nsk', 'nsk'+str(taumin))
                print('taumin:', taumin)

            elif 'zrange' in line[0]:
                zlist = [iz for iz in range(int(line[1]),int(line[2])+1)]
                print('zlist:', zlist)

            elif 'c3pt' in line[0]:
                c3pt_format += [line[1]]
                c3pt_datatype = [line[i] for i in range(2, len(line))]
            elif 'c2pt' in line[0]:
                c2pt_format += [line[1]]

            # Note! The Ein and Eout in the input file must be listed from small to large !!!
            elif 'Ein' in line[0]:
                if int(line[1]) in pxlist:
                    Ein_list += [[float(line[i]) for i in range(2, len(line))]]
                    print('Ein:',', px', pxlist[Ein_count], bxplist[Ein_count], Ein_list[Ein_count])
                    Ein_count += 1
            elif 'Eout' in line[0]:
                if int(line[1]) in pxlist:
                    Eout_list += [[float(line[i]) for i in range(2, len(line))]]
                    print('Eout:', ', px',pxlist[Eout_count], bxplist[Eout_count], Eout_list[Eout_count])
                    Eout_count += 1
            elif 'disp_in' == line[0]:
                Ein_read = np.loadtxt(line[1])
                Ein_list = np.empty((len(pxlist), len(line)-2), dtype=float)
                for i in range(0,len(pxlist)):
                    for j in range(2, len(line)):
                        Ein_list[i][j-2] = Ein_read[pxlist[i]][int(line[j])]
                    #Ein_list[i] = np.array([Ein_read[pxlist[i]][int(line[2])], Ein_read[pxlist[i]][int(line[3])], Ein_read[pxlist[i]][int(line[4])], Ein_read[pxlist[i]][int(line[5])]])
                print('Ein:\n', Ein_list)
            elif 'disp_out' in line[0]:
                read_name = line[1].replace('qxqyqz','qx'+str(abs(qxyz[0]))+'qy'+str(abs(qxyz[1]))+'qz'+str(abs(qxyz[2])))
                Eout_read = np.loadtxt(read_name)
                Eout_list = np.empty((len(pxlist), len(line)-2), dtype=float)
                for i in range(0,len(pxlist)):
                    for j in range(2, len(line)):
                        Eout_list[i][j-2] = Eout_read[pxlist[i]][int(line[j])]
                    #Eout_list[i] = np.array([Eout_read[pxlist[i]][int(line[2])], Eout_read[pxlist[i]][int(line[3])], Eout_read[pxlist[i]][int(line[4])], Eout_read[pxlist[i]][int(line[5])]])
                print('Eout:\n', Eout_list)

        # consider the fold of z
        if 'fz' in foldz:
            foldz_print = 'Data for c3pt will be folded (averaged) by (+-) Z.'
            if '+' in foldz:
                foldz = '+'
            elif '-' in foldz:
                foldz = '-'
            else:
                print('***Please spicify how to fold z.')
                os._exit(0)
        else:
            foldz_print = 'Data for c3pt will NOT be folded (averaged) by (+-) Z.'
            foldz = False

        # consider the average of different qx,qy,qz
        if ave == 'ave':

            qxyz_list = qxyz_generate(qxyz)

            if abs(qxyz[1]) != abs(qxyz[2]):
                qxyz_list += qxyz_generate([qxyz[0],qxyz[2],qxyz[1]])

        else:
            qxyz_list = [qxyz]

        print('\n\nData for c2pt (out state) will be averaged by qxyz.')
        print(foldz_print)
        print('Data for c3pt will be averaged by qxyz with proper sign.')

        # run the analysis from here
        samplename = samplefolder + describe + '_bootssample'
        for ipx in range(0,len(pxlist)):

            print('\n\n>>> PX =', pxlist[ipx])

            ipx_describe = describe.replace('*', str(pxlist[ipx]))
            ipx_describe = ipx_describe.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            ipx_samplename = samplename.replace('*', str(pxlist[ipx]))
            ipx_samplename = ipx_samplename.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))

            # Data format
            ipx_c2pt_format = []
            for i_fmt in  c2pt_format:
                ipx_c2pt_format += [i_fmt.replace('.bxp', '.'+bxplist[ipx])]
            ipx_c3pt_format = []
            for i_fmt in c3pt_format:
                temp = i_fmt.replace('.bxp', '.'+bxplist[ipx])
                ipx_c3pt_format += [temp.replace('*', str(pxlist[ipx]))]
            #print(ipx_c3pt_format)

            # Read 2pt data
            c2pt_data_in, c2pt_data_out = ave_c2pt_data(ipx_c2pt_format, pxlist[ipx], qxyz_list)
            #print(c2pt_data_in[12])
            c3pt_data = []

            igm_count = 0
            for igm in gmlist:

                igm_describe = ipx_describe.replace('gm', igm)
                igm_samplename = ipx_samplename.replace('gm', igm)
                igm_c3pt_format = []
                for i_fmt in ipx_c3pt_format:
                    igm_c3pt_format += [i_fmt.replace('gm', igm)]
                #print(ipx_c3pt_format,igm_c3pt_format)
                print(Ein_list)
                if igm != 'g0':
                    nm_bm = 1
                    igm_sign_list, igm_qxyz_list = sign_combine(Ein_list[ipx][1], Eout_list[ipx][1], pxlist[ipx], qxyz_list, igm)
                else:
                    nm_bm = 1
                    igm_sign_list = [nm_bm for iq in range(0, len(qxyz_list))]

                print('\n  gm=', igm,' c3pt data will be normalized (times) by (Sqrt(Eout*Ein))/((Pout_mu+Pin_mu)/2).')
                print('  qxyz list:', igm_qxyz_list)
                print('  factor list:', igm_sign_list)

                # Read averaged data
                c3pt_data += [ave_c3pt_data(igm_c3pt_format, igm_qxyz_list, tslist, zlist, c3pt_datatype[igm_count], igm_sign_list, foldz)]
                igm_count += 1

            print('\n  C2pt data are collected in shape:', np.shape(c2pt_data_in), np.shape(c2pt_data_out))
            print('  C3pt data are collected in shape:', np.shape(c3pt_data), 'for gmlist:', gmlist)
            c3pt_data = To_breit_c3pt(c3pt_data, gmlist, Ein_list[ipx][1], Eout_list[ipx][1])

            if 'ratio' in describe:
                print('\nConstruct the ratio ...')
                for its in range(0, len(tslist)):
                    if len(gmlist) == 1:
                        its_describe = igm_describe.replace('ts', 'ts'+str(tslist[its]))
                        its_samplename = igm_samplename.replace('ts_bootssample', 'ts'+str(tslist[its])+'_bootssample')
                    else:
                        its_describe = ipx_describe.replace('ts', 'ts'+str(tslist[its]))
                        its_samplename = ipx_samplename.replace('ts_bootssample', 'ts'+str(tslist[its])+'_bootssample')
                    for iz in range(0, len(zlist)):
                        iz_describe = its_describe.replace('_X', '_X'+str(zlist[iz]))
                        iz_samplename = its_samplename.replace('_X', '_X'+str(zlist[iz])) + '.txt'

                        iz_c2pt_data_in = match_c2pt_c3pt(c2pt_data_in, c3pt_data[its][iz])
                        iz_c2pt_data_out = match_c2pt_c3pt(c2pt_data_out, c3pt_data[its][iz])
                        print('  PX:', pxlist[ipx], ', ts:', tslist[its],', z:', zlist[iz], ', data-shape:',np.shape(iz_c2pt_data_in), np.shape(c3pt_data[its][iz]))
                            
                        iz_ratio_err, iz_ratio_sample = gpd_ratio_boots(c3pt_data[its][iz], iz_c2pt_data_in, iz_c2pt_data_out, tslist[its], Ein_list[ipx], Eout_list[ipx], NT, blocksize, taumin, nm_bm,npick)
                        #np.savetxt(iz_samplename,iz_ratio_sample,fmt='%.6e')
                        for itau in range(0, len(iz_ratio_err)):
                            iz_ratio_err[itau][0] -= tslist[its]/2
                            
                        out_file = resultfolder + '/' + iz_describe + '.txt'
                        np.savetxt(out_file, iz_ratio_err,fmt='%.6e')

            elif 'sumfit' in describe:
                print('\nDoing the summation fit ...')
                ipx_bm = []
                for iz in range(0, len(zlist)):
                    iz_samplename = ipx_samplename.replace('_X', '_X'+str(zlist[iz])) + '.txt'

                    iz_c3pt_data = []
                    for its in range(0, len(tslist)):
                        iz_c3pt_data += [c3pt_data[its][iz]]
                        
                    iz_c2pt_data_in = match_c2pt_c3pt(c2pt_data_in, c3pt_data[its][iz])
                    iz_c2pt_data_out = match_c2pt_c3pt(c2pt_data_out, c3pt_data[its][iz])
                    print('PX:', pxlist[ipx],', z:', zlist[iz],', data-shape:',np.shape(iz_c2pt_data_in), np.shape(iz_c3pt_data), '\nEin:', Ein_list[ipx][1], ', Eout:', Eout_list[ipx][1])

                    iz_bm, iz_bm_sample = gpd_sumfit_boots(iz_c3pt_data, iz_c2pt_data_in, iz_c2pt_data_out, tslist, Ein_list[ipx], Eout_list[ipx], NT, blocksize, taumin, nm_bm,npick)
                    np.savetxt(iz_samplename,iz_bm_sample,fmt='%.6e')
                    iz_bm = [zlist[iz]] + iz_bm
                    ipx_bm += [iz_bm]

                ipx_outname = resultfolder + '/' + ipx_describe + '.txt'
                np.savetxt(ipx_outname,ipx_bm,fmt='%.6e')

            elif 'c3ptfit' in describe:

                print('\nDoing the ratio fit ...')

                ipx_bm = []
                for iz in range(0, len(zlist)):

                    iz_samplename = ipx_samplename.replace('_X', '_X'+str(zlist[iz])) + '.txt'

                    iz_c3pt_data = []
                    for its in range(0, len(tslist)):
                        iz_c3pt_data += [c3pt_data[its][iz]]

                    iz_c2pt_data_in = match_c2pt_c3pt(c2pt_data_in, c3pt_data[its][iz])
                    iz_c2pt_data_out = match_c2pt_c3pt(c2pt_data_out, c3pt_data[its][iz])
                    print('PX:', pxlist[ipx], ', z:', zlist[iz],', data-shape:',np.shape(iz_c2pt_data_in), np.shape(iz_c3pt_data), '\nEin:', Ein_list[ipx][1], ', Eout:', Eout_list[ipx][1])

                    iz_bm, iz_bm_sample = gpd_ratiofit_boots(iz_c3pt_data, iz_c2pt_data_in, iz_c2pt_data_out, tslist, Ein_list[ipx], Eout_list[ipx], NT, blocksize, taumin, nm_bm,npick)
                    for irow in range(0, len(iz_bm_sample)):
                        #iz_bm_sample[irow] = [0.938253] + iz_bm_sample[irow]
                        iz_bm_sample[irow] = [0.741335] + iz_bm_sample[irow]
                    np.savetxt(iz_samplename,iz_bm_sample,fmt='%.6e')
                    iz_bm = [zlist[iz]] + iz_bm
                    ipx_bm += [iz_bm]

                ipx_outname = resultfolder + '/' + ipx_describe + '.txt'
                np.savetxt(ipx_outname,ipx_bm,fmt='%.6e')

                        


