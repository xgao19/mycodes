#!/usr/bin/env python3
import numpy as np
import traceback 
import random
import sys
from tools import *
import inspect
from scipy.optimize import curve_fit
from modelfuncs import*
from statistics import*

np.random.seed(2020)
random_uniform = np.random.uniform(0,1,1000000)
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

'''
This code is to construct ratio scheme RG invariant ratio 
from Bootstrap bare matrix elements.
'''

def bootsbm_collect(sample_format,col_sample,zlist,pxlist, qxyz=[]):
    
    # data collection
    bm_mean_data = []
    bm_samples_data = []
    for ipx in range(0,len(pxlist)):
        if len(qxyz) != 0:
            ipx_sample_format = sample_format.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            #if pxlist[ipx] == 5 and 'qx0qy2qz2' in ipx_mean_format:
            #    ipx_mean_format = ipx_mean_format.replace('4ts', '3ts')
            #    ipx_sample_format = ipx_sample_format.replace('4ts', '3ts')

        sampledata = (np.loadtxt((ipx_sample_format.replace('*',str(pxlist[ipx]))).replace('#', str(zlist[0]))))
        nboots = len(sampledata)

        ipx_in_format = ipx_sample_format.replace('*', str(pxlist[ipx]))
        ipx_data = []

        for iz in range(0, len(zlist)):
            iz_ipx_in_name = ipx_in_format.replace('#', str(zlist[iz]))
            jacksample_real = np.loadtxt(iz_ipx_in_name)
            nboots = len(jacksample_real)
            #print(jacksample_real)
            if 'real' in iz_ipx_in_name:
                jacksample_imag = np.loadtxt(iz_ipx_in_name.replace('real','imag'))
                try:
                    jacksample = [complex(jacksample_real[i][col_sample],jacksample_imag[i][col_sample]) for i in range(0, nboots)]
                except:
                    jacksample = [complex(jacksample_real[i],jacksample_imag[i]) for i in range(0, nboots)]
            else:
                try:
                    jacksample = [jacksample_real[i][col_sample] for i in range(0, nboots)]
                except:
                    jacksample = [jacksample_real[i] for i in range(0, nboots)]
            ipx_data += [jacksample]
        bm_samples_data += [ipx_data]
    print('bare matrix samples shape:', np.shape(bm_samples_data),'\n')
    return bm_samples_data

def bootsbm_to_rbm_interpolation(Ns,latsp,up_sample,up_col_sample, down_sample,down_col_sample, \
                zlist, pxlist, outfolder, describe, qxyz):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    zlist_fm = [iz*latsp for iz in zlist]
    zstep = 0.02
    zlist_fm_save = np.arange(0, zlist_fm[-1], zstep)
    
    # data collection
    bm_samples_data = bootsbm_collect(up_sample,up_col_sample,zlist,pxlist,qxyz)
    bm_samples_norm = bm_samples_data

    rITD_samp_lenMax = 0
    for isamp in bm_samples_data:
        for izsamp in isamp:
            if len(izsamp) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(izsamp)
    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)
        
    # reduced bare matrix elements
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_bootssamples'
    
    for ipx in range(0, len(pxlist)):
        
        ipx_ritd_real = []
        ipx_ritd_imag = []

        ipx_sample_real = []
        ipx_sample_imag = []
        
        for iz in range(0, len(zlist)):

            p0_cfg = len(bm_samples_norm[0][iz])
            px_cfg = len(bm_samples_data[ipx][iz])

            sample_real = []
            sample_imag = []

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, rITD_samp_lenMax):
                
                isample = bm_samples_data[ipx][iz][isamp]/bm_samples_data[ipx][0][isamp]
                sample_real += [isample.real]
                sample_imag += [isample.imag]

            '''
            np.savetxt(samplefile+'.real', sample_real)
            if 'real' in up_sample:
                np.savetxt(samplefile+'.imag', sample_imag)
            '''
            ipx_sample_real += [sample_real]
            ipx_sample_imag += [sample_imag]

        ipx_itp_real_samples = []
        ipx_itp_imag_samples = []
        for isamp in range(0, rITD_samp_lenMax):
            ipx_itp_real_samples += [interp1d(zlist_fm, [ipx_sample_real[iz][isamp] for iz in range(0, len(ipx_sample_real))], kind='cubic')(zlist_fm_save)]
            ipx_itp_imag_samples += [interp1d(zlist_fm, [ipx_sample_imag[iz][isamp] for iz in range(0, len(ipx_sample_real))], kind='cubic')(zlist_fm_save)]
        for iz in range(0, len(zlist_fm_save)):

            sample_real = [ipx_itp_real_samples[isamp][iz] for isamp in range(0, rITD_samp_lenMax)]
            sample_imag = [ipx_itp_imag_samples[isamp][iz] for isamp in range(0, rITD_samp_lenMax)]
            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(iz))
            np.savetxt(samplefile+'.real', sample_real, fmt='%.8e')
            if 'real' in up_sample:
                np.savetxt(samplefile+'.imag', sample_imag, fmt='%.8e')

            sort_sample_real = sorted(sample_real)
            sort_sample_imag = sorted(sample_imag)
            mean_real = sort_sample_real[mid]
            mean_real_err = (sort_sample_real[high16]-sort_sample_real[low16])/2
            mean_imag = sort_sample_imag[mid]
            mean_imag_err = (sort_sample_imag[high16]-sort_sample_imag[low16])/2
            ipx_ritd_real += [[zlist_fm_save[iz], mean_real, mean_real_err]]
            ipx_ritd_imag += [[zlist_fm_save[iz], mean_imag, mean_imag_err]]

        print('bare matrix samples shape to save:', np.shape(ipx_ritd_real))
        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#','')
        print('Saving in:',ritd_out_name,'\n')
        ritd_out_name_real = ritd_out_name + '.real'
        np.savetxt(ritd_out_name_real,ipx_ritd_real, fmt='%.8e')
        if 'real' in up_sample:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ipx_ritd_imag, fmt='%.8e')

def bootsbm_to_rbm(Ns,latsp,up_sample,up_col_sample, down_sample,down_col_sample, \
                zlist, pxlist, outfolder, describe, qxyz):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    # data collection
    bm_samples_data = bootsbm_collect(up_sample,up_col_sample,zlist,pxlist,qxyz)
    bm_samples_norm = bm_samples_data

    rITD_samp_lenMax = 0
    for isamp in bm_samples_data:
        for izsamp in isamp:
            if len(izsamp) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(izsamp)
    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)
        
    # reduced bare matrix elements
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_bootssamples'
    
    for ipx in range(0, len(pxlist)):
        
        ipx_ritd_real = []
        ipx_ritd_imag = []
        
        for iz in range(0, len(zlist)):

            p0_cfg = len(bm_samples_norm[0][iz])
            px_cfg = len(bm_samples_data[ipx][iz])

            print('z=',zlist[iz],'p0 #samples:', p0_cfg, 'px', pxlist[ipx], 'nboots:', px_cfg)
            

            ipx_iz_ritd_real = []
            ipx_iz_ritd_imag = []
            sample_real = []
            sample_imag = []

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, rITD_samp_lenMax):
                
                isample = bm_samples_data[ipx][iz][isamp]/bm_samples_data[ipx][0][isamp]
                sample_real += [isample.real]
                sample_imag += [isample.imag]
            
            sort_sample_real = sorted(sample_real)
            sort_sample_imag = sorted(sample_imag)
            mean_real = sort_sample_real[mid]
            mean_real_err = (sort_sample_real[high16]-sort_sample_real[low16])/2
            mean_imag = sort_sample_imag[mid]
            mean_imag_err = (sort_sample_imag[high16]-sort_sample_imag[low16])/2

            ipx_iz_ritd_real = [zlist[iz], mean_real, mean_real_err]
            ipx_iz_ritd_imag = [zlist[iz], mean_imag, mean_imag_err]
            ipx_ritd_real += [ipx_iz_ritd_real]
            ipx_ritd_imag += [ipx_iz_ritd_imag]
            np.savetxt(samplefile+'.real', sample_real)
            if 'real' in up_sample:
                np.savetxt(samplefile+'.imag', sample_imag)

        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print('\n Saving in:',ritd_out_name)
        ritd_out_name_real = ritd_out_name + '.real'
        np.savetxt(ritd_out_name_real,ipx_ritd_real)
        if 'real' in up_sample:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ipx_ritd_imag)

def bootsbm_to_rITD(Ns,latsp,up_sample,up_col_sample, down_sample,down_col_sample, \
                zlist, pxlist, outfolder, describe, qxyz):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)
    
    # data collection
    bm_samples_norm = bootsbm_collect(down_sample,down_col_sample,zlist,[0],[0,0,0])
    bm_samples_data = bootsbm_collect(up_sample,up_col_sample,zlist,pxlist,qxyz)

    rITD_samp_lenMax = 0
    for isamp in bm_samples_data:
        for izsamp in isamp:
            if len(izsamp) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(izsamp)
    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)
        
    # reduced ioffe time distribution
    ritd_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_bootssamples'
    ritd_real = []
    ritd_imag = []
    
    for ipx in range(0, len(pxlist)):
        
        ipx_ritd_real = []
        ipx_ritd_imag = []
        
        for iz in range(0, len(zlist)):

            p0_cfg = len(bm_samples_norm[0][iz])
            px_cfg = len(bm_samples_data[ipx][iz])

            print('z=',zlist[iz],'p0 #samples:', p0_cfg, 'px', pxlist[ipx], 'nboots:', px_cfg)
            

            ipx_iz_ritd_real = []
            ipx_iz_ritd_imag = []
            sample_real = []
            sample_imag = []

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, rITD_samp_lenMax):
                #isample_data = bm_samples_data[ipx][iz][isamp%px_cfg]/bm_samples_data[ipx][0][isamp%px_cfg]
                #isample_norm = bm_samples_norm[0][iz][isamp%p0_cfg]/bm_samples_norm[0][0][isamp%p0_cfg]
                
                if isamp < px_cfg:
                    #print('YES',isamp)
                    isample_data = bm_samples_data[ipx][iz][isamp]/bm_samples_data[ipx][0][isamp]
                else:
                    #print('NO',isamp)
                    ipick = random_uniform_int[-1][isamp]
                    #ipick = np.random.randint(px_cfg)
                    isample_data = bm_samples_data[ipx][iz][ipick%px_cfg]/bm_samples_data[ipx][0][ipick%px_cfg]
                    #isample_data = bm_samples_data[ipx][iz][random.randint(0, px_cfg-1)]/bm_samples_data[ipx][0][random.randint(0, px_cfg-1)]

                if isamp < p0_cfg:
                    isample_norm = bm_samples_norm[0][iz][isamp]/bm_samples_norm[0][0][isamp]
                    #print('YES',isamp)
                else:
                    ipick = random_uniform_int[-1][isamp]
                    #print('NO')
                    #ipick = np.random.randint(p0_cfg)
                    isample_norm = bm_samples_norm[0][iz][ipick%p0_cfg]/bm_samples_norm[0][0][ipick%p0_cfg]
                    #isample_norm = bm_samples_norm[0][iz][random.randint(0, p0_cfg-1)]/bm_samples_norm[0][0][random.randint(0, p0_cfg-1)]
                    
                isample = isample_data/isample_norm
                sample_real += [isample.real]
                sample_imag += [isample.imag]
            
            sort_sample_real = sorted(sample_real)
            sort_sample_imag = sorted(sample_imag)
            mean_real = sort_sample_real[mid]
            mean_real_err = (sort_sample_real[high16]-sort_sample_real[low16])/2
            mean_imag = sort_sample_imag[mid]
            mean_imag_err = (sort_sample_imag[high16]-sort_sample_imag[low16])/2

            ipx_iz_ritd_real = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, mean_real, mean_real_err]
            ipx_iz_ritd_imag = [pxlist[ipx]*zlist[iz]*2*3.14259/Ns, mean_imag, mean_imag_err]
            #ipx_iz_ritd_real = [zlist[iz]*latsp, mean_real, mean_real_err]
            #ipx_iz_ritd_imag = [zlist[iz]*latsp, mean_imag, mean_imag_err]
            ipx_ritd_real += [ipx_iz_ritd_real]
            ipx_ritd_imag += [ipx_iz_ritd_imag]
            np.savetxt(samplefile+'.real', sample_real)
            if 'real' in up_sample:
                np.savetxt(samplefile+'.imag', sample_imag)

        ritd_out_name = ritd_out_format.replace('*',str(pxlist[ipx]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print('\n Saving in:',ritd_out_name)
        ritd_out_name_real = ritd_out_name + '.real'
        if len(ipx_ritd_real) > 1:
            #print('Here')
            np.savetxt(ritd_out_name_real,ipx_ritd_real)
        else:
            p_vec_in = [pxlist[ipx]*2*3.14159/(Ns*latsp)/5.0677, 0, 0]
            p_vec_out = [(pxlist[ipx]+qxyz[0])*2*3.14159/(Ns*latsp)/5.0677, qxyz[1]*2*3.14159/(Ns*latsp)/5.0677, qxyz[2]*2*3.14159/(Ns*latsp)/5.0677]
            Ein = np.sqrt(0.3**2+p_vec_in[0]**2+p_vec_in[1]**2+p_vec_in[2]**2)
            Eout = np.sqrt(0.3**2+p_vec_out[0]**2+p_vec_out[1]**2+p_vec_out[2]**2)
            t = (p_vec_out[0]-p_vec_in[0])**2+(p_vec_out[1]-p_vec_in[1])**2+(p_vec_out[2]-p_vec_in[2])**2-(Eout-Ein)**2
            ipx_ritd_real[0][0] = t
            ipx_ritd_imag[0][0] = t
            ritd_real += ipx_ritd_real
            ritd_imag += ipx_ritd_imag
        if 'real' in up_sample:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ipx_ritd_imag)

    if len(ipx_ritd_real) == 1:
        ritd_out_name = ritd_out_format.replace('*',str(pxlist[0])+'-'+str(pxlist[-1]))
        ritd_out_name = ritd_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        ritd_out_name_real = ritd_out_name + '.real'
        print(np.shape(ritd_real))
        np.savetxt(ritd_out_name_real,ritd_real)
        if 'real' in up_sample:
            ritd_out_name_imag = ritd_out_name + '.imag'
            np.savetxt(ritd_out_name_imag,ritd_imag)
            
if __name__ == "__main__":

    inputfile = open(sys.argv[2],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[2])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    latsp = float(line1st[2])
    print('\n\n\n\n\n\n\njob describe:',describe,'\nNs:',Ns,'lattice spacing:',latsp,'\n')

    # read parameters
    qxyz = []
    lat = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'qxyz' in line[0]:
            qxyz = [int(line[1]), int(line[2]), int(line[3])]
            describe = describe.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            print('qxyz:',qxyz)

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        if 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)

        if 'xvalue' in line[0]:
            xvalue_min = float(line[1])
            xvalue_max = float(line[2])

        if 'upsample' in line[0]:
            up_sample = line[1] #numerator of the RG invariant ratio
            up_col_sample = int(line[2])


        if 'downsample' in line[0]:
            down_sample = line[1] #denominator of the RG invariant ratio
            down_col_sample = int(line[2])

    if sys.argv[1] == 'ritd':
        bootsbm_to_rITD(Ns,latsp,up_sample,up_col_sample,down_sample,down_col_sample,zlist,pxlist,outfolder,describe,qxyz)
    elif sys.argv[1] == 'rbm':
        bootsbm_to_rbm(Ns,latsp,up_sample,up_col_sample,down_sample,down_col_sample,zlist,pxlist,outfolder,describe,qxyz)
    elif sys.argv[1] == 'ibm':
        bootsbm_to_rbm_interpolation(Ns,latsp,up_sample,up_col_sample,down_sample,down_col_sample,zlist,pxlist,outfolder,describe,qxyz)
