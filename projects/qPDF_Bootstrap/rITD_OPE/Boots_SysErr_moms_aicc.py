#!/usr/bin/env python3
import numpy as np
import sys
from tools import *

def data_collection(filename, sample, row):
    
    read_data = []
    for i in sample:
        read_data += [np.loadtxt(filename.replace('#', str(i)))]
        #print(np.shape(read_data))
    dataset = []
    for i in row:
        row_samples = []
        for isamp in range(0, len(read_data)):
            row_samples += [list(read_data[isamp][i])]
        dataset += [row_samples]
    return dataset

def moms_conti(momsSample, contiSample, massSample=[], order=1):
    Latsp = 0
    up = int(len(momsSample)*0.84)
    mid = int(len(momsSample)*0.5)
    down = int(len(momsSample)*0.16)
    momslist0 = []
    momslist1 = []
    momslist2 = []
    while Latsp <= 0.1:
        iasp_momsSample = []
        if order == 1:
            x = Latsp
        elif order == 2:
            x = Latsp*Latsp
        for i in range(0,len(momsSample)):
            iasp_momsSample += [momsSample[i]+x*contiSample[i]]
        iasp_momsSample = sorted(iasp_momsSample)
        momslist0 += [[Latsp*Latsp, iasp_momsSample[down]]]
        momslist1 += [[Latsp*Latsp, iasp_momsSample[mid]]]
        momslist2 += [[Latsp*Latsp, iasp_momsSample[up]]]
        Latsp += 0.001
    return momslist0,momslist1,momslist2


if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    line2nd = commands[1].split()
    sample = [i for i in range(int(line2nd[1]), int(line2nd[2])+1)]
    print('Sample in range: [', line2nd[1],',', line2nd[2],']')

    dataset = []
    orderLatsp = 2

    for index in range(2, len(commands)):

        line = commands[index].split()
        if 'data' == line[0]:
            filename = line[1]
            rowlist = [i for i in range(int(line[2]), int(line[3])+1)]
            print('Reading file:', filename.split('/')[-1], ', row in range: [', line[2],',', line[3],']')
            dataset += data_collection(filename, sample, rowlist)
        elif 'orderLatsp' == line[0]:
            orderLatsp = int(line[1])
            print('Order of continuum extrapolation:', orderLatsp)
    
    print('-- Read data with shape:', np.shape(dataset))

    new_dataset = np.zeros((len(dataset[0][0]), len(dataset[0]), len(dataset)))
    for iv in range(0, len(dataset[0][0])):        #variable
        for isamp in range(0, len(dataset[0])):    #sample
            for ic in range(0, len(dataset)):      #column
                new_dataset[iv][isamp][ic] = dataset[ic][isamp][iv]
                if isamp == 0 and iv == 0:
                    print(dataset[ic][isamp][1])
    print('-- Re-arrange the data with shape:', np.shape(new_dataset))


    MeanErrSamp = np.zeros((len(new_dataset), len(new_dataset [0]), 2))
    for iv in range(0, len(new_dataset)):
        for isamp in range(0, len(new_dataset[0])):

            MeanErrSamp[iv][isamp][0] = 0
            MeanErrSamp_WeightSum = 0
            for iset in range(0, len(new_dataset[iv][isamp])):
                #iset_Weight = np.exp(-0.5 * new_dataset[-2][isamp][iset])
                iset_Weight = 1
                MeanErrSamp[iv][isamp][0] += new_dataset[iv][isamp][iset] * iset_Weight
                MeanErrSamp_WeightSum += iset_Weight
            MeanErrSamp[iv][isamp][0] /= MeanErrSamp_WeightSum

            MeanErrSamp[iv][isamp][1] = 0
            for iset in range(0, len(new_dataset[iv][isamp])):
                #iset_Weight = np.exp(-0.5 * new_dataset[-2][isamp][iset])
                iset_Weight = 1
                MeanErrSamp[iv][isamp][1] += (new_dataset[iv][isamp][iset] - MeanErrSamp[iv][isamp][0])**2 * iset_Weight
                MeanErrSamp_WeightSum += iset_Weight
            MeanErrSamp[iv][isamp][1] = np.sqrt(MeanErrSamp[iv][isamp][1]/MeanErrSamp_WeightSum)
    
    up = int((len(sample)*0.84))
    mid = int((len(sample)*0.5))
    down = int((len(sample)*0.16))
    MeanErr = np.zeros((len(MeanErrSamp), 5))
    for iv in range(0, len(MeanErrSamp)):
        meanlist = sorted([MeanErrSamp[iv][isamp][0] for isamp in range(0, len(MeanErrSamp[iv]))])
        SysErrlist = sorted([MeanErrSamp[iv][isamp][1] for isamp in range(0, len(MeanErrSamp[iv]))])
        MeanErr[iv][0] = (meanlist[down] + meanlist[up])/2
        MeanErr[iv][1] = (meanlist[up]-meanlist[down])/2
        MeanErr[iv][4] = SysErrlist[mid]
        #MeanErr[iv][2] = np.sqrt(MeanErr[iv][1]**2 + SysErrlist[mid]**2)
        MeanErr[iv][2] = MeanErr[iv][1] + SysErrlist[mid]
        MeanErr[iv][3] = np.sqrt(MeanErr[iv][1]**2 + SysErrlist[mid]**2)

    print(MeanErr)
    np.savetxt(outfolder+describe+'.txt', MeanErr)
    


