#!/usr/bin/env python3
import numpy as np

moms_pznm1_format = '/Users/Xiang/Desktop/docs/0-2021/analysis/3-qPDF-ratio/Moments/joint/a2/results/samples/joint_pznm1_rITDmomsritdNNLO_2GeV_zmin2_zmax0.8_rcut_contiN6_CmassN0_bootssample#.txt'
moms_pznm2_format = '/Users/Xiang/Desktop/docs/0-2021/analysis/3-qPDF-ratio/Moments/joint/a2/results/samples/joint_pznm2_rITDmomsritdNNLO_2GeV_zmin2_zmax0.8_rcut_contiN6_CmassN0_bootssample#.txt'

save_name = '/Users/Xiang/Desktop/docs/0-2021/analysis/3-qPDF-ratio/Moments/joint/a2/Sys_Effbeta/joint_PD_rITDmomsritdNNLO_2GeV_contiN6_CmassN0_Effbeta.txt'

npick = 200
up = int(npick*0.84)
mid = int(npick*0.5)
down = int(npick*0.16)

col = [2,3,4]
row = [3,4]

Effbeta_ave_samples = np.zeros((len(col)-1, npick))
Effbeta_std_samples = np.zeros((len(col)-1, npick))
for isamp in range(0, npick):

    moms_pD1_read = np.loadtxt(moms_pznm1_format.replace('#',str(isamp)))
    moms_pD2_read = np.loadtxt(moms_pznm2_format.replace('#',str(isamp)))
    moms_pD1_read = np.array([moms_pD1_read[irow] for irow in row])
    moms_pD2_read = np.array([moms_pD2_read[irow] for irow in row])
    if isamp == 0:
        print('PD1 zmax:',moms_pD1_read[0][1], '-',moms_pD1_read[-1][1])
        print('PD2 zmax:',moms_pD2_read[0][1], '-',moms_pD2_read[-1][1])

    moms_PD_sample = []
    for i in range(0, len(moms_pD1_read)):
        moms_PD_sample += [[1] + [moms_pD1_read[i][icol] for icol in col]]
    for i in range(0, len(moms_pD1_read)):
        moms_PD_sample += [[1] + [moms_pD2_read[i][icol] for icol in col]]

    Effbeta_sample_ave = []
    Effbeta_sample_std = []
    for i in range(1, len(moms_PD_sample[0])-1):
        i_Effbeta = []
        for irow in range(0, len(moms_PD_sample)):
            irow_Effbeta_sample = -1 + 2*i/4*(moms_PD_sample[irow][i-1]-moms_PD_sample[irow][i+1])/moms_PD_sample[irow][i]
            i_Effbeta += [irow_Effbeta_sample]
        Effbeta_ave_samples[i-1][isamp] = np.average(i_Effbeta)
        Effbeta_std_samples[i-1][isamp] = np.std(i_Effbeta)
        #print(2*i, Effbeta_ave_samples[i-1][isamp])

Effbeta_MeanErr = np.zeros((len(col)-1, 5))
for i in range(0, len(Effbeta_ave_samples)):
    i_Effbeta_ave_sorted = sorted(Effbeta_ave_samples[i])
    i_Effbeta_std_sorted = sorted(Effbeta_std_samples[i])

    Effbeta_MeanErr[i][0] = 2*(i+1)
    Effbeta_MeanErr[i][1] = (i_Effbeta_ave_sorted[up] + i_Effbeta_ave_sorted[down])/2
    Effbeta_MeanErr[i][2] = (i_Effbeta_ave_sorted[up] - i_Effbeta_ave_sorted[down])/2
    Effbeta_MeanErr[i][3] = i_Effbeta_std_sorted[mid]
    Effbeta_MeanErr[i][4] = np.sqrt(Effbeta_MeanErr[i][2]**2+Effbeta_MeanErr[i][3]**2)
    print(Effbeta_MeanErr[i])

np.savetxt(save_name, Effbeta_MeanErr, fmt='%.4e')