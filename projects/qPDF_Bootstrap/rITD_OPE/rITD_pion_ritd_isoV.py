#!/usr/bin/env python3
import numpy as np
import numba as nb
from rITD_Numerical_Kernel_isoV import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################## Global parameters ######################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

CF = 4/3
nf = 3
TF = 1/2
CA = 3
PI=3.1415926
pi=3.1415926
fmGeV = 5.0676896
gammaE = 0.5772156649


#@nb.jit
#def alpsmu(mu, alps0 = alps0, mu0=mu0*5.0676896):
#    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
@nb.jit
def alpsmu(mu, Nf = 3):
    mu /= 5.0676896
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s
def alpsmuNNLO(mu, Nf = 3):
    return alpsmu(mu, Nf = 3)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Auxiliary functions ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
@nb.jit
def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def CnNLO(z,n, mu,alps):
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))
def CnNLOpol(z,n, mu,alps):
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)) + alps*CF/(2*pi)*2/(2+3*n+n*n)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ########################## Neat kernels ############################ ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def CnC0NLO(mu,z,n):
    return CnNLO(z,n, mu, alpsmu(mu))/CnNLO(z,0, mu, alpsmu(mu))
def CnC0NLOpol(mu,z,n):
    return CnNLOpol(z,n, mu, alpsmu(mu))/CnNLOpol(z,0, mu, alpsmu(mu))
def CnC0NNLO(mu,z,n,k=0, **kwargs):
    return CnNNLO(z, n, mu, alpsmu(mu))/CnNNLO(z, 0, mu, alpsmu(mu))