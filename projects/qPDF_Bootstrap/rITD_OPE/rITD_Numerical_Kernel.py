#!/usr/bin/env python3
import numpy as np
import math
import traceback 
import sys
from scipy.interpolate import interp1d

PI=3.1415926

#####################################################
################    NNLO kernel    ##################
#####################################################

def CnNNLO(z, n, mu, alps):

    if n == 0:
        return 1. + alps*(0.456712 + 0.802918*alps) + \
            alps*np.log(mu*mu*z*z)*(0.31831  + 0.738604*alps + 0.164647*alps*np.log(mu*mu*z*z))
    
    elif n == 2:
        return 1. + alps*(-0.865982 + 1.89622*alps) + \
            alps*np.log(mu*mu*z*z)*(0.760407 - 0.661031*alps + 0.56141*alps*np.log(mu*mu*z*z))

    elif n == 4:
        return 1. + alps*(-1.69317 + 3.93486*alps) + \
            alps*np.log(mu*mu*z*z)*(0.962003 - 2.09953*alps + 0.807217*alps*np.log(mu*mu*z*z))

    elif n == 6:
        return 1. + alps*(-2.33055 + 6.12108*alps) + \
            alps*np.log(mu*mu*z*z)*(1.09665 - 3.40088*alps + 0.994034*alps*np.log(mu*mu*z*z))

    elif n == 8:
        return 1. + alps*(-2.85812 + 8.32132*alps) + \
            alps*np.log(mu*mu*z*z)*(1.19829  - 4.58506*alps + 1.14706*alps*np.log(mu*mu*z*z))

    elif n == 10:
        return 1. + alps*(-3.31224 + 10.4935*alps) + \
            alps*np.log(mu*mu*z*z)*(1.28007 - 5.6741*alps + 1.27768*alps*np.log(mu*mu*z*z))

    elif n == 12:
        return 1. + alps*(-3.71308 + 12.6225*alps) + \
            alps*np.log(mu*mu*z*z)*(1.34852 - 6.68504*alps + 1.39216*alps*np.log(mu*mu*z*z))

    elif n == 14:
        return 1. + alps*(-4.07319 + 14.703*alps) + \
            alps*np.log(mu*mu*z*z)*(1.40742 - 7.63067*alps + 1.4944*alps*np.log(mu*mu*z*z))

    elif n == 16:
        return 1. + alps*(-4.401 + 16.7342*alps) + \
            alps*np.log(mu*mu*z*z)*(1.4591 - 8.52079*alps + 1.58698*alps*np.log(mu*mu*z*z))

    elif n == 18:
        return 1. + alps*(-4.70246 + 18.7171*alps) + \
            alps*np.log(mu*mu*z*z)*(1.50515  - 9.36302*alps + 1.67173*alps*np.log(mu*mu*z*z))

    elif n == 20:
        return 1. + alps*(-4.98194 + 20.6535*alps) + \
            alps*np.log(mu*mu*z*z)*(1.54668 - 10.1634*alps + 1.74997*alps*np.log(mu*mu*z*z))

    elif n == 22:
        return 1. + alps*(-5.24279 + 22.5455*alps) + \
            alps*np.log(mu*mu*z*z)*(1.5845 - 10.927*alps + 1.82272 *alps*np.log(mu*mu*z*z))

    elif n == 24:
        return 1. + alps*(-5.4876 + 24.3955*alps) + \
            alps*np.log(mu*mu*z*z)*(1.61922 - 11.6576*alps + 1.89077*alps*np.log(mu*mu*z*z))

    elif n == 26:
        return 1. + alps*(-5.71844 + 26.2056*alps) + \
            alps*np.log(mu*mu*z*z)*(1.6513 - 12.3588*alps + 1.95473*alps*np.log(mu*mu*z*z))

    elif n == 28:
        return 1. + alps*(-5.937 + 27.9779*alps) + \
            alps*np.log(mu*mu*z*z)*(1.68113 - 13.0333*alps + 2.01512*alps*np.log(mu*mu*z*z))

    elif n == 30:
        return 1. + alps*(-6.14464 + 29.7146*alps) + \
            alps*np.log(mu*mu*z*z)*(1.709 - 13.6835*alps + 2.07234*alps*np.log(mu*mu*z*z))

    else:
        return 0

def CnC0NNLO_EP_numerical(z, n, mu, alps):

    if n == 0:
        return 1

    elif n == 2:
        return 1. + alps*(-1.32269 + 1.69739*alps) + \
            alps*np.log(mu*mu*z*z)*(0.442097 - 1.18052*alps + 0.256039*alps*np.log(mu*mu*z*z))
    
    elif n == 4:
        return 1. + alps*(-2.14989 + 4.11382*alps) + \
            alps*np.log(mu*mu*z*z)*(0.643693  - 2.44779*alps + 0.437676*alps*np.log(mu*mu*z*z))

    elif n == 6:
        return 1. + alps*(-2.78726 + 6.59114*alps) + \
            alps*np.log(mu*mu*z*z)*(0.778343 - 3.60775*alps + 0.581633*alps*np.log(mu*mu*z*z))

    elif n == 8:
        return 1. + alps*(-3.31483 + 9.03233*alps) + \
            alps*np.log(mu*mu*z*z)*(0.879984 - 4.67041*alps + 0.702307*alps*np.log(mu*mu*z*z))

    elif n == 10:
        return 1. + alps*(-3.76895 + 11.4119*alps) + \
            alps*np.log(mu*mu*z*z)*(0.961758 - 5.65226*alps + 0.806894*alps*np.log(mu*mu*z*z))

    elif n == 12:
        return 1. + alps*(-4.16979 + 13.7239*alps) + \
            alps*np.log(mu*mu*z*z)*(1.03021 - 6.56687*alps + 0.89959*alps*np.log(mu*mu*z*z))

    elif n == 14:
        return 1. + alps*(-4.5299 + 15.9689*alps) + \
            alps*np.log(mu*mu*z*z)*(1.08911 - 7.42477*alps + 0.983084*alps*np.log(mu*mu*z*z))

    elif n == 16:
        return 1. + alps*(-4.85771 + 18.1499*alps) + \
            alps*np.log(mu*mu*z*z)*(1.14079 - 8.23414*alps + 1.05921*alps*np.log(mu*mu*z*z))

    elif n == 18:
        return 1. + alps*(-5.15917 + 20.2704*alps) + \
            alps*np.log(mu*mu*z*z)*(1.18684 - 9.00145*alps + 1.1293*alps*np.log(mu*mu*z*z))

    elif n == 20:
        return 1. + alps*(-5.43865 + 22.3344*alps) + \
            alps*np.log(mu*mu*z*z)*(1.22837 - 9.73188*alps + 1.19432*alps*np.log(mu*mu*z*z))

    elif n == 22:
        return 1. + alps*(-5.6995 + 24.3456*alps) + \
            alps*np.log(mu*mu*z*z)*(1.26619 - 10.4296*alps + 1.25504*alps*np.log(mu*mu*z*z))

    elif n == 24:
        return 1. + alps*(-5.94431 + 26.3074*alps) + \
            alps*np.log(mu*mu*z*z)*(1.30091 - 11.0982*alps + 1.31203*alps*np.log(mu*mu*z*z))

    elif n == 26:
        return 1. + alps*(-6.17516 + 28.2229*alps) + \
            alps*np.log(mu*mu*z*z)*(1.33299 - 11.7406*alps + 1.36578*alps*np.log(mu*mu*z*z))

    elif n == 28:
        return 1. + alps*(-6.39371 + 30.0951*alps) + \
            alps*np.log(mu*mu*z*z)*(1.36282 - 12.3591*alps + 1.41667*alps*np.log(mu*mu*z*z))

    elif n == 30:
        return 1. + alps*(-6.60136 + 31.9266*alps) + \
            alps*np.log(mu*mu*z*z)*(1.39069 - 12.956*alps + 1.46502*alps*np.log(mu*mu*z*z))


#print(CnNNLO(0.04, 0, 3.2*5.0676896, 0.1938))
#print(CnNNLO(0.04, 0, 3.2*5.0676896, 0.24))
#print(CnNNLO(0.04, 0, 3.2*5.0676896, 0.315))


#####################################################
################    NNLO kernel    ##################
#####################################################
def CnNNNLO(z, n, mu, alps):

    if n == 0:
        return 1. + alps*(0.456712 + 0.802918*alps) + \
            alps*np.log(mu*mu*z*z)*(0.31831  + 0.738604*alps + 0.164647*alps*np.log(mu*mu*z*z))\
                 + alps**3/(8*np.pi**3)*(400 + 1/972*np.log(0.793055*mu*mu*z*z)*(563305.\
                      + 9*(18381 + 364*np.pi**2)*np.log(0.793055*mu*mu*z*z) + 23166*np.log(0.793055*mu*mu*z*z)**2))