#!/usr/bin/env python3
import math
import sys
import numpy as np
from tools import *


if __name__ == "__main__":

    zmax_str_list = ['0.24', '0.36', '0.48', '0.60', '0.72', '0.84', '0.96', '1.20']
    zmax_str_list = ['0.24']
    PD = 1
    PN_list = [2, 3, 4, 5]

    samples_dir = '/Users/Xiang/Desktop/docs/0-2021/analysis/3-qPDF-ratio-DNNITD/DNN-ITD/DNN-andes/HISQa04/results/DNN_ITD_PD1_zmax/ITD/samples'
    samples_format_ITD = 'pdf_44_225_sample#-1-ChisqMin-ITD.dat'

    npick = 200
    low = int((0.16*npick))
    up = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    for izmax in range(0, len(zmax_str_list)):


        zmax_str = zmax_str_list[izmax]
        zmax = float(zmax_str)
        samples_dir = samples_dir.replace('zmax', 'zmax'+zmax_str)
        outfolder = samples_dir.replace('/samples', '')
        mkdir(outfolder)

        print('\n-- Start zmax =',zmax_str,'fm.')
        samples_ITD = []
        for isamp in range(0, npick):

            isamp_name = samples_dir + '/' + samples_format_ITD.replace('#', str(isamp))
            isamp_ITD = np.loadtxt(isamp_name)
            samples_ITD += [isamp_ITD]

        ITD_band0 = []
        ITD_band1 = []
        ITD_band2 = []
        ITD_MeanErr = []
        for iQ in range(0, len(isamp_ITD)):
            if isamp_ITD[iQ][1] == 0:
                break
            iQ_samples_sorted = sorted([samples_ITD[isamp][iQ][1] for isamp in range(0, npick)])
            ITD_band0 += [[isamp_ITD[iQ][0], iQ_samples_sorted[low]]]
            ITD_band1 += [[isamp_ITD[iQ][0], iQ_samples_sorted[mid]]]
            ITD_band2 += [[isamp_ITD[iQ][0], iQ_samples_sorted[up]]]
            ITD_MeanErr += [[isamp_ITD[iQ][0], iQ_samples_sorted[mid], (iQ_samples_sorted[up]-iQ_samples_sorted[low])/2]]
        save_band0 = outfolder + '/' + samples_format_ITD.replace('sample#', 'band0')
        save_band1 = outfolder + '/' + samples_format_ITD.replace('sample#', 'band1')
        save_band2 = outfolder + '/' + samples_format_ITD.replace('sample#', 'band2')
        np.savetxt(save_band0, ITD_band0, fmt='%.6e')
        np.savetxt(save_band1, ITD_band1, fmt='%.6e')
        np.savetxt(save_band2, ITD_band2, fmt='%.6e')
        save_MeanErr = outfolder + '/' + samples_format_ITD.replace('sample#', 'MeanErr')
        np.savetxt(save_MeanErr, ITD_MeanErr, fmt='%.6e')
        print('-- Done zmax =',zmax_str,'fm.\n')


            