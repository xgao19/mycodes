#!/usr/bin/env python3
import numpy as np
import numba as nb
import math
import traceback 
import sys
import inspect
from scipy.optimize import curve_fit
#from statistics import *
from rITD_Numerical_Kernel import *
from scipy.interpolate import interp1d

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "Grid_Kernel" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################## Global parameters ######################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

CF = 4/3
nf = 3
TF = 1/2
CA = 3
PI=3.1415926
pi=3.1415926
fmGeV = 5.0676896
gammaE = 0.5772156649


#@nb.jit
#def alpsmu(mu, alps0 = alps0, mu0=mu0*5.0676896):
#    return alps0/(1+9/(4*PI)*alps0*np.log((mu/mu0)**2))
@nb.jit
def alpsmu(mu, Nf = 3):
    mu /= 5.0676896
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s
def alpsmuNNLO(mu, Nf = 3):
    return alpsmu(mu, Nf = 3)
@nb.jit
def as2pi(alps):
    return alps/(2*pi)

def musat(mu):
    mus = 0.6*fmGeV
    return 2*np.exp(-gammaE)*np.sqrt(mu*mu+mus*mus)
def alpsmuNNLOSat(mu):
    mu = musat(mu)
    return alpsmu(mu)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Auxiliary functions ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
@nb.jit
def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])
@nb.jit
def NLLlambda(n, alps):
    beta0 = (11*CA - 4*nf*TF)/6
    return beta0*as2pi(alps)*np.log(n*np.exp(gammaE))
@nb.jit
def NLLg1(NLLlambda, n, mu, z):
    beta0 = (11*CA - 4*nf*TF)/6
    NLLA0 = 2*CF
    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)
    NLLN = n*np.exp(gammaE)

    part1 = -NLLA0/(2*beta0*NLLlambda)
    part21 = -2*NLLlambda + (1+NLLlambda*(2-L/np.log(NLLN)))*np.log(1+NLLlambda*(2-L/np.log(NLLN)))
    part22 = -(1-L*NLLlambda/np.log(NLLN))*np.log(1-L*NLLlambda/np.log(NLLN))
    return part1 * (part21+part22)
@nb.jit
def NLLg2(NLLlambda, n, mu, z):
    beta0 = (11*CA - 4*nf*TF)/6
    beta1 = (102 - 38*nf/3)/4
    NLLA0 = 2*CF
    NLLA1 = 2*CF*(CA*(67/18 - pi*pi/6) - 10/9*nf*TF)
    NLLB0 = -NLLA0
    L = np.log(z*z*mu*mu*np.exp(2*gammaE)/4)
    NLLN = n*np.exp(gammaE)

    part11 = -NLLA0/beta0 * beta1/(4*beta0*beta0)
    part12 = np.log(1+NLLlambda*(2-L/np.log(NLLN)))*np.log(1+NLLlambda*(2-L/np.log(NLLN))) - np.log(1-L*NLLlambda/np.log(NLLN))*np.log(1-L*NLLlambda/np.log(NLLN))
    part1 = part11 * part12
    part2 = -(NLLA0/beta0-NLLA1/beta1) * beta1/(2*beta0*beta0) * (-2*NLLlambda+np.log(1+NLLlambda*(2-L/np.log(NLLN)))-np.log(1-NLLlambda*L/np.log(NLLN)))
    part3 = -NLLB0/(2*beta0)*(np.log(1+NLLlambda*(2-L/np.log(NLLN)))-np.log(1-NLLlambda*L/np.log(NLLN)))
    return part1 + part2 + part3

@nb.jit
def CnNLL(z,n,mu,alps):
    beta0 = (11*CA - 4*nf*TF)/6
    NLLN = n*np.exp(gammaE)
    NLLlambda = beta0*as2pi(alps)*np.log(NLLN)
    part1 = -np.pi*np.pi/3*as2pi(alps)*CF
    part2 = np.log(NLLN)*NLLg1(NLLlambda, n, mu, z)
    part3 = NLLg2(NLLlambda, n, mu, z)
    return np.exp(part1 + part2 + part3)
@nb.jit
def CnNLLEP1(z,n, mu,alps):
    return 1 - 2/(9*np.pi) * (np.pi*np.pi - 6*np.log(1.7810724*n) + 6*np.log(1.7810724*n)*np.log(1.7810724*n) -\
        6*np.log(1.7810724*n)*np.log(0.7930547*mu*mu*z*z)) * alps
@nb.jit
def CnNLLEP2(z,n, mu,alps):
    part1 = - 2/(9*np.pi) * (np.pi*np.pi - 6*np.log(1.7810724*n) + 6*np.log(1.7810724*n)*np.log(1.7810724*n) -\
        6*np.log(1.7810724*n)*np.log(0.7930547*mu*mu*z*z)) * alps
    part21 = -0.666229e-2 + np.log(n)*(-0.450001+np.log(n)*(0.989373e-1+( 0.272224+0.900633e-1*np.log(n))*np.log(n)))
    part22 = (0.519233e-1+np.log(n)*(-0.125662e0+(-0.477517e0-0.180127e0*np.log(n))*np.log(n)))*np.log(mu*mu*z*z)
    part23 = (0.117733 + (0.255954+0.900633e-1*np.log(n))*np.log(n)) * (np.log(mu*mu*z*z))**2
    part2 = alps*alps*(part21+part22+part23)
    return 1 + part1 + part2
#print(CnNLLEP1(0.7,2),CnNLLEP2(0.7,2))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def CnNLO(z,n, mu,alps):
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))
def CnNLOpol(z,n, mu,alps):
    return 1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)) + alps*CF/(2*pi)*2/(2+3*n+n*n)

def CnNLOevo(z,n, mu, alps=0,k=0):
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n))
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n))*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)
    part1 = 1 + alpsmu(muz0)*CF/(2*pi)*part0
    part2 = (alpsmu(muz0)/alpsmu(mu))**Bn
    return part1*part2
def CnNLOevoSat(z,n, mu,alps=0,k=0):
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n))
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n))*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)
    part1 = 1 + alpsmuNNLOSat(muz0)*CF/(2*pi)*part0
    part2 = (alpsmuNNLOSat(muz0)/alpsmuNNLOSat(mu))**Bn
    return part1*part2
def CnNLOLL(z,n, mu,alps):
    return (1 + alps*CF/(2*pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*np.log(mu*mu*z*z*np.exp(2*gammaE)/4)+(5+2*n)/(2+3*n+n*n)+2*Hn(n)))\
        *np.exp(alps*CF/(2*pi)*(-2*Hn2(n)-2*Hn(n)*Hn(n)))
def CnNLONLL(z,n, mu,alps):
    return CnNLL(z,n,mu,alps) - CnNLLEP1(z,n,mu,alps) + CnNLO(z,n,mu,alps)
def CnNLOevoNLL(z,n, mu,alps=0):
    b1 = 11-2*nf/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n))
    muz0 = np.sqrt(4/(z*z*np.exp(2*gammaE)))
    #print(CnNLONLL(z,n,muz0,alpsmu(muz0)),1/(alpsmu(muz0)/alpsmu(mu)),Bn)
    return CnNLONLL(z,n,muz0,alpsmu(muz0)) * (alpsmu(muz0)/alpsmu(mu))**Bn

def CnC0NLOLL(mu,z,n,k=0, **kwargs):
    return CnNLOLL(z,n, mu, **kwargs)/CnNLOLL(z,0, mu, **kwargs)
def CnC0NLONLL(mu,z,n,k=0, **kwargs):
    return CnNLONLL(z,n, mu, **kwargs)/CnNLO(z,0, mu, **kwargs)
def CnC0NLOevoNLL(mu,z,n,k=0, **kwargs):
    return CnNLOevoNLL(z,n, mu, **kwargs)/CnNLOevo(z,0, mu, **kwargs)
def CnC0NLO_EP(mu,z,n,k=0, **kwargs):
    return 1+CnNLO(z,n, mu, **kwargs)-CnNLO(z,0, mu, **kwargs)
def CnC0NLOevo(mu,z,n,k=0, **kwargs):
    return CnNLOevo(z,n, mu, **kwargs)/CnNLOevo(z,0, mu, **kwargs)
def CnC0NLOevoSat(mu,z,n,k=0, **kwargs):
    return CnNLOevoSat(z,n, mu, **kwargs)/CnNLOevoSat(z,0, mu, **kwargs)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ###################### NNLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
'''
Find the kernels in rITD_Numerical_Kernel.py
'''

def CN2p2Pm(n):
    num_list = [0, 0, -13.8786, 0, -18.7847, 0, -22.0259, 0, -24.5027, 0, -26.5244, 0, -28.239, 0, -29.7306, 0, -31.0521]
    return num_list[n]

def GammaN1(n):
    return CF * ((3+2*n)/(2+3*n+n*n) + 2*Hn(n))
def GammaN2(n):
    gm0f1 = CF*CF * (-5/8 + 2*np.pi*np.pi/3) + CF*CA*(49/24-np.pi*np.pi/6) + CF*nf*TF*(-5/6)
    return -CN2p2Pm(n) + gm0f1
def NNLOevoExpo(n, mu):
    #print(mu,alpsmuNNLO(mu))
    beta0 = (11*CA - 4*nf*TF)/6
    beta1 = (102 - 38*nf/3)/4
    part1 = -(GammaN1(n)*np.log(as2pi(alpsmuNNLOSat(mu)))/beta0)
    part2 = -1/(beta0*beta1) * (-beta1*GammaN1(n) + beta0*GammaN2(n)) * np.log(beta0 + as2pi(alpsmuNNLOSat(mu)) * beta1)
    #print(part1, part2)
    return part1 + part2
def CnNNLOevo(mu,z,n,k=0, **kwargs):
    muz0 = np.sqrt(4/(z*z*np.exp(2*gammaE)))
    return CnNNLO(z, n, muz0, alpsmuNNLO(muz0)) * np.exp(NNLOevoExpo(n, mu)-NNLOevoExpo(n, muz0))
def CnC0NNLOevo(mu,z,n,k=0, **kwargs):
    return CnNNLOevo(mu,z,n, **kwargs)/CnNNLOevo(mu,z,0, **kwargs)
def CnNNLOevoSat(mu,z,n,k=0, **kwargs):
    muz0 = np.sqrt(4/(z*z*np.exp(2*gammaE)))
    return CnNNLO(z, n, muz0, alpsmuNNLOSat(muz0)) * np.exp(NNLOevoExpo(n, mu)-NNLOevoExpo(n, muz0))
def CnC0NNLOevoSat(mu,z,n,k=0, **kwargs):
    return CnNNLOevoSat(mu,z,n, **kwargs)/CnNNLOevoSat(mu,z,0, **kwargs)

def CnNNLONLL(z,n, mu, alps):
    return CnNLL(z,n,mu,alps) - CnNLLEP2(z,n,mu,alps) + CnNNLO(z,n,mu,alps)
def CnNNLONLLevo(mu,z,n,k=0, **kwargs):
    muz0 = np.sqrt(4/(z*z*np.exp(2*gammaE)))
    return CnNNLONLL(z, n, muz0, alpsmuNNLO(muz0)) * np.exp(NNLOevoExpo(n, mu)-NNLOevoExpo(n, muz0))
def CnNNLONLLevoSat(mu,z,n,k=0, **kwargs):
    muz0 = np.sqrt(4/(z*z*np.exp(2*gammaE)))
    return CnNNLONLL(z, n, muz0, alpsmuNNLOSat(muz0)) * np.exp(NNLOevoExpo(n, mu)-NNLOevoExpo(n, muz0))
def CnC0NNLONLLevoSat(mu,z,n,k=0, **kwargs):
    return CnNNLONLLevoSat(mu,z,n, **kwargs)/CnNNLOevoSat(mu,z,0, **kwargs)
def CnC0NNLONLL(mu,z,n,k=0, **kwargs):
    return CnNNLONLL(mu,z,n)/CnNNLO(z, 0, mu, alpsmu(mu))



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ########################## Neat kernels ############################ ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def CnC0NLO(mu,z,n):
    return CnNLO(z,n, mu, alpsmu(mu))/CnNLO(z,0, mu, alpsmu(mu))
def CnC0NLOpol(mu,z,n):
    return CnNLOpol(z,n, mu, alpsmu(mu))/CnNLOpol(z,0, mu, alpsmu(mu))
def CnC0NNLO(mu,z,n,k=0, **kwargs):
    return CnNNLO(z, n, mu, alpsmu(mu))/CnNNLO(z, 0, mu, alpsmu(mu))
def CnC0NNLO_EP(mu,z,n,k=0, **kwargs):
    return CnC0NNLO_EP_numerical(z, n, mu, alpsmu(mu))