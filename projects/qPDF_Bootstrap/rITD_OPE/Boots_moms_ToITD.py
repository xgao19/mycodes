#!/usr/bin/env python3
import numpy as np
import sys
from tools import *


def ITD_moms(x, moms):
    ITD = 1
    for i in range(0, len(moms)):
        n = 2 * (i+1)
        ITD += (-1)**(i+1) * moms[i] / np.math.factorial(n) * x**n
    return ITD


if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    line2nd = commands[1].split()
    npick = int(line2nd[1])
    print('-- Number of samples:', line2nd[1],'.')
    Lmax = float(line2nd[3])
    print('-- Lambda reaches:', line2nd[3],'.')

    line3rd = commands[2].split()
    row = int(line3rd[1])
    col_list = [int(line3rd[i]) for i in range(3, len(line3rd))]
    dataset = []
    for isamp in range(0, npick):
        dataset += [[np.loadtxt(line3rd[2].replace('#', str(isamp)))[row][col] for col in col_list]]
        #print(dataset[isamp])
    print('-- Read data with shape:', np.shape(dataset))

    x = np.arange(0, Lmax+0.01, 0.01)
    ITD_samples = []
    for isamp in range(0, npick):
        ITD_samples += [ITD_moms(x, dataset[isamp])]
    
    up = int((npick*0.84))
    mid = int((npick*0.5))
    low = int((npick*0.16))
    ITD_0 = []
    ITD_1 = []
    ITD_2 = []
    for ix in range(0, len(x)):
        ix_samples = sorted([ITD_samples[isamp][ix] for isamp in range(0, npick)])
        ITD_0 += [[x[ix], ix_samples[low]]]
        ITD_1 += [[x[ix], ix_samples[mid]]]
        ITD_2 += [[x[ix], ix_samples[up]]]
    savename = outfolder + '/' + describe + '_band0.dat'
    np.savetxt(savename, ITD_0, fmt='%.6e')
    savename = outfolder + '/' + describe + '_band1.dat'
    np.savetxt(savename, ITD_1, fmt='%.6e')
    savename = outfolder + '/' + describe + '_band2.dat'
    np.savetxt(savename, ITD_2, fmt='%.6e')