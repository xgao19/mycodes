#!/usr/bin/env python3
import numpy as np
import sys
from tools import *
from scipy import special
from scipy import integrate

def pdf_collection(filename, npick, col):
    
    samples = []
    for isamp in range(0, npick):
        read_data = np.loadtxt(filename.replace('#',str(isamp)))
        isamp_data = []
        xlist = []
        for ix in range(0, len(read_data)):
            xlist += [read_data[ix][col[0]]]
            isamp_data +=  [read_data[ix][col[1]]]
        samples += [isamp_data]
    return xlist, samples


if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/band/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    pdf_dataset = []
    popt_dataset = []

    for index in range(1, len(commands)):

        line = commands[index].split()
        if 'npick' == line[0]:
            npick = int(line[1])
            print('npick:', npick)
        elif 'zmax' == line[0]:
            zmax_str = [line[i] for i in range(1, len(line))]
            zmax_list = [float(line[i]) for i in range(1, len(line))]
            print('zmax list:', zmax_list)
        elif 'lambdaL' == line[0]:
            lambdaL_str = [line[i] for i in range(1, len(line))]
            lambdaL_list = [float(line[i]) for i in range(1, len(line))]
            print('lambdaL list:', lambdaL_list)
        elif 'data' == line[0]:
            fileformat = line[1]
            col = [int(line[2]), int(line[3])]
            for iz in range(0, len(zmax_str)):
                for iL in range(0, len(lambdaL_str)):
                    filename = fileformat.replace('zmax','zmax'+zmax_str[iz].replace('.','')).replace('*', lambdaL_str[iL])
                    print('Reading file:', filename.split('/')[-1],' zmax:', zmax_str[iz], 'fm.')
                    xlist, samples = pdf_collection(filename, npick, col)
                    pdf_dataset += [samples]
    print('PDF data shape:',np.shape(pdf_dataset))

    ######## PDF-x
    data_syserr = []
    for ix in range(0, len(xlist)):
        ix_data_syserr = []
        for isamp in range(0, len(pdf_dataset[0])):
            isamp_data = [pdf_dataset[i][isamp][ix] for i in range(0, len(pdf_dataset))]
            isamp_data_syserr = [np.average(isamp_data),np.std(isamp_data)]
            ix_data_syserr += [isamp_data_syserr]
        data_syserr += [ix_data_syserr]
    print('Averaged data shape:',np.shape(data_syserr))

    up = int((len(data_syserr[0])*0.84))
    mid = int((len(data_syserr[0])*0.5))
    down = int((len(data_syserr[0])*0.16))
    pdf0 = []
    pdf0sys = []
    pdf1 = []
    pdf1sys = []
    pdf2 = []
    pdf2sys = []
    for ix in range(0, len(xlist)):
        meanlist = sorted([data_syserr[ix][isamp][0] for isamp in range(0, len(data_syserr[ix]))])
        SysErr = sorted([data_syserr[ix][isamp][1] for isamp in range(0, len(data_syserr[ix]))])[mid]
        pdf0 += [[xlist[ix], meanlist[down], xlist[ix]*meanlist[down]]]
        pdf1 += [[xlist[ix], meanlist[mid], xlist[ix]*meanlist[mid]]]
        pdf2 += [[xlist[ix], meanlist[up], xlist[ix]*meanlist[up]]]
        pdf0sys += [[xlist[ix], meanlist[mid]-np.sqrt(SysErr**2+(meanlist[mid]-meanlist[down])**2), xlist[ix]*(meanlist[mid]-np.sqrt(SysErr**2+(meanlist[mid]-meanlist[down])**2))]]
        pdf1sys += [[xlist[ix], meanlist[mid], xlist[ix]*meanlist[mid]]]
        pdf2sys += [[xlist[ix], meanlist[mid]+np.sqrt(SysErr**2+(meanlist[up]-meanlist[mid])**2),xlist[ix]*(meanlist[mid]+np.sqrt(SysErr**2+(meanlist[up]-meanlist[mid])**2))]]
    savename = outfolder + describe + '_pdfx.txt'
    np.savetxt(savename.replace('pdfx','pdf0'), pdf0)
    np.savetxt(savename.replace('pdfx','pdf0sys'), pdf0sys)
    np.savetxt(savename.replace('pdfx','pdf1'), pdf1)
    np.savetxt(savename.replace('pdfx','pdf1sys'), pdf1sys)
    np.savetxt(savename.replace('pdfx','pdf2'), pdf2)
    np.savetxt(savename.replace('pdfx','pdf2sys'), pdf2sys)