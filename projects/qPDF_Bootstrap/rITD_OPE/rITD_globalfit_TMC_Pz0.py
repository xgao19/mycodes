#!/usr/bin/env python3
import numpy as np
from statistics import *
from rITD_pion_ritdReal import *
from tools import *

np.random.seed(2022)

def PDF_gf_isoV_N(PDF_format, Nsamp):

    PDF_samples = []
    for isamp in range(0, Nsamp):

        isamp_format = PDF_format.replace('#', str(isamp))
        PDF_u = np.loadtxt(isamp_format.replace('*', str(2)))
        PDF_d = np.loadtxt(isamp_format.replace('*', str(1)))
        PDF_ubar = np.loadtxt(isamp_format.replace('*', str(-2)))
        PDF_dbar = np.loadtxt(isamp_format.replace('*', str(-1)))

        isamp_PDF = []
        for ix in range(0, len(PDF_u)):
            row = [-PDF_u[len(PDF_u)-ix-1][0], (PDF_dbar[len(PDF_u)-ix-1][2] - PDF_ubar[len(PDF_u)-ix-1][2])/PDF_u[len(PDF_u)-ix-1][0]]
            isamp_PDF += [row]
        for ix in range(0, len(PDF_u)):
            row = [PDF_u[ix][0], (PDF_u[ix][2] - PDF_d[ix][2])/PDF_u[ix][0]]
            isamp_PDF += [row]

        PDFsum = 0
        for ix in range(1, len(isamp_PDF)):
            PDFsum += isamp_PDF[ix][1] * (isamp_PDF[ix][0]-isamp_PDF[ix-1][0])

        PDF_samples += [isamp_PDF]

    return PDF_samples

def PDF_gf_isoV_pi(PDF_format, Nsamp):

    PDF_samples = []
    for isamp in range(0, Nsamp):

        isamp_format = PDF_format.replace('#', str(isamp))
        PDF_u = np.loadtxt(isamp_format.replace('*', str(2)))
        PDF_d = np.loadtxt(isamp_format.replace('*', str(1)))

        isamp_PDF = []
        for ix in range(0, len(PDF_u)):
            row = [PDF_u[ix][0], -(PDF_u[ix][2] - PDF_d[ix][2])/PDF_u[ix][0]]
            isamp_PDF += [row]

        PDFsum = 0
        for ix in range(1, len(isamp_PDF)):
            PDFsum += isamp_PDF[ix][1] * (isamp_PDF[ix][0]-isamp_PDF[ix-1][0])

        PDF_samples += [isamp_PDF]

    return PDF_samples


def PDF_gf_moments(PDF_samples, Nmoms=20):

    moms_samples = []
    for isamp in range(0, len(PDF_samples)):

        isamp_PDF = PDF_samples[isamp]
        isamp_moms = []
        for imom in range(0, Nmoms+1):

            PDFsum = 0
            for ix in range(1, len(isamp_PDF)):
                PDFsum += (isamp_PDF[ix][1]+isamp_PDF[ix-1][1])/2 * (isamp_PDF[ix][0]-isamp_PDF[ix-1][0]) * ((isamp_PDF[ix][0]+isamp_PDF[ix-1][0])/2)**imom
            isamp_moms += [PDFsum]

        moms_samples += [isamp_moms]

    low = int((0.16*len(PDF_samples)))
    up = int(np.ceil(0.84*len(PDF_samples)))
    mid = int(0.5*len(PDF_samples))
    moms_MeanErr = []
    for imom in range(0, Nmoms+1):
        imom_samples = sorted([moms_samples[isamp][imom] for isamp in range(0, len(PDF_samples))])
        moms_MeanErr += [[imom, imom_samples[mid], (imom_samples[up]-imom_samples[low])/2]]

    return moms_MeanErr, moms_samples

if __name__ == "__main__":

    PDF_N_format = '/Users/Xiang/Desktop/0study/research/software/read_data/mu2p0GeV/NNPDF40_nnlo_as_01180/NNPDF40_nnlo_as_01180_#_*.dat'
    PDF_pi_format = '/Users/Xiang/Desktop/0study/research/software/read_data/mu2p0GeV/JAM21PionPDFnlo/JAM21PionPDFnlo_#_*.dat'
    save_format = '/Users/Xiang/Desktop/docs/0-2022/analysis/0-NpiRatio/gf_rITD_PN0/NNPDF40nnlo_JAM21nlo_2p0GeV_Order'

    Nsamp_N = 101
    Nsamp_pi = 786
    Npick = max(Nsamp_pi, Nsamp_N)

    Nmoms = 30

    PDF_N_samples = PDF_gf_isoV_N(PDF_N_format, Nsamp_N)
    PDF_pi_samples = PDF_gf_isoV_pi(PDF_pi_format, Nsamp_pi)

    moms_N_MearErr, moms_N_samples = PDF_gf_moments(PDF_N_samples, Nmoms)
    moms_pi_MearErr, moms_pi_samples = PDF_gf_moments(PDF_pi_samples, Nmoms)

    print(moms_N_MearErr,'\n',moms_pi_MearErr)

    fmGeV = 5.0676896
    M_N = 0.94
    M_pi = 0.14
    mu = 2*fmGeV

    zlist = np.arange(0.01, 2, 0.01)
    kernel_LO = []
    kernel_NLO = []
    kernel_NNLO = []
    for iz in range(0, len(zlist)):
        z = zlist[iz]
        kernel_LO += [[1 for n in range(0, Nmoms+1)]]
        kernel_NLO += [[CnC0NLO(mu, z, n) for n in range(0, Nmoms+1)]]
        kernel_NNLO += [[CnC0NNLO(mu, z, n) for n in range(0, Nmoms+1)]]

    # LO
    TMC_N_samples = []
    TMC_pi_samples = []
    TMC_NpiRatio_samples = []
    for isamp in range(0, Npick):

        isamp_TMC_N = [[0,1]]
        isamp_TMC_pi = [[0,1]]
        isamp_TMC_NpiRatio = [[0,1]]
        for iz in range(0, len(zlist)):
            z = zlist[iz]
            mass = M_N*fmGeV
            TMC_N = 1
            for i in range(1, int(Nmoms/2)+1):
                n = 2*i
                TMC_N += kernel_LO[iz][n] * ( (0.5*z*mass)**n )/np.math.factorial(n) * moms_N_samples[isamp%len(moms_N_samples)][n]
            isamp_TMC_N += [[z, TMC_N]]

            mass = M_pi*fmGeV
            TMC_pi = 1
            for i in range(1, int(Nmoms/2)+1):
                n = 2*i
                TMC_pi += kernel_LO[iz][n] * ( (0.5*z*mass)**n )/np.math.factorial(n) * moms_pi_samples[isamp%len(moms_pi_samples)][n]
            isamp_TMC_pi += [[z, TMC_pi]]

            isamp_TMC_NpiRatio += [[z, TMC_N/TMC_pi]]

        TMC_N_samples += [isamp_TMC_N]
        TMC_pi_samples += [isamp_TMC_pi]
        TMC_NpiRatio_samples += [isamp_TMC_NpiRatio]
    low = int((0.16*Npick))
    up = int(np.ceil(0.84*Npick))
    mid = int(0.5*Npick)
    TMC_NpiRatio_band0 = []
    TMC_NpiRatio_band1 = []
    TMC_NpiRatio_band2 = []
    for iz in range(0, len(TMC_NpiRatio_samples[0])):
        samples = [TMC_NpiRatio_samples[isamp][iz][1] for isamp in range(0, Npick)]
        TMC_NpiRatio_band0 += [[TMC_NpiRatio_samples[0][iz][0], samples[low]]]
        TMC_NpiRatio_band1 += [[TMC_NpiRatio_samples[0][iz][0], samples[mid]]]
        TMC_NpiRatio_band2 += [[TMC_NpiRatio_samples[0][iz][0], samples[up]]]
    np.savetxt(save_format.replace('Order','LO')+'_band0.dat', TMC_NpiRatio_band0)
    np.savetxt(save_format.replace('Order','LO')+'_band1.dat', TMC_NpiRatio_band1)
    np.savetxt(save_format.replace('Order','LO')+'_band2.dat', TMC_NpiRatio_band2)

    # NLO
    TMC_N_samples = []
    TMC_pi_samples = []
    TMC_NpiRatio_samples = []
    for isamp in range(0, Npick):

        isamp_TMC_N = [[0,1]]
        isamp_TMC_pi = [[0,1]]
        isamp_TMC_NpiRatio = [[0,1]]
        for iz in range(0, len(zlist)):
            z = zlist[iz]
            mass = M_N*fmGeV
            TMC_N = 1
            for i in range(1, int(Nmoms/2)+1):
                n = 2*i
                TMC_N += kernel_NLO[iz][n] * ( (0.5*z*mass)**n )/np.math.factorial(n) * moms_N_samples[isamp%len(moms_N_samples)][n]
            isamp_TMC_N += [[z, TMC_N]]

            mass = M_pi*fmGeV
            TMC_pi = 1
            for i in range(1, int(Nmoms/2)+1):
                n = 2*i
                TMC_pi += kernel_NLO[iz][n] * ( (0.5*z*mass)**n )/np.math.factorial(n) * moms_pi_samples[isamp%len(moms_pi_samples)][n]
            isamp_TMC_pi += [[z, TMC_pi]]

            isamp_TMC_NpiRatio += [[z, TMC_N/TMC_pi]]

        TMC_N_samples += [isamp_TMC_N]
        TMC_pi_samples += [isamp_TMC_pi]
        TMC_NpiRatio_samples += [isamp_TMC_NpiRatio]
    low = int((0.16*Npick))
    up = int(np.ceil(0.84*Npick))
    mid = int(0.5*Npick)
    TMC_NpiRatio_band0 = []
    TMC_NpiRatio_band1 = []
    TMC_NpiRatio_band2 = []
    for iz in range(0, len(TMC_NpiRatio_samples[0])):
        samples = [TMC_NpiRatio_samples[isamp][iz][1] for isamp in range(0, Npick)]
        TMC_NpiRatio_band0 += [[TMC_NpiRatio_samples[0][iz][0], samples[low]]]
        TMC_NpiRatio_band1 += [[TMC_NpiRatio_samples[0][iz][0], samples[mid]]]
        TMC_NpiRatio_band2 += [[TMC_NpiRatio_samples[0][iz][0], samples[up]]]
    np.savetxt(save_format.replace('Order','NLO')+'_band0.dat', TMC_NpiRatio_band0)
    np.savetxt(save_format.replace('Order','NLO')+'_band1.dat', TMC_NpiRatio_band1)
    np.savetxt(save_format.replace('Order','NLO')+'_band2.dat', TMC_NpiRatio_band2)

    # NNLO
    TMC_N_samples = []
    TMC_pi_samples = []
    TMC_NpiRatio_samples = []
    for isamp in range(0, Npick):

        isamp_TMC_N = [[0,1]]
        isamp_TMC_pi = [[0,1]]
        isamp_TMC_NpiRatio = [[0,1]]
        for iz in range(0, len(zlist)):
            z = zlist[iz]
            mass = M_N*fmGeV
            TMC_N = 1
            for i in range(1, int(Nmoms/2)+1):
                n = 2*i
                TMC_N += kernel_NNLO[iz][n] * ( (0.5*z*mass)**n )/np.math.factorial(n) *  moms_N_samples[isamp%len(moms_N_samples)][n]
            isamp_TMC_N += [[z, TMC_N]]

            mass = M_pi*fmGeV
            TMC_pi = 1
            for i in range(1, int(Nmoms/2)+1):
                n = 2*i
                TMC_pi += kernel_NNLO[iz][n] * ( (0.5*z*mass)**n )/np.math.factorial(n) *  moms_pi_samples[isamp%len(moms_pi_samples)][n]
            isamp_TMC_pi += [[z, TMC_pi]]

            isamp_TMC_NpiRatio += [[z, TMC_N/TMC_pi]]

        TMC_N_samples += [isamp_TMC_N]
        TMC_pi_samples += [isamp_TMC_pi]
        TMC_NpiRatio_samples += [isamp_TMC_NpiRatio]
    low = int((0.16*Npick))
    up = int(np.ceil(0.84*Npick))
    mid = int(0.5*Npick)
    TMC_NpiRatio_band0 = []
    TMC_NpiRatio_band1 = []
    TMC_NpiRatio_band2 = []
    for iz in range(0, len(TMC_NpiRatio_samples[0])):
        samples = [TMC_NpiRatio_samples[isamp][iz][1] for isamp in range(0, Npick)]
        TMC_NpiRatio_band0 += [[TMC_NpiRatio_samples[0][iz][0], samples[low]]]
        TMC_NpiRatio_band1 += [[TMC_NpiRatio_samples[0][iz][0], samples[mid]]]
        TMC_NpiRatio_band2 += [[TMC_NpiRatio_samples[0][iz][0], samples[up]]]
    np.savetxt(save_format.replace('Order','NNLO')+'_band0.dat', TMC_NpiRatio_band0)
    np.savetxt(save_format.replace('Order','NNLO')+'_band1.dat', TMC_NpiRatio_band1)
    np.savetxt(save_format.replace('Order','NNLO')+'_band2.dat', TMC_NpiRatio_band2)

    