#!/usr/bin/env python3
import numpy as np
import sys
from tools import *
from scipy import special
from scipy import integrate

def pdfs_model_ab(x, n, *ab):
    #print(ab)
    return (x**n)*special.gamma(2+ab[0]+ab[1])/(special.gamma(1+ab[0])*special.gamma(1+ab[1]))*(x**ab[0])*((1-x)**ab[1])
def pdfs_model_abst(x, n, *abst):
    A = special.gamma(1+abst[1])*(abst[2]*special.gamma(3/2+abst[0])/special.gamma(5/2+abst[0]+abst[1])+\
        (2+abst[0]+abst[1]+abst[3]+abst[0]*abst[3])*special.gamma(1+abst[0])/special.gamma(3+abst[0]+abst[1]))
    return 1/A*(x**n)*(x**abst[0])*((1-x)**abst[1])*(1+abst[2]*np.sqrt(x)+abst[3]*x)
def pdfs_model(x, n, *popt):
    if len(popt) == 2:
        return pdfs_model_ab(x, n, *popt)
    elif len(popt) == 4:
        return pdfs_model_abst(x, n, *popt)

def pdf_collection(filename, npick, row, col, xlist, zpzlist):
    
    dataset_pdf = []
    dataset_ITD = []
    for isamp in range(0, npick):
        read_data = np.loadtxt(filename.replace('#',str(isamp)))[row]

        isamp_data_pdf = []
        isamp_data_ITD = []
        for x in xlist:
            popt = [read_data[col[i]] for i in range(0, len(col))]
            isamp_data_pdf +=  [pdfs_model(x, 0, *popt)]
        for zpz in zpzlist:
            ITD = integrate.quad(lambda x: pdfs_model(x, 0, *popt)*np.cos(x*zpz), 0, 1)[0]
            isamp_data_ITD += [ITD]

        dataset_pdf += [isamp_data_pdf]
        dataset_ITD += [isamp_data_ITD]

    print('Read zmax:',read_data[1],'fm')
    return dataset_pdf, dataset_ITD

def data_collection(filename, npick, row, col):
    
    dataset = []
    for isamp in range(0, npick):
        read_data = np.loadtxt(filename.replace('#',str(isamp)))[row]
        isamp_data = [read_data[col[i]] for i in range(0, len(col))]
        popt = [read_data[col[i]] for i in range(0, len(col))]
        for n in range(1,7):
            isamp_data +=  [integrate.quad(lambda x: pdfs_model(x, n, *popt), 0, 1)[0]]
        isamp_data += [read_data[-3]]
        dataset += [isamp_data]
    return dataset



if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/band/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    line2nd = commands[1].split()
    xmin = float(line2nd[1])
    xmax = float(line2nd[2])
    xmv = float(line2nd[3])
    xlist = [float(line2nd[1])+i*float(line2nd[3]) for i in range(1, int((xmax-xmin)/xmv)+1)]
    print('PDF x in range: [', xlist[0],',', xlist[-1],'], with interval', xmv)
    zpzlist = np.arange(0, 20+0.2, 0.2)
    print('ITD zpz in range: [', zpzlist[0],',', zpzlist[-1],'], with interval', zpzlist[1]-zpzlist[0])

    pdf_dataset = []
    ITD_dataset = []
    popt_dataset = []

    for index in range(2, len(commands)):

        line = commands[index].split()
        if 'npick' == line[0]:
            npick = int(line[1])
        elif 'data' == line[0]:
            row = int(line[1])
            filename = line[2]
            col = [int(line[i]) for i in range(3, len(line))]
            print('Reading file:', filename.split('/')[-1], ', col:', col)
            iPDF, iITD = pdf_collection(filename, npick, row, col, xlist, zpzlist)
            pdf_dataset += [iPDF]
            ITD_dataset += [iITD]
            popt_dataset += [data_collection(filename, npick, row, col)]
    print('PDF data shape:',np.shape(pdf_dataset))
    print('ITD data shape:',np.shape(ITD_dataset))
    print('popt data shape:', np.shape(popt_dataset))

    ######## PDF-x
    data_syserr = []
    for ix in range(0, len(xlist)):
        ix_data_syserr = []
        for isamp in range(0, len(pdf_dataset[0])):
            isamp_data = [pdf_dataset[i][isamp][ix] for i in range(0, len(pdf_dataset))]
            isamp_data_syserr = [np.average(isamp_data),np.std(isamp_data)]
            ix_data_syserr += [isamp_data_syserr]
        data_syserr += [ix_data_syserr]
    #print(np.shape(data_syserr))

    up = int((len(data_syserr[0])*0.84))
    mid = int((len(data_syserr[0])*0.5))
    down = int((len(data_syserr[0])*0.16))
    pdf0 = []
    pdf0sys = []
    pdf1 = []
    pdf1sys = []
    pdf2 = []
    pdf2sys = []
    for ix in range(0, len(xlist)):
        meanlist = sorted([data_syserr[ix][isamp][0] for isamp in range(0, len(data_syserr[ix]))])
        SysErr = sorted([data_syserr[ix][isamp][1] for isamp in range(0, len(data_syserr[ix]))])[mid]
        pdf0 += [[xlist[ix], meanlist[down], xlist[ix]*meanlist[down]]]
        pdf1 += [[xlist[ix], meanlist[mid], xlist[ix]*meanlist[mid]]]
        pdf2 += [[xlist[ix], meanlist[up], xlist[ix]*meanlist[up]]]
        #pdf0sys += [[xlist[ix], meanlist[mid]-np.sqrt(SysErr**2+(meanlist[mid]-meanlist[down])**2), xlist[ix]*(meanlist[mid]-np.sqrt(SysErr**2+(meanlist[mid]-meanlist[down])**2))]]
        #pdf1sys += [[xlist[ix], meanlist[mid], xlist[ix]*meanlist[mid]]]
        #pdf2sys += [[xlist[ix], meanlist[mid]+np.sqrt(SysErr**2+(meanlist[up]-meanlist[mid])**2),xlist[ix]*(meanlist[mid]+np.sqrt(SysErr**2+(meanlist[up]-meanlist[mid])**2))]]
        pdf0sys += [[xlist[ix], meanlist[down]-SysErr, xlist[ix]*(meanlist[down]-SysErr)]]
        pdf1sys += [[xlist[ix], meanlist[mid], xlist[ix]*meanlist[mid]]]
        pdf2sys += [[xlist[ix], meanlist[up]+SysErr,xlist[ix]*(meanlist[up]+SysErr)]]
    savename = outfolder + describe + '_pdfx.txt'
    np.savetxt(savename.replace('pdfx','pdf0'), pdf0)
    np.savetxt(savename.replace('pdfx','pdf0sys'), pdf0sys)
    np.savetxt(savename.replace('pdfx','pdf1'), pdf1)
    np.savetxt(savename.replace('pdfx','pdf1sys'), pdf1sys)
    np.savetxt(savename.replace('pdfx','pdf2'), pdf2)
    np.savetxt(savename.replace('pdfx','pdf2sys'), pdf2sys)


    ######## ITD-zpz
    data_syserr = []
    for izpz in range(0, len(zpzlist)):
        izpz_data_syserr = []
        for isamp in range(0, len(ITD_dataset[0])):
            isamp_data = [ITD_dataset[i][isamp][izpz] for i in range(0, len(ITD_dataset))]
            isamp_data_syserr = [np.average(isamp_data),np.std(isamp_data)]
            izpz_data_syserr += [isamp_data_syserr]
        data_syserr += [izpz_data_syserr]
    #print(np.shape(data_syserr))

    up = int((len(data_syserr[0])*0.84))
    mid = int((len(data_syserr[0])*0.5))
    down = int((len(data_syserr[0])*0.16))
    ITD0 = []
    ITD0sys = []
    ITD1 = []
    ITD1sys = []
    ITD2 = []
    ITD2sys = []
    for izpz in range(0, len(zpzlist)):
        meanlist = sorted([data_syserr[izpz][isamp][0] for isamp in range(0, len(data_syserr[izpz]))])
        SysErr = sorted([data_syserr[izpz][isamp][1] for isamp in range(0, len(data_syserr[izpz]))])[mid]
        ITD0 += [[zpzlist[izpz], meanlist[down]]]
        ITD1 += [[zpzlist[izpz], meanlist[mid]]]
        ITD2 += [[zpzlist[izpz], meanlist[up]]]
        ITD0sys += [[zpzlist[izpz], meanlist[down]-SysErr]]
        ITD1sys += [[zpzlist[izpz], meanlist[mid]]]
        ITD2sys += [[zpzlist[izpz], meanlist[up]+SysErr]]
    savename = outfolder + describe + '_ITD.txt'
    np.savetxt(savename.replace('_ITD.txt','_ITD0.txt'), ITD0)
    np.savetxt(savename.replace('_ITD.txt','_ITD0sys.txt'), ITD0sys)
    np.savetxt(savename.replace('_ITD.txt','_ITD1.txt'), ITD1)
    np.savetxt(savename.replace('_ITD.txt','_ITD1sys.txt'), ITD1sys)
    np.savetxt(savename.replace('_ITD.txt','_ITD2.txt'), ITD2)
    np.savetxt(savename.replace('_ITD.txt','_ITD2sys.txt'), ITD2sys)


    ######## PDF-popt
    data_syserr = []
    for ip in range(0, len(popt_dataset[0][0])):
        ip_data_syserr = []
        for isamp in range(0, len(popt_dataset[0])):
            isamp_data = [popt_dataset[i][isamp][ip] for i in range(0, len(popt_dataset))]
            isamp_data_syserr = [np.average(isamp_data),np.std(isamp_data)]
            ip_data_syserr += [isamp_data_syserr]
        data_syserr += [ip_data_syserr]
        #print(data_syserr[ip])
    #print(np.shape(data_syserr))
    popt = []
    for ip in range(0, len(data_syserr)):
        meanlist = sorted([data_syserr[ip][isamp][0] for isamp in range(0, len(data_syserr[0]))])
        #print(len(data_syserr[ip]),len(meanlist),'\n')
        #print(len(meanlist))
        SysErr = sorted([data_syserr[ip][isamp][1] for isamp in range(0, len(data_syserr[ip]))])[mid]
        ipopt_stderr = np.std(meanlist)*np.sqrt(len(meanlist)/(len(meanlist)-1))
        ip_popt = [meanlist[mid], (meanlist[up]-meanlist[down])/2]
        #ip_popt += [np.sqrt(((meanlist[up]-meanlist[down])/2)**2+SysErr**2), SysErr]
        ip_popt += [(meanlist[up]-meanlist[down])/2+SysErr, SysErr]
        popt += [ip_popt]
        #print(ip_popt)
    savename = outfolder + describe + '_popt.txt'
    np.savetxt(savename, popt)
    
