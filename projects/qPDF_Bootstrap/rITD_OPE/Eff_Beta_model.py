#!/usr/bin/env python3
import numpy as np

pdf_PD1_format = '/Users/Xiang/Desktop/docs/0-2021/analysis/3-qPDF-ratio/DNN/Fit/results/PD1_ab/pdf/pdf_44_225_sample#-1.dat'

save_format = '/Users/Xiang/Desktop/docs/0-2021/analysis/3-qPDF-ratio/Moments/joint/a2/Sys_Effbeta/joint_PD_rITDmomsritdNNLO_2GeV_model_ab'

npick = 200
up = int(npick*0.84)
mid = int(npick*0.5)
down = int(npick*0.16)

Nmax = 130
Nstep = 1

moms_samples = np.zeros((Nmax+1, npick))
for isamp in range(0, npick):
    pdf_sample = list(np.loadtxt(pdf_PD1_format.replace('#',str(isamp))))
    pdf_sample = [[1,0]] + pdf_sample
    #print(pdf_sample)
    for ix in range(0, len(pdf_sample)-1):
        fx = (pdf_sample[ix+1][1] + pdf_sample[ix][1])
        dx = -(pdf_sample[ix+1][0] - pdf_sample[ix][0])
        x = (pdf_sample[ix+1][0] + pdf_sample[ix][0])/2
        for iN in range(0, Nmax+1):
            moms_samples[iN][isamp] += dx*fx*x**iN
print('Collected moments:',np.shape(moms_samples))

moms_MeanErr = np.zeros((Nmax+1, 3))
moms_0 = np.zeros((Nmax+1, 2))
moms_1 = np.zeros((Nmax+1, 2))
moms_2 = np.zeros((Nmax+1, 2))
for n in range(0, Nmax+1):

    n_moms_samples_sorted = sorted(moms_samples[n])
    moms_MeanErr[n][0] = n
    moms_MeanErr[n][1] = (n_moms_samples_sorted[up]+n_moms_samples_sorted[down])/2
    moms_MeanErr[n][2] = (n_moms_samples_sorted[up]-n_moms_samples_sorted[down])/2

    moms_0[n][0] = n
    moms_0[n][1] = n_moms_samples_sorted[down]
    moms_1[n][0] = n
    moms_1[n][1] = n_moms_samples_sorted[mid]
    moms_2[n][0] = n
    moms_2[n][1] = n_moms_samples_sorted[up]
print(moms_MeanErr[0],moms_MeanErr[-1])
np.savetxt(save_format+'_moms_popt', moms_MeanErr, fmt='%.6e')
np.savetxt(save_format+'_moms_0', moms_0, fmt='%.6e')
np.savetxt(save_format+'_moms_1', moms_1, fmt='%.6e')
np.savetxt(save_format+'_moms_2', moms_2, fmt='%.6e')






Effbeta_samples = np.zeros((Nmax+1-4, npick))
for isamp in range(0, npick):
    for n in range(2, Nmax+1-2):
        Effbeta_samples[n-2][isamp] = -1 + n/4*(moms_samples[n-2][isamp]-moms_samples[n+2][isamp])/moms_samples[n][isamp]

Effbeta_MeanErr = np.zeros((Nmax+1-4, 3))
Effbeta_0 = np.zeros((Nmax+1-4, 2))
Effbeta_1 = np.zeros((Nmax+1-4, 2))
Effbeta_2 = np.zeros((Nmax+1-4, 2))
for n in range(2, Nmax+1-2):

    n_Effbeta_samples_sorted = sorted(Effbeta_samples[n-2])
    Effbeta_MeanErr[n-2][0] = n
    Effbeta_MeanErr[n-2][1] = (n_Effbeta_samples_sorted[up]+n_Effbeta_samples_sorted[down])/2
    Effbeta_MeanErr[n-2][2] = (n_Effbeta_samples_sorted[up]-n_Effbeta_samples_sorted[down])/2

    Effbeta_0[n-2][0] = n
    Effbeta_0[n-2][1] = n_Effbeta_samples_sorted[down]
    Effbeta_1[n-2][0] = n
    Effbeta_1[n-2][1] = n_Effbeta_samples_sorted[mid]
    Effbeta_2[n-2][0] = n
    Effbeta_2[n-2][1] = n_Effbeta_samples_sorted[up]
print(Effbeta_MeanErr[0],Effbeta_MeanErr[-1])
np.savetxt(save_format+'_Effbeta_popt', Effbeta_MeanErr, fmt='%.6e')
np.savetxt(save_format+'_Effbeta_0', Effbeta_0, fmt='%.6e')
np.savetxt(save_format+'_Effbeta_1', Effbeta_1, fmt='%.6e')
np.savetxt(save_format+'_Effbeta_2', Effbeta_2, fmt='%.6e')