#!/usr/bin/env python3
import numpy as np
import math
import sys
from scipy.interpolate import interp1d
from tools import *

def LORGI(sample_up, col_up, sample_down, col_down):

    up_x = [sample_up[i][col_up[0]] for i in range(0, len(sample_up))]
    up_y = [sample_up[i][col_up[1]] for i in range(0, len(sample_up))]
    up_fx = interp1d(up_x, up_y)

    down_x = [sample_down[i][col_down[0]] for i in range(0, len(sample_down))]
    down_y = [sample_down[i][col_down[1]] for i in range(0, len(sample_down))]
    down_fx = interp1d(down_x, down_y)

    xmin = max([up_x[0],down_x[0]])
    xmax = min([up_x[-1],down_x[-1]])
    #print(xmin,0.221584/xmin,0.221584/xmin/2)

    dx = 0.002
    xlist = np.arange(xmin, xmax+dx/3, dx)
    fxlist = [[xlist[i], up_fx(xlist[i])/down_fx(xlist[i])] for i in range(0, len(xlist))]
    #print(fxlist)

    return fxlist



if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    sample_up_format_list = []
    sample_up_label_list = []

    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'npick' == line[0]:
            npick = int(line[1])

        elif 'sampledown' in line[0]:
            sample_down_format = line[1]
            col_down = [int(line[2]),int(line[3])]
            describe = describe + '_down' + line[0].replace('sampledown','')
            print('Sample down:', line[1].split('/')[-1])

        elif 'sampleup' in line[0]:
            sample_up_format_list += [line[1]]
            sample_up_label_list += [line[0].replace('sampleup','')]
            col_up = [int(line[2]),int(line[3])]
            print('Sample up',  line[0].replace('sampleup',''), ':', line[1].split('/')[-1])

    sample_ratio_list = [[] for i in range(0, len(sample_up_format_list))]
    for isamp in range(0, npick):
        
        sample_down = np.loadtxt(sample_down_format.replace('#',str(isamp)))
        for ir in range(0, len(sample_ratio_list)):
            sample_up = np.loadtxt(sample_up_format_list[ir].replace('#',str(isamp)))
            sample_ratio_list[ir] += [LORGI(sample_up, col_up, sample_down, col_down)]

    low16 = int((0.16*npick))
    high16 = int(np.ceil(0.84*npick))
    mid = int(0.5*npick)

    for ir in range(0, len(sample_ratio_list)):
        print('Ratio shape:', np.shape(sample_ratio_list[ir]))
        ratio_list_0 = []
        ratio_list_1 = []
        ratio_list_2 = []
        for ix in range(0, len(sample_ratio_list[ir][0])):
            iy = [sample_ratio_list[ir][isamp][ix][1] for isamp in range(0, npick)]
            iy_sort = np.sort(iy)
            #print(iy_sort)
            ratio_list_0 += [[sample_ratio_list[ir][0][ix][0], iy_sort[low16]]]
            ratio_list_1 += [[sample_ratio_list[ir][0][ix][0], iy_sort[mid]]]
            ratio_list_2 += [[sample_ratio_list[ir][0][ix][0], iy_sort[high16]]]
            #print(sample_ratio_list[ir][0][ix][0], iy_sort[mid])
        savesame = outfolder + describe + '_up' + sample_up_label_list[ir] + '_*.txt'
        print('Saving:', savesame.split('/')[-1],'\n')
        #print(np.shape(ratio_list_0),np.shape(ratio_list_1),np.shape(ratio_list_2))
        np.savetxt(savesame.replace('*', '0'), ratio_list_0, fmt='%.6e')
        np.savetxt(savesame.replace('*', '1'), ratio_list_1, fmt='%.6e')
        np.savetxt(savesame.replace('*', '2'), ratio_list_2, fmt='%.6e')

