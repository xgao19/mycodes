#!/usr/bin/env python3
import numpy as np
'''
ReadFile24D = '/Users/Xiang/Desktop/0study/research/lattice/data/DMW-qpdf/analysis_qpdf/24D_rITD/pion_zpz/results/24D.pion.ritd.1hyp.g8.sumfit.nsk2.pxnm.pz*.qx0qy0qz0.X0-12.real'
ReadFile48I = '/Users/Xiang/Desktop/0study/research/lattice/data/DMW-qpdf/analysis_qpdf/48I_rITD/pion_zpz/results/48I.pion.ritd.1hyp.g8.sumfit.nsk2.pxnm.pz*.qx0qy0qz0.X0-12.real'
ReadFile48c64a06 = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_48c64_data/gpd_analysis/rITD/qx0qy0qz0_boots/pion_zpz/results/48c64.pion.ritd.1hyp.g8.c3ptfit.nst2.nsk2.pxnm.pz*.qx0qy0qz0.X0-15.real'
ReadFile64c64a04 = '/Users/Xiang/Desktop/0study/research/lattice/data/pion_64c64_data/gpd_analysis/rITD/qx0qy0qz0_boots/pion_zpz/results/64c64.pion.ritd.1hyp.g8.c3ptfit.nst2.nsk3.pxnm.pz*.qx0qy0qz0.X0-32.real'
ReadFile64c64a076 = '/Users/Xiang/Desktop/0study/research/lattice/data/l64c64a076pi/analysis_qpdf/rITD/g8/qx0qy0qz0_boots/pion_zpz/results/64c64.pion.ritd.1hyp.g8.c3ptfit.nst2.nsk3.pxnm.pz*.qx0qy0qz0.X0-32.real'
outfolder = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/plots_rITD_NNLO_zpz/data/'


Ns24D = 24
Ns48I = 48
Ns48c64a06 = 48
Ns64c64a04 = 64
Ns64c64a076 = 64

latsp24D = 0.194416
latsp48I = 0.114063
latsp48c64a06 = 0.06
latsp64c64a04 = 0.04
latsp64c64a076 = 0.076

Pi=3.14259
fmGeV=5.0676896
pznm = 1
pzlist = [2,3,4]
ReadFile = ReadFile24D
Ns=Ns24D
latsp=latsp24D
outname = outfolder + 'DWF24D_rITD_z#_pz.txt'
ReadData = []
for ipz in range(0, len(pzlist)):
    filename = ReadFile.replace('pxnm', 'pxnm'+str(pznm))
    filename = filename.replace('*',str(pzlist[ipz]))
    ReadData += [np.loadtxt(filename)]
print(np.shape(ReadData))

SaveData = []
for iz in range(0, len(ReadData[0])):
    iz_SaveData = []
    for ipx in range(0, len(ReadData)):
        iz_ipx_Savedata = [pzlist[ipx]*2*Pi/(Ns*latsp)/fmGeV,ReadData[ipx][iz][1],ReadData[ipx][iz][2]]
        iz_SaveData += [iz_ipx_Savedata]
        print(iz_ipx_Savedata)
    SaveName = outname.replace('#', str(iz))
    np.savetxt(SaveName, iz_SaveData)
    SaveData += [iz_SaveData]
print(np.shape(SaveData))
'''
'''
pzmax = 7
pznm = 0
data = []
ErrMax = 0.3
ReadFile = ReadFile64c64a076

savefile = ReadFile.split('/')[-1].replace('*','n').replace('pxnm','pxnm'+str(pznm))
savefile = outfolder + savefile
for i in range(pznm+1, pzmax+1):
    ToRead = ReadFile.replace('pxnm','pxnm'+str(pznm))
    ToRead = ToRead.replace('*', str(i))
    ipz_data = np.loadtxt(ToRead)
    data += [ipz_data]
savedata = []
for ipz in range(0, len(data)):
    for iz in range(0, len(data[ipz])):
        if data[ipz][iz][-1] < ErrMax:
            savedata += [data[ipz][iz]]
np.savetxt(savefile, savedata)
'''

ReadFilel48c64a06 = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/data/rITD_band_zpz/l48c64a06_rITDmomsritdNNLO_3.2GeV_pznm1_zmin3_zmax0.5_contiN6_CmassN0_Pz*_bootssample#.txt'
ReadFilel64c64a04 = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/data/rITD_band_zpz/l64c64a04_rITDmomsritdNNLO_3.2GeV_pznm1_zmin3_zmax0.5_contiN6_CmassN0_Pz*_bootssample#.txt'
ReadFilel64c64a076 = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/data/rITD_band_zpz/l64c64a076_rITDmomsritdNNLO_3.2GeV_pznm1_zmin3_zmax0.5_contiN6_CmassN0_Pz*_bootssample#.txt'
ReadFile24D = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/data/rITD_band_zpz/dwf24D_rITDmomsritdNNLO_3.2GeV_pznm1_zmin3_zmax0.5_contiN6_CmassN0_Pz*_bootssample#.txt'
ReadFile48I = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/data/rITD_band_zpz/dwf48I_rITDmomsritdNNLO_3.2GeV_pznm1_zmin3_zmax0.5_contiN6_CmassN0_Pz*_bootssample#.txt'
outfolder = '/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1/plots_rITD_NNLO_zpz/band/'

Nsamp = 56
ReadFile = ReadFile48I
pzmax = 4
pznm = 1
low16 = int(np.ceil(0.16*Nsamp))
high16 = int(np.floor(0.84*Nsamp))
mid = int(0.5*Nsamp)

dataset = []
for isamp in range(0, Nsamp):
    SampFile = ReadFile.replace('#',str(isamp))
    isampData = []
    for ipz in range(pznm+1, pzmax):
        ToRead = SampFile.replace('*',str(ipz))
        isampData += list(np.loadtxt(ToRead))
    dataset += [isampData]
print(np.shape(dataset))

bootsdata0 = []
bootsdata1 = []
bootsdata2 = []
for izpz in range(0, len(dataset[0])):
    idata = [dataset[isamp][izpz][1] for isamp in range(0, len(dataset))]
    idata = sorted(idata)
    #print(idata)
    bootsdata0 += [[dataset[0][izpz][0], idata[low16]]]
    bootsdata1 += [[dataset[0][izpz][0], idata[mid]]]
    bootsdata2 += [[dataset[0][izpz][0], idata[high16]]]
    #print([dataset[0][izpz][0], idata[mid]])
bootsdata0 = sorted(bootsdata0, key=lambda x: x[0])
bootsdata1 = sorted(bootsdata1, key=lambda x: x[0])
bootsdata2 = sorted(bootsdata2, key=lambda x: x[0])

temp = [1,1]
tempdata0 = []
for i in range(0, len(bootsdata0)):
    row = bootsdata0[i]
    if abs(row[0]-temp[0]) > 0.000001 and i != 0:
        tempdata0 += [temp]
        temp = row
    elif row[1] < temp[1]:
        temp = row
bootsdata0 = tempdata0[1:]

temp = [1,1]
tempdata1 = []
for i in range(0, len(bootsdata1)):
    row = bootsdata1[i]
    if abs(row[0]-temp[0]) > 0.000001 and i != 0:
        tempdata1 += [temp]
        temp = row
    elif row[1] < temp[1]:
        temp = row
bootsdata1 = tempdata1[1:]

temp = [1,1]
tempdata2 = []
for i in range(0, len(bootsdata2)):
    row = bootsdata2[i]
    if abs(row[0]-temp[0]) > 0.000001 and i != 0:
        tempdata2 += [temp]
        temp = row
    elif row[1] > temp[1]:
        temp = row
bootsdata2 = tempdata2[1:]


outname0 = outfolder + ReadFile.split('/')[-1].replace('*','n').replace('bootssample#', 'band0')
outname1 = outfolder + ReadFile.split('/')[-1].replace('*','n').replace('bootssample#', 'band1')
outname2 = outfolder + ReadFile.split('/')[-1].replace('*','n').replace('bootssample#', 'band2')

np.savetxt(outname0, bootsdata0)
np.savetxt(outname1, bootsdata1)
np.savetxt(outname2, bootsdata2)

        

    