#!/usr/bin/env python3
from scipy.optimize import curve_fit
import numpy as np
from modelfuncs import *

latsp = 0.04
zmax = int(0.5/latsp)
datafile = "/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1_300MeV_bin2/single/results/samples/64c64a04_rITDmomsritdNLO_3.2GeV_zmin1_zmax0.7_contiN0_CmassN0_bootssample#.txt"
dataerrfile = "/Users/Xiang/Desktop/0study/research/lattice/data/rITD/pion_moments/joint_pznm1_300MeV_bin2/single/results/64c64a04_rITDmomsritdNLO_3.2GeV_zmin1_zmax0.7_contiN0_CmassN0.txt"
savefile = dataerrfile.replace(".txt","_fit#"+".txt")
dataerrdata = np.loadtxt(dataerrfile)
dataerr = [dataerrdata[i][3] for i in range(0, zmax)] 

kernellist = ['NLO', 'NLOEP', 'NNLO', 'NNLOEP']

def fx(x, moms, r): 
        return polynomial2func_mom2cutoff_NNLO(x, moms, r, latsp)

poptlist = []
for isamp in range(0, 52):
    data = np.loadtxt(datafile.replace('#',str(isamp+1)))
    x = [data[i][1] for i in range(0, zmax)] 
    y = [data[i][2] for i in range(0, zmax)]
    popt,popv=curve_fit(fx, x, y, p0=[0.1,-0.01], sigma=dataerr)
    poptlist += [list(popt)]
    zlist = [0.02+0.005*i for i in range(0, 141)]

r = [poptlist[i][1] for i in range(0, len(poptlist))]
r = sorted(r)
#print(r)
mean = r[int(len(r)*0.5)]
up = r[int(np.ceil(len(r)*0.84))]
down = r[int(len(r)*0.16)]
print(mean, up-mean, mean-down)


moms2list0 = []
moms2list1 = []
moms2list2 = []
for iz in range(0, len(zlist)):
    iz_moms2 = []
    for isamp in range(0, len(poptlist)):
        iz_moms2 += [fx(zlist[iz], poptlist[isamp][0], poptlist[isamp][1])]
    iz_moms2 = sorted(iz_moms2)
    mean = iz_moms2[int(len(iz_moms2)*0.5)]
    up = iz_moms2[int(np.ceil(len(iz_moms2)*0.84))]
    down = iz_moms2[int(len(iz_moms2)*0.16)]
    moms2list0 += [[zlist[iz], down]]
    moms2list1 += [[zlist[iz], mean]]
    moms2list2 += [[zlist[iz], up]]
np.savetxt(savefile.replace('#','0'),moms2list0)
np.savetxt(savefile.replace('#','1'),moms2list1)
np.savetxt(savefile.replace('#','2'),moms2list2)