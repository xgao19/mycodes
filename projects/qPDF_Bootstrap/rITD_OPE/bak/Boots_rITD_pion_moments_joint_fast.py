#!/usr/bin/env python3
import numpy as np
import sys
import time
from statistics import *
from rITD_pion_ritdReal import *
from scipy.optimize import least_squares
from tools import *

np.random.seed(2020)
random_uniform = np.random.uniform(0,1,1000000)
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

'''
This code is used to fit the pion moments model independently:

    1), A constrain of higher moments always smaller is used.
    2), One need to set up the global parameters first.
    3), One can add or change the model functions for fit.
    3), Need "statistics.py" for chisq evaluation.
    4), Need "rITD_pion_ritdReal_moments" for NNLO and so on.
'''

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### rITD functions ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def moms_exp(exp_list):
    exp_list1 = list(map(np.exp, exp_list))
    exp_list2= np.zeros(len(exp_list1))
    for i in range(0, len(exp_list1)):
        exp_list2[i] = np.sum(exp_list1[i:])
    moms_list = np.zeros(len(exp_list1))
    for i in range(0, len(exp_list2)):
        moms_list[i] = np.sum(exp_list2[i:])
    return moms_list

def ritd_Pzn_pznm_real(latsp, z, pz, mass, nmax, moms,bconti,rcut,cmass,k=0, pznm=0, kernel_list=[], **kwargs):
    c = mass*mass/(4*pz*pz)
    ritdsum = 1
    ritdsum_down = 1
    mass0 = 0.14*fmGeV

    # moms list have moments of PDFs as a_n, though we only use the even n in pion case.
    for i in range(1, int(nmax/2)+1):
        n = 2*i
        n_TMC = 0
        #for j in range(0, i+1):
        #    n_TMC += np.math.factorial(n-j)/np.math.factorial(j)/np.math.factorial(n-2*j)*c**j

        #print(n,i,kernel_list[i])
        ritdsum += kernel_list[i]*( (-1)**i*(z*pz)**n/np.math.factorial(n) * n_TMC * (moms[n]+cmass[n-1]*(mass-mass0)+bconti[n-1]*latsp*latsp))
        ritdsum_down += kernel_list[i]*( (-1)**i*(z*pznm)**n/np.math.factorial(n) * n_TMC * (moms[n]+cmass[n-1]*(mass-mass0)+bconti[n-1]*latsp*latsp))
    
    ritdsum += rcut*(latsp*pz)*(latsp*pz)
    ritdsum_down += rcut*(latsp*pznm)*(latsp*pznm)

    return ritdsum/ritdsum_down

def make_ritd_Pzn_pznm_real(nmax, contiN, CmassN, cutoff, k=0, fixN = [], **kwargs):

    def ritd_Pzn_pznm_real_constrained(latsp_mass_pznm_pz_z_kernel, exp_list):

        latsp, mass, pznm, pz, z, kernel_list = latsp_mass_pznm_pz_z_kernel
        moms_list = moms_exp(exp_list[len(fixN):int(nmax/2)])
        moms = np.ones(nmax+1)
        bconti = np.zeros(nmax+1)
        cmass = np.zeros(nmax+1)

        for i in range(0, len(fixN)):
            moms[i] = fixN[i]

        for imoms in range(0, len(moms_list)):
            moms[len(fixN)+2*imoms+1] = 0
            moms[len(fixN)+2*imoms+2] = moms_list[imoms]

        for i in range(0, contiN):
            bconti[i] = exp_list[len(fixN)+len(moms_list)*2]
        for i in range(0, CmassN):
            cmass[i] = exp_list[len(fixN)+len(moms_list)*2+contiN]

        if cutoff == 0:
            rcut = 0 # cutoff effect
        elif cutoff == 1:
            rcut = exp_list[-1]
        else:
            rcut = cutoff

        func = ritd_Pzn_pznm_real(latsp,z,pz,mass,nmax,moms,bconti,rcut,cmass,k,pznm,kernel_list, **kwargs)
        return func

    return ritd_Pzn_pznm_real_constrained

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################### Read data here ########################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def read_rITD_fitdata(inputfile):

    readfile = open(inputfile,'r')
    commands = readfile.readlines()

    for index in range(0, len(commands)):

        line = commands[index].split()
        #print(line)

        if 'Ns' == line[0]:
            Ns = int(line[1])
        
        elif 'Nt' == line[0]:
            Nt = int(line[1])

        elif 'latsp' == line[0]:
            latsp = float(line[1])
            print('  Lattice', str(Ns)+'c'+str(Nt)+',', 'a =', latsp, 'fm.')
        
        elif 'mass' in line[0]:
            mass = float(line[1])
            print('  Hardron mass:', mass, 'GeV')

        elif ('pxrange' in line[0]) or ('pzrange' in line[0]):
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            pxlist_fm = []
            pxlist_GeV = []
            for ipx in pxlist:
                pxlist_fm += [2*PI*ipx/(Ns*latsp)]
                pxlist_GeV += [round(2*PI*ipx/(Ns*latsp)/fmGeV,2)]
            print('  pzlist:',pxlist_GeV,'(GeV)')
            if len(pxlist) == 1:
                describe = describe.replace('pzup','pz'+str(pxmin))
            #print('pzlist:',pxlist)

        elif ('pxnm' == line[0]) or ('pznm' == line[0]):
            pznm = int(line[1])
            if pznm == 0:
                pznm = 0.0001
            pznm_fm = 2*PI*pznm/(Ns*latsp)
            pznm_GeV = round(2*PI*pznm/(Ns*latsp)/fmGeV,5)
            print('  pznm :',pznm_GeV,'(GeV)')

        elif 'mean' in line[0]:
            mean_input_format = line[1]
            mean_col = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]

    rITD_params = [Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]

    # Read the data here
    rITD_mean = [] #rITD_mean[ipx][iz]
    rITD_err = [] #rITD_mean_err[ipx][iz]
    rITD_sample_read = [] #rITD_sample[ipx][iz][isample]
    rITD_sample = [] #rITD_sample[isample][ipx][iz]
    rITD_samp_lenMax = 0
    for ipx in pxlist:

        ipx_mean_file = mean_input_format.replace('*', str(ipx))
        ipx_mean_data = np.loadtxt(ipx_mean_file)
        ipx_rITD_mean = []
        ipx_rITD_err = []
        zlen = len(ipx_mean_data)
        for iz in range(0, zlen):
            ipx_rITD_mean += [ipx_mean_data[iz][1]]
            ipx_rITD_err += [ipx_mean_data[iz][2]]
        rITD_mean += [ipx_rITD_mean]
        rITD_err += [ipx_rITD_err]

        ipx_sample_file = sample_input_format.replace('*', str(ipx))
        ipx_rITD_sample = []
        for iz in range(0, zlen):
            iz_sample_file = ipx_sample_file.replace('#', str(iz))
            ipx_rITD_sample += [np.loadtxt(iz_sample_file)]
            if len(ipx_rITD_sample[iz]) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(ipx_rITD_sample[iz])
        rITD_sample_read += [ipx_rITD_sample]
    print('  rITD data shape:', np.shape(rITD_sample_read), ', sample maximal length:', rITD_samp_lenMax)

    for isamp in range(0,rITD_samp_lenMax):
        ipx_rITD_sample = []
        for ipx in range(0, len(pxlist_fm)):
            iz_rITD_sample = []
            for iz in range(0, zlen):
                rITD_samp_len = len(rITD_sample_read[ipx][iz])
                #iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp%rITD_samp_len]]
                if isamp < rITD_samp_len:
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][isamp]]
                else:
                    ipick = random_uniform_int[-1][isamp]
                    iz_rITD_sample += [rITD_sample_read[ipx][iz][ipick%rITD_samp_len]]
                    #iz_rITD_sample += [rITD_sample_read[ipx][iz][np.random.randint(0, rITD_samp_len)]]
            ipx_rITD_sample += [iz_rITD_sample]
        rITD_sample += [ipx_rITD_sample]

    print('  mean data:', np.shape(rITD_mean), ', sample data:', np.shape(rITD_sample))
    return rITD_params, rITD_err, rITD_sample

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### fitting procedure ########################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def rITD_fit(fitfunc_name, rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu, nmax, alps=None,fixN = [], cutoff=0, contiN=0, CmassN=0, zcombine=True):

    # To determine the alphas of factorization scale
    mu_down = mu
    mu_mid = mu
    mu_up = mu

    if 'ritdLO' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return 1
    elif 'ritdNLO' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLO(mu,z,n,**kwargs)
    elif 'ritdNLOEP' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLO_EP(mu,z,n,**kwargs)
    elif 'ritdNLONLL' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLONLL(mu,z,n,**kwargs)
    elif 'ritdNLOevo' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLOevo(mu,z,n,**kwargs)
    elif 'ritdNNLO' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLO(mu,z,n,**kwargs)
    elif 'ritdNNLOEP' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLO_EP(mu,z,n,**kwargs)
    elif 'ritdNNLOevo' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLOevo(mu,z,n,**kwargs)
    elif 'ritdNNLOevoSat' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NNLOevoSat(mu,z,n,**kwargs)
    elif 'ritdNLOevoSat' == fitfunc_name:
        fitfunc = make_ritd_Pzn_pznm_real(nmax, contiN=contiN, CmassN=CmassN, cutoff=cutoff, fixN=fixN)
        def kernel(mu, z, n, **kwargs):
            return CnC0NLOevoSat(mu,z,n,**kwargs)

    poptlist = []
    zfmlist = []
    zfm = zmv
    momsExpStart = [-2.3,-4.5,-6.6,-8.3,-10.2]
    for zfm in np.arange(zfm, zmax+zmv/2, zmv):

        # fit y
        itdLat = []
        itdLatErr = []
        
        # fit x: (latsp,mass,pznm,pz,z)
        itdlatsp_fm = []
        itdmass_fm = []
        itdpznm_fm = []
        itdpz_fm = []
        itdz_fm = []
        itdz_kernel = []

        for ilat in range(0, len(rITD_params)):

            ilat_rITD = rITD_sample[ilat]
            ilat_rITD_err = rITD_err[ilat]

            ilat_latsp = rITD_params[ilat][2]
            ilat_mass_fm = rITD_params[ilat][3] * fmGeV
            ilat_pznm = rITD_params[ilat][4]
            ilat_pzlist = rITD_params[ilat][-1]

            for ipx in range(0, len(ilat_pzlist)):

                if zcombine == True:
                    zmin = zskip
                else:
                    zmin =  int(zfm/ilat_latsp)

                for iz in range(zmin, int(zfm/ilat_latsp)+1):

                    itdLat += [ilat_rITD[ipx][iz]]
                    itdLatErr += [ilat_rITD_err[ipx][iz]]

                    itdlatsp_fm += [ilat_latsp]
                    itdmass_fm += [ilat_mass_fm]
                    itdpznm_fm += [ilat_pznm]
                    itdpz_fm += [ilat_pzlist[ipx]]
                    itdz_fm += [iz*ilat_latsp]
                    itdz_kernel += []

                    iz_kernel_mu = [1] + [kernel(mu_mid, iz*ilat_latsp, 2*i, alps=alpsmu(mu)) for i in range(1, int(nmax/2)+1)]
                    #iz_kernel_halfmu = [1] + [kernel(mu_down, iz*ilat_latsp, 2*i, alps=alpsmu(mu/2)) for i in range(1, int(nmax/2)+1)]
                    #iz_kernel_doubmu = [1] + [kernel(mu_up, iz*ilat_latsp, 2*i, alps=alpsmu(mu*2)) for i in range(1, int(nmax/2)+1)]
                    #iz_kernel = [iz_kernel_mu, iz_kernel_halfmu, iz_kernel_doubmu]
                    iz_kernel = [iz_kernel_mu, iz_kernel_mu, iz_kernel_mu]
                    itdz_kernel += [iz_kernel]

        #print(itdLat)
        if zcombine == True:
            def ritd_res(popt):
                res = []
                for i in range(0, len(itdz_fm)):
                    latsp_mass_pznm_pz_z_kernel0 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][0])
                    ritd_kernel0 = fitfunc(latsp_mass_pznm_pz_z_kernel0, popt)
                    #latsp_mass_pznm_pz_z_kernel1 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][1])
                    #ritd_kernel1 = fitfunc(latsp_mass_pznm_pz_z_kernel1, popt)
                    ritd_kernel1 = 1
                    #latsp_mass_pznm_pz_z_kernel2 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][2])
                    ritd_kernel2 = 1
                    
                    deviation = (ritd_kernel0 - itdLat[i])*(ritd_kernel0 - itdLat[i])
                    err = itdLatErr[i]*itdLatErr[i] + (ritd_kernel1-ritd_kernel2)/2*(ritd_kernel1-ritd_kernel2)/2
                    res += [np.sqrt(deviation/err)]
                return res
        else:
            def ritd_res(popt):
                res = []
                for i in range(0, len(itdz_fm)):
                    latsp_mass_pznm_pz_z_kernel0 = (itdlatsp_fm[i],itdmass_fm[i],itdpznm_fm[i],itdpz_fm[i],itdz_fm[i],itdz_kernel[i][0])
                    ritd_kernel0 = fitfunc(latsp_mass_pznm_pz_z_kernel0, popt)
                    ritd_kernel1 = 1
                    ritd_kernel2 = 1
                    
                    deviation = (ritd_kernel0 - itdLat[i])*(ritd_kernel0 - itdLat[i])
                    err = itdLatErr[i]*itdLatErr[i] + (ritd_kernel1-ritd_kernel2)/2*(ritd_kernel1-ritd_kernel2)/2
                    res += [np.sqrt(deviation/err)]
                return res

        poptstart = momsExpStart[:int(nmax/2)]
        if cutoff != 0:
            poptstart += [0.01]
        if contiN != 0:
            for i in range(0, int(contiN/2)):
                poptstart += [0.01]
        if CmassN != 0:
            for i in range(0, int(CmassN/2)):
                poptstart += [0.01]

        if zcombine == True and len(itdLat) < len(poptstart) + 2:
            continue
        zfmlist += [zfm]

        timer = time.time()
        res = least_squares(ritd_res, poptstart, method='lm')
        print('tt:', time.time() - timer)
        popt = res.x
        #print(len(itdLat),len(itdz_fm),len(popt))
        resvec = res.fun
        chisq = sum(np.square(resvec)) / (len(itdz_fm)-len(popt))
        aic = sum(np.square(resvec)) + 2*len(popt)
        aicc = 0
        if zcombine == True:
            aicc = sum(np.square(resvec)) + 2*len(popt) + (2*len(popt)*len(popt)+2*len(popt))/(len(itdz_fm)-len(popt)-1)
        #print(popt, list(moms_exp(popt[len(fixN):int(nmax/2)])))
        popt = fixN + list(moms_exp(popt[len(fixN):int(nmax/2)])) + list(popt[int(nmax/2):]) + [chisq, aicc, aic]
        #print(popt)
        poptlist += [popt]

    return poptlist, zfmlist

'''
run the fitting here
'''

if __name__ == "__main__":
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    # read parameters
    fitfunc = []
    rITD_err = []
    poptguess = []
    fixN = []
    dataset = []
    cutoff = False
    fixOrder_alps = None
    conti = 0
    rITD_samp_lenMax = 0
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'mu' in line[0]:
            mu = float(line[1])
            print('Factorization scale:', mu, 'GeV', ', alpha_s =', alpsmu(mu*fmGeV))
            describe = describe.replace('GeV', line[1]+'GeV')

        elif 'fixOrder_alps' in line[0]:
            fixOrder_alps = float(line[1])
            print('Fix order alpha_s:', fixOrder_alps)
            describe = describe + '_alps' + line[1]

        elif 'zskip' in line[0]:
            zskip = int(line[1])
            print('Skip zmin (z/a) =', zskip)
            describe = describe.replace('zmin', 'zmin'+str(zskip))

        elif 'zmax(fm)' in line[0]:
            zmax = float(line[1])
            print('Zmax =', zmax, 'fm')
            describe = describe.replace('_zmax', '_zmax'+str(zmax))

        elif 'zmv(fm)' in line[0]:
            zmv = float(line[1])
            print('Z move =', zmv, 'fm')

        elif 'nmax' in line[0]:
            nmax = int(line[1])
            print('rITD nmax =', nmax)

        elif 'ritd' == line[0]:
            fitfunc += [line[1]]

        elif 'zCombine' in line[0]:
            if 'y' in line[1]:
                zcombine = True
                print('This is a combine fit of a range of z.')
            else:
                zcombine = False
                print('This is a fit for each z.')

        elif 'fixN' in line[0]:
            fixN = np.array([float(line[i]) for i in range(1, len(line))])
            print('rITD fixed moments:', fixN)

        elif 'cutoff' == line[0]:
            if line[1] == 'y':
                cutoff = [1,1,1,1,1]
                describe = describe + '_rcut'
                print('Cutoff parameter: yes')
            elif line[1] == 'n':
                cutoff = [0,0,0,0,0]
                print('Cutoff parameter: no')
            else:
                cutoff = [float(line[1]) for i in range(1, len(line))]
                print('Cutoff parameter r=',cutoff)

        elif 'continuum' == line[0]:
            contiN = int(line[1])
            describe = describe + '_contiN' + str(contiN)

        elif 'masscorrection' == line[0]:
            CmassN = int(line[1])
            describe = describe + '_CmassN' + str(CmassN)

        elif 'data' == line[0]:
            print('Reading data:')
            # data_params=[Ns, Nt, latsp, mass, pznm_fm, pxlist_fm]
            data_params, data_err, data_sample = read_rITD_fitdata(line[1])
            dataset += [[data_params, data_err, data_sample]]
            #print(np.shape(data_sample))
            rITD_err += [data_err]
            if len(data_sample) > rITD_samp_lenMax:
                rITD_samp_lenMax = len(data_sample)

    low16 = int((0.16*rITD_samp_lenMax))
    high16 = int(np.ceil(0.84*rITD_samp_lenMax))
    mid = int(0.5*rITD_samp_lenMax)

    for ifit in range(0, len(fitfunc)):

        moms_list = []
        out_name = outfolder+describe.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit])) + '.txt'
        print('\nGonna save in:',out_name)

        poptsample = [] #poptsample[isamp][iz][ipopt]
        for isamp in range(0, rITD_samp_lenMax):
            print('Bootstrap sample:', isamp, '/', rITD_samp_lenMax)
            rITD_sample = []
            rITD_params = []
            ipick = random_uniform_int[-2][isamp]
            for idata in dataset:
                rITD_params += [idata[0]]
                rITD_samp_len = len(idata[2])
                if isamp < rITD_samp_len:
                    rITD_sample += [idata[2][isamp]]
                else:
                    rITD_sample += [idata[2][ipick%rITD_samp_len]]
            popt, zfmlist = rITD_fit(fitfunc[ifit], rITD_params, rITD_sample, rITD_err, zskip, zmax, zmv, mu*fmGeV, nmax, fixOrder_alps, fixN, cutoff[ifit], contiN, CmassN, zcombine)
            poptsample += [popt]

        for isamp in range(0, rITD_samp_lenMax):
            poptsample_save = []
            sample_name = samplefolder+describe.replace('rITDmoms','rITDmoms'+str(fitfunc[ifit]))+'_bootssample'+str(int(isamp))+'.txt'
            for iz in range(0, len(zfmlist)):
                poptsample_save += [[zskip, zfmlist[iz]] + poptsample[isamp][iz]]
            #print(np.shape(poptsample[isamp]),poptsample[0])
            np.savetxt(sample_name, poptsample_save)

        for iz in range(0, len(zfmlist)):
            iz_moms = [zskip, zfmlist[iz]]
            for ipopt in range(0, len(poptsample[0][iz])):
                ipopt = [poptsample[isamp][iz][ipopt] for isamp in range(0, rITD_samp_lenMax)]
                ipopt = sorted(ipopt)
                ipopt_err = [ipopt[high16] - ipopt[mid], ipopt[mid] - ipopt[low16]]
                iz_moms += [ipopt[mid], max(ipopt_err)]
            np.set_printoptions(formatter={'float': '{: 0.3f}'.format})
            #print(iz_moms)
            moms_list += [iz_moms]
        #print(out_name)
        np.savetxt(out_name, moms_list, fmt='%.4e')

        
