#!/usr/bin/env python3
import numpy as np
import sys
from tools import *
from scipy import special
from scipy import integrate

def pdfs_model_ab(x, n, *ab):
    #print(ab)
    return (x**n)*special.gamma(2+ab[0]+ab[1])/(special.gamma(1+ab[0])*special.gamma(1+ab[1]))*(x**ab[0])*((1-x)**ab[1])
def pdfs_model_abst(x, n, *abst):
    A = special.gamma(1+abst[1])*(abst[2]*special.gamma(3/2+abst[0])/special.gamma(5/2+abst[0]+abst[1])+\
        (2+abst[0]+abst[1]+abst[3]+abst[0]*abst[3])*special.gamma(1+abst[0])/special.gamma(3+abst[0]+abst[1]))
    return 1/A*(x**n)*(x**abst[0])*((1-x)**abst[1])*(1+abst[2]*np.sqrt(x)+abst[3]*x)
def pdfs_model(x, n, *popt):
    if len(popt) == 2:
        return pdfs_model_ab(x, n, *popt)
    elif len(popt) == 4:
        return pdfs_model_abst(x, n, *popt)

def pdf_collection(filename, xlist, col):
    
    read_data = np.loadtxt(filename)
    dataset = []
    for isamp in range(0, len(read_data)):
        isamp_data = []
        for x in xlist:
            popt = [read_data[isamp][col[i]] for i in range(0, len(col))]
            isamp_data +=  [pdfs_model(x, 0, *popt)]
        dataset += [isamp_data]
    print('Read zmax:',read_data[0][1],'fm')
    return dataset

def data_collection(filename, col):
    
    read_data = np.loadtxt(filename)
    dataset = []
    for isamp in range(0, len(read_data)):
        isamp_data = [read_data[isamp][col[i]] for i in range(0, len(col))]
        popt = [read_data[isamp][col[i]] for i in range(0, len(col))]
        for n in range(1,7):
            isamp_data +=  [integrate.quad(lambda x: pdfs_model(x, n, *popt), 0, 1)[0]]
        isamp_data += [read_data[isamp][-1]]
        dataset += [isamp_data]
    return dataset



if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/band/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    line2nd = commands[1].split()
    xmin = float(line2nd[1])
    xmax = float(line2nd[2])
    xmv = float(line2nd[3])
    xlist = [float(line2nd[1])+i*float(line2nd[3]) for i in range(1, int((xmax-xmin)/xmv)+1)]
    print('PDF x in range: [', xlist[0],',', xlist[-1],'], with interval', xmv)

    pdf_dataset = []
    popt_dataset = []

    for index in range(2, len(commands)):

        line = commands[index].split()
        if 'data' == line[0]:
            filename = line[1]
            col = [int(line[i]) for i in range(2, len(line))]
            print('Reading file:', filename.split('/')[-1], ', col:', col)
            pdf_dataset += [pdf_collection(filename, xlist, col)]
            popt_dataset += [data_collection(filename, col)]
    print('PDF data shape:',np.shape(pdf_dataset), ', popt data shape:', np.shape(popt_dataset))

    ######## PDF-x
    data_syserr = []
    for ix in range(0, len(xlist)):
        ix_data_syserr = []
        for isamp in range(0, len(pdf_dataset[0])):
            isamp_data = [pdf_dataset[i][isamp][ix] for i in range(0, len(pdf_dataset))]
            isamp_data_syserr = [np.average(isamp_data),np.std(isamp_data)]
            ix_data_syserr += [isamp_data_syserr]
        data_syserr += [ix_data_syserr]
    #print(np.shape(data_syserr))

    up = int((len(data_syserr[0])*0.84))
    mid = int((len(data_syserr[0])*0.5))
    down = int((len(data_syserr[0])*0.16))
    pdf0 = []
    pdf0sys = []
    pdf1 = []
    pdf1sys = []
    pdf2 = []
    pdf2sys = []
    for ix in range(0, len(xlist)):
        meanlist = sorted([data_syserr[ix][isamp][0] for isamp in range(0, len(data_syserr[ix]))])
        #SysErr = sorted([data_syserr[ix][isamp][1] for isamp in range(0, len(data_syserr[ix]))])[mid]
        SysErr = np.average([data_syserr[ix][isamp][1] for isamp in range(0, len(data_syserr[ix]))])
        pdf_stderr = np.std(meanlist)*np.sqrt(len(meanlist)/(len(meanlist)-1))
        pdf0 += [[xlist[ix], np.average(meanlist)-pdf_stderr]]
        pdf1 += [[xlist[ix], np.average(meanlist)]]
        pdf2 += [[xlist[ix], np.average(meanlist)+pdf_stderr]]
        #pdf0 += [[xlist[ix], meanlist[down]]]
        #pdf1 += [[xlist[ix], meanlist[mid]]]
        #pdf2 += [[xlist[ix], meanlist[up]]]
        #pdf0sys += [[xlist[ix], np.average(meanlist)-np.sqrt(SysErr**2+pdf_stderr**2)]]
        #pdf1sys += [[xlist[ix], np.average(meanlist)]]
        #pdf2sys += [[xlist[ix], np.average(meanlist)+np.sqrt(SysErr**2+pdf_stderr**2)]]
        pdf0sys += [[xlist[ix], np.average(meanlist)-SysErr-pdf_stderr]]
        pdf1sys += [[xlist[ix], np.average(meanlist)]]
        pdf2sys += [[xlist[ix], np.average(meanlist)+SysErr+pdf_stderr]]
    savename = outfolder + describe + '_pdfx.txt'
    np.savetxt(savename.replace('pdfx','pdf0'), pdf0)
    np.savetxt(savename.replace('pdfx','pdf0sys'), pdf0sys)
    np.savetxt(savename.replace('pdfx','pdf1'), pdf1)
    np.savetxt(savename.replace('pdfx','pdf1sys'), pdf1sys)
    np.savetxt(savename.replace('pdfx','pdf2'), pdf2)
    np.savetxt(savename.replace('pdfx','pdf2sys'), pdf2sys)

    for i in range(0, len(pdf0)):
        pdf0[i][1] = pdf0[i][0]*pdf0[i][1]
        pdf0sys[i][1] = pdf0sys[i][0]*pdf0sys[i][1]
        pdf1[i][1] = pdf1[i][0]*pdf1[i][1]
        pdf1sys[i][1] = pdf1sys[i][0]*pdf1sys[i][1]
        pdf2[i][1] = pdf2[i][0]*pdf2[i][1]
        pdf2sys[i][1] = pdf2sys[i][0]*pdf2sys[i][1]
    pdf0 = [[0,0]] + pdf0
    pdf0sys = [[0,0]] + pdf0sys
    pdf1 = [[0,0]] + pdf1
    pdf1sys = [[0,0]] + pdf1sys
    pdf2 = [[0,0]] + pdf2
    pdf2sys = [[0,0]] + pdf2sys
    savename = outfolder + describe + '_pdfx.txt'
    np.savetxt(savename.replace('pdfx','pdfxfx0'), pdf0)
    np.savetxt(savename.replace('pdfx','pdfxfx0sys'), pdf0sys)
    np.savetxt(savename.replace('pdfx','pdfxfx1'), pdf1)
    np.savetxt(savename.replace('pdfx','pdfxfx1sys'), pdf1sys)
    np.savetxt(savename.replace('pdfx','pdfxfx2'), pdf2)
    np.savetxt(savename.replace('pdfx','pdfxfx2sys'), pdf2sys)

    ######## PDF-popt
    data_syserr = []
    for ip in range(0, len(popt_dataset[0][0])):
        ip_data_syserr = []
        for isamp in range(0, len(popt_dataset[0])):
            isamp_data = [popt_dataset[i][isamp][ip] for i in range(0, len(popt_dataset))]
            isamp_data_syserr = [np.average(isamp_data),np.std(isamp_data)]
            ip_data_syserr += [isamp_data_syserr]
        data_syserr += [ip_data_syserr]
        #print(data_syserr[ip])
    #print(np.shape(data_syserr))
    popt = []
    for ip in range(0, len(data_syserr)):
        meanlist = sorted([data_syserr[ip][isamp][0] for isamp in range(0, len(data_syserr[0]))])
        #print(len(data_syserr[ip]),len(meanlist),'\n')
        #print(len(meanlist))
        #SysErr = sorted([data_syserr[ip][isamp][1] for isamp in range(0, len(data_syserr[ip]))])[mid]
        SysErr = np.average([data_syserr[ip][isamp][1] for isamp in range(0, len(data_syserr[ip]))])
        ipopt_stderr = np.std(meanlist)*np.sqrt(len(meanlist)/(len(meanlist)-1))
        ip_popt = [np.average(meanlist), ipopt_stderr, ipopt_stderr]
        ip_popt += [np.sqrt(ipopt_stderr**2+SysErr**2),np.sqrt(ipopt_stderr**2+SysErr**2), SysErr]
        #ip_popt = [meanlist[mid], meanlist[mid]-meanlist[down], meanlist[up]-meanlist[mid]]
        #ip_popt = [meanlist[mid], (meanlist[up]-meanlist[down])/2, (meanlist[up]-meanlist[down])/2]
        #ip_popt = [(meanlist[up]+meanlist[down])/2, (meanlist[up]-meanlist[down])/2, (meanlist[up]-meanlist[down])/2]
        #ip_popt += [np.sqrt(((meanlist[up]-meanlist[down])/2)**2+SysErr**2),np.sqrt(((meanlist[up]-meanlist[down])/2)**2+SysErr**2), SysErr]
        #popt += [ip_popt]
        print(ip_popt)
    savename = outfolder + describe + '_popt.txt'
    np.savetxt(savename, popt)
    
