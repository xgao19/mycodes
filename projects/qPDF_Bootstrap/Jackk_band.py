#!/usr/bin/env python3
import numpy as np
import sys
import os
from tools import *
from modelfuncs import *

pxlist=[0]
mean_row = 0
mean_col = [2,4,6,8,10,12]
sample_col = [0,1,2,3,4,5]

save_format = '/Users/gaoxiang/Desktop/0study/research/lattice/data/nucleon_data/analysis/effectivemass/band/SS_px*_nst3_tmin#'
#mean_file = '/Users/gaoxiang/Desktop/0study/research/lattice/data/nucleon_data/analysis/c2ptfit_jackk/input_fixE0_E1/results/c2pt_a042fm_SP_px*_fixE0_tmax31_E1_twoexp.txt'
#sample_file = '/Users/gaoxiang/Desktop/0study/research/lattice/data/nucleon_data/analysis/c2ptfit_jackk/input_fixE0_E1/results/samples/c2pt_a042fm_SP_px*_fixE0_tmax31_E1_jackksample_tmin#_twoexp'
mean_file = '/Users/gaoxiang/Desktop/0study/research/lattice/data/nucleon_data/analysis/c2ptfit_jackk/input_fixE0_prE1SP/results/c2pt_a042fm_SS_px*_fixE0_prE1_tmax31_threeexp_prior_E1.txt'
sample_file = '/Users/gaoxiang/Desktop/0study/research/lattice/data/nucleon_data/analysis/c2ptfit_jackk/input_fixE0_prE1SP/results/samples/c2pt_a042fm_SS_px*_fixE0_prE1_tmax31_jackksample_tmin#_threeexp_prior_E1'

sample_file = sample_file.replace('#',str(mean_row+2))
save_format = save_format.replace('#',str(mean_row+2))
for ipx in pxlist:
    mean_data = np.loadtxt(mean_file.replace('*',str(ipx)))
    sample_data = np.loadtxt(sample_file.replace('*',str(ipx)))
    print(ipx, mean_data[mean_row][2],mean_data[mean_row][4],mean_data[mean_row][6],mean_data[mean_row][8],mean_data[mean_row][10],mean_data[mean_row][12])
    
    njackk = len(sample_data)
    x = np.arange(0,32,0.01)
    yerr = [0 for i in range(0,len(x))]
    y_up = [[x[i], 0] for i in range(0,len(x))]
    y_dw = [[x[i], 0] for i in range(0,len(x))]
    def Eeff(t):
        return np.log(threeexp(t, mean_data[mean_row][2],mean_data[mean_row][4],mean_data[mean_row][6],mean_data[mean_row][8],mean_data[mean_row][10],mean_data[mean_row][12])/threeexp(t+1, mean_data[mean_row][2],mean_data[mean_row][4],mean_data[mean_row][6],mean_data[mean_row][8],mean_data[mean_row][10],mean_data[mean_row][12]))
        #return np.log(twoexp(t, mean_data[mean_row][2],mean_data[mean_row][4],mean_data[mean_row][6],mean_data[mean_row][8])/twoexp(t+1, mean_data[mean_row][2],mean_data[mean_row][4],mean_data[mean_row][6],mean_data[mean_row][8]))
    y = Eeff(x)
    
    for i in range(0, njackk):
        #print(sample_data[i][0],sample_data[i][1],sample_data[i][2],sample_data[i][3])
        def sample_Eeff(t):
            return np.log(threeexp(t, sample_data[i][0],sample_data[i][1],sample_data[i][2],sample_data[i][3],sample_data[i][4],sample_data[i][5])/threeexp(t+1, sample_data[i][0],sample_data[i][1],sample_data[i][2],sample_data[i][3],sample_data[i][4],sample_data[i][5]))
            #return np.log(twoexp(t, sample_data[i][0],sample_data[i][1],sample_data[i][2],sample_data[i][3])/twoexp(t+1, sample_data[i][0],sample_data[i][1],sample_data[i][2],sample_data[i][3]))
        sample_y = sample_Eeff(x)
        #print(y[-1], sample_y[-1])
        for j in range(0, len(x)):
            yerr[j] += (y[j] - sample_y[j]) * (y[j] - sample_y[j])
    
    for j in range(0, len(x)):
        yerr[j]=np.sqrt(yerr[j]*(njackk-1)/njackk)
        y_up[j][1] = y[j] + yerr[j]
        y_dw[j][1] = y[j] - yerr[j]
        
    out_up = save_format.replace('*',str(ipx)) + '_0.txt'
    out_dw = save_format.replace('*',str(ipx)) + '_1.txt'
    np.savetxt(out_up,y_up)
    np.savetxt(out_dw,y_dw)
