#!/usr/bin/env python3
import numpy as np
import traceback 
import sys
import inspect
import h5py
from scipy.optimize import curve_fit

from tools import *
from modelfuncs import *
from makefuncs import *
from statistics import *

np.random.seed(2020)
random_uniform = np.random.uniform(0,1,1000000)
random_len = int(np.sqrt(len(random_uniform)))
random_uniform_int = []
for i in range(0, random_len):
    i_random_uniform_int = []
    for j in range(0, random_len):
        i_random_uniform_int += [int(random_uniform[i*random_len+j]*len(random_uniform))]
    random_uniform_int += [i_random_uniform_int]
print('\n@@@ You take a 2-D random integer list in shape:', np.shape(random_uniform_int),'\n')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# This code read 2pt data from csv format and read 3pt data 
# from h5 format. 
#
# Some variable names are set as momentum along with x direction,
# , you will need to modify the code to adjust your data format,
# such as the kineimatic paramters and the breit-frame direction.
#
# Note:
#   1, gmlist is important:
#     a), if gmlist[0] == g8 and len(gmlist) > 0, the code
#     will suppose you want to extract breit frame data by 
#     combine different gamma components.
#     b), if gmlist[0] != g8, the code
#     will simply average different gamma components.
#   2, nave or ave means if will average the different
#   momentum transfer \vec{q} with same Q^2.
#   3, c2pt[ts][cfgs], c3pt[gm][ts][z][tau][cfgs]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Take a 2-d list, and average per bin with certain bin size.
def boots_avebin(data, binsize):
    new_data = []
    ncfg = len(data[0])-1
    for i in range(0, len(data)):
        i_new_data = [data[i][0]]
        for j in range(0, int(ncfg/binsize)):
            i_new_data += [np.average(data[i][1+j*binsize:1+(j+1)*binsize])]
        #print(ncfg,binsize,int(ncfg/binsize),len(i_new_data))
        new_data += [i_new_data]
    return new_data

# Take a 2-d list, and pick the data using random_uniform_int.
def boots_pick(data, isamp):
    new_data = []
    ncfg = len(data[0])-1
    for i in range(0, len(data)):
        i_new_data = [data[i][0]]
        i_data = data[i][1:]
        #picklist = []
        for ipick in range(0, ncfg):
            #print(np.shape(i_data), ipick%len(i_data))
            i_new_data += [i_data[random_uniform_int[isamp][ipick]%len(i_data)]]
            #picklist += [random_uniform_int[isamp][ipick]%len(i_data)]
        #print(picklist)
        new_data += [i_new_data]
    return new_data



if __name__ == "__main__":

    if sys.argv[1] == 'da':

        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]
        NS = int((commands[0].split())[1])
        NT = int((commands[0].split())[2])
        blocksize = int((commands[0].split())[3])
        npick = 0

        # create the work folders here
        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
        if 'c3ptfit' in describe:
            resultfolder = resultfolder.replace('results', 'results_c3ptfit')
        elif 'sumfit' in describe:
            resultfolder = resultfolder.replace('results', 'results_sumfit')
        elif 'ratio' in describe:
            resultfolder = resultfolder.replace('results', 'results_ratio')
        mkdir(resultfolder)
        samplefolder = resultfolder + 'samples/'
        mkdir(samplefolder)

        print('\nNote! The Ein and Eout in the input file must be listed from small to large !\n')
        print('\nJob describe:', describe, 'NS:', NS, 'NT:', NT, 'Bootstrap bin size:', blocksize)

        Ein_count = 0
        Eout_count = 0
        taumin = 0
        qxyzlist = []
        c2pt_format = []
        c3pt_format = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'npick' == line[0]:
                npick = int(line[1])
                print('Bootstrap sample number: npick =', npick)

            elif 'gmlist' == line[0]:
                gmlist = [line[i] for i in range(1, len(line))]
                print('gmlist:', gmlist)
                if len(gmlist) == 1:
                    describe = describe.replace('gm', gmlist[0])
            
            elif 'qxyz' in line[0]:
                qxyz = [int(line[1]),int(line[2]),int(line[3])]
                qxyzlist += [qxyz]
                print('qxyz:', qxyz)

            elif 'ts' == line[0]:
                ts = int(line[1])
                describe = describe.replace('ts','ts'+str(ts))
                print('ts:', ts)

            elif 'taumin' in line[0]:
                taumin = int(line[1])
                describe = describe.replace('nsk', 'nsk'+str(taumin))
                print('taumin:', taumin)

            elif 'zrange' in line[0]:
                zlist = [iz for iz in range(int(line[1]),int(line[2])+1)]
                print('zlist:', zlist)

            elif 'c3pt' in line[0]:
                c3pt_format = line[1]
            elif 'c2pt' in line[0]:
                c2pt_format = line[1]
                
        low16 = int((0.16*npick))
        high16 = int(np.ceil(0.84*npick))
        mid = int(0.5*npick)

        # run the analysis from here
        samplename = samplefolder + describe + '_bootssample'
        for iq in range(0,len(qxyzlist)):

            print('\n\n>>> qxyz =', qxyzlist[iq])

            iq_describe = describe.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            iq_samplename = samplename.replace('qxqyqz', 'qx'+str(qxyz[0])+'qy'+str(qxyz[1])+'qz'+str(qxyz[2]))
            iq_c3pt_format = c3pt_format.replace('qx_qy_qz', 'qx'+str(qxyz[0])+'_qy'+str(qxyz[1])+'_qz'+str(qxyz[2]))

            c2pt_data = csvtolist(c2pt_format)
            #iq_ratio_meanerr = []
            
            for iz in range(0, len(zlist)):
                iz_c3pt_format = iq_c3pt_format.replace('#',str(zlist[iz]))
                iz_describe = iq_describe.replace('X','X'+str(zlist[iz]))
                print('    z =', zlist[iz], ',',iz_describe)

                iz_c3pt_data = csvtolist(iz_c3pt_format)
                iz_ratio_meanerr = []
                iz_ratio_samps = [[] for itau in range(taumin, ts+1)]
                
                for isamp in range(0, npick):
                    isamp_c2pt = boots_pick(c2pt_data, isamp)
                    isamp_c3pt = boots_pick(iz_c3pt_data, isamp)

                    for itau in range(taumin, ts+1):
                        isamp_c3pt_ave = np.average(isamp_c3pt[itau][1:])
                        isamp_c2pt_ave = np.average(isamp_c2pt[itau][1:])
                        iz_ratio_samps[itau] += [isamp_c3pt_ave/isamp_c2pt_ave]

                for itau in range(taumin, ts+1):
                    itau_ratio_samps = sorted(iz_ratio_samps[itau])
                    iz_ratio_meanerr += [[itau, itau_ratio_samps[mid], (itau_ratio_samps[high16] - itau_ratio_samps[low16])/2]]
                
                np.savetxt(resultfolder+iz_describe+'.txt',iz_ratio_meanerr)

                