#!/usr/bin/env python3
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
from modelfuncs import *
import os.path
from pylab import *
from scipy.optimize import curve_fit

data = np.loadtxt('/Users/Xiang/Desktop/0study/research/lattice/data/DMW-qpdf/analysis_qpdf/24D_pion_ratio/g8/sumfit/results_sumfit/24D_qpdf_pion_sumfit_g8_nst_nsk2_px0py0pzn_X0.txt')
x = [data[i][0] for i in range(0, len(data))]
y = [data[i][1] for i in range(0, len(data))]
yerr = [data[i][2] for i in range(0, len(data))]

popt,popv = curve_fit(linearfunc, x, y, [0,0],sigma=yerr)
print(popt)