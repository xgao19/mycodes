#!/usr/bin/env python3
import numpy as np
import os
from math import gamma
from math import pow
import traceback 
import sys

import modelfuncs
from modelfuncs import *
from tools import *
#from fine_PseudoPDF import *

def make_sumfit_correction(Ein, Eout):
    def sumfit_corrected(ts, a, b, c):
        return linearfunc_pcorrection(ts, a, b, c, dE1=Eout[3]-Eout[1])
    return sumfit_corrected

def make_gpd_c3pt_ratio(Ein, Eout):
    if Ein[1] < 0.07 and Eout[1] > 0.07:
        if len(Ein) == 4:
            def gpd_ratio_twostate_fit(tstau, B00, B01, B10, B11, B_thermo_2):
                return gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2)
            return gpd_ratio_twostate_fit
        elif len(Ein) == 6:
            def gpd_ratio_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1, B_thermo_2):
                return gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_1=B_thermo_1, B_thermo_2=B_thermo_2)
            return gpd_ratio_threestate_fit
    else:
        if len(Ein) == 4:
            def gpd_ratio_twostate_fit(tstau, B00, B01, B10, B11):
                return gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11)
            return gpd_ratio_twostate_fit
        elif len(Ein) == 6:
            def gpd_ratio_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22):
                return gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22)
            return gpd_ratio_threestate_fit

def make_gpd_c3pt(Ein, Eout):
    if Ein[1] < 0.07 and Eout[1] > 0.07:
        if len(Ein) == 4:
            def gpd_c3pt_twostate_fit(tstau, B00, B01, B10, B11, B_thermo_2):
                return gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2)
            return gpd_c3pt_twostate_fit
        elif len(Ein) == 6:
            def gpd_c3pt_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2):
                return gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=B_thermo_2)
            return gpd_c3pt_threestate_fit
    else:
        if len(Ein) == 4:
            def gpd_c3pt_twostate_fit(tstau, B00, B01, B10, B11):
                return gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11)
            return gpd_c3pt_twostate_fit
        elif len(Ein) == 6:
            def gpd_c3pt_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22):
                return gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22)
            return gpd_c3pt_threestate_fit

def makec3pt_params(c3ptparams, c2ptpopt):

    #print(c3ptparams, c2ptpopt)
    if len(c2ptpopt) == 0:
        c2ptpopt = c3ptparams[0][2:]

    for i in range(0,len(c3ptparams)):
        if c3ptparams[i][0] == 'fixSSA0E0A1E1A2E2':
            #SSfitparams[i][0] = 'fixE0E1'
            c3ptparams[i][2] = c2ptpopt[0]
            c3ptparams[i][3] = c2ptpopt[1]
            c3ptparams[i][4] = c2ptpopt[2]
            c3ptparams[i][5] = c2ptpopt[3]
            c3ptparams[i][6] = c2ptpopt[4]
            c3ptparams[i][7] = c2ptpopt[5]
        elif c3ptparams[i][0] == 'fixSSA0E0A1E1':
            #SSfitparams[i][0] = 'fixE0E1'
            c3ptparams[i][2] = c2ptpopt[0]
            c3ptparams[i][3] = c2ptpopt[1]
            c3ptparams[i][4] = c2ptpopt[2]
            c3ptparams[i][5] = c2ptpopt[3]
        elif c3ptparams[i][0] == 'fixSSA0E0A1':
            #SSfitparams[i][0] = 'fixE0E1'
            c3ptparams[i][2] = c2ptpopt[0]
            c3ptparams[i][3] = c2ptpopt[1]
            c3ptparams[i][4] = c2ptpopt[2]
            #c3ptparams[i][5] = c2ptpopt[3]
        elif c3ptparams[i][0] == 'fixSSA0E0':
            #SSfitparams[i][0] = 'fixE0E1'
            c3ptparams[i][2] = c2ptpopt[0]
            c3ptparams[i][3] = c2ptpopt[1]
        elif c3ptparams[i][0] == 'full' or 'prior':
            pass
        else:
            print('!!! Wrong input! Please check! ')

        return c3ptparams


def makefixfunc(funcarg, fixarg, poptguess):

    if fixarg == 'fixE0':
        func = getattr(modelfuncs,funcarg)
        fix_num = 1
        if 'one' in funcarg:
            def fitfunc(x,A0,E0):
                return func(x,A0,poptguess[1])
        elif 'two' in funcarg:
            #print(poptguess[1])
            def fitfunc(x,A0,E0,A1,E1):
                return func(x,A0,poptguess[1],A1,E1)
        elif 'three' in funcarg:
            def fitfunc(x,A0,E0,A1,E1,A2,E2):
                return func(x,A0,poptguess[1],A1,E1,A2,E2)
        else:
            print('Wrong format or wrong choice!')
            sys.exit(0)
        
    elif fixarg == 'fixA0E0' or fixarg == 'fixSPA0E0':
        func = getattr(modelfuncs,funcarg)
        fix_num = 2
        if 'two' in funcarg:
            def fitfunc(x,A0,E0,A1,E1):
                return func(x,poptguess[0],poptguess[1],A1,E1)
        elif 'three' in funcarg:
            def fitfunc(x,A0,E0,A1,E1,A2,E2):
                return func(x,poptguess[0],poptguess[1],A1,E1,A2,E2)
        else:
            print('Wrong format or wrong choice!')
            sys.exit(0)

    elif fixarg == 'fixE0E1' or fixarg == 'fixSPE0E1':
        func = getattr(modelfuncs,funcarg)
        fix_num = 2
        if 'two' in funcarg:
            def fitfunc(x,E0,A0,E1,A1):
                return func(x,A0,poptguess[1],A1,poptguess[3])
        elif 'three' in funcarg:
            def fitfunc(x,A0,E0,A1,E1,A2,E2):
                return func(x,A0,poptguess[1],A1,poptguess[3],A2,E2)
        else:
            print('Wrong format or wrong choice!')
            sys.exit(0)

    elif fixarg == 'fixE0E2' or fixarg == 'fixSPE0E2':
        func = getattr(modelfuncs,funcarg)
        fix_num = 2
        if 'three' in funcarg:
            def fitfunc(x,A0,E0,A1,E1,A2,E2):
                return func(x,A0,poptguess[1],A1,E1,A2,poptguess[5])
        else:
            print('Wrong format or wrong choice!')
            sys.exit(0)
        
    elif fixarg == 'fixE0E1E2' or fixarg == 'fixSPE0E1E2':
        func = getattr(modelfuncs,funcarg)
        fix_num = 3
        if 'three' in funcarg:
            def fitfunc(x,A0,E0,A1,E1,A2,E2):
                return func(x,A0,poptguess[1],A1,poptguess[3],A2)
        else:
            print('Wrong format or wrong choice!')
            sys.exit(0)
        
    else:
        fitfunc = getattr(modelfuncs,funcarg)
        fix_num = 0

    return fitfunc, fix_num

# prior here is soppose to be a 2d array.
# prior[i] = [x, prior_y, prior_yerr]
def prior_data(x,y,ycov,prior=[[None]]):

    for raw in prior:

        prior_x = modelfuncs.priorsign + 1
        prior_y = raw[0]
        prior_err = raw[1]
        x += [prior_x]
        y += [prior_y]
        prior_temp = []
        new_ycov = []

        for i in range(0, len(ycov)):
            new_ycov += [list(ycov[i]) + [0]]
            prior_temp += [0]

        prior_temp += [prior_err*prior_err]
        new_ycov += [prior_temp]
        ycov = list_to_arr_2d(new_ycov)

    return x,y,ycov
def prior_data_2x(x1,x2,y,ycov,prior=[[None]]):

    for raw in prior:

        prior_x1 = modelfuncs.priorsign + 1
        prior_x2 = modelfuncs.priorsign + 1
        prior_y = raw[0]
        prior_err = raw[1]
        x1 += [prior_x1]
        x2 += [prior_x2]
        y += [prior_y]
        prior_temp = []
        new_ycov = []

        for i in range(0, len(ycov)):
            new_ycov += [list(ycov[i]) + [0]]
            prior_temp += [0]

        prior_temp += [prior_err*prior_err]
        new_ycov += [prior_temp]
        ycov = list_to_arr_2d(new_ycov)

    return x1,x2,y,ycov
