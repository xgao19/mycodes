#!/usr/bin/env python3
import numpy as np
import sys
import os
from tools import *
from bare_matrix import *

def nk_rimom(filename):
    Re_rimom = np.loadtxt(filename)
    Im_rimom = np.loadtxt(filename.replace('R','I'))
    complex_rimom = []
    for i in range(0, int(len(Re_rimom)/2)+1):
        print(i, Re_rimom[i][0], len(Re_rimom)-i,Re_rimom[len(Re_rimom)-i-1][0])
        complex_rimom = [[Re_rimom[len(Re_rimom)-i-1][0], (Re_rimom[i][1]+Re_rimom[len(Re_rimom)-i-1][1])/2, (Im_rimom[i][1]-Im_rimom[len(Re_rimom)-i-1][1])/2]] + complex_rimom
    #print(complex_rimom)
    return complex_rimom



def renormalization_rimom_jackk(zfactor,mean_format,col_mean,sample_format,col_sample,zlist,pxlist,outfolder,describe,latsp=1):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    sampledata = (np.loadtxt((sample_format.replace('*',str(pxlist[0]))).replace('#', str(zlist[0]))))
    njackk = len(sampledata)

    # data collection
    #print(zfactor)
    bm_mean_data = []
    bm_samples_data = []
    bm_mean_data, bm_samples_data = jackkbm_collect(mean_format,col_mean,sample_format,col_sample,zlist,pxlist)
    rn_factor = []
    for i in range(0, len(zlist)):
        #rn_factor += [1/complex(zfactor[zlist[i]][1],zfactor[zlist[i]][2])]
        rn_factor += [complex(zfactor[zlist[i]][1],zfactor[zlist[i]][2])]
    #print(rn_factor)

    '''
    if 'rbm' in mean_format:
        norm = rn_factor[0]
    else:
        norm = 1
    print('norm:',norm)
    '''

    # reduced bm
    rnm_out_format = outfolder + describe
    sample_out_format = samplefolder + describe + '_jackksamples'
    
    for ipx in range(0, len(pxlist)):
        
        ipx_rnm_real = []
        ipx_rnm_imag = []
        norm = bm_mean_data[ipx][0] * rn_factor[0]
        
        for iz in range(0, len(zlist)):

            jackksample_real = []
            jackksample_imag = []
            jackkmean = bm_mean_data[ipx][iz] * rn_factor[iz] / norm
            #print(bm_mean_data[ipx][iz], rn_factor[iz])
            jackkerr_real = 0
            jackkerr_imag = 0
            #print(jackkmean)

            samplefile = sample_out_format.replace('*', str(pxlist[ipx]))
            samplefile = samplefile.replace('#', str(zlist[iz]))
            for isamp in range(0, njackk):
                norm = bm_samples_data[ipx][0][isamp] * rn_factor[0]
                isample = bm_samples_data[ipx][iz][isamp] * rn_factor[iz] / norm
                jackksample_real += [isample.real]
                jackksample_imag += [isample.imag]
                jackkerr_real += (jackkmean.real-isample.real)*(jackkmean.real-isample.real)
                jackkerr_imag += (jackkmean.imag-isample.imag)*(jackkmean.imag-isample.imag)
            ipx_iz_rnm_real = [zlist[iz]*latsp, jackkmean.real, np.sqrt(jackkerr_real*(njackk-1)/njackk)]
            ipx_iz_rnm_imag = [zlist[iz]*latsp, jackkmean.imag, np.sqrt(jackkerr_imag*(njackk-1)/njackk)]
            ipx_rnm_real += [ipx_iz_rnm_real]
            ipx_rnm_imag += [ipx_iz_rnm_imag]
            np.savetxt(samplefile, jackksample_real)
            np.savetxt(samplefile.replace('real','imag'), jackksample_imag)
        rnm_out_name = rnm_out_format.replace('*',str(pxlist[ipx]))
        rnm_out_name = rnm_out_name.replace('#',str(zlist[0])+'-'+str(zlist[-1]))
        print(rnm_out_name)
        np.savetxt(rnm_out_name,ipx_rnm_real)
        np.savetxt(rnm_out_name.replace('real', 'imag'),ipx_rnm_imag)
        #np.savetxt(rnm_out_name.replace('real', 'complex'),ipx_rnm_real+ipx_rnm_imag)

if __name__ == "__main__":

    inputfile = open(sys.argv[2],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[2])
    outfolder = inputfolder + '/results/'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    latsp = 1
    print('\n\n\n\n\n\n\njob describe:',describe,'\n')

    # read parameters
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'zrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        elif 'latsp' == line[0]:
            latsp = float(line[1])

        elif 'pxrange' in line[0]:
            pxmin = int(line[1])
            pxmax = int(line[2])
            pxlist = [ipx for ipx in range(pxmin, pxmax+1)]
            print('pxlist:',pxlist)
        
        elif 'mean' in line[0]:
            mean_input_format = line[1]
            col_mean = int(line[2])

        elif 'sample' in line[0]:
            sample_input_format = line[1]
            col_sample = int(line[2])

        elif 'factor' in line[0]:
            if 'nk' in line[0]:
                zfactor = nk_rimom(line[1])
            else:
                zfactor = np.loadtxt(line[1])

    if sys.argv[1] == 'rnm':
        renormalization_rimom_jackk(zfactor,mean_input_format,col_mean,sample_input_format,col_sample,zlist,pxlist,outfolder,describe,latsp)