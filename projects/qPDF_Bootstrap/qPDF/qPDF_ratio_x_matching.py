#!/usr/bin/env python3
import numpy as np
import numba as nb
import mpmath
import time
from scipy import integrate
import sys

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#----------------- Constants ------------------#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
cf = 4/3
ca = 3
Tf = 1/2
nf = 3
b0 = 1/6 * ( 11 * ca - 4 * nf * Tf )

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#----------------- Functions ------------------#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
@nb.njit
def arccoth(val):
    """
    Inverse hyperbolic cotangent
    """
    return np.arctanh(1. / val)

@nb.njit
def ToLi2(x):
    return np.log(1-x)/x
@nb.njit
def ToLi3(x, z):
    return np.log(x)*np.log(x)/(1-x*z)

def polylog(n,z):

    while n == 0:
        return z/(1 - z)
    while n == 1:
        return -np.log(1 - z)
    while n == 2:
        return integrate.quad(ToLi2, z, 0)[0]
    while n >= 3:
        return mpmath.polylog(3, z)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#------------- Splitting Functions ------------#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
@nb.njit
def Pqq(y):
    return (1 + y*y) / (1-y)
@nb.njit
def PF(y):
    part1 = -2*(1+y*y)/(1-y) * np.log(1-y) * np.log(y)
    part2 = -(3/(1-y) + 2*y) * np.log(y)
    part3 = -1/2*(1+y) * (np.log(y)*np.log(y)) - 5*(1-y)
    return part1+part2+part3
@nb.njit
def PA(y):
    part11 = (1 + y*y) / (1-y)
    part12 = 67/9 - np.pi*np.pi/3 + 11/3 * np.log(y) + np.log(y)*np.log(y)
    part2 = 2 * (1+y) * np.log(y)
    part3 = 40/3 * (1-y)
    return part11*part12 + part2 + part3
@nb.njit
def Pnf(y):
    return 2/3 * (-2*(1-y) + (1+y*y)/(1-y) * (-5/3-np.log(y)))
def Pbar(y):
    part1 = 4*(1-y) + 2*(1+y)*np.log(y)
    part21 = 2*(1+y*y)
    part22 = (np.log(y) * (np.log(y)-2*np.log(1+y)) + (polylog(2,-1/y)-polylog(2,-y)))
    part2 = part21*part22/(1+y)
    return part1 + part2
@nb.njit
def PqqV(y):
    return cf*cf*PF(y) + 1/2*cf*ca*PA(y) + cf*nf*Tf*Pnf(y)
def Pm(y):
    return PqqV(y) - (cf*cf - cf*ca/2)*Pbar(y)
#print(PqqV(0.2),Pm(0.2))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#---------------- Convolutions ----------------#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
@nb.njit
def R1(x):
    return -((1 + x*x)/(1 - x)) - 4*np.log(1 - x)/(1 - x) + 2*(1 - x)
def R1R1(x):
    part1 = 1/(3*(-1 + x))
    part21 = -4*(np.pi)*(np.pi)*(1+(x-4)*x) + 3*(7+x*(-24+7*x)) - 48*np.log(1-x)*np.log(1-x)*np.log(1-x) + 3*np.log(x)
    part22 = 9*x*x*np.log(x) + 12*np.log(1-x)*np.log(1-x)*(-3+(-4+x)*x+4*np.log(x))
    part23 = 4*np.log(1-x)*(4*np.pi*np.pi - 3*(6+x*x) + 12*np.log(x))
    part24 = 24*(3-4*x+x*x+2*np.log(x))*polylog(2,x) - 96*polylog(3,x)
    return part1*(part21+part22+part23+part24)
@nb.njit
def PqqPqq(z):
    return 4*(1+z*z)/(1-z)*np.log(1-z) + 3*(1+z*z)/(1-z) + (-2*(1+z*z)/(1-z)+1+z)*np.log(z) - 2*(1-z)
def PqqR1(x):
    part0 = 5/2*Pqq(x)
    part1 = 1/3/(x-1)
    part21 = 9 - 4*np.pi*np.pi + 6*x + 9*x*x + 6*(5+x*x)*np.log(1-x)*np.log(1-x)
    part22 = 3 * (1 + x * (-8 + 3 * x)) * np.log(x)
    part23 = -6*np.log(1-x) * (-1 + 2*(-4+x)*x + 2*(1+x*x)*np.log(x))
    part24 = -12 * (-1 + x*x) * polylog(2,1-x)
    return part0 + part1*(part21+part22+part23+part24)
def R2(z):
    zeta3 = 1.202056903
    #part1
    part11 = cf*cf/(12*(-1+z*z))
    part121 = -96 * (1+z) * np.log(1-z)*np.log(1-z)*np.log(1-z)
    part122 = -2 * (-6*(1+z)*(19+z*(-36+19*z)) + np.pi*np.pi*(7+z*(-5+z*(-19+z*(7+6*z)))) + 6*np.log(z))
    part123 = -6*(1+z)*np.log(1-z)*np.log(1-z) * (10 + 2*z*(9+(-3+z)*z) + (-7+9*z*z)*np.log(z))
    part124 = np.log(z) * (6*z*(33-43*z*z) - 3*(3+z+8*z*z-2*z*z*z+4*z*z*z*z)*np.log(z) + (-1+z)*(3+z*(2+3*z))*np.log(z)*np.log(z))
    part125 = 2*(1+z)*np.log(1-z) * (12 - 3*z*(11+z) + np.pi*np.pi*(13+5*z*z) + 3*np.log(z)*(2+2*z*(-8+z*(3+2*z))+(1+z*z)*np.log(z)))
    part126 = 4*(-1+z) * (np.pi*np.pi*(5+z*z) + 3*np.log(z) * (10 + 2*z*(10+z) + np.log(z) - 3*z*z*np.log(z)))*np.log(1+z)
    part127 = -8*(-1+z)*(5+z*z)*np.log(1+z)*np.log(1+z)*np.log(1+z)
    part128 = -24 * ((1-z)*(5+z*(10+z)) + (-3+z*(-1+z*(-7+3*z)))*np.log(z)) * polylog(2,-z)
    part129 = -12 * (-2*(1+z)*(1+z*(-9+4*z)) + ((-3+z*(-3+5*z))*np.log(1-z) + np.log(z) + (z*(5+z)*np.log(z) + 5*z*z*z*np.log((1-z)*z)))) * polylog(2,z)
    part1210 = -12 * (1+z) * (-1+7*z*z) * polylog(3,1-z)
    part1211 = (204 + 12*z*(13 + 5*z*(5+z))) * polylog(3,z)
    part1212 = (-42 + 6*z*(-1+z*(-11+3*z))) * polylog(3,z*z)
    part1213 = 48*(-1+z)*(5+z*z)*polylog(3,1/(1+z)) - 24*z*(17+z*(8+5*z))*zeta3
    part1 = part11*(part121+part122+part123+part124+part125+part126+part127+part128+part129+part1210+part1211+part1212+part1213)
    #part2
    part21 = cf*ca/(216*(-1+z*z))
    part221 = -1442 + 2*z*(2621+(2621-721*z)*z) - 18*np.pi*np.pi*(-2+z*(3+z)*(-2+3*z))
    part222 = 72*np.pi*np.pi * (5+z*z) * np.arctanh(z) + 186*np.log(z)
    part2231 = 12*(1+z*z)*np.log(z)*np.log(z)*np.log(z) + 6*(1+z)*np.log(1-z)*np.log(1-z) * (85+(-3*(-2+z)*z+12*(1+z*z)*np.log(z)))
    part2232 = 4*(1+z)*np.log(1-z) * (409 + 9*z*(-4+3*z) + 3*np.log(z)*(59+3*(-6+z)*z+12*np.log(z)))
    part2233 = 3*np.log(z)*np.log(z) * (11 + z*(59 + (65-31*z)*z) + 12*(-1+z)*(-1+3*z*z)*np.log(1 + z))
    part2234 = 2*np.log(z) * (z*(-71 + z*(199+373*z)) - 36*(-1+z)*(5+z*(10+z))*np.log(1+z))
    part2235 = 12*(5+z*z) * (2*(-1+z)*np.log(1+z)*np.log(1+z)*np.log(1+z) - np.pi*np.pi*z*np.log(1-z*z))
    part223 = 3 * (part2231+part2232+part2233+part2234+part2235)
    part224 = 216 * ((1-z)*(5+z*(10+z)) + (-3+z*(-1+z*(-7+3*z))) * np.log(z)) * polylog(2,-z)
    part225 = 36 * (59+47*z-12*z*z + 6*(1+z)*(1+z*z)*np.log(1-z) - 12*(-1+(-2+z)*z)*np.log(z)) * polylog(2,z)
    part226 = 1080*(1+z)*(1+z*z)*polylog(3,1-z) + 216*(-7+z*(-5+z*(-7+3*z))) * polylog(3,z)
    part227 = 54*(7+z+(11-3*z)*z*z) * polylog(3,z*z) - 432*(-1+z)*(5+z*z) * polylog(3,1/(1+z))
    part228 = -216*(12 - 5*z + 3*z*z*z)*zeta3
    part2 = part21*(part221+part222+part223+part224+part225+part226+part227+part228)
    #part3
    part31 = cf*nf*Tf/(54*(1-z))
    part321 = 38*(1+z*(6+z)) + 144*np.log(1-z)*np.log(1-z) + 6*(5+z*(12+5*z))*np.log(z)
    part322 = 9*(1+z*z)*np.log(z)*np.log(z) + 48*np.log(1-z)*(8+3*np.log(z)) + 144*polylog(2,z)
    part3 = part31*(part321+part322)
    return part1 + part2 + part3
def PqqPqqL1(x):
    if x>0 and x<1:
        part1 = 1/(12*(-1 + x))
        part21 = -27 + 8*np.pi*np.pi*(1+x*x) + 168*x*x*np.arctanh(1-2*x) - 24 * (7+3*x*x)*np.log(1-x)*np.log(1-x)
        part22 = -12*np.log(1-x) * (5 + 20*x + 4*(-1+x*x)*np.log(x))
        part23 = 6*np.log(x) * (-2+4*x+(3+9*x*x)*np.log(x)) + 12*(3-7*x*x)*polylog(2,x)
        return part1 * (part21+part22+part23)
    else:
        part1 = 1/(4*(-1 + x))
        part21 = 9 + 8*(1+x*(4+x)) * arccoth(1-2*x)
        part22 = -16*(1+x*x)*polylog(2,1/(1-x)) - 4*(1+3*x*x) * polylog(2,1/x)
        rest = part1*(part21+part22)
        if x>1:
            return rest
        elif x<0:
            return -rest
def PqqL1(x):
    if x>0 and x<1:
        return -(1 + 6*x - 4*x*x + 2*(5+x*x)*np.log(1-x) + 2*(1+x*x)*np.log(x))/(2*(-1+x))
    else:
        rest = (1 + 2*x + 4*(1+x*x)*arccoth(1-2*x))/(2*(-1+x))
        if x>1:
            return rest
        elif x<0:
            return -rest
def PFL1(x):
    zeta3 = 1.202056903
    if x>0 and x<1:
        part1 = 1/(24*(-1 + x))
        part21 = -171 + 720*x - 480*x*x - 4*np.pi*np.pi*(1+2*x) + 120*np.log(x)
        part22 = -24*(-7+x*x) * np.log(1-x)*np.log(1-x) * np.log(x)
        part23 = 4*np.log(x) * (6*x*(-16+11*x) + np.log(x)*(27-12*(-1+x)*x - 5*(-1+x*x)*np.log(x)))
        part24 = 8*np.log(1-x) * ((-1+x)*(27*(-1+x)+2*np.pi*np.pi*(1+x)) + 3*np.log(x)*(2-4*(-2+x)*x + 3*(1+x*x)*np.log(x)))
        part25 = -24 * (1 - 2*x*(1+x) + 8*x*x*np.arctanh(1-2*x) - 4*np.log((1-x)*x)) * polylog(2,x)
        part26 = 48*(1-3*x*x)*polylog(3,1-x) - 24*(7+5*x*x)*polylog(3,x) + 48*(1+3*x*x)*zeta3
        return part1 * (part21+part22+part23+part24+part25+part26)
    else:
        part1 = 1/(24*(-1 + x))
        part21 = 171 + 4*np.pi*np.pi + 8*(-30+np.pi*np.pi)*x
        part22 = 8*np.log((-1+x)/x) * (-15*(-1+x)*(-1+x) + np.pi*np.pi*(1+x*x) + (1+x*x)*np.log((-1+x)/x)*np.log((-1+x)/x))
        part23 = 24 * (-3 + 2*(-1+x)*x) * polylog(2,1/x)
        part24 = -48*(1+x*x)*polylog(3,1/(1-x)) + 24*(3+x*x)*polylog(3,1/x)
        rest = part1 * (part21 + part22 + part23 + part24)
        if x>1:
            return rest
        elif x<0:
            return -rest
def PAL1(x):
    zeta3 = 1.202056903
    if x>0 and x<1:
        part1 = 1/(18*(-1 + x))
        part21 = 296 + 380*x*(-3+2*x) + 3*np.pi*np.pi*(1+6*x-4*x*x) - 374*np.log(x)
        part22 = np.log(x) * (2*(246-193*x)*x - 9*(17+x*(4+x))*np.log(x) - 30*(1+x*x)*np.log(x)*np.log(x))
        part23 = np.log(1-x) * (-910 + 24*np.pi*np.pi + 480*x - 374*x*x - 24*np.log(x)*(11+3*np.log(x)))
        part24 = 6*np.pi*np.pi * (1+x*x) * np.log((1-x)*x) + 6*(-27+5*x*x-24*np.log(x)) * polylog(2,x)
        part25 = 36*(5+x*x)*polylog(3,x) - 72*(1+x*x)*zeta3
        return part1 * (part21 + part22 + part23 + part24 + part25)
    else:
        part1 = 1/(18*(-1 + x))
        part21 = -3*np.pi*np.pi*(1+2*x) + 4*(-74+95*x) - 4*(-187+(240-187*x)*x+3*np.pi*np.pi*(1+x*x))*arccoth(1-2*x)
        part22 = 6*(17+5*x*x)*polylog(2,1/x) - 36*(1+x*x)*polylog(3,1/x)
        rest = part1 * (part21 + part22)
        if x>1:
            return rest
        elif x<0:
            return -rest
def PnfL1(x):
    if x>0 and x<1:
        part1 = 1/(18*(-1 + x))
        part21 = -29 + 32*(3-2*x)*x + 44*x*x*np.log(-1+1/x)
        part22 = np.log(1-x) * (124 - 48*x + 48*np.log(x))
        part23 = 2*np.log(x)*(22 + 4*x*(-3+8*x) + 9*(1+x*x)*np.log(x)) - 12*(-3+x*x)*polylog(2,x)
        return part1 * (part21+part22+part23)
    else:
        part1 = 1/(18*(-1 + x))
        part2 = -29 + 32*x + (44-48*x+44*x*x)*np.log((-1+x)/x) + 12*(1+x*x)*polylog(2,1/x)
        rest = -part1*part2
        if x>1:
            return rest
        elif x<0:
            return -rest
def PbarL1(x):
    zeta3 = 1.202056903
    if x>-1 and x<0:
        #part1
        part11 = 8*(1+x*x) * (polylog(2,-x)-polylog(2,x)-polylog(2,2*x/(-1+x))+polylog(2,(1+x)/2))
        part1 = part11 / (-1+x) * np.arctanh(x)
        #part2
        part2 = 2 * (-1 + x) * polylog(2,-x)
        #part3
        part31 = 4 * (-2*(-1+x)*(-1+x) + np.log(-x*(1+x)) + x*x*np.log(-x*x*x*(1+x))) * polylog(2,x)
        part3 = part31 / (-1+x)
        #part4
        part41 = -2/(1 - x)
        part421 = -2*(7+3*x*x) * polylog(3,1/(1-x)) - (3+7*x*x)*polylog(3,x)
        part422 = (1+x*x)/4 * (polylog(3,x*x) - 8*polylog(3,1+x) + 4*polylog(3,1-x*x))
        part4 = part41*(part421+part422)
        #part5
        part51 = 1/(12*(-1 + x))
        part521 = 135 - 10*np.pi*np.pi + 96*x + 12*np.pi*np.pi*x - 8*(24+np.pi*np.pi)*x*x + 48*(1+x*x)*np.arctanh(x)*np.log(2)*np.log(2)
        part522 = -36*np.pi*np.pi*np.log(1-x) - 48*np.log(-x*(1+x))
        part5231 = 4*(5+3*x*x)*np.log(1-x)*np.log(1-x)*np.log(1-x) - 12*x*np.log(x*x) + np.pi*np.pi*np.log(-x) + np.pi*np.pi*x*x*np.log(-x)
        part5232 = -9*np.log(-x)*np.log(-x) + 12*x*np.log(-x)*np.log(-x) - (3*x*x+12*np.log(2))*np.log(-x)*np.log(-x)
        part5233 = -12*x*x*np.log(2)*np.log(-x)*np.log(-x) - (8+8*x*x)*np.log(-x)*np.log(-x)*np.log(-x)
        part5234 = 12*(1+x*x)*np.arctanh(1+2*x) * (np.log(2)*np.log(2) - np.log(2*x/(-1 + x))*np.log(2*x/(-1 + x)))
        part52351 = -5*np.pi*np.pi*x*x - (1+x*x)*12*np.log(x*x) + 6*(1+x*x)*np.log(4)*np.log(-1/x)
        part52352 = 6*np.log(-x) * (np.log(-16*x*x*x) + x*(8+x*np.log(-16*x*x*x*x*x)))
        part5235 = np.log(1-x) * (part52351 + part52352)
        part5236 = (x*x)*18*np.log(x*x) + (1+x*x)*np.log(1-x)*np.log(1-x)*(-3*np.log(x*x)-12*np.log(1+x)) + 12*x*x*np.log(1+x)
        part5237 = 12*np.log(2)*np.log(-x)*np.log(1+x) + 12*x*x*np.log(2)*np.log(-x)*np.log(1+x)
        part5238 = 9*np.log(-x)*np.log(-x)*np.log(1+x) + 9*x*x*np.log(-x)*np.log(-x)*np.log(1+x) + 12*np.log((1-x)/2)*np.log(1+x)*np.log(1+x)
        part5239 = 12*x*x*np.log((1-x)/2)*np.log(1+x)*np.log(1+x) + 3*(1+x*x)*np.log(-1/x)*np.log(-x)*np.log(-x*(1+x))
        part523 = 4*(part5231+part5232+part5233+part5234+part5235+part5236+part5237+part5238+part5239)
        part524 = 48*(6+x*x)*zeta3
        part5 = part51 * (part521+part522+part523+part524)
        return part1 + part2 + part3 + part4 + part5
    elif x>0:
        #part1
        part1 = -8 + np.pi*np.pi/3 + 4*(1+x)*np.log(1+1/x) - 2*(-1+x)*polylog(2,-1/x)
        #part2
        part2 = (-13 + (2*np.pi*np.pi - 8*zeta3)) / (4 * np.abs(-1+x))
        #part3
        part31 = (1 + x*x) /(-1 + x)
        part321 = -1/3*np.pi*np.pi*np.log(x) - 4/3*np.log(x)*np.log(x)*np.log(x) + 2/3*np.pi*np.pi*np.log(1+x) + 4*np.log(x)*np.log(1+x)*np.log(1+x)
        part322 = -2/3*np.log(1+x)*np.log(1+x)*np.log(1+x) - 4*np.log(x)*polylog(2,(-1+x)/x)
        part323 = 4*np.log(1+x) * (polylog(2,-x) + polylog(2,(-1+x)/(1+x)) - polylog(2,-1+2/(1+x)))
        part324 = 2*polylog(3,-1/x) - 4*polylog(3,(-1+x)/x) + 4*polylog(3,1/(1+x))
        part325 = 4*polylog(3,(-1+x)/(1+x)) - 4*polylog(3,-1 + 2/(1 + x)) - 3*zeta3
        part3 = part31 * (part321 + part322 + part323 + part324 + part325)
        #part4
        part4 = 2*zeta3/(-1+x)
        return part1 + part2 + part3 + part4
    else:
        #part1
        part11 = 6 + 1/(2-2*x) - 8*x*arccoth(1+2*x)
        part12 = -(-15 + np.pi*np.pi*(1+2*x) + 24*x*(-1+np.log(4)))/(6*(-1+x))
        part13 = (-3+8*x*(-1+np.log(16)))/(4*(-1+x)) - 4*np.log(1+1/x) + 2*(-1+x)*polylog(2,-1/x)
        part1 = part11 + part12 + part13
        #part2
        part21 = -(1+x*x)/(6*(-1+x))
        part221 = 8*np.log(2)*np.log(2)*np.log(2)
        part2221 = np.pi*np.pi - 6*np.log(2)*np.log(2) + 6*np.log(1-x)*np.log(1-x)
        part2222 = -6*np.log(-1/x)*np.log(-x/(-1+x)/(-1+x)) - 6*np.log(2*x/(-1+x))*np.log(2*x/(-1+x))
        part222 = 2 * np.log((-1+x)/x) * (part2221+part2222)
        part223 = 24*np.log((-1+x)/x) * (polylog(2,1/(1-x)) + polylog(2,-1/x))
        part224 = 24*np.log(2*x/(-1+x)) * (polylog(2,-2/(-1+x)) + polylog(2,(-1+x)/(2*x)))
        part225 = 24*polylog(3,1/(1-x)) - 24*polylog(3,-2/(-1+x)) + 24*polylog(3,(-1+x)/(2*x)) - 21*zeta3
        part2 = part21 * (part221+part222+part223+part224+part225)
        #part3
        part31 = -(1+x*x)/(2*(-1+x))
        part321 = 4*np.log(-x)*(np.log(1+1/x)*np.log(x/(1+x)) + np.log(2*x/(1+x))*np.log(2*x/(1+x)))
        part322 = 16 * arccoth(1+2*x) * polylog(2,1+1/x)
        part323 = 8*np.log(2*x/(1+x)) * (-polylog(2,(1+x)/2) + polylog(2,(1+x)/(2*x)))
        part324 = -8*polylog(3,1+1/x) - 4*polylog(3,-1/x) + 8*polylog(3,x)
        part325 = -8*polylog(3,(1+x)/2) + 8*polylog(3,(1+x)/(2*x)) + zeta3
        part32 = part321+part322+part323+part324+part325
        part3 = part31 * part32
        return part1 + part2 + part3
#print(PbarL1(2.2))
def PqqR1L1(x):
    zeta3 = 1.202056903
    if x>0 and x<1:
        part1 = 1/(12*(-1+x))
        part21 = -3 + 48*(3-2*x)*x + 4*np.pi*np.pi*(-1+6*(-2+x)*x) + 36*np.log(x)
        part221 = 4*(37+5*x*x)*np.log(1-x)**3 + 6*np.log(1-x)*np.log(1-x)*(13+(2*(16-5*x)*x+6*(-3+x*x)*np.log(x)))
        part222 = np.log(x) * (6*(8-3*x)*x - 4*np.pi*np.pi*(1+x*x) + 9*(1+x*(-8+3*x))*np.log(x))
        part223 = -6*np.log(1-x) * (-27 + (14-11*x)*x + 2*np.pi*np.pi*(1+x*x) + 2*np.log(x)*(3+(2*(-4+x)*x+6*np.log(x))))
        part22 = 2*(part221+part222+part223)
        part23 = 12 * (-7 + (32-15*x)*x + 12*(-1+x*x) * np.log(1-x) + 4*(-5+x*x)*np.log(x)) * polylog(2,x)
        part24 = 96*(-3+x*x)*polylog(3,1-x) + 288*polylog(3,x) + 384*zeta3
        return part1*(part21+part22+part23+part24)
    elif x>1:
        part1 = 1/(12*(-1+x))
        part21 = 3 - 48*x - 8*(9+6*x+9*x*x+4*np.pi*np.pi*(1+x*x)) * arccoth(1-2*x)
        part22 = -4*(3 - 6*(-4+x)*x + 2*(5+x*x)*np.log(-1+x) + 2*(1+5*x*x)*np.log(x))*np.log(x/(-1+x))*np.log(x/(-1+x))
        part23 = 12*(-3 + (-8+x)*x + 8*(-1+x*x)*arccoth(1-2*x)) * polylog(2,1/x)
        part24 = 96*polylog(3,1/x) + 96*(1+x*x)*polylog(3,(-1+x)/x) - 96*(1+x*x)*zeta3
        return part1*(part21+part22+part23+part24)
    else:
        part1 = 1/(12*(-1+x))
        part21 = -3 + 48*x + 2*np.pi*np.pi*(-3+(-8+x)*x) + 16*np.pi*np.pi*x*x*np.log(1-x)
        part22 = 6*(-3+(-8+x)*x)*np.log(1-x)*np.log(1-x) + 16*x*x*np.log(1-x)**3
        part231 = 9 + 6*x + 9*x*x - 2*np.pi*np.pi*(1+x*x) + 9*np.log(1-x) - 3*(-8+x)*x*np.log(1-x)
        part232 = -6*(1+x*x)*np.log(1-x)*np.log(1-x) + 3*arccoth(1-2*x)*(-1+(8-3*x)*x+8*np.log(1-x))
        part23 = 8*arccoth(1-2*x) * (part231+part232)
        part241 = (-3 + (-8+x)*x + 8*(-1+x*x) * arccoth(1-2*x)) * polylog(2,x)
        part242 = 8*(1+x*x)*polylog(3,1/(1-x)) + 8*x*x*polylog(3,x)
        part24 = 12 * (part241 + part242)
        return part1 * (part21+part22+part23+part24)
def R1L1(x):
    if x>0 and x<1:
        part1 = 1/(2*(-1+x))
        part21 = 7 - 6*x + 4*x*x + 6*np.log(1-x) + 20*np.log(1-x)*np.log(1-x)
        part22 = -2*np.log(x) - 2*(-4+x)*x*np.log((1-x)*x) - 8*polylog(2,1-x)
        return part1 * (part21 + part22)
    else:
        rest = (-7 + 2*x + 2*(1+(-4+x)*x)*np.log((-1+x)/x) + 8*polylog(2,1/(1-x)))/(2*(-1+x))
        if x>1:
            return rest
        elif x<0:
            return -rest
def PqqPqqL2(x):
    zeta3 = 1.202056903
    if x>0 and x<1:
        part1 = -1/(12*(1 - x))
        part21 = 12 - 36*x + 24*x*x + 4*np.pi*np.pi*(1-2*(-2+x)*x) - 16*(7+2*x*x)*np.log(1-x)*np.log(1-x)*np.log(1-x)
        part22 = -6*np.log(1-x)*np.log(1-x) * (7 + (28-11*x)*x + (-7+3*x*x)* np.log(x))
        part23 = np.log(1-x) * (-123 - 96*(-2+x)*x + np.pi*np.pi*(2+6*x*x) - 24*np.log(x)*(1-2*(-2+x)*x + (-2+x*x)*np.log(x)))
        part24 = 2*np.log(x) * (36*(-1+x)*x + np.log(x)*(-3+15*(1-2*x)*x+4*(1+3*x*x)*np.log(x)))
        part25 = -12 * (2 + (11-7*x)*x + 3*(-1+x*x)*np.log(1-x) + 4*(-2+x*x)*np.log(x)) * polylog(2,x)
        part26 = 12*(11+x*x)*polylog(3,1-x) + 12*(-9+x*x)*polylog(3,x) - 24*(7+x*x)*zeta3
        return part1 * (part21+part22+part23+part24+part25+part26)
    elif x>1:
        part1 = -1/(12*(1 - x))
        part21 = -4*(-1+x)*(-3+2*np.pi*np.pi*x) + 16*(2+x*x)*np.log(-1+x)*np.log(-1+x)*np.log(-1+x)
        part22 = 2*np.log(x)*np.log(x) * (3 - 3*x*x + np.log(x) + 11*x*x*np.log(x))
        part23 = 6*np.log(-1+x)*np.log(-1+x) * (3 - 3*(-4+x)*x + (-7+3*x*x)*np.log(x))
        part24 = np.log(-1+x) * (27 + 2*np.pi*np.pi*(5+7*x*x) - 6*np.log(x)*(4+8*x+(-3+7*x*x)* np.log(x)))
        part25 = -48 * (-1 + x) * x * polylog(2,1-x)
        part26 = 12 * (2 + x + 3*x*x - 3*(-1+x*x)*np.log(-1+x) + 4*x*x*np.log(x)) * polylog(2,1/x)
        part27 = 12 * (5+7*x*x) * polylog(3,1-x) + 12 * (1+7*x*x) * polylog(3,1/x)
        return part1 * (part21+part22+part23+part24+part25+part26+part27)
    else:
        part1 = -1/(12*(1 - x))
        part21 = -6 * (7 + 5*x*x) * np.log(1-x)*np.log(1-x)*np.log(1-x)
        part22 = np.log(1-x) * (-27 + 8*np.pi*np.pi*(2+x*x) + 24*np.log(-x)*(1-2*(-2+x)*x+x*x*np.log(-x)))
        part23 = np.log(1-x)*np.log(1-x) * (-18 + 18*(-4+x)*x + 12*x*x*np.log(x*x) + 36*np.log(x*x))
        part24 = -12 * (-2 + (-5+x)*x + 3*(-1+x*x)*np.log(1-x) - 2*x*x*np.log(x*x)) * polylog(2,x)
        part25 = 48*polylog(3,x) + 12*(5+7*x*x)*polylog(3,x/(-1+x)) - 84*x*x*zeta3
        part261 = -6 + 6*x + np.pi*np.pi*(-2+(-5+x)*x) + np.pi*np.pi*(1+3*x*x)*np.log(-x)
        part262 = np.log(-x)*np.log(-x) * (-3*(1+x+4*x*x) + np.log(x*x) + 3*x*x*np.log(x*x)) + 30*zeta3
        part26 = -2 * (part261+part262)
        return part1 * (part21+part22+part23+part24+part25+part26)
def PqqL2(x):
    if x>0 and x <1:
        part1 = -1/(2*(-1 + x))
        part21 = 2 - 6*x + 4*x*x + np.log(1-x)*(1 - 2*(-2+x)*x + (7+x*x)*np.log(1-x))
        part22 = -2*(-1+x)*x*np.log(x) + (1+x*x)*np.log(x)*np.log(x) - 4*polylog(2,1-x)
        return part1 * (part21 + part22)
    elif x>1:
        part1 = 1/(2*(-1 + x))
        part21 = 2 - 2*x - 4*x*x*arccoth(1-2*x) + (3+x*x)*np.log(-1+x)*np.log(-1+x) + np.log(-1+x)*(1+4*x-4*np.log(x))
        part22 = np.log(x) * (-2*x + np.log(x) - x*x*np.log(x)) + 4*polylog(2,1/x)
        return part1 * (part21 + part22)
    else:
        part1 = 1/(6*(-1 + x))
        part21 = -6 + 2*np.pi*np.pi + 6*x - 3*np.log(1-x) - 6*x*np.log(1-x) + 6*np.log(1-x)*np.log(1-x)
        part22 = 12*arccoth(1-2*x) * ((-1+x)*x + (1+x*x)*arccoth(1-2*x) - (3+x*x)*np.log(1-x))
        part23 = 12*polylog(2,x)
        return part1 * (part21 + part22 + part23)
@nb.njit
def PqqPqqLY(x):
    if x>0 and x <1:
        return np.log(x) * (-1 - 4*x - x*x - 4*(1+x*x)*np.log(1-x) + np.log(x) + 3*x*x* np.log(x)) / (2*(-1+x))
    else:
        return 0
def PqqPqqLYL1(x):
    zeta3 = 1.202056903
    if x>0 and x<1:
        part1 = 1/(12*(-1 + x))
        part21 = 6 - 18*x + 12*x*x + 2*np.pi*np.pi*(1+2*x) + 12*(-7+x*x)*np.log(1-x)*np.log(1-x)*np.log(x)
        part22 = np.log(x) * (36*(-1+x)*x - 9*(1+5*x*x)*np.log(x) + 10*(1+3*x*x)*np.log(x)*np.log(x))
        part231 = 2*(-1+x) * (-6 + np.pi*np.pi + (6+np.pi*np.pi)*x)
        part232 = 3*np.log(x) * (2 - 4*(-2+x)*x + (-1+3*x*x)*np.log(x))
        part23 = -4*np.log(1-x)*(part231+part232)
        part24 = 6*(-3 + (-4+x)*x + 16*(-1+x*x)*np.arctanh(1-2*x)) * polylog(2,x)
        part25 = 24*(-1+3*x*x)*polylog(3,1-x) + 36*(-1+x*x)*polylog(3,x) - 24*(-1+x*x)*zeta3
        return part1 * (part21 + part22 + part23 + part24 + part25)
    elif x>1:
        part1 = 1/(6*(-1 + x))
        part21 = -3 + 3*x - np.pi*np.pi*(1+2*x) + 6*(1+x*x)*np.log(x)*np.log(x/(-1+x))*np.log(x/(-1+x))
        part22 = 3*(1+x*(4+x))*polylog(2,1/x) - 6*(3+x*x)*polylog(3,1/x)
        part23 = -12*(1+x*x)*polylog(3,(-1+x)/x) + 12*(1+x*x)*zeta3
        return part1 * (part21 + part22 + part23)
    else:
        part1 = -1/(6*(-1 + x))
        part21 = 3 + np.pi*np.pi - 3*x + 2*np.pi*np.pi*x
        part221 = np.pi*np.pi * (1+3*x*x)
        part222 = arccoth(1-2*x) * (3 + 3*x*(4+x) + 4*(1+3*x*x)*arccoth(1-2*x) - 6*(-1+x*x)*np.log(1-x))
        part22 = 2*arccoth(1-2*x) * (part221+part222)
        part23 = 3*(1+x*(4+x))*polylog(2,1/(1-x)) - 6*(3+x*x)*polylog(3,1/(1-x))
        part24 = 6 *(-1+x*x)*polylog(3,x/(-1+x)) - 6*(-1+x*x)*zeta3
        return part1 * (part21 + part22 + part23 + part24)
def PqqPqqLYR1(x):
    zeta3 = 1.202056903
    if x>0 and x<1:
        part1 = 1/(6*(-1 + x))
        part21 = 4*np.pi*np.pi*(-1+x) - 8*np.log(1-x)*np.log(1-x)*np.log(1-x) + 24*(2+x*x)*np.log(1-x)*np.log(1-x)*np.log(x)
        part22 = 3*np.log(x) * (-1 + 7*x*x + np.log(x) + x*(-8+3*x)*np.log(x))
        part23 = -4*np.log(1-x) * (6*(-1+x)*(-1+x) + np.pi*np.pi*(1+x*x) + 3*np.log(x)*(-2+(-4+x)*x+2*np.log(x)))
        part24 = 24 * (1 - x - 2*np.arctanh(1-2*x) + x*x*np.log((1-x)*x)) * polylog(2,x)
        part25 = 24 * (1+x*x)*polylog(3,1-x) - 24 * (1+x*x) * polylog(3,x)
        part26 = 48 * polylog(3,x/(-1+x)) + 24 * (1+x*x) * zeta3
        return part1 * (part21 + part22 + part23 + part24 + part25 + part26)
    else:
        return 0
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
#--Ratio Scheme Matching for the Valence Case--#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def Cratio1(x,mu,pz):
    if x>0 and x<1:
        return -Pqq(x) * np.log(mu*mu/(4*pz*pz)) + Pqq(x) * (np.log(x*(1-x))-1) + 1 + 3/(2*(1-x))
    else:
        #print(x,'\n',2*(1-x))
        rest = Pqq(x)*np.log(x/(x-1)) + 1 - 3/(2*(1-x))
        if x>1:
            return rest
        elif x<0:
            return -rest

def Cratio2Log(y,mu,pz):
    L = np.log(mu*mu/(4*pz*pz))
    part1 = 0
    if y*(1 - y) > 0:
        part11 = 1/2 * (cf*cf*PqqPqq(y) - cf*Pqq(y)*b0) * L*L
        part12 = (-Pm(y) - cf*cf*(PqqR1(y)-5/2*Pqq(y)) + cf*b0*R1(y)) * L
        part1 = part11 + part12
    part2 = (-cf*cf*PqqPqqL1(y) + cf*b0*PqqL1(y)) * L
    return part1 + part2

def Cratio2(y):
    part1 = 0
    if y*(1 - y) > 0:
        part1 = R2(y) -cf*cf*5/2*R1(y) + np.pi*np.pi/6*(cf*cf*PqqPqq(y) -b0*cf*Pqq(y))
    part21 = cf*cf*PFL1(y) + 1/2*cf*ca*PAL1(y) + cf*nf*Tf*PnfL1(y) - (cf*cf-cf*ca/2)*PbarL1(-y)
    part22 = cf*cf*PqqR1L1(y) - cf*b0*R1L1(y) + 2*cf*cf*PqqPqqL2(y) - 2*cf*b0*PqqL2(y)
    return part1 + part21 + part22

def CR1R1(x, mu, pz):
    L = np.log(mu*mu/(4*pz*pz))

    part1 = 0
    if x>0 and x<1:
        part11 = PqqPqq(x)*L*L - 2*(PqqR1(x)-5/2*Pqq(x))*L + np.pi*np.pi/3*PqqPqq(x)
        part12 = R1R1(x) + 2*(PqqPqqLY(x)*L -PqqPqqLYR1(x))
        part1 = part11 + part12
    part2 = -2*PqqPqqL1(x)*L + 4*PqqPqqL2(x) + 2*PqqR1L1(x) - 2*PqqPqqLYL1(x)
    #print(x,PqqPqqLYR1(x))
    return part1+part2

# PDF(x), qPDF(y), return a len(x)*len(y) matrix
# The non-diagonal terms are for: \int dy/|y| C(x/y, mu/yPz)qPDF(y)
# The "diagonal" (x[i]==y[j]) terms are for: N(xPz)qPDF(x)
def simpson(func, a, b):
    mid = (a + b) / 2
    return (func(a) + 4*func(mid) + func(b)) * (b-a) / 6
def rectangular(func, a, b):
    return func(a) * (b-a)

xtest = 0.2
def test(y):
    return -1/abs(xtest)*Cratio1(y/xtest, 100, xtest*10)

#print(test(-3-0.0001))

def my_integral(L, R, eps, x):
    mid = (L+R)/2
    ST = rectangular(test, L, R)
    SL = rectangular(test, L, mid)
    SR = rectangular(test, mid, R)
    if abs((SL+SR-ST)) <= eps or abs(R) < eps/10 or abs(L) < eps/10 or abs(R-x) < eps/10 or abs(L-x) < eps/10:
        return SL+SR
    else:
        return my_integral(L, mid, eps,x) + my_integral(mid, R, eps,x)
#eps = 0.00001
#print('my',my_integral(-3-eps, 3-eps, eps,xtest))

def MR1_diag(x, y, alphas, mu, Pz):

    cst = alphas*cf/(2*np.pi)

    MRMdiagonal = np.zeros(len(x))
    for ix in range(0, len(x)):
        #start0 = time.time()
        for iy in range(0, len(y)):
            if y[iy] != x[ix]:
                MRMdiagonal[ix] += -1/abs(x[ix])*Cratio1(y[iy]/x[ix], mu, x[ix]*Pz)

    return cst*MRMdiagonal
def MR1_nondiag(x, y, alphas, mu, Pz):

    cst = alphas*cf/(2*np.pi)
    
    MRMatrix = np.zeros((len(x), len(y)))
    for ix in range(0, len(x)):
        #start0 = time.time()
        for iy in range(0, len(y)):
            if x[ix] == y[iy]:
                MRMatrix[ix][iy] = 0
            else:
                MRMatrix[ix][iy] = cst*1/abs(y[iy])*Cratio1(x[ix]/y[iy], mu, y[iy]*Pz)
        #print('Timing diagonal',ix,':',time.time()-start0)

    return MRMatrix

def MR1(x, y, alphas, mu, Pz):

    cst = alphas*cf/(2*np.pi)

    MRMdiagonal = np.zeros(len(x))
    for ix in range(0, len(x)):
        #start0 = time.time()
        for iy in range(0, len(y)):
            if y[iy] != x[ix]:
                MRMdiagonal[ix] += -1/abs(x[ix])*Cratio1(y[iy]/x[ix], mu, x[ix]*Pz)
        #print('Timing diagonal',ix,':',time.time()-start0)
    
    MRMatrix = np.zeros((len(x), len(y)))
    for ix in range(0, len(x)):
        #start0 = time.time()
        for iy in range(0, len(y)):
            if x[ix] == y[iy]:
                MRMatrix[ix][iy] = cst*MRMdiagonal[ix]
            else:
                MRMatrix[ix][iy] = cst*1/abs(y[iy])*Cratio1(x[ix]/y[iy], mu, y[iy]*Pz)
        #print('Timing diagonal',ix,':',time.time()-start0)

    return MRMatrix

def MR2(x, y, alphas, mu, Pz):

    cst = alphas/(2*np.pi)
    cst = cst*cst

    MRMdiagonal = np.zeros(len(x))
    for ix in range(0, len(x)):
        start0 = time.time()
        for iy in range(0, len(y)):
            if y[iy] != x[ix]:
                MRMdiagonal[ix] += -1/abs(x[ix])*(Cratio2Log(y[iy]/x[ix], mu, x[ix]*Pz)+Cratio2(y[iy]/x[ix]))
        #print('Timing diagonal',ix,':',time.time()-start0)
    
    MRMatrix = np.zeros((len(x), len(y)))
    for ix in range(0, len(x)):
        start0 = time.time()
        for iy in range(0, len(y)):
            if x[ix] == y[iy]:
                MRMatrix[ix][iy] = cst*MRMdiagonal[ix]
            else:
                MRMatrix[ix][iy] = cst*1/abs(y[iy])*(Cratio2Log(x[ix]/y[iy], mu, y[iy]*Pz)+Cratio2(x[ix]/y[iy]))
        #print('Timing non-diagonal',ix,':',time.time()-start0)
    
    return MRMatrix

def MR1R1(x, y, alphas, mu, Pz):

    cst = alphas*cf/(2*np.pi)
    cst = cst*cst

    MRMdiagonal = np.zeros(len(x))
    for ix in range(0, len(x)):
        start0 = time.time()
        yarray = []
        for iy in range(0, len(y)):
            if y[iy] != x[ix]:
                yarray += [y[iy]]
        def kernel(r):
            return CR1R1(r/x[ix], mu, x[ix]*Pz)
        temp = np.array(list(map(kernel, yarray)))
        MRMdiagonal[ix] = -1/abs(x[ix])*np.sum(temp)
        #print(MRMdiagonal[ix])
        #print('Timing diagonal',ix,':',time.time()-start0)
    
    MRMatrix = np.zeros((len(x), len(y)))
    for ix in range(0, len(x)):
        start0 = time.time()
        for iy in range(0, len(y)):
            if x[ix] == y[iy]:
                MRMatrix[ix][iy] = cst*MRMdiagonal[ix]
            else:
                MRMatrix[ix][iy] = cst*1/abs(y[iy])*CR1R1(x[ix]/y[iy], mu, y[iy]*Pz)
        #print('Timing non-diagonal',ix,':',time.time()-start0)

    return MRMatrix

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

# Double subdivide the array.
@nb.njit
def subdivide(y):
    new_y = np.empty(1)
    new_y[0] = y[0]
    for i in range(1, len(y)):
        new_y = np.append(new_y, (y[i]+y[i-1])/2)
        new_y = np.append(new_y, y[i])
    return new_y

# x is supposed to have 1 igredient only.
def adoptive_y(x,y,MR_diag,MR_nondiag, epsilon = 0.01):

    subep = 0.01/len(y)
    MR = MR_diag(x,y)
    MRsum_diag = np.array(sum(MR_diag))*(y[-1]-y[0])/len(y)

    for iy in range(0, len(y)-1):
        diff = 1
        count = 1
        while diff > epsilon and count < 10:
            new_y = y[0:iy+1] + subdivide(y) 
            new_MR = MR_diag(x, new_y)
            new_MRsum = np.array([sum(new_MR[i]) for i in range(0, len(new_MR))])*(y[-1]-y[0])/len(new_y)
            diff = sum(new_MRsum - MRsum)/sum(new_MRsum)
            print(len(new_y),diff)
            print(new_MRsum, MRsum)
            count += 1
            y = new_y
            MRsum = new_MRsum
    return new_y, new_MR

def pick_logpoints_R(xmin, xmax, nx):
    xlist = []
    for i in range(0, nx):
        ix = (i+1)/(nx+1)
        xlist += [np.log(ix)/np.log(nx)+1]
    xlist = [(xlist[i])*(xmax-xmin)+xmin for i in range(0, len(xlist))]
    return xlist
def pick_logpoints_L(xmin, xmax, nx):
    xlist = []
    for i in range(1, nx):
        ix = (i+1)/(nx+1)
        xlist += [-np.log(ix)/np.log(nx)]
    xlist = [(xlist[i])*(xmax-xmin)+xmin for i in range(0, len(xlist))]
    xlist = sorted(xlist)
    return xlist 
def pick_logpoints_mid(xmin, xmax, nx):
    xlist = pick_logpoints_L(xmin, (xmax+xmin)/2, nx) + pick_logpoints_R((xmax+xmin)/2, xmax, nx)
    return xlist 
#a = pick_logpoints_mid(-3, -1, 10)
#b = [a[i+1]-a[i] for i in range(0, len(a)-1)]
#print(b)

class kernelfunc_list:

    CR1 = MR1
    CR1R1 = MR1R1
    CR2 = MR2

if __name__ == "__main__":

    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()

    line = commands[0].split()
    describe = line[0]
    line = commands[1].split()
    savefolder = line[0]

    eps = None

    for index in range(2,len(commands)):

        line = commands[index].split()
        if 'alphas' == line[0]:
            alphas = float(line[1])
        elif 'muGeV' == line[0]:
            mu = float(line[1])*5.0676896 #fm
            describe = describe + '_mu' + line[1] + 'GeV'
        elif 'pzGeV' == line[0]:
            pz = float(line[1])*5.0676896 #fm
            describe = describe + '_Pz' + line[1] + 'GeV'
        elif 'pdfx' == line[0]:
            xlist = np.array([float(line[i]) for i in range(1, len(line))])
        elif 'kernel' == line[0]:
            kernlfunc = getattr(kernelfunc_list, line[1])
            describe = describe + '_kernel' + line[1]
        elif 'Ny' == line[0] or 'ny' == line[0]:
            Nylist = np.array([int(line[i]) for i in range(1, len(line))])
        elif 'eps' == line[0]:
            eps = float(line[1])
            describe = describe + '_eps' + str(eps)
        elif 'ylimit' == line[0]:
            ymin = float(line[1])
            ymax = float(line[2])

    print(describe)

    for iNy in Nylist:

        iNy_ymv = (ymax - ymin)/float(iNy)

        if eps == None:
            iNy_eps = iNy_ymv
        else:
            iNy_eps = eps
        
        iNy_ymax = ymax - iNy_eps
        iNy_ymin = ymin - iNy_eps
        
        iNy_y = np.array([iNy_ymin + iNy_ymv*i for i in range(0, iNy)])
        iNy_x = np.array([find_nearest(iNy_y, xlist[i]) for i in range(0, len(xlist))])

        savename = savefolder + describe + '_Ny' + str(iNy) + '.txt'
        print(savename)
        #print(y)
        #print(iNy_ymv,iNy_ymin,iNy_ymax, x)
        MRmatrix = kernlfunc(iNy_x, iNy_y, alphas, mu, pz)*iNy_ymv
        np.savetxt(savename, MRmatrix)
    #print(x[0],Ny, MRmatrix[0])
    #print('Timing:',time.time()-start0)
