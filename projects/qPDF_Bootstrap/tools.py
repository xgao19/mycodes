#!/usr/bin/env python3

import os
import sys
import csv
import numpy as np

#----------------------------------------------------#
###################txt to csv data####################
#----------------------------------------------------#
'''
2d list saved as csv file
'''
def savecsv(dataset, savefile, describe=['csv']):
    with open(savefile, 'w') as outfile:
        writer = csv.writer(outfile)
        writer.writerow([describe])
        for raw in dataset:
            writer.writerows([raw])
        outfile.close()

'''
Judge and make a folder
'''
def mkdir(path):

	folder = os.path.exists(path)

	if not folder:                   #judge if folder exits
		os.makedirs(path)            #if folder not exit, then makedirs
		print ("---  new folder...  ---")
		print ("---  OK  ---")
	else:
		print ("---  Result folder exits!  ---")


'''
This part transform the CSV string data file to
a txt float data file in the same folder
'''
def csvtotxt(t_csvfile):
    m_csvdata = open(t_csvfile,'r')
    m_reader = csv.reader(m_csvdata)
    m_txtdata = []
    for m_item in m_reader:
        if m_reader.line_num == 1:           # get rid of the first footnote line
            continue
        m_txtdata.append(list(map(float,m_item)))
    m_csvdata.close()
    m_txtdata = np.array(m_txtdata)

    np.savetxt(t_csvfile+'.txt',m_txtdata)

'''
This part read the CSV string data file and
reform that as a float list
'''
def csvtolist(csvfile):
    csvdata = open(csvfile,'r')
    reader = csv.reader(csvdata)
    listdata = []
    for item in reader:
        if reader.line_num == 1:           # get rid of the first footnote line
            continue
        listdata.append(list(map(float,item)))
    csvdata.close()
    return listdata

'''
This function may be useful when the fitted data is symmtric.
For example, if the dataset ranges in t from 0 to 63, we get 
the average of t and 64-t, then output with t ranges from 0 
to 32.
'''

def folddat(t_dataset):
    m_Nt = len(t_dataset)
    #t_dataset += [t_dataset[0]]
    t_dataset = np.append(t_dataset, [t_dataset[0]], axis=0)
    #print(np.shape(t_dataset))
    temp = np.transpose(t_dataset)
    #print(np.shape(temp),temp[0])
    m_dataset = []
    for m_t in range(int(m_Nt/2)+1):
        m_temparray = list((np.array(t_dataset[m_t]) + np.array(t_dataset[m_Nt-m_t]))/2)
        m_temparray[0] = t_dataset[m_t][0]
        m_dataset.append(list(m_temparray))
    return m_dataset

def folddat_z(data_z1, data_z2, datatype = 'real'):

    #print('!!!',datatype)
    ave_mt = []

    if datatype == 'real':
        for i in range(0, len(data_z1)):
            i_ave_mt = [data_z1[i][0]]
            #print(data_z1[0])
            for j in range(1, len(data_z1[0])):
                i_ave_mt += [(data_z1[i][j]+data_z2[i][j])/2]
            ave_mt += [i_ave_mt]
    elif datatype == 'imag':
        for i in range(0, len(data_z1)):
            i_ave_mt = [data_z1[i][0]]
            for j in range(1, len(data_z1[0])):
                i_ave_mt += [(data_z1[i][j]-data_z2[i][j])/2]
            ave_mt += [i_ave_mt]

    return ave_mt

def fold_z_data(data_z1, data_z2, plus = True):

    #print('!!!',plus)
    ave_mt = []

    if plus == True or plus == '+':
        for i in range(0, len(data_z1)):
            i_ave_mt = [data_z1[i][0]]
            #print(data_z1[i][0])
            for j in range(1, len(data_z1[0])):
                i_ave_mt += [(data_z1[i][j]+data_z2[i][j])/2]
            ave_mt += [i_ave_mt]
    elif plus == '-':
        for i in range(0, len(data_z1)):
            i_ave_mt = [data_z1[i][0]]
            for j in range(1, len(data_z1[0])):
                i_ave_mt += [(data_z1[i][j]-data_z2[i][j])/2]
                #print(data_z1[i][j],data_z2[i][j])
            ave_mt += [i_ave_mt]

    return ave_mt

def construct_proton1(g0_fmt):

    g0_data = csvtolist(g0_fmt)

    g3_fmt = g0_fmt.replace('g0', 'g3')
    g3_fmt = g3_fmt.replace('real', 'imag')
    g3_data = csvtolist(g3_fmt)

    g8_fmt = g0_fmt.replace('g0', 'g8')
    g8_data = csvtolist(g8_fmt)

    g11_fmt = g0_fmt.replace('g0', 'g11')
    g11_fmt = g11_fmt.replace('real', 'imag')
    g11_data = csvtolist(g11_fmt)

    proton1 = []
    for i in range(0, len(g0_data)):
        row = [g0_data[i][0]]
        for j in range(1, len(g0_data[0])):
            row += [g0_data[i][j] - g11_data[i][j] + g8_data[i][j] - g3_data[i][j]]
        proton1 += [row]
     
    #proton1=folddat(proton1)

    savename = g0_fmt.replace('Tg0','1')
    savecsv(proton1, savename)

def construct_proton_unpo(g0_fmt):

    g0_data = csvtolist(g0_fmt)

    g8_fmt = g0_fmt.replace('g0', 'g8')
    g8_data = csvtolist(g8_fmt)

    proton1 = []
    for i in range(0, len(g0_data)):
        row = [g0_data[i][0]]
        for j in range(1, len(g0_data[0])):
            row += [(g0_data[i][j] + g8_data[i][j])/2]
        proton1 += [row]
     
    #proton1=folddat(proton1)

    savename = g0_fmt.replace('Tg0','Unpo')
    savecsv(proton1, savename)


'''
This function is used to change the arragement of 3D list
Exchange the 1st and 2ed label.
'''
def ex12_3d(dataset3d):

    newdata = []
    for i in range(0, len(dataset3d[0])):
        subnewdata = []
        for j in range(0, len(dataset3d)):
            subnewdata += [dataset3d[j][i]] 
        newdata += [subnewdata]
    return newdata
'''
This function is used to change the arragement of 4D list
Exchange the 1st and 2ed label.
'''
def ex12_4d(dataset4d):

    newdata = []
    for i in range(0, len(dataset4d[0])):
        subnewdata = []
        for j in range(0, len(dataset4d)):
            subnewdata += [dataset4d[j][i]] 
        newdata += [subnewdata]
    return newdata

'''
This function is used to transform the 3d array to 2d array.
And each raw start with the 1st and 2ed label for 3d array.
The label1 and label2 both should be array like.
'''
def trans3to2(d3dataset, label1 = None, label2 = None):

    d2dataset = []
    for i in range(0, len(d3dataset)):
        for j in range(0, len(d3dataset[i])):
            if label1 == None:
                raw = list(d3dataset[i][j])
            else:
                raw = [label1[i]] + list(d3dataset[i][j])
            #print([label1[i], j])
            d2dataset += [raw]
    return d2dataset

'''
This function is used to slice the c3pt datasets.
The input dataset is c3data[z][ts][tau][conf].
Here we slice the number of tau in range[1, ts-1]
return c3data[z][ts][tau][conf]
'''
def c3dataslice_sumfit(c3dataset, tslist, taumin = 1):

    for i in range(0, len(tslist)):
        if len(c3dataset[0][i]) < tslist[i]:
            temp = tslist[i]-len(c3dataset[0][i])+1
            if taumin < temp:
                taumin = temp

    c3data = []
    for i in range(0, len(c3dataset)):
        zc3data = []
        for j in range(0, len(c3dataset[i])):
            tszc3data = []
            ts = tslist[j]
            for k in range(taumin, ts-taumin+1):
                tszc3data += [c3dataset[i][j][k]]
            zc3data += [tszc3data]
        c3data += [zc3data]
    return c3data

'''
c3data[z][ts][tau][conf]
normalized by the c3data[0][ts][tau][conf]
'''
def c3data_norm(c3dataset, zlist):

    for i in range(0, len(zlist)):
        z = zlist[i]
        if z == 0:
            z0c3data = c3dataset[i]
            break

    for i in range(0, len(zlist)):
        z = zlist[i]
        if z != 0:
            for j in range(0, len(c3dataset[0])):
                for k in range(0, len(c3dataset[0][0])):
                    for t in range(0, len(c3dataset[0][0][0])):
                        c3dataset[i][j][k][t] /= z0c3data[j][k][t]

    return c3dataset

'''
This function is used to slice the 5d data. [)
'''
def d5slicedelete(d5dataset, drange, ax):

    newd5dataset = []

    if ax == 0:
            newd5dataset = list(d5dataset[:drange[0]]) + list(d5dataset[drange[1]:])

    elif ax == 1:
        for i in range(0, len(d5dataset)):
            id5dataset = d5dataset[i]
            #print(id5dataset[:drange[0]], id5dataset[drange[1]:])
            newd5dataset += [list(id5dataset[:drange[0]]) + list(id5dataset[drange[1]:])]

    elif ax == 2:
        for i in range(0, len(d5dataset)):
            newid5dataset = []
            for j in range(0, len(d5dataset[i])):
                jd5dataset = d5dataset[i][j]
                newid5dataset += [list(jd5dataset[:drange[0]]) + list(jd5dataset[drange[1]:])]
            newd5dataset += [newid5dataset]
    elif ax == 3:
         for i in range(0, len(d5dataset)):
            newid5dataset = []
            for j in range(0, len(d5dataset[i])):
                newjd5dataset = []
                for k in range(0, len(d5dataset[i][j])):
                    kd5dataset = d5dataset[i][j][k]
                    newjd5dataset += [list(kd5dataset[:drange[0]]) + list(kd5dataset[drange[1]:])]
                newid5dataset += [newjd5dataset]
            newd5dataset += [newid5dataset]   
    elif ax == 4:
         for i in range(0, len(d5dataset)):
            newid5dataset = []
            for j in range(0, len(d5dataset[i])):
                newjd5dataset = []
                for k in range(0, len(d5dataset[i][j])):
                    newkd5dataset = []
                    for t in range(0, len(d5dataset[i][j][k])):
                        td5dataset = d5dataset[i][j][k][t]
                        newkd5dataset += [list(td5dataset[:drange[0]]) + list(td5dataset[drange[1]:])]
                    newjd5dataset += [newkd5dataset]
                newid5dataset += [newjd5dataset]
            newd5dataset += [newid5dataset]       
    
    return newd5dataset

'''
This function is used to pick the 5d data.
'''
def d5listpick(d5dataset, picklist, ax):

    newd5dataset = []

    if ax == 0:
        newd5dataset += [d5dataset[0]]
        for pick in picklist:
            pick += 1
            newd5dataset += [d5dataset[pick]]

    elif ax == 1:
        for i in range(0, len(d5dataset)):
            id5dataset = d5dataset[i]
            inewd5dataset = []
            inewd5dataset += [id5dataset[0]]
            for pick in picklist:
                pick += 1
                inewd5dataset += [id5dataset[pick]]
            newd5dataset += inewd5dataset

    elif ax == 2:
        for i in range(0, len(d5dataset)):
            inewd5dataset = []
            for j in range(0, len(d5dataset[i])):
                jd5dataset = d5dataset[i][j]
                jnewd5dataset = []
                jnewd5dataset += [jd5dataset[0]]
                for pick in picklist:
                    pick += 1
                    jnewd5dataset += [jd5dataset[pick]]
                inewd5dataset += [jnewd5dataset]
            newd5dataset += [inewd5dataset]


    elif ax == 3:
         for i in range(0, len(d5dataset)):
            inewd5dataset = []
            for j in range(0, len(d5dataset[i])):
                jnewd5dataset = []
                for k in range(0, len(d5dataset[i][j])):
                    kd5dataset = d5dataset[i][j][k]
                    knewd5dataset = []
                    knewd5dataset += [kd5dataset[0]]
                    for pick in picklist:
                        pick += 1
                        knewd5dataset += [kd5dataset[pick]]
                    jnewd5dataset += [knewd5dataset]
                inewd5dataset += [jnewd5dataset]
            newd5dataset += [inewd5dataset]   

    elif ax == 4:
         for i in range(0, len(d5dataset)):
            inewd5dataset = []
            for j in range(0, len(d5dataset[i])):
                jnewd5dataset = []
                for k in range(0, len(d5dataset[i][j])):
                    knewd5dataset = []
                    for t in range(0, len(d5dataset[i][j][k])):
                        td5dataset = d5dataset[i][j][k][t]
                        tnewd5dataset = []
                        tnewd5dataset += [td5dataset[0]]
                        for pick in picklist:
                            pick += 1
                            tnewd5dataset += [td5dataset[pick]]
                        knewd5dataset += [tnewd5dataset]
                    jnewd5dataset += [jnewd5dataset]
                inewd5dataset += [jnewd5dataset]  
            newd5dataset += [inewd5dataset]   
    
    return newd5dataset

'''
This function is used to caculate the covariance matrix of jackknife blocks.
The input dataset should be a 2d array or list.
jackk_measur[i] = [block1-mean, block2-mean, ...]
'''
def jackkblocks_cov(jackkblockdata):

    #print('\n\naaa\n',jackkblockdata[0],'\n\naaa\n')
    #print('\n\nbbb\n',jackkblockdata[-1],'\n\nbbb\n')

    n_measur = len(jackkblockdata)
    nblock = len(jackkblockdata[0])
    cov_matrix = []
    for i in range(0, n_measur):
        cov_matrix_i = []
        for j in range(0, n_measur):
            cov_ij = 0
            for iblock in range(0, nblock):
                cov_ij += jackkblockdata[i][iblock] * jackkblockdata[j][iblock]
            cov_ij = cov_ij * (nblock - 1) / nblock
            cov_matrix_i += [cov_ij]
        cov_matrix += [cov_matrix_i]
    return cov_matrix

'''
This function is used to caculate the covariance matrix of jackknife blocks.
The input dataset should be a 2d array or list.
jackksample[i] = [x, block1, block2, ...]
'''
def jackksample_cov(jackkmean, jackksamples, stderr=False):

    #print(jackksamples)

    n_measur = len(jackksamples)
    nblock = len(jackksamples[0])-1

    if stderr == False:
        cov_matrix = []
        for i in range(0, n_measur):
            cov_matrix_i = []
            for j in range(0, n_measur):
                cov_ij = 0
                for iblock in range(0, nblock):
                    cov_ij += (jackksamples[i][iblock+1]-jackkmean[i]) * (jackksamples[j][iblock+1]-jackkmean[j])
                cov_ij = cov_ij * (nblock - 1) / nblock
                cov_matrix_i += [cov_ij]
            cov_matrix += [cov_matrix_i]
        return cov_matrix

    else:
        cov_matrix = []
        for i in range(0, n_measur):
            cov_matrix_i = []
            for j in range(0, n_measur):
                cov_ij = 0
                for iblock in range(0, nblock):
                    cov_ij += (jackksamples[i][iblock+1]-jackkmean[i]) * (jackksamples[j][iblock+1]-jackkmean[j])
                if i!=j:
                    cov_ij = 0
                cov_ij = cov_ij * (nblock - 1) / nblock
                cov_matrix_i += [cov_ij]
            cov_matrix += [cov_matrix_i]
        return cov_matrix

'''
bootspick for 2d array
dataset should be arranged as:
dataset[i]:
[x, conf1, conf2, ...]
'''
def bootspick_2d(dataset, bootsample):
    data_boots = [[dataset[i][0]] for i in range(0, len(dataset))]
    for pick in bootsample:
        for i in range(0, len(dataset)):
            data_boots[i] += [dataset[i][pick+1]]
    return data_boots

            
'''
2d list to array
'''
def list_to_arr_2d(datalist):
    
    l1 = len(datalist)
    l2 = len(datalist[0])

    newarray = np.zeros([l1,l2])
    for i in range(0, l1):
        for j in range(0, l2):
            newarray[i][j] = datalist[i][j]
    return newarray

'''
combine the real and imaginary part of the data
'''
def comb_real_img_2d(realdata, imagdata, start = 1):

    complex_data = []
    for i in range(0, len(realdata)):
        realraw = realdata[i]
        complex_raw = realraw[:start]
        for j in range(start, len(realdata[0])):
            complex_raw += [complex(realdata[i][j],imagdata[i][j])]
        complex_data += [complex_raw]
    return complex_data

'''
Take the average of two 2d dataset
'''
def ave_2d(dataset_1, dataset_2):

    dataset_ave = []
    for i in range(0, len(dataset_1)):
        dataraw_ave = [dataset_1[i][0]]
        for j in range(1, len(dataset_2)):
            dataraw_ave += [(dataset_1[i][j]+dataset_2[i][j])/2]
        dataset_ave += [dataraw_ave]

    return dataset_ave

'''
Take the average of several 2d dataset
'''
def ave_2d_lists(dataset, signlist=[]):
    #print('**',len(dataset))

    if len(signlist) == 0:
        signlist = [1 for i in range(0, len(dataset))]

    dataset_ave = []
    for i in range(0, len(dataset[0])):
        dataraw_ave = [dataset[0][i][0]]
        for j in range(1, len(dataset[0][0])):
            ij_ave = 0
            for iset in range(0, len(dataset)):
                ij_ave += dataset[iset][i][j] * signlist[iset]
            dataraw_ave += [ij_ave/len(dataset)]
        dataset_ave += [dataraw_ave]

    return dataset_ave

'''
Take the ratio of two 2d dataset
'''
def ratio_2d_lists(dataset):

    dataset_ratio = []
    for i in range(0, len(dataset[0])):
        dataraw_ratio = [dataset[0][i][0]]
        for j in range(1, len(dataset[0][0])):
            #print(dataset[0][i][j],dataset[1][i][j])
            dataraw_ratio += [dataset[0][i][j]/dataset[1][i][j]]
        dataset_ratio += [dataraw_ratio]

    return dataset_ratio

'''
Combine several dataset and will take real , imag or complex.
'''
def comb_2d_lists(dataset, datatype='real'):

    comb_dataset = []
    #print(np.shape(dataset))

    if datatype == 'real':
        for i_raw in range(0, len(dataset[0])):
            raw_dataset = [dataset[0][i_raw][0].real]
            for i_set in range(0, len(dataset)):
                raw_dataset += [dataset[i_set][i_raw][i].real for i in range(1, len(dataset[i_set][i_raw]))]
            comb_dataset += [raw_dataset]
    elif datatype == 'imag':
        for i_raw in range(0, len(dataset[0])):
            raw_dataset = [dataset[0][i_raw][0].real]
            for i_set in range(0, len(dataset)):
                raw_dataset += [-dataset[i_set][i_raw][i].imag for i in range(1, len(dataset[i_set][i_raw]))]
            comb_dataset += [raw_dataset]
    else:
        for i_raw in range(0, len(dataset[0])):
            raw_dataset = [dataset[0][i_raw][0].real]
            for i_set in range(0, len(dataset)):
                raw_dataset += [dataset[i_set][i_raw][i] for i in range(1, len(dataset[i_set][i_raw]))]
            comb_dataset += [raw_dataset]

    return comb_dataset

def save_avedata(datafile):
    dir_list = ['c2pt_a_src000','c2pt_a_src001','c2pt_a_src010','c2pt_a_src100',\
        'c2pt_a_src110','c2pt_a_src101','c2pt_a_src011','c2pt_a_src111','c2pt_a_srccT000+001']
    savefile = 'c2pt_a_set1-9/'
    dataset = []
    for idir in dir_list:
        dataset += [np.loadtxt(idir+'/'+datafile)]
    dataset = ave_2d_lists(dataset)
    ncfg = len(dataset[0]) - 1
    nrow = len(dataset)
    savecsv(dataset, savefile+datafile, ['csv, nrow='+str(nrow)+', ncfg='+str(ncfg)])
    

def save_data_rescale(datafile, nm=0.0637332):
    data = csvtolist(datafile)
    for row in data:
        for icol in range(1, len(row)):
            row[icol] *= nm 
    savefile = datafile.replace('c2pt.','c2pt.nm.')
    ncfg = len(row) - 1
    nrow = len(data)
    savecsv(data, savefile, ['csv, nrow='+str(nrow)+', ncfg='+str(ncfg)])

def save_folddata(datafile):
    data = csvtolist(datafile)
    data = folddat(data)
    savefile = datafile + '.fold'
    savecsv(data, savefile)

if __name__ == "__main__":

    if sys.argv[1] =='fold':
        save_folddata(sys.argv[2])

    elif sys.argv[1] =='p1':
        construct_proton1(sys.argv[2])

    elif sys.argv[1] =='unpo':
        construct_proton_unpo(sys.argv[2])

    elif sys.argv[1] =='nm':
        save_data_rescale(sys.argv[2])

    elif sys.argv[1] =='ave':
        save_avedata(sys.argv[2])