#!/usr/bin/env python3
import numpy as np
import numba as nb
import time
import traceback 
import sys
import warnings
from tools import *
import modelfuncs
from modelfuncs import *
from makefuncs import *
import inspect
from scipy.optimize import curve_fit

warnings.simplefilter('ignore', category=nb.NumbaDeprecationWarning)
warnings.simplefilter('ignore', category=nb.NumbaWarning)

random_number_file = 'random_number_1e7'

#------------------------------------------------------------#
################# jackknife covariance matrix ################
#------------------------------------------------------------#

# caculate the jackknife covariace matrix
# input file should be the jackknife mean 2d_data:
# mean[x]: [x, y, fmean...]
# while the jackknife block file should be :
# jackblocks[block][x]: [x, y, fmean...]

def jackk_covariance(mean, jackblocks, v=2):

    #print(mean)

    xlen = len(mean)
    ncfg = len(jackblocks)
    cov_matrix = np.zeros([xlen, xlen])
    for i in range(0, ncfg):
        jacksample = jackblocks[i]
        for j in range(0, xlen):
            for k in range(0, xlen):
                cov_matrix[j][k] += (jacksample[j][v]-mean[j][v]) * (jacksample[k][v]-mean[k][v])
    for j in range(0, xlen):
        for k in range(0, xlen):
            cov_matrix[j][k] = cov_matrix[j][k] * (ncfg-1) / ncfg
    
    return cov_matrix

#------------------------------------------------------------#
############### chi square evaluation function ###############
#------------------------------------------------------------#

# caculate the convariant chi-squared between model function and dataset
# dataset is default as a 2_D list, with the 1st column being x

def chisq_value(dataset, func, param, xmin, xmax):

    dataset = dataset[xmin:xmax+1]
    #print(xmin,xmax)
    x = [row[0] for row in dataset]
    y = list(map(np.mean,np.delete(dataset,0,1)))
    covar = (np.cov(np.delete(dataset,0,1)))/(len(dataset[0])-1)
    dy = [y[i]-func(x[i],*param) for i in range(0,len(x))]
    x = np.array(x)
    y = np.array(y)
    #print(covar)
    #print(dy)
    dy = np.array([dy])
    covarinverse =  np.linalg.inv(covar)
    #print(covarinverse)
    csq = np.dot(dy,np.dot(covarinverse,dy.T))
    #print(csq)
    return csq[0][0]

# this is a special case for 3d chisq value caculation
# the dataset is assumed to be dataset[z][ts] = [x,y,f(x,y),err(x,y)]
#@nb.jit(nopython=True)
def chisq_value_stderr(dataset, func, param, cov_matrix = [None]):

    if len(dataset[0]) == 3:
        x = []
        fx = []
        ferr = []
        funcx = []
        df = []
        for i in range(0, len(dataset)):
            x += [dataset[i][0]]
            fx += [dataset[i][1]]
            ferr += [dataset[i][2]]
            funcx += [func(x[i],*param)]
            df += [fx[i]-funcx[i]]

    elif len(dataset[0]) == 2:
        x = np.array([row[0] for row in dataset])
        fx = np.array([row[1] for row in dataset])
        #print(x,len(x))
        df = [fx[i]-func(x[i],*param) for i in range(0,len(x))]

    elif len(dataset[0]) == 4:
        x = np.array([row[0] for row in dataset])
        y = np.array([row[1] for row in dataset])
        fxy = np.array([row[2] for row in dataset])
        ferr = np.array([row[3] for row in dataset])
        df = [fxy[i]-func((x[i],y[i]),*param) for i in range(0,len(x))]
        #print('x,y',x,y)
        #print('fxy',fxy)
        #print('df',df)
    '''
    #print(fx)
    #print(cov_matrix)
    #print(np.sqrt(cov_matrix[0][0]),np.sqrt(cov_matrix[1][1]),np.sqrt(cov_matrix[2][2]))
    if (np.array(cov_matrix)).any() != None:
        #print(x,df)
        csq = 0
        cov_matrix_inv = np.linalg.inv(cov_matrix)
        #print(cov_matrix_inv)
        for i in range(0, len(x)):
            for j in range(0, len(x)):
                #print(1/cov_matrix[i][j],df[i]*df[j]/cov_matrix[i][j])
                csq += df[i]*df[j]*cov_matrix_inv[i][j]
                #print(csq)
        #print(np.linalg.inv(cov_matrix))
        #print('param',param)
        #print('x',x)
        #print('y',y)
        #print('fxy',fxy)
        #print('df',df)
        #print('csq',csq)
    else:
        csq = 0
        for i in range(0, len(x)):
            csq += df[i]*df[i]/(ferr[i]*ferr[i])
    '''
    csq = 0
    for i in range(0, len(x)):
        csq += df[i]*df[i]/(ferr[i]*ferr[i])
    #print('x',x)
    #print('y',y)
    #print('fx',fx)
    #print('ferr',ferr)
    #print('param',*param)
    #print('?',csq)
    return csq

@nb.jit
def chisq_value_stderr_simplify(dataset, func, param):

    csq = 0
    for i in range(0, dataset.shape[0]):
        ferr = dataset[i][2]
        df = dataset[i][1]-func(dataset[i][0],*param)
        csq += df*df/(ferr*ferr)

    return csq

'''
def general_chisq_value(func, param, x, y, err):

    if len(err) == 2:
        covar = err
    else:
        covar = np.diag(err)
    dy = [y[i]-func(x[i],*param) for i in range(0,len(x))]
    #print(covar)
    dy = np.array([dy])
    covarinverse =  np.linalg.inv(covar)
    #print(covarinverse)
    #print(dy.T)
    #print(np.dot(covarinverse,dy.T))
    csq = np.dot(dy,np.dot(covarinverse,dy.T))
    #print(csq)
    return csq[0][0]
'''

#------------------------------------------------------------#
######################### AICc tools #########################
#------------------------------------------------------------#

# caculate the AICc 
# dataset is default as a 2_D list, with the 1st column being x

def aicc_value(dataset, func, param, xmin=0, xmax=0, datatype='conf', fix_num=0, **kwargs):
    
    if datatype == 'conf':
        chisq = chisq_value(dataset, func, param, xmin, xmax)
        #print("chisq",chisq)
        dnum = len(dataset[0])
    elif datatype == 'meanerr':
        chisq = chisq_value_stderr(dataset, func, param,  **kwargs)
        dnum = 1e10
    else:
        print('Wrong datatype!!!')
    
    pnum = len(param) - fix_num
    #print(len(param), pnum)

    lnL = -0.5*chisq
    #print('chisq:',chisq)
    aic = 2*pnum - 2 * lnL
    aicc = aic + (2*pnum*pnum + 2*pnum)/(dnum - pnum -1)
    return aicc,chisq

# dataset here is assumed as a 2_D list, each ray: xmin xmax ... aicc
# position list store the position of weighted paramter for different model result

def aicc_weight(dataset, position):

    #pick weighted data
    pdataset = []
    pp = 0
    for i in range(0,len(dataset)):
        if i == 0 or dataset[i][0] > dataset[i-1][0]:
            pdataset += [[dataset[i][0],dataset[i][1],dataset[i][position[pp]],dataset[i][-1]]]
        else:
            pp += 1
            pdataset += [[dataset[i][0],dataset[i][1],dataset[i][position[pp]],dataset[i][-1]]]
    dataset = pdataset
    #print(dataset)
            
    # now the dataset raw is : xmin xmax param aicc
    dataset = sorted(dataset,key=(lambda x:x[0]))  # sorted by the tmin, put different model together
    output = []
    count = 0
    while count < len(dataset):
        subdata = [dataset[count]]
        count += 1
        while count < len(dataset) and dataset[count][0] == subdata[0][0]:
            subdata += [dataset[count]]
            count += 1
        subdata = sorted(subdata,key=(lambda x:x[-1]))
        numerator = 0
        denominator = 0
        for i in range(0,len(subdata)):
            numerator += subdata[i][2] * np.exp(0.5*(subdata[0][-1]-subdata[i][-1]))
            denominator += np.exp(0.5*(subdata[0][-1]-subdata[i][-1]))
        output += [[subdata[0][0],subdata[0][1],numerator/denominator]]
    return output

#------------------------------------------------------------#
################# General Bootstrap Function #################
#------------------------------------------------------------#

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# bootstrap for general goals
# the general data set should in the shape as list:
# x0 conf1 conf2 conf3...
# x1 conf1 conf2 conf3...
# ...
# xN conf1 conf2 conf3...
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def bootstrap(dataset, npick, func, params, random_number=[None],savesample=None,**kwargs):

    nconf = len(dataset[0])-1
    bootsvalue = []
    datasetT = np.transpose(dataset)
    failpercent = np.zeros([len(params)])

    aicc_samplelist = []
    two_samplelist = []
    three_samplelist = []

    for i in range(0,int(npick)):

        if np.array(random_number).any() == None:
            bootsample = list(map(int,np.random.uniform(0,nconf,nconf)))
            if i ==0:
                print('\nGenerate random number by np.random.uniform!!!\n')
        else:
            bootsample = random_number[i*nconf:(i+1)*nconf]
            #print(bootsample,i,'ed')
            bootsample = [int(bootsample[isamp]*nconf) for isamp in range(0, len(bootsample))]
        subdata = np.array([datasetT[0]])

        for pick in bootsample:
            subdata = np.append(subdata,[datasetT[pick+1]],axis=0)
        subdata = np.transpose(subdata)

        if func.__name__ == 'aiccfit':
            subboots,fail = func(subdata, params, failpercent)   # the function return a 2d list
            for f in range(0, len(fail)):
                failpercent[f] = (failpercent[f]*i+fail[f])/(i+3)
            #print('failpercent',failpercent)
        else:
            subboots = func(subdata, params)

        bootsvalue += [subboots]

        if savesample != None:
            two_samplelist += [[i] + subboots[0]]
            three_samplelist += [[i] + subboots[1]]
            aicc_samplelist += [[i] + subboots[-1]]
    #print(np.shape(subboots),np.shape(subboots[0]),np.shape(subboots[1]),np.shape(subboots[2]))
    if savesample != None:
        #np.savetxt(savesample+'_', aicc_samplelist)
        np.savetxt(savesample+'_twostate.txt', two_samplelist)
        np.savetxt(savesample+'_threestate.txt', three_samplelist)


    low16 = int(np.ceil(0.16*npick))
    high16 = int(np.floor(0.84*npick))
    mid = int(0.5*npick)

    popt = []
    for i in range(0,len(bootsvalue[0])):        # range in the number of models   
        modelboots = [bootsvalue[k][i][:] for k in range(0, npick)]
        modelboots = np.transpose(modelboots)
        ipopt = []
        for raw in modelboots:       # range in the number of parameters in the model
            raw = sorted(raw)
            raw = np.array(raw)
            subpopt = raw[mid]
            suberrorn = raw[mid]-raw[low16]
            suberrorp = raw[high16]-raw[mid]
            ipopt += [subpopt, suberrorn, suberrorp]
        popt += [ipopt]
    return popt

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# bootstrap for special case, which need SS and SP datasets
# the general data set should in the shape as list:
# x0 conf1 conf2 conf3...
# x1 conf1 conf2 conf3...
# ...
# xN conf1 conf2 conf3...
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def SSbootstrap(SPdataset,SSdataset, npick, func, SPparams, SSparams, **kwargs):

    nconf = len(SPdataset[0])-1
    bootsvalue = []
    SPdatasetT = np.transpose(SPdataset)
    SSdatasetT = np.transpose(SSdataset)
    SPfailpercent = np.zeros([len(SPparams)])
    SSfailpercent = np.zeros([len(SSparams)])
    for i in range(0,int(npick)):
        bootsample = list(map(int,np.random.uniform(0,nconf,nconf)))
        SPsubdata = np.array([SPdatasetT[0]])
        SSsubdata = np.array([SSdatasetT[0]])
        for pick in bootsample:
            SPsubdata = np.append(SPsubdata,[SPdatasetT[pick+1]],axis=0)
            SSsubdata = np.append(SSsubdata,[SSdatasetT[pick+1]],axis=0)
        SPsubdata = np.transpose(SPsubdata)
        SSsubdata = np.transpose(SSsubdata)
        if func.__name__ == 'aiccfit':
            SPsample,SPfail = func(SPsubdata, SPparams, SPfailpercent)   # the function return a 2d list
            for f in range(0, len(SPfail)):
                SPfailpercent[f] = (SPfailpercent[f]*i+SPfail[f])/(i+3)
            #print('failpercent',failpercent)
        else:
            SPsample = func(SPsubdata, SPparams)   # the function return a 2d list
        SPaiccsample = SPsample[-1]

        # modify the SS parameter by the SP fitting results
        #print(SPaiccsample)
        for i in range(0,len(SSparams)):
            if SSparams[i][0] == 'fixSPE0E1':
                #SSfitparams[i][0] = 'fixE0E1'
                SSparams[i][3] = SPaiccsample[1]
                SSparams[i][5] = SPaiccsample[3]
        
        #print('SPaiccsample',SPaiccsample)
        #print('SSinput',SSfitparams)

        if func.__name__ == 'aiccfit':
            SSsample,SSfail = func(SSsubdata, SSparams, SSfailpercent)   # the function return a 2d list
            for f in range(0, len(SSfail)):
                SSfailpercent[f] = (SSfailpercent[f]*i+SSfail[f])/(i+3)
            #print('failpercent',failpercent)
        else:
            SSsample = func(SSsubdata, SSparams)   # the function return a 2d list
        sample = list(SPsample) + list(SSsample)
        bootsvalue += [sample]                 # bootsvalue here is a 3d list

    low16 = int(np.ceil(0.16*npick))
    high16 = int(np.floor(0.84*npick))
    mid = int(0.5*npick)

    popt = []
    for i in range(0,len(bootsvalue[0])):        # range in the number of models   
        modelboots = [bootsvalue[k][i][:] for k in range(0, npick)]
        modelboots = np.transpose(modelboots)
        ipopt = []
        for raw in modelboots:       # range in the number of parameters in the model
            raw = sorted(raw)
            raw = np.array(raw)
            subpopt = raw[mid]
            suberrorn = raw[mid]-raw[low16]
            suberrorp = raw[high16]-raw[mid]
            ipopt += [subpopt, suberrorn, suberrorp]
        popt += [ipopt]
    return popt

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# bootstrap for special case: for every sample, first fit SPc-
# -pt, then use that to fit the SSc2pt, then use the E0 and E1
# to fit the c3pt/c2pt with several gaven ts datas
# the general SP/SSc2data set should in the format as list:
# x0 conf1 conf2 conf3...
# x1 conf1 conf2 conf3...
# ...
# xN conf1 conf2 conf3...
# the general tsc2data and tsc3data should in the format as list:
# tsc2data.shape = (ts, conf), tsc3data.shape = (ts, tau, conf)
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def c3ptfitbootstrap(c2func,c3func,SSc2dataset,SSc2params,SSratioc3,SSratioc2,SSratioerr,tsc3params,tslist,zlist,npick,random_number=[None],jointc2=[None],ratio=True,taumin=1, sample_save=None,normalization=False,**kwargs):

    #normalization=True
    #print(SSratioerr)
    jointc2_boots = [None]

    nconf = len(SSc2dataset[0])-1
    bootsvalue = []
    SSdatasetT = np.transpose(SSc2dataset)
    SSratioc2T = np.transpose(SSratioc2)

    SSfailpercent = np.zeros([len(SSc2params)])
    for ipick in range(0,int(npick)):

        if len(random_number) == 0:
            bootsample = list(map(int,np.random.uniform(0,nconf,nconf)))
            print('Generate random number by np.random.uniform.')
        else:
            bootsample = random_number[ipick*nconf:(ipick+1)*nconf]
            bootsample = [int(bootsample[isamp]*nconf) for isamp in range(0, len(bootsample))]

        SSsubdata = np.array([SSdatasetT[0]])
        SSratioc2subdata = np.array([SSratioc2T[0]])
        for pick in bootsample:
            SSsubdata = np.append(SSsubdata,[SSdatasetT[pick+1]],axis=0)
            SSratioc2subdata = np.append(SSratioc2subdata,[SSratioc2T[pick+1]],axis=0)
        SSsubdata = np.transpose(SSsubdata)
        SSratioc2subdata = np.transpose(SSratioc2subdata)
        SSratioc3subdata = d5listpick(SSratioc3, bootsample, 3)

        if (np.array(jointc2)).any() != None:
            jointc2_boots = bootspick_2d(jointc2, bootsample)

        if c2func != None:

            if c2func.__name__ == 'aiccfit':
                SSsample,SSfail = c2func(SSsubdata, SSc2params, SSfailpercent)   # the function return a 2d list
                for f in range(0, len(SSfail)):
                    SSfailpercent[f] = (SSfailpercent[f]*ipick+SSfail[f])/(ipick+3)
            else:
                SSsample = c2func(SSsubdata, SSc2params)   # the function return a 2d list
            SSaiccsample = SSsample[-1]
        else:
            SSaiccsample = []

        # modify the c3 parameter by the SS fitting results
        tsc3params = makec3pt_params(tsc3params, SSaiccsample)

        bootssample = []
        for z in range(0,len(zlist)):
            c3sample = c3func(tslist,SSratioc3subdata[z],SSratioc2subdata,SSratioerr[z],tsc3params,jointc2=jointc2_boots,ratio=ratio,taumin=taumin)
            bootssample += [list(c3sample)]

        bootsvalue += [bootssample]                  # bootsvalue here is a 3d list, bootsvalue[npick][z][value]
        print('bootssample', ipick, '/', npick,'done')

    if normalization == True:
        for z in range(0, len(zlist)):
            if zlist[z] == 0:
                z0 = z
        for i in range(0, int(npick)):
            for z in range(0, len(zlist)):
                if zlist[z] != 0:
                    bootsvalue[i][z][4] /= bootsvalue[i][z0][4]

    # save the bootstrap samples
    if sample_save != None:
        for z in range(0,len(zlist)):
            z_sample = []
            for ib in range(0, npick):
                z_sample += [[ib] + bootsvalue[ib][z]]
            z_sample_save = sample_save + '_Z' + str(zlist[z])
            np.savetxt(z_sample_save, z_sample, fmt='%.8e')

    low16 = int(np.ceil(0.16*npick))
    high16 = int(np.floor(0.84*npick))
    mid = int(0.5*npick)
   
    popt = []
    for i in range(0,len(zlist)):        # range in the number of z  
        modelboots = [bootsvalue[k][i][:] for k in range(0, npick)]
        modelboots = np.transpose(modelboots)
        ipopt = [zlist[i]]
        ipoptmean = []
        for raw in modelboots:       # range in the number of parameters in the model
            raw = sorted(raw)
            raw = np.array(raw)
            subpopt = raw[mid]
            suberrorn = raw[mid]-raw[low16]
            suberrorp = raw[high16]-raw[mid]
            ipoptmean += [subpopt]
            ipopt += [subpopt, suberrorn, suberrorp]
        popt += [ipopt]
    #print([np.shape(SSratioerr[-1][i]) for i in range(0, len(SSratioerr[0]))],trans3to2(SSratioerr[i]))
    return popt

def c3ptfitbootstrap_rioffe(Ns,c2func,c3func,SPc2dataset_allpx,SPc2params_allpx,SSc2dataset_allpx,SSc2params_allpx,SSratioc3_allpx,SSratioc2_allpx,SSratioerr_allpx,tsc3params_allpx,pxlist,tslist,zlist,npick,samplesave=None):

    nconf = len(SPc2dataset_allpx[0][0])-1
    print('configuration number:',nconf)
    bootsvalue = []

    SPc2datasetT = []
    SSc2datasetT = []
    SSratioc2T = []
    SPc2failpercent = []
    SSc2failpercent = []

    for p in range(0, len(pxlist)):
        SPc2datasetT += [np.transpose(SPc2dataset_allpx[p])]
        SSc2datasetT += [np.transpose(SSc2dataset_allpx[p])]
        SSratioc2T += [np.transpose(SSratioc2_allpx[p])]
        SPc2failpercent += [np.zeros(len(SPc2params_allpx[p]))]
        SSc2failpercent += [np.zeros(len(SSc2params_allpx[p]))]

    for ipick in range(0,int(npick)):

        #print('bootssample', ipick, '/', npick,'begin')
        bootsample = list(map(int,np.random.uniform(0,nconf,nconf)))
        
        bootsamplevalue = []
        for p in range(0, len(pxlist)):
            SPc2subdata = np.array([SPc2datasetT[p][0]])
            SSc2subdata = np.array([SSc2datasetT[p][0]])
            SSratioc2subdata = np.array([SSratioc2T[p][0]])
            for pick in bootsample:
                SPc2subdata = np.append(SPc2subdata,[SPc2datasetT[p][pick+1]],axis=0)
                SSc2subdata = np.append(SSc2subdata,[SSc2datasetT[p][pick+1]],axis=0)
                SSratioc2subdata = np.append(SSratioc2subdata,[SSratioc2T[p][pick+1]],axis=0)
            SPc2subdata = np.transpose(SPc2subdata)
            SSc2subdata = np.transpose(SSc2subdata)
            SSratioc2subdata = np.transpose(SSratioc2subdata)
            SSratioc3subdata = d5listpick(SSratioc3_allpx[p], bootsample, 3)
            
            if (np.array(SPc2params_allpx)).any() != None:
                if c2func.__name__ == 'aiccfit':
                    SPc2sample, SPfail = c2func(SPc2subdata, SPc2params_allpx[p], SPc2failpercent[p])   # the function return a 2d list
                    for f in range(0, len(SPfail)):
                        SPc2failpercent[p][f] = (SPc2failpercent[p][f]*ipick+SPfail[f])/(ipick+3)
                        #print('failpercent',failpercent)
                else:
                    SPc2sample = c2func(SPc2subdata, SPc2params_allpx[p])   # the function return a 2d list
                SPc2aiccsample = SPc2sample[-1]
                
                # modify the SS parameter by the SP fitting results
                #print(SPaiccsample)
                for i in range(0,len(SSc2params_allpx[p])):
                    if SSc2params_allpx[p][i][0] == 'fixSPE0E1':
                        SSc2params_allpx[p][i][3] = SPc2aiccsample[1]
                        SSc2params_allpx[p][i][5] = SPc2aiccsample[3]
            
            if c2func.__name__ == 'aiccfit':
                SSc2sample,SSfail = c2func(SSc2subdata, SSc2params_allpx[p], SSc2failpercent[p])   # the function return a 2d list
                for f in range(0, len(SSfail)):
                    SSc2failpercent[p][f] = (SSc2failpercent[p][f]*ipick+SSfail[f])/(ipick+1)
            else:
                SSc2sample = c2func(SSc2subdata, SSc2params_allpx[p])   # the function return a 2d list
            SSc2aiccsample = SSc2sample[-1]
            #print('SSc2aiccsample',SSc2aiccsample)
            
            # modify the c3 parameter by the SS fitting results
            for i in range(0,len(tsc3params_allpx[p])):
                if tsc3params_allpx[p][i][0] == 'fixSSA0E0A1E1':
                    tsc3params_allpx[p][i][2] = SSc2aiccsample[0]
                    tsc3params_allpx[p][i][3] = SSc2aiccsample[1]
                    tsc3params_allpx[p][i][4] = SSc2aiccsample[2]
                    tsc3params_allpx[p][i][5] = SSc2aiccsample[3]
                    Zv = 4
                elif tsc3params_allpx[p][i][0] == 'fixSSA0E0A1':
                    tsc3params_allpx[p][i][2] = SSc2aiccsample[0]
                    tsc3params_allpx[p][i][3] = SSc2aiccsample[1]
                    tsc3params_allpx[p][i][4] = SSc2aiccsample[2]
                    Zv = 3
                elif tsc3params_allpx[p][i][0] == 'fixSSA0E0A1E1A2E2':
                    tsc3params_allpx[p][i][2] = SSc2aiccsample[0]
                    tsc3params_allpx[p][i][3] = SSc2aiccsample[1]
                    tsc3params_allpx[p][i][4] = SSc2aiccsample[2]
                    tsc3params_allpx[p][i][5] = SSc2aiccsample[3]
                    tsc3params_allpx[p][i][6] = SSc2aiccsample[4]
                    tsc3params_allpx[p][i][7] = SSc2aiccsample[5]
                    Zv = 6
                elif tsc3params_allpx[p][i][0] == 'full':
                    tsc3params_allpx[p][i][2] = SSc2aiccsample[0]
                    tsc3params_allpx[p][i][3] = SSc2aiccsample[1]
                    tsc3params_allpx[p][i][4] = SSc2aiccsample[2]
                    tsc3params_allpx[p][i][5] = SSc2aiccsample[3]
                    Zv = 4
                else:
                    print('!!! Wrong input! Please check!~ ')
            
            pxbootsvalue = []
            for z in range(0,len(zlist)):
                #print('\nz',zlist[z])
                c3sample = c3func(tslist,SSratioc3subdata[z],SSratioc2subdata,SSratioerr_allpx[p][z],tsc3params_allpx[p])
                pxbootsvalue += [c3sample[Zv]]
                #print(c3sample)
            bootsamplevalue += [pxbootsvalue]
        #print('ioffetime:',bootsamplevalue)
        bootsamplevalue = ioffeto_reduced(bootsamplevalue, zlist)
        #print('reduced_ioffetime:',bootsamplevalue)
        if samplesave != None:
            for z in range(0, len(zlist)):
                samplename = samplesave + str(zlist[z]) + '_bootssample_' + str(ipick) + '.txt'
                sampleoutfile = open(samplename,'w')
                #print(bootsamplevalue[z])
                sample_rioffe = [[pxlist[p]*zlist[z]*2*3.14259/Ns, bootsamplevalue[z][p+1]] for p in range(0,len(pxlist))]
                np.savetxt(sampleoutfile,sample_rioffe,fmt='%.6e')
                sampleoutfile.close()

        print('bootssample', ipick, '/', npick,'done')
        bootsvalue += [bootsamplevalue]                  # bootsvalue here is a 3d list, bootsvalue[npick][z][value]

    low16 = int(np.ceil(0.16*npick))
    high16 = int(np.floor(0.84*npick))
    mid = int(0.5*npick)

    #print(bootsvalue)
    popt = []
    print(np.shape(bootsvalue))
    for i in range(0,len(zlist)):        # range in the number of z  
        zboots = [bootsvalue[k][i][:] for k in range(0, npick)]
        zboots = np.transpose(zboots)
        ipopt = []
        for p in range(0, len(pxlist)):       # range in the number of parameters in the model
            raw = zboots[p+1]
            raw = sorted(raw)
            raw = np.array(raw)
            subpopt = raw[mid]
            suberrorn = raw[mid]-raw[low16]
            suberrorp = raw[high16]-raw[mid]
            ipopt += [[pxlist[p]*zlist[i]*2*3.14259/Ns, subpopt, suberrorn, suberrorp]]
        popt += [ipopt]
    return popt

# this function transform the ioffetime distribution to reduced one
def ioffeto_reduced(ioffedata, zlist):

    rioffe = []                
    for z in range(0,len(zlist)):
        zrioffe = []
        for px in range(0,len(ioffedata)):
            zrioffe += [ioffedata[px][z]/ioffedata[0][z]*ioffedata[0][0]/ioffedata[px][0]]
            #print(px,z,ioffedata[px][z],ioffedata[0][z],ioffedata[0][0],ioffedata[px][0])
        zrioffe = [zlist[z]] + zrioffe
        rioffe += [zrioffe]
    return rioffe

#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# fit and get the fitting error based on the sample files
# the input file should contain the general format of files
# 1st line: job_discrible num_variable(number of variables)
# line: fitfunc poptguess
# line: fitfunc poptguess
# ...
# line xrange(data x range)
# line xvaluerange(f(x) range)
# errfile fitting_error_file mean_column error_column
# samplefile filename_sample sample_min sample_max x_column y_column
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
def fit_samplefile(fitmethod,commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    # create the result folder
    inputfolder = os.path.dirname(commandfile)
    outfolder = inputfolder + '/results'
    mkdir(outfolder)
        
    # 1st line, read describe and number of variables
    describe = (commands[0].split())[0]
    print('job describle:',describe)
    num_variable = int((commands[0].split())[1])
        
    # 2nd line, read fitting function and fitting guess
    #line = commands[1].split()
    #fitfunc = getattr(modelfuncs,line[0])
    #poptguess = list(map(float,line[1:]))
    #print('fitfunc:',line[0])
    #print('poptguess:',poptguess)
                
    # read error file and sample file
    xvaluemin = -1e50
    xvaluemax = 1e50
    ymeans = []  # ymean[i][mean]
    sampletype = []
    xy_errs = []
    labels = []
    fitparameter = []
    fileintv = -1
    for index in range(0,len(commands)):

        line = commands[index].split()

        # read px range
        if line[0] == 'xrange':
            xmin = int(line[1])
            xmax = int(line[2])
            print('xrange:',xmin,'~',xmax) 
        
        elif line[0] == 'xvaluerange':
            xvaluemin = float(line[1])
            xvaluemax = float(line[2])
            print('xvaluerange:',xvaluemin,'~',xvaluemax) 

        # read data range
        elif line[0] == 'filerange' and int(line[3]) >= int(line[2]):
            filemin = int(line[1])
            filemaxl = int(line[2])
            filemaxr = int(line[3])
            print('zrange:',filemin,'~','[',filemaxl,',',filemaxr,']')
        elif line[0] == 'filerange' and int(line[3]) < int(line[2]):
            filemin = int(line[1])
            filemax = int(line[2])
            fileintv = int(line[3])
            print('zrange:',filemin,'~',filemax,', with interval',fileintv)

        # read the fitting function
        elif line[0] == 'fitfunc':
            fitparameter += [list(map(str,line))]

        elif line[0] == 'errfile':
            labelmin = int(line[1])
            labelmax = int(line[2])
            fileformat = line[3]
            print(labelmin,labelmax,fileformat)
            yerr = []
            for ilabel in range(labelmin, labelmax+1):
                labels += [float(ilabel)]
                errfile = np.loadtxt(fileformat.replace('*',str(ilabel)))
                errfile = errfile[xmin:xmax+1]
                #yerr = []
                ymean = []
                xcol = int(line[4])
                meancol = int(line[5])
                if len(line) == 7:  # means only one column is error, like jackknife error
                    errcol = int(line[6])
                    sampletype += ['jackknife']
                    for ierr in range(0, len(errfile)):
                        if xvaluemin <= errfile[ierr][xcol] <= xvaluemax:
                            ymean += [[errfile[ierr][xcol],errfile[ierr][meancol],errfile[ierr][errcol]]]
                            yerr += [errfile[ierr][errcol]]
                elif len(line) == 8:  # means 2 columns are error, like bootstrap error
                    errcol1 = int(line[6])
                    errcol2 = int(line[7])
                    sampletype += ['bootstrap']
                    for ierr in range(0, len(errfile)): # get error for differet x, start from 1 because the first point is always 1
                        if xvaluemin <= errfile[ierr][xcol] <= xvaluemax:
                            ymean += [[errfile[ierr][xcol],errfile[ierr][meancol],(errfile[ierr][errcol1] + errfile[ierr][errcol2])/2]]
                            yerr += [(errfile[ierr][errcol1] + errfile[ierr][errcol2])/2]
                if ymean != []:
                    ymeans += [ymean]
                #if yerr != []:
                #yerrs += yerr

        elif line[0] == 'samplefile':
            labelmin = int(line[1])
            labelmax = int(line[2])
            fileformat = line[3]
            labelcount = 0
            for ilabel in range(labelmin, labelmax+1):
                samplefile = fileformat.replace('*', str(ilabel))
                xy_err = []
                for samp in range(int(line[4]),int(line[5])+1):
                    count = labelcount
                    samplename = samplefile + str(samp) + '.txt'
                    sampledata = np.loadtxt(samplename)
                    sampledata = sampledata[xmin:xmax+1]
                    xy_err_samp = []
                    for isamp in range(0, len(sampledata)):  # get x,y for differet x, start from 1 because the first point is always 1
                        if xvaluemin <= sampledata[isamp][int(line[6])] <= xvaluemax:
                            #print(count)
                            xy_err_samp += [[sampledata[isamp][int(line[6])], sampledata[isamp][int(line[7])], yerr[count]]]
                            count += 1
                    if samp == int(line[5]):
                        labelcount = count
                        #print(labelcount)
                    if xy_err_samp != []:
                        xy_err += [xy_err_samp]
                if xy_err != []:
                    xy_errs += [xy_err]
    print('ymeans',np.shape(ymeans))
    print('xy_errors',np.shape(xy_errs))
    print('sampletype',np.shape(sampletype))
    print('labels:',labels)

    # y = f(x)
    print('sample type:', sampletype[0])
    if num_variable == 1:
        popts = []

        for index in range(0, len(sampletype)):

            fitpopt = fitmethod(xy_errs[index], ymeans[index], fitparameter, sampletype[index])
            '''
            if sampletype[index] == 'bootstrap':
                poptmean = [popt[3*i] for i in range(0, int(len(popt)/3))]
            elif sampletype[index] == 'jackknife':
                poptmean = [popt[2*i] for i in range(0, int(len(popt)/2))]
            
            chisq = chisq_value_stderr(ymeans[index], fitfunc, poptmean)
            '''
            for imodel in range(0, len(popt)):
                popt += [labels[index] + fitpopt[imodel]]
            popts += [popt]
    
    # y = f(x1, x2)
    elif num_variable == 2:
        jackcov = [None]
        nsample = len(xy_errs[0])
        popts = []
        if fileintv == -1:
            for filemax in range(filemaxl, filemaxr+1):
                print(filemax, 'in range:', '[',filemaxl, ':', filemaxr, ']')
                new_xyerrs = xy_errs[filemin:filemax+1]
                new_labels = labels[filemin:filemax+1]
                new_xyerrs = ex12_4d(new_xyerrs)
                #print(np.shape(new_xyerrs))
                temp_xyerrs = []
                for i in range(0, nsample):
                    #print(new_xyerrs[i])
                    ixy_errs = new_xyerrs[i]
                    ixy_errs = trans3to2(ixy_errs,new_labels)
                    temp_xyerrs += [ixy_errs]
                    #print(i,'new_xyerrs:', ixy_errs[0])
                new_xyerrs = temp_xyerrs
                new_ymeans = ymeans[filemin:filemax+1]
                new_ymeans = trans3to2(new_ymeans,new_labels)
                #print(np.shape(new_xyerrs),np.shape(new_ymeans))
                jackcov = jackk_covariance(new_ymeans, new_xyerrs)
                #print(jackcov, '\n')
                fitpopt = fitmethod(new_xyerrs, new_ymeans, fitparameter, sampletype[filemax], jackcov)
                popt = []
                for imodel in range(0, len(fitpopt)):
                    popt += [[new_labels[0], new_labels[-1]] + fitpopt[imodel]]
                popts += [popt]
        else:
            file_end = filemax
            while filemin+fileintv <= file_end and len(xy_errs) > filemin:
                filemax = filemin+fileintv
                print(filemax, 'to', file_end)
                new_xyerrs = xy_errs[filemin:filemax+1]
                new_labels = labels[filemin:filemax+1]
                new_xyerrs = ex12_4d(new_xyerrs)
                #print(np.shape(new_xyerrs))
                temp_xyerrs = []
                for i in range(0, nsample):
                    #print(new_xyerrs)
                    ixy_errs = new_xyerrs[i]
                    ixy_errs = trans3to2(ixy_errs,new_labels)
                    temp_xyerrs += [ixy_errs]
                    #print(i,'new_xyerrs:', ixy_errs[0])
                new_xyerrs = temp_xyerrs
                new_ymeans = ymeans[filemin:filemax+1]
                new_ymeans = trans3to2(new_ymeans,new_labels)
                #print('\n',new_ymeans)
                fitpopt = fitmethod(new_xyerrs, new_ymeans, fitparameter, sampletype[filemax])
                popt = []
                for imodel in range(0, len(fitpopt)):
                    popt += [[(new_labels[0]+new_labels[-1])/2] + fitpopt[imodel]]
                popts += [popt]
                filemin += 1
    popts = ex12_3d(popts)
    #print(popts)
    for imodel in range(0, len(popts)):
        if imodel < len(fitparameter):
            modelname = fitparameter[imodel][1]
        else:
            modelname = 'aicc'
        print(modelname)
        outfilename = outfolder + '/' + describe + '_' + modelname + '.txt'
        #print(outfilename)
        outfile = open(outfilename,'w')
        np.savetxt(outfile, popts[imodel], fmt='%.6e')
        outfile.close()
        



            
