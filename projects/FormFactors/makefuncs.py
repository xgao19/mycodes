#!/usr/bin/env python3
import numpy as np
import os
from math import gamma
from math import pow
import traceback 
import sys

from modelfuncs_g8 import *
from tools import *
#from fine_PseudoPDF import *

def make_gpd_c3pt_ratio(Ein, Eout):
    if Ein[1] < 0.07 and Eout[1] > 0.07:
        if len(Ein) == 4:
            def gpd_ratio_twostate_fit(tstau, B00, B01, B10, B11, B_thermo_2):
                return gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2)
            return gpd_ratio_twostate_fit
        elif len(Ein) == 6:
            def gpd_ratio_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2):
                return gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=B_thermo_2)
            return gpd_ratio_threestate_fit
    else:
        if len(Ein) == 4:
            def gpd_ratio_twostate_fit(tstau, B00, B01, B10, B11):
                return gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11)
            return gpd_ratio_twostate_fit
        elif len(Ein) == 6:
            def gpd_ratio_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22):
                return gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22)
            return gpd_ratio_threestate_fit

def make_gpd_c3pt(Ein, Eout):
    if Ein[1] < 0.07 and Eout[1] > 0.07:
        if len(Ein) == 4:
            def gpd_c3pt_twostate_fit(tstau, B00, B01, B10, B11, B_thermo_2):
                return gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2)
            return gpd_c3pt_twostate_fit
        elif len(Ein) == 6:
            def gpd_c3pt_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2):
                return gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=B_thermo_2)
            return gpd_c3pt_threestate_fit
    else:
        if len(Ein) == 4:
            def gpd_c3pt_twostate_fit(tstau, B00, B01, B10, B11):
                return gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11)
            return gpd_c3pt_twostate_fit
        elif len(Ein) == 6:
            def gpd_c3pt_threestate_fit(tstau, B00, B01, B10, B11, B02, B20, B12, B21, B22):
                return gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22)
            return gpd_c3pt_threestate_fit