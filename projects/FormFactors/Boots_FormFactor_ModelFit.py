#!/usr/bin/env python3
import numpy as np
from scipy.optimize import least_squares
from scipy.misc import derivative

from utils.tools import *
from FormFactors.modelfuncs_FFs import *

if __name__ == "__main__":

    fmGeV = 5.0676896

    # read the job describe and commands
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    describe = (commands[0].split())[0]

    datafolder = os.path.dirname(sys.argv[1])
    resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder
    mkdir(resultfolder)
    samplefolder = resultfolder + '/samples/'
    mkdir(samplefolder)
    print('\nJob describe:', describe)

    samples = []
    Mlist = []
    for index in range(1, len(commands)):

        line = commands[index].split()

        if 'meanerr' == line[0]:
            mean_format = line[1]
            picklist_meanerr = [int(line[i]) for i in range(2, len(line))]
            print('  Mean errors file:', mean_format.split('/')[-1])
            meanerr_read = np.loadtxt(mean_format)
            meanerr = [meanerr_read[i] for i in picklist_meanerr]

        elif 'samples' == line[0]:
            sample_format = line[1]
            picklist_samples = [int(line[i]) for i in range(2, len(line))]
            print('  Sample format:', sample_format.split('/')[-1])
            samples += [np.loadtxt(sample_format.replace('#', str(i))) for i in picklist_samples]
            npick = len(samples[0])
            low, mid, up = int(0.16*npick), int(0.5*npick), int(0.84*npick)

        elif 'qsqmax' == line[0]:
            qsqmax = float(line[1])
            print('Qsq max:', qsqmax)

        elif 'Mlist' == line[0]:
            Mlist = np.array([float(line[i]) for i in range(1, len(line))])
            print('M list:', Mlist)

        elif 'ak' == line[0]:
            nk = int(line[1])
            print('k order:', nk)
    
    if 'VMD' in describe and len(Mlist) > 0:
        
        def ff_model(qsq, popt):
            F0 = popt
            return ff_VDM(qsq, F0, Mlist)
        
        popt_start = 0.5*np.ones(len(Mlist))

    elif 'VMD' in describe and len(Mlist) == 0:

        def ff_model(qsq, popt):
            return ff_VDM(qsq, 1, popt[0])
        
        popt_start = 0.5*np.ones(1)

    else:

        tcut = 4 * 0.14**2
        topt = tcut * (1-np.sqrt(1+qsqmax/tcut))
        def ff_model(qsq, popt):
            ak = popt
            return ff_z(qsq, tcut, topt, ak)
        
        popt_start = 0.5*np.ones(nk)


    popt_samples = []
    chisq_samples = []
    popt_x = np.arange(0, 50, 0.01)
    popt_fx_samples = []
    for isamp in range(0, len(samples[0])):

        if 'VMD' in describe:
            def mini_ff_res(popt):
                ff_res = np.zeros(len(picklist_meanerr))
                for i in range(0, len(ff_res)):
                    ff_res[i] = (ff_model(meanerr[i][0], popt) - samples[i][isamp]) / meanerr[i][2]
                return ff_res
            res = least_squares(mini_ff_res, popt_start, method='trf', bounds=(0.*np.ones(len(popt_start)), np.ones(len(popt_start))))
        else:
            def mini_ff_res(popt):
                ff_res = np.zeros(len(picklist_meanerr))
                for i in range(0, len(ff_res)):
                    ff_res[i] = (ff_model(meanerr[i][0], popt) - samples[i][isamp]) / meanerr[i][2]
                #for k in range(1, len(popt)):
                #    ff_res = np.append(ff_res, (popt[k]/popt[k-1])/5)
                return ff_res
            res = least_squares(mini_ff_res, popt_start, method='trf')
        chisq = sum(np.square(res.fun)) / (len(picklist_meanerr)-len(res.x))
        def ff_fx(qsq):
            return ff_model(qsq, res.x)
        radius = -6*derivative(ff_fx, 0, dx=1e-6)/fmGeV/fmGeV
        popt_samples += [list(res.x) + [radius, chisq]]
        print(list(res.x) + [radius, chisq], ff_fx(0))
        popt_fx = np.array([ff_fx(qsq) for qsq in popt_x])
        popt_fx_samples += [popt_fx]

    savename = samplefolder + describe + '_samples.dat'
    np.savetxt(savename, popt_samples, fmt='%.6f')

    popt_list = []
    for ipopt in range(0, len(popt_samples[0])):
        ipopt_samples = sorted([popt_samples[isamp][ipopt] for isamp in range(0, len(popt_samples))])
        popt_list += [[(ipopt_samples[up]+ipopt_samples[low])/2, (ipopt_samples[up]-ipopt_samples[low])/2]]
    savename = resultfolder + describe + '.dat'
    np.savetxt(savename, popt_list, fmt='%.6f')

    popt_fx0 = []
    popt_fx1 = []
    popt_fx2 = []
    for ix in range(0, len(popt_x)):
        ix_samples = sorted([popt_fx_samples[isamp][ix] for isamp in range(0, len(popt_fx_samples))])
        popt_fx0 += [[popt_x[ix], ix_samples[low], popt_x[ix]*ix_samples[low]]]
        popt_fx1 += [[popt_x[ix], ix_samples[mid], popt_x[ix]*ix_samples[mid]]]
        popt_fx2 += [[popt_x[ix], ix_samples[up], popt_x[ix]*ix_samples[up]]]
    savename = resultfolder + describe + '_band0.dat'
    np.savetxt(savename, popt_fx0, fmt='%.6f')
    savename = resultfolder + describe + '_band1.dat'
    np.savetxt(savename, popt_fx1, fmt='%.6f')
    savename = resultfolder + describe + '_band2.dat'
    np.savetxt(savename, popt_fx2, fmt='%.6f')