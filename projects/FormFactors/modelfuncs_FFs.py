#!/usr/bin/env python3
import numpy as np

fmGeV = 5.0676896

def array2D_exit(array1D, array2D):
    for i in range(0, len(array2D)):
        if (array1D==array2D[i]).all() == True:
            return True
    return False


def Ep(a,Lt,Ls,mass,px,qx,qy,qz):
    px = (px+qx)*np.pi/(Ls/2)/a/fmGeV
    py = qy*np.pi/(Ls/2)/a/fmGeV
    pz = qz*np.pi/(Ls/2)/a/fmGeV
    return np.sqrt(mass**2+px**2+py**2+pz**2)

def Eff_radius(FF, Qsq):
    return 6*(1/FF-1)/Qsq


#####################################################
############### Form factor models ##################
#####################################################
def ff_z(qsq, tcut, topt, ak):
    t = -qsq
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-topt))/(np.sqrt(tcut-t)+np.sqrt(tcut-topt)) 
    #a0 = 1
    #z0 = (np.sqrt(tcut)-np.sqrt(tcut-topt))/(np.sqrt(tcut)+np.sqrt(tcut-topt))
    #for k in range(0, len(ak)):
    #    a0 -= ak[k] * np.power(z0, k+1)

    #f = a0
    f = 0
    for k in range(0, len(ak)):
        #f += ak[k] * np.power(z, k+1)
        f += ak[k] * np.power(z, k)

    return f

# qsq [GeV^2], F0, M [GeV]
def ff_VDM_monopole(qsq, F0, M):
    return F0/(1+qsq/M**2)


# qsq [GeV^2], F0, M [GeV]
def ff_VDM(qsq, F0, M):
    return np.sum(F0/(1+qsq/M**2))