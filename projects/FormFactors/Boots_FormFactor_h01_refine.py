#!/usr/bin/env python3
import numpy as np
from tools import *
from scipy.optimize import least_squares
from scipy.misc import derivative
from scipy.optimize import curve_fit

def array2D_exit(array1D, array2D):
    for i in range(0, len(array2D)):
        if (array1D==array2D[i]).all() == True:
            return True
    return False

def ff_z(qsq, tcut, topt, ak):
    t = -qsq
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-topt))/(np.sqrt(tcut-t)+np.sqrt(tcut-topt)) 
    a0 = 1
    z0 = (np.sqrt(tcut)-np.sqrt(tcut-topt))/(np.sqrt(tcut)+np.sqrt(tcut-topt))
    for k in range(0, len(ak)):
        a0 -= ak[k] * np.power(z0, k+1)

    f = a0
    for k in range(0, len(ak)):
        f += ak[k] * np.power(z, k+1)

    return f

def Ep(a,Lt,Ls,mass,px,qx,qy,qz):
    px = (px+qx)*np.pi/(Ls/2)/a/fmGeV
    py = qy*np.pi/(Ls/2)/a/fmGeV
    pz = qz*np.pi/(Ls/2)/a/fmGeV
    return np.sqrt(mass**2+px**2+py**2+pz**2)

def Eff_radius(FF, Qsq):
    #print(Qsq,FF,6*(1/FF-1)/Qsq,6*(1/FF-1)/Qsq/fmGeV/fmGeV)
    return 6*(1/FF-1)/Qsq


if __name__ == "__main__":

    if sys.argv[1] == 'rbm':
        # read the job describe and commands
        inputfile = open(sys.argv[2],'r')
        commands = inputfile.readlines()
        describe = (commands[0].split())[0]

        datafolder = os.path.dirname(sys.argv[2])
        resultfolder = datafolder + '/results'   # default, mkdir at the input file folder
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        print('\nJob describe:', describe)

        pxlist = []
        qxyzlist = []
        for index in range(1, len(commands)):

            line = commands[index].split()

            if 'mass' in line[0]:
                mass = float(line[1])
                #describe = describe.replace('mass', 'mass'+str(mass)+'GeV')
                print('  Hadron mass (GeV):', mass)

            elif 'px' == line[0]:
                pxlist = [i for i in range(int(line[1]), int(line[2])+1)]
                describe = describe.replace('*', str(pxlist[0])+'-'+str(pxlist[-1]))
                print('  px: ', pxlist)

            elif 'qxyz' == line[0]:
                qxyz = [int(line[1]), int(line[2]), int(line[3])]
                qxyzlist += [qxyz]
                print('  qxyz: ', qxyz)

            elif 'sampleup' == line[0]:
                sample_up_format = line[1]
                col_up = int(line[2])
                print('Data on top:', sample_up_format.split('/')[-1])
            elif 'sampledown' == line[0]:
                sample_down_format = line[1]
                col_down = int(line[2])
                print('Data on bottom:', sample_down_format.split('/')[-1])

        print('\nJob describe:', describe)

        fmGeV = 5.0676896
        data_sample = []
        data_pxqxyz = []
        for ipx in range(0, len(pxlist)):
            ipx_sample_up_format = sample_up_format.replace('*', str(pxlist[ipx]))
            ipx_sample_down_format = sample_down_format.replace('*', str(pxlist[ipx]))
            for iq in range(0, len(qxyzlist)):
                iq_sample_up_format = ipx_sample_up_format.replace('qxqyqz', 'qx'+str(qxyzlist[iq][0])+'qy'+str(qxyzlist[iq][1])+'qz'+str(qxyzlist[iq][2]))
                iq_sample_down_format = ipx_sample_down_format.replace('qxqyqz', 'qx'+str(qxyzlist[iq][0])+'qy'+str(qxyzlist[iq][1])+'qz'+str(qxyzlist[iq][2]))
                iq_data_up_read = np.loadtxt(iq_sample_up_format)
                iq_data_down_read = np.loadtxt(iq_sample_down_format)

                iq_data_sample = [iq_data_up_read[isamp][col_up]/iq_data_down_read[isamp][col_down] for isamp in range(0, len(iq_data_up_read))]
                data_sample += [iq_data_sample]
                data_pxqxyz += [[pxlist[ipx]] + qxyzlist[iq] + [iq_data_up_read[0][0]]]
        print('Collect data shape:', np.shape(data_sample))

        npick = len(data_sample[0])
        mid = int(npick*0.5)
        high16 = int(npick*0.84)
        low16 = int(npick*0.16)

        data_rbm_collection = []
        for iq in range(0, len(data_sample)):
            iq_data_rbm_samples_sort = np.sort(data_sample[iq])
            iq_err_down = iq_data_rbm_samples_sort[mid] - iq_data_rbm_samples_sort[low16]
            iq_err_up = iq_data_rbm_samples_sort[high16] - iq_data_rbm_samples_sort[mid]
            data_rbm_collection += [data_pxqxyz[iq]+[iq_data_rbm_samples_sort[mid], max(iq_err_up,iq_err_down)]]
        
        save_name = resultfolder + '/' + describe + '.txt'
        np.savetxt(save_name, data_rbm_collection, fmt='%.6e')
        save_name = samplefolder + describe + '_bootssamples.txt'
        np.savetxt(save_name, data_sample, fmt='%.6e')
        print('Saved,',describe+'.txt')
