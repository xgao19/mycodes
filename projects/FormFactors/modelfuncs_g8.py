#!/usr/bin/env python3
import numpy as np
import os
from tools import *
from math import gamma
from math import pow
import traceback 
from scipy import integrate
from scipy import special

#------------------------------------------------------------#
###################### model functions #######################
#------------------------------------------------------------#
priorsign = 1000
# define the T~0 c2pt function
def oneexp(x, A0, E0):
    return A0 * np.exp(-E0 * x)
def twoexp(x, A0, E0, A1, E1):
    return A0 * np.exp(-E0 * x) + A1 * np.exp(-E1 * x)
def threeexp(x, A0, E0, A1, E1, A2, E2):
    return A0 * np.exp(-E0 * x) + A1 * np.exp(-E1 * x)+A2 * np.exp(-E2 * x)

# define the function to fit
TSIZE = 64                # length of t direction
def onestate(x, A0, E0):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate(x, A0, E0, A1, E1):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))
def threestate(x, A0, E0, A1, E1, A2, E2):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

def onestate_2pi(x, A0, E0, C0):
    return (A0+C0*np.exp(-E0 * TSIZE)) * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x)))
def twostate_2pi(x, A0, E0, A1, E1, C0):
    return (A0+C0*np.exp(-E0 * TSIZE)) * (np.exp(-E0 * x)+np.exp(-E0* (TSIZE-x))) + A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))

def threestate_t(x, A0, E0, A1, E1, A2, E2):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)/x+np.exp(-E2 * (TSIZE-x))/(TSIZE-x))

def threestate_joint(x, A0, E0, A1, E1, A2, E2, B1, B2, B3):
    return A0 * (np.exp(-E0 * x)+np.exp(-E0 * (TSIZE-x)))+A1 * (np.exp(-E1 * x)+np.exp(-E1 * (TSIZE-x)))+A2 * (np.exp(-E2 * x)+np.exp(-E2 * (TSIZE-x)))

#####################################################
##############c3pt/c2pt ratio fitting################
#####################################################
def c3pt_ratio_two_sum_plot(tsnsk, A0, E0, A1, E1, B0, B1, B2, B3):
    ts, nsk = tsnsk
    #print(tsnsk)
    def f(x):
        return c3pt_twostate((ts, x), A0, E0, A1, E1, B0, B1, B2, B3)/twostate(ts,A0,E0,A1,E1)
    y, err = integrate.quad(f, nsk-0.5, ts-nsk+0.5)
    return y
def c3pt_ratio_three_sum_plot(tsnsk, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts, nsk = tsnsk
    def f(x):
        return c3pt_ratio_three((ts, x), A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)
    y, err = integrate.quad(f, nsk-0.5, ts-nsk+0.5)
    return y
def c3pt_ratio_two_exp_plot(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    tau = tau + ts/2
    tstau=(ts,tau)
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twoexp(ts,A0,E0,A1,E1)
def c3pt_ratio_two_exp_plotmid(invts, A0, E0, A1, E1, B0, B1, B2, B3):
    ts = 1/invts
    tau = ts/2
    tstau=(ts,tau)
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twoexp(ts,A0,E0,A1,E1)
def c3pt_ratio_three_exp_plot(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    tau = tau + ts/2
    tstau=(ts,tau)
    return c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)/threeexp(ts,A0,E0,A1,E1,A2,E2)
def c3pt_ratio_two_exp_approx_plot(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    tau = tau + ts/2
    dE = E1 - E0
    return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)



def c3pt_ratio_two_exp(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twoexp(ts,A0,E0,A1,E1)
def c3pt_ratio_two_exp_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    #return B0
    return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)
def c3pt_ratio_two_exp_approx2(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)+B3*np.exp(-dE*ts)
def c3pt_ratio_two_exp_approx_prdE(tstau, B0, B1, B2, dE):
    ts,tau = tstau
    try:
        ylist = []
        for i in range(0, len(ts)):
            if ts[i] < priorsign:
                ylist += [B0+B1*np.exp(-dE*(ts[i]-tau[i]))+B2*np.exp(-dE*tau[i])]
            else:
                ylist += [dE]
        return np.array(ylist)
    except:
        if ts < priorsign:
            return B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)
        else:
            return dE

def c3pt_ratio_three_exp(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    return c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)/threeexp(ts,A0,E0,A1,E1, A2, E2)
def c3pt_ratio_three_exp_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    dE1 = E1 - E0
    dE2 = E2 - E0
    return B0+B1*np.exp(-dE1*(ts-tau))+B2*np.exp(-dE1*tau)+C1*np.exp(-dE2*(ts-tau))+C2*np.exp(-dE2*tau)


# special case: fix E1list

def c3pt_ratio_two(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    return c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twostate(ts,A0,E0,A1,E1)
def c3pt_ratio_two_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    return c3pt_twostate_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3)/twostate(ts,A0,E0,A1,E1)
def c3pt_ratio_three(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    return c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5)/threestate(ts,A0,E0,A1,E1,A2,E2)
def c3pt_ratio_three_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    return c3pt_threestate_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3)/threestate(ts,A0,E0,A1,E1,A2,E2)
def c3pt_ratio_three_approx_ov2(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    return c3pt_threestate_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3)/twostate(ts,A0,E0,A1,E1)

#####################################################
#####################c3pt fit########################
#####################################################

def c3pt_twostate_approx(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau))

def c3pt_twostate(tstau, A0, E0, A1, E1, B0, B1, B2, B3):
    ts,tau = tstau
    dE = E1 - E0
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE*(ts-tau))+B2*np.exp(-dE*tau)+B3*np.exp(-dE*ts))

def c3pt_threestate(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3, C4, C5):
    ts,tau = tstau
    dE1 = E1 - E0
    dE2 = E2 - E0
    dE12 = E2 - E1
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE1*(ts-tau))+B2*np.exp(-dE1*tau)+B3*A1/A0*np.exp(-dE1*ts)+C1*np.exp(-dE2*(ts-tau))+C2*np.exp(-dE2*tau)+C3*np.exp(-dE12*tau)+C4*np.exp(-dE12*ts)+C5*np.exp(-dE2*ts))
def c3pt_threestate_approx(tstau, A0, E0, A1, E1, A2, E2, B0, B1, B2, B3, C1, C2, C3):
    ts,tau = tstau
    dE1 = E1 - E0
    dE2 = E2 - E0
    #print(E1,E0)
    return A0*np.exp(-E0*ts)*(B0+B1*np.exp(-dE1*(ts-tau))+B2*np.exp(-dE1*tau)+B3*np.exp(-dE1*ts)+C1*np.exp(-dE2*(ts-tau))+C2*np.exp(-dE2*tau))

#####################################################
###################gpd c3pt fit######################
#####################################################
#Ein is like [A0, E0, A1, E1, ...]
def gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    NT=64
    part00 = B00*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-Eout[1]*tau-Ein[1]*(ts-tau))
    part01 = B01*np.sqrt(Eout[0])*np.sqrt(Ein[2])*np.exp(-Eout[1]*tau-Ein[3]*(ts-tau))
    part10 = B10*np.sqrt(Eout[2])*np.sqrt(Ein[0])*np.exp(-Eout[3]*tau-Ein[1]*(ts-tau))
    part11 = B11*np.sqrt(Eout[2])*np.sqrt(Ein[2])*np.exp(-Eout[3]*tau-Ein[3]*(ts-tau))
    #part_thermo_1 = B_thermo_1*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*(ts-tau)-Eout[1]*(NT-ts))
    p000 = np.sqrt(Eout[1]**2 - Ein[1]**2)/2
    E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    #print(Ein[1], Eout[1], Ein[1]+Eout[1],E11)
    part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-E11*tau-Ein[1]*(NT-ts))
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-Ein[1]*(NT-ts))
    part_thermo_3 = B_thermo_3*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    return part00 + part01 + part10 + part11 + part_thermo_2  + part_thermo_3

def gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=0, B_thermo_3=0, mass_in=[], mass_out=[]):
    ts,tau = tstau
    NT=64
    #print(2*np.sqrt(Ein[1]*Eout[1])/(Ein[1]+Eout[1]))
    part00 = B00*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-Eout[1]*tau-Ein[1]*(ts-tau))
    part01 = B01*np.sqrt(Eout[0])*np.sqrt(Ein[2])*np.exp(-Eout[1]*tau-Ein[3]*(ts-tau))
    part10 = B10*np.sqrt(Eout[2])*np.sqrt(Ein[0])*np.exp(-Eout[3]*tau-Ein[1]*(ts-tau))
    part11 = B11*np.sqrt(Eout[2])*np.sqrt(Ein[2])*np.exp(-Eout[3]*tau-Ein[3]*(ts-tau))
    part02 = B02*np.sqrt(Eout[0])*np.sqrt(Ein[4])*np.exp(-Eout[1]*tau-Ein[5]*(ts-tau))
    part20 = B20*np.sqrt(Eout[4])*np.sqrt(Ein[0])*np.exp(-Eout[5]*tau-Ein[1]*(ts-tau))
    part12 = B12*np.sqrt(Eout[2])*np.sqrt(Ein[4])*np.exp(-Eout[3]*tau-Ein[5]*(ts-tau))
    part21 = B21*np.sqrt(Eout[4])*np.sqrt(Ein[2])*np.exp(-Eout[5]*tau-Ein[3]*(ts-tau))
    part22 = B22*np.sqrt(Eout[4])*np.sqrt(Ein[4])*np.exp(-Eout[5]*tau-Ein[5]*(ts-tau))
    #part_thermo_1 = B_thermo_1*Ein[0]*np.exp(-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    p000 = np.sqrt(abs(Eout[1]**2 - Ein[1]**2))/2
    E11 = 2*np.sqrt(p000**2+Ein[1]**2)
    part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-E11*tau-Ein[1]*(NT-ts))
    #part_thermo_2 = B_thermo_2*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-Ein[1]*(NT-ts))
    part_thermo_3 = B_thermo_3*np.sqrt(Eout[0])*np.sqrt(Ein[0])*np.exp(-(Ein[1]+Eout[1])*tau-2*Ein[1]*(ts-tau)-Ein[1]*(NT-ts))
    return part00 + part01 + part10 + part11 + part02 + part20 + part12 + part21+ part22 + part_thermo_2  + part_thermo_3

def gpd_ratio_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_1=0, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #print(Ein)
    part1 = gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2, B_thermo_3)/twostate(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part2 = twostate(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3])*twostate(tau, Ein[0],Ein[1],Ein[2],Ein[3])*twostate(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part3 = twostate(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3])*twostate(tau, Eout[0],Eout[1],Eout[2],Eout[3])*twostate(ts, Eout[0],Eout[1],Eout[2],Eout[3])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    #B02 = 0
    #B20 = 0
    #B22=0
    part1 = gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2, B_thermo_3)/threestate(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part2 = threestate(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threestate(tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threestate(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part3 = threestate(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threestate(tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threestate(ts, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_two_plot(tstau,Ain0,Ein0,Ain1,Ein1,Aout0,Eout0,Aout1,Eout1,B00,B01,B10,B11, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    tau = tau+ts/2
    tstau=(ts,tau)
    Ein = [Ain0,Ein0,Ain1,Ein1]
    Eout = [Aout0,Eout0,Aout1,Eout1]
    part1 = gpd_c3pt_twostate(tstau, Ein, Eout, B00, B01, B10, B11, B_thermo_2=B_thermo_2, B_thermo_3=B_thermo_3)/twoexp(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part2 = twoexp(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(tau, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(ts, Ein[0],Ein[1],Ein[2],Ein[3])
    part3 = twoexp(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3])*twoexp(tau, Eout[0],Eout[1],Eout[2],Eout[3])*twoexp(ts,Eout[0],Eout[1],Eout[2],Eout[3])
    return part1*np.sqrt(part2/part3)

def gpd_ratio_three_plot(tstau, Ain0,Ein0,Ain1,Ein1,Ain2,Ein2,Aout0,Eout0,Aout1,Eout1,Aout2,Eout2, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=0, B_thermo_3=0):
    ts,tau = tstau
    tau = tau+ts/2
    tstau=(ts,tau)
    Ein = [Ain0,Ein0,Ain1,Ein1,Ain2,Ein2]
    Eout = [Aout0,Eout0,Aout1,Eout1,Aout2,Eout2]
    #B02 = 0
    #B20 = 0
    part1 = gpd_c3pt_threestate(tstau, Ein, Eout, B00, B01, B10, B11, B02, B20, B12, B21, B22, B_thermo_2=B_thermo_2, B_thermo_3=B_thermo_3)/threeexp(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part2 = threeexp(ts-tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threeexp(tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threeexp(ts, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])
    part3 = threeexp(ts-tau, Ein[0],Ein[1],Ein[2],Ein[3],Ein[4],Ein[5])*threeexp(tau, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])*threeexp(ts, Eout[0],Eout[1],Eout[2],Eout[3],Eout[4],Eout[5])
    return part1*np.sqrt(part2/part3)


#####################################################
###############    rITD joint fit    ################
#####################################################

def form_factor(t, *ak):
    tcut = 4*0.3*0.3
    to = tcut*(1-np.sqrt(1+0.353/tcut))
    z = (np.sqrt(tcut-t)-np.sqrt(tcut-to))/(np.sqrt(tcut-t)+np.sqrt(tcut-to))
    print(z,len(z),to)
    fmfct = 1
    for i in range(0, len(ak)):
        fmfct += ak[i] * (z**(i+1))
    return fmfct

def polynomial_form_factor(t, *ak):
    t=-t
    fmfct = 1
    for i in range(0, len(ak)):
        fmfct += ak[i] * t**(i+1)
    return fmfct
