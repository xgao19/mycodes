#!/usr/bin/env python3

import numpy as np
from scipy import integrate
from scipy.special import spence

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Constants ######################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

f_pi = 0.13 # GeV
CF = 4/3
nf = 3
beta0 = 11 - 2/3*nf

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ################## Auxiliary functions #################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def Li2(z):
    return spence(complex(1-z))
def H(u,v):
    ubar = 1-u
    vbar = 1-v
    return 1/(1-u-v) * (Li2(vbar/u) + Li2(ubar/v) + Li2(u*v/(ubar*vbar)) - Li2(u/vbar) - Li2(v/ubar) - Li2(ubar*vbar/(u*v)))
    #return (Li2(vbar/u) + Li2(ubar/v) + Li2(u*v/(ubar*vbar)) - Li2(u/vbar) - Li2(v/ubar) - Li2(ubar*vbar/(u*v)))
def R(u,v):
    ubar = 1-u
    vbar = 1-v
    return ((1/(u-v)**2 * ((2*u*v-u-v)*(np.log(u)+np.log(v)) + (-2*u*v**2-2*v**2+10*u*v-2*v-4*u**2)*np.log(vbar)/v + (-2*v*u**2-2*u**2+10*u*v-2*u-4*v**2)*np.log(ubar)/u - (v*vbar**2+u*ubar**2)*H(u,vbar)))).real
def f_UV(x, y, Qsq, muR):
    xbar = 1-x
    ybar = 1-y
    return 1/4*beta0 * (5/3 - np.log(xbar*ybar) + np.log(muR**2/Qsq))
def f_IR(x, y, Qsq, muF):
    xbar = 1-x
    ybar = 1-y
    return 2/3*(3+np.log(xbar*ybar)) * (1/2*np.log(xbar*ybar) - np.log(muF**2/Qsq))
def f_C(x,y):
    xbar = 1-x
    ybar = 1-y
    #return 1/12 * (-34+12*np.log(xbar*ybar)+np.log(x)*np.log(y) + np.log(xbar)*np.log(ybar)-np.log(x)*np.log(ybar)-np.log(xbar)*np.log(y) + (1-x-y)*H(x,y)+R(x,y))
    #return 1/12 * (-34+12*np.log(xbar*ybar)+np.log(x)*np.log(y) + np.log(xbar)*np.log(ybar)-np.log(x)*np.log(ybar)-np.log(xbar)*np.log(y) + H(x,y)+R(x,y))
    return 1/12 * (-34+12*np.log(xbar*ybar)+np.log(x)*np.log(y) + np.log(xbar)*np.log(ybar)-np.log(x)*np.log(ybar)-np.log(xbar)*np.log(y) + (1-x-y)*H(x,y)+R(x,y))
def T_0(x, y, Qsq):
    return 4/3 * 16*np.pi / (Qsq*(1-x)*(1-y))
def T_1(x, y, Qsq, muR, muF):
    return f_UV(x, y, Qsq, muR) + f_IR(x, y, Qsq, muF) + f_C(x,y)



def C_Gegenbauer_n(x, alpha, n):
    if n == 0:
        return 1
    elif n == 1:
        return 2 * alpha * x
    else:
        return 1/n * (2*x*(n+alpha-1)*C_Gegenbauer_n(x, alpha, n-1) - (n+2*alpha-2)*C_Gegenbauer_n(x, alpha, n-2))
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

# mu GeV
def alpsmu(mu, Nf = 3):
    #mu /= 5.0676896
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/0.332**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ##################### Main functions ###################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def DA_pion_as_LO(x, muF):
    return 6*x*(1-x)

def DA_pion_LO(x, muF, Bn, mu0):
    sum = 1
    for i in range(0, len(Bn)):
        n = (i+1)*2
        gamma_n = CF * (3 + 2/(n+1)/(n+2) - 4*Hn(n+1))
        sum += Bn[i]*(alpsmu(mu0)/alpsmu(muF))**(gamma_n/beta0) * C_Gegenbauer_n(2*x-1, 3/2, n)
    return 6*x*(1-x) * sum

def F0(Qsq, muR, muF, DA, Bn, mu0):
    intg = integrate.dblquad(lambda x, y: alpsmu(muR)*DA(x,muF,Bn,mu0)*DA(y,muF,Bn,mu0)/((1-x)*(1-y)), 0, 1, 0, 1)
    #print(integrate.dblquad(lambda x, y: DA(x,muF)*DA(y,muF)/((1-x)*(1-y)), 0, 1, 0, 1))
    return 8/9*np.pi * f_pi*f_pi/Qsq * intg[0]
def FF_DA_0(Qsq, muR, muF, DA, Bn, mu0):
    print('>>>', alpsmu(muR) * f_pi/np.sqrt(24))
    def intg(x, y):
        return alpsmu(muR) * f_pi/np.sqrt(24)*DA(y,muF,Bn,mu0) * T_0(x, y, Qsq) * f_pi/np.sqrt(24)*DA(x,muF,Bn,mu0)
    return integrate.dblquad(intg, 0, 1, 0, 1)[0]
def FF_DA_1a(Qsq, muR, muF, DA, Bn, mu0):
    def intg(x, y):
        return (alpsmu(muR)**2/np.pi * f_pi/np.sqrt(24)*DA(y,muF,Bn,mu0) * T_0(x, y, Qsq) * T_1(x, y, Qsq, muR, muF) * f_pi/np.sqrt(24)*DA(x,muF,Bn,mu0))
    #print('>>>',intg(0.2,0.3))
    return integrate.dblquad(intg, 0, 1, 0, 1)[0]

qsq = 20
print(alpsmu(np.sqrt(qsq)))
#print(qsq*F0(qsq, np.sqrt(0.25*qsq), np.sqrt(qsq), DA_pion_LO, [2/3], 0.5))
print(qsq*FF_DA_0(qsq, np.sqrt(0.25*qsq), np.sqrt(qsq), DA_pion_LO, [2/3], 0.5))
#print(f_C(0.2,0.3), T_1(0.2,0.3, qsq, np.sqrt(0.25*qsq), np.sqrt(qsq)))
#print(qsq*FF_DA_1a(qsq, np.sqrt(0.25*qsq), np.sqrt(qsq), DA_pion_LO, [2/3], 0.5))
#print(H(0.2,0.3),R(0.2,0.3))
#print(integrate.dblquad(R, 0, 1, 0, 1)[0])