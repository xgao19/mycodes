#!/usr/bin/env python3
import numpy as np
import h5py
import sys
import os
from utils.tools import *
from scipy.optimize import least_squares

"""
================================================================================
                            Constants and Functions
================================================================================
"""

TSIZE = 128
def c2pt_Nstate(x, AE):
    c2pt = 0
    for i in range(0, int(len(AE)/2)):
        A = AE[2*i]
        E = AE[2*i+1]
        c2pt += A * (np.exp(-E * x)+np.exp(-E * (TSIZE-x)))
    return c2pt
def qTMDWF_Nstate(x, AE, mx):
    qTMDWF = 0
    for i in range(0, int(len(AE)/2)):
        A = AE[2*i]
        E = AE[2*i+1]
        qTMDWF += np.sqrt(A*2*E) * mx[i]/2 * (np.exp(-E * x)+np.exp(-E * (TSIZE-x)))
    return qTMDWF
def qTMDWF_ratio_Nstate(x, AE, mx):
    return qTMDWF_Nstate(x, AE, mx)/c2pt_Nstate(x, AE)

def read_qTMDWF_h5(file, Tdirlist, bTlist, bzlist, phase):
    #print(phase)
    signlist = [-1 for i in range(0, 64)] + [1 for i in range(0, 64)]
    f = h5py.File(file,'r')
    data = []
    for ib, bT in enumerate(bTlist):
        ib_path = '/bT' + str(bT)
        ib_data = []
        for iz, bz in enumerate(bzlist):
            iz_path = ib_path + '/bz' + str(bz)
            iz_data = []
            for iT, Tdir in enumerate(Tdirlist):
                iT_path = Tdir + iz_path
                print(iT_path)
                iT_data = np.array(f[iT_path])
                iT_data_new = []
                for row in iT_data:
                    #iT_data_new += [[(row[i].real)*signlist[i] for i in range(0, len(row))]]
                    iT_data_new += [[(row[i]*np.exp(complex(0, -phase*bz))).real*signlist[i] for i in range(0, len(row))]]
                iz_data += [iT_data_new]
            iz_data = ave_2d_lists(iz_data)
            ib_data += [folddat(np.transpose(iz_data))]
        data += [ib_data]
    return data

def boots_pick(samples, npick):
    np.random.seed(2020)
    random_mx = np.array(list(map(int,np.random.uniform(0,len(samples),npick*npick))))
    boots_samples = np.array(samples)[random_mx]
    return np.mean(boots_samples.reshape(npick,npick), axis=1)

def qTMDWF_mx_ratio_boots(c2pt, qTMDWF, bTlist, bzlist, npick):

    boots_c2pt = []
    for i in range(0, len(c2pt)):
        boots_c2pt += [boots_pick(c2pt[i], npick)]
    
    boots_qTMDWF = []
    boots_ratio = []
    for ib, bT in enumerate(bTlist):
        ib_qTMDWF = []
        ib_ratio = []
        for iz, bz in enumerate(bzlist):
            iz_qTMDWF = []
            iz_ratio = []
            for i in range(0, len(qTMDWF[0][0])):
                i_qTMDWF = boots_pick(qTMDWF[ib][iz][i], npick)
                iz_qTMDWF += [i_qTMDWF]
                iz_ratio += [i_qTMDWF/boots_c2pt[i]]
                #if ib == 0 and iz == 0 and (i_qTMDWF/boots_c2pt[i])[0]<0 and i == 2:
                #    print(bT,bz, i, i_qTMDWF, boots_c2pt[i], i_qTMDWF/boots_c2pt[i])
            ib_qTMDWF += [iz_qTMDWF]
            ib_ratio += [iz_ratio]
        boots_qTMDWF += [ib_qTMDWF]
        boots_ratio += [ib_ratio]
    #print(np.shape(boots_c2pt), np.shape(boots_qTMDWF), np.shape(boots_ratio))

    return boots_ratio




"""
================================================================================
                                    Analysis
================================================================================
"""

if __name__ == "__main__":

    # read the job describe and commands
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    describe = (commands[0].split())[0]
    NS = int((commands[0].split())[1])
    NT = int((commands[0].split())[2])
    blocksize = int((commands[0].split())[3])

    # create the work folders here
    datafolder = os.path.dirname(sys.argv[1])
    resultfolder = datafolder + '/results/'   # default, mkdir at the input file folder

    print('\nJob describe:', describe, '\nNS:', NS, 'NT:', NT, 'Bootstrap bin size:', blocksize)
    phase = 0

    for index in range(1, len(commands)):

        line = commands[index].split()

        if 'npick' == line[0]:
            npick = int(line[1])
            low, mid, up = int(0.16*npick), int(0.5*npick), int(0.84*npick)
            print('Bootstrap sample number: npick =', npick)

        elif 'bxp' == line[0]:
            bxp = line[1]
            print('bxp:', bxp)
            describe = describe.replace('bxp', bxp)

        elif 'gm' == line[0]:
            gmlist = [line[i] for i in range(1, len(line))]
            print('gmlist:', gmlist)

        elif 'eta' == line[0]:
            etalist = [line[i] for i in range(1, len(line))]
            print('etalist', etalist)

        elif 'pz' == line[0]:
            pzlist = [int(line[i]) for i in range(1, len(line))]
            print('pzlist:', pzlist)
        
        elif 'Tdir' == line[0]:
            Tdirlist = [line[i] for i in range(1, len(line))]
            print('Tdir list:', Tdirlist)
        elif 'bT' == line[0]:
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bT list:', bTlist)
        elif 'bz' == line[0]:
            bzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bz list:', bzlist)

        elif 'c2pt' == line[0]:
            c2pt_format = line[1]
            c2pt_format = c2pt_format.replace('bxp', bxp)
            print('2pt format:', c2pt_format.split('/')[-1])

        elif 'qTMDWF' == line[0]:
            qTMDWF_format = line[1]
            qTMDWF_format = qTMDWF_format.replace('bxp', bxp)
            print('qTMDWF format:', qTMDWF_format.split('/')[-1])
        
        elif 'disp' == line[0]:
            disp = np.loadtxt(line[1])
            
            AE_MeanErr = []
            for iAE in range(0, 4):
                iAE_samples = sorted([disp[isamp][iAE] for isamp in range(0, len(disp))])
                AE_MeanErr += [iAE_samples[int(0.5*len(iAE_samples))]]
            print('Dispersion relation:', line[1].split('/')[-1], ', in shape:', np.shape(disp), ', Mean Err:', AE_MeanErr)

        elif 'trange' == line[0]:
            tmin = int(line[1])
            tmax = int(line[2])
            print('Fit tmin =', tmin, ', tmax =', tmax)
    
    if 'ratio' in describe:

        resultfolder = resultfolder.replace('results', 'results_ratio')
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)

        for ig, gm in enumerate(gmlist):
            ig_describe = describe.replace('gm', gm)
            ig_qTMDWF_format = qTMDWF_format.replace('gm', gm)
            for ie, eta in enumerate(etalist):
                ie_describe = ig_describe.replace('eta', 'eta'+str(eta))
                ie_qTMDWF_format = ig_qTMDWF_format.replace('eta', 'eta'+str(eta))
                for ip, pz in enumerate(pzlist):

                    # NOTE!!! DA phase
                    phase = 2*np.pi / NS * pz / 2

                    ip_describe = ie_describe.replace('*', str(pz))
                    ip_c2pt_file = c2pt_format.replace('*', str(pz))
                    ip_qTMDWF_file = ie_qTMDWF_format.replace('*', str(pz))

                    ip_c2pt = csvtolist(ip_c2pt_file)
                    ip_c2pt = [ip_c2pt[i][1:] for i in range(0, len(ip_c2pt))]
                    ip_c2pt = folddat(ip_c2pt)
                    print(ip_qTMDWF_file)
                    ip_qTMDWF = read_qTMDWF_h5(ip_qTMDWF_file, Tdirlist, bTlist, bzlist, phase)
                    print('\n',ip_describe, ':')
                    print('\tc2pt shape:', np.shape(ip_c2pt))
                    print('\tqTMDWF shape:', np.shape(ip_qTMDWF))
                    #print(ip_qTMDWF[0][0][0],ip_qTMDWF[0][0][1],ip_qTMDWF[0][0][2])

                    ip_ratio_samples = qTMDWF_mx_ratio_boots(ip_c2pt, ip_qTMDWF, bTlist, bzlist, npick)
                    print('\tqTMDWF/c2pt ratio shape:', np.shape(ip_ratio_samples))

                    for ib, bT in enumerate(bTlist):
                        ib_describe = ip_describe + '_bT' + str(bT)
                        for iz, bz in enumerate(bzlist):

                            iz_describe_samples = ib_describe + '_bz' + str(bz) + '_samples'
                            iz_save_samples = []
                            for i in range(0, len(ip_ratio_samples[ib][iz])):
                                iz_save_samples += [[i] + list(ip_ratio_samples[ib][iz][i])]
                            np.savetxt(samplefolder+iz_describe_samples+'.dat',iz_save_samples,fmt='%.6e')

                            iz_describe = ib_describe + '_bz' + str(bz)
                            iz_MeanErr = []
                            for i in range(0, len(ip_ratio_samples[ib][iz])):
                                i_samples = sorted(ip_ratio_samples[ib][iz][i])
                                i_Err = (i_samples[up] - i_samples[low]) / 2
                                i_Mean = (i_samples[up] + i_samples[low]) / 2
                                iz_MeanErr += [[i, i_Mean, i_Err]]
                            np.savetxt(resultfolder+iz_describe+'.dat',iz_MeanErr,fmt='%.6e')

    elif 'nst1' in describe:

        resultfolder = resultfolder.replace('results', 'results_fit_nst1')
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)

        for ig, gm in enumerate(gmlist):
            ig_describe = describe.replace('gm', gm)
            ig_qTMDWF_format = qTMDWF_format.replace('gm', gm)
            for ie, eta in enumerate(etalist):
                ie_describe = ig_describe.replace('eta', 'eta'+str(eta))
                ie_qTMDWF_format = ig_qTMDWF_format.replace('eta', 'eta'+str(eta))
                for ip, pz in enumerate(pzlist):

                    phase = 2*np.pi / NS * pz / 2

                    ip_describe = ie_describe.replace('*', str(pz))
                    ip_c2pt_file = c2pt_format.replace('*', str(pz))
                    ip_qTMDWF_file = ie_qTMDWF_format.replace('*', str(pz))

                    ip_c2pt = csvtolist(ip_c2pt_file)
                    ip_c2pt = [ip_c2pt[i][1:] for i in range(0, len(ip_c2pt))]
                    ip_c2pt = folddat(ip_c2pt)
                    ip_qTMDWF = read_qTMDWF_h5(ip_qTMDWF_file, Tdirlist, bTlist, bzlist, phase)
                    print('\n',ip_describe, ':')
                    print('\tc2pt shape:', np.shape(ip_c2pt))
                    print('\tqTMDWF shape:', np.shape(ip_qTMDWF))
                    #print(ip_qTMDWF[0][0][0],ip_qTMDWF[0][0][1],ip_qTMDWF[0][0][2])

                    ip_ratio_samples = qTMDWF_mx_ratio_boots(ip_c2pt, ip_qTMDWF, bTlist, bzlist, npick)
                    print('\tqTMDWF/c2pt ratio shape:', np.shape(ip_ratio_samples))

                    for ib, bT in enumerate(bTlist):
                        ib_describe = ip_describe + '_bT' + str(bT) + '_tmin' + str(tmin) + '_tmax' + str(tmax)
                        ib_describe = ip_describe + '_bT' + str(bT)
                        print('\tbT:', bT)

                        ib_MeanErr = []
                        for iz, bz in enumerate(bzlist):
                            iz_describe = ib_describe + '_bz' + str(bz)

                            iz_MeanErr = []
                            for i in range(0, len(ip_ratio_samples[ib][iz])):
                                i_samples = sorted(ip_ratio_samples[ib][iz][i])
                                i_Err = (i_samples[up] - i_samples[low]) / 2
                                i_Mean = (i_samples[up] + i_samples[low]) / 2
                                iz_MeanErr += [[i, i_Mean, i_Err]]

                            iz_popt_samples = []
                            for isamp in range(0, npick):
                                x = np.arange(tmin, tmax+1, 1)
                                y = np.array([ip_ratio_samples[ib][iz][i][isamp] for i in range(tmin, tmax+1)])
                                yerr = np.array([iz_MeanErr[i][2] for i in range(tmin, tmax+1)])
                                AE = disp[isamp][:2]
                                poptstart = [1]

                                def Fit_res(popt):
                                    data_diff = qTMDWF_ratio_Nstate(x, AE, popt) - y
                                    res = data_diff/yerr
                                    return res
                                res = least_squares(Fit_res, poptstart, method='trf')
                                popt = res.x
                                chisq = sum(np.square(res.fun[:])) / (len(y)-len(popt))
                                iz_popt_samples += [list(popt) + [chisq]]
                            np.savetxt(samplefolder+iz_describe+'_bootsamples.dat',iz_popt_samples,fmt='%.6e')

                            iz_MeanErr = [bz, tmin, tmax]
                            for ip in range(0, len(iz_popt_samples[0])):
                                ip_samples = sorted([iz_popt_samples[i][ip] for i in range(0, len(iz_popt_samples))])
                                iz_MeanErr += [(ip_samples[up]+ip_samples[low])/2,(ip_samples[up]-ip_samples[low])/2]
                            print('\t',["%.2e"%iz_MeanErr[i] for i in range(0, len(iz_MeanErr))])
                            ib_MeanErr += [iz_MeanErr]
                        np.savetxt(resultfolder+ib_describe+'.dat',ib_MeanErr,fmt='%.6e')

    elif 'nst2' in describe:

        resultfolder = resultfolder.replace('results', 'results_fit_nst2')
        mkdir(resultfolder)
        samplefolder = resultfolder + '/samples/'
        mkdir(samplefolder)
        bandfolder = resultfolder + '/bands/'
        mkdir(bandfolder)

        xlist = np.arange(0, 30, 0.1)

        for ig, gm in enumerate(gmlist):
            ig_describe = describe.replace('gm', gm)
            ig_qTMDWF_format = qTMDWF_format.replace('gm', gm)
            for ie, eta in enumerate(etalist):
                ie_describe = ig_describe.replace('eta', 'eta'+str(eta))
                ie_qTMDWF_format = ig_qTMDWF_format.replace('eta', 'eta'+str(eta))
                for ip, pz in enumerate(pzlist):

                    phase = 2*np.pi / NS * pz / 2

                    ip_describe = ie_describe.replace('*', str(pz))
                    ip_c2pt_file = c2pt_format.replace('*', str(pz))
                    ip_qTMDWF_file = ie_qTMDWF_format.replace('*', str(pz))

                    ip_c2pt = csvtolist(ip_c2pt_file)
                    ip_c2pt = [ip_c2pt[i][1:] for i in range(0, len(ip_c2pt))]
                    ip_c2pt = folddat(ip_c2pt)
                    ip_qTMDWF = read_qTMDWF_h5(ip_qTMDWF_file, Tdirlist, bTlist, bzlist, phase)
                    print('\n',ip_describe, ':')
                    print('\tc2pt shape:', np.shape(ip_c2pt))
                    print('\tqTMDWF shape:', np.shape(ip_qTMDWF))
                    #print(ip_qTMDWF[0][0][0],ip_qTMDWF[0][0][1],ip_qTMDWF[0][0][2])

                    ip_ratio_samples = qTMDWF_mx_ratio_boots(ip_c2pt, ip_qTMDWF, bTlist, bzlist, npick)
                    print('\tqTMDWF/c2pt ratio shape:', np.shape(ip_ratio_samples))

                    for ib, bT in enumerate(bTlist):
                        ib_describe = ip_describe + '_bT' + str(bT) + '_tmin' + str(tmin) + '_tmax' + str(tmax)
                        ib_describe = ip_describe + '_bT' + str(bT)
                        print('\tbT:', bT)

                        ib_MeanErr = []
                        for iz, bz in enumerate(bzlist):
                            iz_describe = ib_describe + '_bz' + str(bz)

                            iz_MeanErr = []
                            for i in range(0, len(ip_ratio_samples[ib][iz])):
                                i_samples = sorted(ip_ratio_samples[ib][iz][i])
                                i_Err = (i_samples[up] - i_samples[low]) / 2
                                i_Mean = (i_samples[up] + i_samples[low]) / 2
                                iz_MeanErr += [[i, i_Mean, i_Err]]
                            
                            #print(bz, iz_MeanErr)

                            iz_popt_samples = []
                            iz_band_samples = []
                            for isamp in range(0, npick):
                                x = np.arange(tmin, tmax+1, 1)
                                y = np.array([ip_ratio_samples[ib][iz][i][isamp] for i in range(tmin, tmax+1)])
                                yerr = np.array([iz_MeanErr[i][2] for i in range(tmin, tmax+1)])
                                AE = disp[isamp][:4]
                                if any(i < 0 for i in AE):
                                    AE = AE_MeanErr
                                poptstart = [0.1,-0.1]

                                def Fit_res(popt):
                                    data_diff = qTMDWF_ratio_Nstate(x, AE, popt) - y
                                    res = data_diff/yerr
                                    return res
                                res = least_squares(Fit_res, poptstart, method='trf')
                                popt = res.x
                                chisq = sum(np.square(res.fun[:])) / (len(y)-len(popt))
                                iz_popt_samples += [list(popt) + [chisq]]
                                iz_band_samples += [qTMDWF_ratio_Nstate(xlist, AE, popt)]
                            np.savetxt(samplefolder+iz_describe+'_bootsamples.dat',iz_popt_samples,fmt='%.6e')

                            iz_MeanErr = [bz, tmin, tmax]
                            for ip in range(0, len(iz_popt_samples[0])):
                                ip_samples = sorted([iz_popt_samples[i][ip] for i in range(0, len(iz_popt_samples))])
                                iz_MeanErr += [(ip_samples[up]+ip_samples[low])/2,(ip_samples[up]-ip_samples[low])/2]
                            print('\t',["%.2e"%iz_MeanErr[i] for i in range(0, len(iz_MeanErr))])
                            ib_MeanErr += [iz_MeanErr]

                            iz_band0 = []
                            iz_band1 = []
                            iz_band2 = []
                            for ix, x in enumerate(xlist):
                                ix_samples = sorted([iz_band_samples[isamp][ix] for isamp in range(0, len(iz_band_samples))])
                                iz_band0 += [[x, ix_samples[low]]]
                                iz_band1 += [[x, ix_samples[mid]]]
                                iz_band2 += [[x, ix_samples[up]]]
                            np.savetxt(bandfolder+ib_describe+'_bz'+str(bz)+'_band0.dat',iz_band0,fmt='%.6e')
                            np.savetxt(bandfolder+ib_describe+'_bz'+str(bz)+'_band1.dat',iz_band1,fmt='%.6e')
                            np.savetxt(bandfolder+ib_describe+'_bz'+str(bz)+'_band2.dat',iz_band2,fmt='%.6e')

                        np.savetxt(resultfolder+ib_describe+'.dat',ib_MeanErr,fmt='%.6e')
                                


                            
