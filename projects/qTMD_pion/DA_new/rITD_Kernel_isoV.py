#!/usr/bin/env python3
import numpy as np
from  scipy.special import gamma
from  scipy.special import jv

from qGPD_nucleon.Constants import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Auxiliary functions ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def alphas(mu, Nf = 3):
    return alphas_5loop(mu, Nf)

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

def gamma_n(n, alps):
    return alps*CF/(4*np.pi) * (4*Hn(n+1) - 2/(n+1)/(n+2) - 3)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#mu in GeV
def cnNLO_T5_C(n, alps):
    return 1 + alps*CF/(2*np.pi) * ((5+2*n)/(2+3*n+n*n) + 2*(1-Hn(n))*Hn(n) - 2*Hn2(n))

def FnLO_T5_C(n, mu, alps, lbd, z):
    alps = 0
    gO = alps*CF/(4*np.pi) * 3 #gamma_O
    gn = gamma_n(n, alps) #gamma_n
    part1 = cnNLO_T5_C(n, alps) * (mu*mu*z*z)**(gn+gO) * gamma(2-gn)*gamma(1+n)/gamma(1+n+gn)
    part2 = 3/4*(complex(0,1))**n*np.sqrt(np.pi) * (n+1)*(n+2)/2 * gamma(n+gn+5/2)/gamma(n+5/2)
    part3 = (lbd/2)**(-3/2-gn) * jv(n+gn+3/2, lbd)
    #print(part1, part2, part3)
    #print('LO',n, lbd, (lbd/2)**(-3/2) * jv(n+gn+3/2, lbd))
    return (part1 * part2 * part3).real

def FnNLO_T5_C(n, mu, alps, lbd, z):
    gO = alps*CF/(4*np.pi) * 3 #gamma_O
    gn = gamma_n(n, alps) #gamma_n
    part1 = cnNLO_T5_C(n, alps) * (mu*mu*z*z)**(gn+gO) * gamma(2-gn)*gamma(1+n)/gamma(1+n+gn)
    part2 = 3/4*complex(0,1)**n*np.sqrt(np.pi) * (n+1)*(n+2)/2 * gamma(n+gn+5/2)/gamma(n+5/2)
    part3 = (lbd/2)**(-3/2-gn) * jv(n+gn+3/2, lbd)
    #print('NLO',(part1 * part2 * part3).real)
    return (part1 * part2 * part3).real

#L = np.log(mu*mu*z*z*np.exp(2*gammaE)/4)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ########################## rITD kernels ############################ ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def FnF0LO_T5_C(n,mu,lbd,z):
    return 1
def FnF0LO_T5_C(n,mu,lbd,z):
    return FnNLO_T5_C(n, mu, alphas(mu), lbd, z)/FnNLO_T5_C(0, mu, alphas(mu), lbd, z)