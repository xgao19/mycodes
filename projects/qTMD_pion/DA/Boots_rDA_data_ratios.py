#!/usr/bin/env python3
import numpy as np
import sys
from utils.tools import *
from statistics import*


def bootsratio_to_rbm(up_sample,down_sample,pzlist,bTlist,zlist,outfolder,describe):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    for ipz, pz in enumerate(pzlist):

        ipz_describe = describe.replace('*', str(pz))


        ipz_upsample = up_sample.replace('*', str(pz))
        ipz_downsample = down_sample.replace('*', str(pz))

        for ibT, bT in enumerate(bTlist):

            ibT_describe = ipz_describe + '_bT' + str(bT)

            ibT_upsample = ipz_upsample.replace('@', str(bT))
            ibT_downsample = ipz_downsample.replace('@', str(bT))
            ibT_downdata = np.loadtxt(ibT_downsample)
            npick = len(ibT_downdata[0]) - 1
            low = int(0.16*npick)
            mid = int(0.5*npick)
            up = int(0.84*npick)

            ibT_ratios = []
            for iz, z in enumerate(zlist):

                iz_describe = ibT_describe + '_bz' + str(z)

                iz_upsamples = ibT_upsample.replace('#',str(z))
                iz_updata = np.loadtxt(iz_upsamples)
                #print(np.shape(iz_updata), np.shape(ibT_downdata))

                iz_ratios_samples = []
                iz_ratios_MeanErr = []
                for i in range(0, len(iz_updata)):
                    i_ratios = iz_updata[i][1:]/ibT_downdata[i][1:]
                    iz_ratios_samples += [[iz_updata[i][0]] + list(i_ratios)]

                    i_ratios_sorted = sorted(i_ratios)
                    iz_ratios_MeanErr += [[iz_updata[i][0]] + [(i_ratios_sorted[up]+i_ratios_sorted[low])/2, (i_ratios_sorted[up]-i_ratios_sorted[low])/2]]
                
                np.savetxt(outfolder+iz_describe+'.dat',iz_ratios_MeanErr,fmt='%.6e')
                np.savetxt(samplefolder+iz_describe+'_samples.dat',iz_ratios_samples,fmt='%.6e')

def bootsratio_to_ritd(up_sample,down_sample,pzlist,bTlist,zlist,outfolder,describe):

    samplefolder = outfolder + 'samples/'
    mkdir(samplefolder)

    for ipz, pz in enumerate(pzlist):

        ipz_describe = describe.replace('*', str(pz))


        ipz_upsample = up_sample.replace('*', str(pz))
        ipz_downsample = down_sample.replace('*', str(pz))

        for ibT, bT in enumerate(bTlist):

            ibT_describe = ipz_describe + '_bT' + str(bT)

            ibT_upsample = ipz_upsample.replace('@', str(bT))
            ibT_downsample = ipz_downsample.replace('@', str(bT))

            ibT_ratios = []
            for iz, z in enumerate(zlist):

                iz_describe = ibT_describe + '_bz' + str(z)

                iz_upsamples = ibT_upsample.replace('#',str(z))
                iz_updata = np.loadtxt(iz_upsamples)
                iz_downsamples = ibT_downsample.replace('#',str(z))
                iz_downdata = np.loadtxt(iz_downsamples)
                #print(np.shape(iz_updata), np.shape(ibT_downdata))

                npick = len(iz_updata[0]) - 1
                low = int(0.16*npick)
                mid = int(0.5*npick)
                up = int(0.84*npick)

                iz_ratios_samples = []
                iz_ratios_MeanErr = []
                for i in range(0, len(iz_updata)):
                    i_ratios = iz_updata[i][1:]/iz_downdata[i][1:]
                    iz_ratios_samples += [[iz_updata[i][0]] + list(i_ratios)]

                    i_ratios_sorted = sorted(i_ratios)
                    iz_ratios_MeanErr += [[iz_updata[i][0]] + [(i_ratios_sorted[up]+i_ratios_sorted[low])/2, (i_ratios_sorted[up]-i_ratios_sorted[low])/2]]
                
                np.savetxt(outfolder+iz_describe+'.dat',iz_ratios_MeanErr,fmt='%.6e')
                np.savetxt(samplefolder+iz_describe+'_samples.dat',iz_ratios_samples,fmt='%.6e')

                

if __name__ == "__main__":

    inputfile = open(sys.argv[2],'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[2])

    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    print('\n\n\n\n\n\n\njob describe:',describe)

    # read parameters
    lat = []
    for index in range(0, len(commands)):

        line = commands[index].split()

        if 'bzrange' in line[0]:
            zmin = int(line[1])
            zmax = int(line[2])
            zlist = [iz for iz in range(zmin, zmax+1)]
            print('zlist:',zlist)

        elif 'bTrange' in line[0]:
            bTmin = int(line[1])
            bTmax = int(line[2])
            bTlist = [ib for ib in range(bTmin, bTmax+1)]
            print('bTlist:', bTlist)

        elif 'pzrange' in line[0]:
            pzmin = int(line[1])
            pzmax = int(line[2])
            pzlist = [ipz for ipz in range(pzmin, pzmax+1)]
            print('pxlist:',pzlist)

        elif 'upsample' in line[0]:
            up_sample = line[1] 

        elif 'downsample' in line[0]:
            down_sample = line[1] 

    if sys.argv[1] == 'rbm':
        outfolder = inputfolder + '/results_ratios_rbm/'   # default, mkdir at the input file folder
        mkdir(outfolder)
        bootsratio_to_rbm(up_sample,down_sample,pzlist,bTlist,zlist,outfolder,describe)

    elif sys.argv[1] == 'ritd':
        outfolder = inputfolder + '/results_ratios_rITD/'   # default, mkdir at the input file folder
        mkdir(outfolder)
        bootsratio_to_ritd(up_sample,down_sample,pzlist,bTlist,zlist,outfolder,describe)