#!/usr/bin/env python3
import numpy as np
import os
from utils.tools import mkdir
from qTMD_pion.TMDWF_64I.Boots_CS_qTMDWF_matching_type2 import *

def data_collection(mean_format, sample_format, bTlist, pzlist):
    mean = []
    samples = []
    for iT, bT in enumerate(bTlist):
        iT_mean_format = mean_format.replace('@', str(bT))
        iT_sample_format = sample_format.replace('@', str(bT))
        iT_mean, iT_samples = [], []
        for ipz, pz in enumerate(pzlist):
            ipz_mean = np.loadtxt(iT_mean_format.replace('*', str(pz)))
            ipz_samples = np.loadtxt(iT_sample_format.replace('*', str(pz)))
            iT_mean += [ipz_mean]
            iT_samples += [ipz_samples]
        mean += [iT_mean]
        samples += [iT_samples]
    return mean, samples


'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":


    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    
    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    Nt = int(line1st[2])
    latsp = float(line1st[3])
    fmGeV = 5.0676896
    dP = 2*np.pi/(Ns*latsp*fmGeV)
    print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',round(latsp,3),'fm, dP =', round(dP,2), 'GeV.\n')
    
    # read parameters
    for index in range(1, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'mu':
            mu = float(line[1])
            #print('mu =', mu, ' GeV; alphas =', round(alphas(mu),3))

        elif line[0] == 'bTrange':
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bTlist:',bTlist)

        elif line[0] == 'pzrange':
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('pz list:',pzlist)

        elif line[0] == 'pskip':
            pskiplist = [int(line[i]) for i in range(1, len(line))]
            print('pskip list:', pskiplist)

        elif line[0] == 'kernel':
            kernellist = [line[i] for i in range(1, len(line))]
            print('kernel list:', kernellist)

        elif line[0] == 'mean':
            mean_format = line[1]
            print('\nmean:', mean_format.split('/')[-1])

        elif line[0] == 'sample':
            sample_format = line[1]
            print('sample:', sample_format.split('/')[-1],'\n')

    mean, samples = data_collection(mean_format, sample_format, bTlist, pzlist)
    xlist = [mean[0][0][ix][0] for ix in range(0, len(mean[0][0]))]
    npick = len(samples[0][0][0])
    low, mid, up = int(0.16*npick), int(0.5*npick), int(0.84*npick)
    print('Read data in shape:', np.shape(mean), np.shape(samples))

    for ik, kernel in enumerate(kernellist):
        ik_describe = describe.replace('kernel', kernel)
        if kernel in ['LO']:
            CS_correction = CS_Dgamma(mu, 0)
        elif kernel in ['NLO', 'NLL']:
            CS_correction = CS_Dgamma(mu, 1)
        elif kernel in ['NNLO', 'NNLL']:
            CS_correction = CS_Dgamma(mu, 2)
        print(kernel,'; mu =', mu, ' GeV; alphas =', round(CS_correction.alphas(mu),3))

        for iT, bT in enumerate(bTlist):
            iT_describe = ik_describe.replace('@', str(bT))

            for iskip, pskip in enumerate(pskiplist):
                iskip_describe = iT_describe.replace('Pskip', 'Pskip'+str(pskip))

                for ipz in range(pskip, len(pzlist)):
                    pz1, pz2 = pzlist[ipz-pskip], pzlist[ipz]
                    pz1_data, pz2_data = np.array(samples[iT][ipz-1]), np.array(samples[iT][ipz])
                    ipz_describe = iskip_describe.replace('*', str(pz2))
                    pz1, pz2 = pz1*dP, pz2*dP

                    ipz_CS_samples = []
                    xlist_new = []
                    for ix, x in enumerate(xlist):

                        if x >= 0.1 and x <=0.9:
                            xlist_new += [x]
                            if 'CG' in describe:
                                if 'LL' not in kernel:
                                    ix_CS_samples = 1/np.log(pz1/pz2) * np.log(abs(pz1_data[ix]/pz2_data[ix])) + CS_correction.Dgamma_CG(pz1, pz2, x, 'real')
                                elif 'LL' in kernel:
                                    ix_CS_samples = 1/np.log(pz1/pz2) * np.log(abs(pz1_data[ix]/pz2_data[ix])) + CS_correction.Dgamma_CG_RG(pz1, pz2, x, 'real')
                            elif 'real' in describe:
                                if 'LL' not in kernel:
                                    ix_CS_samples = 1/np.log(pz1/pz2) * np.log(abs(pz1_data[ix]/pz2_data[ix])) + CS_correction.Dgamma_GI(pz1, pz2, x, 'real')
                                elif 'LL' in kernel:
                                    ix_CS_samples = 1/np.log(pz1/pz2) * np.log(abs(pz1_data[ix]/pz2_data[ix])) + CS_correction.Dgamma_GI_RG(pz1, pz2, x, 'real')
                            elif 'imag' in describe:
                                if 'LL' not in kernel:
                                    ix_CS_samples = 1/np.log(pz1/pz2) * np.log(abs(pz1_data[ix]/pz2_data[ix])) + CS_correction.Dgamma_GI(pz1, pz2, x, 'imag')
                                elif 'LL' in kernel:
                                    ix_CS_samples = 1/np.log(pz1/pz2) * np.log(abs(pz1_data[ix]/pz2_data[ix])) + CS_correction.Dgamma_GI_RG(pz1, pz2, x, 'imag')
                            ipz_CS_samples += [ix_CS_samples]

                    # save mean err bands
                    ipz_band0, ipz_band1, ipz_band2 = [], [], []
                    for ix, x in enumerate(xlist_new):
                        ix_CS_samples = sorted(ipz_CS_samples[ix])
                        ipz_band0 += [[x, ix_CS_samples[low]]]
                        ipz_band1 += [[x, ix_CS_samples[mid]]]
                        ipz_band2 += [[x, ix_CS_samples[up]]]
                    savename = bandfolder + ipz_describe + '_CSx_band0.dat'
                    np.savetxt(savename, ipz_band0, fmt='%.6e')
                    savename = bandfolder + ipz_describe + '_CSx_band1.dat'
                    np.savetxt(savename, ipz_band1, fmt='%.6e')
                    savename = bandfolder + ipz_describe + '_CSx_band2.dat'
                    np.savetxt(savename, ipz_band2, fmt='%.6e')

                    # save samples
                    savename = samplefolder + ipz_describe + '_CSx_samples.dat'
                    np.savetxt(savename, ipz_CS_samples, fmt='%.6e')
                    print('Save data: bT =', bT, '; P1 =', round(pz1,3), '/ P2 =', round(pz2,3))