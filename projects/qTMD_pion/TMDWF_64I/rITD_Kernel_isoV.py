#!/usr/bin/env python3
import numpy as np
import numba as nb
from qGPD_nucleon.OPE.rITD_Numerical_Kernel_isoV import *
from qGPD_nucleon.Constants import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Auxiliary functions ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def alphas(mu, Nf = 3):
    return alphas_5loop(mu, Nf)

@nb.jit
def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#mu in GeV
def CnNLO(z,n, mu,alps):
    L = np.log(mu*mu*z*z*np.exp(2*gammaE)/4)
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*L\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

#mu in GeV
def CnNLOpol(z, n, mu,alps):
    L = np.log(mu*mu*z*z*np.exp(2*gammaE)/4)
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*L\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)) + alps*CF/(2*np.pi)*2/(2+3*n+n*n)


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ########################## rITD kernels ############################ ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def CnC0LO(mu,z,n):
    return 1
def CnC0NLO(mu,z,n):
    return CnNLO(z*fmGeV, n, mu, alphas(mu))/CnNLO(z*fmGeV, 0, mu, alphas(mu))
def CnC0NLOpol(mu,z,n):
    return CnNLOpol(z*fmGeV, n, mu, alphas(mu))/CnNLOpol(z*fmGeV, 0, mu, alphas(mu))
def CnC0NNLO(mu,z,n):
    return CnNNLO(z*fmGeV, n, mu, alphas(mu))/CnNNLO(z*fmGeV, 0, mu, alphas(mu))