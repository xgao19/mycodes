#!/usr/bin/env python3
import numpy as np
import numba as nb
import math
import sys
import os
from scipy.interpolate import interp1d
from scipy import integrate
from utils.tools import *


'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":


    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    
    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    Nt = int(line1st[2])
    latsp = float(line1st[3])
    #zstep = latsp
    print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
    
    fmGeV = 5.0676896
    # read parameters
    for index in range(1, len(commands)):
        
        line = commands[index].split()
        
        if line[0] == 'zstep':
            zstep = float(line[1])
            #zsave = np.arange(0, 5, zstep)
            print('zstep:',zstep,'fm')

        elif line[0] == 'bTrange':
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bTlist:',bTlist)

        elif line[0] == 'bzrange':
            zlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('zlist:', zlist)
            describe = describe.replace('#',line[1] + '-' + line[2])

        elif line[0] == 'pzrange':
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('pzlist:',pzlist)

        elif line[0] == 'eta':
            eta = int(line[1])
            print('eta:', eta)

        elif line[0] == 'mean':
            mean_format = line[1]
            print('\nmean:', mean_format.split('/')[-1])

        elif line[0] == 'sample':
            sample_format = line[1]
            print('sample:', sample_format.split('/')[-1],'\n')

    xlist = np.arange(-1, 2, 0.01)
    for iT, bT in enumerate(bTlist):

        iT_describe = describe.replace('@', str(bT))
        iT_mean = mean_format.replace('@', str(bT))
        iT_sample = sample_format.replace('@', str(bT))

        for ipz, pz in enumerate(pzlist):

            ipz_describe = iT_describe.replace('*', str(pz))

            ipz_mean = np.loadtxt(iT_mean.replace('*', str(pz)))
            ipz_samples = []

            for iz, z in enumerate(zlist):
                ipz_samples += [np.loadtxt(iT_sample.replace('*', str(pz)).replace('#', str(z)))]

            print(np.shape(ipz_mean), np.shape(ipz_samples))

            npick = len(ipz_samples[0])
            low, mid, up = int((0.16*npick)), int(0.5*npick), int(np.ceil(0.84*npick))

            ipz_fm = round(2*np.pi*pzlist[ipz]/(Ns*latsp),2)
            print('\npz =', ipz_fm/fmGeV, '[GeV]; npick:', npick)

            ipz_qx_samples = []
            for isamp in range(0, npick):

                if isamp % 10 == 0:
                    print(isamp,'/', npick)

                isamp_bz = [zlist[i]*latsp*2 for i in range(0, len(zlist))]
                isamp_hz = [ipz_samples[i][isamp] for i in range(0, len(ipz_samples))]
                itp_hz = interp1d(isamp_bz, isamp_hz, kind='slinear')

                zsave = np.arange(0, isamp_bz[-1], zstep)

                def qPDF(x):
                    #return 2*ipz_fm/(2*np.pi)*integrate.quad(lambda z: itp_hz(z)*np.cos((x-0.5)*ipz_fm*z), 0, isamp_bz[-1])[0]
                    DFT = 0
                    for iz in range(1, len(zsave)):
                        z1 = zsave[iz-1]
                        z2 = zsave[iz]
                        dz = z2 - z1
                        hz = 2*ipz_fm/(2*np.pi) * (itp_hz(z1)*np.cos((x-0.5)*ipz_fm*z1)+itp_hz(z2)*np.cos((x-0.5)*ipz_fm*z2))/2
                        DFT += hz*dz
                    return DFT

                isamp_qx = [qPDF(x) for x in xlist]
                ipz_qx_samples += [isamp_qx]


            # save qx mean err
            ipz_qPDF_save_mean = []
            ipz_qPDF_save_0 = []
            ipz_qPDF_save_1 = []
            ipz_qPDF_save_2 = []
            for ix in range(0, len(xlist)):
                ix_qPDF_save_samples = [ipz_qx_samples[isamp][ix] for isamp in range(0, npick)]
                ix_qPDF_save_samples = sorted(ix_qPDF_save_samples)
                ipz_qPDF_save_mean += [[xlist[ix], (ix_qPDF_save_samples[up]+ix_qPDF_save_samples[low])/2, (ix_qPDF_save_samples[up]-ix_qPDF_save_samples[low])/2]]
                ipz_qPDF_save_0 += [[xlist[ix], ix_qPDF_save_samples[low]]]
                ipz_qPDF_save_1 += [[xlist[ix], ix_qPDF_save_samples[mid]]]
                ipz_qPDF_save_2 += [[xlist[ix], ix_qPDF_save_samples[up]]]

            save_name = bandfolder + ipz_describe + '_qPDF_0.txt'
            np.savetxt(save_name, ipz_qPDF_save_0, fmt='%.8e')
            save_name = bandfolder + ipz_describe + '_qPDF_1.txt'
            np.savetxt(save_name, ipz_qPDF_save_1, fmt='%.8e')
            save_name = bandfolder + ipz_describe + '_qPDF_2.txt'
            np.savetxt(save_name, ipz_qPDF_save_2, fmt='%.8e')

            # save qx samples
            sample_name = samplefolder + ipz_describe + '_qPDF_bootssamples.txt'
            np.savetxt(sample_name, np.transpose(ipz_qx_samples), fmt='%.8e')




