#!/usr/bin/env python3
import numpy as np
import math
import sys
from utils.Constants import *
import scipy.special as sp

########################################
#--------- Construct C kernel ---------#
########################################

beta = {
    0: 11/3*CA - 4/3*TF*nf,
    1: 34/3*CA**2 - (20/3*CA + 4*CF)*TF*nf,
    2: 2857/54*CA**3 + (2*CF**2 - 205/9*CF*CA - 1415/27*CA**2)*TF*nf + (44/9*CF + 158/27*CA)*TF**2*nf**2,
}

cusp = {
    0: 2*CF,
    1: 2*CF * ((67/9-np.pi**2/3)*CA - 20/9*TF*nf),
    2: 2*CF * (CA**2*(245/6-134*np.pi**2/27+11*np.pi**4/45 + 22/3*sp.zeta(3)) + CA*TF*nf*(-418/27+40*np.pi**2/27-56/3*sp.zeta(3)) + CF*TF*nf*(-55/3+16*sp.zeta(3)) - 16/27*TF**2*nf**2),
}

GI_noncuspMu = {
    0: -2*CF,
    1: CF * (CF*(-4+14/3*np.pi**2-24*sp.zeta(3)) + CA*(-554/27-11*np.pi**2/6+22*sp.zeta(3)) + nf*(80/27+np.pi**2/3)),
}

GI_noncuspC = {
    0: 2*CF - complex(0, np.pi * cusp[0]),
    1: CF * (CF*(4-14/3*np.pi**2+24*sp.zeta(3)) + CA*(950/27+11*np.pi**2/9-22*sp.zeta(3)) + nf*(-152/27-2*np.pi**2/9)) - complex(0, np.pi*(cusp[1]+beta[0]*(2*2*CF-cusp[0]))),
}

CG_noncuspMu = {
    0: -6*CF,
    1: 0,
}

CG_noncuspC = {
    0: 6*CF,
    1: 0,
}

############################################################
##----------------- kernels for CG TMDWF -----------------##
############################################################

def kernel_CG(mu0, mu, order):

    Lz = np.log(mu0**2/mu**2)
    aS = alphas_nloop(mu, order-1)/(4*np.pi)
    kernel = 1

    if order > 0:
        Lz2 = -0.5*Lz**2
        Lz1 = 3*Lz
        Lz0 = -12 + 7*np.pi**2/12
        kernel += aS * CF * (Lz2 + Lz1 + Lz0)

    if order > 1:
        sys.exit('NNLO CG matching not avaible')

    return kernel

# RG cusp
def kernel_CG_K1(mu0, mu, order):

    r = alphas_nloop(mu, order)/alphas_nloop(mu0, order)
    aS = alphas_nloop(mu0, order)/(4*np.pi)
    kernel = 1/aS * (1-1/r-np.log(r))

    if order > 0:
        kernel +=  (cusp[1]/cusp[0]-beta[1]/beta[0])*(1-r+np.log(r)) + beta[1]/(2*beta[0])*np.log(r)**2
    if order > 1:
        kernel += aS * (((beta[1]/beta[0])**2-beta[2]/beta[0])*((1-r**2)/2+np.log(r)) \
            + (beta[1]/beta[0]*cusp[1]/cusp[0]-(beta[1]/beta[0])**2) * (1-r+r*np.log(r)) \
                - (cusp[2]/cusp[0]-beta[1]/beta[0]*cusp[1]/cusp[0])*(1-r)**2/2)
        #kernel += 0
    if order > 2:
        sys.exit('NNNLL CG matching not avaible')
        
    return -cusp[0]/(4*beta[0]**2) * kernel

# RG CG_noncuspC
def kernel_CG_K2(mu0, mu, order):

    r = alphas_nloop(mu, order-1)/alphas_nloop(mu0, order-1)
    aS = alphas_nloop(mu0, order-1)/(4*np.pi)
    kernel = 0

    if order > 0:
        kernel += np.log(r)
    if order > 1:
        #kernel += aS * (CG_noncuspC[1]/CG_noncuspC[0] - beta[1]/beta[0]) * (r-1)
        kernel += 0
    if order > 2:
        sys.exit('NNNLL CG matching not avaible')
    #print('>>',mu0, mu,r)

    return -CG_noncuspC[0]/(2*beta[0]) * kernel

# RG noncusp_mu
def kernel_CG_K3(mu0, mu, order):

    r = alphas_nloop(mu, order-1)/alphas_nloop(mu0, order-1)
    aS = alphas_nloop(mu0, order-1)/(4*np.pi)
    kernel = 0

    if order > 0:
        kernel += np.log(r)
    if order > 1:
        #kernel += aS * (CG_noncuspMu[1]/CG_noncuspMu[0] - beta[1]/beta[0]) * (r-1)
        kernel += 0
    if order > 2:
        sys.exit('NNNLL CG matching not avaible')

    return -CG_noncuspMu[0]/(2*beta[0]) * kernel

# RG noncusp_eta
def kernel_CG_eta(mu0, mu, order):

    r = alphas_nloop(mu, order-1)/alphas_nloop(mu0, order-1)
    aS = alphas_nloop(mu0, order-1)/(4*np.pi)
    kernel = 0

    if order > 0:
        kernel += np.log(r)
    if order > 1:
        kernel += aS * (cusp[1]/cusp[0]-beta[1]/beta[0])*(r-1)
        #kernel += 0
    if order > 2:
        sys.exit('NNNLL CG matching not avaible')

    #return -complex(0, np.pi)*cusp[0]/(2*beta[0]) * kernel
    return 0

############################################################
##----------------- kernels for GI TMDWF -----------------##
############################################################

def kernel_GI(mu0, mu, order):

    Lz = np.log(mu0**2/mu**2) + complex(0, np.pi)
    aS = alphas_nloop(mu, order-1)/(4*np.pi)
    kernel = 1

    if order > 0:
        Lz2 = -0.5*Lz**2
        Lz1 = Lz
        Lz0 = -2 - 5*np.pi**2/12
        kernel += aS * CF * (Lz2 + Lz1 + Lz0)

    if order > 1:
        Lz4 = CF/4 * Lz**4
        Lz3 = -(CF-11/9*CA+2/9*nf) * Lz**3
        Lz2 = ((3+5*np.pi**2/12)*CF + (np.pi**2/3-100/9)*CA + 16/9*nf) * Lz**2
        Lz1 = -((11*np.pi**2/2-24*sp.zeta(3))*CF + (22*sp.zeta(3)-44*np.pi**2/9-950/27)*CA + (152/27+8*np.pi**2/9)*nf)*Lz
        Lz0 = (-30*sp.zeta(3) + 65*np.pi**2/3 - 167*np.pi**4/144 - 16)*CF + (241*sp.zeta(3)/9 + 53*np.pi**4/60 - 1759*np.pi**2/108 - 3884/81)*CA + (2*sp.zeta(3)/9 + 113*np.pi**2/54 + 656/81) * nf
        kernel += aS**2 * CF/2 * (Lz4 + Lz3 + Lz2 + Lz1 + Lz0)

    if order > 2:
        sys.exit('NNNLO GI matching not avaible')

    return kernel

# RG cusp
def kernel_GI_K1(mu0, mu, order):

    r = alphas_nloop(mu, order)/alphas_nloop(mu0, order)
    aS = alphas_nloop(mu0, order)/(4*np.pi)
    kernel = 1/aS * (1-1/r-np.log(r))

    if order > 0:
        kernel +=  (cusp[1]/cusp[0]-beta[1]/beta[0])*(1-r+np.log(r)) + beta[1]/(2*beta[0])*np.log(r)**2
    if order > 1:
        kernel += aS * (((beta[1]/beta[0])**2-beta[2]/beta[0])*((1-r**2)/2+np.log(r)) \
            + (beta[1]/beta[0]*cusp[1]/cusp[0]-(beta[1]/beta[0])**2) * (1-r+r*np.log(r)) \
                - (cusp[2]/cusp[0]-beta[1]/beta[0]*cusp[1]/cusp[0])*(1-r)**2/2)
    if order > 2:
        sys.exit('NNNLL GI matching not avaible')
        
    return -cusp[0]/(4*beta[0]**2) * kernel

# RG GI_noncuspC
def kernel_GI_K2(mu0, mu, order):

    r = alphas_nloop(mu, order-1)/alphas_nloop(mu0, order-1)
    aS = alphas_nloop(mu0, order-1)/(4*np.pi)
    kernel = 0

    if order > 0:
        kernel += np.log(r)
    if order > 1:
        kernel += aS * (GI_noncuspC[1]/GI_noncuspC[0] - beta[1]/beta[0]) * (r-1)
    if order > 2:
        sys.exit('NNNLL GI matching not avaible')
    #print('>>',mu0, mu,r)

    return -GI_noncuspC[0]/(2*beta[0]) * kernel

# RG noncusp_mu
def kernel_GI_K3(mu0, mu, order):

    r = alphas_nloop(mu, order-1)/alphas_nloop(mu0, order-1)
    aS = alphas_nloop(mu0, order-1)/(4*np.pi)
    kernel = 0

    if order > 0:
        kernel += np.log(r)
    if order > 1:
        kernel += aS * (GI_noncuspMu[1]/GI_noncuspMu[0] - beta[1]/beta[0]) * (r-1)
    if order > 2:
        sys.exit('NNNLL GI matching not avaible')

    return -GI_noncuspMu[0]/(2*beta[0]) * kernel

# RG noncusp_eta
def kernel_GI_eta(mu0, mu, order):

    r = alphas_nloop(mu, order-1)/alphas_nloop(mu0, order-1)
    aS = alphas_nloop(mu0, order-1)/(4*np.pi)
    kernel = 0

    if order > 0:
        kernel += np.log(r)
    if order > 1:
        kernel += aS * (cusp[1]/cusp[0]-beta[1]/beta[0])*(r-1)
    if order > 2:
        sys.exit('NNNLL GI matching not avaible')

    return -complex(0, np.pi)*cusp[0]/(2*beta[0]) * kernel