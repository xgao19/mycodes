#!/usr/bin/env python3
# The types are defined in 2307.12359, by whether expanding ratios in alphas or not
import numpy as np
import math
import sys
from utils.Constants import *
import scipy.special as sp
from qTMD_pion.TMDWF_64I.Boots_CS_qTMDWF_matching_kernelC import *

########################################
#--------- Construct C kernel ---------#
########################################
class CS_Dgamma:

    order = 0 # order of the perturbative expansion
    mu = 0    # factorization scale mu, running coupling, and alps/4pi

    def __init__(self, mu, order):
        self.order = order
        self.mu = mu
    
    #mu in GeV
    def alphas(self, mu, Nf = 3):
        return alphas_nloop(mu, self.order, Nf)

    def Dgamma_CG(self, P1, P2, x, cplx='real'):

        if self.order < 2:
            sum_pQCD_P1_x = kernel_CG(2*x*P1, self.mu, self.order)
            sum_pQCD_P2_x = kernel_CG(2*x*P2, self.mu, self.order)
            sum_pQCD_P1_xbar = kernel_CG(2*(1-x)*P1, self.mu, self.order)
            sum_pQCD_P2_xbar = kernel_CG(2*(1-x)*P2, self.mu, self.order)
        else:
            sys.exit('NNLO CG matching not avaible')

        if cplx == 'real':
            return -1/np.log(P1/P2) * (np.log(np.absolute(sum_pQCD_P1_x/sum_pQCD_P2_x)) + np.log(np.absolute(sum_pQCD_P1_xbar/sum_pQCD_P2_xbar)))
        elif cplx == 'imag':
            return 0

    def Dgamma_GI(self, P1, P2, x, cplx='real'):

        if self.order < 3:
            sum_pQCD_P1_x = kernel_GI(2*x*P1, self.mu, self.order)
            sum_pQCD_P2_x = kernel_GI(2*x*P2, self.mu, self.order)
            sum_pQCD_P1_xbar = kernel_GI(2*(1-x)*P1, self.mu, self.order)
            sum_pQCD_P2_xbar = kernel_GI(2*(1-x)*P2, self.mu, self.order)
        else:
            sys.exit('NNNLO GI matching not avaible')

        if cplx == 'real':
            return -1/np.log(P1/P2) * (np.log(np.absolute(sum_pQCD_P1_x/sum_pQCD_P2_x)) + np.log(np.absolute(sum_pQCD_P1_xbar/sum_pQCD_P2_xbar)))
        elif cplx == 'imag':
            return -1/np.log(P1/P2) * (np.angle(sum_pQCD_P1_x/sum_pQCD_P2_x) + np.angle(sum_pQCD_P1_xbar/sum_pQCD_P2_xbar))

    def Dgamma_CG_RG(self, P1, P2, x, cplx='real'):

        if self.order < 3:
            sum_pQCD_P1_x = -2*kernel_CG_K1(self.mu, 2*x*P1, self.order) + kernel_CG_K2(self.mu, 2*x*P1, self.order)
            sum_pQCD_P2_x = -2*kernel_CG_K1(self.mu, 2*x*P2, self.order) + kernel_CG_K2(self.mu, 2*x*P2, self.order)
            sum_pQCD_P1_xbar = -2*kernel_CG_K1(self.mu, 2*(1-x)*P1, self.order) + kernel_CG_K2(self.mu, 2*(1-x)*P1, self.order)
            sum_pQCD_P2_xbar = -2*kernel_CG_K1(self.mu, 2*(1-x)*P2, self.order) + kernel_CG_K2(self.mu, 2*(1-x)*P2, self.order)
        else:
            sys.exit('NNNLL CG matching not avaible')

        if cplx == 'real':
            return -1/np.log(P1/P2) * (sum_pQCD_P1_x-sum_pQCD_P2_x + sum_pQCD_P1_xbar-sum_pQCD_P2_xbar).real
        elif cplx == 'imag':
            return -1/np.log(P1/P2) * (sum_pQCD_P1_x-sum_pQCD_P2_x + sum_pQCD_P1_xbar-sum_pQCD_P2_xbar).imag

    def Dgamma_GI_RG(self, P1, P2, x, cplx='real'):

        if self.order < 3:
            sum_pQCD_P1_x = -2*kernel_GI_K1(self.mu, 2*x*P1, self.order) + kernel_GI_K2(self.mu, 2*x*P1, self.order)
            sum_pQCD_P2_x = -2*kernel_GI_K1(self.mu, 2*x*P2, self.order) + kernel_GI_K2(self.mu, 2*x*P2, self.order)
            sum_pQCD_P1_xbar = -2*kernel_GI_K1(self.mu, 2*(1-x)*P1, self.order) + kernel_GI_K2(self.mu, 2*(1-x)*P1, self.order)
            sum_pQCD_P2_xbar = -2*kernel_GI_K1(self.mu, 2*(1-x)*P2, self.order) + kernel_GI_K2(self.mu, 2*(1-x)*P2, self.order)
        else:
            sys.exit('NNNLL GI matching not avaible')

        if cplx == 'real':
            return -1/np.log(P1/P2) * (sum_pQCD_P1_x-sum_pQCD_P2_x + sum_pQCD_P1_xbar-sum_pQCD_P2_xbar).real
        elif cplx == 'imag':
            return -1/np.log(P1/P2) * (sum_pQCD_P1_x-sum_pQCD_P2_x + sum_pQCD_P1_xbar-sum_pQCD_P2_xbar).imag

'''
savedir = '/Users/Xiang/Desktop/docs/0-2023/projects/6-TMDWF-pion-64I/1-analysis/6-CSkernel/plots_kernel/data_type1/'
saveformat = 'CS_correction_CG_NNLL_P*_Pskip.dat'
Ns, latsp = 64, 0.0836493
dP = 2*np.pi/(Ns*latsp*fmGeV)
plist = [i for i in range(4, 9)]
pskip = 3
saveformat = saveformat.replace('Pskip', 'Pskip'+str(pskip))
CS_correction = CS_Dgamma(2, 2)
left = 0.25
xlist = np.arange(left, 1-left, 0.01)
for ip in range(pskip, len(plist)):
    P1, P2 = plist[ip-pskip]*dP, plist[ip]*dP
    print(plist[ip-pskip], plist[ip], round(P1,2), round(P2,2))
    kernels = np.zeros([len(xlist),3])
    for ix, x in enumerate(xlist):
        kernels[ix][0] = x
        kernels[ix][1] = CS_correction.Dgamma_CG_RG(P1,P2,x,'real')
        kernels[ix][2] = CS_correction.Dgamma_CG_RG(P1,P2,x,'imag')
    savename = savedir + saveformat.replace('*', str(plist[ip]))
    np.savetxt(savename, kernels, fmt='%.6e')
'''