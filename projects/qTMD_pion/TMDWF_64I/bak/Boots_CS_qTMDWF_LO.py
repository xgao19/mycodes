#!/usr/bin/env python3
import numpy as np
import numba as nb
import math
import sys
import os
from scipy.interpolate import interp1d
from scipy import integrate
from utils.tools import *

def data_collection(mean_format, sample_format, bTlist, pzlist):
    mean = []
    samples = []
    for iT, bT in enumerate(bTlist):
        iT_mean_format = mean_format.replace('@', str(bT))
        iT_sample_format = sample_format.replace('@', str(bT))
        iT_mean, iT_samples = [], []
        for ipz, pz in enumerate(pzlist):
            ipz_mean = np.loadtxt(iT_mean_format.replace('*', str(pz)))
            ipz_samples = np.loadtxt(iT_sample_format.replace('*', str(pz)))
            iT_mean += [ipz_mean]
            iT_samples += [ipz_samples]
        mean += [iT_mean]
        samples += [iT_samples]
    return mean, samples

def CSkernel_LO(P1, P2, P1_data, P2_data):

    return 1/np.log(P1/P2) * np.log(abs(P1_data/P2_data))

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":


    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    
    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    Nt = int(line1st[2])
    latsp = float(line1st[3])
    fmGeV = 5.0676896
    dP = 2*np.pi/(Ns*latsp*fmGeV)
    print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',round(latsp,3),'fm, dP =', round(dP,2), 'GeV.\n')
    
    # read parameters
    for index in range(1, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'bTrange':
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bTlist:',bTlist)

        elif line[0] == 'pzrange':
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            pskip = int(line[3])
            print('pzlist:',pzlist, '; pskip =', pskip)

        elif line[0] == 'mean':
            mean_format = line[1]
            print('\nmean:', mean_format.split('/')[-1])

        elif line[0] == 'sample':
            sample_format = line[1]
            print('sample:', sample_format.split('/')[-1],'\n')

    mean, samples = data_collection(mean_format, sample_format, bTlist, pzlist)
    xlist = [mean[0][0][ix][0] for ix in range(0, len(mean[0][0]))]
    npick = len(samples[0][0][0])
    low, mid, up = int(0.16*npick), int(0.5*npick), int(0.84*npick)
    print('Read data in shape:', np.shape(mean), np.shape(samples))

    CS_samples = []
    for iT, bT in enumerate(bTlist):
        iT_describe = describe.replace('@', str(bT))

        iT_CS_samples = []
        for ipz in range(1, len(pzlist)):
            pz1, pz2 = pzlist[ipz-1], pzlist[ipz]
            pz1_data, pz2_data = np.array(samples[iT][ipz-1]), np.array(samples[iT][ipz])
            ipz_describe = iT_describe.replace('*', str(pz2))
            pz1, pz2 = pz1*dP, pz2*dP

            ipz_CS_samples = []
            for ix, x in enumerate(xlist):
                ix_CS_samples = CSkernel_LO(pz1, pz2, pz1_data[ix], pz2_data[ix])
                ipz_CS_samples += [ix_CS_samples]

            # save mean err bands
            ipz_band0, ipz_band1, ipz_band2 = [], [], []
            for ix, x in enumerate(xlist):
                ix_CS_samples = sorted(ipz_CS_samples[ix])
                ipz_band0 += [[x, ix_CS_samples[low]]]
                ipz_band1 += [[x, ix_CS_samples[mid]]]
                ipz_band2 += [[x, ix_CS_samples[up]]]
            savename = bandfolder + ipz_describe + '_CSx_LO_band0.dat'
            np.savetxt(savename, ipz_band0, fmt='%.6e')
            savename = bandfolder + ipz_describe + '_CSx_LO_band1.dat'
            np.savetxt(savename, ipz_band1, fmt='%.6e')
            savename = bandfolder + ipz_describe + '_CSx_LO_band2.dat'
            np.savetxt(savename, ipz_band2, fmt='%.6e')

            # save samples
            savename = samplefolder + ipz_describe + '_CSx_LO_samples.dat'
            np.savetxt(savename, ipz_CS_samples, fmt='%.6e')
            print('Save data: bT =', bT, '; P1 =', round(pz1,3), '/ P2 =', round(pz2,3))
