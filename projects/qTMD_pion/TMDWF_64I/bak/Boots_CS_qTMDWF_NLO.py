#!/usr/bin/env python3
import numpy as np
import numba as nb
import math
import sys
import os
from scipy.interpolate import interp1d
from scipy import integrate
from utils.tools import *
from utils.Constants import *

#mu in GeV
def alphas(mu, Nf = 3):
    return alphas_5loop(mu, Nf)

def data_collection(mean_format, sample_format, bTlist, pzlist):
    mean = []
    samples = []
    for iT, bT in enumerate(bTlist):
        iT_mean_format = mean_format.replace('@', str(bT))
        iT_sample_format = sample_format.replace('@', str(bT))
        iT_mean, iT_samples = [], []
        for ipz, pz in enumerate(pzlist):
            ipz_mean = np.loadtxt(iT_mean_format.replace('*', str(pz)))
            ipz_samples = np.loadtxt(iT_sample_format.replace('*', str(pz)))
            iT_mean += [ipz_mean]
            iT_samples += [ipz_samples]
        mean += [iT_mean]
        samples += [iT_samples]
    return mean, samples

def CSkernel_LO(P1, P2, P1_data, P2_data):

    return 1/np.log(P1/P2) * np.log(abs(P1_data/P2_data))

# pz = xPz, always real
def CS_C_NLO_CG(pz, mu, alphas):
    Lz = np.log(mu**2/(2*pz)**2)
    return -alphas*CF/(4*np.pi) * (0.5*Lz**2 + 3*Lz + 12 - 7*np.pi**2/12)

# pz = xPz, complex
def CS_C_NLO_GI(pz, mu, alphas):
    Lz = np.log((2*pz)**2/mu**2) + complex(0, np.pi)
    return alphas*CF/(4*np.pi) * (-0.5*Lz**2 + Lz - 2 - 5*np.pi**2/12)

def CSkernel_NLO_CG(P1, P2, P1_data, P2_data, mu, x = 1):
    GammaMS = -1/np.log(P1/P2) * ((CS_C_NLO_CG(x*P1, mu, alphas(mu))-CS_C_NLO_CG(x*P2, mu, alphas(mu))) + (CS_C_NLO_CG((1-x)*P1, mu, alphas(mu))-CS_C_NLO_CG((1-x)*P2, mu, alphas(mu))))
    print(x, GammaMS)
    return 1/np.log(P1/P2) * np.log(abs(P1_data/P2_data)) + GammaMS

def CSkernel_NLO_GI_real(P1, P2, P1_data, P2_data, mu, x = 1):
    GammaMS = -1/np.log(P1/P2) * ((CS_C_NLO_GI(x*P1, mu, alphas(mu))-CS_C_NLO_GI(x*P2, mu, alphas(mu))) + (CS_C_NLO_GI((1-x)*P1, mu, alphas(mu))-CS_C_NLO_GI((1-x)*P2, mu, alphas(mu))))
    #print(x, GammaMS)
    return 1/np.log(P1/P2) * np.log(abs(P1_data/P2_data)) + GammaMS.real

def CSkernel_NLO_GI_imag(P1, P2, P1_data, P2_data, mu, x = 1):
    GammaMS = -1/np.log(P1/P2) * ((CS_C_NLO_GI(x*P1, mu, alphas(mu))-CS_C_NLO_GI(x*P2, mu, alphas(mu))) + (CS_C_NLO_GI((1-x)*P1, mu, alphas(mu))-CS_C_NLO_GI((1-x)*P2, mu, alphas(mu))))
    return 1/np.log(P1/P2) * np.log(abs(P1_data/P2_data)) + GammaMS.imag

mu = 2
for P in range(5, 8):
    P1, P2 = P-1, P
    GammaMS = []
    for x in np.arange(0.02, 1, 0.02):
        GammaMS += [[x, -1/np.log(P1/P2) * ((CS_C_NLO_CG(x*P1, mu, alphas(mu))-CS_C_NLO_CG(x*P2, mu, alphas(mu))) + (CS_C_NLO_CG((1-x)*P1, mu, alphas(mu))-CS_C_NLO_CG((1-x)*P2, mu, alphas(mu))))]]
    print(np.shape(GammaMS))

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":


    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    
    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    Nt = int(line1st[2])
    latsp = float(line1st[3])
    fmGeV = 5.0676896
    dP = 2*np.pi/(Ns*latsp*fmGeV)
    print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',round(latsp,3),'fm, dP =', round(dP,2), 'GeV.\n')
    
    # read parameters
    for index in range(1, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'mu':
            mu = float(line[1])
            print('mu =', mu, ' GeV; alphas =', round(alphas(mu),3))
        elif line[0] == 'bTrange':
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bTlist:',bTlist)

        elif line[0] == 'pzrange':
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            pskip = int(line[3])
            print('pzlist:',pzlist, '; pskip =', pskip)

        elif line[0] == 'mean':
            mean_format = line[1]
            print('\nmean:', mean_format.split('/')[-1])

        elif line[0] == 'sample':
            sample_format = line[1]
            print('sample:', sample_format.split('/')[-1],'\n')

    mean, samples = data_collection(mean_format, sample_format, bTlist, pzlist)
    xlist = [mean[0][0][ix][0] for ix in range(0, len(mean[0][0]))]
    npick = len(samples[0][0][0])
    low, mid, up = int(0.16*npick), int(0.5*npick), int(0.84*npick)
    print('Read data in shape:', np.shape(mean), np.shape(samples))

    CS_samples = []
    for iT, bT in enumerate(bTlist):
        iT_describe = describe.replace('@', str(bT))

        iT_CS_samples = []
        for ipz in range(1, len(pzlist)):
            pz1, pz2 = pzlist[ipz-1], pzlist[ipz]
            pz1_data, pz2_data = np.array(samples[iT][ipz-1]), np.array(samples[iT][ipz])
            ipz_describe = iT_describe.replace('*', str(pz2))
            pz1, pz2 = pz1*dP, pz2*dP

            ipz_CS_samples = []
            for ix, x in enumerate(xlist):
                if 'CG' in describe:
                    ix_CS_samples = CSkernel_NLO_CG(pz1, pz2, pz1_data[ix], pz2_data[ix], mu, x = x)
                elif 'real' in describe:
                    ix_CS_samples = CSkernel_NLO_GI_real(pz1, pz2, pz1_data[ix], pz2_data[ix], mu, x = x)
                elif 'imag' in describe:
                    ix_CS_samples = CSkernel_NLO_GI_imag(pz1, pz2, pz1_data[ix], pz2_data[ix], mu, x = x)
                ipz_CS_samples += [ix_CS_samples]

            # save mean err bands
            ipz_band0, ipz_band1, ipz_band2 = [], [], []
            for ix, x in enumerate(xlist):
                ix_CS_samples = sorted(ipz_CS_samples[ix])
                ipz_band0 += [[x, ix_CS_samples[low]]]
                ipz_band1 += [[x, ix_CS_samples[mid]]]
                ipz_band2 += [[x, ix_CS_samples[up]]]
            savename = bandfolder + ipz_describe + '_CSx_LO_band0.dat'
            np.savetxt(savename, ipz_band0, fmt='%.6e')
            savename = bandfolder + ipz_describe + '_CSx_LO_band1.dat'
            np.savetxt(savename, ipz_band1, fmt='%.6e')
            savename = bandfolder + ipz_describe + '_CSx_LO_band2.dat'
            np.savetxt(savename, ipz_band2, fmt='%.6e')

            # save samples
            savename = samplefolder + ipz_describe + '_CSx_LO_samples.dat'
            np.savetxt(savename, ipz_CS_samples, fmt='%.6e')
            print('Save data: bT =', bT, '; P1 =', round(pz1,3), '/ P2 =', round(pz2,3))