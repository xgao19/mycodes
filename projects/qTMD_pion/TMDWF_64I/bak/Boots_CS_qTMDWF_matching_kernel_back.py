#!/usr/bin/env python3
import numpy as np
import math
import sys
from utils.Constants import *
import scipy.special as sp



########################################
#--------- Construct C kernel ---------#
########################################
class CS_C:

    order = 0 # order of the perturbative expansion
    mu = 0    # factorization scale mu, running coupling, and alps/4pi

    def __init__(self, mu, order):
        self.order = order
        self.mu = mu

    #mu in GeV
    def alphas(self, mu, Nf = 3):
        print(mu)
        return alphas_nloop(mu, self.order, Nf)

    def kernel_CG(self, pz):

        Lz = np.log((2*pz)**2/self.mu**2)
        aS = self.alphas(self.mu)/(4*np.pi)
        
        sum_pQCD = 0
        if self.order > 0:
            Lz2 = -0.5*Lz**2
            Lz1 = 3*Lz
            Lz0 = -12 + 7*np.pi**2/12
            sum_pQCD += aS * CF * (Lz2 + Lz1 + Lz0)
        if self.order > 1:
            sys.exit('NNLO CG matching not avaible')
        return sum_pQCD

    def kernel_GI(self, pz):

        Lz = np.log((2*pz)**2/self.mu**2) + complex(0, np.pi)
        aS = alphas(self.mu)/(4*np.pi)

        sum_pQCD = 0
        if self.order > 0:
            Lz2 = -0.5*Lz**2
            Lz1 = Lz
            Lz0 = -2 - 5*np.pi**2/12
            sum_pQCD += aS * CF * (Lz2 + Lz1 + Lz0)
        if self.order > 1:
            Lz4 = CF/4 * Lz**4
            Lz3 = -(CF-11/9*CA+2/9*nf) * Lz**3
            Lz2 = ((3+5*np.pi**2/12)*CF + (np.pi**2/3-100/9)*CA + 16/9*nf) * Lz**2
            Lz1 = -((11*np.pi**2/2-24*sp.zeta(3))*CF + (22*sp.zeta(3)-44*np.pi**2/9-950/27)*CA + (152/27+8*np.pi**2/9)*nf)*Lz
            Lz0 = (-30*sp.zeta(3) + 65*np.pi**2/3 - 167*np.pi**4/144 - 16)*CF + (241*sp.zeta(3)/9 + 53*np.pi**4/60 - 1759*np.pi**2/108 - 3884/81)*CA + (2*sp.zeta(3)/9 + 113*np.pi**2/54 + 656/81) * nf
            sum_pQCD += aS**2 * CF/2 * (Lz4 + Lz3 + Lz2 + Lz1 + Lz0)
        if self.order > 2:
            sys.exit('NNNLO GI matching not avaible')
        return sum_pQCD

class CS_C_RG:

    order = 0 # order of the perturbative expansion
    mu = 0 # factorization scale mu, running coupling, and alps/4pi

    def __init__(self, mu, order):
        self.order = order
        self.mu = mu

    beta = {
        0: 11/3*CA - 4/3*TF*nf,
        1: 34/3*CA**2 - (20/3*CA + 4*CF)*TF*nf,
        2: 2857/54*CA**3 + (2*CF**2 - 205/9*CF*CA - 1415/27*CA**2)*TF*nf + (44/9*CF + 158/27*CA)*TF**2*nf**2,
    }

    cusp = {
        0: 2*CF,
        1: 2*CF * ((67/9-np.pi**2/3)*CA - 20/9*TF*nf),
        2: 2*CF * (CA**2*(245/6-134*np.pi**2/27+11*np.pi**4/45 + 22/3*sp.zeta(3)) + CA*TF*nf*(-418/27+40*np.pi**2/27-56/3*sp.zeta(3)) + CF*TF*nf*(-55/3+16*sp.zeta(3)) - 16/27*TF**2*nf**2),
    }

    noncusp = {
        0: -2*CF,
        1: CF * (CF*(-4+14/3*np.pi**2-24*sp.zeta(3) + CA*(-554/27-11*np.pi**2/6+22*sp.zeta(3)) + nf*(80/27+np.pi**2/3))),
    }

    noncuspC = {
        0: 2*CF - complex(0, np.pi * cusp[0]),
        1: CF * (CF*(4-14/3*np.pi**2+24*sp.zeta(3) + CA*(950/27+11*np.pi**2/9-22*sp.zeta(3)) + nf*(-152/27-2*np.pi**2/9))) - complex(0, np.pi*(cusp[1]+beta[0]*(2*2*CF-cusp[0]))),
    }

    #mu in GeV
    def alphas(self, mu, Nf = 3):
        print(mu)
        return alphas_nloop(mu, self.order, Nf)

    def K_cusp(self, mu0, mu):

        aS0, aS = alphas(mu0)/(4*np.pi), alphas(mu)/(4*np.pi)
        r = aS0/aS

        sum_pQCD = 1/aS * (1-1/r-np.log(r))
        if self.order > 0:
            sum_pQCD += (cusp[1]/cusp[0]-beta[1]/beta[0]) * (1-r+np.log(r)) + beta[1]/(2*beta[0]) * np.log(r)**2
        if self.order > 1:
            sum_pQCD += aS * ((beta[1]**2/beta[0]**2-beta[2]/beta0)*((1-r**2)/2+np.log(r)) + (beta[1]*cusp[1]/(beta[0]*cusp[0])-beta[1]**2/beta[0]**2)*(1-r+r*np.log(r)) - (cusp[2]/cusp[0]-beta[1]*cusp[1]/(beta[0]*cusp[0]))*(1-r)**2/2)
        if self.order > 2:
            sys.exit('NNNLO is not avaible')
        return -cusp[0]/(4*beta[0]**2) * sum_pQCD

    def K_noncusp(self, mu0, mu):

        aS0, aS = alphas(mu0)/(4*np.pi), alphas(mu)/(4*np.pi)
        r = aS0/aS

        sum_pQCD = 0
        if self.order > 0:
            sum_pQCD += np.log(r)
        if self.order > 1:
            sum_pQCD += aS * (noncusp[1]/noncusp[0] - beta[1]/beta[0]) * (r-1)
        if self.order > 2:
            sys.exit('NNNLO is not avaible')
        return -noncusp[0]/(2*beta[0]) * sum_pQCD

    def K_noncuspC(self, mu0, mu):

        aS0, aS = alphas(mu0)/(4*np.pi), alphas(mu)/(4*np.pi)
        r = aS0/aS

        sum_pQCD = 0
        if self.order > 0:
            sum_pQCD += np.log(r)
        if self.order > 1:
            sum_pQCD += aS * (noncuspC[1]/noncuspC[0] - beta[1]/beta[0]) * (r-1)
        if self.order > 2:
            sys.exit('NNNLO is not avaible')
        return -noncuspC[0]/(2*beta[0]) * sum_pQCD

    def eta(self, mu0, mu):

        aS0, aS = alphas(mu0)/(4*np.pi), alphas(mu)/(4*np.pi)
        r = aS0/aS

        sum_pQCD = 0
        if self.order > 0:
            sum_pQCD += np.log(r)
        if self.order > 1:
            sum_pQCD += aS * (cups[1]/cups[0] - beta[1]/beta[0]) * (r-1)
        if self.order > 2:
            sys.exit('NNNLO is not avaible')
        return -complex(0, np.pi)*cups[0]/(2*beta[0]) * sum_pQCD

CS_C_TEST = CS_C(2, 1)
print(CS_C_TEST.alphas(2.))
print(CS_C_TEST.kernel_CG(1))