#!/usr/bin/env python3
import numpy as np
import numba as nb
import taichi as ti
import math
import sys
import os
from scipy.special import gamma
from scipy.special import gammainc
from scipy.interpolate import interp1d
from scipy import integrate
from scipy.optimize import least_squares
from utils.tools import *

#ti.init(default_ip=ti.i64, default_fp=ti.f64)

def hz_extra_abAsymp(z, N1, N2, a, d, meff = 0):
    N1 = N1 # normalization constant
    N2 = N1 # normalization constant
    a = abs(a) # x^alpha*(1-x)^alpha
    b = abs(a) # x^alpha*(1-x)^alpha
    d = (abs(d)+meff)*fmGeV # decay constant, meff in [GeV]
    #return (N1*gamma(1+a)/complex(0, -z)**(a+1) + N2*np.exp(complex(0, z))*gamma(1+b)/complex(0, z)**(b+1)) * np.exp(-d*z)
    #return (N1/z**(1+a)*np.cos((np.pi * (a+1))/2) + N2/z**(1+b)*np.cos((-np.pi * (b+1))/2 + z)) * np.exp(-d*z)
    return (N1/complex(0, -z)**(a+1) + N2*np.exp(complex(0, z))/complex(0, z)**(b+1)) * np.exp(-d*z)
    #return (N1*np.exp(complex(0, -0.5*z))/complex(0, -z)**(a+1) + N2*np.exp(complex(0, 0.5*z))/complex(0, z)**(b+1)) * np.exp(-d*z)

def hz_extra_cos(z, N, a, b, d, meff=0):
    N1 = N # normalization constant
    N2 = N # normalization constant
    a = a # x^alpha*(1-x)^alpha
    b = b # x^alpha*(1-x)^alpha
    d = (abs(d)+meff)*fmGeV # decay constant, meff in [GeV]
    return N * np.cos(a*z+b) * np.exp(-d*z)

def sample_collaction(sample_format, bTlist, pzlist, zlist):

    samples = []
    for iT, bT in enumerate(bTlist):
        iT_format = sample_format.replace('@', str(bT))
        iT_samples = []
        for ipz, pz in enumerate(pzlist):
            ipz_format = iT_format.replace('*', str(pz))
            ipz_samples = []
            for iz, z in enumerate(zlist):
                iz_format = ipz_format.replace('#', str(z))
                iz_samples = np.loadtxt(iz_format)
                if 'real' in iz_format:
                    iz_format = iz_format.replace('real', 'imag')
                    iz_samples_imag = np.loadtxt(iz_format)
                    iz_samples = [complex(iz_samples[i], iz_samples_imag[i]) for i in range(0, len(iz_samples))]
                ipz_samples += [iz_samples]
            iT_samples += [ipz_samples]
        samples += [iT_samples]

    print('collect samples in shape (bT, pz, bz, npick):', np.shape(samples))
    return samples

def mean_collaction(mean_format, bTlist, pzlist):

    meanerr = []
    for iT, bT in enumerate(bTlist):
        iT_format = mean_format.replace('@', str(bT))
        iT_meanerr = []
        for ipz, pz in enumerate(pzlist):
            ipz_meanerr = np.loadtxt(iT_format.replace('*', str(pz)))
            ipz_meanerr = [ipz_meanerr[i][0:] for i in range(0, len(ipz_meanerr))]
            if 'real' in iT_format:
                ipz_meanerr_imag = np.loadtxt(iT_format.replace('*', str(pz)).replace('real','imag'))
                ipz_meanerr_imag = [ipz_meanerr_imag[i][0:] for i in range(0, len(ipz_meanerr))]
                ipz_meanerr = [[ipz_meanerr[i][0], ipz_meanerr[i][1], complex(ipz_meanerr[i][2], ipz_meanerr_imag[i][2]), complex(ipz_meanerr[i][3], ipz_meanerr_imag[i][3])] for i in range(0, len(ipz_meanerr))]
            iT_meanerr += [ipz_meanerr]
        meanerr += [iT_meanerr]

    print('collect mean in shape (bT, pz, bz, col):', np.shape(meanerr))
    return meanerr

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":


    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    
    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    Nt = int(line1st[2])
    latsp = float(line1st[3])
    #zstep = latsp
    print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',latsp,'fm.\n')
    
    fmGeV = 5.0676896
    # read parameters
    for index in range(1, len(commands)):
        
        line = commands[index].split()
        
        if line[0] == 'zstep':
            zstep = float(line[1])
            #zsave = np.arange(0, 5, zstep)
            print('zstep:',zstep,'fm')

        elif line[0] == 'bTrange':
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bTlist:',bTlist)

        elif line[0] == 'bzrange':
            zlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('zlist:', zlist)
            #describe = describe.replace('#',line[1] + '-' + line[2])

        elif line[0] == 'bzstart':
            bzstart = int(line[1])
            print('bzstart:', bzstart)
            #describe = describe.replace('#', str(bzstart) + '-' + str(zlist[-1]))

        elif line[0] == 'pzrange':
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('pzlist:',pzlist)

        elif line[0] == 'eta':
            eta = int(line[1])
            print('eta:', eta)

        elif line[0] == 'mean':
            mean_format = line[1]
            print('\nmean:', mean_format.split('/')[-1])

        elif line[0] == 'sample':
            sample_format = line[1]
            print('sample:', sample_format.split('/')[-1],'\n')

    # parameter setup
    xmin, xmax, xstep = -1, 2.01, 0.05
    xlist = np.arange(xmin, xmax, xstep)
    meff = 0.01

    # data loading
    data_samples = sample_collaction(sample_format, bTlist, pzlist, zlist)
    data_meanerr = mean_collaction(mean_format, bTlist, pzlist)
    npick = len(data_samples[0][0][0])
    low, mid, up = int((0.16*npick)), int(0.5*npick), int(np.ceil(0.84*npick))

    # looping over fit
    for iT, bT in enumerate(bTlist):

        iT_describe = describe.replace('@', str(bT))

        for ipz, pz in enumerate(pzlist):

            ipz_describe = iT_describe.replace('*', str(pz))
            ipz_fm = round(2*np.pi*pzlist[ipz]/(Ns*latsp),2)
            print('\nbT =', bT, '; pz =', round(ipz_fm/fmGeV,2), '[GeV] ; npick:', npick)

            zmin = bzstart
            for row in data_meanerr[iT][ipz]:
                print(row[0], round(abs(row[2]), 2))
                if abs(row[2]) < 0.2 and row[0] >= zmin:
                    zmin = int(row[0])
                    print('zmin =', zmin, ', hz[zmin] =', row[2])
                    break

            if zmin > zlist[-1] - 4:
                zmin = zlist[-1] - 4
            zminlist = [i for i in range(zmin, zmin+3)]
            zminlist = [6]
            print('zmin list:', zminlist)

            # first fit the central value to setup the fitting intial
            popt_mean = []
            for izmin, zmin in enumerate(zminlist):
                check = 100
                for i in range(0, 200):
                    def Fit_res(popt):
                        res = []
                        popt[0] = popt[1]
                        popt[3] = popt[4]
                        for iz in range(zmin, len(zlist)):
                            diff_real = hz_extra_abAsymp(data_meanerr[iT][ipz][iz][1], popt[0], popt[1], popt[2], popt[6], meff=meff).real - data_meanerr[iT][ipz][iz][2].real
                            diff_imag = hz_extra_abAsymp(data_meanerr[iT][ipz][iz][1], popt[3], popt[4], popt[5], popt[6], meff=meff).real - data_meanerr[iT][ipz][iz][2].imag
                            res += [diff_real/data_meanerr[iT][ipz][iz][3].real, diff_imag/data_meanerr[iT][ipz][iz][3].imag]
                        return res
                    poptstart = [np.random.normal(-0.5, 0.5) for i in range(0, 7)]
                    res = least_squares(Fit_res, poptstart, method='trf', bounds=([-10 for i in range(0, len(poptstart))],[10 for i in range(0, len(poptstart))]))
                    popt = list(res.x)
                    chisq = sum(np.square(res.fun[:])) / (len(res.fun[:])-len(popt))
                    if i >50 and chisq <= 2 and abs(popt[0])<5 and chisq < check:
                        popt_temp = popt+[chisq]
                        #print('popt mean:', [round(popt_temp[i],2) for i in range(0, len(popt_temp))])
                        break
                    elif chisq < check:
                        check = chisq
                        popt_temp = popt+[chisq]
                popt_mean += [popt_temp]
                print('popt mean:', [round(popt_mean[izmin][i],2) for i in range(0, len(popt_mean[izmin]))])
            # fitting loop over samples
            ipz_popt_samples = []
            ipz_hz_Extra_real = []
            ipz_hz_Extra_imag = []
            ipz_qx_real = []
            ipz_qx_imag = []
            for isamp in range(0, npick):

                if isamp % 10 == 0:
                    print(isamp,'/', npick)

                isamp_popt = []
                isamp_hz_Extra_real = []
                isamp_hz_Extra_imag = []
                isamp_qx_real = []
                isamp_qx_imag = []
                for izmin, zmin in enumerate(zminlist):

                    #
                    # here do the DFT at 'short' distance
                    #
                    isamp_bz = [zlist[iz]*latsp*2 for iz in range(0, zlist[-1])]
                    isamp_hz_real = [data_samples[iT][ipz][iz][isamp].real for iz in range(0, zlist[-1])]
                    isamp_hz_imag = [data_samples[iT][ipz][iz][isamp].imag for iz in range(0, zlist[-1])]
                    itp_hz_real = interp1d(isamp_bz, isamp_hz_real, kind='slinear')
                    itp_hz_imag = interp1d(isamp_bz, isamp_hz_imag, kind='slinear')
                    #zDFT = np.arange(0, isamp_bz[-1], zstep)
                    zDFT = np.arange(0, zmin*latsp*2, zstep)

                    def qPDF_DFT_real(x: float):
                        DFT = 0
                        for iz in range(1, len(zDFT)):
                            z1 = zDFT[iz-1]
                            z2 = zDFT[iz]
                            dz = z2 - z1
                            hz = 2*ipz_fm/(2*np.pi) * (itp_hz_real(z1)*np.cos((x-0.5)*ipz_fm*z1)+itp_hz_real(z2)*np.cos((x-0.5)*ipz_fm*z2))/2
                            DFT += hz*dz
                        return DFT

                    def qPDF_DFT_imag(x: float):
                        DFT = 0
                        for iz in range(1, len(zDFT)):
                            z1 = zDFT[iz-1]
                            z2 = zDFT[iz]
                            dz = z2 - z1
                            hz = 2*ipz_fm/(2*np.pi) * (itp_hz_imag(z1)*np.cos((x-0.5)*ipz_fm*z1)+itp_hz_imag(z2)*np.cos((x-0.5)*ipz_fm*z2))/2
                            DFT += hz*dz
                        return DFT

                    #
                    # here fit the long distance behavior
                    #
                    zsave = np.arange(data_meanerr[iT][ipz][zmin][1], 15*latsp*2, 0.02)

                    def Fit_res(popt):
                        res = []
                        popt[0] = popt[1]
                        popt[3] = popt[4]
                        for iz in range(zmin, len(zlist)):
                            diff_real = hz_extra_abAsymp(data_meanerr[iT][ipz][iz][1], popt[0], popt[1], popt[2], popt[6], meff=meff).real - data_samples[iT][ipz][iz][isamp].real
                            diff_imag = hz_extra_abAsymp(data_meanerr[iT][ipz][iz][1], popt[3], popt[4], popt[5], popt[6], meff=meff).real - data_samples[iT][ipz][iz][isamp].imag
                            res += [diff_real/data_meanerr[iT][ipz][iz][3].real, diff_imag/data_meanerr[iT][ipz][iz][3].imag]
                        return res
                    poptstart = popt_mean[izmin]
                    res = least_squares(Fit_res, poptstart, method='trf')
                    popt = list(res.x)
                    popt[-1] = abs(popt[-1]) + meff
                    chisq = sum(np.square(res.fun[:])) / (len(res.fun[:])-len(popt))
                    isamp_popt += [popt+[chisq]]

                    #
                    # here save the hz from the model prediction
                    #
                    def hz_real_predict(z):
                        return hz_extra_abAsymp(z, popt[0], popt[1], popt[2], popt[6]).real
                    def hz_imag_predict(z):
                        return hz_extra_abAsymp(z, popt[3], popt[4], popt[5], popt[6]).real
                    izmin_hz_Extra_real = [hz_real_predict(z) for z in zsave]
                    izmin_hz_Extra_imag = [hz_imag_predict(z) for z in zsave]
                    isamp_hz_Extra_real += [izmin_hz_Extra_real]
                    isamp_hz_Extra_imag += [izmin_hz_Extra_imag]

                    #
                    # here save the qPDF(x) from the model prediction
                    #
                    def qPDF_real(x):
                        return qPDF_DFT_real(x) + 2*ipz_fm/(2*np.pi) * integrate.quad(lambda z: hz_real_predict(z)*np.cos((x-0.5)*ipz_fm*z), zDFT[-1], 1000, limit=500)[0]
                    def qPDF_imag(x):
                        return qPDF_DFT_imag(x) + 2*ipz_fm/(2*np.pi) * integrate.quad(lambda z: hz_imag_predict(z)*np.cos((x-0.5)*ipz_fm*z), zDFT[-1], 1000, limit=500)[0]

                    isamp_qx_real += [[qPDF_real(x) for x in xlist]]
                    isamp_qx_imag += [[qPDF_imag(x) for x in xlist]]

                ipz_popt_samples += [isamp_popt]            # fit parameters
                ipz_hz_Extra_real += [isamp_hz_Extra_real]  # hz extrapolation real
                ipz_hz_Extra_imag += [isamp_hz_Extra_imag]  # hz extrapolation imag
                ipz_qx_real += [isamp_qx_real]              # qx real
                ipz_qx_imag += [isamp_qx_imag]              # qx imag

            # save popt mean err
            ipz_popt_meanerr = []
            for izmin, zmin in enumerate(zminlist):
                izmin_meanerr = [zmin]
                for ipopt in range(0, len(ipz_popt_samples[0][0])):
                    samples = sorted([ipz_popt_samples[isamp][izmin][ipopt] for isamp in range(0, npick)])
                    izmin_meanerr += [(samples[up]+samples[low])/2, (samples[up]-samples[low])/2]
                ipz_popt_meanerr += [izmin_meanerr]
                izmin_meanerr = [round(izmin_meanerr[i],2) for i in range(0, len(izmin_meanerr))]
                print(izmin_meanerr,'\n')
            savename = outfolder + ipz_describe.replace('#', '') + '_popt.dat'
            np.savetxt(savename, ipz_popt_meanerr, fmt='%.6e')

            # save extrapolated hz mean err
            for izmin, zmin in enumerate(zminlist):
                zsave = np.arange(data_meanerr[iT][ipz][zmin][1], 15*latsp*2, 0.02)
                izmin_real_0, izmin_real_1, izmin_real_2 = [], [], []
                izmin_imag_0, izmin_imag_1, izmin_imag_2 = [], [], []
                for iz, z in enumerate(zsave):
                    samples = sorted([ipz_hz_Extra_real[isamp][izmin][iz] for isamp in range(0, npick)])
                    izmin_real_0 += [[z, samples[low]]]
                    izmin_real_1 += [[z, samples[mid]]]
                    izmin_real_2 += [[z, samples[up]]]
                    samples = sorted([ipz_hz_Extra_imag[isamp][izmin][iz] for isamp in range(0, npick)])
                    izmin_imag_0 += [[z, samples[low]]]
                    izmin_imag_1 += [[z, samples[mid]]]
                    izmin_imag_2 += [[z, samples[up]]]
                izmin_describe = ipz_describe.replace('#', str(zmin) + '-' + str(zlist[-1]))
                savename = bandfolder + izmin_describe + '_hz_real_band0.dat'
                np.savetxt(savename, izmin_real_0, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_hz_real_band1.dat'
                np.savetxt(savename, izmin_real_1, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_hz_real_band2.dat'
                np.savetxt(savename, izmin_real_2, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_hz_imag_band0.dat'
                np.savetxt(savename, izmin_imag_0, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_hz_imag_band1.dat'
                np.savetxt(savename, izmin_imag_1, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_hz_imag_band2.dat'
                np.savetxt(savename, izmin_imag_2, fmt='%.6e')

            # save qx mean err
            for izmin, zmin in enumerate(zminlist):
                izmin_real_0, izmin_real_1, izmin_real_2 = [], [], []
                izmin_imag_0, izmin_imag_1, izmin_imag_2 = [], [], []
                for ix, x in enumerate(xlist):
                    samples = sorted([ipz_qx_real[isamp][izmin][ix] for isamp in range(0, npick)])
                    izmin_real_0 += [[x, samples[low]]]
                    izmin_real_1 += [[x, samples[mid]]]
                    izmin_real_2 += [[x, samples[up]]]
                    samples = sorted([ipz_qx_imag[isamp][izmin][ix] for isamp in range(0, npick)])
                    izmin_imag_0 += [[x, samples[low]]]
                    izmin_imag_1 += [[x, samples[mid]]]
                    izmin_imag_2 += [[x, samples[up]]]
                izmin_describe = ipz_describe.replace('#', str(zmin) + '-' + str(zlist[-1]))
                savename = bandfolder + izmin_describe + '_qPDFx_real_band0.dat'
                np.savetxt(savename, izmin_real_0, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_qPDFx_real_band1.dat'
                np.savetxt(savename, izmin_real_1, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_qPDFx_real_band2.dat'
                np.savetxt(savename, izmin_real_2, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_qPDFx_imag_band0.dat'
                np.savetxt(savename, izmin_imag_0, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_qPDFx_imag_band1.dat'
                np.savetxt(savename, izmin_imag_1, fmt='%.6e')
                savename = bandfolder + izmin_describe + '_qPDFx_imag_band2.dat'
                np.savetxt(savename, izmin_imag_2, fmt='%.6e')

            # save qx samples
            for izmin, zmin in enumerate(zminlist):
                izmin_describe = ipz_describe.replace('#', str(zmin) + '-' + str(zlist[-1]))
                samples_real, samples_imag = [], []
                for ix, x in enumerate(xlist):
                    samples_real += [[ipz_qx_real[isamp][izmin][ix] for isamp in range(0, npick)]]
                    samples_imag += [[ipz_qx_imag[isamp][izmin][ix] for isamp in range(0, npick)]]
                savename = samplefolder + izmin_describe + '_qPDFx_real_samples.dat'
                np.savetxt(savename, samples_real, fmt='%.6e')
                savename = samplefolder + izmin_describe + '_qPDFx_imag_samples.dat'
                np.savetxt(savename, samples_imag, fmt='%.6e')