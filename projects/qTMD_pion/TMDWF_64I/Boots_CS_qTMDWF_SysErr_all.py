#!/usr/bin/env python3
import numpy as np
import numba as nb
import math
import sys
import os
from scipy.interpolate import interp1d
from scipy import integrate
from utils.tools import *

def data_collection(mean_format, sample_format, bTlist, pzlist):
    mean = []
    samples = []
    for iT, bT in enumerate(bTlist):
        iT_mean_format = mean_format.replace('@', str(bT))
        iT_sample_format = sample_format.replace('@', str(bT))
        iT_mean, iT_samples = [], []
        for ipz, pz in enumerate(pzlist):
            ipz_mean = list(np.loadtxt(iT_mean_format.replace('*', str(pz))))
            ipz_samples = list(np.loadtxt(iT_sample_format.replace('*', str(pz))))
            iT_mean += [ipz_mean]
            iT_samples += [ipz_samples]
        mean += [iT_mean]
        samples += [iT_samples]
    return mean, samples

def CSkernel_LO(P1, P2, P1_data, P2_data):

    return 1/np.log(P2/P1) * np.log(abs(P2_data/P1_data))

'''----------------------------------'''
'''----------    #main#     ---------'''
'''----------------------------------'''

if __name__ == "__main__":


    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    
    # read and create the output folder
    inputfolder = os.path.dirname(sys.argv[1])
    outfolder = inputfolder + '/results/'
    mkdir(outfolder)
    outfolder = outfolder
    samplefolder = outfolder + '/samples/'
    mkdir(samplefolder)
    bandfolder = outfolder + '/bands/'
    mkdir(bandfolder)
    
    # read describe of this command file
    line1st = commands[0].split()
    describe = line1st[0]
    Ns = int(line1st[1])
    Nt = int(line1st[2])
    latsp = float(line1st[3])
    fmGeV = 5.0676896
    dP = 2*np.pi/(Ns*latsp*fmGeV)
    print('\n\n\n\n\n\n\nJob describe:',describe,'\nEnsemble:',Ns,'x',Nt,', a =',round(latsp,3),'fm, dP =', round(dP,8), 'GeV.\n')
    
    # read parameters
    for index in range(1, len(commands)):
        
        line = commands[index].split()

        if line[0] == 'bTrange':
            bTlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('bTlist:',bTlist)
            describe = describe.replace('@', str(bTlist[0])+'-'+str(bTlist[-1]))

        elif line[0] == 'pzrange':
            pzlist = [i for i in range(int(line[1]), int(line[2])+1)]
            print('pz list:',pzlist)

        elif line[0] == 'pskip':
            pskiplist = [int(line[i]) for i in range(1, len(line))]
            pskiplist = pskiplist[:1]
            print('pskip list:', pskiplist)

        elif line[0] == 'kernel':
            kernellist = [line[i] for i in range(1, len(line))]
            print('kernel list:', kernellist)

        elif line[0] == 'xrange':
            xlist = [i for i in range(int(line[1]), int(line[2])+1)]
            #print('xlist:', xlist)

        elif line[0] == 'mean':
            mean_format = line[1]
            print('\nmean:', mean_format.split('/')[-1])

        elif line[0] == 'sample':
            sample_format = line[1]
            print('sample:', sample_format.split('/')[-1],'\n')

    for ik, kernel in enumerate(kernellist):
        ik_mean_format = mean_format.replace('_kernel', '_'+kernel)
        ik_sample_format = sample_format.replace('_kernel', '_'+kernel)
        ik_describe = describe.replace('_kernel', '_'+kernel)

        mean, samples = [[] for iT in range(0, len(bTlist))], [[] for iT in range(0, len(bTlist))]
        xllist = [[] for iT in range(0, len(bTlist))]
        for iskip, pskip in enumerate(pskiplist):
            iskip_mean_format = ik_mean_format.replace('Pskip', 'Pskip'+str(pskip))
            iskip_sample_format = ik_sample_format.replace('Pskip', 'Pskip'+str(pskip))
            iskip_describe = ik_describe.replace('Pskip', 'Pskip'+str(pskip))
            iskip_pzlist = pzlist[pskip:]

            iskip_mean, iskip_samples = data_collection(iskip_mean_format, iskip_sample_format, bTlist, iskip_pzlist)
            npick = len(iskip_samples[0][0][0])
            low, mid, up = int(0.16*npick), int(0.5*npick), int(0.84*npick)

            xstart, xskip = iskip_mean[0][0][0][0], iskip_mean[0][0][1][0]-iskip_mean[0][0][0][0]
            for iT, bT in enumerate(bTlist):
                for ipz, pz in enumerate(iskip_pzlist):
                    xmin = min(1/(2*np.pi/64*bT*2*pz), 0.4) # 2*x*pz*bT>1
                    xl = max(int((xmin - xstart)/xskip), int((0.7/(2*pz*dP)-xstart)/xskip)) # 2*x*pz > 0.7
                    xr = len(iskip_mean[0][0]) - xl - 1
                    xllist[iT] += [xl]

            for iT in range(0, len(bTlist)):
                mean[iT] += iskip_mean[iT]
                samples[iT] += iskip_samples[iT]

        print('Kernel:', kernel)
        print('Read data in shape:', np.shape(mean), np.shape(samples))
        #print('xlist:',mean[0][0][xlist[0]][0],'-',mean[0][0][xlist[-1]][0])
        
        meanerr = []
        for iT, bT in enumerate(bTlist):
            iT_samples = []
            for i in range(0, len(samples[iT])):
                xl = xllist[iT][i]
                xr = len(mean[0][0]) - xl - 1
                xlist = [x for x in range(xl, xr)]
                iT_samples += [samples[iT][i][ix] for ix in xlist]
            print(np.shape(iT_samples))
            iT_mean_samples = sorted(np.mean(iT_samples, axis=0))
            iT_err_samples  = sorted(np.std(iT_samples, axis=0))
            iT_mean, iT_err_std, iT_err_sys = (iT_mean_samples[up]+iT_mean_samples[low])/2, (iT_mean_samples[up]-iT_mean_samples[low])/2, iT_err_samples[mid]
            #ipz_meanerr += [[bT, iT_mean, iT_err_std, iT_err_sys, np.sqrt(iT_err_std**2+iT_err_sys**2)]]
            meanerr += [[bT, iT_mean, iT_err_std, iT_err_sys, np.sqrt(iT_err_std**2+iT_err_sys**2)]]
        savename = outfolder + ik_describe.replace('*','') + 'CSx_Sys.dat'
        np.savetxt(savename, meanerr, fmt='%.6e')
        '''
        for ipz, pz in enumerate(iskip_pzlist):
            ipz_describe = iskip_describe.replace('*', str(pz))
            ipz_meanerr = []
            for iT, bT in enumerate(bTlist):
                iT_samples = [samples[iT][ipz][ix] for ix in xlist]
                iT_mean_samples = sorted(np.mean(iT_samples, axis=0))
                iT_err_samples  = sorted(np.std(iT_samples, axis=0))
                iT_mean, iT_err_std, iT_err_sys = (iT_mean_samples[up]+iT_mean_samples[low])/2, (iT_mean_samples[up]-iT_mean_samples[low])/2, iT_err_samples[mid]
                #ipz_meanerr += [[bT, iT_mean, iT_err_std, iT_err_sys, np.sqrt(iT_err_std**2+iT_err_sys**2)]]
                ipz_meanerr += [[bT, iT_mean, iT_err_std, iT_err_sys, iT_err_std+iT_err_sys]]
            savename = outfolder + ipz_describe + 'CSx_Sys.dat'
            np.savetxt(savename, ipz_meanerr, fmt='%.6e')
        '''