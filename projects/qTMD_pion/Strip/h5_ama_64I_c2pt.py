#!/usr/bin/env python3

import os
import sys
import h5py
import csv
import numpy as np

bxp="GSRC_W40_k0"
dir = "c2pt_cfgs/"
ex_files_format = dir+bxp+"_#_ex_files.txt"
sl_files_format = dir+bxp+"_#_sl_files.txt"
ama_files_format = dir + "64I"+bxp+".c2pt.PXPYPZ.real.cfg.dat"
list_cfg_64I_ALL = np.loadtxt('list_cfg_64I',dtype=int)
list_cfg_64I = list_cfg_64I_ALL
print("Ncfg=",len(list_cfg_64I))
pxlist = [0]
pylist = [0]
pzlist = [i for i in range(0, 4)]
smlist = ['SS']
Wlist = ['ama']
ama_files_format = ama_files_format.replace('c2pt.',str(Wlist[0])+'.c2pt.')
Nt=128
Ncfg = len(list_cfg_64I)

plist =  [ [0,0, pz, 0] for pz in pzlist ]
data_collection = np.zeros((len(plist), Nt, Ncfg+1))
for ip in range(0, len(plist)):
    for i in range(0, Nt):
        data_collection[ip][i][0] = i

def source_pos(sourcename):
    sourcename = sourcename.split('/')[-1]
    sourcename = sourcename.split('.')[4]
    x = np.char.index(sourcename, 'x')
    y = np.char.index(sourcename, 'y')
    z = np.char.index(sourcename, 'z')
    t = np.char.index(sourcename, 't')
    pos = np.array([int(sourcename[x+1:y]),int(sourcename[y+1:z]),int(sourcename[z+1:t]),int(sourcename[t+1:])])
    return pos

def pos_ph(p, pos):
    i = complex(0,1) * 2*np.pi/64
    #return np.exp(i * p[0] * pos[0]) * np.exp(i * p[1] * pos[1]) * np.exp(i * p[2] * pos[2])
    return 1
#return complex(-0.930532, - 0.254249)

for icfg, cfg in enumerate(list_cfg_64I):

    print(cfg)

    # ex
    src_files = open(ex_files_format.replace('#', str(cfg)), 'r')
    src_lines = src_files.readlines()
    src_files.close()

    cfg_data_ex = []
    cfg_data_bias = []
    for iline, line in enumerate(src_lines):
        isrc_file_name = line.split()[0]
        isrc_pos = source_pos(isrc_file_name)

        isrc_file_ex = h5py.File(isrc_file_name, "r")
        isrc_file_sl = h5py.File(isrc_file_name.replace("ex.","sl."), "r")

        isrc_data_ex = []
        isrc_data_bias = []
        for ip, p in enumerate(plist):
            ip_path = 'SS/5/'+'PX'+str(p[0])+'PY'+str(p[1])+'PZ'+str(p[2])
            ip_data_ex = np.array(isrc_file_ex[ip_path])#*pos_ph(p, isrc_pos)
            isrc_data_ex += [ip_data_ex]
            ip_data_sl = np.array(isrc_file_sl[ip_path])#*pos_ph(p, isrc_pos)
            #print(p,'\n',ip_data_ex,'\n',pos_ph(p, isrc_pos),'\n',ip_data_ex*pos_ph(p, isrc_pos), '\n\n')
            #print(pos_ph(p, isrc_pos))
            ip_data_bias = ip_data_ex - ip_data_sl
            isrc_data_bias += [ip_data_bias]
            #print(ip_data_ex[4], ip_data_bias[4])
        cfg_data_ex += [isrc_data_ex]
        cfg_data_bias += [isrc_data_bias]
        isrc_file_ex.close()

    # sl
    src_files = open(sl_files_format.replace('#', str(cfg)), 'r')
    src_lines = src_files.readlines()
    src_files.close()

    cfg_data_sl = []
    for iline, line in enumerate(src_lines):
        isrc_file_name = line.split()[0]
        isrc_pos = source_pos(isrc_file_name)

        isrc_file = h5py.File(isrc_file_name, "r")

        isrc_data = []
        for ip, p in enumerate(plist):
            ip_path = 'SS/5/'+'PX'+str(p[0])+'PY'+str(p[1])+'PZ'+str(p[2])
            ip_data = np.array(isrc_file[ip_path])#*pos_ph(p, isrc_pos)
            isrc_data += [ip_data]
        cfg_data_sl += [isrc_data]
        isrc_file.close()
    print(np.shape(cfg_data_sl))

    # ama
    for ip, p in enumerate(plist):
        for i in range(0, len(cfg_data_bias[0][0])):
            i_data_bias = [cfg_data_bias[iline][ip][i].real for iline in range(0, len(cfg_data_bias))]
            i_data_sl = [cfg_data_sl[iline][ip][i].real for iline in range(0, len(cfg_data_sl))]
            data_collection[ip][i][icfg+1] = np.mean(i_data_sl)+np.mean(i_data_bias)


for ip, p in enumerate(plist):
    savename = ama_files_format.replace('PXPYPZ','PX'+str(p[0])+'PY'+str(p[1])+'PZ'+str(p[2]))
    with open(savename, 'w') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(['csv, Nt='+str(Nt)+',Ncfg='+str(Ncfg)])
        for raw in data_collection[ip]:
            writer.writerows([raw])
        outfile.close()


    
