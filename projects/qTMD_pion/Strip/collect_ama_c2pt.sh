#!/bin/bash

cfglist=`cat list_cfg_64I`
path_in='c2pt'
path_out="c2pt_cfgs"
mkdir -p $path_out 1>/dev/null 2>&1
gm='I'
#bxplist=('PointSrc_gf')
smlist=('SS')
pxlist=(0)
pylist=(0)
pzlist=(0 1 2 3 4)
filesize="K"
bxp='GSRC_W40_k0'

for cfg in ${cfglist[*]}
do
    echo $cfg
    rm ${path_out}/${bxp}_${cfg}_ex_files.txt 1>/dev/null 2>&1
    src_count=0
    for exsrc in `ls -lh $path_in/64I.c2pt.$cfg.ex.*.$bxp.h5 | grep $filesize | awk '{print $9}' | cut -d "." -f5`
    do
        #if [ $src_count -gt 10 ]
        #then
        #    break
        #fi
        src_count=`expr $src_count + 1`
        echo "ex source $src_count: ${exsrc}"
        echo "${path_in}/64I.c2pt.$cfg.ex.${exsrc}.$bxp.h5" >> ${path_out}/${bxp}_${cfg}_ex_files.txt
    done
done

for cfg in ${cfglist[*]}
do
    echo $cfg
    rm ${path_out}/${bxp}_${cfg}_sl_files.txt 1>/dev/null 2>&1
    src_count=0
    for slsrc in `ls -lh $path_in/64I.c2pt.$cfg.sl.*.$bxp.h5 | grep $filesize | awk '{print $9}' | cut -d "." -f5`
    do
        src_count=`expr $src_count + 1`
        echo "sl source $src_count: ${slsrc}"
        echo "${path_in}/64I.c2pt.$cfg.sl.${slsrc}.$bxp.h5" >> ${path_out}/${bxp}_${cfg}_sl_files.txt
    done
done 
