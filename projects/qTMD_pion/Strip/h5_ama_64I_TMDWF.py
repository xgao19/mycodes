#!/usr/bin/env python3

import os
import sys
import h5py
import csv
import numpy as np


"""
================================================================================
                                    data setup
================================================================================
"""

bxp="GSRC_W40_k0"
dir = "qTMDWF_cfgs/"
ex_files_format = dir+bxp+"_#_ex_files.txt"
sl_files_format = dir+bxp+"_#_sl_files.txt"
ama_files_format = dir + "64I.qTMDWF.ama."+bxp+".gm.eta.PXPYPZ.h5"
list_cfg_64I_ALL = np.loadtxt('list_cfg_64I',dtype=int)
list_cfg_64I = list_cfg_64I_ALL
print("Ncfg=",len(list_cfg_64I))
Ncfg = len(list_cfg_64I)
Nt=128

sm = 'SP'
gmlist=["5", "T5", "Z5"]
pxlist = [0]
pylist = [0]
pzlist = [i for i in range(0, 4)]
plist  =  [ [0,0, pz, 0] for pz in pzlist ]
bTdir  = ['b_X', 'b_Y']
etalist= [15,20,25]
bTlist = [i for i in range(0, 16)]
bzlist = [i for i in range(0, 16)]

"""
================================================================================
                            frequent used functions
================================================================================
"""

def source_pos(sourcename):
    sourcename = sourcename.split('/')[-1]
    sourcename = sourcename.split('.')[4]
    x = np.char.index(sourcename, 'x')
    y = np.char.index(sourcename, 'y')
    z = np.char.index(sourcename, 'z')
    t = np.char.index(sourcename, 't')
    pos = np.array([int(sourcename[x+1:y]),int(sourcename[y+1:z]),int(sourcename[z+1:t]),int(sourcename[t+1:])])
    return pos

def pos_ph(p, pos):
    i = complex(0,1) * 2*np.pi/64
    #return np.exp(i * p[0] * pos[0]) * np.exp(i * p[1] * pos[1]) * np.exp(i * p[2] * pos[2])
    return 1
#return complex(-0.930532, - 0.254249)


"""
================================================================================
                                    AMA loops
================================================================================
"""


save_files = []
for ip, p in enumerate(plist):
    for ig, gm in enumerate(gmlist):
        ig_ama_files_format = ama_files_format.replace('PXPYPZ','PX'+str(p[0])+'PY'+str(p[1])+'PZ'+str(p[2])).replace('gm',gm)
        for ie, eta in enumerate(etalist):
            ie_ama_files_format = ig_ama_files_format.replace('eta', 'eta'+str(eta))
            save_files += [ie_ama_files_format]
            print('Gonna save in:',ie_ama_files_format)
print('Gonna save in shape:', np.shape(save_files))

print('\n\nStart AMA loops:')
save_data = []
for icfg, cfg in enumerate(list_cfg_64I):

    print('\n@',cfg)

    # collect data: ex
    src_files = open(ex_files_format.replace('#', str(cfg)), 'r')
    src_lines = src_files.readlines()
    src_files.close()

    cfg_data_ex = []
    cfg_data_bias = []
    for iline, line in enumerate(src_lines):
        isrc_file_name = line.split()[0]

        isrc_file_ex = h5py.File(isrc_file_name, "r")
        isrc_file_sl = h5py.File(isrc_file_name.replace("ex.","sl."), "r")

        isrc_data_bias = []
        for ip, p in enumerate(plist):
            for ig, gm in enumerate(gmlist):

                ig_path = 'SP/'+gm+'/PX'+str(p[0])+'PY'+str(p[1])+'PZ'+str(p[2])
 
                for ie, eta in enumerate(etalist):  

                    ie_data_bias = []

                    for idir, Tdir in enumerate(bTdir):
                        idir_path = ig_path + '/' + Tdir  + '/eta' + str(eta)
                        idir_data_bias = []
                        for iT, bT in enumerate(bTlist):
                            iT_path = idir_path + '/bT' + str(bT)
                            iT_data_bias = []
                            for iz, bz in enumerate(bzlist):
                                iz_path = iT_path + '/bz' + str(bz)
                                iz_data_ex = np.array(isrc_file_ex[iz_path])
                                iz_data_sl = np.array(isrc_file_sl[iz_path])
                                iz_data_bias = iz_data_ex - iz_data_sl
                                iT_data_bias += [iz_data_bias]
                            idir_data_bias += [iT_data_bias]
                        ie_data_bias += [idir_data_bias]
                    isrc_data_bias += [ie_data_bias]
        cfg_data_bias += [isrc_data_bias]
        print('\tex isrc:', iline+1)
        isrc_file_ex.close()
        isrc_file_sl.close()
    print('\tCollect bias in shape:', np.shape(cfg_data_bias))

    # collect data: sl
    src_files = open(sl_files_format.replace('#', str(cfg)), 'r')
    src_lines = src_files.readlines()
    src_files.close()

    cfg_data_sl = []
    for iline, line in enumerate(src_lines):
        isrc_file_name = line.split()[0]

        isrc_file_sl = h5py.File(isrc_file_name, "r")

        isrc_data_sl = []
        for ip, p in enumerate(plist):
            for ig, gm in enumerate(gmlist):

                ig_path = 'SP/'+gm+'/PX'+str(p[0])+'PY'+str(p[1])+'PZ'+str(p[2])
 
                for ie, eta in enumerate(etalist):  

                    ie_data_sl = []

                    for idir, Tdir in enumerate(bTdir):
                        idir_path = ig_path + '/' + Tdir  + '/eta' + str(eta)
                        idir_data_sl = []
                        for iT, bT in enumerate(bTlist):
                            iT_path = idir_path + '/bT' + str(bT)
                            iT_data_sl = []
                            for iz, bz in enumerate(bzlist):
                                iz_path = iT_path + '/bz' + str(bz)
                                iz_data_sl = np.array(isrc_file_sl[iz_path])
                                iT_data_sl += [iz_data_sl]
                            idir_data_sl += [iT_data_sl]
                        ie_data_sl += [idir_data_sl]
                    isrc_data_sl += [ie_data_sl]
        cfg_data_sl += [isrc_data_sl]
        print('\tsl isrc:', iline+1)
        isrc_file_sl.close()
    print('\tCollect sloppy in shape:', np.shape(cfg_data_sl))

    # collect data: ama
    cfg_data_ama = []
    for iset in range(0, len(cfg_data_bias[0])):

        iset_data_ama = []
        for idir, Tdir in enumerate(bTdir):
            idir_data_ama = []
            for iT, bT in enumerate(bTlist):
                iT_data_ama = []
                for iz, bz in enumerate(bzlist):
                    iz_data_bias = [cfg_data_bias[isrc][iset][idir][iT][iz] for isrc in range(0, len(cfg_data_bias))]
                    iz_data_sl = [cfg_data_sl[isrc][iset][idir][iT][iz] for isrc in range(0, len(cfg_data_sl))]
                    iz_data_ama = np.mean(iz_data_bias, axis=0) + np.mean(iz_data_sl, axis=0)
                    iT_data_ama += [iz_data_ama]
                idir_data_ama += [iT_data_ama]
            iset_data_ama += [idir_data_ama]
        cfg_data_ama += [iset_data_ama]
    save_data += [cfg_data_ama]
print('Collect ama in shape:', np.shape(save_data))


for iset, save_h5 in enumerate(save_files):
    f = h5py.File(save_h5, 'w')
    for idir, Tdir in enumerate(bTdir):
        g_Tdir = f.create_group(Tdir)
        for iT, bT in enumerate(bTlist):
            g_bT = g_Tdir.create_group('bT' + str(bT))
            for iz, bz in enumerate(bzlist):
                iz_data = [np.array(save_data[icfg][iset][idir][iT][iz]) for icfg in range(0, len(save_data))]
                g_bT.create_dataset('bz' + str(bz), data=iz_data)
    f.close()