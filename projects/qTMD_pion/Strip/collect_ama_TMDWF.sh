#!/bin/bash

cfglist=`cat list_cfg_64I`
path_in='qTMDWF'
path_out="qTMDWF_cfgs"
mkdir -p $path_out 1>/dev/null 2>&1
filesize="5"
bxp='GSRC_W40_k0'

for cfg in ${cfglist[*]}
do
    echo $cfg
    rm ${path_out}/${bxp}_${cfg}_ex_files.txt 1>/dev/null 2>&1
    src_count=0
    for exsrc in `ls -lh $path_in/64I.qTMDWF.$cfg.ex.*.$bxp.h5 | grep $filesize | awk '{print $9}' | cut -d "." -f5`
    do
        #if [ $src_count -gt 10 ]
        #then
        #    break
        #fi
        src_count=`expr $src_count + 1`
        echo "ex source $src_count: ${exsrc}"
        echo "${path_in}/64I.qTMDWF.$cfg.ex.${exsrc}.$bxp.h5" >> ${path_out}/${bxp}_${cfg}_ex_files.txt
    done
done

for cfg in ${cfglist[*]}
do
    echo $cfg
    rm ${path_out}/${bxp}_${cfg}_sl_files.txt 1>/dev/null 2>&1
    src_count=0
    for slsrc in `ls -lh $path_in/64I.qTMDWF.$cfg.sl.*.$bxp.h5 | grep $filesize | awk '{print $9}' | cut -d "." -f5`
    do
        src_count=`expr $src_count + 1`
        echo "sl source $src_count: ${slsrc}"
        echo "${path_in}/64I.qTMDWF.$cfg.sl.${slsrc}.$bxp.h5" >> ${path_out}/${bxp}_${cfg}_sl_files.txt
    done
done 
