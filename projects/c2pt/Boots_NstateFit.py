#!/usr/bin/env python3
import numpy as np
import h5py
import sys
import os
from utils.tools import *
from scipy.optimize import least_squares
from scipy.linalg import cholesky
from scipy.linalg import inv

"""
================================================================================
                            Constants and Functions
================================================================================
"""

#def boots_pick(samples, npick):
#    np.random.seed(2020)
#    random_mx = np.array(list(map(int,np.random.uniform(0,len(samples),npick*npick))))
#    boots_samples = np.array(samples)[random_mx]
#    return np.mean(boots_samples.reshape(npick,npick), axis=1)

TSIZE = 64
print("NOTE: TSIZE =", TSIZE)
def c2pt_Nstate(x, AE):
    c2pt = 0
    for i in range(0, int(len(AE)/2)):
        A = AE[2*i]
        E = AE[2*i+1]
        c2pt += A * (np.exp(-E * x)+np.exp(-E * (TSIZE-x)))
    return c2pt

def c2pt_1state(x, A, E):
    c2pt = A * (np.exp(-E * x)+np.exp(-E * (TSIZE-x)))
    #print(c2pt)
    return c2pt

def read_qTMDWF_h5(file, Tdirlist, bTlist, bzlist):
    signlist = [-1 for i in range(0, 64)] + [1 for i in range(0, 64)]
    f = h5py.File(file,'r')
    data = []
    for iT, Tdir in enumerate(Tdirlist):
        iT_path = Tdir
        iT_data = []
        for ib, bT in enumerate(bTlist):
            ib_path = iT_path + '/bT' + str(bT)
            ib_data = []
            for iz, bz in enumerate(bzlist):
                iz_path = ib_path + '/bz' + str(bz)
                iz_data = np.array(f[iz_path])
                iz_data_new = []
                for row in iz_data:
                    iz_data_new += [[(row[i].real)*signlist[i] for i in range(0, len(row))]]
                #ib_data += [np.transpose(iz_data_new)]
                ib_data += [folddat(np.transpose(iz_data_new))]
            iT_data += [ib_data]
        data += [iT_data]
    return data[0]

# Take a 2-d list, and average per bin with certain bin size.
def boots_avebin(data, binsize):
    new_data = []
    ncfg = len(data[0])-1
    for i in range(0, len(data)):
        i_new_data = [data[i][0]]
        for j in range(0, int(ncfg/binsize)):
            i_new_data += [np.average(data[i][1+j*binsize:1+(j+1)*binsize])]
        #print(ncfg,binsize,int(ncfg/binsize),len(i_new_data))
        new_data += [i_new_data]
    return new_data

def boots_pick(samples, npick):
    np.random.seed(2020)
    random_mx = np.array(list(map(int,np.random.uniform(0,len(samples),npick*npick))))
    boots_samples = np.array(samples)[random_mx]
    return np.mean(boots_samples.reshape(npick,npick), axis=1)


"""
================================================================================
                                    Analysis
================================================================================
"""

if __name__ == "__main__":

    # read the job describe and commands
    inputfile = open(sys.argv[1],'r')
    commands = inputfile.readlines()
    describe = (commands[0].split())[0]
    line = commands[1].split()
    dataset = csvtolist(line[0])

     # default, mkdir at the input file folder
    datafolder = os.path.dirname(sys.argv[1])
    resultfolder = datafolder + '/results/'
    mkdir(resultfolder)
    samplefolder = resultfolder + '/samples/'
    mkdir(samplefolder)
    samplename = samplefolder + describe + '_samples'
    print('\n\n\njob describe:', describe)

    # read the fit range xmin and xmax
    xminl = int(line[2])
    xminr = int(line[3])
    xmax = int(line[4])
    npick = int(line[5])
    binsize = 1
    if len(line) == 7:
        binsize = int(line[6])
    print('tmin range:',xminl,xminr, '<<>> tmax:',xmax, '\nnpick:',npick, '\nbin size:', binsize)
    low = int(0.16*npick)
    mid = int(0.5*npick)
    up  = int(0.84*npick)

    # fold or not fold the data
    print('\n@@@ configuration number:', len(dataset[-1])-1)
    print('@@@',line[0].split('/')[-1],np.shape(dataset))
    if line[1] == 'f':
            dataset = folddat(dataset)
            print('\n@@@ Data is folded!!!')
    elif line[1] == 'nf':
            print('\n@@@ Data is unfolded!!!')
    print('@@@ tmax:',dataset[-1][0],'\n')

    # collect the fit model and fit parameters
    poptstart_all = []
    prior_all = []
    fix_all = []
    for index in range(2,len(commands)):
        line = commands[index].split()

        # poptstart for Nstate fit
        if line[0] == 'fit':
            linestart = [float(line[i]) for i in range(1, len(line))]
            poptstart_all += [linestart]

        # priors for the fit
        elif line[0] == 'prior':
            lineprior = [int(line[1]), float(line[2]), float(line[3]) ]
            prior_all += [lineprior]

        # fix parameter for the fit
        elif line[0] == 'fix':
            linefix = [int(line[1]), float(line[2])]
            fix_all += [linefix]

    print('fit initial condition:',linestart)
    print('fit priors:',prior_all)
    print('fit fix:',fix_all)


    print('\n')

    # fit loop
    for i, poptstart in enumerate(poptstart_all):

        imodel = str(int(len(poptstart)/2)) + 'state'
        imodel_name = resultfolder + '/' + describe.replace('nst','nst'+str(int(len(poptstart)/2)))  + '_tmax' + str(xmax) + '_' + imodel + '.dat'
        print(imodel_name.split('/')[-1])

        popt_list = []
        for xmin in range(xminl,xminr+1):
                
            xmin_sample_name = samplefolder + '/' + describe.replace('nst','nst'+str(int(len(poptstart)/2))) + '_tmin' + str(xmin)  + '_tmax' + str(xmax) + '_' + imodel + '_bootsamples.dat'
            xmin_data = dataset[xmin:xmax+1]
            #xmin_data = dataset[xmin:xmin+15]
            if binsize > 1:
                xmin_data = boots_avebin(xmin_data, binsize)
            print(np.shape(xmin_data), '[', xmin_data[0][0], '~', xmin_data[-1][0], ']')

            boots_c2pt = []
            for i in range(0, len(xmin_data)):
                boots_c2pt += [boots_pick(xmin_data[i][1:], npick)]
            boots_c2pt = np.transpose(boots_c2pt)

            x = np.array([row[0] for row in xmin_data])
            y = np.mean(np.delete(xmin_data,0,1), axis=1)
            ycov = np.cov(np.delete(xmin_data,0,1))

            # scipy.linalg.cholesky requires lower=True to return L L^T = A
            # Chisq = (y - yd)^T C^{-1} (y-yd)
            # transform = L such that C = L L^T
            # C^{-1} = L^{-T} L^{-1}
            # Chisq = (y - yd)^T L^{-T} L^{-1} (y-yd)
            # Define (y-yd)' = L^{-1} (y-yd)
            # by solving
            # L (y-yd)' = (y-yd)
            # and minimize (y-yd)'^T (y-yd)'
            transform = cholesky(ycov, lower=True)
            #ystd = np.std(np.delete(xmin_data,0,1), axis=1)

            popt_samples = []
            for i in range(0, npick):
                y = boots_c2pt[i]
                if len(fix_all) == 0:
                    def Fit_res(popt):
                        data_diff = c2pt_Nstate(x, popt) - y
                        res = np.dot(inv(transform), data_diff)
                        #res = data_diff/ystd
                        for line in prior_all:
                            res = np.append(res, (popt[line[0]]-line[1])/line[2])
                        return res
                else:
                    def Fit_res(popt):
                        for fix in fix_all:
                            #popt[fix[0]] = fix[1]
                            popt_in = list(popt[:fix[0]]) + [fix[1]] + list(popt[fix[0]:])
                        #print(popt_in)
                        data_diff = c2pt_Nstate(x, popt_in) - y
                        res = np.dot(inv(transform), data_diff)
                        #res = data_diff/ystd
                        for line in prior_all:
                            res = np.append(res, (popt[line[0]]-line[1])/line[2])
                        return res
                i_poptstart = np.array(poptstart)*np.random.normal(1, 0.3, len(poptstart))
                #print('\n',i_poptstart)
                res = least_squares(Fit_res, np.delete(i_poptstart, [fix[0] for fix in fix_all]), method='lm', diff_step=0.1)
                if len(fix_all) == 0:
                    popt = res.x
                else:
                    for fix in fix_all:
                        popt= list(res.x[:fix[0]]) + [fix[1]] + list(res.x[fix[0]:])
                chisq = sum(np.square(res.fun[:])) / (len(y)-len(popt))
                popt_samples += [list(popt) + [chisq]]
                #print(chisq)
            np.savetxt(xmin_sample_name, popt_samples, fmt='%.6e')
            popt_MeanErr = [xmin, xmax]
            for ip in range(0, len(poptstart)+1):
                ip_samples = sorted([popt_samples[i][ip] for i in range(0, len(popt_samples))])
                popt_MeanErr += [(ip_samples[up]+ip_samples[low])/2, (ip_samples[up]-ip_samples[low])/2]
            print(["%.4e"%popt_MeanErr[i] for i in range(0, len(popt_MeanErr))])
            popt_list += [popt_MeanErr]

        np.savetxt(imodel_name, popt_list, fmt='%.6e')
        

            



    '''
    # save the fit results
    if 'prior' in fitparams[0][0]:
            fitparams = fitparams[1:]
    for imodel in range(0,len(popt)):
            modelname = fitparams[imodel][1]
            print(modelname)
            outfilename = resultfolder + '/' + describe + '_' + modelname + '.txt'
            outfile = open(outfilename,'w')
            np.savetxt(outfile, popt[imodel], fmt='%.6e')
            outfile.close()

    # save the aicc picked model:
    outfilename = resultfolder + '/' + describe + '_aicc.txt'
    #outfile = open(outfilename,'w')
    #np.savetxt(outfile, popt_aicc, fmt='%.6e')
    #outfile.close()
    '''
