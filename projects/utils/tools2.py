#!/usr/bin/env python3

import os
import csv
import numpy as np
from tools import *

#----------------------------------------------------#
###################Jackknife tools####################
#----------------------------------------------------#
'''
Split 2d data into Jackknife blocks
'''


'''
command file should be:
c2pt_SP 543 32
c2pt px0 filepostion
c2pt px1 fileposition
'''
def txtcsvRI(commandfile):

    inputfile = open(commandfile,'r')
    commands = inputfile.readlines()

    # read and create the output folder
    inputfolder = os.path.dirname(commandfile)
    outfolder = inputfolder + '/results'   # default, mkdir at the input file folder
    mkdir(outfolder)
    outfolder = outfolder
            
    # read describe of this command file
    describe = (commands[0].split())[0]
    ncfg = int((commands[0].split())[1])
    nt = int((commands[0].split())[2])
    print('describe:',describe,', ncfg =',ncfg,', nt =',nt)

    for index in range(0,len(commands)):
        line = commands[index].split()
        if line[0] == 'c2pt' or line[0] == 'c3pt':
            label = line[1]
            filename = line[2]
            c2ptfile = open(filename,"r")
            c2ptread = c2ptfile.readlines()
            c2ptdata = []
            for line in c2ptread:
                dataline = line.split()
                dataline = list(map(float, dataline))
                c2ptdata += [dataline]
            c2ptfile.close()
            #c2ptdata = np.loadtxt(filename)
            print(np.shape(c2ptdata))
            newc2pt_real = []
            newc2pt_imag = []
            for t in range(0, nt):
                inewc2pt_real = [t]
                inewc2pt_imag = [t]
                for i in range(0, ncfg):
                    inewc2pt_real += [c2ptdata[i*nt+t+1][2]]
                    inewc2pt_imag += [-c2ptdata[i*nt+t+1][1]]
                newc2pt_real += [inewc2pt_real]
                newc2pt_imag += [inewc2pt_imag]
            #print(np.shape(newc2pt_real),newc2pt_real[1][1],newc2pt_real[2][1],newc2pt_real[3][1])
            savename_real = outfolder + '/' + describe + '_' + label + '_real.txt'
            savename_imag = outfolder + '/' + describe + '_' + label + '_imag.txt'
            savecsv(newc2pt_real, savename_real, c2ptdata[0])
            savecsv(newc2pt_imag, savename_imag, c2ptdata[0])

