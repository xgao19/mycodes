#!/usr/bin/env python3
import numpy as np
import numba as nb
import scipy.optimize as op
#from qGPD_nucleon.OPE.rITD_Numerical_Kernel_isoV import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ######################## Global parameters ######################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

CF = 4/3
nf = 3
TF = 1/2
CA = 3
fmGeV = 5.0676896
gammaE = 0.5772156649

beta = {
    0: 11/3*CA - 4/3*TF*nf,
    1: 34/3*CA**2 - (20/3*CA + 4*CF)*TF*nf,
    2: 2857/54*CA**3 + (2*CF**2 - 205/9*CF*CA - 1415/27*CA**2)*TF*nf + (44/9*CF + 158/27*CA)*TF**2*nf**2,
}

def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

# one-loop alphas; mu = [GeV]
def alphas_1loop(mu, Nf = 3):
    LambdaQCD = 0.184
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    L = np.log(mu**2/LambdaQCD**2)
    return 1/(beta0*L)

# n-loop alphas; mu = [GeV]
def alphas_nloop(mu, order = 0, Nf = 3):

    aS = 0.293/(4*np.pi)
    X = 1 + aS*beta[0]*np.log((mu/2)**2)

    if order < 1:
        return aS*4*np.pi/X
    elif order == 1:
        return aS*4*np.pi / (X + aS*beta[1]/beta[0]*np.log(X))
    elif order == 2:
        return aS*4*np.pi / (X + aS*beta[1]/beta[0]*np.log(X) + aS**2 * (beta[2]/beta[0]*(1-1/X) + beta[1]**2/beta[0]**2*(np.log(X)/X+1/X-1)))
    else:
        sys.exit('NNNLO running coupling not coded.')
    #elif order == 3:
    #    return aS*4*np.pi / (X + aS*beta[1]/beta[0]*np.log(X) \
    #        + aS**2 * (beta[2]/beta[0]*(1-1/X) + beta[1]**2/beta[0]**2*(np.log(X)/X+1/X-1)) \
    #            + aS**3 * (-beta[1]**3/(2*beta[0]**3) + beta[1]*beta[2]/beta[0]**2*np.log(X)/X**2 + (1-1/X)*(beta[3]/(2*beta[0]*(1+1/X)) - beta[1]*beta[2]/beta[0]**2+1/2*(1-1/X))))

# five-loop alphas; mu = [GeV]
def alphas_5loop(mu, Nf = 3):
    LambdaQCD = 0.332
    beta0 = (33 - 2*Nf)/(12 * np.pi)
    beta1 = (153 - 19*Nf)/(24 * np.pi**2)
    beta2 = (77139 - 15099*Nf + 325*Nf**2)/(3456 * np.pi**3)
    beta3 = (29242.964136194125 - 6946.289617003554*Nf + 405.0890404598629*Nf**2 + 1.4993141289437584*Nf**3)/(256 * np.pi**4)
    beta4 = (524.5582754592147 - 181.79877882258594*Nf + 17.156013333434416*Nf**2 - 0.22585710219837543*Nf**3 - 0.0017992914141834987*Nf**4)/(np.pi**5)
    L = np.log(mu**2/LambdaQCD**2)
    alpha_s = 1/(beta0*L) - 1/(beta0**2*L**2)*beta1/beta0*np.log(L) + 1/(beta0**3*L**3)*((beta1/beta0)**2*(np.log(L)**2-np.log(L)-1) + beta2/beta0) + \
        1/(beta0**4*L**4)*((beta1/beta0)**3*(-np.log(L)**3 + 5/2*np.log(L)**2 + 2*np.log(L) - 1/2) - (3*beta1*beta2)/beta0**2*np.log(L) + beta3/(2*beta0)) + \
            1/(beta0**5*L**5)*((3*beta1**2*beta2)/beta0**3*(2*np.log(L)**2 - np.log(L) - 1) + (beta1/beta0)**4*(6*np.log(L)**4 - 26*np.log(L)**3 - 9*np.log(L)**2 + 24*np.log(L) + 7)/6 \
                - (beta1*beta3)/(6*beta0**2)*(12*np.log(L) + 1) + 5/3*(beta2/beta0)**2 + beta4/(3*beta0))
    return alpha_s

#mulist = np.array([1,2,3,4,5,6,7,8,9,10])
#print(alphas_1loop(mulist))
#print(alphas_5loop(mulist))