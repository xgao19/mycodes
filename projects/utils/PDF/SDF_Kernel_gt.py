#!/usr/bin/env python3
import numpy as np
import numba as nb
from utils.PDF.SDF_Kernel_gt_Numerical import *
from utils.Constants import *

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### Auxiliary functions ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#mu in GeV
def alphas(mu, Nf = 3):
    return alphas_5loop(mu, Nf)

@nb.jit
def as2pi(alps):
    return alps/(2*np.pi)

@nb.jit
def polynomial2(xz, a3):
    z,x = xz
    return 1 - a3 * (x*x/2)
def Hn(n):
    if n == 0:
        return 0
    else:
        return sum([1/i for i in range(1,n+1)])
def Hn2(n):
    if n == 0:
        return 0
    else:
        return sum([1/i/i for i in range(1,n+1)])

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NLO Matching Kernel ######################## ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#mu in GeV
def CnNLO(z,n, mu,alps):
    L = np.log(mu*mu*z*z*np.exp(2*gammaE)/4)
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*L\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n))

#mu in GeV
def CnNLOpol(z, n, mu,alps):
    L = np.log(mu*mu*z*z*np.exp(2*gammaE)/4)
    return 1 + alps*CF/(2*np.pi)*(((3+2*n)/(2+3*n+n*n)+2*Hn(n))*L\
        +(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)) + alps*CF/(2*np.pi)*2/(2+3*n+n*n)


def CnNLOevo(z,n, mu, alps=0,k=0):
    b1 = 11-2*3/3
    Bn = 2*CF/b1*((3+2*n)/(2+3*n+n*n)+2*Hn(n))
    muz0 = np.sqrt(4*np.exp(k)/(z*z*np.exp(2*gammaE)))
    part0 = ((3+2*n)/(2+3*n+n*n)+2*Hn(n))*k+(5+2*n)/(2+3*n+n*n)+2*(1-Hn(n))*Hn(n)-2*Hn2(n)
    part1 = 1 + alphas(muz0)*CF/(2*np.pi)*part0
    part2 = (alphas(muz0)/alphas(mu))**Bn
    return part1*part2

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ####################### NNLO Matching Kernel ####################### ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def CN2p2Pm(n):
    num_list = [0, 0, -13.8786, 0, -18.7847, 0, -22.0259, 0, -24.5027, 0, -26.5244, 0, -28.239, 0, -29.7306, 0, -31.0521]
    return num_list[n]
def GammaN1(n):
    return CF * ((3+2*n)/(2+3*n+n*n) + 2*Hn(n))
def GammaN2(n):
    gm0f1 = CF*CF * (-5/8 + 2*np.pi*np.pi/3) + CF*CA*(49/24-np.pi*np.pi/6) + CF*nf*TF*(-5/6)
    return -CN2p2Pm(n) + gm0f1
def NNLOevoExpo(n, mu):
    #print(mu,alpsmuNNLO(mu))
    beta0 = (11*CA - 4*nf*TF)/6
    beta1 = (102 - 38*nf/3)/4
    part1 = -(GammaN1(n)*np.log(as2pi(alphas(mu)))/beta0)
    part2 = -1/(beta0*beta1) * (-beta1*GammaN1(n) + beta0*GammaN2(n)) * np.log(beta0 + as2pi(alphas(mu)) * beta1)
    #print(part1, part2)
    return part1 + part2

def CnNNLOevo(z,n, mu,k=0, **kwargs):
    muz0 = np.sqrt(4/(z*z*np.exp(2*gammaE)))
    return CnNNLO(z, n, muz0, alphas(muz0)) * np.exp(NNLOevoExpo(n, mu)-NNLOevoExpo(n, muz0))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# ^v^ ########################## rITD kernels ############################ ^v^ #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def CnC0LO(mu,z,n):
    return 1
def CnC0NLO(mu,z,n):
    return CnNLO(z*fmGeV, n, mu, alphas(mu))/CnNLO(z*fmGeV, 0, mu, alphas(mu))
def CnC0NLOevo(mu,z,n):
    return CnNLOevo(z*fmGeV, n, mu, alphas(mu))/CnNLOevo(z*fmGeV, 0, mu, alphas(mu))
def CnC0NLOpol(mu,z,n):
    return CnNLOpol(z*fmGeV, n, mu, alphas(mu))/CnNLOpol(z*fmGeV, 0, mu, alphas(mu))
def CnC0NNLO(mu,z,n):
    return CnNNLO(z*fmGeV, n, mu, alphas(mu))/CnNNLO(z*fmGeV, 0, mu, alphas(mu))
def CnC0NNLOevo(mu,z,n):
    return CnNNLOevo(z*fmGeV, n, mu, alphas(mu))/CnNNLOevo(z*fmGeV, 0, mu, alphas(mu))