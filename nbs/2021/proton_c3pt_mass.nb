(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      8366,        223]
NotebookOptionsPosition[      6508,        185]
NotebookOutlinePosition[      6844,        200]
CellTagsIndexPosition[      6801,        197]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   RowBox[{"Log", "[", 
    RowBox[{"2.729411224168268", "/", "6.094887826785494"}], "]"}]}], "/", 
  "2"}]], "Input",
 CellChangeTimes->{{3.836892362189712*^9, 3.836892403838745*^9}},
 CellLabel->"In[31]:=",ExpressionUUID->"e5bb646f-fb45-4e82-94c3-d244b078e811"],

Cell[BoxData["0.40168222074639226`"], "Output",
 CellChangeTimes->{{3.8368923967672358`*^9, 3.836892404067741*^9}},
 CellLabel->"Out[31]=",ExpressionUUID->"01be388b-fc22-4d97-9aac-7cb67830244c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"proton_posSxminus", ".", "D"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"EGeV", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         RowBox[{"Log", "[", 
          RowBox[{"x", "/", "y"}], "]"}]}], "/", "2"}], "/", "0.076"}], "/", 
      "5.0676896"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"2.729411224168268", ",", "6.094887826785494"}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"6.094887826785494", ",", "14.077514688144957"}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"14.077514688144957", ",", "32.986837049401697"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.8368924056530743`*^9, 3.836892534919619*^9}, {
  3.836892594069252*^9, 3.8368925986073847`*^9}},
 CellLabel->"In[45]:=",ExpressionUUID->"0f50531f-a254-445a-bc21-aece0774e238"],

Cell[BoxData["1.042939247550206`"], "Output",
 CellChangeTimes->{{3.836892459032668*^9, 3.8368925007012997`*^9}, 
   3.8368925352640753`*^9, 3.83689259951012*^9},
 CellLabel->"Out[46]=",ExpressionUUID->"67cbeb2f-5eea-4863-8ed9-001f2a731ab8"],

Cell[BoxData["1.0867721847058898`"], "Output",
 CellChangeTimes->{{3.836892459032668*^9, 3.8368925007012997`*^9}, 
   3.8368925352640753`*^9, 3.8368925995121202`*^9},
 CellLabel->"Out[47]=",ExpressionUUID->"93c5419c-d688-4df3-b26d-197306ca30b7"],

Cell[BoxData["1.1054681852500803`"], "Output",
 CellChangeTimes->{{3.836892459032668*^9, 3.8368925007012997`*^9}, 
   3.8368925352640753`*^9, 3.8368925995139093`*^9},
 CellLabel->"Out[48]=",ExpressionUUID->"5f2bfdde-b73c-458e-be38-f0091d7849c0"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"proton_posSxminus", ".", "U"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"EGeV", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         RowBox[{"Log", "[", 
          RowBox[{"x", "/", "y"}], "]"}]}], "/", "2"}], "/", "0.076"}], "/", 
      "5.0676896"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"5.3972096230341855", ",", "12.089709075879716"}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"12.089709075879716", ",", "28.070265101888895"}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"28.070265101888895", ",", "66.0060965724331"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.83689263491352*^9, 3.83689265115221*^9}, {
  3.836892703214148*^9, 3.836892743162911*^9}},
 CellLabel->"In[61]:=",ExpressionUUID->"47a5d6c8-c3fa-47a1-8e79-8ffe83e1fbfb"],

Cell[BoxData["1.0469741965504726`"], "Output",
 CellChangeTimes->{
  3.836892651486926*^9, {3.836892704036428*^9, 3.836892743568809*^9}},
 CellLabel->"Out[62]=",ExpressionUUID->"f8edd260-8350-4410-b81b-df7edae08844"],

Cell[BoxData["1.0935589515415174`"], "Output",
 CellChangeTimes->{
  3.836892651486926*^9, {3.836892704036428*^9, 3.83689274357067*^9}},
 CellLabel->"Out[63]=",ExpressionUUID->"038f7ad7-1413-4cf4-94d8-bcc4aacdc122"],

Cell[BoxData["1.1100203639395447`"], "Output",
 CellChangeTimes->{
  3.836892651486926*^9, {3.836892704036428*^9, 3.8368927435723352`*^9}},
 CellLabel->"Out[64]=",ExpressionUUID->"0e5ca0ca-7dd0-4c28-8e97-000f9f447edd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"proton_posSxplus", ".", "D"}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"EGeV", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"-", 
         RowBox[{"Log", "[", 
          RowBox[{"x", "/", "y"}], "]"}]}], "/", "2"}], "/", "0.076"}], "/", 
      "5.0676896"}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"2.729411224168268", ",", "6.094887826785494"}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"6.094887826785494", ",", "14.077514688144957"}], "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EGeV", "[", 
    RowBox[{"14.077514688144957", ",", "32.986837049401697"}], 
    "]"}]}]}]], "Input",
 CellChangeTimes->{{3.836892792849902*^9, 3.836892794492712*^9}},
 CellLabel->"In[65]:=",ExpressionUUID->"9822ce45-ea4f-4152-bd23-982eaa48e78e"],

Cell[BoxData["1.042939247550206`"], "Output",
 CellChangeTimes->{3.836892794901794*^9},
 CellLabel->"Out[66]=",ExpressionUUID->"7bb4b466-3fb3-474f-bdaa-4accdd7224c2"],

Cell[BoxData["1.0867721847058898`"], "Output",
 CellChangeTimes->{3.836892794903885*^9},
 CellLabel->"Out[67]=",ExpressionUUID->"fa1b4f52-27b0-46f7-8d67-06a0a90f815c"],

Cell[BoxData["1.1054681852500803`"], "Output",
 CellChangeTimes->{3.836892794905136*^9},
 CellLabel->"Out[68]=",ExpressionUUID->"fa4d06de-7c4f-4221-9b6d-8fa1352726a5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"2", "*", "0.076", "*", "5.0676896"}]], "Input",
 CellChangeTimes->{{3.836893943902071*^9, 3.836893954136135*^9}},
 CellLabel->"In[69]:=",ExpressionUUID->"49c8ac65-04a3-4a51-b3d4-c565fb2149a9"],

Cell[BoxData["0.7702888191999999`"], "Output",
 CellChangeTimes->{3.836893954441346*^9},
 CellLabel->"Out[69]=",ExpressionUUID->"d0460456-8655-4686-86a1-70e243e4e45c"]
}, Open  ]]
},
WindowSize->{808, 807},
WindowMargins->{{Automatic, 57}, {Automatic, 0}},
FrontEndVersion->"12.0 for Mac OS X x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 303, 7, 30, "Input",ExpressionUUID->"e5bb646f-fb45-4e82-94c3-d244b078e811"],
Cell[886, 31, 194, 2, 34, "Output",ExpressionUUID->"01be388b-fc22-4d97-9aac-7cb67830244c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1117, 38, 1010, 27, 115, "Input",ExpressionUUID->"0f50531f-a254-445a-bc21-aece0774e238"],
Cell[2130, 67, 241, 3, 34, "Output",ExpressionUUID->"67cbeb2f-5eea-4863-8ed9-001f2a731ab8"],
Cell[2374, 72, 245, 3, 34, "Output",ExpressionUUID->"93c5419c-d688-4df3-b26d-197306ca30b7"],
Cell[2622, 77, 245, 3, 34, "Output",ExpressionUUID->"5f2bfdde-b73c-458e-be38-f0091d7849c0"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2904, 85, 1005, 27, 115, "Input",ExpressionUUID->"47a5d6c8-c3fa-47a1-8e79-8ffe83e1fbfb"],
Cell[3912, 114, 216, 3, 34, "Output",ExpressionUUID->"f8edd260-8350-4410-b81b-df7edae08844"],
Cell[4131, 119, 215, 3, 34, "Output",ExpressionUUID->"038f7ad7-1413-4cf4-94d8-bcc4aacdc122"],
Cell[4349, 124, 218, 3, 34, "Output",ExpressionUUID->"0e5ca0ca-7dd0-4c28-8e97-000f9f447edd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4604, 132, 956, 26, 115, "Input",ExpressionUUID->"9822ce45-ea4f-4152-bd23-982eaa48e78e"],
Cell[5563, 160, 166, 2, 34, "Output",ExpressionUUID->"7bb4b466-3fb3-474f-bdaa-4accdd7224c2"],
Cell[5732, 164, 167, 2, 34, "Output",ExpressionUUID->"fa1b4f52-27b0-46f7-8d67-06a0a90f815c"],
Cell[5902, 168, 167, 2, 34, "Output",ExpressionUUID->"fa4d06de-7c4f-4221-9b6d-8fa1352726a5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6106, 175, 216, 3, 30, "Input",ExpressionUUID->"49c8ac65-04a3-4a51-b3d4-c565fb2149a9"],
Cell[6325, 180, 167, 2, 69, "Output",ExpressionUUID->"d0460456-8655-4686-86a1-70e243e4e45c"]
}, Open  ]]
}
]
*)

